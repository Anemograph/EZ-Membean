# A simple python script that parses the important parts of the membean new word pages
# Beautiful Soup is required

from bs4 import BeautifulSoup
import os,json
with open("../out.json", 'w') as jsonFile:
    for files in os.listdir("."):
        with open(files, 'r') as fp:
            soup = BeautifulSoup(fp, "lxml")

            word = soup.find(id='wordform-container').h1.text
            context = soup.find(id="context-paragraph").text
            definition = soup.find(id="definition").li.text
            question = soup.find(class_="choice answer").text
            glance = soup.find(class_="one-word-tab-right").text
            rootContainers = soup.find_all(class_="common")
            rootDict = {}
            if(rootContainers):
                for x in rootContainers:
                    root = x.parent.text
                    meaning = x.parent.parent.parent.find(class_="meaning").text
                    rootDict[root] = meaning


            json.dump({word : {"def": definition, "cont": context, "quest": question, "hint": glance, "root": rootDict }}, jsonFile, indent=4);
