
<!DOCTYPE html>
<html>
<head>
<title>Word: wholesome | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An acrid smell or taste is strong, unpleasant, and stings your nose or throat.</p>
<p class='rw-defn idx1' style='display:none'>The adjective agrarian is used to describe something that is related to farmland or the economy that is concerned with agriculture.</p>
<p class='rw-defn idx2' style='display:none'>Aliment is something, usually food, that feeds, nourishes, or supports someone or something else.</p>
<p class='rw-defn idx3' style='display:none'>Someone is baleful if they are filled with bad intent, anger, or hatred towards another person.</p>
<p class='rw-defn idx4' style='display:none'>Something brackish is distasteful and unpleasant usually because something else is mixed in with it; brackish water, for instance, is a mixture of fresh and salt water and cannot be drunk.</p>
<p class='rw-defn idx5' style='display:none'>A comestible is something that can be eaten.</p>
<p class='rw-defn idx6' style='display:none'>A contagion is a disease—or the transmission of that disease—that is easily spread from one person to another through touch or the air.</p>
<p class='rw-defn idx7' style='display:none'>Someone who has excellent culinary skills is able to cook food very well.</p>
<p class='rw-defn idx8' style='display:none'>Something that has curative properties can be used for curing people&#8217;s illnesses.</p>
<p class='rw-defn idx9' style='display:none'>A dank room or building is unpleasantly damp and chilly.</p>
<p class='rw-defn idx10' style='display:none'>If you describe something, especially food and drink, as delectable, you mean that it is very pleasant, tasty, or attractive.</p>
<p class='rw-defn idx11' style='display:none'>Something that is deleterious has a harmful effect.</p>
<p class='rw-defn idx12' style='display:none'>Something detrimental causes damage, harm, or loss to someone or something.</p>
<p class='rw-defn idx13' style='display:none'>If you imbibe ideas, values, or qualities, you absorb them into your mind.</p>
<p class='rw-defn idx14' style='display:none'>If someone is inimical, they are unfriendly and will likely cause you harm.</p>
<p class='rw-defn idx15' style='display:none'>A malignant thing or person, such as a tumor or criminal, is deadly or does great harm.</p>
<p class='rw-defn idx16' style='display:none'>If something is noisome, it is extremely unpleasant, especially because it is very dirty or has an awful smell.</p>
<p class='rw-defn idx17' style='display:none'>Something that is noxious is harmful, extremely unpleasant, and often poisonous.</p>
<p class='rw-defn idx18' style='display:none'>When you nurture someone, you feed and take care of them.</p>
<p class='rw-defn idx19' style='display:none'>If you describe something as palatable, such as an idea or suggestion, you believe that people will accept it; palatable food or drink is agreeable to the taste.</p>
<p class='rw-defn idx20' style='display:none'>A pathogen is an organism, such as a bacterium or virus, that causes disease.</p>
<p class='rw-defn idx21' style='display:none'>If you have a pathological condition, you are extreme or unreasonable in something that you do.</p>
<p class='rw-defn idx22' style='display:none'>Something that is pernicious is very harmful or evil, often in a way that is hidden or not quickly noticed.</p>
<p class='rw-defn idx23' style='display:none'>Potable water is clean and safe to drink.</p>
<p class='rw-defn idx24' style='display:none'>A putrid substance is decaying or rotting; therefore, it is also foul and stinking.</p>
<p class='rw-defn idx25' style='display:none'>To quaff is to gulp down a drink.</p>
<p class='rw-defn idx26' style='display:none'>A salubrious place or area is pleasant, clean, healthy, and comfortable to live in.</p>
<p class='rw-defn idx27' style='display:none'>A salutary experience is beneficial to you since it strengthens you in some way, although it may be unpleasant as you undergo it; this word also refers to promoting good health.</p>
<p class='rw-defn idx28' style='display:none'>An environment or character can be sordid—the former dirty, the latter low or base in an immoral or greedy sort of way.</p>
<p class='rw-defn idx29' style='display:none'>Subsistence is the means someone has to support their existence, usually referring to food and shelter.</p>
<p class='rw-defn idx30' style='display:none'>Sustenance is that which supports life; it usually refers to food or nourishment of some kind or to one&#8217;s livelihood.</p>
<p class='rw-defn idx31' style='display:none'>Something is toothsome when it is tasty, attractive, or pleasing in some way; this adjective can apply to food, people, or objects.</p>
<p class='rw-defn idx32' style='display:none'>An unguent is a soothing salve.</p>
<p class='rw-defn idx33' style='display:none'>If you describe something as unsavory, you mean that it is unpleasant or morally unacceptable.</p>
<p class='rw-defn idx34' style='display:none'>Something that is unsullied is unstained and clean.</p>
<p class='rw-defn idx35' style='display:none'>A virulent disease is very dangerous and spreads very quickly.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>wholesome</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='wholesome#' id='pronounce-sound' path='audio/words/amy-wholesome'></a>
HOHL-suhm
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='wholesome#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='wholesome#' id='context-sound' path='audio/wordcontexts/brian-wholesome'></a>
Yoga is a <em>wholesome</em> or healthy activity which not only promotes excellent health, but also tends to improve one&#8217;s character.  The practice of yoga insists that one eat a <em>wholesome</em>, beneficial diet which makes one&#8217;s physical body feel great.  The <em>wholesome</em> practice of yoga leads towards a sense of well-being and concern for one&#8217;s fellow human beings as well, stressing the holiness of all life and non-violence towards all.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does something that is <em>wholesome</em> do?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
It improves your character, mind, and body.
</li>
<li class='choice '>
<span class='result'></span>
It encourages you to go on a new adventure.
</li>
<li class='choice '>
<span class='result'></span>
It forces you to do difficult physical exercises.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='wholesome#' id='definition-sound' path='audio/wordmeanings/amy-wholesome'></a>
The adjective <em>wholesome</em> can refer to something that is good for the health of your body and also for your moral health.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>healthy</em>
</span>
</span>
</div>
<a class='quick-help' href='wholesome#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='wholesome#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/wholesome/memory_hooks/4401.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Whole</span></span> Foods, and Then <span class="emp2"><span>Some</span></span></span></span> When I think of <span class="emp3"><span>whole</span></span><span class="emp2"><span>some</span></span> I think of the organic food sold at <span class="emp3"><span>Whole</span></span> Foods, which is very good for you, and then <span class="emp2"><span>some</span></span>!
</p>
</div>

<div id='memhook-button-bar'>
<a href="wholesome#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/wholesome/memory_hooks">Use other hook</a>
<a href="wholesome#" id="memhook-use-own" url="https://membean.com/mywords/wholesome/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='wholesome#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Much like most successful weight loss plans, dieters are encouraged to eat <b>wholesome</b> foods high in fiber, along with lean protein to reduce cravings and help satisfy hunger on fewer calories.
<cite class='attribution'>
&mdash;
WebMD
</cite>
</li>
<li>
Although a year would pass before several of the White Sox admitted throwing the Series, there was talk, and the committee quite possibly felt the game needed a more <b>wholesome</b> image.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
"Macho" is not a word that comes up frequently in discussions of [Chris] O'Donnell, who has often been described as "<b>wholesome</b>," "clean-cut" and, yes, "bland."
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
<li>
However, the recipe in question, which comes from Heidi Swanson's "Super Natural Cooking," asks us to re-evaluate our pantries in the spirit of more <b>wholesome</b>, healthful eating.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/wholesome/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='wholesome#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>whole</td>
<td>
&rarr;
</td>
<td class='meaning'>whole, healthy</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='some_having' data-tree-url='//cdn1.membean.com/public/data/treexml' href='wholesome#'>
<span class=''></span>
-some
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>having a certain quality</td>
</tr>
</table>
<p>A <em>wholesome</em> food has the &#8220;quality of being whole or healthy,&#8221; that is, it is not pumped full of chemicals or preservatives, but rather is very &#8220;healthy&#8221; for the body.</p>
<a href="wholesome#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> Old English</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='wholesome#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Nourish Life</strong><span> Just look at all the wholesome food available at this farmers' market!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/wholesome.jpg' video_url='examplevids/wholesome' video_width='350'></span>
<div id='wt-container'>
<img alt="Wholesome" height="288" src="https://cdn1.membean.com/video/examplevids/wholesome.jpg" width="350" />
<div class='center'>
<a href="wholesome#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='wholesome#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Wholesome" src="https://cdn2.membean.com/public/images/wordimages/cons2/wholesome.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='wholesome#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>agrarian</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>aliment</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>comestible</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>culinary</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>curative</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>delectable</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>imbibe</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>nurture</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>palatable</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>potable</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>quaff</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>salubrious</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>salutary</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>subsistence</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>sustenance</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>toothsome</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>unguent</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>unsullied</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>acrid</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>baleful</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>brackish</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>contagion</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>dank</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>deleterious</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>detrimental</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>inimical</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>malignant</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>noisome</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>noxious</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>pathogen</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>pathological</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>pernicious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>putrid</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>sordid</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>unsavory</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>virulent</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="wholesome" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>wholesome</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="wholesome#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

