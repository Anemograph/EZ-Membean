
<!DOCTYPE html>
<html>
<head>
<title>Word: prophetic | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>To <em>adumbrate</em> is to describe something incompletely or to suggest future events based on limited current knowledge.</p>
<p class='rw-defn idx1' style='display:none'>Augury is the process or art of reading omens about the future through specific ceremonies.</p>
<p class='rw-defn idx2' style='display:none'>If you are bemused, you are puzzled and confused; hence, you are lost or absorbed in puzzling thought.</p>
<p class='rw-defn idx3' style='display:none'>A clairvoyant person believes that they can predict the future or communicate with the spirits of the dead.</p>
<p class='rw-defn idx4' style='display:none'>Something that is clandestine is hidden or kept a secret, often because it is illegal.</p>
<p class='rw-defn idx5' style='display:none'>An epiphany is the moment when someone suddenly realizes or understands something of great significance or importance.</p>
<p class='rw-defn idx6' style='display:none'>If someone behaves in a furtive way, they do things sneakily and secretly in order to avoid being noticed.</p>
<p class='rw-defn idx7' style='display:none'>A harbinger is a sign that foretells that something is going to happen, especially something bad.</p>
<p class='rw-defn idx8' style='display:none'>Hindsight is the looking back upon an event to see what should have been done instead of what was done.</p>
<p class='rw-defn idx9' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx10' style='display:none'>An ominous sign predicts or shows that something bad will happen in the near future.</p>
<p class='rw-defn idx11' style='display:none'>A portent is a warning that indicates what is likely to happen in the future, which is usually something unpleasant.</p>
<p class='rw-defn idx12' style='display:none'>A first event is a precursor to a second event if the first event is responsible for the development or existence of the second.</p>
<p class='rw-defn idx13' style='display:none'>To presage a future event is to give a sign or warning that something (usually) bad is about to happen.</p>
<p class='rw-defn idx14' style='display:none'>Someone who is prescient knows or is able to predict what will happen in the future.</p>
<p class='rw-defn idx15' style='display:none'>To prognosticate is to predict or forecast something.</p>
<p class='rw-defn idx16' style='display:none'>Stupor is a state in which someone&#8217;s mind and senses are dulled; consequently, they are unable to think clearly or act normally, usually due to the use of alcohol or drugs.</p>
<p class='rw-defn idx17' style='display:none'>A surreptitious deed is done secretly to avoid bringing any attention to it.</p>
<p class='rw-defn idx18' style='display:none'>Something uncanny is very strange, unnatural, or highly unusual.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>prophetic</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='prophetic#' id='pronounce-sound' path='audio/words/amy-prophetic'></a>
pruh-FET-ik
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='prophetic#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='prophetic#' id='context-sound' path='audio/wordcontexts/brian-prophetic'></a>
Some people believe that tarot cards have <em>prophetic</em> powers because they can supposedly predict the future.  When I was young, my older sister foretold my upcoming adult life through these <em>prophetic</em> cards.  The cards would always foretell that I would marry early in life; their <em>prophetic</em> signs were accurate, because that is exactly what I did.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If someone has <em>prophetic</em> abilities, what can they do?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
They can make predictions about the future.
</li>
<li class='choice '>
<span class='result'></span>
They can tell what other people are thinking.
</li>
<li class='choice '>
<span class='result'></span>
They can learn new things extremely quickly.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='prophetic#' id='definition-sound' path='audio/wordmeanings/amy-prophetic'></a>
Something or someone that is <em>prophetic</em> has the quality or ability to predict the future.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>predictive</em>
</span>
</span>
</div>
<a class='quick-help' href='prophetic#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='prophetic#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/prophetic/memory_hooks/4440.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Prophet</span></span> <span class="emp2"><span>Prophet</span></span>ic for <span class="emp2"><span>Profit</span></span></span></span> The <span class="emp2"><span>prophet</span></span> was accurately <span class="emp2"><span>prophet</span></span>ic for making huge <span class="emp2"><span>profit</span></span>s on the stock market, so everyone would ask for his services.
</p>
</div>

<div id='memhook-button-bar'>
<a href="prophetic#" id="add-public-hook" style="" url="https://membean.com/mywords/prophetic/memory_hooks">Use other public hook</a>
<a href="prophetic#" id="memhook-use-own" url="https://membean.com/mywords/prophetic/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='prophetic#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
While the gods themselves do not make an appearance, some of the humans of the story do have visions which we readers know to be significantly <b>prophetic</b>.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
In the Morrinsville College 1998 yearbook, there's a <b>prophetic</b> phrase written in Comic Sans: "Most likely to become Prime Minister . . . Jacinda Ardern."
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
Its post-modern message—lamenting society's addiction to constant digital communication—proved <b>prophetic</b>, as so many of us began to quell quarantine-induced pangs of isolation with increased screen time.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/prophetic/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='prophetic#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>prophet</td>
<td>
&rarr;
</td>
<td class='meaning'>prophet, predictor</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ic_like' data-tree-url='//cdn1.membean.com/public/data/treexml' href='prophetic#'>
<span class=''></span>
-ic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>nature of, like</td>
</tr>
</table>
<p>A <em>prophetic</em> person has the nature of a predictor.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='prophetic#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Groundhog Day</strong><span> Phil is prophetic.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/prophetic.jpg' video_url='examplevids/prophetic' video_width='350'></span>
<div id='wt-container'>
<img alt="Prophetic" height="288" src="https://cdn1.membean.com/video/examplevids/prophetic.jpg" width="350" />
<div class='center'>
<a href="prophetic#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='prophetic#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Prophetic" src="https://cdn2.membean.com/public/images/wordimages/cons2/prophetic.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='prophetic#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adumbrate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>augury</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>clairvoyant</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>epiphany</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>harbinger</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>ominous</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>portent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>precursor</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>presage</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>prescient</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>prognosticate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>uncanny</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>bemused</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>clandestine</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>furtive</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>hindsight</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>stupor</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>surreptitious</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="prophetic" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>prophetic</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="prophetic#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

