
<!DOCTYPE html>
<html>
<head>
<title>Word: sartorial | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Sartorial-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/sartorial-large.jpg?qdep8" />
</div>
<a href="sartorial#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>To accentuate something is to emphasize it or make it more noticeable.</p>
<p class='rw-defn idx1' style='display:none'>A man who is dapper has a very neat and clean appearance; he is both fashionable and stylish.</p>
<p class='rw-defn idx2' style='display:none'>A man who is debonair wears fashionable clothes and is sophisticated, charming, friendly, and confident.</p>
<p class='rw-defn idx3' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx4' style='display:none'>To denude an area is to remove the plants and trees that cover it; it can also mean to make something bare.</p>
<p class='rw-defn idx5' style='display:none'>If you embellish something, you make it more beautiful by decorating it.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is fastidious cares excessively about small details and wants to keep everything correct, tidy, very clean, and in perfect order.</p>
<p class='rw-defn idx7' style='display:none'>If someone or something is flamboyant, the former is trying to show off in a way that deliberately attracts attention, and the latter is brightly colored and highly decorated.</p>
<p class='rw-defn idx8' style='display:none'>A gauche person behaves in an awkward and unsuitable way in public because they lack social skills.</p>
<p class='rw-defn idx9' style='display:none'>A genteel person is well-mannered, has good taste, and is very polite in social situations; a genteel person is often from an upper-class background.</p>
<p class='rw-defn idx10' style='display:none'>Something grotesque is so distorted or misshapen that it is disturbing, bizarre, gross, or very ugly.</p>
<p class='rw-defn idx11' style='display:none'>Something that is immaculate is very clean, pure, or completely free from error.</p>
<p class='rw-defn idx12' style='display:none'>If you describe someone&#8217;s appearance or behavior as impeccable, you mean that it is perfect and therefore impossible to criticize.</p>
<p class='rw-defn idx13' style='display:none'>Something loathsome is offensive, disgusting, and brings about intense dislike.</p>
<p class='rw-defn idx14' style='display:none'>Something macabre is so gruesome or frightening that it causes great horror in those who see it.</p>
<p class='rw-defn idx15' style='display:none'>Someone is considered meticulous when they act with careful attention to detail.</p>
<p class='rw-defn idx16' style='display:none'>If you do something with panache, you do it in a way that shows great skill, style, and confidence.</p>
<p class='rw-defn idx17' style='display:none'>Animals preen when they smooth out their fur or their feathers; humans preen by making themselves beautiful in front of a mirror.</p>
<p class='rw-defn idx18' style='display:none'>To be punctilious is to pay precise attention to detail.</p>
<p class='rw-defn idx19' style='display:none'>Something that is repulsive is offensive, highly unpleasant, or just plain disgusting.</p>
<p class='rw-defn idx20' style='display:none'>People or things that are resplendent are beautiful, bright, and impressive in appearance.</p>
<p class='rw-defn idx21' style='display:none'>When you experience revulsion, you feel a great deal of disgust or extreme dislike for something.</p>
<p class='rw-defn idx22' style='display:none'>A slovenly person is untidy or messy.</p>
<p class='rw-defn idx23' style='display:none'>The word squalor describes very dirty and unpleasant conditions that people live or work in, usually due to poverty or neglect.</p>
<p class='rw-defn idx24' style='display:none'>A troglodyte is reclusive, severely lacking in social skills, and is out of step with current times.</p>
<p class='rw-defn idx25' style='display:none'>To act in an uncouth manner is to be awkward and unmannerly.</p>
<p class='rw-defn idx26' style='display:none'>An unkempt person or thing is untidy and has not been kept neat.</p>
<p class='rw-defn idx27' style='display:none'>If you say someone&#8217;s behavior is unseemly, you disapprove of it because it is not in good taste or not suitable for a particular situation.</p>
<p class='rw-defn idx28' style='display:none'>An unsightly person presents an ugly, unattractive, or disagreeable face to the world.</p>
<p class='rw-defn idx29' style='display:none'>If you behave in an urbane way, you are behaving in a polite, refined, and civilized fashion in social situations.</p>
<p class='rw-defn idx30' style='display:none'>A yokel is uneducated, naive, and unsophisticated; they do not know much about modern life or ideas because they sequester themselves in a rural setting.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>sartorial</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='sartorial#' id='pronounce-sound' path='audio/words/amy-sartorial'></a>
sawr-TOR-ee-uhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='sartorial#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='sartorial#' id='context-sound' path='audio/wordcontexts/brian-sartorial'></a>
Jay-Z has <em>sartorial</em> elegance or dresses very well, and his fashion line has been available in retail stores for several years now.  His wife, Beyonce, is fashionable as well, and she attributes her fine taste in clothing to her mother who has such great <em>sartorial</em> skills that she makes all her own clothing.  The clothing brand sold by Jay-Z has helped many men show up to important events with impressive <em>sartorial</em> fashion, that is, very well dressed.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Who is likely to have <em>sartorial</em> skill?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A musician.
</li>
<li class='choice answer '>
<span class='result'></span>
A tailor.
</li>
<li class='choice '>
<span class='result'></span>
A talented public speaker.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='sartorial#' id='definition-sound' path='audio/wordmeanings/amy-sartorial'></a>
If you have good <em>sartorial</em> taste, you know what to wear to your best advantage, or you have the skills of a tailor.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>of clothing</em>
</span>
</span>
</div>
<a class='quick-help' href='sartorial#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='sartorial#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/sartorial/memory_hooks/6240.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Tailor</span></span></span></span> Unscramble the last six letters of "sar<span class="emp3"><span>torial</span></span>" and you get "<span class="emp3"><span>tailor</span></span>."
</p>
</div>

<div id='memhook-button-bar'>
<a href="sartorial#" id="add-public-hook" style="" url="https://membean.com/mywords/sartorial/memory_hooks">Use other public hook</a>
<a href="sartorial#" id="memhook-use-own" url="https://membean.com/mywords/sartorial/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='sartorial#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The attempt turned tragic when Siefert made the <b>sartorial</b> blunder of wearing the boat shoes with socks, incurring the derision and scorn of his more fashion-forward peers.
<cite class='attribution'>
&mdash;
The Onion
</cite>
</li>
<li>
The golfer who just made a <b>sartorial</b> splash on the golf course in a pair of red plaid slacks would not be a man to give up his new freedom of plumage overnight.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
If the tiny status symbol isn’t enough to coax fans into buying adult clothing, each garment will also come with a helpful pamphlet explaining the rules of fashion to <b>sartorial</b> n00bs [newbies or inexperienced newcomers].
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
For men, <b>sartorial</b> good taste can be reduced to one rule: If Fred Astaire would not have worn it, don’t wear it. For women, substitute Grace Kelly.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/sartorial/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='sartorial#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>sartor</td>
<td>
&rarr;
</td>
<td class='meaning'>tailor</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='al_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='sartorial#'>
<span class=''></span>
-al
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p><em>Sartorial</em> expertise &#8220;relates to tailors.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='sartorial#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Sartorial" src="https://cdn0.membean.com/public/images/wordimages/cons2/sartorial.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='sartorial#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>accentuate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>dapper</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>debonair</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>embellish</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>fastidious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>flamboyant</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>genteel</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>immaculate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>impeccable</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>meticulous</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>panache</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>preen</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>punctilious</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>resplendent</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>urbane</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='4' class = 'rw-wordform notlearned'><span>denude</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>gauche</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>grotesque</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>loathsome</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>macabre</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>repulsive</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>revulsion</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>slovenly</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>squalor</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>troglodyte</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>uncouth</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>unkempt</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>unseemly</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>unsightly</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>yokel</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="sartorial" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>sartorial</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="sartorial#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

