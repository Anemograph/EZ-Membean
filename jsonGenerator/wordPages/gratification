
<!DOCTYPE html>
<html>
<head>
<title>Word: gratification | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx1' style='display:none'>Someone who is crestfallen is severely disappointed, sad, or depressed.</p>
<p class='rw-defn idx2' style='display:none'>If you are despondent, you are extremely unhappy because you are in an unpleasant situation that you do not think will improve.</p>
<p class='rw-defn idx3' style='display:none'>If you are disconsolate, you are very unhappy or so sad that nothing will make you feel better.</p>
<p class='rw-defn idx4' style='display:none'>When you disillusion someone, you show that a belief they hold dear is untrue—thereby disheartening and disappointing them.</p>
<p class='rw-defn idx5' style='display:none'>Something that is dolorous, such as music or news, causes mental pain and sorrow because it itself is full of grief and sorrow.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx7' style='display:none'>When you are ecstatic about something, you are overjoyed or extremely happy about it.</p>
<p class='rw-defn idx8' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx9' style='display:none'>If you show exuberance, you display great excitement, energy, and enthusiasm.</p>
<p class='rw-defn idx10' style='display:none'>If you exult, you show great pleasure and excitement, especially about something you have achieved.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx12' style='display:none'>If something, such as an idea or plan, comes to fruition, it produces the result you wanted to achieve from it.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is inconsolable has been so devastated by a terrible event that no one can help them feel better about it.</p>
<p class='rw-defn idx14' style='display:none'>If someone is lachrymose, they tend to cry often; if something, such as a song, is lachrymose, it tends to cause tears because it is so sad.</p>
<p class='rw-defn idx15' style='display:none'>If someone is lugubrious, they are looking very sad or gloomy.</p>
<p class='rw-defn idx16' style='display:none'>If you are melancholy, you look and feel sad.</p>
<p class='rw-defn idx17' style='display:none'>Someone who is morose is unhappy, bad-tempered, and unwilling to talk very much.</p>
<p class='rw-defn idx18' style='display:none'>If you are pensive, you are deeply thoughtful, often in a sad and/or serious way.</p>
<p class='rw-defn idx19' style='display:none'>Revelry is a festive celebration that includes wild, noisy, and happy dancing, eating, and drinking.</p>
<p class='rw-defn idx20' style='display:none'>If you are sanguine about a situation, especially a difficult one, you are confident and cheerful that everything will work out the way you want it to.</p>
<p class='rw-defn idx21' style='display:none'>Someone who is saturnine is looking miserable and sad, sometimes in a threatening or unfriendly way.</p>
<p class='rw-defn idx22' style='display:none'>If a person is vindicated, their ideas, decisions, or actions—once considered wrong—are proved correct or not to be blamed.</p>
<p class='rw-defn idx23' style='display:none'>Someone who is woebegone is very sad and filled with grief.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>gratification</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='gratification#' id='pronounce-sound' path='audio/words/amy-gratification'></a>
grat-i-fi-KAY-shun
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='gratification#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='gratification#' id='context-sound' path='audio/wordcontexts/brian-gratification'></a>
When Ragav finished his coding for the day, he felt a great deal of <em>gratification</em> or satisfaction in his wonderful accomplishment.  He felt such <em>gratification</em> or enjoyment because he had had to solve a particularly difficult problem, and he was able to do so after much thought and effort.  His <em>gratification</em> or delight extended only until the next day, however, when he had to fix yet another problem that had cropped up in the complex code.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is <em>gratification</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is the uncomfortably full feeling you get after eating too much.
</li>
<li class='choice answer '>
<span class='result'></span>
It is the satisfied feeling that you get from doing something well.
</li>
<li class='choice '>
<span class='result'></span>
It is the feeling of relaxation you get when you are on vacation.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='gratification#' id='definition-sound' path='audio/wordmeanings/amy-gratification'></a>
When you have a feeling of <em>gratification</em>, you experience satisfaction or pleasure about a job well done.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>satisfaction</em>
</span>
</span>
</div>
<a class='quick-help' href='gratification#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='gratification#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/gratification/memory_hooks/6169.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Great</span></span> Satis<span class="emp3"><span>faction</span></span></span></span> <span class="emp1"><span>Grat</span></span>i<span class="emp3"><span>fication</span></span> is <span class="emp1"><span>great</span></span> satis<span class="emp3"><span>faction</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="gratification#" id="add-public-hook" style="" url="https://membean.com/mywords/gratification/memory_hooks">Use other public hook</a>
<a href="gratification#" id="memhook-use-own" url="https://membean.com/mywords/gratification/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='gratification#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
"The values of human solidarity that once drove our quest for a humane society seem to have been replaced, or are being threatened, by a crass materialism and pursuit of social goals of instant <b>gratification</b>."
<span class='attribution'>&mdash; Nelson Mandela (President of South Africa 1994-1999, anti-apartheid revolutionary, and philanthropist)</span>
<img alt="Nelson mandela (president of south africa 1994-1999, anti-apartheid revolutionary, and philanthropist)" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Nelson Mandela (President of South Africa 1994-1999, anti-apartheid revolutionary, and philanthropist).jpg?qdep8" width="80" />
</li>
<li>
Fermenting the ingredients over several days gives the dish its distinctive tang, but this easy version, which takes advantage of the funky depth of Asian fish sauce, offers relatively instant <b>gratification</b>.
<cite class='attribution'>
&mdash;
Epicurious
</cite>
</li>
<li>
When you're 22, on the cusp of greatness and playing for a team on the upswing, delayed <b>gratification</b> is a burden that’s hard to bear.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
It is the nature of desire not to be satisfied, and most men live only for the <b>gratification</b> of it.
<cite class='attribution'>
&mdash;
Aristotle
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/gratification/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='gratification#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='grat_pleasing' data-tree-url='//cdn1.membean.com/public/data/treexml' href='gratification#'>
<span class=''></span>
grat
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>pleasing, welcome</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='i_connective' data-tree-url='//cdn1.membean.com/public/data/treexml' href='gratification#'>
<span class=''></span>
-i-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>connective</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='fic_make' data-tree-url='//cdn1.membean.com/public/data/treexml' href='gratification#'>
<span class='common'></span>
fic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make, do</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ation_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='gratification#'>
<span class=''></span>
-ation
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state or quality of</td>
</tr>
</table>
<p><em>Gratification</em> is the &#8220;state of having been made pleased&#8221; or the &#8220;state of making (another) welcome or pleased.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='gratification#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Star Trek Voyager</strong><span> Seven of Nine finds gratification in helping reconstruct a civilization instead of destroying one.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/gratification.jpg' video_url='examplevids/gratification' video_width='350'></span>
<div id='wt-container'>
<img alt="Gratification" height="288" src="https://cdn1.membean.com/video/examplevids/gratification.jpg" width="350" />
<div class='center'>
<a href="gratification#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='gratification#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Gratification" src="https://cdn3.membean.com/public/images/wordimages/cons2/gratification.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='gratification#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>ecstatic</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>exuberance</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>exult</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>fruition</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>revelry</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>sanguine</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>vindicate</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>crestfallen</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>despondent</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>disconsolate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>disillusion</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>dolorous</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>inconsolable</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>lachrymose</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>lugubrious</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>melancholy</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>morose</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>pensive</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>saturnine</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>woebegone</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='gratification#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
gratify
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>please, satisfy</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="gratification" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>gratification</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="gratification#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

