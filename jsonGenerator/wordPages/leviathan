
<!DOCTYPE html>
<html>
<head>
<title>Word: leviathan | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Leviathan-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/leviathan-large.jpg?qdep8" />
</div>
<a href="leviathan#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>If you abridge a book or play, you make it shorter by making cuts to the original.</p>
<p class='rw-defn idx1' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx2' style='display:none'>Attrition is the process of gradually decreasing the strength of something (such as an enemy force) by continually attacking it.</p>
<p class='rw-defn idx3' style='display:none'>To augment something is to increase its value or effectiveness by adding something to it.</p>
<p class='rw-defn idx4' style='display:none'>Something colossal is extremely big, gigantic, or huge.</p>
<p class='rw-defn idx5' style='display:none'>If something daunts you, it makes you worried about dealing with it because you think it will be very difficult or dangerous.</p>
<p class='rw-defn idx6' style='display:none'>Debility is a state of being physically or mentally weak, usually due to illness.</p>
<p class='rw-defn idx7' style='display:none'>If you describe someone, usually a young woman, as demure, you mean that she is quiet, shy, and always behaves modestly.</p>
<p class='rw-defn idx8' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx9' style='display:none'>If something is formidable, it is impressive in size, power, or skill; therefore, it can make you feel frightened, alarmed, or in awe because it is so powerful or difficult to deal with.</p>
<p class='rw-defn idx10' style='display:none'>Something gargantuan is extremely large.</p>
<p class='rw-defn idx11' style='display:none'>Something that is imperceptible is either impossible to be perceived by the mind or is very difficult to perceive.</p>
<p class='rw-defn idx12' style='display:none'>Something that is inconspicuous does not attract attention and is not easily seen or noticed because it is small or ordinary.</p>
<p class='rw-defn idx13' style='display:none'>An indomitable foe cannot be beaten.</p>
<p class='rw-defn idx14' style='display:none'>The adjective inexorable describes a process that is impossible to stop once it has begun.</p>
<p class='rw-defn idx15' style='display:none'>Something infinitesimal is so extremely small or minute that it is very hard to measure it.</p>
<p class='rw-defn idx16' style='display:none'>An irrevocable action or decision is impossible to change, reverse, or stop.</p>
<p class='rw-defn idx17' style='display:none'>A juggernaut is a very powerful force, organization, or group whose influence cannot be stopped; its strength and power can overwhelm or crush any competition or anything else that stands in its way.</p>
<p class='rw-defn idx18' style='display:none'>If you describe a situation or process as labyrinthine, you mean that it is very complicated, involved, and difficult to understand.</p>
<p class='rw-defn idx19' style='display:none'>A macrocosm is a large, complex, and organized system or structure that is made of many small parts that form one unit, such as a society or the universe.</p>
<p class='rw-defn idx20' style='display:none'>A microcosm is a small group, place, or activity that has all the same qualities as a much larger one; therefore, it seems like a smaller version of it.</p>
<p class='rw-defn idx21' style='display:none'>Something minuscule is extremely small in size or amount.</p>
<p class='rw-defn idx22' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx23' style='display:none'>An omnipotent being or entity is unlimited in its power.</p>
<p class='rw-defn idx24' style='display:none'>A task or responsibility is onerous if you dislike having to do it because it is difficult or tiring.</p>
<p class='rw-defn idx25' style='display:none'>Something that is prodigious is very large, impressive, or great.</p>
<p class='rw-defn idx26' style='display:none'>When someone exhibits profundity, they display great intellectual depth and understanding; profundity can also be the depth or complexity of something.</p>
<p class='rw-defn idx27' style='display:none'>If you describe someone as redoubtable, you have great respect for their power and strength; you may be afraid of them as well.</p>
<p class='rw-defn idx28' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx29' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>
<p class='rw-defn idx30' style='display:none'>Something that is voluminous is long, large, or vast in size.</p>
<p class='rw-defn idx31' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>leviathan</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='leviathan#' id='pronounce-sound' path='audio/words/amy-leviathan'></a>
li-VAHY-uh-thuhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='leviathan#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='leviathan#' id='context-sound' path='audio/wordcontexts/brian-leviathan'></a>
The <em>leviathan</em> or overwhelming giant of hidden crime touched every aspect of life in 1930&#8217;s Chicago.  Crime bosses desired power and thus created an enormous system or <em>leviathan</em> of business dealings and political connections.  The monstrous, underground black market of crime and weaponry was a <em>leviathan</em> of epic proportions that shaped the dangerous world of Al Capone and other famous outlaws.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>leviathan</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Something that is huge and tremendously strong.
</li>
<li class='choice '>
<span class='result'></span>
Something that is unlawful and dangerous.
</li>
<li class='choice '>
<span class='result'></span>
Something that controls and influences everyday life.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='leviathan#' id='definition-sound' path='audio/wordmeanings/amy-leviathan'></a>
A <em>leviathan</em> is something that is very large, powerful, difficult to control, and rather frightening.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>giant</em>
</span>
</span>
</div>
<a class='quick-help' href='leviathan#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='leviathan#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/leviathan/memory_hooks/5678.json'></span>
<p>
<span class="emp0"><span>Nothing Bigger <span class="emp1"><span>Than</span></span> <span class="emp3"><span>Levi</span></span>'s</span></span> I would rather wear <span class="emp3"><span>Levi</span></span>'s <span class="emp1"><span>than</span></span> any other jeans; it's not because <span class="emp3"><span>Levi</span></span>'s is a <span class="emp3"><span>levi</span></span>a<span class="emp1"><span>than</span></span> amongst clothes companies--it's just that I like the jeans that <span class="emp3"><span>Levi</span></span> Strauss makes more <span class="emp1"><span>than</span></span> any other jean.
</p>
</div>

<div id='memhook-button-bar'>
<a href="leviathan#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/leviathan/memory_hooks">Use other hook</a>
<a href="leviathan#" id="memhook-use-own" url="https://membean.com/mywords/leviathan/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='leviathan#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
That’s because the British had a force of 32,000 men against his 12,000. If Washington had not changed his thinking, the American Revolution almost surely would have failed because the Continental Army was no match for the British <b>leviathan</b>.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
These <b>leviathans</b> who live and migrate in waters along the East Coast of North America teeter closer to the brink of extinction than perhaps any other whale species.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
Two-second ads have been popping up anew—briefly, of course—on stations owned by Clear Channel Communications, the radio <b>leviathan</b> that developed the concept and began selling it to marketers last year.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
For Orpheus' lute was strung with poets' sinews,/ Whose golden touch could soften steel and stones,/ Make tigers tame, and huge <b>leviathans</b>/ Forsake unsounded deeps to dance on sands.
<cite class='attribution'>
&mdash;
William Shakespeare, from "The Two Gentlemen of Verona"
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/leviathan/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='leviathan#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>The word <em>leviathan</em> originally referred to a huge and monstrous sea creature mentioned in the Bible.</p>
<a href="leviathan#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> Hebrew</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='leviathan#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Godzilla</strong><span> The original Godzilla, a menacing leviathan.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/leviathan.jpg' video_url='examplevids/leviathan' video_width='350'></span>
<div id='wt-container'>
<img alt="Leviathan" height="288" src="https://cdn1.membean.com/video/examplevids/leviathan.jpg" width="350" />
<div class='center'>
<a href="leviathan#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='leviathan#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Leviathan" src="https://cdn1.membean.com/public/images/wordimages/cons2/leviathan.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='leviathan#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>augment</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>colossal</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>daunt</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>formidable</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>gargantuan</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>indomitable</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>inexorable</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>irrevocable</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>juggernaut</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>labyrinthine</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>macrocosm</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>omnipotent</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>onerous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>prodigious</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>profundity</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>redoubtable</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>voluminous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abridge</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>attrition</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>debility</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>demure</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>imperceptible</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>inconspicuous</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>infinitesimal</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>microcosm</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>minuscule</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="leviathan" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>leviathan</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="leviathan#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

