
<!DOCTYPE html>
<html>
<head>
<title>Word: atrophy | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Atrophy-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/atrophy-large.jpg?qdep8" />
</div>
<a href="atrophy#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>An affliction is something that causes pain and mental suffering, especially a medical condition.</p>
<p class='rw-defn idx1' style='display:none'>Affluence is the state of having a lot of money and many possessions, granting one a high economic standard of living.</p>
<p class='rw-defn idx2' style='display:none'>Aliment is something, usually food, that feeds, nourishes, or supports someone or something else.</p>
<p class='rw-defn idx3' style='display:none'>An anodyne is a medicine that soothes or relieves pain.</p>
<p class='rw-defn idx4' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx5' style='display:none'>Attrition is the process of gradually decreasing the strength of something (such as an enemy force) by continually attacking it.</p>
<p class='rw-defn idx6' style='display:none'>Something is a blight if it spoils or damages things severely; blight is often used to describe something that ruins or kills crops.</p>
<p class='rw-defn idx7' style='display:none'>A comestible is something that can be eaten.</p>
<p class='rw-defn idx8' style='display:none'>A copious amount of something is a very large amount of it.</p>
<p class='rw-defn idx9' style='display:none'>Debility is a state of being physically or mentally weak, usually due to illness.</p>
<p class='rw-defn idx10' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx11' style='display:none'>A person or animal that is emaciated is extremely thin because of a serious illness or lack of food.</p>
<p class='rw-defn idx12' style='display:none'>If you show exuberance, you display great excitement, energy, and enthusiasm.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx14' style='display:none'>Something luxuriant, such as plants or hair, is growing well and is very healthy.</p>
<p class='rw-defn idx15' style='display:none'>A malady is a serious illness; it can also be used to refer to serious and widespread problems within a society or an organization.</p>
<p class='rw-defn idx16' style='display:none'>Malaise is a feeling of discontent, general unease, or even illness in a person or group for which there does not seem to be a quick and easy solution because the cause of it is not clear.</p>
<p class='rw-defn idx17' style='display:none'>If something is obsolescent, it is slowly becoming no longer needed because something newer or more effective has been invented.</p>
<p class='rw-defn idx18' style='display:none'>Something that is opulent is very impressive, grand, and is made from expensive materials.</p>
<p class='rw-defn idx19' style='display:none'>A profusion of something is a very large quantity or variety of it.</p>
<p class='rw-defn idx20' style='display:none'>If something bad, such as crime or disease, is rampant, there is a lot of it—and it is increasing in a way that is difficult to control.</p>
<p class='rw-defn idx21' style='display:none'>If you say that something bad or unpleasant is rife somewhere, you mean that it is very common.</p>
<p class='rw-defn idx22' style='display:none'>A salubrious place or area is pleasant, clean, healthy, and comfortable to live in.</p>
<p class='rw-defn idx23' style='display:none'>A salutary experience is beneficial to you since it strengthens you in some way, although it may be unpleasant as you undergo it; this word also refers to promoting good health.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>atrophy</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='atrophy#' id='pronounce-sound' path='audio/words/amy-atrophy'></a>
A-truh-fee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='atrophy#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='atrophy#' id='context-sound' path='audio/wordcontexts/brian-atrophy'></a>
After his motorcycle accident, Ross noticed the weakening effect that <em>atrophy</em> was having upon his leg muscles, which were no longer able to support him.  The doctor told him that six months of lying inactive in bed had <em><em>atrophied</em></em> and weakened his leg muscles.  After several trying weeks of physical therapy, Ross had regained some of his strength but had completely stopped his muscles from <em><em>atrophying</em></em> or wasting away any further.  In a few months, however, he was stronger than ever, and the force of <em><em>atrophy</em></em> or decay had ceased to be a factor in body, mind, and spirit.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What happens to a muscle that is undergoing <em>atrophy</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It remains the same size but develops the ability to endure for longer periods of time.
</li>
<li class='choice answer '>
<span class='result'></span>
It gets smaller due to malnourishment.
</li>
<li class='choice '>
<span class='result'></span>
It gets stronger and bigger due to rigorous exercise.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='atrophy#' id='definition-sound' path='audio/wordmeanings/amy-atrophy'></a>
<em>Atrophy</em> is a process by which parts of the body, such as muscles and organs, lessen in size or weaken in strength due to internal nerve damage, poor nutrition, or aging.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>wasting away</em>
</span>
</span>
</div>
<a class='quick-help' href='atrophy#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='atrophy#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/atrophy/memory_hooks/65898.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Trophy</span></span> for Lazy Man</span></span>  So, what did the laziest man in the world get for being the laziest man?  A<span class="emp3"><span>trophy</span></span>!
</p>
</div>

<div id='memhook-button-bar'>
<a href="atrophy#" id="add-public-hook" style="" url="https://membean.com/mywords/atrophy/memory_hooks">Use other public hook</a>
<a href="atrophy#" id="memhook-use-own" url="https://membean.com/mywords/atrophy/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='atrophy#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
A sojourn in space is a great way to ruin one’s physique—the microgravity results in dramatic loss of muscle mass. Residents of the International Space Station exercise regularly to stave off the <b>atrophy</b>, but perhaps there’s another way. Scientists have now found that if they treat spacefaring mice with a particular molecule, the animals not only maintain their muscles, they even bulk up a bit.
<cite class='attribution'>
&mdash;
Scientific American
</cite>
</li>
<li>
SMA stands for spinal muscular <b>atrophy</b>, and I was born with it. I've never walked or stood, and over time have lost the use of my hands. It's a progressive neuromuscular weakness.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
If you struggle with balance issues or have achy, arthritic joints that make it hard to stand for prolonged periods of time, you will want to find alternative ways to work out safely. This is especially important because being sedentary accelerates muscle <b>atrophy</b> and joint deterioration.
<cite class='attribution'>
&mdash;
The Philadelphia Inquirer
</cite>
</li>
<li>
The scientists studied 998 women ages 73 to 87 and free of dementia, periodically giving them tests of learning and memory. They used magnetic resonance imaging to detect brain <b>atrophy</b>, or wasting, and then scored the deterioration on its degree of similarity to the brain <b>atrophy</b> characteristic of Alzheimer’s disease.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/atrophy/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='atrophy#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='a_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='atrophy#'>
<span class=''></span>
a-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not, without</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='troph_nourishment' data-tree-url='//cdn1.membean.com/public/data/treexml' href='atrophy#'>
<span class=''></span>
troph
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>nourishment, food</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='y_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='atrophy#'>
<span class=''></span>
-y
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state or condition</td>
</tr>
</table>
<p>When one&#8217;s muscles are afflicted with <em>atrophy</em>, they are &#8220;without food&#8221; or do &#8220;not&#8221; have &#8220;nourishment&#8221; and thus weaken and waste away.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='atrophy#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Atrophy" src="https://cdn2.membean.com/public/images/wordimages/cons2/atrophy.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='atrophy#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>affliction</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>attrition</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>blight</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>debility</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>emaciated</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>malady</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>malaise</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>obsolescent</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>affluence</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>aliment</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>anodyne</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>comestible</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>copious</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>exuberance</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>luxuriant</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>opulent</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>profusion</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>rampant</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>rife</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>salubrious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>salutary</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='atrophy#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
atrophy
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to deteriorate or waste away</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="atrophy" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>atrophy</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="atrophy#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

