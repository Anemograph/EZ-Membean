
<!DOCTYPE html>
<html>
<head>
<title>Word: plastic | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you acclimate yourself to a new situation or environment, you adapt and get used to it.</p>
<p class='rw-defn idx1' style='display:none'>If you acquiesce to something, you allow it to happen by doing what someone wants without putting up a struggle or voicing your own concerns.</p>
<p class='rw-defn idx2' style='display:none'>If you are amenable to doing something, you willingly accept it without arguing.</p>
<p class='rw-defn idx3' style='display:none'>Someone cedes land or power to someone else by giving it to them, often because of political or military pressure.</p>
<p class='rw-defn idx4' style='display:none'>A situation or thing that is discordant does not fit in with other things; therefore, it is disagreeable, strange, or unpleasant.</p>
<p class='rw-defn idx5' style='display:none'>An animal or person that is docile is not aggressive; rather, they are well-behaved, quiet, and easy to control.</p>
<p class='rw-defn idx6' style='display:none'>Someone is being dogmatic when they express opinions in an assertive and often overly self-important manner.</p>
<p class='rw-defn idx7' style='display:none'>If you criticize someone&#8217;s arguments as being facile, you consider their ideas simplistic and not well thought through.</p>
<p class='rw-defn idx8' style='display:none'>Something formative shapes or influences the growth of something else.</p>
<p class='rw-defn idx9' style='display:none'>A soft material that becomes hardened into stone over time has become fossilized.</p>
<p class='rw-defn idx10' style='display:none'>If someone is fractious, they are easily upset or annoyed over unimportant things.</p>
<p class='rw-defn idx11' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is incorrigible has bad habits or does bad things and is unlikely to ever change; this word is often used in a humorous way.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is malleable is easily influenced or controlled by other people.</p>
<p class='rw-defn idx14' style='display:none'>Something meretricious seems good and useful; in fact, it’s just showy and does not have much value at all.</p>
<p class='rw-defn idx15' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx16' style='display:none'>Someone who is obstreperous is noisy, unruly, and difficult to control.</p>
<p class='rw-defn idx17' style='display:none'>Someone has a parochial outlook when they are mostly concerned with narrow or limited issues that affect a small, local area or a very specific cause; they are also unconcerned with wider or more general issues.</p>
<p class='rw-defn idx18' style='display:none'>Something or someone that is protean is adaptable, variable, or changeable in nature.</p>
<p class='rw-defn idx19' style='display:none'>A recalcitrant animal or person is difficult to control and refuses to obey orders—even after stiff punishment.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>plastic</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='plastic#' id='pronounce-sound' path='audio/words/amy-plastic'></a>
PLAS-tik
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='plastic#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='plastic#' id='context-sound' path='audio/wordcontexts/brian-plastic'></a>
The director never found much success because her critics claimed she was too <em>plastic</em>: her movies were too much like everyone else&#8217;s and she was too eager to please.  Sheila found it difficult to make any headway in the business, and disliked the <em>plastic</em> nature of the producers she encountered: they all seemed so fake and easily influenced by whatever made money.  Sheila had never realized in film school that cutting through the <em>plastic</em>, false atmosphere of Hollywood would be so difficult.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How might a friend who is <em>plastic</em> act?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They might cover up their insecurity by staying busy all the time.
</li>
<li class='choice '>
<span class='result'></span>
They might pretend to be your friend to get something they want.
</li>
<li class='choice answer '>
<span class='result'></span>
They might wear the same clothes and like the same things as you.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='plastic#' id='definition-sound' path='audio/wordmeanings/amy-plastic'></a>
A person who is <em>plastic</em> can be easily influenced and molded by others.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>formable</em>
</span>
</span>
</div>
<a class='quick-help' href='plastic#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='plastic#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/plastic/memory_hooks/5920.json'></span>
<p>
<span class="emp0"><span>E<span class="emp3"><span>lastic</span></span> Like P<span class="emp3"><span>lastic</span></span></span></span> Just like the e<span class="emp3"><span>lastic</span></span> band on my underwear can mold itself to various sizes, so too does the p<span class="emp3"><span>lastic</span></span> nature of my personality mold itself into just about any shape or person it needs to be at a given moment.
</p>
</div>

<div id='memhook-button-bar'>
<a href="plastic#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/plastic/memory_hooks">Use other hook</a>
<a href="plastic#" id="memhook-use-own" url="https://membean.com/mywords/plastic/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='plastic#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
We know that the human brain is highly <b>plastic</b>; neurons and synapses change as circumstances change.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
Dr. Shors says that her work has shown that the female brain, at least, is very <b>plastic</b>, changing dramatically during life in response to pregnancy and menopause as well as puberty.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Greatness gives you power, and power allows you to control your world, and if your world keeps expanding beyond the wealthy little fiefdom of professional golf and into the mad <b>plastic</b> nation of pure celebrity, so be it.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/plastic/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='plastic#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='plas_form' data-tree-url='//cdn1.membean.com/public/data/treexml' href='plastic#'>
<span class=''></span>
plas
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>form, mold</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ic_like' data-tree-url='//cdn1.membean.com/public/data/treexml' href='plastic#'>
<span class=''></span>
-ic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>like</td>
</tr>
</table>
<p>Just like <em>plastic</em> can be easily &#8220;molded or shaped&#8221; into just about any &#8220;form,&#8221; so too can a &#8220;plastic&#8221; person be either easily &#8220;molded&#8221; or influenced by others, or can change to fit any given circumstance.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='plastic#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Plastic" src="https://cdn0.membean.com/public/images/wordimages/cons2/plastic.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='plastic#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>acclimate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>acquiesce</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>amenable</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>cede</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>docile</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>facile</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>formative</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>malleable</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>meretricious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>protean</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='4' class = 'rw-wordform notlearned'><span>discordant</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>dogmatic</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>fossilized</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>fractious</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>incorrigible</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>obstreperous</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>parochial</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>recalcitrant</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="plastic" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>plastic</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="plastic#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

