
<!DOCTYPE html>
<html>
<head>
<title>Word: boon | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If your behavior or manner is altruistic, you show you care more about other people and their interests than you care about yourself.</p>
<p class='rw-defn idx1' style='display:none'>When you apportion something, such as blame or money, you decide how it should be shared among various people.</p>
<p class='rw-defn idx2' style='display:none'>If you describe a decision, rule, or plan as arbitrary, you think that it was decided without any thought, standard, or system to guide it; therefore, it can seem unfair.</p>
<p class='rw-defn idx3' style='display:none'>The adjective auspicious describes a positive beginning of something, such as a new business, or a certain time that looks to have a good chance of success or prosperity.</p>
<p class='rw-defn idx4' style='display:none'>A benefaction is a charitable contribution of money or assistance that someone gives to a person or organization.</p>
<p class='rw-defn idx5' style='display:none'>Someone who is benevolent wishes others well, often by being kind, filled with goodwill, and charitable towards them.</p>
<p class='rw-defn idx6' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx7' style='display:none'>When you bequeath something, you hand it down to someone in a will or pass it on from one generation to the next.</p>
<p class='rw-defn idx8' style='display:none'>When something is bestowed upon you—usually something valuable—you are given or presented it.</p>
<p class='rw-defn idx9' style='display:none'>Something is a blight if it spoils or damages things severely; blight is often used to describe something that ruins or kills crops.</p>
<p class='rw-defn idx10' style='display:none'>A cataclysm is a violent, sudden event that causes great change and/or harm.</p>
<p class='rw-defn idx11' style='display:none'>Consternation is the feeling of anxiety or fear, sometimes paralyzing in its effect, and often caused by something unexpected that has happened.</p>
<p class='rw-defn idx12' style='display:none'>A debacle is something, such as an event or attempt, that fails completely in an embarrassing way.</p>
<p class='rw-defn idx13' style='display:none'>Something that is deleterious has a harmful effect.</p>
<p class='rw-defn idx14' style='display:none'>A deluge is a sudden, heavy downfall of rain; it can also be a large number of things, such as papers or e-mails, that someone gets all at the same time, making them very difficult to handle.</p>
<p class='rw-defn idx15' style='display:none'>Depredation can be the act of destroying or robbing a village—or the overall damage that time can do to things held dear.</p>
<p class='rw-defn idx16' style='display:none'>Deprivation is a state during which people lack something, especially adequate food and shelter; deprivation can also describe something being taken away from someone.</p>
<p class='rw-defn idx17' style='display:none'>If something discomfits you, it makes you feel embarrassed, confused, uncomfortable, or frustrated.</p>
<p class='rw-defn idx18' style='display:none'>If something disconcerts you, it makes you feel anxious, worried, or confused.</p>
<p class='rw-defn idx19' style='display:none'>When someone feels disquiet about a situation, they feel very worried or nervous.</p>
<p class='rw-defn idx20' style='display:none'>If you exult, you show great pleasure and excitement, especially about something you have achieved.</p>
<p class='rw-defn idx21' style='display:none'>If you are experiencing felicity about something, you are very happy about it.</p>
<p class='rw-defn idx22' style='display:none'>If an event becomes a fiasco, it is a complete and embarrassing failure.</p>
<p class='rw-defn idx23' style='display:none'>Something fortuitous happens purely by chance and produces a successful or pleasant result.</p>
<p class='rw-defn idx24' style='display:none'>Someone who is impecunious has very little money, especially over a long period of time.</p>
<p class='rw-defn idx25' style='display:none'>An imposition is giving someone an additional duty or extra work that is not welcomed by that person.</p>
<p class='rw-defn idx26' style='display:none'>When something happens at an inopportune time it is inconvenient or not suitable.</p>
<p class='rw-defn idx27' style='display:none'>A lamentable state of affairs is miserable and just plain awful.</p>
<p class='rw-defn idx28' style='display:none'>Largess is the generous act of giving money or presents to a large number of people.</p>
<p class='rw-defn idx29' style='display:none'>If someone exhibits magnanimity towards another, they show them kindness and noble generosity, especially after defeating them in battle or after having been treated badly by them.</p>
<p class='rw-defn idx30' style='display:none'>A malevolent person or thing is evil due to deliberate attempts to cause harm.</p>
<p class='rw-defn idx31' style='display:none'>Malfeasance is an unlawful act, especially one committed by a trusted public official.</p>
<p class='rw-defn idx32' style='display:none'>A mendicant is a beggar who asks for money by day on the streets.</p>
<p class='rw-defn idx33' style='display:none'>A munificent person is extremely generous, especially with money.</p>
<p class='rw-defn idx34' style='display:none'>Pandemonium is a very noisy and uncontrolled situation, especially one that is caused by a lot of angry or excited people.</p>
<p class='rw-defn idx35' style='display:none'>Penury is the state of being extremely poor.</p>
<p class='rw-defn idx36' style='display:none'>Something that is pernicious is very harmful or evil, often in a way that is hidden or not quickly noticed.</p>
<p class='rw-defn idx37' style='display:none'>Philanthropy is unselfish support in the form of donating money, work, or gifts to positive social purposes; philanthropy is also overall love for humans in general.</p>
<p class='rw-defn idx38' style='display:none'>If soldiers pillage a place, such as a town or museum, they steal a lot of things and damage it using violent methods.</p>
<p class='rw-defn idx39' style='display:none'>If you are in a plight, you are in trouble of some kind or in a state of unfortunate circumstances.</p>
<p class='rw-defn idx40' style='display:none'>When you plunder, you forcefully take or rob others of their possessions, usually during times of war, natural disaster, or other unrest.</p>
<p class='rw-defn idx41' style='display:none'>When you proffer something to someone, you offer it up or hold it out to them to take.</p>
<p class='rw-defn idx42' style='display:none'>Something that is propitious shows favorable conditions.</p>
<p class='rw-defn idx43' style='display:none'>A providential event is a very lucky one because it happens at exactly the right time and often when it is needed most.</p>
<p class='rw-defn idx44' style='display:none'>To ransack a place is to thoroughly loot or rob items from it; it can also mean to look through something thoroughly by examining every bit of it.</p>
<p class='rw-defn idx45' style='display:none'>When you ravage something, you completely destroy, wreck, or damage it.</p>
<p class='rw-defn idx46' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx47' style='display:none'>If you are sanguine about a situation, especially a difficult one, you are confident and cheerful that everything will work out the way you want it to.</p>
<p class='rw-defn idx48' style='display:none'>A scourge was originally a whip used for torture and now refers to something that torments or causes serious trouble or devastation.</p>
<p class='rw-defn idx49' style='display:none'>Serendipity is the good fortune that some people encounter in finding or making interesting and valuable discoveries purely by luck.</p>
<p class='rw-defn idx50' style='display:none'>Severe problems, suffering, or difficulties experienced in a particular situation are all examples of tribulations.</p>
<p class='rw-defn idx51' style='display:none'>Vicissitudes can be unexpected or simply normal changes and variations that happen during the course of people&#8217;s lives.</p>
<p class='rw-defn idx52' style='display:none'>A windfall is the sudden appearance of a large amount of money that is completely unexpected.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>boon</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='boon#' id='pronounce-sound' path='audio/words/amy-boon'></a>
boon
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='boon#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='boon#' id='context-sound' path='audio/wordcontexts/brian-boon'></a>
After a severe drought of several years, farmers were finally granted an unexpected <em>boon</em> or welcome blessing as gentle rains began to fall.  Within a few short weeks dried fields were filled with the great <em>boon</em> or benefit of lush pasture, which fed hungry animals sufficiently.  An even greater <em>boon</em> or bonus of a bumper crop of wheat was given to farmers as well that wonderful year, and soon the terrible years of drought became a distant memory.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How would you feel when receiving a <em>boon</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
You would feel saddened that you have to leave your job.
</li>
<li class='choice answer '>
<span class='result'></span>
You would feel grateful for your good fortune.
</li>
<li class='choice '>
<span class='result'></span>
You would feel anxious about the information it holds.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='boon#' id='definition-sound' path='audio/wordmeanings/amy-boon'></a>
When you are granted a <em>boon</em>, you are given a special gift or favor that is of great benefit to you.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>bonus</em>
</span>
</span>
</div>
<a class='quick-help' href='boon#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='boon#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/boon/memory_hooks/4887.json'></span>
<p>
<span class="emp0"><span>Ba<span class="emp1"><span>boon</span></span> <span class="emp1"><span>Boon</span></span></span></span> The zoo received a <span class="emp1"><span>boon</span></span> when a pregnant ba<span class="emp1"><span>boon</span></span> gave birth to triplets--a very welcome <span class="emp1"><span>boon</span></span> of baby ba<span class="emp1"><span>boon</span></span>s!
</p>
</div>

<div id='memhook-button-bar'>
<a href="boon#" id="add-public-hook" style="" url="https://membean.com/mywords/boon/memory_hooks">Use other public hook</a>
<a href="boon#" id="memhook-use-own" url="https://membean.com/mywords/boon/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='boon#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Exercise may help speed the healing of skin wounds in healthy older adults, even if they aren't lifelong exercisers, a new study shows. The finding could be a <b>boon</b> to older adults, since age tends to slow down wound healing. . . .
<cite class='attribution'>
&mdash;
WebMD
</cite>
</li>
<li>
While nearly every athlete in the country would get a chance to benefit from having the right to profit off their own name, many believe it would be an even bigger <b>boon</b> to the University of Texas than other places.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Disney+ is also not the <b>boon</b> investors are hoping for, he said.
<cite class='attribution'>
&mdash;
CNBC
</cite>
</li>
<li>
"Overall, the weight of the evidence is that it has been a major <b>boon</b> to the economy to have such a supply of labor at many levels," says Alejandro Portes of Princeton University.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/boon/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='boon#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>From a root word meaning &#8220;request&#8221;; the <em>boon</em> would be the answer to that &#8220;request.&#8221;</p>
<a href="boon#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> Old Norse</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='boon#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Boon" src="https://cdn3.membean.com/public/images/wordimages/cons2/boon.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='boon#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>altruistic</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>apportion</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>arbitrary</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>auspicious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>benefaction</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>benevolent</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>bequeath</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>bestow</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>exult</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>felicity</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>fortuitous</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>largess</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>magnanimity</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>munificent</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>philanthropy</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>proffer</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>propitious</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>providential</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>sanguine</span> &middot;</li><li data-idx='49' class = 'rw-wordform notlearned'><span>serendipity</span> &middot;</li><li data-idx='51' class = 'rw-wordform notlearned'><span>vicissitude</span> &middot;</li><li data-idx='52' class = 'rw-wordform notlearned'><span>windfall</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='9' class = 'rw-wordform notlearned'><span>blight</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>cataclysm</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>consternation</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>debacle</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>deleterious</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>deluge</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>depredation</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>deprivation</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>discomfit</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>disconcert</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>disquiet</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>fiasco</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>impecunious</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>imposition</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>inopportune</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>lamentable</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>malevolent</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>malfeasance</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>mendicant</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>pandemonium</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>penury</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>pernicious</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>pillage</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>plight</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>plunder</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>ransack</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>ravage</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='48' class = 'rw-wordform notlearned'><span>scourge</span> &middot;</li><li data-idx='50' class = 'rw-wordform notlearned'><span>tribulation</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='boon#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
boon
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>jolly, merry</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="boon" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>boon</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="boon#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

