
<!DOCTYPE html>
<html>
<head>
<title>Word: prefatory | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An antecedent of something, such as an event or organization, has happened or existed before it and can be similar to it.</p>
<p class='rw-defn idx1' style='display:none'>A codicil is a supplement, usually to a will, that is added after the main part has been written.</p>
<p class='rw-defn idx2' style='display:none'>A denouement is the end of a book, play, or series of events when everything is explained and comes to a conclusion.</p>
<p class='rw-defn idx3' style='display:none'>When something ensues, it happens after or as a result of another event.</p>
<p class='rw-defn idx4' style='display:none'>An inaugural event celebrates the beginning of something, such as the term of a president or the beginning of a series of meetings.</p>
<p class='rw-defn idx5' style='display:none'>To initiate something is to begin or start it.</p>
<p class='rw-defn idx6' style='display:none'>A preamble is an introduction to a formal document that explains the document&#8217;s purpose.</p>
<p class='rw-defn idx7' style='display:none'>A first event is a precursor to a second event if the first event is responsible for the development or existence of the second.</p>
<p class='rw-defn idx8' style='display:none'>A retrograde action causes a return to a condition or situation that is worse instead of better than the present one.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>prefatory</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='prefatory#' id='pronounce-sound' path='audio/words/amy-prefatory'></a>
PREF-uh-tohr-ee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='prefatory#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='prefatory#' id='context-sound' path='audio/wordcontexts/brian-prefatory'></a>
Carol told a funny story in her <em>prefatory</em> remarks about the speaker she was introducing.  The speaker was her old friend Pete who was prepared to explain anything Carol might say in her preliminary, <em>prefatory</em> talk.  When it was Pete&#8217;s turn to get up, he offered <em>prefatory</em> or opening remarks by thanking Carol, and by saying that all the positive things she had said about him were definitely true.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What are <em>prefatory</em> comments?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Humorous anecdotes used to defuse an escalating argument.
</li>
<li class='choice answer '>
<span class='result'></span>
Beginning words that serve as an opening to a public address.
</li>
<li class='choice '>
<span class='result'></span>
Kind remarks about a speaker that one has known a long time.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='prefatory#' id='definition-sound' path='audio/wordmeanings/amy-prefatory'></a>
<em>Prefatory</em> comments refer to an introduction to a book or speech.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>preliminary</em>
</span>
</span>
</div>
<a class='quick-help' href='prefatory#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='prefatory#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/prefatory/memory_hooks/4442.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Pre</span></span>p a <span class="emp2"><span>Fat</span></span>e St<span class="emp3"><span>ory</span></span></span></span> How does one <span class="emp1"><span>pre</span></span>p a <span class="emp2"><span>fat</span></span>e st<span class="emp3"><span>ory</span></span>?  <span class="emp2"><span>Fat</span></span>e st<span class="emp3"><span>ory</span></span> <span class="emp1"><span>pre</span></span><span class="emp2"><span>fat</span></span><span class="emp3"><span>ory</span></span> remarks must not turn one away from one's <span class="emp2"><span>fat</span></span>e, but rather <span class="emp1"><span>pre</span></span>p one to accept that one's <span class="emp2"><span>fat</span></span>e is simply a st<span class="emp3"><span>ory</span></span>, and nothing more.
</p>
</div>

<div id='memhook-button-bar'>
<a href="prefatory#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/prefatory/memory_hooks">Use other hook</a>
<a href="prefatory#" id="memhook-use-own" url="https://membean.com/mywords/prefatory/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='prefatory#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
In Studs Terkel's pioneering oral history of World War II, _''The Good War''_, the author placed quotation marks around the title. He explained in a <b>prefatory</b> note that he added them "not as a matter of caprice or editorial comment, but simply because the adjective 'good' mated to the noun 'war' is so incongruous."
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
On the evening after the first day's competition at Nyeri I sat with friends in the lounge at the Outspan Hotel, wondering whom I could coerce into writing a <b>prefatory</b> note to Keino.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
There are sixty of the poems, and they belong, as the <b>prefatory</b> note tells us, to different periods of the author’s life.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/prefatory/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='prefatory#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pre_before' data-tree-url='//cdn1.membean.com/public/data/treexml' href='prefatory#'>
<span class='common'></span>
pre-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>before, in front</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='fat_speak' data-tree-url='//cdn1.membean.com/public/data/treexml' href='prefatory#'>
<span class=''></span>
fat
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>speak, talk, say</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ory_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='prefatory#'>
<span class=''></span>
-ory
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p><em>Prefatory</em> remarks are those &#8220;relating to speaking, talking, or saying&#8221; something &#8220;before or in front of&#8221; a book or speech to serve as an opening or introduction.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='prefatory#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Prefatory" src="https://cdn1.membean.com/public/images/wordimages/cons2/prefatory.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='prefatory#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>antecedent</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>inaugural</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>initiate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>preamble</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>precursor</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>codicil</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>denouement</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>ensue</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>retrograde</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='prefatory#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
preface
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>an introduction to a book or speech</td>
</tr>
<tr>
<td class='wordform'>
<span>
preface
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to introduce a speech or a book</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="prefatory" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>prefatory</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="prefatory#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

