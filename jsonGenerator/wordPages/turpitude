
<!DOCTYPE html>
<html>
<head>
<title>Word: turpitude | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you act with abandon, you give in to the impulse of the moment and behave in a wild, uncontrolled way.</p>
<p class='rw-defn idx1' style='display:none'>The word abject emphasizes a very bad situation or quality, thereby making it even worse.</p>
<p class='rw-defn idx2' style='display:none'>If you abominate something, you hate it because you think it is extremely wrong and unacceptable.</p>
<p class='rw-defn idx3' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx4' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx5' style='display:none'>Camaraderie is a feeling of friendship and trust among a group of people who have usually known each over a long period of time.</p>
<p class='rw-defn idx6' style='display:none'>If you comport yourself in a particular way, you behave in that way.</p>
<p class='rw-defn idx7' style='display:none'>If you feel compunction about doing something, you feel that you should not do it because it is bad or wrong.</p>
<p class='rw-defn idx8' style='display:none'>A man who is dapper has a very neat and clean appearance; he is both fashionable and stylish.</p>
<p class='rw-defn idx9' style='display:none'>A man who is debonair wears fashionable clothes and is sophisticated, charming, friendly, and confident.</p>
<p class='rw-defn idx10' style='display:none'>A decadent person has low moral standards and is more interested in pleasure than serious matters.</p>
<p class='rw-defn idx11' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx12' style='display:none'>Depravity is behavior that is immoral, corrupt, or evil.</p>
<p class='rw-defn idx13' style='display:none'>If you say a person&#8217;s actions are despicable, you think they are extremely unpleasant or nasty.</p>
<p class='rw-defn idx14' style='display:none'>If you refer to the enormity of a situation, problem, or event, you mean that it is very evil or morally offensive; this word has nothing to do with physical size.</p>
<p class='rw-defn idx15' style='display:none'>An action that is flagrant shows that someone does not care if they obviously break the rules or highly offend people.</p>
<p class='rw-defn idx16' style='display:none'>If you describe something as heinous, you mean that is extremely evil, shocking, and bad.</p>
<p class='rw-defn idx17' style='display:none'>If you describe someone&#8217;s appearance or behavior as impeccable, you mean that it is perfect and therefore impossible to criticize.</p>
<p class='rw-defn idx18' style='display:none'>Something that has inestimable value or benefit has so much of it that it cannot be calculated.</p>
<p class='rw-defn idx19' style='display:none'>An infamous person has a very bad reputation because they have done disgraceful or dishonorable things.</p>
<p class='rw-defn idx20' style='display:none'>People who are ingenuous are excessively trusting and believe almost everything that people tell them, especially because they have not had much life experience.</p>
<p class='rw-defn idx21' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx22' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx23' style='display:none'>A malevolent person or thing is evil due to deliberate attempts to cause harm.</p>
<p class='rw-defn idx24' style='display:none'>Malfeasance is an unlawful act, especially one committed by a trusted public official.</p>
<p class='rw-defn idx25' style='display:none'>Malice is a type of hatred that causes a strong desire to harm others both physically and emotionally.</p>
<p class='rw-defn idx26' style='display:none'>If you malign someone, you say unpleasant things about them to damage their reputation.</p>
<p class='rw-defn idx27' style='display:none'>A nefarious activity is considered evil and highly dishonest.</p>
<p class='rw-defn idx28' style='display:none'>If you describe people or things as odious, you think that they are extremely unpleasant.</p>
<p class='rw-defn idx29' style='display:none'>If you perpetrate something, you commit a crime or do some other bad thing for which you are responsible.</p>
<p class='rw-defn idx30' style='display:none'>To be politic in a decision is to be socially wise and diplomatic.</p>
<p class='rw-defn idx31' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx32' style='display:none'>Someone who is profligate is lacking in restraint, which can manifest in carelessly and wastefully using resources, such as energy or money; this kind of person can also act in an immoral way.</p>
<p class='rw-defn idx33' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx34' style='display:none'>A reprobate has a bad character and behaves in an immoral or improper way.</p>
<p class='rw-defn idx35' style='display:none'>If something is reviled, it is intensely hated and criticized.</p>
<p class='rw-defn idx36' style='display:none'>A scrupulous person takes great care to do everything in an extremely honest, conscientious, and fair manner.</p>
<p class='rw-defn idx37' style='display:none'>Something statutory, such as the power given to a governor or president, is created, established, and controlled by rules and laws; hence, it has the full force of the law behind it and must be followed.</p>
<p class='rw-defn idx38' style='display:none'>If you say someone&#8217;s behavior is unseemly, you disapprove of it because it is not in good taste or not suitable for a particular situation.</p>
<p class='rw-defn idx39' style='display:none'>Something that is unsullied is unstained and clean.</p>
<p class='rw-defn idx40' style='display:none'>If you behave in an urbane way, you are behaving in a polite, refined, and civilized fashion in social situations.</p>
<p class='rw-defn idx41' style='display:none'>A vicious person is corrupt, violent, aggressive, and/or highly immoral.</p>
<p class='rw-defn idx42' style='display:none'>Something vile is evil, very unpleasant, horrible, or extremely bad.</p>
<p class='rw-defn idx43' style='display:none'>If someone vitiates something, they make it less effective, weaker, or reduced in quality.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>turpitude</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='turpitude#' id='pronounce-sound' path='audio/words/amy-turpitude'></a>
TUR-pi-tood
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='turpitude#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='turpitude#' id='context-sound' path='audio/wordcontexts/brian-turpitude'></a>
The villain&#8217;s motive for acting with such <em>turpitude</em> or evil cruelty remained mysterious.  More than one life had been lost as this wicked killer persisted in pursuing a path of corruption, vice, and <em>turpitude</em>, leaving a trail of victims in his wretched path.  The determined Detective Banks pursued the foul murderer and ultimately stopped him in the act of committing yet another immoral act of shameful <em>turpitude</em>.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Which person is demonstrating <em>turpitude</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Felice feels slow and lethargic.
</li>
<li class='choice '>
<span class='result'></span>
Thelma thinks hard but ineffectively.
</li>
<li class='choice answer '>
<span class='result'></span>
David does despicable deeds.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='turpitude#' id='definition-sound' path='audio/wordmeanings/amy-turpitude'></a>
<em>Turpitude</em> is the state of engaging in immoral or evil activities.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>wickedness</em>
</span>
</span>
</div>
<a class='quick-help' href='turpitude#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='turpitude#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/turpitude/memory_hooks/3878.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Torped</span></span>o Atti<span class="emp2"><span>tude</span></span></span></span> In the mind of a <span class="emp1"><span>torped</span></span>o, it's okay to act with <span class="emp1"><span>turpi</span></span><span class="emp2"><span>tude</span></span>:  darting around at high speeds, blowing things up, and waging war.
</p>
</div>

<div id='memhook-button-bar'>
<a href="turpitude#" id="add-public-hook" style="" url="https://membean.com/mywords/turpitude/memory_hooks">Use other public hook</a>
<a href="turpitude#" id="memhook-use-own" url="https://membean.com/mywords/turpitude/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='turpitude#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
These criminal offenses, which are often identified with physical harm or damage, fit the general description of <b>turpitude</b>.
<cite class='attribution'>
&mdash;
Suffolk News-Herald
</cite>
</li>
<li>
"When a member of the Bar is convicted of an offense involving moral <b>turpitude</b>, disbarment is mandatory," the District of Columbia Court of Appeals wrote in its opinion, which is posted on its Web site.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
The A.C.L.U. contends that the attorney general’s list violates the Alabama Constitution, saying only the Legislature can decide what crimes fit the "moral <b>turpitude</b>" category.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
The inspector general should be completely independent in order to conduct investigations of moral and ethical <b>turpitude</b>, without any bias or interference from political sides to increase transparency.
<cite class='attribution'>
&mdash;
Chicago Tribune
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/turpitude/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='turpitude#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>turp</td>
<td>
&rarr;
</td>
<td class='meaning'>shameless, base</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='itude_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='turpitude#'>
<span class=''></span>
-itude
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state or quality of</td>
</tr>
</table>
<p><em>Turpitude</em> is the &#8220;state or quality of&#8221; acting in a &#8220;shameless or base&#8221; manner.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='turpitude#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Turpitude" src="https://cdn2.membean.com/public/images/wordimages/cons2/turpitude.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='turpitude#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abandon</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abject</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abominate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>decadent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>depravity</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>despicable</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>enormity</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>flagrant</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>heinous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>infamous</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>malevolent</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>malfeasance</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>malice</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>malign</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>nefarious</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>odious</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>perpetrate</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>profligate</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>reprobate</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>revile</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>unseemly</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>vicious</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>vile</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>vitiate</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>camaraderie</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>comport</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>compunction</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>dapper</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>debonair</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>impeccable</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>inestimable</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>ingenuous</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>politic</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>scrupulous</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>statutory</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>unsullied</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>urbane</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="turpitude" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>turpitude</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="turpitude#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

