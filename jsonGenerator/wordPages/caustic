
<!DOCTYPE html>
<html>
<head>
<title>Word: caustic | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you take an acerbic tone with someone, you are criticizing them in a clever but critical and mean way.</p>
<p class='rw-defn idx1' style='display:none'>An acrid smell or taste is strong, unpleasant, and stings your nose or throat.</p>
<p class='rw-defn idx2' style='display:none'>An acrimonious meeting or discussion is bitter, resentful, and filled with angry contention.</p>
<p class='rw-defn idx3' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx4' style='display:none'>When you are astringent towards someone, you speak to or write about them in a critical and hurtful manner.</p>
<p class='rw-defn idx5' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx6' style='display:none'>Someone who has a bilious personality is highly irritable and bad-tempered.</p>
<p class='rw-defn idx7' style='display:none'>A captious person has a fondness for catching others at fault; hence, they are overly critical and raise unwarranted objections too often.</p>
<p class='rw-defn idx8' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx9' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx10' style='display:none'>A man who is debonair wears fashionable clothes and is sophisticated, charming, friendly, and confident.</p>
<p class='rw-defn idx11' style='display:none'>A dulcet sound is soft and pleasant in a gentle way.</p>
<p class='rw-defn idx12' style='display:none'>People who are ingenuous are excessively trusting and believe almost everything that people tell them, especially because they have not had much life experience.</p>
<p class='rw-defn idx13' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx14' style='display:none'>A mellifluous voice or piece of music is gentle and pleasant to listen to.</p>
<p class='rw-defn idx15' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx16' style='display:none'>If you mollify someone, you say or do something to make that person feel less angry or upset.</p>
<p class='rw-defn idx17' style='display:none'>To be politic in a decision is to be socially wise and diplomatic.</p>
<p class='rw-defn idx18' style='display:none'>Something pungent, such as a spice, aroma, or speech, is sharp and penetrating.</p>
<p class='rw-defn idx19' style='display:none'>Someone&#8217;s sardonic smile, expression, or comment shows a lack of respect for what someone else has said or done; this occurs because the former thinks they are too important to consider or discuss such a supposedly trivial matter of the latter.</p>
<p class='rw-defn idx20' style='display:none'>Trenchant comments or criticisms are expressed forcefully, directly, and clearly, even though they may be hurtful to the receiver.</p>
<p class='rw-defn idx21' style='display:none'>To come out of something unscathed is to come out of it uninjured.</p>
<p class='rw-defn idx22' style='display:none'>A virulent disease is very dangerous and spreads very quickly.</p>
<p class='rw-defn idx23' style='display:none'>Vitriolic words are bitter, unkind, and mean-spirited.</p>
<p class='rw-defn idx24' style='display:none'>Vituperative remarks are full of hate, anger, and cruel criticism.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>caustic</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='caustic#' id='pronounce-sound' path='audio/words/amy-caustic'></a>
KAW-stik
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='caustic#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='caustic#' id='context-sound' path='audio/wordcontexts/brian-caustic'></a>
At the dinner party, Margery made many sarcastic and <em>caustic</em> remarks about how she believes that vegetarians are foolish and wrong.  She even made <em>caustic</em> and stinging puns about &#8220;vegetables&#8221; and human judgment, all the while knowing that her host&#8217;s sister was a dedicated vegetarian.  Whether she meant to be serious or witty, Margery&#8217;s <em>caustic</em>, cutting comments ensured that she wouldn&#8217;t be invited back any time soon.  Margery&#8217;s biting, <em>caustic</em> humor has driven many of her friends away.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What would someone with a <em>caustic</em> sense of humor do?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Tell a joke that makes everyone in the room laugh.
</li>
<li class='choice answer '>
<span class='result'></span>
Tell a joke that is biting, offensive, and mocking.
</li>
<li class='choice '>
<span class='result'></span>
Tell a joke in the wrong place at the wrong time.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='caustic#' id='definition-sound' path='audio/wordmeanings/amy-caustic'></a>
A <em>caustic</em> remark is unkind, critical, cruel, and mocking.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>critical and cruel</em>
</span>
</span>
</div>
<a class='quick-help' href='caustic#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='caustic#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/caustic/memory_hooks/3047.json'></span>
<p>
<span class="emp0"><span>Be<span class="emp1"><span>caus</span></span>e It's Mean It <span class="emp2"><span>Tic</span></span>ks Me Off</span></span>  "Your constantly <span class="emp1"><span>caus</span></span><span class="emp2"><span>tic</span></span> remarks go way beyond just joking, Jeremy; it's really starting to <span class="emp2"><span>tic</span></span>k me off be<span class="emp1"><span>caus</span></span>e you're so severe and judgmental."
</p>
</div>

<div id='memhook-button-bar'>
<a href="caustic#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/caustic/memory_hooks">Use other hook</a>
<a href="caustic#" id="memhook-use-own" url="https://membean.com/mywords/caustic/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='caustic#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
If it's a woman, it's <b>caustic</b>; if it's a man, it's authoritative.
<span class='attribution'>&mdash; Barbara Walters, American journalist</span>
<img alt="Barbara walters, american journalist" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Barbara Walters, American journalist.jpg?qdep8" width="80" />
</li>
<li>
The rival exchange is the perennial target of the <b>caustic</b> e-mails that Mr. Cummings fires off each week . . . .
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Howard, which the _Village Voice_ called "the last angry duck," became a cult phenomenon, although not everyone fell under his <b>caustic</b> spell.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
_Crooked Rain’s_ clean production and insidiously catchy melodies hardly signify that Pavement [has] sold out—if anything, their vision is more warped and <b>caustic</b> than before.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/caustic/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='caustic#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='caust_burnt' data-tree-url='//cdn1.membean.com/public/data/treexml' href='caustic#'>
<span class=''></span>
caust
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>burnt, red hot</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ic_like' data-tree-url='//cdn1.membean.com/public/data/treexml' href='caustic#'>
<span class=''></span>
-ic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>nature of, like</td>
</tr>
</table>
<p><em>Caustic</em> remarks are &#8220;burning&#8221; or &#8220;red hot,&#8221; so they hurt those who are exposed to them.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='caustic#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>SpongeBob SquarePants</strong><span> Squidward's comments are pretty caustic towards SpongeBob.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/caustic.jpg' video_url='examplevids/caustic' video_width='350'></span>
<div id='wt-container'>
<img alt="Caustic" height="288" src="https://cdn1.membean.com/video/examplevids/caustic.jpg" width="350" />
<div class='center'>
<a href="caustic#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='caustic#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Caustic" src="https://cdn1.membean.com/public/images/wordimages/cons2/caustic.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='caustic#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>acerbic</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>acrid</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>acrimonious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>astringent</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>bilious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>captious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>pungent</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>sardonic</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>trenchant</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>virulent</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>vitriolic</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>vituperative</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>debonair</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>dulcet</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>ingenuous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>mellifluous</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>mollify</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>politic</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>unscathed</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="caustic" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>caustic</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="caustic#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

