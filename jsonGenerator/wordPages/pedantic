
<!DOCTYPE html>
<html>
<head>
<title>Word: pedantic | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An assiduous person works hard to ensure that something is done properly and completely.</p>
<p class='rw-defn idx1' style='display:none'>When you are astringent towards someone, you speak to or write about them in a critical and hurtful manner.</p>
<p class='rw-defn idx2' style='display:none'>Something austere is simple and plain in its style; an austere person is strict and severe with themselves.</p>
<p class='rw-defn idx3' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx4' style='display:none'>If you are ___, you are cautious; you think carefully about something before saying or doing it.</p>
<p class='rw-defn idx5' style='display:none'>A conjecture is a theory or guess that is based on information that is not certain or complete.</p>
<p class='rw-defn idx6' style='display:none'>Something convoluted, such as a difficult concept or procedure, is complex and takes many twists and turns.</p>
<p class='rw-defn idx7' style='display:none'>A cursory examination or reading of something is very quick, incomplete, and does not pay close attention to detail.</p>
<p class='rw-defn idx8' style='display:none'>If you deliberate, you think about or discuss something very carefully, especially before making an important decision.</p>
<p class='rw-defn idx9' style='display:none'>Didactic speech or writing is intended to teach something, especially a moral lesson.</p>
<p class='rw-defn idx10' style='display:none'>Entropy is the lack of organization or measure of disorder currently in a system.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is erudite is steeped in knowledge because they have read and studied extensively.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is exacting expects others to work very hard and carefully.</p>
<p class='rw-defn idx13' style='display:none'>Something that is extemporaneous, such as an action, speech, or performance, is done without any preparation or practice beforehand.</p>
<p class='rw-defn idx14' style='display:none'>Someone who is fastidious cares excessively about small details and wants to keep everything correct, tidy, very clean, and in perfect order.</p>
<p class='rw-defn idx15' style='display:none'>Something that is hypothetical is based on possible situations or events rather than actual ones.</p>
<p class='rw-defn idx16' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx17' style='display:none'>Someone is considered meticulous when they act with careful attention to detail.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx19' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx20' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx21' style='display:none'>If you peruse some written text, you read it over carefully.</p>
<p class='rw-defn idx22' style='display:none'>To be punctilious is to pay precise attention to detail.</p>
<p class='rw-defn idx23' style='display:none'>A scrupulous person takes great care to do everything in an extremely honest, conscientious, and fair manner.</p>
<p class='rw-defn idx24' style='display:none'>Someone who is sedulous works hard and performs tasks very carefully and thoroughly, not stopping their work until it is completely accomplished.</p>
<p class='rw-defn idx25' style='display:none'>To act in an uncouth manner is to be awkward and unmannerly.</p>
<p class='rw-defn idx26' style='display:none'>An unkempt person or thing is untidy and has not been kept neat.</p>
<p class='rw-defn idx27' style='display:none'>If you repeat something verbatim, you use the same words that were spoken or written.</p>
<p class='rw-defn idx28' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>
<p class='rw-defn idx29' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>
<p class='rw-defn idx30' style='display:none'>A yokel is uneducated, naive, and unsophisticated; they do not know much about modern life or ideas because they sequester themselves in a rural setting.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>pedantic</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='pedantic#' id='pronounce-sound' path='audio/words/amy-pedantic'></a>
puh-DAN-tik
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='pedantic#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='pedantic#' id='context-sound' path='audio/wordcontexts/brian-pedantic'></a>
Professor Snape, in the Harry Potter books, is considered to be <em>pedantic</em> because he is overly concerned about formal rules.  Snape will often choose to be unreasonable because his <em>pedantic</em> nature leads him to place dull, precise regulations over people.  His obvious discomfort when other, less <em>pedantic</em> teachers allow rules to be broken shows in his short and angry replies to these more lenient actions.  As a <em>pedant</em>, he finds any such irregularities annoying because he thinks that all rules are to be followed to the letter.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How might a <em>pedantic</em> person act?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
They would constantly correct others&#8217; minor mistakes.
</li>
<li class='choice '>
<span class='result'></span>
They like routine and do the same things in the same order every day.
</li>
<li class='choice '>
<span class='result'></span>
They would love to learn new things and explore new places.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='pedantic#' id='definition-sound' path='audio/wordmeanings/amy-pedantic'></a>
If someone is <em>pedantic</em>, they give too much importance to unimportant details and formal rules.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>excessively detail-oriented</em>
</span>
</span>
</div>
<a class='quick-help' href='pedantic#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='pedantic#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/pedantic/memory_hooks/3200.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Ped</span></span>ro the Rom<span class="emp1"><span>antic</span></span></span></span> "Should I ask her out before or after I see her?  Should I try to kiss her before, during, or after the movie?  Should I try to hold her hand for 2 minutes, or 5?  I have to do it right!"  Thus went the mind of <span class="emp3"><span>ped</span></span><span class="emp1"><span>antic</span></span> <span class="emp3"><span>Ped</span></span>ro the rom<span class="emp1"><span>antic</span></span> 18.5 hours per day.
</p>
</div>

<div id='memhook-button-bar'>
<a href="pedantic#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/pedantic/memory_hooks">Use other hook</a>
<a href="pedantic#" id="memhook-use-own" url="https://membean.com/mywords/pedantic/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='pedantic#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
If you want to be <b>pedantic</b> about it, NSFW isn't an acronym, because true acronyms (NASA, BAFTA, and the like) are pronounced as words, and you can't do that to NSFW without spraining your lips. NSFW is an initialism.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
The result is a casual, refreshing, seemingly innocent view of baseball, a game that Angell assumes not only is loved but should be loved—and without being <b>pedantic</b> or naive he explains why this is a proper attitude.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/pedantic/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='pedantic#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ped_child' data-tree-url='//cdn1.membean.com/public/data/treexml' href='pedantic#'>
<span class=''></span>
ped
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>child</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ant_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='pedantic#'>
<span class=''></span>
-ant
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ic_like' data-tree-url='//cdn1.membean.com/public/data/treexml' href='pedantic#'>
<span class=''></span>
-ic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>like</td>
</tr>
</table>
<p>A <em>pedant</em> used to be a word that meant &#8220;schoolmaster;&#8221; a teacher tends to be an enforcer of rules, which the student, or child, is expected to follow.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='pedantic#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Seinfeld</strong><span> Kramer is being pedantic.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/pedantic.jpg' video_url='examplevids/pedantic' video_width='350'></span>
<div id='wt-container'>
<img alt="Pedantic" height="288" src="https://cdn1.membean.com/video/examplevids/pedantic.jpg" width="350" />
<div class='center'>
<a href="pedantic#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='pedantic#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Pedantic" src="https://cdn1.membean.com/public/images/wordimages/cons2/pedantic.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='pedantic#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>assiduous</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>astringent</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>austere</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>circumspect</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>convoluted</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>deliberate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>didactic</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>erudite</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>exacting</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>fastidious</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>meticulous</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>peruse</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>punctilious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>scrupulous</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>sedulous</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>verbatim</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>conjecture</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>cursory</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>entropy</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>extemporaneous</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>hypothetical</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>uncouth</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>unkempt</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>yokel</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='pedantic#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
pedant
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>someone who is too rule conscious or shows off her learning</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="pedantic" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>pedantic</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="pedantic#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

