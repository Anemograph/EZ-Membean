
<!DOCTYPE html>
<html>
<head>
<title>Word: occlude | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Occlude-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/occlude-large.jpg?qdep8" />
</div>
<a href="occlude#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When you abscond, you leave suddenly from a place that has imprisoned or persecuted you, or you leave from a place with something that doesn&#8217;t belong to you.</p>
<p class='rw-defn idx1' style='display:none'>A bastion is considered an important and effective defense of a way of life or principle; a bastion can also refer to a heavily fortified place, such as a castle.</p>
<p class='rw-defn idx2' style='display:none'>When something bedevils you, it causes you a lot of problems and difficulties over a period of time.</p>
<p class='rw-defn idx3' style='display:none'>When you beleaguer someone, you act with the intent to annoy or harass that person repeatedly until they finally give you what you want.</p>
<p class='rw-defn idx4' style='display:none'>If you are beset by something, you are experiencing serious problems or dangers because of it.</p>
<p class='rw-defn idx5' style='display:none'>When you cavort, you jump and dance around in a playful, excited, or physically lively way.</p>
<p class='rw-defn idx6' style='display:none'>If one&#8217;s powers or rights are circumscribed, they are limited or restricted.</p>
<p class='rw-defn idx7' style='display:none'>If you circumvent something, such as a rule or restriction, you try to get around it in a clever and perhaps dishonest way.</p>
<p class='rw-defn idx8' style='display:none'>To contravene a law, rule, or agreement is to do something that is not allowed or is forbidden by that law, rule, or agreement.</p>
<p class='rw-defn idx9' style='display:none'>Duress is the application or threat of force to compel someone to act in a particular way.</p>
<p class='rw-defn idx10' style='display:none'>If a fact or idea eludes you, you cannot remember or understand it; if you elude someone, you manage to escape or hide from them.</p>
<p class='rw-defn idx11' style='display:none'>An enclave is a small area within a larger area of a city or country in which people of a different nationality or culture live.</p>
<p class='rw-defn idx12' style='display:none'>If something encumbers you, it makes it difficult for you to move freely or do what you want.</p>
<p class='rw-defn idx13' style='display:none'>When people equivocate, they avoid making a clear statement; they are deliberately vague in order to avoid giving a direct answer to a question.</p>
<p class='rw-defn idx14' style='display:none'>When you expedite an action or process, you make it happen more quickly.</p>
<p class='rw-defn idx15' style='display:none'>When you extricate yourself from a difficult or unpleasant situation, you manage to escape it.</p>
<p class='rw-defn idx16' style='display:none'>When you facilitate something, such as an event or project, you make it easier for everyone to get it done by giving assistance.</p>
<p class='rw-defn idx17' style='display:none'>If something fetters you, it restricts you from doing something you want to do.</p>
<p class='rw-defn idx18' style='display:none'>If someone gambols, they run, jump, and play like a young child or animal.</p>
<p class='rw-defn idx19' style='display:none'>The adjective hermetic describes something that is set apart, isolated, or separate from the influence or interference of society at large.</p>
<p class='rw-defn idx20' style='display:none'>A hiatus is a period of time when there is a break or interruption in some activity.</p>
<p class='rw-defn idx21' style='display:none'>A hindrance is an obstacle or obstruction that gets in your way and prevents you from doing something.</p>
<p class='rw-defn idx22' style='display:none'>An impediment is something that blocks or obstructs progress; it can also be a weakness or disorder, such as having difficulty with speaking.</p>
<p class='rw-defn idx23' style='display:none'>If you incarcerate someone, you put them in prison or jail.</p>
<p class='rw-defn idx24' style='display:none'>To induce a state is to bring it about; to induce someone to do something is to persuade them to do it.</p>
<p class='rw-defn idx25' style='display:none'>The adjective inexorable describes a process that is impossible to stop once it has begun.</p>
<p class='rw-defn idx26' style='display:none'>Something that inhibits you from doing something restricts or keeps you from doing it.</p>
<p class='rw-defn idx27' style='display:none'>When someone is described as itinerant, they are characterized by traveling or moving about from place to place.</p>
<p class='rw-defn idx28' style='display:none'>A manacle is a circular, usually metallic device used to chain someone&#8217;s wrists and/or ankles together.</p>
<p class='rw-defn idx29' style='display:none'>To obviate a problem is to eliminate it or do something that makes solving the problem unnecessary.</p>
<p class='rw-defn idx30' style='display:none'>To parry is to ward something off or deflect it.</p>
<p class='rw-defn idx31' style='display:none'>If someone leads a peripatetic life, they travel from place to place, living and working only for a short time in each place before moving on.</p>
<p class='rw-defn idx32' style='display:none'>When you perpetuate something, you keep it going or continue it indefinitely.</p>
<p class='rw-defn idx33' style='display:none'>When you preclude something from happening, you prevent it from doing so.</p>
<p class='rw-defn idx34' style='display:none'>If someone is predisposed to something, they are made favorable or inclined to it in advance, or they are made susceptible to something, such as a disease.</p>
<p class='rw-defn idx35' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx36' style='display:none'>A riposte is a quick and clever reply that is often made in answer to criticism of some kind.</p>
<p class='rw-defn idx37' style='display:none'>If you sequester someone, you keep them separate from other people.</p>
<p class='rw-defn idx38' style='display:none'>A stricture is a rule or condition that imposes restrictions or limits on what you can do.</p>
<p class='rw-defn idx39' style='display:none'>Something that stymies you presents an obstacle that prevents you from doing what you need or want to do.</p>
<p class='rw-defn idx40' style='display:none'>If something is subsumed, it is included within a larger class or group as a member rather than being considered separately.</p>
<p class='rw-defn idx41' style='display:none'>When you thwart someone&#8217;s ambitions, you obstruct or stop them from happening.</p>
<p class='rw-defn idx42' style='display:none'>When you trammel something or someone, you bring about limits to freedom or hinder movements or activities in some fashion.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Verb</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>occlude</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='occlude#' id='pronounce-sound' path='audio/words/amy-occlude'></a>
uh-KLOOD
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='occlude#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='occlude#' id='context-sound' path='audio/wordcontexts/brian-occlude'></a>
After the avalanche had struck the previous evening, our trail was blocked or <em>occluded</em>&#8212;we could not continue north. We tried going back south on the trail, but that way was filled with rocks as well; hence, our way forward in that direction was similarly <em>occluded</em> or closed off. So, we decided that we had to go off the trail to the east; finally, we found an area that was not <em>occluded</em> or obstructed where we could pass. Then, it was simply a matter of going around the blockage or occlusion until we could head north once again.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of <em>occluding</em> something?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Imagining the worst possible outcome to prepare yourself mentally.
</li>
<li class='choice answer '>
<span class='result'></span>
Blocking a hallway from use because there is a leak in the ceiling.
</li>
<li class='choice '>
<span class='result'></span>
Stretching your abilities to overcome an extraordinary physical challenge.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='occlude#' id='definition-sound' path='audio/wordmeanings/amy-occlude'></a>
If something, such as a road, is <em>occluded</em>, it has been closed off or blocked; therefore, cars are prevented from continuing on their way until the road is opened once again.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>block</em>
</span>
</span>
</div>
<a class='quick-help' href='occlude#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='occlude#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/occlude/memory_hooks/4572.json'></span>
<p>
<span class="emp0"><span>D<span class="emp1"><span>oc</span></span>tors Ex<span class="emp2"><span>clude</span></span>d</span></span> The hurricane <span class="emp1"><span>oc</span></span><span class="emp2"><span>clude</span></span>d every attempt at rescue of the flood victims, and even ex<span class="emp2"><span>clude</span></span>d d<span class="emp1"><span>oc</span></span>tors from treating the injured.
</p>
</div>

<div id='memhook-button-bar'>
<a href="occlude#" id="add-public-hook" style="" url="https://membean.com/mywords/occlude/memory_hooks">Use other public hook</a>
<a href="occlude#" id="memhook-use-own" url="https://membean.com/mywords/occlude/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='occlude#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
As the deposits harden and <b>occlude</b> the arterial lumen, blood flow to distant tissues decreases and a clot may become lodged, completely blocking the artery.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Phobos' close, fast orbit makes it cross paths with the sun fairly often—near-daily. But because the moon is so small it never fully <b>occludes</b> the sun to create a total eclipse. Part of the sun's disc is always visible.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Network isolation produces an echo chamber in which everybody agrees, reigning ideas are never challenged, and the sentiments and opinions of people outside of the network are systematically <b>occluded</b>.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/occlude/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='occlude#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='oc_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='occlude#'>
<span class=''></span>
oc-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='clud_shut' data-tree-url='//cdn1.membean.com/public/data/treexml' href='occlude#'>
<span class=''></span>
clud
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>shut, close</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='occlude#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>To <em>occlude</em> something is to &#8220;thoroughly shut or close&#8221; it.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='occlude#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Occlude" src="https://cdn3.membean.com/public/images/wordimages/cons2/occlude.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='occlude#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>bastion</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bedevil</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>beleaguer</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>beset</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>circumscribe</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>contravene</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>duress</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>enclave</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>encumber</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>equivocate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>fetter</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>hermetic</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>hindrance</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>impediment</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>incarcerate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>inexorable</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>inhibit</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>manacle</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>preclude</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>sequester</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>stricture</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>stymie</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>subsume</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>thwart</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>trammel</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abscond</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>cavort</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>circumvent</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>elude</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>expedite</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>extricate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>facilitate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>gambol</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>hiatus</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>induce</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>itinerant</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>obviate</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>parry</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>peripatetic</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>perpetuate</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>predispose</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>riposte</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='occlude#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
occlusion
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>blockage; obstruction</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="occlude" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>occlude</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="occlude#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

