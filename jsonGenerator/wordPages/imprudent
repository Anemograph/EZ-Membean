
<!DOCTYPE html>
<html>
<head>
<title>Word: imprudent | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Acuity is sharpness or clearness of vision, hearing, or thought.</p>
<p class='rw-defn idx1' style='display:none'>Acumen is the ability to make quick decisions and keen, precise judgments.</p>
<p class='rw-defn idx2' style='display:none'>If you adjudicate a competition or dispute, you officially decide who is right or what should be done concerning any difficulties that may arise.</p>
<p class='rw-defn idx3' style='display:none'>If you describe a decision, rule, or plan as arbitrary, you think that it was decided without any thought, standard, or system to guide it; therefore, it can seem unfair.</p>
<p class='rw-defn idx4' style='display:none'>When you describe someone as astute, you think they quickly understand situations or behavior and use it to their advantage.</p>
<p class='rw-defn idx5' style='display:none'>Cogitating about something is thinking deeply about it for a long time.</p>
<p class='rw-defn idx6' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx7' style='display:none'>If you deliberate, you think about or discuss something very carefully, especially before making an important decision.</p>
<p class='rw-defn idx8' style='display:none'>When you discern something, you notice, detect, or understand it, often after thinking about it carefully or studying it for some time.  </p>
<p class='rw-defn idx9' style='display:none'>Something that is extemporaneous, such as an action, speech, or performance, is done without any preparation or practice beforehand.</p>
<p class='rw-defn idx10' style='display:none'>If you think something, such as a statement or idea, is fatuous, you consider it stupid or extremely silly; a fatuous hope is unrealistic.</p>
<p class='rw-defn idx11' style='display:none'>When you formulate a plan of action, you carefully work it out or design it in great detail ahead of time.</p>
<p class='rw-defn idx12' style='display:none'>Something frivolous is not worth taking seriously or considering because it is silly or childish.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is impetuous does things quickly and rashly without thinking carefully first.</p>
<p class='rw-defn idx14' style='display:none'>An impromptu speech is unplanned or spontaneous—it has not been practiced in any way beforehand.</p>
<p class='rw-defn idx15' style='display:none'>If you describe someone&#8217;s behavior as inane, you think it is completely stupid or without much meaning.</p>
<p class='rw-defn idx16' style='display:none'>If an idea or thought is incisive, it is expressed in a penetrating and knowledgeable manner that is clear and brief; additionally, it can demonstrate impressive understanding of related ideas or thoughts.</p>
<p class='rw-defn idx17' style='display:none'>Someone who is indiscreet shows lack of judgment, especially because they say or do things in public that should only be said or done privately—if at all.</p>
<p class='rw-defn idx18' style='display:none'>Insouciance is a lack of concern or worry for something that should be shown more careful attention or consideration.</p>
<p class='rw-defn idx19' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx20' style='display:none'>A judicious person shows good or sound judgment because they are wise and careful in making decisions.</p>
<p class='rw-defn idx21' style='display:none'>If you describe something as ludicrous, you mean that it is extremely silly, stupid, or just plain ridiculous.</p>
<p class='rw-defn idx22' style='display:none'>A myopic person is unable to visualize the long-term negative consequences of their current actions; rather, they are narrowly focused upon short-term results.</p>
<p class='rw-defn idx23' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx24' style='display:none'>Someone is obtuse when they are slow to understand things; someone can choose to be obtuse when they deliberately do not try to figure things out.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is omniscient seems to know absolutely everything.</p>
<p class='rw-defn idx26' style='display:none'>Someone is overweening when they are not modest; rather, they think way too much of themselves and let everyone know about it.</p>
<p class='rw-defn idx27' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx28' style='display:none'>To be politic in a decision is to be socially wise and diplomatic.</p>
<p class='rw-defn idx29' style='display:none'>Someone who is prescient knows or is able to predict what will happen in the future.</p>
<p class='rw-defn idx30' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx31' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx32' style='display:none'>A puerile person is childish, immature, and foolish.</p>
<p class='rw-defn idx33' style='display:none'>Quixotic plans or ideas are not very practical or realistic; they are often based on unreasonable hopes for improving the world.</p>
<p class='rw-defn idx34' style='display:none'>If you ruminate on something, you think carefully and deeply about it over a long period of time.</p>
<p class='rw-defn idx35' style='display:none'>A sagacious person is wise, intelligent, and has the ability to make good practical decisions.</p>
<p class='rw-defn idx36' style='display:none'>If you are sapient, you are wise or very learned.</p>
<p class='rw-defn idx37' style='display:none'>If you commit a solecism, you make an error of some kind, such as one of a grammatical or social nature.</p>
<p class='rw-defn idx38' style='display:none'>To act with temerity is to act in a carelessly and irresponsibly bold way.</p>
<p class='rw-defn idx39' style='display:none'>To act in an uncouth manner is to be awkward and unmannerly.</p>
<p class='rw-defn idx40' style='display:none'>An unfounded claim is not based upon evidence; instead, it is unproven or groundless.</p>
<p class='rw-defn idx41' style='display:none'>If you say someone&#8217;s behavior is unseemly, you disapprove of it because it is not in good taste or not suitable for a particular situation.</p>
<p class='rw-defn idx42' style='display:none'>An unwarranted decision cannot be justified because there is no evidence to back it up; therefore, it is needless and uncalled-for.</p>
<p class='rw-defn idx43' style='display:none'>If you behave in an urbane way, you are behaving in a polite, refined, and civilized fashion in social situations.</p>
<p class='rw-defn idx44' style='display:none'>Something that is vacuous is empty or blank, such as a mind or stare.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>imprudent</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='imprudent#' id='pronounce-sound' path='audio/words/amy-imprudent'></a>
im-PROOD-nt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='imprudent#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='imprudent#' id='context-sound' path='audio/wordcontexts/brian-imprudent'></a>
Marianne acted in an <em>imprudent</em> or unwise way at her place of work.  She was unhappy with her job review, so she yelled at her boss, which wasn&#8217;t the only <em>imprudent</em> thing she did which lacked good judgment.  She also e-mailed all her co-workers and openly criticized the organization, a clearly <em>imprudent</em> or foolish thing to do.  Acting in such an <em>imprudent</em> or thoughtless manner will surely get her fired, and probably make it very hard to get a job elsewhere.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of an <em>imprudent</em> act?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
You decide to go to a movie after you&#8217;ve finished your homework.
</li>
<li class='choice answer '>
<span class='result'></span>
You set off on a long hike without taking any water with you.
</li>
<li class='choice '>
<span class='result'></span>
You refuse to help an angry customer until they calm down. 
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='imprudent#' id='definition-sound' path='audio/wordmeanings/amy-imprudent'></a>
When you act in an <em>imprudent</em> fashion, you do something that is unwise, is lacking in good judgment, or has no forethought.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>unwise</em>
</span>
</span>
</div>
<a class='quick-help' href='imprudent#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='imprudent#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/imprudent/memory_hooks/5686.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Imp</span></span> Is <span class="emp1"><span>Rude</span></span></span></span> That little <span class="emp3"><span>imp</span></span> was <span class="emp1"><span>rude</span></span> to the Big Boss Man, clearly an <span class="emp3"><span>imp</span></span><span class="emp1"><span>rude</span></span>nt thing to do!
</p>
</div>

<div id='memhook-button-bar'>
<a href="imprudent#" id="add-public-hook" style="" url="https://membean.com/mywords/imprudent/memory_hooks">Use other public hook</a>
<a href="imprudent#" id="memhook-use-own" url="https://membean.com/mywords/imprudent/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='imprudent#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
A spokesman for the Pentagon, Cmdr. Bob Mehal, said it would be <b>imprudent</b> to comment on "which patches do or do not represent classified units."
<cite class='attribution'>
&mdash;
New York Times
</cite>
</li>
<li>
“The market has moved to the nonbanks because the nonbanks’ appetite for risk is much higher,” said Edward J. Pinto, a director of the Center on Housing Risk. He has argued that the F.H.A. is not only failing to help low-income communities with its programs, but is actually weakening them with <b>imprudent</b> loans.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Fed Chairman Ben S. Bernanke, in office since 2006, has been bailing out the <b>imprudent</b> for the past year, believing he has no other choice. His predecessor, Alan Greenspan, episodically bailed them out during his 18 years as head Fed.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/imprudent/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='imprudent#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='im_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='imprudent#'>
<span class='common'></span>
im-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='prud_foresee' data-tree-url='//cdn1.membean.com/public/data/treexml' href='imprudent#'>
<span class=''></span>
prud
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>look ahead</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ent_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='imprudent#'>
<span class=''></span>
-ent
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p>When one is being <em>imprudent</em> in making a decision one has &#8220;not looked ahead&#8221; before making it, which can make choices unwise.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='imprudent#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Fire of Fun</strong><span> This player does something rather imprudent.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/imprudent.jpg' video_url='examplevids/imprudent' video_width='350'></span>
<div id='wt-container'>
<img alt="Imprudent" height="288" src="https://cdn1.membean.com/video/examplevids/imprudent.jpg" width="350" />
<div class='center'>
<a href="imprudent#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='imprudent#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Imprudent" src="https://cdn3.membean.com/public/images/wordimages/cons2/imprudent.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='imprudent#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>arbitrary</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>extemporaneous</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>fatuous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>frivolous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>impetuous</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>impromptu</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>inane</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>indiscreet</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>insouciance</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>ludicrous</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>myopic</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>obtuse</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>overweening</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>puerile</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>quixotic</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>solecism</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>temerity</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>uncouth</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>unfounded</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>unseemly</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>unwarranted</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>vacuous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>acuity</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>acumen</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>adjudicate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>astute</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>cogitate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>deliberate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>discern</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>formulate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>incisive</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>judicious</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>omniscient</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>politic</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>prescient</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>ruminate</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>sagacious</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>sapient</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>urbane</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='imprudent#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
prudent
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>wise, using good judgment</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="imprudent" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>imprudent</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="imprudent#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

