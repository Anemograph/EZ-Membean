
<!DOCTYPE html>
<html>
<head>
<title>Word: precursor | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An antecedent of something, such as an event or organization, has happened or existed before it and can be similar to it.</p>
<p class='rw-defn idx1' style='display:none'>A codicil is a supplement, usually to a will, that is added after the main part has been written.</p>
<p class='rw-defn idx2' style='display:none'>A denouement is the end of a book, play, or series of events when everything is explained and comes to a conclusion.</p>
<p class='rw-defn idx3' style='display:none'>A derivative is something borrowed from something else, such as an English word that comes from another language.</p>
<p class='rw-defn idx4' style='display:none'>When something ensues, it happens after or as a result of another event.</p>
<p class='rw-defn idx5' style='display:none'>A harbinger is a sign that foretells that something is going to happen, especially something bad.</p>
<p class='rw-defn idx6' style='display:none'>A predecessor comes before someone else in a job or is an ancestor of someone.</p>
<p class='rw-defn idx7' style='display:none'>Prefatory comments refer to an introduction to a book or speech.</p>
<p class='rw-defn idx8' style='display:none'>To presage a future event is to give a sign or warning that something (usually) bad is about to happen.</p>
<p class='rw-defn idx9' style='display:none'>A progenitor is someone&#8217;s ancestor; they can also be the originator of something.</p>
<p class='rw-defn idx10' style='display:none'>To prognosticate is to predict or forecast something.</p>
<p class='rw-defn idx11' style='display:none'>Something or someone that is prophetic has the quality or ability to predict the future.</p>
<p class='rw-defn idx12' style='display:none'>A ramification from an action is a result or consequence of it—and is often unanticipated.</p>
<p class='rw-defn idx13' style='display:none'>A repercussion of an act is the result or effect of it.</p>
<p class='rw-defn idx14' style='display:none'>A retrograde action causes a return to a condition or situation that is worse instead of better than the present one.</p>
<p class='rw-defn idx15' style='display:none'>Two acts that are synchronous occur at the same time.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>precursor</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='precursor#' id='pronounce-sound' path='audio/words/amy-precursor'></a>
PREE-kur-suhr
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='precursor#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='precursor#' id='context-sound' path='audio/wordcontexts/brian-precursor'></a>
The car phone was the <em>precursor</em> or ancestor of the modern day cell phone, since the car phone was the original mobile phone.  Only very wealthy people had an opportunity to use this <em>precursor</em> or originator of the cell phone.  Now, of course, cell phones are everywhere, but if it hadn&#8217;t been for the car phone as the parent or <em>precursor</em> of the cell phone, the cell phone probably would not be available today.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a likely <em>precursor</em> to a storm?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Planning an outdoor event.
</li>
<li class='choice answer '>
<span class='result'></span>
The presence of dark clouds.
</li>
<li class='choice '>
<span class='result'></span>
Flooding of roads and bridges.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='precursor#' id='definition-sound' path='audio/wordmeanings/amy-precursor'></a>
A first event is a <em>precursor</em> to a second event if the first event is responsible for the development or existence of the second.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>forerunner</em>
</span>
</span>
</div>
<a class='quick-help' href='precursor#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='precursor#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/precursor/memory_hooks/5373.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Pr</span></span>ay<span class="emp1"><span>e</span></span>r <span class="emp2"><span>or</span></span> <span class="emp3"><span>Curs</span></span>e?</span></span> Sometimes it is hard to know whether a <span class="emp1"><span>pre</span></span><span class="emp3"><span>curs</span></span><span class="emp2"><span>or</span></span> to some future event is worthy of our <span class="emp1"><span>pr</span></span>ay<span class="emp1"><span>e</span></span>rs <span class="emp2"><span>or</span></span> our <span class="emp3"><span>curs</span></span>es, because one never knows what it's going to change into!
</p>
</div>

<div id='memhook-button-bar'>
<a href="precursor#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/precursor/memory_hooks">Use other hook</a>
<a href="precursor#" id="memhook-use-own" url="https://membean.com/mywords/precursor/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='precursor#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Another <b>precursor</b> [to the failure of excess salt getting flushed from surface waters] is more floating ice than usual, which reduces the amount of ocean surface exposed to the winds, in turn reducing evaporation.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
Offering comic relief to the 1981 film’s solemnity, Bubo was a figure of George Lucas-like whimsy: the echo of R2D2, <b>precursor</b> to Jar Jar Binks.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
<li>
One more point about the value of splurging: In 2008, as a <b>precursor</b> to their Super Bowl seasons, Arizona and Pittsburgh signed no one from other teams in the first two days of free agency.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/precursor/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='precursor#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pre_before' data-tree-url='//cdn1.membean.com/public/data/treexml' href='precursor#'>
<span class='common'></span>
pre-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>before, in front</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='curs_ran' data-tree-url='//cdn1.membean.com/public/data/treexml' href='precursor#'>
<span class=''></span>
curs
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>ran, hurried</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='or_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='precursor#'>
<span class=''></span>
-or
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state</td>
</tr>
</table>
<p>A <em>precursor</em> is that which has &#8220;run before&#8221; or &#8220;hurried in front of&#8221; some later event.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='precursor#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>House MD</strong><span> The swollen blood vessel on the patient's intestine could be a precursor to her developing something far worse.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/precursor.jpg' video_url='examplevids/precursor' video_width='350'></span>
<div id='wt-container'>
<img alt="Precursor" height="288" src="https://cdn1.membean.com/video/examplevids/precursor.jpg" width="350" />
<div class='center'>
<a href="precursor#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='precursor#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Precursor" src="https://cdn0.membean.com/public/images/wordimages/cons2/precursor.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='precursor#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>antecedent</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>harbinger</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>predecessor</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>prefatory</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>presage</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>progenitor</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>prognosticate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>prophetic</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>codicil</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>denouement</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>derivative</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>ensue</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>ramification</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>repercussion</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>retrograde</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>synchronous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="precursor" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>precursor</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="precursor#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

