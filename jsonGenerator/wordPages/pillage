
<!DOCTYPE html>
<html>
<head>
<title>Word: pillage | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you are acquisitive, you are driven to pursue and own wealth and possessions—often in a greedy fashion.</p>
<p class='rw-defn idx1' style='display:none'>When you advocate a plan or action, you publicly push for implementing it.</p>
<p class='rw-defn idx2' style='display:none'>If your behavior or manner is altruistic, you show you care more about other people and their interests than you care about yourself.</p>
<p class='rw-defn idx3' style='display:none'>If you appropriate something that does not belong to you, you take it for yourself without the right to do so.</p>
<p class='rw-defn idx4' style='display:none'>When you arrogate something, such as a position or privilege, you take it even though you don&#8217;t have the legal right to it.</p>
<p class='rw-defn idx5' style='display:none'>A bastion is considered an important and effective defense of a way of life or principle; a bastion can also refer to a heavily fortified place, such as a castle.</p>
<p class='rw-defn idx6' style='display:none'>When you bolster something or someone, you offer support in a moment of need.</p>
<p class='rw-defn idx7' style='display:none'>If you describe a person&#8217;s behavior as brazen, you mean that they are not embarrassed by anything they do—they simply don&#8217;t care what other people think about them.</p>
<p class='rw-defn idx8' style='display:none'>Mass carnage is the massacre or slaughter of many people at one time, usually in battle or from an unusually intense natural disaster.</p>
<p class='rw-defn idx9' style='display:none'>If someone is granted clemency, they are punished less severely than they could have been—they have been shown mercy.</p>
<p class='rw-defn idx10' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx11' style='display:none'>Someone defoliates a tree or plant by removing its leaves, usually by applying a chemical agent.</p>
<p class='rw-defn idx12' style='display:none'>To denude an area is to remove the plants and trees that cover it; it can also mean to make something bare.</p>
<p class='rw-defn idx13' style='display:none'>Depredation can be the act of destroying or robbing a village—or the overall damage that time can do to things held dear.</p>
<p class='rw-defn idx14' style='display:none'>Deprivation is a state during which people lack something, especially adequate food and shelter; deprivation can also describe something being taken away from someone.</p>
<p class='rw-defn idx15' style='display:none'>If you desecrate something that is considered holy or very special, you deliberately spoil or damage it.</p>
<p class='rw-defn idx16' style='display:none'>When you are guilty of encroachment, you intrude upon or invade another person&#8217;s private space.</p>
<p class='rw-defn idx17' style='display:none'>If you refer to the enormity of a situation, problem, or event, you mean that it is very evil or morally offensive; this word has nothing to do with physical size.</p>
<p class='rw-defn idx18' style='display:none'>If you expropriate something, you take it away for your own use although it does not belong to you; governments frequently expropriate private land to use for public purposes.</p>
<p class='rw-defn idx19' style='display:none'>If you forage for things, such as keys or coins, you search for them inside a backpack or purse; when animals forage, they search for food over a wide area.</p>
<p class='rw-defn idx20' style='display:none'>When there is havoc, there is great disorder, widespread destruction, and much confusion.</p>
<p class='rw-defn idx21' style='display:none'>If someone demonstrates impudence, they behave in a very rude or disrespectful way.</p>
<p class='rw-defn idx22' style='display:none'>Something that is inconspicuous does not attract attention and is not easily seen or noticed because it is small or ordinary.</p>
<p class='rw-defn idx23' style='display:none'>An incursion is an unpleasant intrusion, such as a sudden hostile attack or a land being flooded.</p>
<p class='rw-defn idx24' style='display:none'>Someone who is indiscreet shows lack of judgment, especially because they say or do things in public that should only be said or done privately—if at all.</p>
<p class='rw-defn idx25' style='display:none'>To infringe on another person&#8217;s rights is to violate or intrude upon those rights.</p>
<p class='rw-defn idx26' style='display:none'>Largess is the generous act of giving money or presents to a large number of people.</p>
<p class='rw-defn idx27' style='display:none'>If someone exhibits magnanimity towards another, they show them kindness and noble generosity, especially after defeating them in battle or after having been treated badly by them.</p>
<p class='rw-defn idx28' style='display:none'>Malfeasance is an unlawful act, especially one committed by a trusted public official.</p>
<p class='rw-defn idx29' style='display:none'>A mercenary person is one whose sole interest is in earning money.</p>
<p class='rw-defn idx30' style='display:none'>A munificent person is extremely generous, especially with money.</p>
<p class='rw-defn idx31' style='display:none'>Someone is overweening when they are not modest; rather, they think way too much of themselves and let everyone know about it.</p>
<p class='rw-defn idx32' style='display:none'>Philanthropy is unselfish support in the form of donating money, work, or gifts to positive social purposes; philanthropy is also overall love for humans in general.</p>
<p class='rw-defn idx33' style='display:none'>When you plunder, you forcefully take or rob others of their possessions, usually during times of war, natural disaster, or other unrest.</p>
<p class='rw-defn idx34' style='display:none'>To be politic in a decision is to be socially wise and diplomatic.</p>
<p class='rw-defn idx35' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx36' style='display:none'>When you procure something, you obtain or get it in some fashion.</p>
<p class='rw-defn idx37' style='display:none'>When you proffer something to someone, you offer it up or hold it out to them to take.</p>
<p class='rw-defn idx38' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx39' style='display:none'>To purloin is to steal.</p>
<p class='rw-defn idx40' style='display:none'>To ransack a place is to thoroughly loot or rob items from it; it can also mean to look through something thoroughly by examining every bit of it.</p>
<p class='rw-defn idx41' style='display:none'>When you ravage something, you completely destroy, wreck, or damage it.</p>
<p class='rw-defn idx42' style='display:none'>To act with temerity is to act in a carelessly and irresponsibly bold way.</p>
<p class='rw-defn idx43' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>
<p class='rw-defn idx44' style='display:none'>If you say someone&#8217;s behavior is unseemly, you disapprove of it because it is not in good taste or not suitable for a particular situation.</p>
<p class='rw-defn idx45' style='display:none'>When you usurp someone else&#8217;s power, position, or role, you take it from them although you do not have the right to do so.</p>
<p class='rw-defn idx46' style='display:none'>A windfall is the sudden appearance of a large amount of money that is completely unexpected.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>pillage</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='pillage#' id='pronounce-sound' path='audio/words/amy-pillage'></a>
PIL-ij
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='pillage#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='pillage#' id='context-sound' path='audio/wordcontexts/brian-pillage'></a>
Throughout human history, it has been the habit of invading armies to destroy and <em>pillage</em> the homes and lands of their enemies.  It has always seemed unfair that it is the non-fighting, common people whose possessions are stolen and <em>pillaged</em>.  In spite of many treaties and agreements to prevent looting and killing in times of war, <em><em>pillaging</em></em> nevertheless remains common because the conquerors believe that it is their right to rob the conquered.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of <em>pillaging</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A person who sneaks illegal goods from one country into another.
</li>
<li class='choice '>
<span class='result'></span>
A family that finds an abandoned house and decides to live in it for a while.
</li>
<li class='choice answer '>
<span class='result'></span>
An angry mob that smashes store windows and takes everything inside.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='pillage#' id='definition-sound' path='audio/wordmeanings/amy-pillage'></a>
If soldiers <em>pillage</em> a place, such as a town or museum, they steal a lot of things and damage it using violent methods.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>raid and steal</em>
</span>
</span>
</div>
<a class='quick-help' href='pillage#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='pillage#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/pillage/memory_hooks/3244.json'></span>
<p>
<span class="emp0"><span>Vikings P<span class="emp1"><span>illage</span></span> V<span class="emp1"><span>illage</span></span></span></span> Today the band of Vikings p<span class="emp1"><span>illage</span></span>d our v<span class="emp1"><span>illage</span></span>, so now we have nothing left, and we'll die of starvation for sure.
</p>
</div>

<div id='memhook-button-bar'>
<a href="pillage#" id="add-public-hook" style="" url="https://membean.com/mywords/pillage/memory_hooks">Use other public hook</a>
<a href="pillage#" id="memhook-use-own" url="https://membean.com/mywords/pillage/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='pillage#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The Vikings' sin was to attack and <b>pillage</b> the holy monasteries, the sacred places of the Christian world.
<cite class='attribution'>
&mdash;
BBC
</cite>
</li>
<li>
Designers such as John Galliano, Alexander McQueen and even Gwen Stefani endlessly <b>pillage</b> Westwood’s archives, including her advertising and editorial images, as their own.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
After the Giants' final out in 1957, 11,000 New Yorkers ran on to the field to <b>pillage—</b>[they were furious] their beloved team was forsaking the Polo Grounds for San Francisco.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/pillage/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='pillage#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='age_do' data-tree-url='//cdn1.membean.com/public/data/treexml' href='pillage#'>
<span class=''></span>
-age
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state</td>
</tr>
</table>
<p><em>Pillage</em> comes from a root word meaning to pluck; hence, <em>pillaging</em> is &quot;the state of plundering or plucking goods from others.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='pillage#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Platoon</strong><span> These soldiers are pillaging the village.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/pillage.jpg' video_url='examplevids/pillage' video_width='350'></span>
<div id='wt-container'>
<img alt="Pillage" height="288" src="https://cdn1.membean.com/video/examplevids/pillage.jpg" width="350" />
<div class='center'>
<a href="pillage#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='pillage#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Pillage" src="https://cdn3.membean.com/public/images/wordimages/cons2/pillage.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='pillage#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>acquisitive</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>appropriate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>arrogate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>brazen</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>carnage</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>defoliate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>denude</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>depredation</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>deprivation</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>desecrate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>encroachment</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>enormity</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>expropriate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>forage</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>havoc</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>impudence</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>incursion</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>indiscreet</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>infringe</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>malfeasance</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>mercenary</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>overweening</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>plunder</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>procure</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>purloin</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>ransack</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>ravage</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>temerity</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>unseemly</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>usurp</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>advocate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>altruistic</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>bastion</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>bolster</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>clemency</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>inconspicuous</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>largess</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>magnanimity</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>munificent</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>philanthropy</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>politic</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>proffer</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>windfall</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="pillage" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>pillage</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="pillage#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

