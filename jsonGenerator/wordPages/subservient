
<!DOCTYPE html>
<html>
<head>
<title>Word: subservient | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abase yourself, people respect you less because you act in a way that is beneath you; due to this behavior, you are lowered or reduced in rank, esteem, or reputation. </p>
<p class='rw-defn idx1' style='display:none'>The word abject emphasizes a very bad situation or quality, thereby making it even worse.</p>
<p class='rw-defn idx2' style='display:none'>An acolyte is someone who serves a leader as a devoted assistant or believes in the leader&#8217;s ideas.</p>
<p class='rw-defn idx3' style='display:none'>If you acquiesce to something, you allow it to happen by doing what someone wants without putting up a struggle or voicing your own concerns.</p>
<p class='rw-defn idx4' style='display:none'>When something is ancillary to something else, such as a workbook to a textbook, it supports it but is less important than that which it supports.</p>
<p class='rw-defn idx5' style='display:none'>An autonomous person makes their own decisions without being influenced by anyone else; an autonomous country or organization is independent and has the power to govern itself.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is contumacious is purposely stubborn, contrary, or disobedient.</p>
<p class='rw-defn idx7' style='display:none'>If you behave with deference towards someone, you show them respect and accept their opinions or decisions, especially because they have an important position.</p>
<p class='rw-defn idx8' style='display:none'>A despot is a leader who has a lot of power and uses it in a cruel and unreasonable way.</p>
<p class='rw-defn idx9' style='display:none'>A dissident is someone who disagrees publicly with a government, especially in a country where this is not allowed.</p>
<p class='rw-defn idx10' style='display:none'>If someone is fractious, they are easily upset or annoyed over unimportant things.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is incorrigible has bad habits or does bad things and is unlikely to ever change; this word is often used in a humorous way.</p>
<p class='rw-defn idx12' style='display:none'>A lackey is a person who follows their superior&#8217;s orders so completely that they never openly question those commands.</p>
<p class='rw-defn idx13' style='display:none'>A manacle is a circular, usually metallic device used to chain someone&#8217;s wrists and/or ankles together.</p>
<p class='rw-defn idx14' style='display:none'>A minion is an unimportant person who merely obeys the orders of someone else; they  usually perform boring tasks that require little skill.</p>
<p class='rw-defn idx15' style='display:none'>If someone is being obsequious, they are trying so hard to please someone that they lack sincerity in their actions towards that person.</p>
<p class='rw-defn idx16' style='display:none'>Someone who is obstreperous is noisy, unruly, and difficult to control.</p>
<p class='rw-defn idx17' style='display:none'>A peremptory action, such as a decree or demand, is authoritative and absolute; therefore, it is not open to debate but must be carried out.</p>
<p class='rw-defn idx18' style='display:none'>A recalcitrant animal or person is difficult to control and refuses to obey orders—even after stiff punishment.</p>
<p class='rw-defn idx19' style='display:none'>Refractory people deliberately don&#8217;t obey someone in authority and so are difficult to deal with or control.</p>
<p class='rw-defn idx20' style='display:none'>A restive person is not willing or able to keep still or be patient because they are bored or dissatisfied with something; consequently, they are becoming difficult to control.</p>
<p class='rw-defn idx21' style='display:none'>If you say that someone is servile, you don&#8217;t respect them because they are too obedient, too agreeable, and too willing to do anything for another person.</p>
<p class='rw-defn idx22' style='display:none'>If you sidle, you walk slowly, cautiously and often sideways in a particular direction, usually because you do not want to be noticed.</p>
<p class='rw-defn idx23' style='display:none'>If you are supine, you are lying on your back with your face upward.</p>
<p class='rw-defn idx24' style='display:none'>In the Middle Ages, a vassal was a worker who served a lord in exchange for land to live upon; today you are a vassal if you serve another and are largely controlled by them.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>subservient</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='subservient#' id='pronounce-sound' path='audio/words/amy-subservient'></a>
suhb-SUR-vee-uhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='subservient#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='subservient#' id='context-sound' path='audio/wordcontexts/brian-subservient'></a>
The earnest, selfless butler was completely <em>subservient</em> to his employers, putting aside his own needs to see to their every wish and desire.  He moved silently from room to room, spoke softly, and anticipated their every want with the <em>subservient</em> speed of one who puts the wishes of others before his own.  Bertram the butler turned <em>subservient</em> or constant service into an art form, and even his bowed back reflected his practice of bowing before others.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How does someone who is <em>subservient</em> act?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They quietly rebel against authority in small and subtle ways.
</li>
<li class='choice answer '>
<span class='result'></span>
They readily do whatever other people want them to do.
</li>
<li class='choice '>
<span class='result'></span>
They stay in the background and try not to attract attention.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='subservient#' id='definition-sound' path='audio/wordmeanings/amy-subservient'></a>
If you are <em>subservient</em>, you are too eager and willing to do what other people want and often put your own wishes aside.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>eager to please</em>
</span>
</span>
</div>
<a class='quick-help' href='subservient#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='subservient#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/subservient/memory_hooks/3201.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Sub</span></span>way <span class="emp2"><span>Serv</span></span>a<span class="emp2"><span>nt</span></span></span></span> The man was so eager to please his boss at <span class="emp2"><span>Sub</span></span>way that he eagerly did absolutely everything that he could, acting like a <span class="emp3"><span>sub</span></span><span class="emp2"><span>serv</span></span>ie<span class="emp2"><span>nt</span></span> <span class="emp2"><span>serv</span></span>a<span class="emp2"><span>nt</span></span> rather than an employee with rights.
</p>
</div>

<div id='memhook-button-bar'>
<a href="subservient#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/subservient/memory_hooks">Use other hook</a>
<a href="subservient#" id="memhook-use-own" url="https://membean.com/mywords/subservient/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='subservient#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
I always resented the role of a drummer as nothing more than a <b>subservient</b> figure.
<cite class='attribution'>
&mdash;
Max Roach, American jazz drummer and composer
</cite>
</li>
<li>
While Isaac Asimov imagined how humanity might relate to his <b>subservient</b> robots, Sir Arthur [Clarke] created aliens so different from humans that communication was impossible.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
Not for the first time, Britain is made to look like a <b>subservient</b> satellite taken wretchedly for granted by the country that is supposed to be its closest ally.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
In one of his earliest works, Edmund Burke tells us that fear is the hallmark of a despotic society. Fear is used to make a population stupid and <b>subservient</b>; it is used to chill the natural demand for the most basic of freedoms and liberties.
<cite class='attribution'>
&mdash;
Harper's Magazine
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/subservient/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='subservient#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='sub_under' data-tree-url='//cdn1.membean.com/public/data/treexml' href='subservient#'>
<span class='common'></span>
sub-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>under, from below</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='serv_serve' data-tree-url='//cdn1.membean.com/public/data/treexml' href='subservient#'>
<span class=''></span>
serv
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>wait on, serve</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ent_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='subservient#'>
<span class=''></span>
-ent
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p>A <em>subservient</em> person is used to &#8220;being in a condition of waiting on or serving others from below&#8221; since he is &#8220;under&#8221; their authority.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='subservient#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Patrick Santiague</strong><span> This is one subservient chicken!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/subservient.jpg' video_url='examplevids/subservient' video_width='350'></span>
<div id='wt-container'>
<img alt="Subservient" height="288" src="https://cdn1.membean.com/video/examplevids/subservient.jpg" width="350" />
<div class='center'>
<a href="subservient#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='subservient#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Subservient" src="https://cdn1.membean.com/public/images/wordimages/cons2/subservient.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='subservient#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abase</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abject</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>acolyte</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>acquiesce</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>ancillary</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>deference</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>lackey</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>manacle</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>minion</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>obsequious</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>servile</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>supine</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>vassal</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='5' class = 'rw-wordform notlearned'><span>autonomous</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>contumacious</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>despot</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>dissident</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>fractious</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>incorrigible</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>obstreperous</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>peremptory</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>recalcitrant</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>refractory</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>restive</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>sidle</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="subservient" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>subservient</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="subservient#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

