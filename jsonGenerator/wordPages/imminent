
<!DOCTYPE html>
<html>
<head>
<title>Word: imminent | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Imminent-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/imminent-large.jpg?qdep8" />
</div>
<a href="imminent#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When something bedevils you, it causes you a lot of problems and difficulties over a period of time.</p>
<p class='rw-defn idx1' style='display:none'>If someone beguiles you, you are charmed by and attracted to them; you can also be tricked or deceived by someone who beguiles you.</p>
<p class='rw-defn idx2' style='display:none'>When you beleaguer someone, you act with the intent to annoy or harass that person repeatedly until they finally give you what you want.</p>
<p class='rw-defn idx3' style='display:none'>If you are beset by something, you are experiencing serious problems or dangers because of it.</p>
<p class='rw-defn idx4' style='display:none'>If you circumvent something, such as a rule or restriction, you try to get around it in a clever and perhaps dishonest way.</p>
<p class='rw-defn idx5' style='display:none'>If something confounds you, it makes you feel surprised and confused, often because it does not meet your predefined expectations.</p>
<p class='rw-defn idx6' style='display:none'>To contravene a law, rule, or agreement is to do something that is not allowed or is forbidden by that law, rule, or agreement.</p>
<p class='rw-defn idx7' style='display:none'>Duress is the application or threat of force to compel someone to act in a particular way.</p>
<p class='rw-defn idx8' style='display:none'>If a fact or idea eludes you, you cannot remember or understand it; if you elude someone, you manage to escape or hide from them.</p>
<p class='rw-defn idx9' style='display:none'>If something encumbers you, it makes it difficult for you to move freely or do what you want.</p>
<p class='rw-defn idx10' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx11' style='display:none'>An impending event is approaching fast or is about to occur; this word usually has a negative implication, referring to something threatening or harmful coming.</p>
<p class='rw-defn idx12' style='display:none'>Something that is ineluctable is impossible to avoid or escape, however much you try.</p>
<p class='rw-defn idx13' style='display:none'>The adjective inexorable describes a process that is impossible to stop once it has begun.</p>
<p class='rw-defn idx14' style='display:none'>An irrevocable action or decision is impossible to change, reverse, or stop.</p>
<p class='rw-defn idx15' style='display:none'>To obviate a problem is to eliminate it or do something that makes solving the problem unnecessary.</p>
<p class='rw-defn idx16' style='display:none'>An ominous sign predicts or shows that something bad will happen in the near future.</p>
<p class='rw-defn idx17' style='display:none'>To parry is to ward something off or deflect it.</p>
<p class='rw-defn idx18' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx19' style='display:none'>If you employ subterfuge, you use a secret plan or action to get what you want by outwardly doing one thing that cleverly hides your true intentions.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>imminent</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='imminent#' id='pronounce-sound' path='audio/words/amy-imminent'></a>
IM-uh-nuhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='imminent#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='imminent#' id='context-sound' path='audio/wordcontexts/brian-imminent'></a>
With final exams <em>imminent</em> or soon to take place, Michael decided he needed a break from the stress of studying.  He gathered his friends together for a party to celebrate the <em>imminent</em>, fast-approaching end of their long semester.  Some chose not to attend Michael&#8217;s party since an <em>imminent</em> or inescapable event was about to occur the next day: their research papers were due.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Which of these puts you in <em>imminent</em> danger?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A fire in your kitchen was put out by firefighters before it destroyed the whole house.
</li>
<li class='choice '>
<span class='result'></span>
A tornado is forecast to possibly occur in your state sometime next week.
</li>
<li class='choice answer '>
<span class='result'></span>
A large and angry herd of elephants is chasing you through the jungle.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='imminent#' id='definition-sound' path='audio/wordmeanings/amy-imminent'></a>
An <em>imminent</em> event, especially an event that is unpleasant, is almost certain to happen within the very near future.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>happening soon</em>
</span>
</span>
</div>
<a class='quick-help' href='imminent#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='imminent#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/imminent/memory_hooks/6235.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Ne</span></span>w <span class="emp1"><span>Immi</span></span>gra<span class="emp1"><span>nt</span></span>s</span></span> A wave of <span class="emp2"><span>ne</span></span>w <span class="emp1"><span>immi</span></span>gra<span class="emp1"><span>nt</span></span>s is due in our town any day now; this <span class="emp1"><span>immi</span></span><span class="emp2"><span>ne</span></span><span class="emp1"><span>nt</span></span> eve<span class="emp1"><span>nt</span></span> has been well-prepared for by town officials since we have known about it now for three weeks.
</p>
</div>

<div id='memhook-button-bar'>
<a href="imminent#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/imminent/memory_hooks">Use other hook</a>
<a href="imminent#" id="memhook-use-own" url="https://membean.com/mywords/imminent/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='imminent#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
“Humans are forcing [these whales] ever closer to extinction and that extinction is <b>imminent</b> unless we take action now."
<cite class='attribution'>
&mdash;
Cape Cod Times
</cite>
</li>
<li>
The Manatee County Public Safety Department told people near the plant to evacuate due to an "<b>imminent</b> uncontrolled release of wastewater." . . . . [saying,] "structural collapse could occur at any time."
<cite class='attribution'>
&mdash;
CBS News
</cite>
</li>
<li>
U.N. Secretary-General Antonio Guterres said on Friday that war-torn Yemen is in “<b>imminent</b> danger of the worst famine the world has seen for decades, . . . In the absence of immediate action, millions of lives may be lost,” Guterres said in a statement . . . .
<cite class='attribution'>
&mdash;
Reuters
</cite>
</li>
<li>
A recent reawakening of the tallest geyser in the world is not a harbinger of an <b>imminent</b> volcanic eruption, a new study reports.
<cite class='attribution'>
&mdash;
Science News
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/imminent/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='imminent#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='im_in' data-tree-url='//cdn1.membean.com/public/data/treexml' href='imminent#'>
<span class='common'></span>
im-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>in, on</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='min_lean' data-tree-url='//cdn1.membean.com/public/data/treexml' href='imminent#'>
<span class=''></span>
min
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>lean forward, threaten</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ent_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='imminent#'>
<span class=''></span>
-ent
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p>An <em>imminent</em> situation is one that is &#8220;hanging forward on&#8221; someone, or is &#8220;threatening&#8221; him because it is about to occur very soon.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='imminent#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Return of the Jedi</strong><span> Luke is aware of the imminent attack on the rebel fleet.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/imminent.jpg' video_url='examplevids/imminent' video_width='350'></span>
<div id='wt-container'>
<img alt="Imminent" height="288" src="https://cdn1.membean.com/video/examplevids/imminent.jpg" width="350" />
<div class='center'>
<a href="imminent#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='imminent#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Imminent" src="https://cdn3.membean.com/public/images/wordimages/cons2/imminent.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='imminent#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>bedevil</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>beleaguer</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>beset</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>duress</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>encumber</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>impending</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>ineluctable</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>inexorable</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>irrevocable</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>ominous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>beguile</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>circumvent</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>confound</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>contravene</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>elude</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>obviate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>parry</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>subterfuge</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="imminent" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>imminent</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="imminent#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

