
<!DOCTYPE html>
<html>
<head>
<title>Word: consensus | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you are in accord with someone, you are in agreement or harmony with them.</p>
<p class='rw-defn idx1' style='display:none'>If you acquiesce to something, you allow it to happen by doing what someone wants without putting up a struggle or voicing your own concerns.</p>
<p class='rw-defn idx2' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx3' style='display:none'>When you act in an aloof fashion towards others, you purposely do not participate in activities with them; instead, you remain distant and detached.</p>
<p class='rw-defn idx4' style='display:none'>An amicable person is very friendly and agreeable towards others.</p>
<p class='rw-defn idx5' style='display:none'>Amity is a peaceful friendship between two parties, particularly between two countries.</p>
<p class='rw-defn idx6' style='display:none'>If you have animus against someone, you have a strong feeling of dislike or hatred towards them, often without a good reason or based upon your personal prejudices.</p>
<p class='rw-defn idx7' style='display:none'>Your antagonist is an opponent in a competition or battle.</p>
<p class='rw-defn idx8' style='display:none'>Antipathy is a strong feeling of dislike or hostility towards someone or something.</p>
<p class='rw-defn idx9' style='display:none'>The antithesis of something is its opposite.</p>
<p class='rw-defn idx10' style='display:none'>An autonomous person makes their own decisions without being influenced by anyone else; an autonomous country or organization is independent and has the power to govern itself.</p>
<p class='rw-defn idx11' style='display:none'>Bedlam is a situation with a great deal of noise and confusion.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is cantankerous is bad-tempered and always finds things to complain about.</p>
<p class='rw-defn idx13' style='display:none'>A captious person has a fondness for catching others at fault; hence, they are overly critical and raise unwarranted objections too often.</p>
<p class='rw-defn idx14' style='display:none'>To cavil is to make unnecessary complaints about things that are unimportant.</p>
<p class='rw-defn idx15' style='display:none'>A chaotic state of affairs is in a state of confusion and complete disorder.</p>
<p class='rw-defn idx16' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx17' style='display:none'>Conciliation is a process intended to end an argument between two groups of people.</p>
<p class='rw-defn idx18' style='display:none'>When someone is contentious, they are argumentative and likely to provoke a fight.</p>
<p class='rw-defn idx19' style='display:none'>When a situation, such as an argument or election, is in a deadlock, the participants have come to a complete stop because neither side can proceed further.</p>
<p class='rw-defn idx20' style='display:none'>A situation or thing that is discordant does not fit in with other things; therefore, it is disagreeable, strange, or unpleasant.</p>
<p class='rw-defn idx21' style='display:none'>Things that are disparate are clearly different from each other and belong to different groups or classes.</p>
<p class='rw-defn idx22' style='display:none'>Dissension is a disagreement or difference of opinion among a group of people that can cause conflict.</p>
<p class='rw-defn idx23' style='display:none'>A dissident is someone who disagrees publicly with a government, especially in a country where this is not allowed.</p>
<p class='rw-defn idx24' style='display:none'>Dissonance is an unpleasant situation of opposition in which ideas or actions are not in agreement or harmony; dissonance also refers to a harsh combination of sounds.</p>
<p class='rw-defn idx25' style='display:none'>A small group or issue that is factious is controversial and promotes strong disagreement or dissatisfaction within a larger group.</p>
<p class='rw-defn idx26' style='display:none'>A fracas is a rough and noisy fight or loud argument that can involve multiple people.</p>
<p class='rw-defn idx27' style='display:none'>When two people are in a harmonious state, they are in agreement with each other; when a sound is harmonious, it is pleasant or agreeable to the ear.</p>
<p class='rw-defn idx28' style='display:none'>If someone is implacable, they very stubbornly react to situations or the opinions of others because of strong feelings that make them unwilling to change their mind.</p>
<p class='rw-defn idx29' style='display:none'>If two people are incompatible, they do not get along, tend to disagree, and are unable to cooperate with one another.</p>
<p class='rw-defn idx30' style='display:none'>If someone is inimical, they are unfriendly and will likely cause you harm.</p>
<p class='rw-defn idx31' style='display:none'>Two irreconcilable opinions or points of view are so opposed to each other that it is not possible to accept both of them or reach a settlement between them.</p>
<p class='rw-defn idx32' style='display:none'>A nonconformist is unwilling to believe in the same things other people do or act in a fashion that society sets as a standard.</p>
<p class='rw-defn idx33' style='display:none'>Pandemonium is a very noisy and uncontrolled situation, especially one that is caused by a lot of angry or excited people.</p>
<p class='rw-defn idx34' style='display:none'>Polarization between two groups is a division or separation caused by a difference in opinion or conflicting views.</p>
<p class='rw-defn idx35' style='display:none'>If two people have established a good rapport, their connection is such that they have a good understanding of and can communicate well with one other.</p>
<p class='rw-defn idx36' style='display:none'>Rapprochement is the development of greater understanding and friendliness between two countries or groups of people after a period of unfriendly relations.</p>
<p class='rw-defn idx37' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx38' style='display:none'>A schism causes a group or organization to divide into two groups as a result of differences in their aims and beliefs.</p>
<p class='rw-defn idx39' style='display:none'>Strife is struggle or conflict that sometimes turns violent.</p>
<p class='rw-defn idx40' style='display:none'>Synergy is the extra energy or additional effectiveness gained when two groups or organizations combine their efforts instead of working separately.</p>
<p class='rw-defn idx41' style='display:none'>All people involved in a unanimous decision agree or are united in their opinion about something.</p>
<p class='rw-defn idx42' style='display:none'>A unilateral decision is entirely one-sided, involving only one facet of an issue or one country among many.</p>
<p class='rw-defn idx43' style='display:none'>Doing something in unison is doing it all together at one time.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>consensus</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='consensus#' id='pronounce-sound' path='audio/words/amy-consensus'></a>
kuhn-SEN-suhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='consensus#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='consensus#' id='context-sound' path='audio/wordcontexts/brian-consensus'></a>
The <em>consensus</em> is that we want to look for a new cleaning service; since we have all agreed on this, let&#8217;s go ahead and get started.  We reached this <em>consensus</em> or general opinion quite easily because very few of us were pleased with the current providers of that service.  Hopefully the next time we have to reach a <em>consensus</em> or overall similar feeling about something it will be this easy!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When is a <em>consensus</em> reached?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When a new member wants to join a group but is not able to do so.
</li>
<li class='choice '>
<span class='result'></span>
When one member of a group makes a decision for the whole group.
</li>
<li class='choice answer '>
<span class='result'></span>
When all members of a committee agree on a course of action.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='consensus#' id='definition-sound' path='audio/wordmeanings/amy-consensus'></a>
When a group of people reaches a <em>consensus</em>, it has reached a general agreement about something.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>agreement</em>
</span>
</span>
</div>
<a class='quick-help' href='consensus#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='consensus#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/consensus/memory_hooks/4237.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Us</span></span> <span class="emp2"><span>Can</span></span> <span class="emp3"><span>Sens</span></span>e</span></span> We have all reached a <span class="emp2"><span>con</span></span><span class="emp3"><span>sens</span></span><span class="emp1"><span>us</span></span>, so <span class="emp1"><span>us</span></span> <span class="emp2"><span>can</span></span> <span class="emp3"><span>sens</span></span>e what everyone else feels!
</p>
</div>

<div id='memhook-button-bar'>
<a href="consensus#" id="add-public-hook" style="" url="https://membean.com/mywords/consensus/memory_hooks">Use other public hook</a>
<a href="consensus#" id="memhook-use-own" url="https://membean.com/mywords/consensus/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='consensus#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Historically, the claim of <b>consensus</b> has been the first refuge of scoundrels; it is a way to avoid debate by claiming that the matter is already settled. Whenever you hear the <b>consensus</b> of scientists agrees on something or other, reach for your wallet, because you're being had.
<span class='attribution'>&mdash; Michael Crichton, American author and filmmaker</span>
<img alt="Michael crichton, american author and filmmaker" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Michael Crichton, American author and filmmaker.jpg?qdep8" width="80" />
</li>
<li>
Clearly, there is <b>consensus</b> that calcium is necessary for good health — but no <b>consensus</b> on whether calcium is best when consumed from dairy or other sources.
<cite class='attribution'>
&mdash;
WebMD
</cite>
</li>
<li>
The upper range [of the oil spill's flow rate] was not included in their report because scientists analyzing the flow were unable to reach a <b>consensus</b> on how bad it could be.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/consensus/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='consensus#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='con_with' data-tree-url='//cdn1.membean.com/public/data/treexml' href='consensus#'>
<span class='common'></span>
con-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>with, together</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='sens_felt' data-tree-url='//cdn1.membean.com/public/data/treexml' href='consensus#'>
<span class='common'></span>
sens
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>felt, sensed, perceived</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='us_noun' data-tree-url='//cdn1.membean.com/public/data/treexml' href='consensus#'>
<span class=''></span>
-us
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>forms a Latin noun</td>
</tr>
</table>
<p>When everyone in a group has reached a similar opinion that they have &#8220;felt with&#8221; other members of the group, that group has reached a <em>consensus</em>.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='consensus#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Occupy Wall Street</strong><span> It's positive to work via consensus.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/consensus.jpg' video_url='examplevids/consensus' video_width='350'></span>
<div id='wt-container'>
<img alt="Consensus" height="288" src="https://cdn1.membean.com/video/examplevids/consensus.jpg" width="350" />
<div class='center'>
<a href="consensus#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='consensus#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Consensus" src="https://cdn2.membean.com/public/images/wordimages/cons2/consensus.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='consensus#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>accord</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>acquiesce</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>amicable</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>amity</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>conciliation</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>harmonious</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>rapport</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>rapprochement</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>synergy</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>unanimous</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>unison</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>aloof</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>animus</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>antagonist</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>antipathy</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>antithesis</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>autonomous</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>bedlam</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>cantankerous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>captious</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>cavil</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>chaotic</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>contentious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>deadlock</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>discordant</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>disparate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>dissension</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>dissident</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>dissonance</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>factious</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>fracas</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>implacable</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>incompatible</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>inimical</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>irreconcilable</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>nonconformist</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>pandemonium</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>polarization</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>schism</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>strife</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>unilateral</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="consensus" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>consensus</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="consensus#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

