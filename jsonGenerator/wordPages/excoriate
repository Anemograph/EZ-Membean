
<!DOCTYPE html>
<html>
<head>
<title>Word: excoriate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Someone who has an abrasive manner is unkind and rude, wearing away at you in an irritating fashion.</p>
<p class='rw-defn idx1' style='display:none'>Adulation is praise and admiration for someone that includes more than they deserve, usually for the purposes of flattery.</p>
<p class='rw-defn idx2' style='display:none'>When you arraign someone, you make them come to court and answer criminal charges made against them.</p>
<p class='rw-defn idx3' style='display:none'>When you are astringent towards someone, you speak to or write about them in a critical and hurtful manner.</p>
<p class='rw-defn idx4' style='display:none'>A captious person has a fondness for catching others at fault; hence, they are overly critical and raise unwarranted objections too often.</p>
<p class='rw-defn idx5' style='display:none'>When you castigate someone, you criticize or punish them severely.</p>
<p class='rw-defn idx6' style='display:none'>Censure is written or verbal disapproval and harsh criticism of something that someone has done.</p>
<p class='rw-defn idx7' style='display:none'>To decry something is to speak against it and find fault with it.</p>
<p class='rw-defn idx8' style='display:none'>If you denounce people or actions, you criticize them severely in public because you feel strongly that they are wrong or evil.</p>
<p class='rw-defn idx9' style='display:none'>If you deprecate something, you disapprove of it strongly.</p>
<p class='rw-defn idx10' style='display:none'>An emollient substance soothes your skin and provides moisture.</p>
<p class='rw-defn idx11' style='display:none'>An encomium strongly praises someone or something via oral or written communication.</p>
<p class='rw-defn idx12' style='display:none'>A eulogy is a speech or other piece of writing, often part of a funeral, in which someone or something is highly praised.</p>
<p class='rw-defn idx13' style='display:none'>If you exculpate someone, you prove that they are not guilty of a crime.</p>
<p class='rw-defn idx14' style='display:none'>An exemplary person is one that sets an ideal or praiseworthy example for others to follow.</p>
<p class='rw-defn idx15' style='display:none'>When someone is exonerated from guilt, a task, or a responsibility, they are set free or excused from it.</p>
<p class='rw-defn idx16' style='display:none'>If you extol something or someone, you praise it or them very enthusiastically.</p>
<p class='rw-defn idx17' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx18' style='display:none'>A laudatory article or speech expresses praise or admiration for someone or something.</p>
<p class='rw-defn idx19' style='display:none'>When someone is lionized, they are treated as being very important or famous—although they may not deserve to be.</p>
<p class='rw-defn idx20' style='display:none'>A paean is a piece of writing, verbal expression, or song that expresses enthusiastic praise, admiration, or joy.</p>
<p class='rw-defn idx21' style='display:none'>A panegyric is a speech or article that praises someone or something a lot.</p>
<p class='rw-defn idx22' style='display:none'>If someone is <em>pilloried</em>, they are publicly criticized or ridiculed by a lot of people, especially in the media.</p>
<p class='rw-defn idx23' style='display:none'>If you receive a plaudit, you receive admiration, praise, and approval from someone.</p>
<p class='rw-defn idx24' style='display:none'>To remonstrate with someone is to tell that person that you strongly disapprove of something they have said or done.</p>
<p class='rw-defn idx25' style='display:none'>A reprobate has a bad character and behaves in an immoral or improper way.</p>
<p class='rw-defn idx26' style='display:none'>To come out of something unscathed is to come out of it uninjured.</p>
<p class='rw-defn idx27' style='display:none'>If you vilify people, you write or say bad things about them in an attempt to make them unpopular.</p>
<p class='rw-defn idx28' style='display:none'>Vituperative remarks are full of hate, anger, and cruel criticism.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>excoriate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='excoriate#' id='pronounce-sound' path='audio/words/amy-excoriate'></a>
ik-SKOHR-ee-ayt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='excoriate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='excoriate#' id='context-sound' path='audio/wordcontexts/brian-excoriate'></a>
The commanding sergeant harshly yelled at the young soldier, <em><em>excoriating</em></em> him for the mistakes he had made in the training maneuver.  The nervous private hung his head in shame at being <em>excoriated</em> or verbally scolded in front of the entire platoon.  Finding every fault with recruits by <em><em>excoriating</em></em> or expressing an angry judgment of their misdeeds is common during basic training.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What might you say to <em>excoriate</em> someone?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
“I’m sorry to be the one to have to tell you this bad news.”
</li>
<li class='choice answer '>
<span class='result'></span>
“You made a horrible decision, and I am ashamed of you!”
</li>
<li class='choice '>
<span class='result'></span>
“You are cleared of all wrongdoing and are free to go.”
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='excoriate#' id='definition-sound' path='audio/wordmeanings/amy-excoriate'></a>
If you <em>excoriate</em> someone, you express very strong disapproval of something they did.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>criticize</em>
</span>
</span>
</div>
<a class='quick-help' href='excoriate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='excoriate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/excoriate/memory_hooks/4015.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Ex</span></span><span class="emp2"><span>cori</span></span>ating <span class="emp2"><span>Corey</span></span></span></span> <span class="emp2"><span>Corey</span></span>'s baseball coach <span class="emp1"><span>ex</span></span><span class="emp2"><span>cori</span></span>ated him so harshly when he ran the bases backward that <span class="emp2"><span>Corey</span></span> crawled under the bench and whimpered, "<span class="emp2"><span>Corey</span></span>'s not here. No more <span class="emp2"><span>Corey</span></span>. I'm just the <span class="emp1"><span>ex</span></span>-<span class="emp2"><span>Corey</span></span>."  <span class="emp2"><span>Corey</span></span> had been <span class="emp1"><span>ex</span></span><span class="emp2"><span>cori</span></span>ated.
</p>
</div>

<div id='memhook-button-bar'>
<a href="excoriate#" id="add-public-hook" style="" url="https://membean.com/mywords/excoriate/memory_hooks">Use other public hook</a>
<a href="excoriate#" id="memhook-use-own" url="https://membean.com/mywords/excoriate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='excoriate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
And Tuesday, she used a series of late-day tweets like this one to <b>excoriate</b> her critics: "The other side has reached a new low—attacking my family, my education and playing politics with something that is deeply personal."
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
His freshman year, in one of his first games on the Florida State basketball team, Charlie heard coach Pat Kennedy <b>excoriate</b> his players during a timeout, cursing a few times for emphasis. Charlie turned to somebody on the bench and asked, "Can't he say that without yelling?"
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Fidel would use the occasion to expatiate on the statistics of Cuban economic triumphs, or to <b>excoriate</b> the enemy across the water in the United States. By contrast, Raúl Castro's hour-long speech to a crowd of 100,000 in Camaguey contained some unusually sharp criticism of Cuba's own shortcomings.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/excoriate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='excoriate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ex_out' data-tree-url='//cdn1.membean.com/public/data/treexml' href='excoriate#'>
<span class='common'></span>
ex-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>out of, from, off</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='cori_leather' data-tree-url='//cdn1.membean.com/public/data/treexml' href='excoriate#'>
<span class=''></span>
cori
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>hide, leather, skin</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ate_quality' data-tree-url='//cdn1.membean.com/public/data/treexml' href='excoriate#'>
<span class=''></span>
-ate
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make someone have a certain quality</td>
</tr>
</table>
<p>To <em>excoriate</em> was originally to &#8220;strip off the skin,&#8221; which evolved into &#8220;stinging words that sharply criticize.&#8221;  Which would hurt more?</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='excoriate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Excoriate" src="https://cdn0.membean.com/public/images/wordimages/cons2/excoriate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='excoriate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abrasive</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>arraign</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>astringent</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>captious</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>castigate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>censure</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>decry</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>denounce</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>deprecate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>pillory</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>remonstrate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>reprobate</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>vilify</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>vituperative</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>adulation</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>emollient</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>encomium</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>eulogy</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>exculpate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>exemplary</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>exonerate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>extol</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>laudatory</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>lionize</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>paean</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>panegyric</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>plaudit</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>unscathed</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="excoriate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>excoriate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="excoriate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

