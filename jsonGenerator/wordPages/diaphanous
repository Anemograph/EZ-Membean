
<!DOCTYPE html>
<html>
<head>
<title>Word: diaphanous | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Diaphanous-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/diaphanous-large.jpg?qdep8" />
</div>
<a href="diaphanous#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx1' style='display:none'>Something ethereal has a delicate beauty that makes it seem not part of the real world.</p>
<p class='rw-defn idx2' style='display:none'>A gossamer material is very thin, light, and delicate.</p>
<p class='rw-defn idx3' style='display:none'>An impenetrable barrier cannot be gotten through by any means; this word can refer to parts of a building such as walls and doors—or to a problem of some kind that cannot be solved.</p>
<p class='rw-defn idx4' style='display:none'>Limpid speech or prose is clear, simple, and easily understood; things, such as pools of water or eyes, are limpid if they are clear or transparent.</p>
<p class='rw-defn idx5' style='display:none'>If someone is lucid, they are able to understand things and think clearly; this adjective also describes writing or speech that is crystal clear and easy to understand.</p>
<p class='rw-defn idx6' style='display:none'>If you describe something as nebulous, you mean that it is unclear, vague, and not clearly defined; a shape that is nebulous has no clear boundaries.</p>
<p class='rw-defn idx7' style='display:none'>Something is opaque if it is either difficult to understand or is not transparent.</p>
<p class='rw-defn idx8' style='display:none'>Something that is pellucid is either extremely clear because it is transparent to the eye or it is very easy for the mind to understand.</p>
<p class='rw-defn idx9' style='display:none'>When a substance permeates something, it enters into all parts of it.</p>
<p class='rw-defn idx10' style='display:none'>To rarefy something is to make it less dense, as in oxygen at high altitudes; this word also refers to purifying or refining something, thereby making it less ordinary or commonplace.</p>
<p class='rw-defn idx11' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx12' style='display:none'>A translucent object allows some light to pass through it.</p>
<p class='rw-defn idx13' style='display:none'>Something that is vaporous is not completely formed but foggy and misty; in the same vein, a vaporous idea is insubstantial and vague.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>diaphanous</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='diaphanous#' id='pronounce-sound' path='audio/words/amy-diaphanous'></a>
dahy-AF-uh-nuhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='diaphanous#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='diaphanous#' id='context-sound' path='audio/wordcontexts/brian-diaphanous'></a>
When Dinah attended the formal party at her employer&#8217;s home, many were surprised by the lavender veil that she was wearing, made from a thin, <em>diaphanous</em> material that was translucent.  When Tristan tried to converse with Dinah at the table, it was hard for him to do so, even though he could almost see right through Dinah&#8217;s <em>diaphanous</em> veil to her partially revealed face. Such finely woven and <em>diaphanous</em> material gave hints of her facial features; nevertheless, Tristan would have preferred that she remove the veil so he could see her uncovered face.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of something made from <em>diaphanous</em> material?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A very thin veil that a woman wears to cover her face on her wedding day.
</li>
<li class='choice '>
<span class='result'></span>
A shimmering outfit that a dancer performs in that changes color as they move.
</li>
<li class='choice '>
<span class='result'></span>
A thick robe of expensive material that a religious leader wears to perform certain ceremonies.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='diaphanous#' id='definition-sound' path='audio/wordmeanings/amy-diaphanous'></a>
A <em>diaphanous</em> cloth is thin enough to see through.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>filmy</em>
</span>
</span>
</div>
<a class='quick-help' href='diaphanous#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='diaphanous#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/diaphanous/memory_hooks/3595.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Aid</span></span> <span class="emp3"><span>Pou</span></span>ting <span class="emp2"><span>Hans</span></span>!</span></span> After the evil and sly witch cast a spell on handsome <span class="emp2"><span>Hans</span></span>, he became see-through; please <span class="emp1"><span>aid</span></span> <span class="emp3"><span>pou</span></span>ting  <span class="emp2"><span>Hans</span></span> in his <span class="emp1"><span>dia</span></span><span class="emp3"><span>p</span></span><span class="emp2"><span>han</span></span><span class="emp3"><span>ou</span></span><span class="emp2"><span>s</span></span> state, which is really quite too revealing.
</p>
</div>

<div id='memhook-button-bar'>
<a href="diaphanous#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/diaphanous/memory_hooks">Use other hook</a>
<a href="diaphanous#" id="memhook-use-own" url="https://membean.com/mywords/diaphanous/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='diaphanous#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Right after sunset, in the northwestern sky, Hale-Bopp’s distinctive head and <b>diaphanous</b> tail should be out in full regalia-without any need for a telescope or binoculars, and unmistakable even in light-polluted cities.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
Exactly 17 years after burrowing into the soil as larvae, the cicadas had begun to resurface spot on schedule, fully grown, shedding their brittle skins and flexing damp, <b>diaphanous</b> wings.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/diaphanous/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='diaphanous#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='dia_through' data-tree-url='//cdn1.membean.com/public/data/treexml' href='diaphanous#'>
<span class=''></span>
dia-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>through, across</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='phan_cause' data-tree-url='//cdn1.membean.com/public/data/treexml' href='diaphanous#'>
<span class=''></span>
phan
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>cause to appear, show</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ous_possessing' data-tree-url='//cdn1.membean.com/public/data/treexml' href='diaphanous#'>
<span class=''></span>
-ous
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>possessing the nature of</td>
</tr>
</table>
<p>A <em>diaphanous</em> cloth &#8220;possesses the nature of&#8221; light &#8220;showing through or across&#8221; it, &#8220;causing&#8221; what is underneath &#8220;to appear.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='diaphanous#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Diaphanous" src="https://cdn1.membean.com/public/images/wordimages/cons2/diaphanous.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='diaphanous#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>ethereal</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>gossamer</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>limpid</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>lucid</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>pellucid</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>permeate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>rarefy</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>translucent</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>vaporous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>impenetrable</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>nebulous</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>opaque</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="diaphanous" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>diaphanous</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="diaphanous#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

