
<!DOCTYPE html>
<html>
<head>
<title>Word: verdant | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Verdant-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/verdant-large.jpg?qdep8" />
</div>
<a href="verdant#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>Affluence is the state of having a lot of money and many possessions, granting one a high economic standard of living.</p>
<p class='rw-defn idx2' style='display:none'>The adjective agrarian is used to describe something that is related to farmland or the economy that is concerned with agriculture.</p>
<p class='rw-defn idx3' style='display:none'>Atrophy is a process by which parts of the body, such as muscles and organs, lessen in size or weaken in strength due to internal nerve damage, poor nutrition, or aging.</p>
<p class='rw-defn idx4' style='display:none'>Something is a blight if it spoils or damages things severely; blight is often used to describe something that ruins or kills crops.</p>
<p class='rw-defn idx5' style='display:none'>The adjective bucolic is used to describe things that are related to or characteristic of rural or country life.</p>
<p class='rw-defn idx6' style='display:none'>A copious amount of something is a very large amount of it.</p>
<p class='rw-defn idx7' style='display:none'>A cornucopia is a large quantity and variety of something good and nourishing.</p>
<p class='rw-defn idx8' style='display:none'>When there is a dearth of something, there is a scarcity or lack of it.</p>
<p class='rw-defn idx9' style='display:none'>When something is diffuse, it is widely spread out and scattered.</p>
<p class='rw-defn idx10' style='display:none'>If you show exuberance, you display great excitement, energy, and enthusiasm.</p>
<p class='rw-defn idx11' style='display:none'>If someone, such as a writer, is described as having fecundity, they have the ability to produce many original and different ideas.</p>
<p class='rw-defn idx12' style='display:none'>If something, such as an idea or plan, comes to fruition, it produces the result you wanted to achieve from it.</p>
<p class='rw-defn idx13' style='display:none'>Something luxuriant, such as plants or hair, is growing well and is very healthy.</p>
<p class='rw-defn idx14' style='display:none'>Malaise is a feeling of discontent, general unease, or even illness in a person or group for which there does not seem to be a quick and easy solution because the cause of it is not clear.</p>
<p class='rw-defn idx15' style='display:none'>Something minuscule is extremely small in size or amount.</p>
<p class='rw-defn idx16' style='display:none'>Something that is opulent is very impressive, grand, and is made from expensive materials.</p>
<p class='rw-defn idx17' style='display:none'>A panoply is a large and impressive collection of people or things.</p>
<p class='rw-defn idx18' style='display:none'>A paucity of something is not enough of it.</p>
<p class='rw-defn idx19' style='display:none'>If you suffer privation, you live without many of the basic things required for a comfortable life.</p>
<p class='rw-defn idx20' style='display:none'>Someone who behaves in a prodigal way spends a lot of money and/or time carelessly and wastefully with no concern for the future.</p>
<p class='rw-defn idx21' style='display:none'>A profusion of something is a very large quantity or variety of it.</p>
<p class='rw-defn idx22' style='display:none'>If something bad, such as crime or disease, is rampant, there is a lot of it—and it is increasing in a way that is difficult to control.</p>
<p class='rw-defn idx23' style='display:none'>If you say that something bad or unpleasant is rife somewhere, you mean that it is very common.</p>
<p class='rw-defn idx24' style='display:none'>Something that is sumptuous is impressive, grand, and very expensive.</p>
<p class='rw-defn idx25' style='display:none'>The adjective sylvan describes things that are related to forests or trees.</p>
<p class='rw-defn idx26' style='display:none'>Something that is vernal occurs in spring; since spring is the time when new plants start to grow, the adjective vernal can also be used to suggest youth and freshness.</p>
<p class='rw-defn idx27' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>verdant</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='verdant#' id='pronounce-sound' path='audio/words/amy-verdant'></a>
VUR-dnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='verdant#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='verdant#' id='context-sound' path='audio/wordcontexts/brian-verdant'></a>
The farmer celebrated the coming of spring and the resultant <em>verdant</em> pastures.  No longer looking out upon brown, lifeless fields, he rushed about the lush <em>verdant</em> land with the newborn lambs.  Eying his tractor, affectionately called &#8220;Big Red,&#8221; he knew in time he would once again tend the <em>verdant</em>, blooming land in haying season.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Which of the following is the best description of a <em>verdant</em> landscape?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A backyard in the summer with freshly mowed grass surrounded by lush vegetation.
</li>
<li class='choice '>
<span class='result'></span>
An autumn forest filled with trees ablaze with orange and red and yellow leaves.
</li>
<li class='choice '>
<span class='result'></span>
A large open field of yellow-brown grain waving and dancing in the wind.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='verdant#' id='definition-sound' path='audio/wordmeanings/amy-verdant'></a>
A place described as <em>verdant</em> is green because of all the plants and trees that grow there.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>green</em>
</span>
</span>
</div>
<a class='quick-help' href='verdant#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='verdant#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/verdant/memory_hooks/4154.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Ver</span></span>y Many <span class="emp1"><span>Ant</span></span>s <span class="emp3"><span>D</span></span><span class="emp1"><span>ance</span></span> in <span class="emp2"><span>Ver</span></span><span class="emp3"><span>d</span></span><span class="emp1"><span>ant</span></span> Forests</span></span>  <span class="emp1"><span>Ant</span></span>s <span class="emp2"><span>ver</span></span>y much love to <span class="emp3"><span>d</span></span><span class="emp1"><span>ance</span></span> about in <span class="emp2"><span>ver</span></span><span class="emp3"><span>d</span></span><span class="emp1"><span>ant</span></span> forests, climbing all over the green vegetation.
</p>
</div>

<div id='memhook-button-bar'>
<a href="verdant#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/verdant/memory_hooks">Use other hook</a>
<a href="verdant#" id="memhook-use-own" url="https://membean.com/mywords/verdant/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='verdant#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
I fell in love with Rwanda the moment I saw those <b>verdant</b>, rolling hills rise up beneath the wings of the plane as we descended toward Kigali airport.
<span class='attribution'>&mdash; Naomi Benaron, American author</span>
<img alt="Naomi benaron, american author" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Naomi Benaron, American author.jpg?qdep8" width="80" />
</li>
<li>
Fed by the heavy rains we've had lately, its lushly <b>verdant—if</b> carefully cultivated—profusion of plant life is an apt evocation of what you're about [to] see on the walls of the museum.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Spanning <b>verdant</b> national parks, mist-covered wind farms, industrial paper mills and sprawling, discarded automobile lots, his images offer a unique glimpse into the ways humans have shaped the planet.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
The cobblestones that pave the pathway through the front garden were carried up by the family from a nearby beach. A ume plum tree that Asawa planted still stands in a the <b>verdant</b> garden, now overgrown with oxalis and nasturtium.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/verdant/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='verdant#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='verd_green' data-tree-url='//cdn1.membean.com/public/data/treexml' href='verdant#'>
<span class=''></span>
verd
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>green</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ant_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='verdant#'>
<span class=''></span>
-ant
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p>A <em>verdant</em> landscape is &#8220;in a state or condition&#8221; of being &#8220;green.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='verdant#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: National Geographic: Transformation of Powai</strong><span> How to make urban environments more verdant.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/verdant.jpg' video_url='examplevids/verdant' video_width='350'></span>
<div id='wt-container'>
<img alt="Verdant" height="288" src="https://cdn1.membean.com/video/examplevids/verdant.jpg" width="350" />
<div class='center'>
<a href="verdant#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='verdant#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Verdant" src="https://cdn1.membean.com/public/images/wordimages/cons2/verdant.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='verdant#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>affluence</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>agrarian</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>bucolic</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>copious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>cornucopia</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>diffuse</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>exuberance</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>fecundity</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>fruition</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>luxuriant</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>opulent</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>panoply</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>prodigal</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>profusion</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>rampant</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>rife</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>sumptuous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>sylvan</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>vernal</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>atrophy</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>blight</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>dearth</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>malaise</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>minuscule</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>paucity</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>privation</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="verdant" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>verdant</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="verdant#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

