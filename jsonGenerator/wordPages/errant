
<!DOCTYPE html>
<html>
<head>
<title>Word: errant | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Errant-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/errant-large.jpg?qdep8" />
</div>
<a href="errant#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When you act with abandon, you give in to the impulse of the moment and behave in a wild, uncontrolled way.</p>
<p class='rw-defn idx1' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx2' style='display:none'>If something goes awry, it does not happen in the way that was planned.</p>
<p class='rw-defn idx3' style='display:none'>If you employ chicanery, you are devising and carrying out clever plans and trickery to cheat and deceive people.</p>
<p class='rw-defn idx4' style='display:none'>A circuitous route, journey, or piece of writing is long and complicated rather than simple and direct.</p>
<p class='rw-defn idx5' style='display:none'>Something that is delusive deceives you by giving a false belief about yourself or the situation you are in.</p>
<p class='rw-defn idx6' style='display:none'>If you disabuse someone of an idea or notion, you persuade them that the idea is in fact untrue.</p>
<p class='rw-defn idx7' style='display:none'>Someone is being dogmatic when they express opinions in an assertive and often overly self-important manner.</p>
<p class='rw-defn idx8' style='display:none'>An exemplary person is one that sets an ideal or praiseworthy example for others to follow.</p>
<p class='rw-defn idx9' style='display:none'>A thing or action that is expedient is useful, advantageous, or appropriate to get something accomplished.</p>
<p class='rw-defn idx10' style='display:none'>A fallacy is an idea or belief that is false.</p>
<p class='rw-defn idx11' style='display:none'>A heretic is someone who doubts or acts in opposition to commonly or generally accepted beliefs.</p>
<p class='rw-defn idx12' style='display:none'>Heterodox beliefs, ideas, or practices are different from accepted or official ones.</p>
<p class='rw-defn idx13' style='display:none'>Something illusory appears real or possible; in fact, it is neither.</p>
<p class='rw-defn idx14' style='display:none'>If you describe someone&#8217;s appearance or behavior as impeccable, you mean that it is perfect and therefore impossible to criticize.</p>
<p class='rw-defn idx15' style='display:none'>If something is infallible, it is never wrong and so is incapable of making mistakes.</p>
<p class='rw-defn idx16' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx17' style='display:none'>If you describe a situation or process as labyrinthine, you mean that it is very complicated, involved, and difficult to understand.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is meritorious is worthy of receiving recognition or is praiseworthy because of what they have accomplished.</p>
<p class='rw-defn idx19' style='display:none'>A mountebank is a smooth-talking, dishonest person who tricks and cheats people.</p>
<p class='rw-defn idx20' style='display:none'>A social norm is the standard, model, or rule by which people conduct themselves.</p>
<p class='rw-defn idx21' style='display:none'>If an object oscillates, it moves repeatedly from one point to another and then back again; if you oscillate between two moods or attitudes, you keep changing from one to the other.</p>
<p class='rw-defn idx22' style='display:none'>To be politic in a decision is to be socially wise and diplomatic.</p>
<p class='rw-defn idx23' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx24' style='display:none'>Someone who behaves in a prodigal way spends a lot of money and/or time carelessly and wastefully with no concern for the future.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is profligate is lacking in restraint, which can manifest in carelessly and wastefully using resources, such as energy or money; this kind of person can also act in an immoral way.</p>
<p class='rw-defn idx26' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx27' style='display:none'>Protocol is the rules of conduct or proper behavior that a social situation demands.</p>
<p class='rw-defn idx28' style='display:none'>If you are staid, you are set in your ways; consequently, you are settled, serious, law-abiding, conservative, and traditional—and perhaps even a tad dull.</p>
<p class='rw-defn idx29' style='display:none'>If you are stalwart, you are dependable, sturdy, and firm in what you do or promise.</p>
<p class='rw-defn idx30' style='display:none'>Something statutory, such as the power given to a governor or president, is created, established, and controlled by rules and laws; hence, it has the full force of the law behind it and must be followed.</p>
<p class='rw-defn idx31' style='display:none'>To act in an uncouth manner is to be awkward and unmannerly.</p>
<p class='rw-defn idx32' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx33' style='display:none'>The verity of something is the truth or reality of it.</p>
<p class='rw-defn idx34' style='display:none'>A wastrel is a lazy person who wastes time and money.</p>
<p class='rw-defn idx35' style='display:none'>If you waver, you cannot decide between two things because you have serious doubts about which choice is better.</p>
<p class='rw-defn idx36' style='display:none'>If you act in a wayward fashion, you are very headstrong, independent, disobedient, unpredictable, and practically unmanageable.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>errant</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='errant#' id='pronounce-sound' path='audio/words/amy-errant'></a>
ER-uhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='errant#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='errant#' id='context-sound' path='audio/wordcontexts/brian-errant'></a>
David&#8217;s parents believed him to be a bad and <em>errant</em> son: he was always getting into trouble at school.  David&#8217;s parents hoped that he might attend college, but in an <em>errant</em> and unacceptable decision David ran off and spent all the savings they had gathered for his education.  After David was arrested for stealing vehicles, his parents found their son&#8217;s <em>errant</em>, wrong behavior to be unbearable, and they hoped that prison might teach him something.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What happens when someone acts in an <em>errant</em> fashion?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They work hard to involve everyone in an activity.
</li>
<li class='choice '>
<span class='result'></span>
They are annoyingly persistent at getting what they want.
</li>
<li class='choice answer '>
<span class='result'></span>
They act in a way that is considered unacceptable.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='errant#' id='definition-sound' path='audio/wordmeanings/amy-errant'></a>
<em>Errant</em> things or people behave in a way that is unacceptable or wrong, such as a missile that travels in the wrong direction or a student who does not follow classroom rules.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>wrong</em>
</span>
</span>
</div>
<a class='quick-help' href='errant#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='errant#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/errant/memory_hooks/4322.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Ant</span></span>onio's <span class="emp3"><span>Err</span></span>ors</span></span> <span class="emp3"><span>Err</span></span><span class="emp1"><span>ant</span></span> <span class="emp1"><span>Ant</span></span>onio was always making social <span class="emp3"><span>err</span></span>ors, from crashing parties to which he was not invited to shoplifting items that he didn't even w<span class="emp1"><span>ant</span></span> or need.
</p>
</div>

<div id='memhook-button-bar'>
<a href="errant#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/errant/memory_hooks">Use other hook</a>
<a href="errant#" id="memhook-use-own" url="https://membean.com/mywords/errant/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='errant#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
As Uncle Victor had once told me long ago, a conversation is like having a catch with someone. A good partner tosses the ball directly into your glove, making it almost impossible for you to miss it; when he is on the receiving end, he catches everything sent his way, even the most <b>errant</b> and incompetent throws.
<span class='attribution'>&mdash; Paul Auster, American writer and film director, from _Moon Palace_</span>
<img alt="Paul auster, american writer and film director, from _moon palace_" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Paul Auster, American writer and film director, from _Moon Palace_.jpg?qdep8" width="80" />
</li>
<li>
Pilots who violate restricted airspace over Washington would be liable for civil fines of up to $100,000 under legislation that is to be introduced today by Republican and Democratic House leaders. Members also warned that <b>errant</b> pilots face a growing danger of being shot down by air defenses.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
A key concern at that time was the debris created by [the] Chinese satellite’s destruction—and that will also be a focus now, as the U.S. determines exactly when and under what circumstances to shoot down its [own] <b>errant</b> satellite.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
Worst of all is its policy toward Pakistan, where the administration refuses to distance the U.S. from the increasingly <b>errant</b> autocrat Pervez Musharraf.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/errant/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='errant#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='err_wander' data-tree-url='//cdn1.membean.com/public/data/treexml' href='errant#'>
<span class=''></span>
err
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>wander, make a mistake</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ant_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='errant#'>
<span class=''></span>
-ant
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p>An <em>errant</em> person is &#8220;being in a state or condition of wandering or making a mistake.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='errant#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Real Genius</strong><span> An errant laser is attacking a house.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/errant.jpg' video_url='examplevids/errant' video_width='350'></span>
<div id='wt-container'>
<img alt="Errant" height="288" src="https://cdn1.membean.com/video/examplevids/errant.jpg" width="350" />
<div class='center'>
<a href="errant#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='errant#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Errant" src="https://cdn2.membean.com/public/images/wordimages/cons2/errant.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='errant#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abandon</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>awry</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>chicanery</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>circuitous</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>delusive</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>fallacy</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>heretic</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>heterodox</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>illusory</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>labyrinthine</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>mountebank</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>oscillate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>prodigal</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>profligate</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>uncouth</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>wastrel</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>waver</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>wayward</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='6' class = 'rw-wordform notlearned'><span>disabuse</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>dogmatic</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>exemplary</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>expedient</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>impeccable</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>infallible</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>meritorious</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>norm</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>politic</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>protocol</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>staid</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>stalwart</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>statutory</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="errant" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>errant</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="errant#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

