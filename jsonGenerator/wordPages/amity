
<!DOCTYPE html>
<html>
<head>
<title>Word: amity | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx1' style='display:none'>If you have an affiliation with a group or another person, you are officially involved or connected with them.</p>
<p class='rw-defn idx2' style='display:none'>If you have animus against someone, you have a strong feeling of dislike or hatred towards them, often without a good reason or based upon your personal prejudices.</p>
<p class='rw-defn idx3' style='display:none'>Your antagonist is an opponent in a competition or battle.</p>
<p class='rw-defn idx4' style='display:none'>Someone who is cantankerous is bad-tempered and always finds things to complain about.</p>
<p class='rw-defn idx5' style='display:none'>A captious person has a fondness for catching others at fault; hence, they are overly critical and raise unwarranted objections too often.</p>
<p class='rw-defn idx6' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx7' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx8' style='display:none'>Consanguinity is the state of being related to someone else by blood or having a similar close relationship to them.</p>
<p class='rw-defn idx9' style='display:none'>When someone is contentious, they are argumentative and likely to provoke a fight.</p>
<p class='rw-defn idx10' style='display:none'>A situation or thing that is discordant does not fit in with other things; therefore, it is disagreeable, strange, or unpleasant.</p>
<p class='rw-defn idx11' style='display:none'>Dissension is a disagreement or difference of opinion among a group of people that can cause conflict.</p>
<p class='rw-defn idx12' style='display:none'>Dissonance is an unpleasant situation of opposition in which ideas or actions are not in agreement or harmony; dissonance also refers to a harsh combination of sounds.</p>
<p class='rw-defn idx13' style='display:none'>Empathy is the ability to understand how people feel because you can imagine what it is to be like them.</p>
<p class='rw-defn idx14' style='display:none'>A small group or issue that is factious is controversial and promotes strong disagreement or dissatisfaction within a larger group.</p>
<p class='rw-defn idx15' style='display:none'>If two people are incompatible, they do not get along, tend to disagree, and are unable to cooperate with one another.</p>
<p class='rw-defn idx16' style='display:none'>If someone is inimical, they are unfriendly and will likely cause you harm.</p>
<p class='rw-defn idx17' style='display:none'>A malevolent person or thing is evil due to deliberate attempts to cause harm.</p>
<p class='rw-defn idx18' style='display:none'>Malice is a type of hatred that causes a strong desire to harm others both physically and emotionally.</p>
<p class='rw-defn idx19' style='display:none'>Pandemonium is a very noisy and uncontrolled situation, especially one that is caused by a lot of angry or excited people.</p>
<p class='rw-defn idx20' style='display:none'>Polarization between two groups is a division or separation caused by a difference in opinion or conflicting views.</p>
<p class='rw-defn idx21' style='display:none'>The propinquity of a thing is its nearness in location, relationship, or similarity to another thing.</p>
<p class='rw-defn idx22' style='display:none'>If two people have established a good rapport, their connection is such that they have a good understanding of and can communicate well with one other.</p>
<p class='rw-defn idx23' style='display:none'>Rapprochement is the development of greater understanding and friendliness between two countries or groups of people after a period of unfriendly relations.</p>
<p class='rw-defn idx24' style='display:none'>Repugnance is a strong feeling of dislike for something or someone you find horrible and offensive.</p>
<p class='rw-defn idx25' style='display:none'>A schism causes a group or organization to divide into two groups as a result of differences in their aims and beliefs.</p>
<p class='rw-defn idx26' style='display:none'>A skirmish is a minor battle between small army units; it can also be a fight or argument between individuals.</p>
<p class='rw-defn idx27' style='display:none'>Strife is struggle or conflict that sometimes turns violent.</p>
<p class='rw-defn idx28' style='display:none'>A troglodyte is reclusive, severely lacking in social skills, and is out of step with current times.</p>
<p class='rw-defn idx29' style='display:none'>Doing something in unison is doing it all together at one time.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>amity</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='amity#' id='pronounce-sound' path='audio/words/amy-amity'></a>
AM-i-tee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='amity#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='amity#' id='context-sound' path='audio/wordcontexts/brian-amity'></a>
There was great <em>amity</em> between the two writers which was indicated by their friendly and affectionate letters to one another.  There was little <em>amity</em> in their respective nations&#8217; relations, however, which had been at war with each other for much of the writers&#8217; lives.  The <em>amity</em> or friendship the two authors felt towards one another may ultimately have helped the governments come to peaceful terms.  Since the nations&#8217; most famous literary citizens had such <em>amity</em> and warmth in their relationship, the leaders of the two countries appointed them as good-will ambassadors.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of <em>amity</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A feeling of obligation to support and serve an organization.
</li>
<li class='choice answer '>
<span class='result'></span>
A feeling of peace and friendship between two neighboring nations.
</li>
<li class='choice '>
<span class='result'></span>
A feeling of appreciation for someone else&#8217;s creative art.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='amity#' id='definition-sound' path='audio/wordmeanings/amy-amity'></a>
<em>Amity</em> is a peaceful friendship between two parties, particularly between two countries.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>friendship</em>
</span>
</span>
</div>
<a class='quick-help' href='amity#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='amity#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/amity/memory_hooks/5098.json'></span>
<p>
<span class="emp0"><span>The Friend and <span class="emp1"><span>a</span></span> Baseball <span class="emp2"><span>Mit</span></span>t</span></span>  I gave my best friend <span class="emp1"><span>a</span></span> baseball <span class="emp2"><span>mit</span></span>t as a sign of <span class="emp1"><span>a</span></span><span class="emp2"><span>mit</span></span>y towards him.
</p>
</div>

<div id='memhook-button-bar'>
<a href="amity#" id="add-public-hook" style="" url="https://membean.com/mywords/amity/memory_hooks">Use other public hook</a>
<a href="amity#" id="memhook-use-own" url="https://membean.com/mywords/amity/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='amity#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The spectacle of the entire membership of both Germany’s and France’s lower houses of parliament due to gather there next week in an atmosphere of <b>amity</b> should stir the hearts of everyone who desires peace and prosperity in the world.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
The uneasy <b>amity</b> between these two men is a wonderful ideal but, the way it’s played out in "Five Minutes of Heaven," it seems more symbolic than real.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
All parties concerned have found the Tersk auction to be a stable, and profitable, island of <b>amity</b> amid the shifting tides of East-West trade. Equine dealings between the U.S. and the Soviets are even beginning to transcend buying and selling.
<cite class='attribution'>
&mdash;
Time
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/amity/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='amity#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ami_friend' data-tree-url='//cdn1.membean.com/public/data/treexml' href='amity#'>
<span class=''></span>
ami
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>friend</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ty_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='amity#'>
<span class=''></span>
-ty
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state, quality</td>
</tr>
</table>
<p><em>Amity</em> is the &#8220;state or quality&#8221; of being a &#8220;friend.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='amity#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Flatland</strong><span> Carl Sagan describes the amity between an apple and a square.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/amity.jpg' video_url='examplevids/amity' video_width='350'></span>
<div id='wt-container'>
<img alt="Amity" height="288" src="https://cdn1.membean.com/video/examplevids/amity.jpg" width="350" />
<div class='center'>
<a href="amity#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='amity#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Amity" src="https://cdn1.membean.com/public/images/wordimages/cons2/amity.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='amity#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>affiliation</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>consanguinity</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>empathy</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>propinquity</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>rapport</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>rapprochement</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>unison</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>animus</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>antagonist</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>cantankerous</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>captious</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>contentious</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>discordant</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>dissension</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>dissonance</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>factious</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>incompatible</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>inimical</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>malevolent</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>malice</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>pandemonium</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>polarization</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>repugnance</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>schism</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>skirmish</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>strife</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>troglodyte</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="amity" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>amity</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="amity#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

