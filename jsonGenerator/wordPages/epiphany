
<!DOCTYPE html>
<html>
<head>
<title>Word: epiphany | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Epiphany-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/epiphany-large.jpg?qdep8" />
</div>
<a href="epiphany#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>To <em>adumbrate</em> is to describe something incompletely or to suggest future events based on limited current knowledge.</p>
<p class='rw-defn idx1' style='display:none'>Augury is the process or art of reading omens about the future through specific ceremonies.</p>
<p class='rw-defn idx2' style='display:none'>A decadent person has low moral standards and is more interested in pleasure than serious matters.</p>
<p class='rw-defn idx3' style='display:none'>When someone disinters a dead body, they dig it up; likewise, something disinterred is exposed or revealed to the public after having been hidden for some time.</p>
<p class='rw-defn idx4' style='display:none'>To divulge something is to reveal information that was once private or secret.</p>
<p class='rw-defn idx5' style='display:none'>If you elucidate something, you make it easier to understand by giving more relevant information.</p>
<p class='rw-defn idx6' style='display:none'>When you envisage something, you imagine or consider its future possibility.</p>
<p class='rw-defn idx7' style='display:none'>Exegesis is a detailed explanation or interpretation of a piece of writing, especially a religious one.</p>
<p class='rw-defn idx8' style='display:none'>To explicate an idea or plan is to make it clear by explaining it.</p>
<p class='rw-defn idx9' style='display:none'>If you fathom something complicated or mysterious, you are able to understand it.</p>
<p class='rw-defn idx10' style='display:none'>If an idea or thought is incisive, it is expressed in a penetrating and knowledgeable manner that is clear and brief; additionally, it can demonstrate impressive understanding of related ideas or thoughts.</p>
<p class='rw-defn idx11' style='display:none'>The adjective ineffable refers to something that is so impressive and beautiful that you cannot describe it in words.</p>
<p class='rw-defn idx12' style='display:none'>Something mediocre is average or ordinary in quality; it’s just OK.</p>
<p class='rw-defn idx13' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx14' style='display:none'>To obfuscate something is to make it deliberately unclear, confusing, and difficult to understand.</p>
<p class='rw-defn idx15' style='display:none'>Something is opaque if it is either difficult to understand or is not transparent.</p>
<p class='rw-defn idx16' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx17' style='display:none'>To presage a future event is to give a sign or warning that something (usually) bad is about to happen.</p>
<p class='rw-defn idx18' style='display:none'>To prognosticate is to predict or forecast something.</p>
<p class='rw-defn idx19' style='display:none'>If you are suffering from tedium, you are bored.</p>
<p class='rw-defn idx20' style='display:none'>A wastrel is a lazy person who wastes time and money.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>epiphany</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='epiphany#' id='pronounce-sound' path='audio/words/amy-epiphany'></a>
i-PIF-uh-nee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='epiphany#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='epiphany#' id='context-sound' path='audio/wordcontexts/brian-epiphany'></a>
Amira was walking down the street one morning when she experienced a sudden moment of clarity or <em>epiphany</em> about her life: she was unhappy because she chose unhappiness.  That deep <em>epiphany</em> or moment of understanding altered her life in previously unimagined ways, with the result that she became a very happy and cheerful woman.  She often joked that if someone were to draw a cartoon of that day, he or she would have to indicate her <em>epiphany</em> by drawing a lightning bolt striking from the blue.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What might someone say when they have an <em>epiphany</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
&#8220;Aha! I finally understand!&#8221;
</li>
<li class='choice '>
<span class='result'></span>
&#8220;I&#8217;ll have to try again tomorrow.&#8221;
</li>
<li class='choice '>
<span class='result'></span>
&#8220;I really wish I knew what to do.&#8221;
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='epiphany#' id='definition-sound' path='audio/wordmeanings/amy-epiphany'></a>
An <em>epiphany</em> is the moment when someone suddenly realizes or understands something of great significance or importance.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>sudden insight</em>
</span>
</span>
</div>
<a class='quick-help' href='epiphany#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='epiphany#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/epiphany/memory_hooks/3356.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Han</span></span>d<span class="emp1"><span>y</span></span> <span class="emp3"><span>Pipe</span></span></span></span> I suppose that one way to get an instant <span class="emp3"><span>epip</span></span><span class="emp1"><span>hany</span></span> is to smoke that <span class="emp1"><span>han</span></span>d<span class="emp1"><span>y</span></span> <span class="emp3"><span>pipe</span></span> of yours, but that doesn't really count.
</p>
</div>

<div id='memhook-button-bar'>
<a href="epiphany#" id="add-public-hook" style="" url="https://membean.com/mywords/epiphany/memory_hooks">Use other public hook</a>
<a href="epiphany#" id="memhook-use-own" url="https://membean.com/mywords/epiphany/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='epiphany#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
I suddenly had a little <b>epiphany</b>: all the books we own, both read and unread, are the fullest expression of self we have at our disposal.
<span class='attribution'>&mdash; Nick Hornby, English writer and lyricist, from _The Polysyllabic Spree_</span>
<img alt="Nick hornby, english writer and lyricist, from _the polysyllabic spree_" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Nick Hornby, English writer and lyricist, from _The Polysyllabic Spree_.jpg?qdep8" width="80" />
</li>
<li>
Robinson realized that her company needed to evolve. She needed to be able to generate revenue without having to put in extra time. She had an <b>epiphany</b>. Rather than provide more services, she needed to sell more digital products.
<cite class='attribution'>
&mdash;
Forbes
</cite>
</li>
<li>
We started talking and what fascinated me at the time was when they told me that Quincy has the largest Asian population per capita. I had an <b>epiphany</b>,” he said. “As an immigrant, I’ve always seen myself as a minority. . . I thought ‘Really? Me, as a majority?’ And that’s when it clicked for me.”
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
"The stars aligned for Craig, and he had this <b>epiphany</b>," explains David Rocchio, recounting his business partner’s concern about America’s future prosperity when he came into the office the next morning.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/epiphany/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='epiphany#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>epi-</td>
<td>
&rarr;
</td>
<td class='meaning'>forth</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='phan_cause' data-tree-url='//cdn1.membean.com/public/data/treexml' href='epiphany#'>
<span class=''></span>
phan
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>cause to appear, show</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='y_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='epiphany#'>
<span class=''></span>
-y
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state or condition</td>
</tr>
</table>
<p>An <em>epiphany</em> is &#8220;that which shows forth,&#8221; giving clarity and wisdom to the beholder.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='epiphany#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Epiphany" src="https://cdn3.membean.com/public/images/wordimages/cons2/epiphany.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='epiphany#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adumbrate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>augury</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>disinter</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>divulge</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>elucidate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>envisage</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>exegesis</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>explicate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>fathom</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>incisive</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>ineffable</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>presage</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>prognosticate</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>decadent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>mediocre</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>obfuscate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>opaque</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>tedium</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>wastrel</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="epiphany" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>epiphany</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="epiphany#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

