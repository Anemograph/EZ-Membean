
<!DOCTYPE html>
<html>
<head>
<title>Word: consign | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abjure a belief or a way of behaving, you state publicly that you will give it up or reject it.</p>
<p class='rw-defn idx1' style='display:none'>To abrogate an act is to end it by official and sometimes legal means.</p>
<p class='rw-defn idx2' style='display:none'>When you are in accord with someone, you are in agreement or harmony with them.</p>
<p class='rw-defn idx3' style='display:none'>An adjunct is something that is added to or joined to something else that is larger or more important.</p>
<p class='rw-defn idx4' style='display:none'>When you advocate a plan or action, you publicly push for implementing it.</p>
<p class='rw-defn idx5' style='display:none'>Something done under the aegis of an organization or person is done with their support, backing, and protection.</p>
<p class='rw-defn idx6' style='display:none'>When you act in an aloof fashion towards others, you purposely do not participate in activities with them; instead, you remain distant and detached.</p>
<p class='rw-defn idx7' style='display:none'>Amity is a peaceful friendship between two parties, particularly between two countries.</p>
<p class='rw-defn idx8' style='display:none'>Antipathy is a strong feeling of dislike or hostility towards someone or something.</p>
<p class='rw-defn idx9' style='display:none'>When you are under another person&#8217;s auspices, you are being guided and protected by them.</p>
<p class='rw-defn idx10' style='display:none'>A bastion is considered an important and effective defense of a way of life or principle; a bastion can also refer to a heavily fortified place, such as a castle.</p>
<p class='rw-defn idx11' style='display:none'>When you bolster something or someone, you offer support in a moment of need.</p>
<p class='rw-defn idx12' style='display:none'>When you buttress an argument, idea, or even a building, you make it stronger by providing support.</p>
<p class='rw-defn idx13' style='display:none'>A carapace is the protective shell on the back of some animals, such as the tortoise or crab; by extension, if you build a carapace around yourself, you adopt a behavior or attitude designed to protect yourself.</p>
<p class='rw-defn idx14' style='display:none'>A coalition is a temporary union of different political or social groups that agrees to work together to achieve a shared aim.</p>
<p class='rw-defn idx15' style='display:none'>A confidant is a close and trusted friend to whom you can tell secret matters of importance and remain assured that they will be kept safe.</p>
<p class='rw-defn idx16' style='display:none'>To corroborate something that has been said or reported means to give information or evidence that further supports it.</p>
<p class='rw-defn idx17' style='display:none'>Credence is the mental act of believing or accepting that something is true.</p>
<p class='rw-defn idx18' style='display:none'>A situation or thing that is discordant does not fit in with other things; therefore, it is disagreeable, strange, or unpleasant.</p>
<p class='rw-defn idx19' style='display:none'>An emissary is someone who acts as a representative from one government or leader to another.</p>
<p class='rw-defn idx20' style='display:none'>An entourage is a group of assistants, servants, and other people who tag along with an important person.</p>
<p class='rw-defn idx21' style='display:none'>If you expunge something, you completely remove it or do away with it.</p>
<p class='rw-defn idx22' style='display:none'>If you extirpate something, you completely destroy it because it is unpleasant or unwanted.</p>
<p class='rw-defn idx23' style='display:none'>Fidelity towards something, such as a marriage or promise, is faithfulness or loyalty towards it.</p>
<p class='rw-defn idx24' style='display:none'>A fiduciary relationship arises when one individual has the trust and confidence of another, such as a financial client; subsequently, that trustworthy person is entrusted with the management or protection of the client&#8217;s money.</p>
<p class='rw-defn idx25' style='display:none'>If you flout a rule or custom, you deliberately refuse to conform to it.</p>
<p class='rw-defn idx26' style='display:none'>If you gainsay something, you say that it is not true and therefore reject it.</p>
<p class='rw-defn idx27' style='display:none'>If someone is inimical, they are unfriendly and will likely cause you harm.</p>
<p class='rw-defn idx28' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx29' style='display:none'>An irrevocable action or decision is impossible to change, reverse, or stop.</p>
<p class='rw-defn idx30' style='display:none'>Liaison is the cooperation and exchange of information between people or organizations in order to facilitate their joint efforts; this word also refers to a person who coordinates a connection between people or organizations.</p>
<p class='rw-defn idx31' style='display:none'>A mountebank is a smooth-talking, dishonest person who tricks and cheats people.</p>
<p class='rw-defn idx32' style='display:none'>A plenipotentiary is someone who has full power to take action or make decisions on behalf of their government in a foreign country.</p>
<p class='rw-defn idx33' style='display:none'>A poseur pretends to have a certain quality or social position, usually because they want to influence others in some way.</p>
<p class='rw-defn idx34' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx35' style='display:none'>A prophylactic is used as a preventative or protective agent to keep someone free from disease or infection.</p>
<p class='rw-defn idx36' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx37' style='display:none'>If you recant, you publicly announce that your once firmly held beliefs or statements were wrong and that you no longer agree with them.</p>
<p class='rw-defn idx38' style='display:none'>If you reciprocate, you give something to someone because they have given something similar to you.</p>
<p class='rw-defn idx39' style='display:none'>If you renege on a deal, agreement, or promise, you do not do what you promised or agreed to do.</p>
<p class='rw-defn idx40' style='display:none'>If you repudiate something, you state that you do not accept or agree with it and no longer want to be connected with it in any way.</p>
<p class='rw-defn idx41' style='display:none'>When someone in power rescinds a law or agreement, they officially end it and state that it is no longer valid.</p>
<p class='rw-defn idx42' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx43' style='display:none'>A surrogate is someone who temporarily takes the place of another person, usually acting as a representative because that person is not available to or cannot carry out a task.</p>
<p class='rw-defn idx44' style='display:none'>Synergy is the extra energy or additional effectiveness gained when two groups or organizations combine their efforts instead of working separately.</p>
<p class='rw-defn idx45' style='display:none'>To be under someone&#8217;s tutelage is to be under their guidance and teaching.</p>
<p class='rw-defn idx46' style='display:none'>When you are vigilant, you are keenly watchful, careful, or attentive to a task or situation.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>consign</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='consign#' id='pronounce-sound' path='audio/words/amy-consign'></a>
kuhn-SAHYN
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='consign#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='consign#' id='context-sound' path='audio/wordcontexts/brian-consign'></a>
When I leave the country for study abroad, I have decided to <em>consign</em> or hand over my precious book collection to my English professor.  I know that she will take this duty seriously, so I have no hesitation whatsoever in <em>consigning</em> or entrusting it into her care.  I also have to <em>consign</em> or deliver my pet ferret to someone, but I don&#8217;t want to ask Professor Reames to do that duty as well!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of <em>consigning</em> something to someone?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
You ask your daughter to complete an important job as soon as she can.
</li>
<li class='choice answer '>
<span class='result'></span>
You temporarily give your card collection to a friend to watch over.
</li>
<li class='choice '>
<span class='result'></span>
You dedicate something you&#8217;ve written to your daughter.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='consign#' id='definition-sound' path='audio/wordmeanings/amy-consign'></a>
When you <em>consign</em> someone into another&#8217;s care, you sign them over or entrust them to that person&#8217;s protection.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>hand over</em>
</span>
</span>
</div>
<a class='quick-help' href='consign#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='consign#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/consign/memory_hooks/4875.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Sign</span></span> Over</span></span> When you con<span class="emp3"><span>sign</span></span> something to someone, you <span class="emp3"><span>sign</span></span> it over to her.
</p>
</div>

<div id='memhook-button-bar'>
<a href="consign#" id="add-public-hook" style="" url="https://membean.com/mywords/consign/memory_hooks">Use other public hook</a>
<a href="consign#" id="memhook-use-own" url="https://membean.com/mywords/consign/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='consign#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
[I]t was so pleasant at her time of life to be able to <b>consign</b> her single daughters to the care of their sister. . . .
<span class='attribution'>&mdash; Jane Austen, English novelist, from _Pride and Prejudice_</span>
<img alt="Jane austen, english novelist, from _pride and prejudice_" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Jane Austen, English novelist, from _Pride and Prejudice_.jpg?qdep8" width="80" />
</li>
<li>
While my son arrived on schedule, his test results did not. So he was <b>consigned</b> to the quarantine cottage.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Unable to choose whether to <b>consign</b> the trove to Sotheby's or Christie's, company president Takashi Hashiyama put the decision in the auction houses' hands: Representatives from each company would visit Maspro's Tokyo office to compete in a game of rock, paper, scissors.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
For example, the state alleges Wiener attempted to <b>consign</b> a collection of 380 artifacts owned by her mother, Doris Wiener, to the auction house Sotheby's but didn't have adequate documentation about their origin.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/consign/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='consign#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='con_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='consign#'>
<span class='common'></span>
con-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='sign_mark' data-tree-url='//cdn1.membean.com/public/data/treexml' href='consign#'>
<span class='common'></span>
sign
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>mark, seal, sign</td>
</tr>
</table>
<p>When one <em>consigns</em> someone over to someone or someplace else, one &#8220;thoroughly marks&#8221; them for or &#8220;thoroughly seals&#8221; them into that new condition.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='consign#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Harvey</strong><span> This sister is consigning her brother into the care of an institution.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/consign.jpg' video_url='examplevids/consign' video_width='350'></span>
<div id='wt-container'>
<img alt="Consign" height="288" src="https://cdn1.membean.com/video/examplevids/consign.jpg" width="350" />
<div class='center'>
<a href="consign#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='consign#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Consign" src="https://cdn3.membean.com/public/images/wordimages/cons2/consign.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='consign#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>accord</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>adjunct</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>advocate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>aegis</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>amity</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>auspices</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>bastion</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>bolster</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>buttress</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>carapace</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>coalition</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>confidant</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>corroborate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>credence</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>emissary</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>entourage</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>fidelity</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>fiduciary</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>liaison</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>plenipotentiary</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>prophylactic</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>reciprocate</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>surrogate</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>synergy</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>tutelage</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>vigilant</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abjure</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abrogate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>aloof</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>antipathy</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>discordant</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>expunge</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>extirpate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>flout</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>gainsay</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>inimical</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>irrevocable</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>mountebank</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>poseur</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>recant</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>renege</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>repudiate</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>rescind</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="consign" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>consign</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="consign#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

