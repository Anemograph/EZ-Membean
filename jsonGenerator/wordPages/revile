
<!DOCTYPE html>
<html>
<head>
<title>Word: revile | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx1' style='display:none'>The adjective auspicious describes a positive beginning of something, such as a new business, or a certain time that looks to have a good chance of success or prosperity.</p>
<p class='rw-defn idx2' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx3' style='display:none'>When you berate someone, you speak to them angrily because they have done something wrong.</p>
<p class='rw-defn idx4' style='display:none'>If you besmirch someone, you spoil their good reputation by saying bad things about them, usually things that you know are not true.</p>
<p class='rw-defn idx5' style='display:none'>Calumny consists of untrue or unfair statements about someone expressly made to hurt their reputation.</p>
<p class='rw-defn idx6' style='display:none'>If you chastise someone, you speak to them angrily or punish them for doing something wrong.</p>
<p class='rw-defn idx7' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx8' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx9' style='display:none'>To defile something holy or important is to do something to it or say something about it that is highly offensive to others.</p>
<p class='rw-defn idx10' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx11' style='display:none'>If you denounce people or actions, you criticize them severely in public because you feel strongly that they are wrong or evil.</p>
<p class='rw-defn idx12' style='display:none'>When you exalt another person, you either praise them highly or promote them to a higher position in an organization.</p>
<p class='rw-defn idx13' style='display:none'>When you pay homage to another person, you show them great admiration or respect; you might even worship them.</p>
<p class='rw-defn idx14' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx15' style='display:none'>If you hurl invective at another person, you are verbally abusing or highly criticizing them.</p>
<p class='rw-defn idx16' style='display:none'>If you malign someone, you say unpleasant things about them to damage their reputation.</p>
<p class='rw-defn idx17' style='display:none'>A misanthrope is someone who hates and mistrusts people.</p>
<p class='rw-defn idx18' style='display:none'>Philanthropy is unselfish support in the form of donating money, work, or gifts to positive social purposes; philanthropy is also overall love for humans in general.</p>
<p class='rw-defn idx19' style='display:none'>If someone is <em>pilloried</em>, they are publicly criticized or ridiculed by a lot of people, especially in the media.</p>
<p class='rw-defn idx20' style='display:none'>Something that is propitious shows favorable conditions.</p>
<p class='rw-defn idx21' style='display:none'>A reprobate has a bad character and behaves in an immoral or improper way.</p>
<p class='rw-defn idx22' style='display:none'>When you are reverent, you show a great deal of respect, admiration, or even awe for someone or something.</p>
<p class='rw-defn idx23' style='display:none'>A salubrious place or area is pleasant, clean, healthy, and comfortable to live in.</p>
<p class='rw-defn idx24' style='display:none'>A salutary experience is beneficial to you since it strengthens you in some way, although it may be unpleasant as you undergo it; this word also refers to promoting good health.</p>
<p class='rw-defn idx25' style='display:none'>Something that has a stigma is not socially acceptable; therefore, it has a strong feeling of shame or disgrace attached to it.</p>
<p class='rw-defn idx26' style='display:none'>If you traduce someone, you deliberately say hurtful and untrue things to damage their reputation.</p>
<p class='rw-defn idx27' style='display:none'>A troglodyte is reclusive, severely lacking in social skills, and is out of step with current times.</p>
<p class='rw-defn idx28' style='display:none'>Something that is unsullied is unstained and clean.</p>
<p class='rw-defn idx29' style='display:none'>If you vilify people, you write or say bad things about them in an attempt to make them unpopular.</p>
<p class='rw-defn idx30' style='display:none'>Vitriolic words are bitter, unkind, and mean-spirited.</p>
<p class='rw-defn idx31' style='display:none'>Vituperative remarks are full of hate, anger, and cruel criticism.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>revile</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='revile#' id='pronounce-sound' path='audio/words/amy-revile'></a>
ri-VAHYL
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='revile#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='revile#' id='context-sound' path='audio/wordcontexts/brian-revile'></a>
Whenever poor Peter visited his mother-in-law, she unfailingly <em>reviled</em> or harshly scolded him, telling him that he was never good enough for her daughter.  She always had verbally abused or <em>reviled</em> Peter, despite his best efforts to please and impress her.  She sharply criticized or <em>reviled</em> Peter&#8217;s every choice and action&#8212;he just couldn&#8217;t do anything right.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Why might someone be <em>reviled</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
They have committed horrible acts against other people.
</li>
<li class='choice '>
<span class='result'></span>
They have worked hard to make their community better.
</li>
<li class='choice '>
<span class='result'></span>
They have been sweating out in the sun for many hours.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='revile#' id='definition-sound' path='audio/wordmeanings/amy-revile'></a>
If something is <em>reviled</em>, it is intensely hated and criticized.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>sharply scold</em>
</span>
</span>
</div>
<a class='quick-help' href='revile#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='revile#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/revile/memory_hooks/4818.json'></span>
<p>
<span class="emp0"><span>R<span class="emp1"><span>evil</span></span>e the D<span class="emp1"><span>evil</span></span></span></span> Since the d<span class="emp1"><span>evil</span></span> isn't always such a nice guy, it would be wise to r<span class="emp1"><span>evil</span></span>e the d<span class="emp1"><span>evil</span></span> and his <span class="emp1"><span>evil</span></span> deeds.
</p>
</div>

<div id='memhook-button-bar'>
<a href="revile#" id="add-public-hook" style="" url="https://membean.com/mywords/revile/memory_hooks">Use other public hook</a>
<a href="revile#" id="memhook-use-own" url="https://membean.com/mywords/revile/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='revile#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
That is exactly what politicians of both parties try to do when they spew venomous rhetoric about their rivals. That is exactly what media figures do when they go beyond criticism of their fellow citizens and openly <b>revile</b> them. <b>Reviling</b> people you share a combat outpost with is an incredibly stupid thing to do, and public figures who imagine their nation isn’t, potentially, one huge combat outpost are deluding themselves.
<span class='attribution'>&mdash; Sebastian Junger, American journalist</span>
<img alt="Sebastian junger, american journalist" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Sebastian Junger, American journalist.jpg?qdep8" width="80" />
</li>
<li>
Purists <b>revile</b> him for his refusal to play in either the U.S. or the British Open, not to mention his use of the unsightly long putter.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Mr Correa’s victory, many fear, would push Ecuador into the club of Latin American countries with governments that <b>revile</b> the United States, erode democracy and use their oil revenues to defy economic gravity.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Every good Hungarian loves to <b>revile</b> the 1920 Treaty of Trianon, which broke up the defeated Austro-Hungarian empire, a superpower that stretched from the Adriatic coast to the Russian frontier.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/revile/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='revile#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='re_back' data-tree-url='//cdn1.membean.com/public/data/treexml' href='revile#'>
<span class='common'></span>
re-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>back, again</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='vil_mean' data-tree-url='//cdn1.membean.com/public/data/treexml' href='revile#'>
<span class=''></span>
vil
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>mean, base</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='revile#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>When one is <em>vile</em>, one is &#8220;mean&#8221; or &#8220;wicked&#8221; or &#8220;base.&#8221;  Hence to <em>revile</em> a <em>vile</em> person is to throw that &#8220;meanness or wickedness back&#8221; at him.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='revile#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Revile" src="https://cdn2.membean.com/public/images/wordimages/cons2/revile.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='revile#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>berate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>besmirch</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>calumny</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>chastise</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>defile</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>denounce</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>invective</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>malign</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>misanthrope</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>pillory</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>reprobate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>stigma</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>traduce</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>troglodyte</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>vilify</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>vitriolic</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>vituperative</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>auspicious</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>exalt</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>homage</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>philanthropy</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>propitious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>reverent</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>salubrious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>salutary</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>unsullied</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="revile" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>revile</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="revile#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

