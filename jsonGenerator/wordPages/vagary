
<!DOCTYPE html>
<html>
<head>
<title>Word: vagary | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx1' style='display:none'>If you describe something as ambiguous, you mean that it is unclear or confusing because it can be understood in multiple ways.</p>
<p class='rw-defn idx2' style='display:none'>Someone who is capricious changes their mind repeatedly or behaves in unexpected ways; a capricious series of events follows no predictable pattern.</p>
<p class='rw-defn idx3' style='display:none'>Something ephemeral, such as some insects or a sunset, lasts for only a short time or has a very short lifespan.</p>
<p class='rw-defn idx4' style='display:none'>When people equivocate, they avoid making a clear statement; they are deliberately vague in order to avoid giving a direct answer to a question.</p>
<p class='rw-defn idx5' style='display:none'>Erratic behavior is irregular, unpredictable, and unusual.</p>
<p class='rw-defn idx6' style='display:none'>Something that is evanescent lasts for only a short time before disappearing from sight or memory.</p>
<p class='rw-defn idx7' style='display:none'>Things that fluctuate vary or change often, rising or falling seemingly at random.</p>
<p class='rw-defn idx8' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is impulsive tends to do things without thinking about them ahead of time; therefore, their actions can be unpredictable.</p>
<p class='rw-defn idx10' style='display:none'>If someone leaves an indelible impression on you, it will not be forgotten; an indelible mark is permanent or impossible to erase or remove.</p>
<p class='rw-defn idx11' style='display:none'>The adjective inexorable describes a process that is impossible to stop once it has begun.</p>
<p class='rw-defn idx12' style='display:none'>Something that is interminable continues for a very long time in a boring or annoying way.</p>
<p class='rw-defn idx13' style='display:none'>An inveterate person is always doing a particular thing, especially something questionable—and they are not likely to stop doing it.</p>
<p class='rw-defn idx14' style='display:none'>Someone who is irresolute is unable to decide what to do.</p>
<p class='rw-defn idx15' style='display:none'>An irrevocable action or decision is impossible to change, reverse, or stop.</p>
<p class='rw-defn idx16' style='display:none'>Someone who is malleable is easily influenced or controlled by other people.</p>
<p class='rw-defn idx17' style='display:none'>A mercurial person&#8217;s mind or mood changes often, quickly, and unpredictably.</p>
<p class='rw-defn idx18' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx19' style='display:none'>If an object oscillates, it moves repeatedly from one point to another and then back again; if you oscillate between two moods or attitudes, you keep changing from one to the other.</p>
<p class='rw-defn idx20' style='display:none'>Something that is perennial lasts a very long time and is enduring.</p>
<p class='rw-defn idx21' style='display:none'>Someone who is pertinacious is determined to continue doing something rather than giving up—even when it gets very difficult.</p>
<p class='rw-defn idx22' style='display:none'>A person who is plastic can be easily influenced and molded by others.</p>
<p class='rw-defn idx23' style='display:none'>Something or someone that is protean is adaptable, variable, or changeable in nature.</p>
<p class='rw-defn idx24' style='display:none'>Someone who is sedulous works hard and performs tasks very carefully and thoroughly, not stopping their work until it is completely accomplished.</p>
<p class='rw-defn idx25' style='display:none'>Spontaneity is freedom to act when and how you want to, often in an unpredictable or unplanned way.</p>
<p class='rw-defn idx26' style='display:none'>If you are stalwart, you are dependable, sturdy, and firm in what you do or promise.</p>
<p class='rw-defn idx27' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx28' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx29' style='display:none'>If you are temperamental, you tend to become easily upset and experience unpredictable mood swings.</p>
<p class='rw-defn idx30' style='display:none'>Something that is temporal deals with the present and somewhat brief time of this world.</p>
<p class='rw-defn idx31' style='display:none'>To be tentative is to be hesitant or uncertain about something; an agreement or decision of this kind is likely to have changes before it reaches its final form.</p>
<p class='rw-defn idx32' style='display:none'>Something that has the quality of transience lasts for only a short time or is constantly changing.</p>
<p class='rw-defn idx33' style='display:none'>If you are unflagging while doing a task, you are untiring when working upon it and do not stop until it is finished.</p>
<p class='rw-defn idx34' style='display:none'>When someone vacillates, they go back and forth about a choice or opinion, unable to make a firm decision.</p>
<p class='rw-defn idx35' style='display:none'>Vicissitudes can be unexpected or simply normal changes and variations that happen during the course of people&#8217;s lives.</p>
<p class='rw-defn idx36' style='display:none'>Something that is volatile can change easily and vary widely.</p>
<p class='rw-defn idx37' style='display:none'>If you waver, you cannot decide between two things because you have serious doubts about which choice is better.</p>
<p class='rw-defn idx38' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>vagary</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='vagary#' id='pronounce-sound' path='audio/words/amy-vagary'></a>
VAY-guh-ree
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='vagary#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='vagary#' id='context-sound' path='audio/wordcontexts/brian-vagary'></a>
We wanted to go sailing today, but the <em><em>vagaries</em></em> of or changes in the weather made it hard to get out.  The wind was a true <em>vagary</em>, up and down and completely unpredictable.  Then there was the <em>vagary</em> of my mother&#8217;s mood: one minute she wanted to go, the next she wanted to stay home.  As my sister says, we expect <em><em>vagaries</em></em> when it comes to natural phenomena, and we should also learn to accept the same inconsistencies in human nature!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How would someone feel about <em>vagaries</em> in their health?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
They would be worried about the unexplained new symptoms they are suddenly having.
</li>
<li class='choice '>
<span class='result'></span>
They would be prepared for a slow decline after being given a certain diagnosis.
</li>
<li class='choice '>
<span class='result'></span>
They would be happy to hear their doctor say that they are still as healthy as ever.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='vagary#' id='definition-sound' path='audio/wordmeanings/amy-vagary'></a>
A <em>vagary</em> is an unpredictable or unexpected change in action or behavior.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>unpredictable change</em>
</span>
</span>
</div>
<a class='quick-help' href='vagary#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='vagary#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/vagary/memory_hooks/5184.json'></span>
<p>
<span class="emp0"><span>Ram <span class="emp1"><span>Aries</span></span> <span class="emp3"><span>Vag</span></span>ue</span></span>  My 300-pound Scottish Blackface ram's <span class="emp3"><span>vag</span></span><span class="emp1"><span>aries</span></span> are legendary; his <span class="emp3"><span>vag</span></span>ue temperament is so unpredictable that <span class="emp1"><span>Aries</span></span> can smash into me one day, and want to be petted the next.
</p>
</div>

<div id='memhook-button-bar'>
<a href="vagary#" id="add-public-hook" style="" url="https://membean.com/mywords/vagary/memory_hooks">Use other public hook</a>
<a href="vagary#" id="memhook-use-own" url="https://membean.com/mywords/vagary/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='vagary#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Each nation has different obstacles and different goals, shaped by the <b>vagaries</b> of history and of experience. Yet as I talk to young people around the world I am impressed not by the diversity but by the closeness of their goals, their desires and their concerns and their hope for the future.
<span class='attribution'>&mdash; Robert F. Kennedy, (Bobby), American politician and lawyer and former U.S. Attorney General</span>
<img alt="Robert f" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Robert F. Kennedy, (Bobby), American politician and lawyer and former U.S. Attorney General.jpg?qdep8" width="80" />
</li>
<li>
The breakdown of Charismatic in the Belmont Stakes was not caused by the complicity of humans; it was a <b>vagary</b> of nature, a happenstance of life. . . . The horse was hurt while doing what he wanted to do.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
On May 10, 2013, the Moon passed directly in front of the Sun. Normally, this would result in a total solar eclipse but for a <b>vagary</b> of orbital mechanics: It happened when the Moon was near apogee, the point in its orbit when it’s farthest from Earth. That makes the Moon look a bit smaller, so it cannot completely cover the Sun.
<cite class='attribution'>
&mdash;
Slate
</cite>
</li>
<li>
If Kiprotich’s victory was surprising, it also represented the <b>vagary</b> and possibility of running 26.2 miles. Nothing is assured in a race this long, especially in wilting heat and on a loop course loaded with so many turns that it could feel at times like a slalom run.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/vagary/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='vagary#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='vag_wander' data-tree-url='//cdn1.membean.com/public/data/treexml' href='vagary#'>
<span class=''></span>
vag
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>wander, roam, rove</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ary_relates' data-tree-url='//cdn1.membean.com/public/data/treexml' href='vagary#'>
<span class=''></span>
-ary
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>A <em>vagary</em> is a &#8220;wandering, roaming, or roving&#8221; from one thing to another.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='vagary#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Big Fish</strong><span> Since there are so many vagaries to profits in oil drilling, he decides to go to where he thinks he can make money for sure.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/vagary.jpg' video_url='examplevids/vagary' video_width='350'></span>
<div id='wt-container'>
<img alt="Vagary" height="288" src="https://cdn1.membean.com/video/examplevids/vagary.jpg" width="350" />
<div class='center'>
<a href="vagary#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='vagary#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Vagary" src="https://cdn2.membean.com/public/images/wordimages/cons2/vagary.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='vagary#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>ambiguous</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>capricious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>ephemeral</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>equivocate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>erratic</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>evanescent</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>fluctuate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>impulsive</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>irresolute</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>malleable</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>mercurial</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>oscillate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>plastic</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>protean</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>spontaneity</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>temperamental</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>temporal</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>tentative</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>transience</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>vacillate</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>vicissitude</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>volatile</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>waver</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='8' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>indelible</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>inexorable</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>interminable</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>inveterate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>irrevocable</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>perennial</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>pertinacious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>sedulous</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>stalwart</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>unflagging</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="vagary" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>vagary</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="vagary#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

