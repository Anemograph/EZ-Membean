
<!DOCTYPE html>
<html>
<head>
<title>Word: dispel | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>The process of ablation is the removal of diseased organs or harmful substances from the body, often through surgical procedure.</p>
<p class='rw-defn idx2' style='display:none'>If someone in authority abolishes a law or practice, they formally put an end to it.</p>
<p class='rw-defn idx3' style='display:none'>To abrogate an act is to end it by official and sometimes legal means.</p>
<p class='rw-defn idx4' style='display:none'>Accretion is the slow, gradual process by which new things are added and something gets bigger.</p>
<p class='rw-defn idx5' style='display:none'>If you acquiesce to something, you allow it to happen by doing what someone wants without putting up a struggle or voicing your own concerns.</p>
<p class='rw-defn idx6' style='display:none'>Something done under the aegis of an organization or person is done with their support, backing, and protection.</p>
<p class='rw-defn idx7' style='display:none'>To agglomerate a group of things is to gather them together without any noticeable order.</p>
<p class='rw-defn idx8' style='display:none'>An aggregate is the final or sum total after many different amounts or scores have been added together.</p>
<p class='rw-defn idx9' style='display:none'>You allay someone&#8217;s fear or concern by making them feel less afraid or worried.</p>
<p class='rw-defn idx10' style='display:none'>When two or more things, such as organizations, amalgamate, they combine to become one large thing.</p>
<p class='rw-defn idx11' style='display:none'>When something is annihilated, it is completely destroyed or wiped out.</p>
<p class='rw-defn idx12' style='display:none'>A bastion is considered an important and effective defense of a way of life or principle; a bastion can also refer to a heavily fortified place, such as a castle.</p>
<p class='rw-defn idx13' style='display:none'>When you beget something, you cause it to happen or create it.</p>
<p class='rw-defn idx14' style='display:none'>If something burgeons, it grows or develops quickly.</p>
<p class='rw-defn idx15' style='display:none'>A cataclysm is a violent, sudden event that causes great change and/or harm.</p>
<p class='rw-defn idx16' style='display:none'>If two or more things coalesce, they come together to form a single larger unit.</p>
<p class='rw-defn idx17' style='display:none'>A coalition is a temporary union of different political or social groups that agrees to work together to achieve a shared aim.</p>
<p class='rw-defn idx18' style='display:none'>People convene when they gather together or are called together by someone for a meeting.</p>
<p class='rw-defn idx19' style='display:none'>If you decimate something, you destroy a large part of it, reducing its size and effectiveness greatly.</p>
<p class='rw-defn idx20' style='display:none'>Someone defoliates a tree or plant by removing its leaves, usually by applying a chemical agent.</p>
<p class='rw-defn idx21' style='display:none'>Something that is deleterious has a harmful effect.</p>
<p class='rw-defn idx22' style='display:none'>A demise can be the death of someone or the slow end of something.</p>
<p class='rw-defn idx23' style='display:none'>To denude an area is to remove the plants and trees that cover it; it can also mean to make something bare.</p>
<p class='rw-defn idx24' style='display:none'>A disaffected member of a group or organization is not satisfied with it; consequently, they feel little loyalty towards it.</p>
<p class='rw-defn idx25' style='display:none'>When you dismantle something, you take it apart or destroy it piece by piece. </p>
<p class='rw-defn idx26' style='display:none'>A dispersal of something is its scattering or distribution over a wide area.</p>
<p class='rw-defn idx27' style='display:none'>When something dissipates, it either disappears because it is driven away, or it is completely used up in some way—sometimes wastefully.</p>
<p class='rw-defn idx28' style='display:none'>Dissolution is the breaking up or official end of a group, such as a couple or institution; it can also be the act of separating something into smaller components.</p>
<p class='rw-defn idx29' style='display:none'>If you divest someone of power, rights, or authority, you take those things away from them.</p>
<p class='rw-defn idx30' style='display:none'>To efface something is to erase or remove it completely from recognition or memory.</p>
<p class='rw-defn idx31' style='display:none'>When you elicit a response from someone, you draw it forth or cause it to happen by something you say or do.</p>
<p class='rw-defn idx32' style='display:none'>If something engenders a particular feeling or attitude, it causes that condition.</p>
<p class='rw-defn idx33' style='display:none'>When you eradicate something, you tear it up by the roots or remove it completely.</p>
<p class='rw-defn idx34' style='display:none'>When you excise something, you remove it by cutting it out.</p>
<p class='rw-defn idx35' style='display:none'>If you expunge something, you completely remove it or do away with it.</p>
<p class='rw-defn idx36' style='display:none'>To expurgate part of a book, play, or other text is to remove parts of it before publishing because they are considered objectionable, harmful, or offensive.</p>
<p class='rw-defn idx37' style='display:none'>If you extirpate something, you completely destroy it because it is unpleasant or unwanted.</p>
<p class='rw-defn idx38' style='display:none'>If someone, such as a writer, is described as having fecundity, they have the ability to produce many original and different ideas.</p>
<p class='rw-defn idx39' style='display:none'>Gestation is the process by which a new idea,  plan, or piece of work develops in your mind.</p>
<p class='rw-defn idx40' style='display:none'>To induce a state is to bring it about; to induce someone to do something is to persuade them to do it.</p>
<p class='rw-defn idx41' style='display:none'>To liquidate a business or company is to close it down and sell the things that belong to it in order to pay off its debts.</p>
<p class='rw-defn idx42' style='display:none'>Something luxuriant, such as plants or hair, is growing well and is very healthy.</p>
<p class='rw-defn idx43' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx44' style='display:none'>When you obliterate something, you destroy it to such an extent that there is nothing or very little of it left.</p>
<p class='rw-defn idx45' style='display:none'>A profusion of something is a very large quantity or variety of it.</p>
<p class='rw-defn idx46' style='display:none'>Something or someone that is prolific is highly fruitful and so produces a lot of something.</p>
<p class='rw-defn idx47' style='display:none'>A scourge was originally a whip used for torture and now refers to something that torments or causes serious trouble or devastation.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>dispel</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='dispel#' id='pronounce-sound' path='audio/words/amy-dispel'></a>
dih-SPEL
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='dispel#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='dispel#' id='context-sound' path='audio/wordcontexts/brian-dispel'></a>
George <em>dispelled</em> or did away with the myth that pigs are dumb creatures.  In his pig show, he proved that they are very smart, thereby <em>dispelling</em> or ridding people&#8217;s minds of the belief that they are just greedy and dirty.  His pigs did such amazing stunts and performed such incredible tricks that the notion that pigs are simply dumb creatures was completely <em>dispelled</em> or scattered into thin air.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What happens when you <em>dispel</em> something?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
You get rid of it.
</li>
<li class='choice '>
<span class='result'></span>
You prove it to be true.
</li>
<li class='choice '>
<span class='result'></span>
You find a way to keep it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='dispel#' id='definition-sound' path='audio/wordmeanings/amy-dispel'></a>
When you <em>dispel</em> a thought from your mind, you cause it to go away or disappear; when you do the same to a crowd, you cause it to scatter into different directions.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>make go away</em>
</span>
</span>
</div>
<a class='quick-help' href='dispel#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='dispel#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/dispel/memory_hooks/3218.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Dis</span></span>appear by <span class="emp3"><span>Spell</span></span></span></span> The thought was finally and unexpectedly <span class="emp1"><span>dis</span></span><span class="emp3"><span>pell</span></span>ed from my mind, almost seeming to <span class="emp1"><span>dis</span></span>appear by means of an unknown <span class="emp3"><span>spell</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="dispel#" id="add-public-hook" style="" url="https://membean.com/mywords/dispel/memory_hooks">Use other public hook</a>
<a href="dispel#" id="memhook-use-own" url="https://membean.com/mywords/dispel/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='dispel#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
My younger brother’s death in Vietnam was both sobering and cause for reflection. In _Fallen Angels_ I wanted to <b>dispel</b> the notion of war as either romantic or simplistically heroic.
<span class='attribution'>&mdash; Walter Dean Myers, American young adult author</span>
<img alt="Walter dean myers, american young adult author" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Walter Dean Myers, American young adult author.jpg?qdep8" width="80" />
</li>
<li>
Supporting Ismail’s mission to share Muslim life with the world and <b>dispel</b> stereotypes pertaining to American Muslims, the Council on American-Islamic Relations is collecting powerful photographs in its American Muslim Legacy Photo Project.
<cite class='attribution'>
&mdash;
East Bay Times
</cite>
</li>
<li>
The milk-green waters beyond the salt pans look almost glacial, but the burning hot wind, the camels and dizzying mirages <b>dispel</b> the illusion.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Local media reports said the police used tear gas to <b>dispel</b> the crowd. But the demonstrations have continued outside the building despite the cold weather.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/dispel/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='dispel#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='dis_apart' data-tree-url='//cdn1.membean.com/public/data/treexml' href='dispel#'>
<span class='common'></span>
dis-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>apart</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pel_push' data-tree-url='//cdn1.membean.com/public/data/treexml' href='dispel#'>
<span class=''></span>
pel
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>push</td>
</tr>
</table>
<p>When a fear is <em>dispelled</em>, it is &#8220;pushed apart&#8221; so that it no longer has any effect.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='dispel#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Wochit News</strong><span> McDonalds begins online campaign to dispel rumors about the ingredients in its food.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/dispel.jpg' video_url='examplevids/dispel' video_width='350'></span>
<div id='wt-container'>
<img alt="Dispel" height="288" src="https://cdn1.membean.com/video/examplevids/dispel.jpg" width="350" />
<div class='center'>
<a href="dispel#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='dispel#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Dispel" src="https://cdn0.membean.com/public/images/wordimages/cons2/dispel.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='dispel#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>ablation</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abolish</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>abrogate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>allay</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>annihilate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>cataclysm</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>decimate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>defoliate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>deleterious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>demise</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>denude</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>disaffected</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>dismantle</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>dispersal</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>dissipate</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>dissolution</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>divest</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>efface</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>eradicate</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>excise</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>expunge</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>expurgate</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>extirpate</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>liquidate</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>obliterate</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>scourge</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='4' class = 'rw-wordform notlearned'><span>accretion</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>acquiesce</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>aegis</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>agglomerate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>aggregate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>amalgamate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>bastion</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>beget</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>burgeon</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>coalesce</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>coalition</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>convene</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>elicit</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>engender</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>fecundity</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>gestation</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>induce</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>luxuriant</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>profusion</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>prolific</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="dispel" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>dispel</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="dispel#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

