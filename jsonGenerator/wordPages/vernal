
<!DOCTYPE html>
<html>
<head>
<title>Word: vernal | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Vernal-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/vernal-large.jpg?qdep8" />
</div>
<a href="vernal#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>The adjective agrarian is used to describe something that is related to farmland or the economy that is concerned with agriculture.</p>
<p class='rw-defn idx2' style='display:none'>Something antiquated is old-fashioned and not suitable for modern needs or conditions.</p>
<p class='rw-defn idx3' style='display:none'>Atrophy is a process by which parts of the body, such as muscles and organs, lessen in size or weaken in strength due to internal nerve damage, poor nutrition, or aging.</p>
<p class='rw-defn idx4' style='display:none'>Something is a blight if it spoils or damages things severely; blight is often used to describe something that ruins or kills crops.</p>
<p class='rw-defn idx5' style='display:none'>Someone who is callow is young, immature, and inexperienced; consequently, they possess little knowledge of the world.</p>
<p class='rw-defn idx6' style='display:none'>A convalescent person spends time resting to gain health and strength after a serious illness or operation.</p>
<p class='rw-defn idx7' style='display:none'>If something, such as a sound, is undergoing a crescendo, it is getting louder and louder or increasing in intensity and is about to reach its peak strength.</p>
<p class='rw-defn idx8' style='display:none'>When there is a dearth of something, there is a scarcity or lack of it.</p>
<p class='rw-defn idx9' style='display:none'>Decrepitude is the state of being very old, worn out, or very ill; therefore, something or someone is no longer in good physical condition or good health.</p>
<p class='rw-defn idx10' style='display:none'>A dilapidated building, vehicle, etc. is old, broken-down, and in very bad condition.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx12' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx13' style='display:none'>If you describe a person, group, or civilization as effete, you mean it is weak, exhausted,  powerless, unproductive, and/or corrupt.</p>
<p class='rw-defn idx14' style='display:none'>Someone who is effusive expresses happiness, pleasure, admiration, praise, etc., in an extremely enthusiastic way.</p>
<p class='rw-defn idx15' style='display:none'>If something enervates you, it makes you very tired and weak—almost to the point of collapse.</p>
<p class='rw-defn idx16' style='display:none'>If you show exuberance, you display great excitement, energy, and enthusiasm.</p>
<p class='rw-defn idx17' style='display:none'>If someone, such as a writer, is described as having fecundity, they have the ability to produce many original and different ideas.</p>
<p class='rw-defn idx18' style='display:none'>If something, such as an idea or plan, comes to fruition, it produces the result you wanted to achieve from it.</p>
<p class='rw-defn idx19' style='display:none'>Gestation is the process by which a new idea,  plan, or piece of work develops in your mind.</p>
<p class='rw-defn idx20' style='display:none'>Something inanimate is dead, motionless, or not living; therefore, it does not display the same traits as an active and alive organism.</p>
<p class='rw-defn idx21' style='display:none'>Something luxuriant, such as plants or hair, is growing well and is very healthy.</p>
<p class='rw-defn idx22' style='display:none'>If you describe something as moribund, you imply that it is no longer effective; it may also be coming to the end of its useful existence.</p>
<p class='rw-defn idx23' style='display:none'>If something is obsolescent, it is slowly becoming no longer needed because something newer or more effective has been invented.</p>
<p class='rw-defn idx24' style='display:none'>A profusion of something is a very large quantity or variety of it.</p>
<p class='rw-defn idx25' style='display:none'>When you refurbish something, you repair it to improve its appearance or ability to function.</p>
<p class='rw-defn idx26' style='display:none'>To rejuvenate someone is to make that person feel young and strong again; to rejuvenate something is to make it like new once more.</p>
<p class='rw-defn idx27' style='display:none'>If you say that something bad or unpleasant is rife somewhere, you mean that it is very common.</p>
<p class='rw-defn idx28' style='display:none'>A senescent person is becoming old and showing the effects of getting older.</p>
<p class='rw-defn idx29' style='display:none'>Something that is superannuated is so old and worn out that it is no longer working or useful.</p>
<p class='rw-defn idx30' style='display:none'>The adjective sylvan describes things that are related to forests or trees.</p>
<p class='rw-defn idx31' style='display:none'>A place described as verdant is green because of all the plants and trees that grow there.</p>
<p class='rw-defn idx32' style='display:none'>A vibrant person is lively and full of energy in a way that is exciting and attractive.</p>
<p class='rw-defn idx33' style='display:none'>If someone is described as vivacious, they are lively and have a happy, lighthearted manner.</p>
<p class='rw-defn idx34' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>
<p class='rw-defn idx35' style='display:none'>A wizened person is very old, shrunken with age, and has a lot of wrinkles on their skin.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>vernal</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='vernal#' id='pronounce-sound' path='audio/words/amy-vernal'></a>
VUR-nl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='vernal#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='vernal#' id='context-sound' path='audio/wordcontexts/brian-vernal'></a>
The coming of spring with the <em>vernal</em> equinox occurs around March 21st in the northern hemisphere.  Not only does the <em>vernal</em> or springlike influence show in plant life, but humans, too, show signs of youthfulness and renewal.  More people are seen outside enjoying the <em>vernal</em> wonders of buds on trees and animal life springing forth from seeming months of hibernation.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What would be an example of a <em>vernal</em> holiday?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Mother&#8217;s Day, since it takes place in the spring.
</li>
<li class='choice '>
<span class='result'></span>
Halloween, since it takes place in the fall.
</li>
<li class='choice '>
<span class='result'></span>
The Fourth of July, since it takes place in the summer. 
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='vernal#' id='definition-sound' path='audio/wordmeanings/amy-vernal'></a>
Something that is <em>vernal</em> occurs in spring; since spring is the time when new plants start to grow, the adjective <em>vernal</em> can also be used to suggest youth and freshness.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>springlike</em>
</span>
</span>
</div>
<a class='quick-help' href='vernal#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='vernal#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/vernal/memory_hooks/4483.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Vern</span></span>al <span class="emp2"><span>Turn</span></span></span></span> I love the <span class="emp2"><span>vern</span></span>al <span class="emp2"><span>turn</span></span> from winter to spring!
</p>
</div>

<div id='memhook-button-bar'>
<a href="vernal#" id="add-public-hook" style="" url="https://membean.com/mywords/vernal/memory_hooks">Use other public hook</a>
<a href="vernal#" id="memhook-use-own" url="https://membean.com/mywords/vernal/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='vernal#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
An unusually early spring equinox arrives late Thursday night, as days continue to grow longer as sunsets arrive later. The shifting seasons are marked by the <b>vernal</b> equinox. Equinoxes occur twice a year—once to usher in fall, and once to herald the arrival of spring.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
But it’s spring—thank God! The days are chill and blue, the leaves are their palest, sweetest green and you can smell the earth awaken. We’re bathed in lemony <b>vernal</b> light and tomorrow when the clocks go forward we’ll get a whole extra hour of it in the evenings.
<cite class='attribution'>
&mdash;
The London Times
</cite>
</li>
<li>
Spring is in the air, and the animal kingdom is on the move. <b>Vernal</b> migrations feature everything from fish and birds to big, shaggy mammals and tiny insects. . . . “It’s hard; it’s a taxing, energetically expensive journey,” Davis says. “It allows them to exploit different resources that they wouldn’t have been able to find if they’d stayed put, but a lot of animals die trying to complete migrations.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
From the gallery walls to glamorous floral-infused gowns, “Monet” sparks the <b>vernal</b> spirit. The exhibition features some 50 major works focused on the famed water lilies and bountiful gardens lovingly tended to by Monet at his achingly romantic country home in Giverny, France.
<cite class='attribution'>
&mdash;
The San Francisco Chronicle
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/vernal/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='vernal#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>vern</td>
<td>
&rarr;
</td>
<td class='meaning'>occurring in spring</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='al_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='vernal#'>
<span class=''></span>
-al
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>The adjective <em>vernal</em> means &#8220;of or relating to&#8221; that which &#8220;occurs in spring.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='vernal#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Fantasia 2000</strong><span> It's the vernal time of year!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/vernal.jpg' video_url='examplevids/vernal' video_width='350'></span>
<div id='wt-container'>
<img alt="Vernal" height="288" src="https://cdn1.membean.com/video/examplevids/vernal.jpg" width="350" />
<div class='center'>
<a href="vernal#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='vernal#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Vernal" src="https://cdn0.membean.com/public/images/wordimages/cons2/vernal.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='vernal#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>agrarian</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>callow</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>convalescent</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>crescendo</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>effusive</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>exuberance</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>fecundity</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>fruition</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>gestation</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>luxuriant</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>profusion</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>refurbish</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>rejuvenate</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>rife</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>sylvan</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>verdant</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>vibrant</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>vivacious</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>antiquated</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>atrophy</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>blight</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>dearth</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>decrepitude</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>dilapidated</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>effete</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>enervate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>inanimate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>moribund</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>obsolescent</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>senescent</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>superannuated</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>wizened</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="vernal" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>vernal</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="vernal#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

