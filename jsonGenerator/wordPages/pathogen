
<!DOCTYPE html>
<html>
<head>
<title>Word: pathogen | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Pathogen-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/pathogen-large.jpg?qdep8" />
</div>
<a href="pathogen#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>The adjective auspicious describes a positive beginning of something, such as a new business, or a certain time that looks to have a good chance of success or prosperity.</p>
<p class='rw-defn idx1' style='display:none'>Someone is baleful if they are filled with bad intent, anger, or hatred towards another person.</p>
<p class='rw-defn idx2' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx3' style='display:none'>A contagion is a disease—or the transmission of that disease—that is easily spread from one person to another through touch or the air.</p>
<p class='rw-defn idx4' style='display:none'>Something that has curative properties can be used for curing people&#8217;s illnesses.</p>
<p class='rw-defn idx5' style='display:none'>Something that is deleterious has a harmful effect.</p>
<p class='rw-defn idx6' style='display:none'>If someone is inimical, they are unfriendly and will likely cause you harm.</p>
<p class='rw-defn idx7' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx8' style='display:none'>A malevolent person or thing is evil due to deliberate attempts to cause harm.</p>
<p class='rw-defn idx9' style='display:none'>A malignant thing or person, such as a tumor or criminal, is deadly or does great harm.</p>
<p class='rw-defn idx10' style='display:none'>If something is noisome, it is extremely unpleasant, especially because it is very dirty or has an awful smell.</p>
<p class='rw-defn idx11' style='display:none'>Something that is noxious is harmful, extremely unpleasant, and often poisonous.</p>
<p class='rw-defn idx12' style='display:none'>Something that is pernicious is very harmful or evil, often in a way that is hidden or not quickly noticed.</p>
<p class='rw-defn idx13' style='display:none'>A pestilence is either an infectious disease that is deadly or an agent of some kind that is destructive or dangerous.</p>
<p class='rw-defn idx14' style='display:none'>Something that is propitious shows favorable conditions.</p>
<p class='rw-defn idx15' style='display:none'>A salubrious place or area is pleasant, clean, healthy, and comfortable to live in.</p>
<p class='rw-defn idx16' style='display:none'>A salutary experience is beneficial to you since it strengthens you in some way, although it may be unpleasant as you undergo it; this word also refers to promoting good health.</p>
<p class='rw-defn idx17' style='display:none'>To taint is to give an undesirable quality that damages a person&#8217;s reputation or otherwise spoils something.</p>
<p class='rw-defn idx18' style='display:none'>A virulent disease is very dangerous and spreads very quickly.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>pathogen</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='pathogen#' id='pronounce-sound' path='audio/words/amy-pathogen'></a>
PATH-uh-juhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='pathogen#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='pathogen#' id='context-sound' path='audio/wordcontexts/brian-pathogen'></a>
When honeybees started disappearing in North America, many wondered if there was perhaps a <em>pathogen</em> or virus that was causing the death of thousands of hives.  Some scientists now believe that a particular fungus may have been the <em>pathogen</em> that led to this diseased state of the bees.  Others believe the <em>pathogenic</em> or disease-causing source was a particular bacterium&#8212;beekeepers hope that a solution to the mystery will come soon.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>pathogen</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A doctor who specializes in skin infections.
</li>
<li class='choice answer '>
<span class='result'></span>
The virus that causes the common cold.
</li>
<li class='choice '>
<span class='result'></span>
A person who doesn&#8217;t know right from wrong.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='pathogen#' id='definition-sound' path='audio/wordmeanings/amy-pathogen'></a>
A <em>pathogen</em> is an organism, such as a bacterium or virus, that causes disease.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>disease-causing germ</em>
</span>
</span>
</div>
<a class='quick-help' href='pathogen#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='pathogen#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/pathogen/memory_hooks/6250.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Pat</span></span> the <span class="emp2"><span>Hog</span></span> to Heav<span class="emp3"><span>en</span></span></span></span> When <span class="emp1"><span>Pat</span></span> the <span class="emp2"><span>hog</span></span> caught a fatal disease from a nasty <span class="emp1"><span>pat</span></span><span class="emp2"><span>hog</span></span><span class="emp3"><span>en</span></span>, <span class="emp1"><span>Pat</span></span> went to <span class="emp2"><span>hog</span></span> heav<span class="emp3"><span>en</span></span>, and quickly.
</p>
</div>

<div id='memhook-button-bar'>
<a href="pathogen#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/pathogen/memory_hooks">Use other hook</a>
<a href="pathogen#" id="memhook-use-own" url="https://membean.com/mywords/pathogen/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='pathogen#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Written by a team of animal researchers from China’s top academies, the paper traces the epidemiological origin of the virus, provides pictures of the <b>pathogen</b> under an electron microscope and offers its genetic sequence.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
The large number of <b>pathogens</b> suggested, she said, that the bees' immune systems had been suppressed, allowing the proliferation of infections.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
The human immune system keeps a record of <b>pathogens</b> it has met before, in the form of antibodies that fight against them and then stick around for life.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Children, on the other hand, are constantly challenged by <b>pathogens</b> that are new to their bodies, and that is one reason they are more adept than adults at fending off the coronavirus.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/pathogen/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='pathogen#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='path_feeling' data-tree-url='//cdn1.membean.com/public/data/treexml' href='pathogen#'>
<span class='common'></span>
path
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>suffering, disease, feeling</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='gen_born' data-tree-url='//cdn1.membean.com/public/data/treexml' href='pathogen#'>
<span class='common'></span>
gen
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>born, produced</td>
</tr>
</table>
<p>A <em>pathogen</em> is a microscopic organism from which a &#8220;disease is born,&#8221; and consequently &#8220;suffering&#8221; can be &#8220;produced.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='pathogen#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>TV Special: From Viruses and Bacteria: The Story of the Warm Wet Spots</strong><span> One way to spread pathogens.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/pathogen.jpg' video_url='examplevids/pathogen' video_width='350'></span>
<div id='wt-container'>
<img alt="Pathogen" height="288" src="https://cdn1.membean.com/video/examplevids/pathogen.jpg" width="350" />
<div class='center'>
<a href="pathogen#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='pathogen#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Pathogen" src="https://cdn2.membean.com/public/images/wordimages/cons2/pathogen.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='pathogen#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>baleful</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>contagion</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>deleterious</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>inimical</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>malevolent</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>malignant</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>noisome</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>noxious</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>pernicious</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>pestilence</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>taint</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>virulent</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>auspicious</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>curative</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>propitious</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>salubrious</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>salutary</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="pathogen" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>pathogen</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="pathogen#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

