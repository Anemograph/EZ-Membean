
<!DOCTYPE html>
<html>
<head>
<title>Word: munificent | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Munificent-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/munificent-large.jpg?qdep8" />
</div>
<a href="munificent#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When you are acquisitive, you are driven to pursue and own wealth and possessions—often in a greedy fashion.</p>
<p class='rw-defn idx1' style='display:none'>Affluence is the state of having a lot of money and many possessions, granting one a high economic standard of living.</p>
<p class='rw-defn idx2' style='display:none'>If your behavior or manner is altruistic, you show you care more about other people and their interests than you care about yourself.</p>
<p class='rw-defn idx3' style='display:none'>Avarice is the extremely strong desire to have a lot of money and possessions.</p>
<p class='rw-defn idx4' style='display:none'>A benefaction is a charitable contribution of money or assistance that someone gives to a person or organization.</p>
<p class='rw-defn idx5' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx6' style='display:none'>Depravity is behavior that is immoral, corrupt, or evil.</p>
<p class='rw-defn idx7' style='display:none'>Depredation can be the act of destroying or robbing a village—or the overall damage that time can do to things held dear.</p>
<p class='rw-defn idx8' style='display:none'>If you refer to the enormity of a situation, problem, or event, you mean that it is very evil or morally offensive; this word has nothing to do with physical size.</p>
<p class='rw-defn idx9' style='display:none'>Largess is the generous act of giving money or presents to a large number of people.</p>
<p class='rw-defn idx10' style='display:none'>Lavish praise, giving, or a meal is rich, plentiful, or very generous; it can sometimes border on being too much.</p>
<p class='rw-defn idx11' style='display:none'>If someone exhibits magnanimity towards another, they show them kindness and noble generosity, especially after defeating them in battle or after having been treated badly by them.</p>
<p class='rw-defn idx12' style='display:none'>A mercenary person is one whose sole interest is in earning money.</p>
<p class='rw-defn idx13' style='display:none'>A misanthrope is someone who hates and mistrusts people.</p>
<p class='rw-defn idx14' style='display:none'>A notorious person is well-known by the public at large; they are usually famous for doing something bad.</p>
<p class='rw-defn idx15' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx16' style='display:none'>A parsimonious person is not willing to give or spend money.</p>
<p class='rw-defn idx17' style='display:none'>Philanthropy is unselfish support in the form of donating money, work, or gifts to positive social purposes; philanthropy is also overall love for humans in general.</p>
<p class='rw-defn idx18' style='display:none'>If you say that you&#8217;ve received a pittance, you mean that you received a small amount of something—and you know that you deserved more.</p>
<p class='rw-defn idx19' style='display:none'>When you plunder, you forcefully take or rob others of their possessions, usually during times of war, natural disaster, or other unrest.</p>
<p class='rw-defn idx20' style='display:none'>When you procure something, you obtain or get it in some fashion.</p>
<p class='rw-defn idx21' style='display:none'>When you proffer something to someone, you offer it up or hold it out to them to take.</p>
<p class='rw-defn idx22' style='display:none'>A profusion of something is a very large quantity or variety of it.</p>
<p class='rw-defn idx23' style='display:none'>To ransack a place is to thoroughly loot or rob items from it; it can also mean to look through something thoroughly by examining every bit of it.</p>
<p class='rw-defn idx24' style='display:none'>A troglodyte is reclusive, severely lacking in social skills, and is out of step with current times.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>munificent</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='munificent#' id='pronounce-sound' path='audio/words/amy-munificent'></a>
myoo-NIF-uh-suhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='munificent#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='munificent#' id='context-sound' path='audio/wordcontexts/brian-munificent'></a>
The principal at our school likes to announce when a <em>munificent</em> or highly generous supporter has made a large donation.  I&#8217;m not sure whether he is trying to acknowledge kind, <em>munificent</em> acts of giving, or whether he is attempting to shame those who do not support the school.  I would like to be more <em>munificent</em> or giving with money than I am, but I don&#8217;t have the funds to be as charitable as I&#8217;d like to be.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When is someone <em>munificent</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When they cannot give money to a cause.
</li>
<li class='choice answer '>
<span class='result'></span>
When they act in a highly giving way.
</li>
<li class='choice '>
<span class='result'></span>
When they talk publicly about others.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='munificent#' id='definition-sound' path='audio/wordmeanings/amy-munificent'></a>
A <em>munificent</em> person is extremely generous, especially with money.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>giving</em>
</span>
</span>
</div>
<a class='quick-help' href='munificent#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='munificent#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/munificent/memory_hooks/3743.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>M</span></span>o<span class="emp2"><span>n</span></span>ey S<span class="emp1"><span>u</span></span>f<span class="emp1"><span>ficient</span></span></span></span> Because of the <span class="emp2"><span>m</span></span><span class="emp1"><span>u</span></span><span class="emp2"><span>n</span></span><span class="emp1"><span>ificent</span></span> donation from Mr. Googolplex, we now have suf<span class="emp1"><span>ficient</span></span> <span class="emp2"><span>m</span></span>o<span class="emp2"><span>n</span></span>ey to build our orphanage.
</p>
</div>

<div id='memhook-button-bar'>
<a href="munificent#" id="add-public-hook" style="" url="https://membean.com/mywords/munificent/memory_hooks">Use other public hook</a>
<a href="munificent#" id="memhook-use-own" url="https://membean.com/mywords/munificent/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='munificent#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
<b>Munificent</b> (and, possibly, narcissistic) alumni have donated millions of dollars for the privilege of having their names permanently etched in stone at some of the world's finest business schools.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
The new flow of corporate bucks and now the torrent of cash from high-profile individuals like [Ted] Turner and George Soros have tended to submerge what used to be the main force in the American philanthropic world, the foundations. There are now more than 40,000 of them in this country, and they are still hugely <b>munificent</b>, but their impact and even their reputation are not what they used to be.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/munificent/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='munificent#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='mun_gift' data-tree-url='//cdn1.membean.com/public/data/treexml' href='munificent#'>
<span class=''></span>
mun
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>gift, public service</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='i_connective' data-tree-url='//cdn1.membean.com/public/data/treexml' href='munificent#'>
<span class=''></span>
-i-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>connective</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='fic_make' data-tree-url='//cdn1.membean.com/public/data/treexml' href='munificent#'>
<span class='common'></span>
fic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make, do</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ent_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='munificent#'>
<span class=''></span>
-ent
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p>Someone who is <em>munificent</em> is &#8220;in the state or condition of making gifts&#8221; or &#8220;doing public service.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='munificent#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Munificent" src="https://cdn1.membean.com/public/images/wordimages/cons2/munificent.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='munificent#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>affluence</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>altruistic</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>benefaction</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>largess</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>lavish</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>magnanimity</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>philanthropy</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>proffer</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>profusion</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>acquisitive</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>avarice</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>depravity</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>depredation</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>enormity</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>mercenary</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>misanthrope</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>notorious</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>parsimonious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>pittance</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>plunder</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>procure</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>ransack</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>troglodyte</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="munificent" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>munificent</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="munificent#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

