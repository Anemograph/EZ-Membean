
<!DOCTYPE html>
<html>
<head>
<title>Word: sanguine | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you deal with a difficult situation with aplomb, you deal with it in a confident and skillful way.</p>
<p class='rw-defn idx1' style='display:none'>The adjective auspicious describes a positive beginning of something, such as a new business, or a certain time that looks to have a good chance of success or prosperity.</p>
<p class='rw-defn idx2' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx3' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx4' style='display:none'>A choleric person becomes angry very easily.</p>
<p class='rw-defn idx5' style='display:none'>Complacent persons are too confident and relaxed because they think that they can deal with a situation easily; however, in many circumstances, this is not the case.</p>
<p class='rw-defn idx6' style='display:none'>If something daunts you, it makes you worried about dealing with it because you think it will be very difficult or dangerous.</p>
<p class='rw-defn idx7' style='display:none'>If you are despondent, you are extremely unhappy because you are in an unpleasant situation that you do not think will improve.</p>
<p class='rw-defn idx8' style='display:none'>Someone who is diffident is shy, does not want to draw notice to themselves, and is lacking in self-confidence.</p>
<p class='rw-defn idx9' style='display:none'>A disaffected member of a group or organization is not satisfied with it; consequently, they feel little loyalty towards it.</p>
<p class='rw-defn idx10' style='display:none'>When you disillusion someone, you show that a belief they hold dear is untrue—thereby disheartening and disappointing them.</p>
<p class='rw-defn idx11' style='display:none'>Something that is dolorous, such as music or news, causes mental pain and sorrow because it itself is full of grief and sorrow.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx13' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx14' style='display:none'>A fervid person has strong feelings about something, such as a humanitarian cause; therefore, they are very sincere and enthusiastic about it.</p>
<p class='rw-defn idx15' style='display:none'>Someone who is inconsolable has been so devastated by a terrible event that no one can help them feel better about it.</p>
<p class='rw-defn idx16' style='display:none'>An indomitable foe cannot be beaten.</p>
<p class='rw-defn idx17' style='display:none'>If someone is lugubrious, they are looking very sad or gloomy.</p>
<p class='rw-defn idx18' style='display:none'>A person&#8217;s morale is their current state of self-confidence, how they feel emotionally, and how motivated they are to complete tasks.</p>
<p class='rw-defn idx19' style='display:none'>Someone who is morose is unhappy, bad-tempered, and unwilling to talk very much.</p>
<p class='rw-defn idx20' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx21' style='display:none'>Someone who is phlegmatic stays calm and unemotional even in dangerous or exciting situations.</p>
<p class='rw-defn idx22' style='display:none'>Someone who is saturnine is looking miserable and sad, sometimes in a threatening or unfriendly way.</p>
<p class='rw-defn idx23' style='display:none'>Skittish persons or animals are made easily nervous or alarmed; they are likely to change behavior quickly and unpredictably.</p>
<p class='rw-defn idx24' style='display:none'>To be tentative is to be hesitant or uncertain about something; an agreement or decision of this kind is likely to have changes before it reaches its final form.</p>
<p class='rw-defn idx25' style='display:none'>To be timorous is to be fearful.</p>
<p class='rw-defn idx26' style='display:none'>Someone is tremulous when they are shaking slightly from nervousness or fear; they may also simply be afraid of something.</p>
<p class='rw-defn idx27' style='display:none'>Someone who is zealous spends a lot of time, energy, and effort to support something—  notably that of a political or religious nature—because they believe in it very strongly.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>sanguine</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='sanguine#' id='pronounce-sound' path='audio/words/amy-sanguine'></a>
SANG-gwin
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='sanguine#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='sanguine#' id='context-sound' path='audio/wordcontexts/brian-sanguine'></a>
Gertrude found her new job to be hard, but she remained <em>sanguine</em> or cheerful through every challenge.  She wanted the restaurant to give her enough experience to attend cooking school, and even though it was tough, her attitude remained hopeful, <em>sanguine</em>, and optimistic.  In fact, the only thing that got Gertrude through her job as dishwasher and assistant to the chef was her <em>sanguine</em>, rose-colored, and confident view that all would work out.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When is someone considered <em>sanguine</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When they are hardworking and reliable in the tasks that they undergo, no matter how difficult.
</li>
<li class='choice answer '>
<span class='result'></span>
When they are positive and sure about their ability to handle challenging situations.
</li>
<li class='choice '>
<span class='result'></span>
When they have big dreams for the future and are willing to work for them.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='sanguine#' id='definition-sound' path='audio/wordmeanings/amy-sanguine'></a>
If you are <em>sanguine</em> about a situation, especially a difficult one, you are confident and cheerful that everything will work out the way you want it to.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>cheerfully optimistic</em>
</span>
</span>
</div>
<a class='quick-help' href='sanguine#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='sanguine#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/sanguine/memory_hooks/3161.json'></span>
<p>
<span class="emp0"><span>Always <span class="emp1"><span>Sang</span></span> <span class="emp3"><span>Win</span></span></span></span>  Sal <span class="emp1"><span>sang</span></span> songs for the <span class="emp3"><span>win</span></span>ners of any game he watched--he was always <span class="emp1"><span>sang</span></span><span class="emp3"><span>uine</span></span> that at least one team would <span class="emp3"><span>win</span></span>.  He <span class="emp1"><span>sang</span></span> loudest of all when his favorite team played, knowing that however outmatched they were they would somehow <span class="emp3"><span>win</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="sanguine#" id="add-public-hook" style="" url="https://membean.com/mywords/sanguine/memory_hooks">Use other public hook</a>
<a href="sanguine#" id="memhook-use-own" url="https://membean.com/mywords/sanguine/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='sanguine#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Folks <b>sanguine</b> about the prospects of mobility are more likely than the pessimistic to report that they are "very happy," no matter where they fall on the economic spectrum.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
His <b>sanguine</b> spirit turns every firefly into a star.
<cite class='attribution'>
&mdash;
Sir Arthur Conan Doyle, British writer and physician, from _The Parasite_
</cite>
</li>
<li>
Harry Truman, a plainspoken man with gut instincts for what was right, forcefully began the struggle against Soviet expansionism, a challenge that Roosevelt was too <b>sanguine</b> about.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
<li>
Equally <b>sanguine</b> about the results, Huner assured this year’s election process is beyond reproach.
<cite class='attribution'>
&mdash;
Putnam County Sentinel
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/sanguine/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='sanguine#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='sanguin_blood' data-tree-url='//cdn1.membean.com/public/data/treexml' href='sanguine#'>
<span class=''></span>
sanguin
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>blood</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='sanguine#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>An abundance of the medieval humor &#8220;blood&#8221; was once thought to make someone <em>sanguine</em>, or &#8220;cheerful&#8221; and &#8220;optimistic.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='sanguine#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Sanguine" src="https://cdn3.membean.com/public/images/wordimages/cons2/sanguine.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='sanguine#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>aplomb</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>auspicious</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>complacent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>fervid</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>indomitable</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>morale</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>zealous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='4' class = 'rw-wordform notlearned'><span>choleric</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>daunt</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>despondent</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>diffident</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>disaffected</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>disillusion</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>dolorous</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>inconsolable</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>lugubrious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>morose</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>phlegmatic</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>saturnine</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>skittish</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>tentative</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>timorous</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>tremulous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="sanguine" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>sanguine</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="sanguine#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

