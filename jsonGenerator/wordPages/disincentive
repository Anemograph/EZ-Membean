
<!DOCTYPE html>
<html>
<head>
<title>Word: disincentive | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you abet someone, you help them in a criminal or illegal act.</p>
<p class='rw-defn idx1' style='display:none'>A catalyst is an agent that enacts change, such as speeding up a chemical reaction or causing an event to occur.</p>
<p class='rw-defn idx2' style='display:none'>When you are constrained, you are forced to do something or are kept from doing it.</p>
<p class='rw-defn idx3' style='display:none'>If you curtail something, you reduce or limit it.</p>
<p class='rw-defn idx4' style='display:none'>A deterrent keeps someone from doing something against you.</p>
<p class='rw-defn idx5' style='display:none'>When you dissuade someone, you try to discourage or prevent them from doing something.</p>
<p class='rw-defn idx6' style='display:none'>If something encumbers you, it makes it difficult for you to move freely or do what you want.</p>
<p class='rw-defn idx7' style='display:none'>When you expedite an action or process, you make it happen more quickly.</p>
<p class='rw-defn idx8' style='display:none'>When you facilitate something, such as an event or project, you make it easier for everyone to get it done by giving assistance.</p>
<p class='rw-defn idx9' style='display:none'>If something fetters you, it restricts you from doing something you want to do.</p>
<p class='rw-defn idx10' style='display:none'>A hindrance is an obstacle or obstruction that gets in your way and prevents you from doing something.</p>
<p class='rw-defn idx11' style='display:none'>An impediment is something that blocks or obstructs progress; it can also be a weakness or disorder, such as having difficulty with speaking.</p>
<p class='rw-defn idx12' style='display:none'>To induce a state is to bring it about; to induce someone to do something is to persuade them to do it.</p>
<p class='rw-defn idx13' style='display:none'>Something that inhibits you from doing something restricts or keeps you from doing it.</p>
<p class='rw-defn idx14' style='display:none'>A manacle is a circular, usually metallic device used to chain someone&#8217;s wrists and/or ankles together.</p>
<p class='rw-defn idx15' style='display:none'>If something, such as a road, is occluded, it has been closed off or blocked; therefore, cars are prevented from continuing on their way until the road is opened once again.</p>
<p class='rw-defn idx16' style='display:none'>To parry is to ward something off or deflect it.</p>
<p class='rw-defn idx17' style='display:none'>When you preclude something from happening, you prevent it from doing so.</p>
<p class='rw-defn idx18' style='display:none'>If someone is predisposed to something, they are made favorable or inclined to it in advance, or they are made susceptible to something, such as a disease.</p>
<p class='rw-defn idx19' style='display:none'>When you stifle someone&#8217;s creativity or inner drive, you prevent it from being expressed.</p>
<p class='rw-defn idx20' style='display:none'>A stricture is a rule or condition that imposes restrictions or limits on what you can do.</p>
<p class='rw-defn idx21' style='display:none'>When you are stultified by something, you lose interest in it because it is dull, boring, or time-consuming.</p>
<p class='rw-defn idx22' style='display:none'>Something that stymies you presents an obstacle that prevents you from doing what you need or want to do.</p>
<p class='rw-defn idx23' style='display:none'>A tether is a restraint, such as a leash, rope, or chain, that holds something in place.</p>
<p class='rw-defn idx24' style='display:none'>When you thwart someone&#8217;s ambitions, you obstruct or stop them from happening.</p>
<p class='rw-defn idx25' style='display:none'>When you trammel something or someone, you bring about limits to freedom or hinder movements or activities in some fashion.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>disincentive</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='disincentive#' id='pronounce-sound' path='audio/words/amy-disincentive'></a>
dis-in-SEN-tiv
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='disincentive#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='disincentive#' id='context-sound' path='audio/wordcontexts/brian-disincentive'></a>
There are several <em>disincentives</em> or reasons why I don&#8217;t want to have a summer job.  The first <em>disincentive</em> is the fact that I won&#8217;t be able to sit around all day watching TV, which would be a major bummer.  The second <em>disincentive</em> or factor keeping me from wanting to work is the fact that I&#8217;m lazy; I would have to put forth way too much effort if I had to work!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>disincentive</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
It is something that discourages you from doing something.
</li>
<li class='choice '>
<span class='result'></span>
It is a reminder to yourself to remember to do something.
</li>
<li class='choice '>
<span class='result'></span>
It is a sequence of events that ultimately ends in disaster.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='disincentive#' id='definition-sound' path='audio/wordmeanings/amy-disincentive'></a>
A <em>disincentive</em> to do something does not encourage you to do that thing; rather, it restrains and hinders you from doing it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>discouragement</em>
</span>
</span>
</div>
<a class='quick-help' href='disincentive#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='disincentive#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/disincentive/memory_hooks/4657.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Dis</span></span>appointing <span class="emp3"><span>Incentive</span></span>!</span></span> You call making five bucks an hour an <span class="emp3"><span>incentive</span></span> to work for you?  You've got to be kidding--that's a <span class="emp1"><span>dis</span></span><span class="emp3"><span>incentive</span></span>, or highly <span class="emp1"><span>dis</span></span>appointing <span class="emp3"><span>incentive</span></span>!
</p>
</div>

<div id='memhook-button-bar'>
<a href="disincentive#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/disincentive/memory_hooks">Use other hook</a>
<a href="disincentive#" id="memhook-use-own" url="https://membean.com/mywords/disincentive/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='disincentive#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Most of the tax hike is going to come from people like me; and I don’t like it, and do think it adds a <b>disincentive</b> to work harder.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
They believe it provides a potential <b>disincentive</b> to return to work, and some recipients can earn more from unemployment benefits than from their income.
<cite class='attribution'>
&mdash;
Forbes
</cite>
</li>
<li>
The EV will be much cheaper to run after purchase, but there’s no denying that this massive initial premium is a considerable <b>disincentive</b> to widespread early adoption.
<cite class='attribution'>
&mdash;
Forbes
</cite>
</li>
<li>
It said the "heavily subsidised" charges were a <b>disincentive</b> for clubs to take on the running of the facilities themselves through community asset transfers.
<cite class='attribution'>
&mdash;
BBC News
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/disincentive/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='disincentive#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='dis_apart' data-tree-url='//cdn1.membean.com/public/data/treexml' href='disincentive#'>
<span class='common'></span>
dis-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>apart, not, away from</td>
</tr>
<tr>
<td class='partform'>in-</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='cent_sing' data-tree-url='//cdn1.membean.com/public/data/treexml' href='disincentive#'>
<span class=''></span>
cent
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>sing</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ive_does' data-tree-url='//cdn1.membean.com/public/data/treexml' href='disincentive#'>
<span class=''></span>
-ive
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or that which does something</td>
</tr>
</table>
<p>Just as an <em>incentive</em> is a &#8220;thorough singing&#8221; to get you to do something, so too is a <em>disincentive</em> a &#8220;thorough singing&#8221; to get you &#8220;not&#8221; to do something.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='disincentive#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Kitchen Confidential</strong><span> No longer will sharing tips be a disincentive to sell more!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/disincentive.jpg' video_url='examplevids/disincentive' video_width='350'></span>
<div id='wt-container'>
<img alt="Disincentive" height="288" src="https://cdn1.membean.com/video/examplevids/disincentive.jpg" width="350" />
<div class='center'>
<a href="disincentive#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='disincentive#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Disincentive" src="https://cdn2.membean.com/public/images/wordimages/cons2/disincentive.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='disincentive#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>constrain</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>curtail</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>deterrent</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>dissuade</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>encumber</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>fetter</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>hindrance</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>impediment</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>inhibit</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>manacle</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>occlude</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>parry</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>preclude</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>stifle</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>stricture</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>stultify</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>stymie</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>tether</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>thwart</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>trammel</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abet</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>catalyst</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>expedite</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>facilitate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>induce</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>predispose</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='disincentive#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
incentive
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>encouragement; motivation</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="disincentive" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>disincentive</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="disincentive#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

