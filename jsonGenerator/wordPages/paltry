
<!DOCTYPE html>
<html>
<head>
<title>Word: paltry | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Paltry-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/paltry-large.jpg?qdep8" />
</div>
<a href="paltry#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>A bagatelle is something that is not important or of very little value or significance.</p>
<p class='rw-defn idx1' style='display:none'>A cardinal rule or quality is considered to be the most important or basic in a set of rules or qualities.</p>
<p class='rw-defn idx2' style='display:none'>Chaff is the husks of grain separated from the edible plant; it can also be useless matter in general that is cast aside.</p>
<p class='rw-defn idx3' style='display:none'>The crux of a problem or argument is the most important or difficult part of it that affects everything else.</p>
<p class='rw-defn idx4' style='display:none'>A curio is an object that is very old or unusual; thus, it is viewed as interesting.</p>
<p class='rw-defn idx5' style='display:none'>A cynosure is an object that serves as the center of attention.</p>
<p class='rw-defn idx6' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx7' style='display:none'>Something described as dross is of very poor quality and has little or no value.</p>
<p class='rw-defn idx8' style='display:none'>Something that is epochal is highly significant or important; this adjective often refers to the bringing about or marking of the beginning of a new era.</p>
<p class='rw-defn idx9' style='display:none'>An exiguous amount of something is meager, small, or limited in nature.</p>
<p class='rw-defn idx10' style='display:none'>If something is formidable, it is impressive in size, power, or skill; therefore, it can make you feel frightened, alarmed, or in awe because it is so powerful or difficult to deal with.</p>
<p class='rw-defn idx11' style='display:none'>Something frivolous is not worth taking seriously or considering because it is silly or childish.</p>
<p class='rw-defn idx12' style='display:none'>The gravity of a situation or event is its seriousness or importance.</p>
<p class='rw-defn idx13' style='display:none'>Inconsequential matters are unimportant or are of little to no significance.</p>
<p class='rw-defn idx14' style='display:none'>An indispensable item is absolutely necessary or essential—it cannot be done without.</p>
<p class='rw-defn idx15' style='display:none'>Something minuscule is extremely small in size or amount.</p>
<p class='rw-defn idx16' style='display:none'>A momentous occurrence is very important, significant, or vital in some way.</p>
<p class='rw-defn idx17' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx18' style='display:none'>A monumental event is very great, impressive, or extremely important in some way.</p>
<p class='rw-defn idx19' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx20' style='display:none'>Nominal can refer to someone who is in charge in name only, or it can refer to a very small amount of something; both are related by their relative insignificance.</p>
<p class='rw-defn idx21' style='display:none'>Something that is opulent is very impressive, grand, and is made from expensive materials.</p>
<p class='rw-defn idx22' style='display:none'>A palatial structure is grand and impressive, such as a palace or mansion.</p>
<p class='rw-defn idx23' style='display:none'>Something that is of paramount importance or significance is chief or supreme in those things.</p>
<p class='rw-defn idx24' style='display:none'>Anything picayune is unimportant, insignificant, or minor.</p>
<p class='rw-defn idx25' style='display:none'>If you say that you&#8217;ve received a pittance, you mean that you received a small amount of something—and you know that you deserved more.</p>
<p class='rw-defn idx26' style='display:none'>Something that is pivotal is more important than anything else in a situation or a system; consequently, it is the key to its success.</p>
<p class='rw-defn idx27' style='display:none'>Something predominant is the most important or the most common thing in a group.</p>
<p class='rw-defn idx28' style='display:none'>When someone exhibits profundity, they display great intellectual depth and understanding; profundity can also be the depth or complexity of something.</p>
<p class='rw-defn idx29' style='display:none'>Something prosaic is dull, boring, and ordinary.</p>
<p class='rw-defn idx30' style='display:none'>People or things that are resplendent are beautiful, bright, and impressive in appearance.</p>
<p class='rw-defn idx31' style='display:none'>A scant amount of something is a very limited or slight amount of it; hence, it is inadequate or insufficient in size or quantity.</p>
<p class='rw-defn idx32' style='display:none'>Something that is sumptuous is impressive, grand, and very expensive.</p>
<p class='rw-defn idx33' style='display:none'>If you refer to something as a trifle, you mean that it is of little or no importance or value.</p>
<p class='rw-defn idx34' style='display:none'>A trite remark or idea is so overused that it is no longer interesting or novel.</p>
<p class='rw-defn idx35' style='display:none'>Something trivial is of little value or is not important.</p>
<p class='rw-defn idx36' style='display:none'>Something uncanny is very strange, unnatural, or highly unusual.</p>
<p class='rw-defn idx37' style='display:none'>A venial fault or mistake is not very serious; therefore, it can be forgiven or excused.</p>
<p class='rw-defn idx38' style='display:none'>A watershed is a crucial event or turning point in either the history of a nation or the life of an individual that brings about a significant change.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>paltry</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='paltry#' id='pronounce-sound' path='audio/words/amy-paltry'></a>
PAWL-tree
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='paltry#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='paltry#' id='context-sound' path='audio/wordcontexts/brian-paltry'></a>
I cannot believe the <em>paltry</em> or insignificant salary that my mother received teaching, even in the late twentieth century.  The Catholic school where she had already worked for over 20 years was paying her the <em>paltry</em> or miserable salary of less than 18,000 dollars per year!  Despite the fact that the school was paying her such a <em>paltry</em> or low amount of money for working so hard, the head of the school still asked her to contribute over 10% of that salary towards the church!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What might someone say about something <em>paltry</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
I&#8217;ve never received such a thoughtful gift.
</li>
<li class='choice '>
<span class='result'></span>
This reminds me of something my grandmother used to make.
</li>
<li class='choice answer '>
<span class='result'></span>
That wasn&#8217;t worth the time it took to open it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='paltry#' id='definition-sound' path='audio/wordmeanings/amy-paltry'></a>
Something that is <em>paltry</em> is practically worthless or insignificant.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>almost worthless</em>
</span>
</span>
</div>
<a class='quick-help' href='paltry#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='paltry#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/paltry/memory_hooks/4568.json'></span>
<p>
<span class="emp0"><span>Roger D<span class="emp1"><span>altrey</span></span></span></span> Roger D<span class="emp1"><span>altrey</span></span> is no p<span class="emp1"><span>altry</span></span> rock singer, rather he's the founder of no less than The Who, one of the greatest classic rock bands of all time.  That makes D<span class="emp1"><span>altrey</span></span> not p<span class="emp1"><span>altry</span></span> at a<span class="emp1"><span>ll(try)</span></span>!
</p>
</div>

<div id='memhook-button-bar'>
<a href="paltry#" id="add-public-hook" style="" url="https://membean.com/mywords/paltry/memory_hooks">Use other public hook</a>
<a href="paltry#" id="memhook-use-own" url="https://membean.com/mywords/paltry/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='paltry#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Compared with the thousands of drownings and other injuries sustained on the water, the dozens of shark attacks seem like a <b>paltry</b> few.
<cite class='attribution'>
&mdash;
WebMD
</cite>
</li>
<li>
The Heisman [Trophy] talk is also less insistent, a consequence of an injury-plagued junior year in which White gained a relatively <b>paltry</b> 633 yards [after gaining 1,908 his sophomore year]. But his determination is unchanged.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
As often happens in class-action lawsuits, the amount of money [plaintiffs] have begun to receive in damages comes nowhere near the amount they lost. . . . One ex-Enron employee who wanted to remain anonymous says the <b>paltry</b> sum of money he will probably receive from these settlements is of no consequence to him.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
An aged man is but a <b>paltry</b> thing, / a tattered coat upon a stick, . . . .
<cite class='attribution'>
&mdash;
William Butler Yeats, Irish poet, dramatist, and prose writer, from "Sailing to Byzantium"
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/paltry/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='paltry#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p><em>Paltry</em> comes from a root word meaning &#8220;rag.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='paltry#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Paltry" src="https://cdn2.membean.com/public/images/wordimages/cons2/paltry.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='paltry#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>bagatelle</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>chaff</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>dross</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>exiguous</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>frivolous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>inconsequential</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>minuscule</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>nominal</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>picayune</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>pittance</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>prosaic</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>scant</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>trifle</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>trite</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>trivial</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>venial</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>cardinal</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>crux</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>curio</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>cynosure</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>epochal</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>formidable</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>gravity</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>indispensable</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>momentous</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>monumental</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>opulent</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>palatial</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>paramount</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>pivotal</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>predominant</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>profundity</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>resplendent</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>sumptuous</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>uncanny</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>watershed</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="paltry" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>paltry</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="paltry#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

