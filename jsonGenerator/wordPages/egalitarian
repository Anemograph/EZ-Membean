
<!DOCTYPE html>
<html>
<head>
<title>Word: egalitarian | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An autocratic person rules with complete power; consequently, they make decisions and give orders to people without asking them for their opinion or advice.</p>
<p class='rw-defn idx1' style='display:none'>An autonomous person makes their own decisions without being influenced by anyone else; an autonomous country or organization is independent and has the power to govern itself.</p>
<p class='rw-defn idx2' style='display:none'>Bourgeois members of society are from the upper middle class and are typically conservative, traditional, and materialistic.</p>
<p class='rw-defn idx3' style='display:none'>A despot is a leader who has a lot of power and uses it in a cruel and unreasonable way.</p>
<p class='rw-defn idx4' style='display:none'>Draconian rules and laws are extremely strict and harsh.</p>
<p class='rw-defn idx5' style='display:none'>An echelon is one level of status or rank in an organization.</p>
<p class='rw-defn idx6' style='display:none'>An egotistical person thinks about or is concerned with no one else other than themselves.</p>
<p class='rw-defn idx7' style='display:none'>Exclusion is the act of shutting someone out of a group or an activity.</p>
<p class='rw-defn idx8' style='display:none'>A gradation is a series of successive small differences or changes in something that add up to an overall major change; this word can also refer to a degree or step in that series of changes.</p>
<p class='rw-defn idx9' style='display:none'>Hegemony manifests when a country, group, or organization has more political control or influence than others.</p>
<p class='rw-defn idx10' style='display:none'>A hierarchy is a system of organization in a society, company, or other group in which people are divided into different ranks or levels of importance.</p>
<p class='rw-defn idx11' style='display:none'>The word homogeneous is used to describe a group that has members or parts which are similar or are all of the same type.</p>
<p class='rw-defn idx12' style='display:none'>A matriarch is an older and powerful woman who controls a family, community, or other social group.</p>
<p class='rw-defn idx13' style='display:none'>A patriarch is a male leader of a family or tribe; a patriarch can also be a man who is the founder of a group or organization.</p>
<p class='rw-defn idx14' style='display:none'>A preconception is a conceived notion that you already have, usually in the form of a bias or prejudice of some kind.</p>
<p class='rw-defn idx15' style='display:none'>A person&#8217;s stature in society is their social standing; this level of importance is determined by their position in life or their character.</p>
<p class='rw-defn idx16' style='display:none'>Strata are Earth&#8217;s layers of rock or regions of the atmosphere; they can also be the different social, cultural, and economic levels of a society.</p>
<p class='rw-defn idx17' style='display:none'>If someone subjugates a group of people or country, they conquer and bring it under control by force.</p>
<p class='rw-defn idx18' style='display:none'>Suffrage is the right of people to vote in public elections.</p>
<p class='rw-defn idx19' style='display:none'>If one thing is tantamount to another thing, it means that it is equivalent to the other.</p>
<p class='rw-defn idx20' style='display:none'>In the Middle Ages, a vassal was a worker who served a lord in exchange for land to live upon; today you are a vassal if you serve another and are largely controlled by them.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>egalitarian</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='egalitarian#' id='pronounce-sound' path='audio/words/amy-egalitarian'></a>
ee-gal-uh-TAIR-ee-uhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='egalitarian#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='egalitarian#' id='context-sound' path='audio/wordcontexts/brian-egalitarian'></a>
My father has always believed in <em>egalitarian</em> or equal status for all people in our world.  He has never understood why some nations do not treat people in an <em>egalitarian</em> or unbiased way, but rather give rights to some people, but no rights to others.  He has devoted his life to promoting an <em>egalitarian</em> or classless system, thereby giving absolutely free status to all in the economic, political, and social realms.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Which of these quotes expresses an <em>egalitarian</em> viewpoint?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
“That government is best which governs least.”—Henry David Thoreau
</li>
<li class='choice '>
<span class='result'></span>
“We sleep safely at night because rough men stand ready to visit violence on those who would harm us.”—Winston Churchill
</li>
<li class='choice answer '>
<span class='result'></span>
“Because equal rights, fair play, justice, are all like the air: we all have it, or none of us has it.”—Maya Angelou
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='egalitarian#' id='definition-sound' path='audio/wordmeanings/amy-egalitarian'></a>
An <em>egalitarian</em> social position states that all people should be equal or treated in the same way when it comes to economic, political, and social rights.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>of equal treatment</em>
</span>
</span>
</div>
<a class='quick-help' href='egalitarian#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='egalitarian#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/egalitarian/memory_hooks/4506.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Equal</span></span> and Human<span class="emp2"><span>itarian</span></span></span></span> An <span class="emp3"><span>egal</span></span><span class="emp2"><span>itarian</span></span> system makes all things <span class="emp3"><span>equal</span></span> for all people, who are therefore all treated in a human<span class="emp2"><span>itarian</span></span> way.
</p>
</div>

<div id='memhook-button-bar'>
<a href="egalitarian#" id="add-public-hook" style="" url="https://membean.com/mywords/egalitarian/memory_hooks">Use other public hook</a>
<a href="egalitarian#" id="memhook-use-own" url="https://membean.com/mywords/egalitarian/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='egalitarian#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
"[Washington, D.C.] was built around the idea that every citizen was equally important," Berg says. "The Mall was designed as open to all comers . . . It's a very sort of <b>egalitarian</b> idea."
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
"Young children are rarely rewarded for individual achievement. There is an extremely <b>egalitarian</b> culture in their school life. But as they get older they are exposed to more meritocratic institutions, and that might change their views on equality," he says.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
<li>
Mothers can inspire their daughters to achieve and mothers have a unique influence on their sons’ attitudes to gender equality. Taking steps to be <b>egalitarian</b> role models, reducing and renegotiating household labor, and being mindful of gender bias in children’s books and toys can all make a difference.
<cite class='attribution'>
&mdash;
Harvard Business Review
</cite>
</li>
<li>
Cyberspace is—or can be—a good, friendly and <b>egalitarian</b> place to meet.
<cite class='attribution'>
&mdash;
Douglas Adams, English writer, author of _The Hitchhiker’s Guide to the Galaxy_
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/egalitarian/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='egalitarian#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='equ_equal' data-tree-url='//cdn1.membean.com/public/data/treexml' href='egalitarian#'>
<span class=''></span>
equ
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>equal</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='arian_pertaining' data-tree-url='//cdn1.membean.com/public/data/treexml' href='egalitarian#'>
<span class=''></span>
-arian
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>pertaining to</td>
</tr>
</table>
<p><em>Egalitarian</em> concepts promote rights being &#8220;equal&#8221; for all people.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='egalitarian#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Egalitarian" src="https://cdn3.membean.com/public/images/wordimages/cons2/egalitarian.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='egalitarian#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>autonomous</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bourgeois</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>echelon</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>homogeneous</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>suffrage</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>tantamount</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>autocratic</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>despot</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>draconian</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>egotistical</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>exclusion</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>gradation</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>hegemony</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>hierarchy</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>matriarch</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>patriarch</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>preconception</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>stature</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>strata</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>subjugate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>vassal</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="egalitarian" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>egalitarian</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="egalitarian#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

