
<!DOCTYPE html>
<html>
<head>
<title>Word: vignette | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Vignette-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/vignette-large.jpg?qdep8" />
</div>
<a href="vignette#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>If you abridge a book or play, you make it shorter by making cuts to the original.</p>
<p class='rw-defn idx1' style='display:none'>If one&#8217;s powers or rights are circumscribed, they are limited or restricted.</p>
<p class='rw-defn idx2' style='display:none'>If you delineate something, such as an idea or situation or border, you describe it in great detail.</p>
<p class='rw-defn idx3' style='display:none'>A piece of writing is discursive if it includes a lot of information that is not relevant to the main subject.</p>
<p class='rw-defn idx4' style='display:none'>An evocation of something creates or summons a clear mental image or impression of it through words, pictures, or music.</p>
<p class='rw-defn idx5' style='display:none'>To expatiate upon a subject is to speak or write in detail and at length about it.</p>
<p class='rw-defn idx6' style='display:none'>A garrulous person talks a lot, especially about unimportant things.</p>
<p class='rw-defn idx7' style='display:none'>A person who is being laconic uses very few words to say something.</p>
<p class='rw-defn idx8' style='display:none'>Someone who is loquacious talks too much.</p>
<p class='rw-defn idx9' style='display:none'>A pithy statement or piece of writing is brief but intelligent; it is also precise and to the point.</p>
<p class='rw-defn idx10' style='display:none'>Prattle is mindless chatter that seems like it will never stop.</p>
<p class='rw-defn idx11' style='display:none'>Something that is prolix, such as a lecture or speech, is excessively wordy; consequently, it can be tiresome to read or listen to.</p>
<p class='rw-defn idx12' style='display:none'>If someone is sententious, they are terse or brief in writing and speech; they also often use proverbs to appear morally upright and wise.</p>
<p class='rw-defn idx13' style='display:none'>Something that is succinct is clearly and briefly explained without using any unnecessary words.</p>
<p class='rw-defn idx14' style='display:none'>Something that is superfluous is unnecessary; it is more than what is wanted or needed at the current time.</p>
<p class='rw-defn idx15' style='display:none'>To be terse in speech is to be short and to the point, often in an abrupt manner that may seem unfriendly.</p>
<p class='rw-defn idx16' style='display:none'>Verbiage is an excessive use of words to convey something that could be expressed using fewer words; it can also be the manner or style in which someone uses words.</p>
<p class='rw-defn idx17' style='display:none'>Something that is verbose, such as a speech or article, contains too many words.</p>
<p class='rw-defn idx18' style='display:none'>A vibrant person is lively and full of energy in a way that is exciting and attractive.</p>
<p class='rw-defn idx19' style='display:none'>A voluble person can speak easily with speech that naturally flows; voluble people also tend to be quite chatty.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>vignette</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='vignette#' id='pronounce-sound' path='audio/words/amy-vignette'></a>
vin-YET
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='vignette#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='vignette#' id='context-sound' path='audio/wordcontexts/brian-vignette'></a>
The author used <em>vignettes</em>, or small pictures and brief, detailed sketches, to further interest in the mystery.  Clues were hidden in these small pictorial and written sketches at the beginning of each chapter, and each <em>vignette</em> revealed an upcoming event in the story.  By keeping the reader interested with these descriptive scenes and <em>vignettes</em>, the author&#8217;s artistic talent, as well as his writing ability, was showcased beautifully.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is the purpose of a <em>vignette</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
To provide the final clue that is needed to solve a mystery in a book. 
</li>
<li class='choice answer '>
<span class='result'></span>
To focus on a single situation or character for a brief time. 
</li>
<li class='choice '>
<span class='result'></span>
To outline the plot of a story to help an author before they start writing.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='vignette#' id='definition-sound' path='audio/wordmeanings/amy-vignette'></a>
A <em>vignette</em> is a picture or a short description in a book that shows the typical features of a person or situation that it represents.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>literary sketch</em>
</span>
</span>
</div>
<a class='quick-help' href='vignette#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='vignette#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/vignette/memory_hooks/4544.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>V</span></span><span class="emp3"><span>ign</span></span><span class="emp1"><span>ette</span></span> S<span class="emp3"><span>ign</span></span> of Cor<span class="emp1"><span>vette</span></span></span></span>  A <span class="emp1"><span>v</span></span><span class="emp3"><span>ign</span></span><span class="emp1"><span>ette</span></span> of a Cor<span class="emp1"><span>vette</span></span> sports car was the chief s<span class="emp3"><span>ign</span></span> of the main character, a race car driver.
</p>
</div>

<div id='memhook-button-bar'>
<a href="vignette#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/vignette/memory_hooks">Use other hook</a>
<a href="vignette#" id="memhook-use-own" url="https://membean.com/mywords/vignette/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='vignette#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Remember: a story is not a <b>vignette</b>. It has a beginning, middle and an end. It is not merely a snapshot in time.
<span class='attribution'>&mdash; Chuck Wendig, American author, comic book writer, and blogger</span>
<img alt="Chuck wendig, american author, comic book writer, and blogger" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Chuck Wendig, American author, comic book writer, and blogger.jpg?qdep8" width="80" />
</li>
<li>
This is a <b>vignette</b> not from the dying days of some despotic East European country, but from a day this September in Male, capital of the Maldives, a tropical paradise.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Instead, this appears to be the final <b>vignette</b> for the Flying Dutchman: He hopped aboard a golf cart, waved to the fans and drove away from the Indy 500 at considerably slower speed than he should have.
<cite class='attribution'>
&mdash;
CBS News
</cite>
</li>
<li>
One of the things that I fell in love with right off the bat was this graffiti writer called Revs, who during the 1990s spent six years writing his autobiography on the walls of subway tunnels, . . . he would go down and put up a five by seven foot rectangle of white or yellow paint and then write a little <b>vignette</b> from his childhood on the panel, and he would number them within a series and then sign it.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/vignette/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='vignette#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='vin_wine' data-tree-url='//cdn1.membean.com/public/data/treexml' href='vignette#'>
<span class=''></span>
vin
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>wine, grapes used to make wine</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ette_little' data-tree-url='//cdn1.membean.com/public/data/treexml' href='vignette#'>
<span class=''></span>
-ette
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>little</td>
</tr>
</table>
<p>One definition of <em>vignette</em> is a decorative border on a page in a book, often illustrated with vine leaves (from which grow &#8220;wine grapes&#8221;); a <em>vignette</em>, or &#8220;little,&#8221; literary sketch, is the contents or words which lie within the decorated borders of that single page.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='vignette#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Monty Python's Flying Circus</strong><span> They get in trouble for performing their vignette.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/vignette.jpg' video_url='examplevids/vignette' video_width='350'></span>
<div id='wt-container'>
<img alt="Vignette" height="288" src="https://cdn1.membean.com/video/examplevids/vignette.jpg" width="350" />
<div class='center'>
<a href="vignette#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='vignette#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Vignette" src="https://cdn3.membean.com/public/images/wordimages/cons2/vignette.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='vignette#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abridge</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>circumscribe</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>delineate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>evocation</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>laconic</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>pithy</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>sententious</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>succinct</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>terse</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>verbiage</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>vibrant</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>discursive</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>expatiate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>garrulous</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>loquacious</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>prattle</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>prolix</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>superfluous</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>verbose</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>voluble</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="vignette" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>vignette</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="vignette#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

