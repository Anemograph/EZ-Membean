
<!DOCTYPE html>
<html>
<head>
<title>Word: concede | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If someone abdicates, they give up their responsibility for something, such as a king&#8217;s transfer of power when he gives up his throne.</p>
<p class='rw-defn idx1' style='display:none'>If you abjure a belief or a way of behaving, you state publicly that you will give it up or reject it.</p>
<p class='rw-defn idx2' style='display:none'>The abnegation of something is someone&#8217;s giving up their rights or claim to it or denying themselves of it; this action may or may not be in their best interest.</p>
<p class='rw-defn idx3' style='display:none'>When you accede to a demand or proposal, you agree to it, especially after first disagreeing with it.</p>
<p class='rw-defn idx4' style='display:none'>When you are in accord with someone, you are in agreement or harmony with them.</p>
<p class='rw-defn idx5' style='display:none'>If you acquiesce to something, you allow it to happen by doing what someone wants without putting up a struggle or voicing your own concerns.</p>
<p class='rw-defn idx6' style='display:none'>If you are amenable to doing something, you willingly accept it without arguing.</p>
<p class='rw-defn idx7' style='display:none'>Antipathy is a strong feeling of dislike or hostility towards someone or something.</p>
<p class='rw-defn idx8' style='display:none'>If you appropriate something that does not belong to you, you take it for yourself without the right to do so.</p>
<p class='rw-defn idx9' style='display:none'>When you arrogate something, such as a position or privilege, you take it even though you don&#8217;t have the legal right to it.</p>
<p class='rw-defn idx10' style='display:none'>When you capitulate, you accept or agree to do something after having resisted doing so for a long time.</p>
<p class='rw-defn idx11' style='display:none'>Someone cedes land or power to someone else by giving it to them, often because of political or military pressure.</p>
<p class='rw-defn idx12' style='display:none'>If you condone someone&#8217;s behavior, you go along with it and provide silent support for it—despite having doubts about it.</p>
<p class='rw-defn idx13' style='display:none'>If someone will countenance something, they will approve, tolerate, or support it.</p>
<p class='rw-defn idx14' style='display:none'>If you behave with deference towards someone, you show them respect and accept their opinions or decisions, especially because they have an important position.</p>
<p class='rw-defn idx15' style='display:none'>If you demur, you delay in doing or mildly object to something because you don&#8217;t really want to do it.</p>
<p class='rw-defn idx16' style='display:none'>If you describe someone, usually a young woman, as demure, you mean that she is quiet, shy, and always behaves modestly.</p>
<p class='rw-defn idx17' style='display:none'>An animal or person that is docile is not aggressive; rather, they are well-behaved, quiet, and easy to control.</p>
<p class='rw-defn idx18' style='display:none'>If you expropriate something, you take it away for your own use although it does not belong to you; governments frequently expropriate private land to use for public purposes.</p>
<p class='rw-defn idx19' style='display:none'>Someone who is incorrigible has bad habits or does bad things and is unlikely to ever change; this word is often used in a humorous way.</p>
<p class='rw-defn idx20' style='display:none'>If people try to ingratiate themselves with you, they try to get your approval by doing or saying things that they think will please you.</p>
<p class='rw-defn idx21' style='display:none'>Intractable problems, situations, or people are very difficult or impossible to deal with.</p>
<p class='rw-defn idx22' style='display:none'>An intransigent person is stubborn; thus, they refuse to change their ideas or behavior, often in a way that seems unreasonable.</p>
<p class='rw-defn idx23' style='display:none'>Someone who is malleable is easily influenced or controlled by other people.</p>
<p class='rw-defn idx24' style='display:none'>Someone who is obdurate is stubborn, unreasonable, or uncontrollable; hence, they simply refuse to change their behavior, actions, or beliefs to fit any situation whatsoever.</p>
<p class='rw-defn idx25' style='display:none'>Obeisance is respect and obedience shown to someone or something, expressed by bowing or some other humble gesture.</p>
<p class='rw-defn idx26' style='display:none'>Someone who is obstreperous is noisy, unruly, and difficult to control.</p>
<p class='rw-defn idx27' style='display:none'>To parry is to ward something off or deflect it.</p>
<p class='rw-defn idx28' style='display:none'>To propitiate another is to calm or soothe them; it is often giving someone what they want so that everyone is happy.</p>
<p class='rw-defn idx29' style='display:none'>When someone proscribes an activity, they prohibit it by establishing a rule against it.</p>
<p class='rw-defn idx30' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx31' style='display:none'>A recalcitrant animal or person is difficult to control and refuses to obey orders—even after stiff punishment.</p>
<p class='rw-defn idx32' style='display:none'>Refractory people deliberately don&#8217;t obey someone in authority and so are difficult to deal with or control.</p>
<p class='rw-defn idx33' style='display:none'>When you relinquish something, you give it up or let it go.</p>
<p class='rw-defn idx34' style='display:none'>A reprobate has a bad character and behaves in an immoral or improper way.</p>
<p class='rw-defn idx35' style='display:none'>If you repudiate something, you state that you do not accept or agree with it and no longer want to be connected with it in any way.</p>
<p class='rw-defn idx36' style='display:none'>If you say that someone is servile, you don&#8217;t respect them because they are too obedient, too agreeable, and too willing to do anything for another person.</p>
<p class='rw-defn idx37' style='display:none'>If someone subjugates a group of people or country, they conquer and bring it under control by force.</p>
<p class='rw-defn idx38' style='display:none'>If you are subservient, you are too eager and willing to do what other people want and often put your own wishes aside.</p>
<p class='rw-defn idx39' style='display:none'>When you usurp someone else&#8217;s power, position, or role, you take it from them although you do not have the right to do so.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>concede</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='concede#' id='pronounce-sound' path='audio/words/amy-concede'></a>
kuhn-SEED
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='concede#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='concede#' id='context-sound' path='audio/wordcontexts/brian-concede'></a>
I did not want to <em>concede</em> or acknowledge the fact that the love of my life was going to marry someone else.  I thought that if I <em>conceded</em> or admitted to this fact, my heart would break into so many pieces that I would never be able to put it back together.  My friends said it would be healthy to <em>concede</em> or give in to this fact, but when I did my heart did, indeed, shatter.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is one way to <em>concede</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
You admit that you have lost.
</li>
<li class='choice '>
<span class='result'></span>
You start a fight with a stranger.
</li>
<li class='choice '>
<span class='result'></span>
You tell everyone how great you are.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='concede#' id='definition-sound' path='audio/wordmeanings/amy-concede'></a>
When you <em>concede</em> to something, you unwillingly admit it is true or give in to it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>give in</em>
</span>
</span>
</div>
<a class='quick-help' href='concede#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='concede#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/concede/memory_hooks/3512.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Conked</span></span> on the Head</span></span> When my opponent <span class="emp1"><span>conked</span></span> me on the head I had to <span class="emp1"><span>conced</span></span>e defeat.
</p>
</div>

<div id='memhook-button-bar'>
<a href="concede#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/concede/memory_hooks">Use other hook</a>
<a href="concede#" id="memhook-use-own" url="https://membean.com/mywords/concede/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='concede#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Not until the space shuttle started flying did NASA <b>concede</b> that some astronauts didn't have to be fast-jet pilots. And at that point, sure enough, women started becoming astronauts.
<span class='attribution'>&mdash; Henry Spencer, Canadian computer programmer and space enthusiast</span>
<img alt="Henry spencer, canadian computer programmer and space enthusiast" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Henry Spencer, Canadian computer programmer and space enthusiast.jpg?qdep8" width="80" />
</li>
<li>
State officials <b>concede</b> that cloud computing does not create as many jobs as traditional industries, but they argue that construction jobs are important and that data centers are just one tactic in a host of economic development strategies . . . .
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
But even if we <b>concede</b> the point that athletics gives every school massive national exposure, it still does not address the lingering question: to what end is all this exposure?
<cite class='attribution'>
&mdash;
Forbes
</cite>
</li>
<li>
On average 49 [billion cubic meters] of water flows through the Blue Nile tributary a year, and Ethiopia has consistently refused to <b>concede</b> to giving Egypt a commitment to a specific amount that it will allow to flow through the dam. It sees Egypt's demands as a legacy of agreements that were made without its involvement. Egypt's official response betrayed powerlessness rather than resolve.
<cite class='attribution'>
&mdash;
The BBC
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/concede/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='concede#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='con_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='concede#'>
<span class='common'></span>
con-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ced_yield' data-tree-url='//cdn1.membean.com/public/data/treexml' href='concede#'>
<span class=''></span>
ced
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>yield</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='concede#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>When one <em>concedes</em> a point in a debate, one &#8220;thoroughly yields&#8221; to it.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='concede#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>True Grit</strong><span> LeBeouf concedes that Cogburn was right.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/concede.jpg' video_url='examplevids/concede' video_width='350'></span>
<div id='wt-container'>
<img alt="Concede" height="288" src="https://cdn1.membean.com/video/examplevids/concede.jpg" width="350" />
<div class='center'>
<a href="concede#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='concede#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Concede" src="https://cdn0.membean.com/public/images/wordimages/cons2/concede.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='concede#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abdicate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abjure</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abnegation</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>accede</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>accord</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>acquiesce</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>amenable</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>capitulate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>cede</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>condone</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>countenance</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>deference</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>demure</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>docile</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>ingratiate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>malleable</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>obeisance</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>propitiate</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>relinquish</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>servile</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>subservient</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='7' class = 'rw-wordform notlearned'><span>antipathy</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>appropriate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>arrogate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>demur</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>expropriate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>incorrigible</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>intractable</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>intransigent</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>obdurate</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>obstreperous</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>parry</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>proscribe</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>recalcitrant</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>refractory</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>reprobate</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>repudiate</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>subjugate</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>usurp</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='concede#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
concession
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>something that is unwillingly granted or admitted</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="concede" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>concede</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="concede#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

