
<!DOCTYPE html>
<html>
<head>
<title>Word: surreptitious | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Surreptitious-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/surreptitious-large.jpg?qdep8" />
</div>
<a href="surreptitious#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx1' style='display:none'>Something that is clandestine is hidden or kept a secret, often because it is illegal.</p>
<p class='rw-defn idx2' style='display:none'>A conclave is a meeting between a group of people who discuss something secretly.</p>
<p class='rw-defn idx3' style='display:none'>Covert activities or situations are secret and hidden.</p>
<p class='rw-defn idx4' style='display:none'>Someone who is disingenuous is not straightforward or is dishonest in what they say or do.</p>
<p class='rw-defn idx5' style='display:none'>To divulge something is to reveal information that was once private or secret.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is effusive expresses happiness, pleasure, admiration, praise, etc., in an extremely enthusiastic way.</p>
<p class='rw-defn idx7' style='display:none'>If you exude a quality or feeling, people easily notice that you have a large quantity of it because it flows from you; if a smell or liquid exudes from something, it flows steadily and slowly from it.</p>
<p class='rw-defn idx8' style='display:none'>An action that is flagrant shows that someone does not care if they obviously break the rules or highly offend people.</p>
<p class='rw-defn idx9' style='display:none'>If someone behaves in a furtive way, they do things sneakily and secretly in order to avoid being noticed.</p>
<p class='rw-defn idx10' style='display:none'>Something that is inconspicuous does not attract attention and is not easily seen or noticed because it is small or ordinary.</p>
<p class='rw-defn idx11' style='display:none'>People who are ingenuous are excessively trusting and believe almost everything that people tell them, especially because they have not had much life experience.</p>
<p class='rw-defn idx12' style='display:none'>Something that is insidious is dangerous because it seems harmless or not important; nevertheless, over time it gradually develops the capacity to cause harm and damage.</p>
<p class='rw-defn idx13' style='display:none'>Something that is latent, such as an undiscovered talent or undiagnosed disease, exists but is not active or has not developed at the moment—it may develop or become active in the future.</p>
<p class='rw-defn idx14' style='display:none'>If you describe something as nebulous, you mean that it is unclear, vague, and not clearly defined; a shape that is nebulous has no clear boundaries.</p>
<p class='rw-defn idx15' style='display:none'>Something is opaque if it is either difficult to understand or is not transparent.</p>
<p class='rw-defn idx16' style='display:none'>An overt act is not hidden or secret but is done in an open and public way.</p>
<p class='rw-defn idx17' style='display:none'>If a mood or feeling is palpable, it is so strong and intense that it is easily noticed and is almost able to be physically felt.</p>
<p class='rw-defn idx18' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx19' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx20' style='display:none'>If you sidle, you walk slowly, cautiously and often sideways in a particular direction, usually because you do not want to be noticed.</p>
<p class='rw-defn idx21' style='display:none'>If you employ subterfuge, you use a secret plan or action to get what you want by outwardly doing one thing that cleverly hides your true intentions.</p>
<p class='rw-defn idx22' style='display:none'>Something that is tangible is able to be touched and thus is considered real.</p>
<p class='rw-defn idx23' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>
<p class='rw-defn idx24' style='display:none'>Wiles are clever tricks or cunning schemes that are used to persuade someone to do what you want.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>surreptitious</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='surreptitious#' id='pronounce-sound' path='audio/words/amy-surreptitious'></a>
sur-uhp-TISH-uhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='surreptitious#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='surreptitious#' id='context-sound' path='audio/wordcontexts/brian-surreptitious'></a>
Nora wanted a midnight snack, so she <em>surreptitiously</em> or quietly crept downstairs to the kitchen, trying not to wake anyone.  She did not ask her brother to join her, but instead moved <em>surreptitiously</em> or silently through the dark hallways of the house.  Nora&#8217;s careful plan to raid the cookie jar was executed with <em>surreptitious</em> care or sneaky cleverness; unfortunately, she did not remember Max the dog who barked until he too quickly received a snack.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>surreptitious</em> act?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Sneaking into the back row of your college classroom so your professor doesn&#8217;t notice you are late.
</li>
<li class='choice '>
<span class='result'></span>
Pretending to accidentally bump into a group of friends when you planned to meet them all along.
</li>
<li class='choice '>
<span class='result'></span>
Eating the last piece of cake in the refrigerator even though you know your roommate was saving it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='surreptitious#' id='definition-sound' path='audio/wordmeanings/amy-surreptitious'></a>
A <em>surreptitious</em> deed is done secretly to avoid bringing any attention to it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>sneaky</em>
</span>
</span>
</div>
<a class='quick-help' href='surreptitious#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='surreptitious#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/surreptitious/memory_hooks/4844.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Syrup</span></span> Susp<span class="emp2"><span>icious</span></span></span></span> My sneaky <span class="emp1"><span>surrep</span></span><span class="emp2"><span>titious</span></span> children are no longer allowed to pour their own del<span class="emp2"><span>icious</span></span> <span class="emp1"><span>syrup</span></span>, for they <span class="emp1"><span>surrep</span></span><span class="emp2"><span>titious</span></span>ly dump on ounce upon ounce when I turn my back, making me highly susp<span class="emp2"><span>icious</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="surreptitious#" id="add-public-hook" style="" url="https://membean.com/mywords/surreptitious/memory_hooks">Use other public hook</a>
<a href="surreptitious#" id="memhook-use-own" url="https://membean.com/mywords/surreptitious/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='surreptitious#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
For several days after my first book was published, I carried it about in my pocket and took <b>surreptitious</b> peeps at it to make sure the ink had not faded.
<span class='attribution'>&mdash; James M. Barrie, Scottish novelist and playwright, known for creating the character Peter Pan</span>
<img alt="James m" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/James M. Barrie, Scottish novelist and playwright, known for creating the character Peter Pan.jpg?qdep8" width="80" />
</li>
<li>
Germany’s Supreme Court ruled last year that evidence gained from <b>surreptitious</b> searches of a suspect’s computers [was] inadmissible in the absence of surveillance laws regulating police hacking activity.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
Several services even use the <b>surreptitious</b> data storage to reinstate traditional cookies that a user deleted, which is called "re-spawning" in homage to video games where zombies come back to life even after being "killed," the report found.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
The public’s help is needed to identify a woman who police say swapped a toy ring for a diamond ring during an online sale meetup. The <b>surreptitious</b> swap happened on July 21 at a McDonald’s in Norwalk.
<cite class='attribution'>
&mdash;
CBS Los Angeles
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/surreptitious/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='surreptitious#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='sur_under' data-tree-url='//cdn1.membean.com/public/data/treexml' href='surreptitious#'>
<span class=''></span>
sur-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>under</td>
</tr>
<tr>
<td class='partform'>rept</td>
<td>
&rarr;
</td>
<td class='meaning'>snatched, grabbed, seized</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='itious_nature' data-tree-url='//cdn1.membean.com/public/data/treexml' href='surreptitious#'>
<span class=''></span>
-itious
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of the nature of</td>
</tr>
</table>
<p>When someone has been <em>surreptitious</em> about doing something, he has &#8220;snatched, grabbed, or seized&#8221; it &#8220;under&#8221; someone else&#8217;s notice or awareness on purpose.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='surreptitious#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Through the Wormhole</strong><span> The pink sticky note is placed on the child's head surreptitiously.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/surreptitious.jpg' video_url='examplevids/surreptitious' video_width='350'></span>
<div id='wt-container'>
<img alt="Surreptitious" height="288" src="https://cdn1.membean.com/video/examplevids/surreptitious.jpg" width="350" />
<div class='center'>
<a href="surreptitious#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='surreptitious#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Surreptitious" src="https://cdn2.membean.com/public/images/wordimages/cons2/surreptitious.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='surreptitious#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>clandestine</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>conclave</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>covert</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>disingenuous</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>furtive</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>inconspicuous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>insidious</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>latent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>nebulous</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>opaque</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>sidle</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>subterfuge</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>wile</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>divulge</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>effusive</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>exude</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>flagrant</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>ingenuous</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>overt</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>palpable</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>tangible</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="surreptitious" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>surreptitious</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="surreptitious#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

