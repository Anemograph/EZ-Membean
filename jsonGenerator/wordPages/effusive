
<!DOCTYPE html>
<html>
<head>
<title>Word: effusive | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Apathy is a lack of interest or unenthusiastic involvement in an activity; there is no effort to change or improve it at all.</p>
<p class='rw-defn idx1' style='display:none'>When you have ardor for something, you have an intense feeling of love, excitement, and admiration for it.</p>
<p class='rw-defn idx2' style='display:none'>If you describe a person&#8217;s behavior or speech as brusque, you mean that they say or do things abruptly or too quickly in a somewhat rude and impolite way.</p>
<p class='rw-defn idx3' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx4' style='display:none'>If you describe someone, usually a young woman, as demure, you mean that she is quiet, shy, and always behaves modestly.</p>
<p class='rw-defn idx5' style='display:none'>Someone who is dour is serious, stubborn, and unfriendly; they can also be gloomy.</p>
<p class='rw-defn idx6' style='display:none'>Weather that is dreary tends to be depressing and gloomy; a situation or person that is dreary tends to be boring or uninteresting.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx8' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx9' style='display:none'>If something enervates you, it makes you very tired and weak—almost to the point of collapse.</p>
<p class='rw-defn idx10' style='display:none'>Ennui is the feeling of being bored, tired, and dissatisfied with life.</p>
<p class='rw-defn idx11' style='display:none'>If something enthralls you, it makes you so interested and excited that you give it all your attention.</p>
<p class='rw-defn idx12' style='display:none'>If you show exuberance, you display great excitement, energy, and enthusiasm.</p>
<p class='rw-defn idx13' style='display:none'>If someone, such as a writer, is described as having fecundity, they have the ability to produce many original and different ideas.</p>
<p class='rw-defn idx14' style='display:none'>A garrulous person talks a lot, especially about unimportant things.</p>
<p class='rw-defn idx15' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx16' style='display:none'>Something inanimate is dead, motionless, or not living; therefore, it does not display the same traits as an active and alive organism.</p>
<p class='rw-defn idx17' style='display:none'>An indolent person is lazy.</p>
<p class='rw-defn idx18' style='display:none'>Insouciance is a lack of concern or worry for something that should be shown more careful attention or consideration.</p>
<p class='rw-defn idx19' style='display:none'>An introvert is someone who primarily prefers being by themselves instead of hanging out with others socially; nevertheless, they still enjoy spending time with friends.</p>
<p class='rw-defn idx20' style='display:none'>If someone has a jaundiced view of something, they ignore the good parts and can only see the bad aspects of it.</p>
<p class='rw-defn idx21' style='display:none'>If you do something in a lackadaisical way, you do it carelessly and without putting much effort into it—thereby showing that you are not really interested in what you&#8217;re doing.</p>
<p class='rw-defn idx22' style='display:none'>A person who is being laconic uses very few words to say something.</p>
<p class='rw-defn idx23' style='display:none'>A languid person is slow, relaxed, and shows little energy or interest in doing anything.</p>
<p class='rw-defn idx24' style='display:none'>If you are lethargic, you are tired, lack energy, and are unwilling to exert effort.</p>
<p class='rw-defn idx25' style='display:none'>When you are listless, you lack energy and interest and are unwilling to exert any effort.</p>
<p class='rw-defn idx26' style='display:none'>If you describe something as moribund, you imply that it is no longer effective; it may also be coming to the end of its useful existence.</p>
<p class='rw-defn idx27' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx28' style='display:none'>If you are pensive, you are deeply thoughtful, often in a sad and/or serious way.</p>
<p class='rw-defn idx29' style='display:none'>Someone who is phlegmatic stays calm and unemotional even in dangerous or exciting situations.</p>
<p class='rw-defn idx30' style='display:none'>A pithy statement or piece of writing is brief but intelligent; it is also precise and to the point.</p>
<p class='rw-defn idx31' style='display:none'>A profusion of something is a very large quantity or variety of it.</p>
<p class='rw-defn idx32' style='display:none'>Something or someone that is prolific is highly fruitful and so produces a lot of something.</p>
<p class='rw-defn idx33' style='display:none'>Something that is prolix, such as a lecture or speech, is excessively wordy; consequently, it can be tiresome to read or listen to.</p>
<p class='rw-defn idx34' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx35' style='display:none'>A stoic person does not show their emotions and does not complain when bad things happen to them.</p>
<p class='rw-defn idx36' style='display:none'>If you are stolid, you have or show little emotion about anything at all.</p>
<p class='rw-defn idx37' style='display:none'>A taciturn person is quiet or silent by nature.</p>
<p class='rw-defn idx38' style='display:none'>If you are suffering from tedium, you are bored.</p>
<p class='rw-defn idx39' style='display:none'>If your body is affected by torpor, you are severely lacking in energy; therefore, you are idle—and can even be numb.</p>
<p class='rw-defn idx40' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>
<p class='rw-defn idx41' style='display:none'>Something that is verbose, such as a speech or article, contains too many words.</p>
<p class='rw-defn idx42' style='display:none'>If someone is described as vivacious, they are lively and have a happy, lighthearted manner.</p>
<p class='rw-defn idx43' style='display:none'>A voluble person can speak easily with speech that naturally flows; voluble people also tend to be quite chatty.</p>
<p class='rw-defn idx44' style='display:none'>Someone who is zealous spends a lot of time, energy, and effort to support something—  notably that of a political or religious nature—because they believe in it very strongly.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>effusive</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='effusive#' id='pronounce-sound' path='audio/words/amy-effusive'></a>
i-FYOO-siv
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='effusive#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='effusive#' id='context-sound' path='audio/wordcontexts/brian-effusive'></a>
When Peggy saw her daughter Diane perform in the skating competition with grace and poise, she was <em>effusive</em> and highly enthusiastic in her high praise.  Impressed by her daughter&#8217;s achievement, Peggy told her again and again how proud she was in an <em>effusive</em>, lengthy, and windy speech.  Diane was touched but a little embarrassed by her mother&#8217;s <em>effusive</em> and overflowing compliments and support, especially since she had the feeling that her mother was overdoing it.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of an <em>effusive</em> statement?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
You need to study some more—I don&#8217;t think you&#8217;ll pass the history test next week if you don&#8217;t!
</li>
<li class='choice '>
<span class='result'></span>
I&#8217;m really not sure about crossing that bridge—it doesn&#8217;t look secure at all!
</li>
<li class='choice answer '>
<span class='result'></span>
I absolutely love how kind you are—if only everyone could follow your stupendous example!
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='effusive#' id='definition-sound' path='audio/wordmeanings/amy-effusive'></a>
Someone who is <em>effusive</em> expresses happiness, pleasure, admiration, praise, etc., in an extremely enthusiastic way.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>enthusiastic</em>
</span>
</span>
</div>
<a class='quick-help' href='effusive#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='effusive#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/effusive/memory_hooks/3788.json'></span>
<p>
<span class="emp0"><span>L<span class="emp1"><span>ive</span></span> <span class="emp3"><span>Fus</span></span>ion</span></span> When Bertha's son made almost a perfect score on the GRE, she turned into a l<span class="emp1"><span>ive</span></span> <span class="emp3"><span>fus</span></span>ion reactor by giving such ef<span class="emp3"><span>fus</span></span><span class="emp1"><span>ive</span></span> praise that she didn't stop radiating positive energy for two full weeks.
</p>
</div>

<div id='memhook-button-bar'>
<a href="effusive#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/effusive/memory_hooks">Use other hook</a>
<a href="effusive#" id="memhook-use-own" url="https://membean.com/mywords/effusive/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='effusive#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
They're so <b>effusive</b> with their love . . . theater fans. I'm a big comic book fan and there's a lot of parallels with them that they're just dedicated and loyal.
<cite class='attribution'>
&mdash;
Okieriete Onaodowan, American actor and singer
</cite>
</li>
<li>
In truth, fervor around his music wouldn’t fully take off in the United States until after Beethoven died in 1827, and it would take major nationwide shifts in how music was consumed, and in technology and demography—not to mention the <b>effusive</b> praise of a few key admirers—to boost the composer’s profile in the young, rapidly growing country.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
Read the old cuttings and you can’t help but be struck by their <b>effusive</b> reaction to almost everything she did. [Maggie] Smith was “bright, a chemical explosion, a star” according to one critic, “funny, pretty, ambitious and wound up like a spring” in the words of another, who liked her so much on stage that he asked her out for lunch.
<cite class='attribution'>
&mdash;
The Independent
</cite>
</li>
<li>
Tate drew <b>effusive</b> praise from coach Dabo Swinney for his performance at a position in which the Tigers are in most need of improvement. . . . “The [offensive lineman] is a difficult position to play, but where he’s so unique is his mental capacity, his knowledge of the game.”
<cite class='attribution'>
&mdash;
The Greenville News
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/effusive/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='effusive#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ef_out' data-tree-url='//cdn1.membean.com/public/data/treexml' href='effusive#'>
<span class=''></span>
ef-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>out of, from</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='fus_pour' data-tree-url='//cdn1.membean.com/public/data/treexml' href='effusive#'>
<span class=''></span>
fus
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>poured, poured out, spread out</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ive_does' data-tree-url='//cdn1.membean.com/public/data/treexml' href='effusive#'>
<span class=''></span>
-ive
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or that which does something</td>
</tr>
</table>
<p>When someone has been <em>effusive</em> in her praise, words have just &#8220;poured out from&#8221; her.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='effusive#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Effusive" src="https://cdn1.membean.com/public/images/wordimages/cons2/effusive.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='effusive#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>ardor</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>enthrall</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>exuberance</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>fecundity</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>garrulous</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>profusion</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>prolific</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>prolix</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>verbose</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>vivacious</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>voluble</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>zealous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>apathy</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>brusque</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>demure</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>dour</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>dreary</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>enervate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>ennui</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>inanimate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>indolent</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>insouciance</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>introvert</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>jaundiced</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>lackadaisical</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>laconic</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>languid</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>lethargic</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>listless</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>moribund</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>pensive</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>phlegmatic</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>pithy</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>stoic</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>stolid</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>taciturn</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>tedium</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>torpor</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="effusive" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>effusive</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="effusive#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

