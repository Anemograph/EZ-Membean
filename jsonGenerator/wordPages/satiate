
<!DOCTYPE html>
<html>
<head>
<title>Word: satiate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>Someone who is abstemious avoids doing too much of something enjoyable, such as eating or drinking; rather, they consume in a moderate fashion.</p>
<p class='rw-defn idx2' style='display:none'>Abstinence is the practice of keeping away from or avoiding something you enjoy—such as the physical pleasures of excessive food and drink—usually for health or religious reasons.</p>
<p class='rw-defn idx3' style='display:none'>You allay someone&#8217;s fear or concern by making them feel less afraid or worried.</p>
<p class='rw-defn idx4' style='display:none'>When you assuage an unpleasant feeling, you make it less painful or severe, thereby calming it.</p>
<p class='rw-defn idx5' style='display:none'>Something austere is simple and plain in its style; an austere person is strict and severe with themselves.</p>
<p class='rw-defn idx6' style='display:none'>Avarice is the extremely strong desire to have a lot of money and possessions.</p>
<p class='rw-defn idx7' style='display:none'>Something that is capacious has a lot of space and can contain a lot of things.</p>
<p class='rw-defn idx8' style='display:none'>Something that is cloying is way too sweet and tempting; therefore, it nauseates you or makes you disgusted after a while because you&#8217;re inclined to have too much of it.</p>
<p class='rw-defn idx9' style='display:none'>If you covet something that someone else has, you have a strong desire to have it for yourself.</p>
<p class='rw-defn idx10' style='display:none'>When there is a dearth of something, there is a scarcity or lack of it.</p>
<p class='rw-defn idx11' style='display:none'>A decadent person has low moral standards and is more interested in pleasure than serious matters.</p>
<p class='rw-defn idx12' style='display:none'>A deluge is a sudden, heavy downfall of rain; it can also be a large number of things, such as papers or e-mails, that someone gets all at the same time, making them very difficult to handle.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is epicurean derives great pleasure in material and sensual things, especially good food and drink.</p>
<p class='rw-defn idx14' style='display:none'>An exorbitant price or fee is much higher than what it should be or what is considered reasonable.</p>
<p class='rw-defn idx15' style='display:none'>Someone who is frugal spends very little money—and even then only on things that are absolutely necessary.</p>
<p class='rw-defn idx16' style='display:none'>Hedonism is the belief that pleasure is important, so much so that life should be spent doing only things that one enjoys.</p>
<p class='rw-defn idx17' style='display:none'>Someone who is indulgent tends to let other people have what they want; someone can be kind to excess when being indulgent.</p>
<p class='rw-defn idx18' style='display:none'>If something is inordinate, it is much larger in amount or degree than is normally expected.</p>
<p class='rw-defn idx19' style='display:none'>Someone who is jaded is bored with or weary of the world because they have had too much experience with it.</p>
<p class='rw-defn idx20' style='display:none'>When you behave with moderation, you live in a balanced and measured way; you do nothing to excess.</p>
<p class='rw-defn idx21' style='display:none'>A parsimonious person is not willing to give or spend money.</p>
<p class='rw-defn idx22' style='display:none'>A paucity of something is not enough of it.</p>
<p class='rw-defn idx23' style='display:none'>If you describe a person&#8217;s behavior as rapacious, you disapprove of them because they always want more money, goods, or possessions than they really need.</p>
<p class='rw-defn idx24' style='display:none'>If you are ravenous, you are extremely hungry.</p>
<p class='rw-defn idx25' style='display:none'>If someone regales you, they tell you stories and jokes to entertain you— and they could also serve you a wonderful feast.</p>
<p class='rw-defn idx26' style='display:none'>Stores that are replete with goods to sell are well-stocked and completely supplied to satisfy many customers.</p>
<p class='rw-defn idx27' style='display:none'>If you have a surfeit of something, you have much more than what you need.</p>
<p class='rw-defn idx28' style='display:none'>A sybarite is very fond of expensive and luxurious things; therefore, they are devoted to a life of pleasure.</p>
<p class='rw-defn idx29' style='display:none'>If you show temperance, you limit yourself so that you don’t do too much of something; you act in a controlled and well-balanced way.</p>
<p class='rw-defn idx30' style='display:none'>An unquenchable desire or thirst cannot be satisfied or gotten rid of.</p>
<p class='rw-defn idx31' style='display:none'>A voracious person has a strong desire to want a lot of something, especially food.</p>
<p class='rw-defn idx32' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>satiate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='satiate#' id='pronounce-sound' path='audio/words/amy-satiate'></a>
SAY-shee-ayt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='satiate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='satiate#' id='context-sound' path='audio/wordcontexts/brian-satiate'></a>
After eating a dozen doughnuts at the bakery, Doughnut Don&#8217;s appetite for sugar was <em>satiated</em> or completely satisfied.  These sugar needs happened from time to time, but Doughnut Don could always count on &#8220;Bart&#8217;s Bakery&#8221; on the corner to fully fill or <em>satiate</em> his large sweet tooth.  In fact, after some visits to Bart&#8217;s, Doughnut Don consumed so many doughnuts that he often felt too full, stuffed, or <em>satiated</em> by sugary delights.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If something <em>satiates</em> you, how do you feel?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Wet and uncomfortably cold.
</li>
<li class='choice answer '>
<span class='result'></span>
Filled and completely content.
</li>
<li class='choice '>
<span class='result'></span>
Overwhelmed and very tired.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='satiate#' id='definition-sound' path='audio/wordmeanings/amy-satiate'></a>
If something, such as food or drink, <em>satiates</em> you, it satisfies your need or desire so completely that you often feel that you have had too much.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>stuff</em>
</span>
</span>
</div>
<a class='quick-help' href='satiate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='satiate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/satiate/memory_hooks/4275.json'></span>
<p>
<img alt="Satiate" src="https://cdn3.membean.com/public/images/wordimages/hook/satiate.jpg?qdep8" />
<span class="emp0"><span><span class="emp1"><span>Say</span></span> <span class="emp2"><span>She</span></span> <span class="emp3"><span>Ate</span></span></span></span> My foodie friend would not stop talking or thinking about food.  She would constantly <span class="emp1"><span>say</span></span> <span class="emp2"><span>she</span></span> <span class="emp3"><span>ate</span></span> this, and <span class="emp1"><span>say</span></span> <span class="emp2"><span>she</span></span> <span class="emp3"><span>ate</span></span> that, as she <span class="emp1"><span>sa</span></span><span class="emp2"><span>ti</span></span><span class="emp3"><span>ate</span></span>d herself all day.
</p>
</div>

<div id='memhook-button-bar'>
<a href="satiate#" id="add-public-hook" style="" url="https://membean.com/mywords/satiate/memory_hooks">Use other public hook</a>
<a href="satiate#" id="memhook-use-own" url="https://membean.com/mywords/satiate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='satiate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Half a drink will not quench your thirst / Half a meal will not <b>satiate</b> your hunger / Half the way will get you no where / Half an idea will bear you no results
<span class='attribution'>&mdash; Kahlil Gibran, Lebanese-American poet and visual artist, from "Do Not Love Half-Midnight"</span>
<img alt="Kahlil gibran, lebanese-american poet and visual artist, from &quot;do not love half-midnight&quot;" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Kahlil Gibran, Lebanese-American poet and visual artist, from &quot;Do Not Love Half-Midnight&quot;.jpg?qdep8" width="80" />
</li>
<li>
Stop by Hub Cafe on Wednesday to peruse a selection of local vegetables and cheese from Pekarek Produce, Shadowbrook Farm and Dutchgirl Creamery. If the produce doesn’t <b>satiate</b> your hunger, the cafe will be open for business.
<cite class='attribution'>
&mdash;
The Daily Nebraskan
</cite>
</li>
<li>
The building will reopen when that [repair and testing] process is complete, but until then, the museum has devised another way to <b>satiate</b> history-hungry visitors.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
So maybe, entomunchies? Alternative proteins? Whatever we call them, there’s a good chance more bugs will soon hit store shelves. And as we continue to refine our palate, scientists hope to <b>satiate</b> our eco-friendly appetites one cricket at a time.
<cite class='attribution'>
&mdash;
Discover Magazine
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/satiate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='satiate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='sat_enough' data-tree-url='//cdn1.membean.com/public/data/treexml' href='satiate#'>
<span class=''></span>
sat
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>enough</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ate_quality' data-tree-url='//cdn1.membean.com/public/data/treexml' href='satiate#'>
<span class=''></span>
-ate
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make someone have a certain quality</td>
</tr>
</table>
<p>To <em>satiate</em> someone is to &#8220;make her have enough&#8221; of something.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='satiate#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>TV Advertisement: Alka Seltzer (1972)</strong><span> He has been satiated to his own discomfort.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/satiate.jpg' video_url='examplevids/satiate' video_width='350'></span>
<div id='wt-container'>
<img alt="Satiate" height="288" src="https://cdn1.membean.com/video/examplevids/satiate.jpg" width="350" />
<div class='center'>
<a href="satiate#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='satiate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Satiate" src="https://cdn2.membean.com/public/images/wordimages/cons2/satiate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='satiate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>allay</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>assuage</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>avarice</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>capacious</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>cloy</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>covet</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>decadent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>deluge</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>epicurean</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>exorbitant</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>hedonism</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>indulgent</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>inordinate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>jaded</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>rapacious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>ravenous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>regale</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>replete</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>surfeit</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>sybarite</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>voracious</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>abstemious</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abstinence</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>austere</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>dearth</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>frugal</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>moderation</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>parsimonious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>paucity</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>temperance</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>unquenchable</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='satiate#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
satiated
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>full</td>
</tr>
<tr>
<td class='wordform'>
<span>
insatiable
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not able to be satisfied</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="satiate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>satiate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="satiate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

