
<!DOCTYPE html>
<html>
<head>
<title>Word: pugnacious | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx1' style='display:none'>If you are amenable to doing something, you willingly accept it without arguing.</p>
<p class='rw-defn idx2' style='display:none'>If you are bellicose, you behave in an aggressive way and are likely to start an argument or fight.</p>
<p class='rw-defn idx3' style='display:none'>A belligerent person or country is aggressive, very unfriendly, and likely to start a fight.</p>
<p class='rw-defn idx4' style='display:none'>Someone who has a bilious personality is highly irritable and bad-tempered.</p>
<p class='rw-defn idx5' style='display:none'>Someone who is boisterous is noisy, excitable, and full of boundless energy; therefore, they show a lack of disciplined restraint at times.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is cantankerous is bad-tempered and always finds things to complain about.</p>
<p class='rw-defn idx7' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx8' style='display:none'>Conciliation is a process intended to end an argument between two groups of people.</p>
<p class='rw-defn idx9' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx10' style='display:none'>When someone is contentious, they are argumentative and likely to provoke a fight.</p>
<p class='rw-defn idx11' style='display:none'>A man who is debonair wears fashionable clothes and is sophisticated, charming, friendly, and confident.</p>
<p class='rw-defn idx12' style='display:none'>If you describe someone, usually a young woman, as demure, you mean that she is quiet, shy, and always behaves modestly.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is diffident is shy, does not want to draw notice to themselves, and is lacking in self-confidence.</p>
<p class='rw-defn idx14' style='display:none'>Dissension is a disagreement or difference of opinion among a group of people that can cause conflict.</p>
<p class='rw-defn idx15' style='display:none'>A small group or issue that is factious is controversial and promotes strong disagreement or dissatisfaction within a larger group.</p>
<p class='rw-defn idx16' style='display:none'>If someone is inimical, they are unfriendly and will likely cause you harm.</p>
<p class='rw-defn idx17' style='display:none'>An irascible person becomes angry very easily.</p>
<p class='rw-defn idx18' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx19' style='display:none'>A polemic is a strong written or spoken statement that usually attacks or less often defends a particular idea, opinion, or belief.</p>
<p class='rw-defn idx20' style='display:none'>A troglodyte is reclusive, severely lacking in social skills, and is out of step with current times.</p>
<p class='rw-defn idx21' style='display:none'>A truculent person is bad-tempered, easily annoyed, and prone to arguing excessively.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>pugnacious</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='pugnacious#' id='pronounce-sound' path='audio/words/amy-pugnacious'></a>
puhg-NAY-shuhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='pugnacious#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='pugnacious#' id='context-sound' path='audio/wordcontexts/brian-pugnacious'></a>
Charlotte Marie is one <em>pugnacious</em> dog: she will start a fight over the smallest thing.  One day an unlucky salesman came to the house where this <em>pugnacious</em>, combative dog lives.  It&#8217;s a good thing she&#8217;s small, because she tore right out of the house and <em>pugnaciously</em> bit him on both ankles, barking and biting wildly.  Her owner wishes the <em>pugnacious</em> Charlotte Marie wasn&#8217;t so ready to fight, although it does come in handy when unwanted visitors come to the door.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How might a <em>pugnacious</em> person act?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They would be sneaky and corrupt in order to achieve a goal. 
</li>
<li class='choice '>
<span class='result'></span>
They would tell funny stories and jokes to make people like them.
</li>
<li class='choice answer '>
<span class='result'></span>
They would use force and aggression to get what they want.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='pugnacious#' id='definition-sound' path='audio/wordmeanings/amy-pugnacious'></a>
To act in a <em>pugnacious</em> manner is to act in a combative and aggressive way.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>aggressive</em>
</span>
</span>
</div>
<a class='quick-help' href='pugnacious#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='pugnacious#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/pugnacious/memory_hooks/4777.json'></span>
<p>
<span class="emp0"><span>Vi<span class="emp1"><span>cious</span></span> <span class="emp3"><span>Pug</span></span></span></span>  The <span class="emp3"><span>pug</span></span>na<span class="emp1"><span>cious</span></span> <span class="emp3"><span>pug</span></span> was so vi<span class="emp1"><span>cious</span></span> it would bite everyone it met.
</p>
</div>

<div id='memhook-button-bar'>
<a href="pugnacious#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/pugnacious/memory_hooks">Use other hook</a>
<a href="pugnacious#" id="memhook-use-own" url="https://membean.com/mywords/pugnacious/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='pugnacious#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The critical difference between the two: While Nadal is clearly galvanized by the concept of a rivalry, Federer can appear annoyed by the presence of such a bold and <b>pugnacious</b> challenger.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Brickman, 64, is a <b>pugnacious</b>, abrasive political conservative who is given to offering reporters red-meat sound bites savaging plaintiffs lawyers for alleged ethical abuses.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
A <b>pugnacious</b> patrician who was a peacemaker to the core, he became the first director of the Peace Corps and ran Lyndon Johnston’s War on Poverty.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
The winner of the inaugural Caesars Palace Grand Prix was Alan Jones, 35, a stocky, <b>pugnacious</b> Australian who had won the driving title last year.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/pugnacious/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='pugnacious#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pugn_fight' data-tree-url='//cdn1.membean.com/public/data/treexml' href='pugnacious#'>
<span class=''></span>
pugn
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>fight</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='acious_inclined' data-tree-url='//cdn1.membean.com/public/data/treexml' href='pugnacious#'>
<span class=''></span>
-acious
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>inclined to, abounding in</td>
</tr>
</table>
<p>One who is <em>pugnacious</em> is &#8220;inclined to or abounding in&#8221; wanting to &#8220;fight.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='pugnacious#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Tom and Jerry</strong><span> A pugnacious dog is after the cat again.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/pugnacious.jpg' video_url='examplevids/pugnacious' video_width='350'></span>
<div id='wt-container'>
<img alt="Pugnacious" height="288" src="https://cdn1.membean.com/video/examplevids/pugnacious.jpg" width="350" />
<div class='center'>
<a href="pugnacious#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='pugnacious#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Pugnacious" src="https://cdn2.membean.com/public/images/wordimages/cons2/pugnacious.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='pugnacious#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>bellicose</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>belligerent</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>bilious</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>boisterous</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>cantankerous</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>contentious</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>dissension</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>factious</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>inimical</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>irascible</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>polemic</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>troglodyte</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>truculent</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>amenable</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>conciliation</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>debonair</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>demure</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>diffident</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='pugnacious#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
pugnacity
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>aggressiveness </td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="pugnacious" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>pugnacious</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="pugnacious#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

