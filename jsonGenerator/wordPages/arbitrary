
<!DOCTYPE html>
<html>
<head>
<title>Word: arbitrary | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An autocratic person rules with complete power; consequently, they make decisions and give orders to people without asking them for their opinion or advice.</p>
<p class='rw-defn idx1' style='display:none'>An autonomous person makes their own decisions without being influenced by anyone else; an autonomous country or organization is independent and has the power to govern itself.</p>
<p class='rw-defn idx2' style='display:none'>Someone who is capricious changes their mind repeatedly or behaves in unexpected ways; a capricious series of events follows no predictable pattern.</p>
<p class='rw-defn idx3' style='display:none'>A despot is a leader who has a lot of power and uses it in a cruel and unreasonable way.</p>
<p class='rw-defn idx4' style='display:none'>Someone is being dogmatic when they express opinions in an assertive and often overly self-important manner.</p>
<p class='rw-defn idx5' style='display:none'>Empirical evidence or study is based on real experience or scientific experiments rather than on unproven theories.</p>
<p class='rw-defn idx6' style='display:none'>Erratic behavior is irregular, unpredictable, and unusual.</p>
<p class='rw-defn idx7' style='display:none'>If something is gratuitous, it is freely given; nevertheless, it is usually unnecessary and can be harmful or upsetting.</p>
<p class='rw-defn idx8' style='display:none'>Someone who is imperious behaves in a proud, overbearing, and highly confident manner that shows they expect to be obeyed without question.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is impetuous does things quickly and rashly without thinking carefully first.</p>
<p class='rw-defn idx10' style='display:none'>Someone who is impulsive tends to do things without thinking about them ahead of time; therefore, their actions can be unpredictable.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is indiscreet shows lack of judgment, especially because they say or do things in public that should only be said or done privately—if at all.</p>
<p class='rw-defn idx12' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx13' style='display:none'>A judicious person shows good or sound judgment because they are wise and careful in making decisions.</p>
<p class='rw-defn idx14' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx15' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx16' style='display:none'>A petulant person behaves in an unreasonable and childish way, especially because they cannot get their own way or what they want.</p>
<p class='rw-defn idx17' style='display:none'>A potentate is a ruler who has great power over people.</p>
<p class='rw-defn idx18' style='display:none'>A rubric is a set of instructions at the beginning of a document, such as an examination or term paper, that is usually printed in a different style so as to highlight its importance.</p>
<p class='rw-defn idx19' style='display:none'>A scrupulous person takes great care to do everything in an extremely honest, conscientious, and fair manner.</p>
<p class='rw-defn idx20' style='display:none'>Something statutory, such as the power given to a governor or president, is created, established, and controlled by rules and laws; hence, it has the full force of the law behind it and must be followed.</p>
<p class='rw-defn idx21' style='display:none'>A subjective opinion is not based upon facts or hard evidence; rather, it rests upon someone&#8217;s personal feelings or beliefs about a matter or concern.</p>
<p class='rw-defn idx22' style='display:none'>An unfounded claim is not based upon evidence; instead, it is unproven or groundless.</p>
<p class='rw-defn idx23' style='display:none'>An unwarranted decision cannot be justified because there is no evidence to back it up; therefore, it is needless and uncalled-for.</p>
<p class='rw-defn idx24' style='display:none'>If you do something of your own volition, you choose to do it because you want to—not because you are forced to.</p>
<p class='rw-defn idx25' style='display:none'>A wanton action deliberately harms someone or damages something for no apparent or good reason.</p>
<p class='rw-defn idx26' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>arbitrary</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='arbitrary#' id='pronounce-sound' path='audio/words/amy-arbitrary'></a>
AHR-bi-trer-ee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='arbitrary#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='arbitrary#' id='context-sound' path='audio/wordcontexts/brian-arbitrary'></a>
According to Serena&#8217;s viewpoint, the casting for the play seemed quite random or <em>arbitrary</em>.  Serena watched as brilliant performers were <em><em>arbitrarily</em></em> given minor roles for no real reason, while less talented actors received the leads.  According to her friends in the cast, even the director&#8217;s expectations during rehearsal seemed unclear and <em>arbitrary</em>&#8212;they simply could not figure out what was required of them.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean when something is <em>arbitrary</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It seems like an unusually negative response.
</li>
<li class='choice answer '>
<span class='result'></span>
It appears to be made in a random way.
</li>
<li class='choice '>
<span class='result'></span>
It rewards people who do not deserve to be rewarded.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='arbitrary#' id='definition-sound' path='audio/wordmeanings/amy-arbitrary'></a>
If you describe a decision, rule, or plan as <em>arbitrary</em>, you think that it was decided without any thought, standard, or system to guide it; therefore, it can seem unfair.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>random</em>
</span>
</span>
</div>
<a class='quick-help' href='arbitrary#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='arbitrary#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/arbitrary/memory_hooks/6069.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Arby</span></span>'s or Bak<span class="emp2"><span>ery</span></span> for Lunch?</span></span> It's pretty <span class="emp1"><span>arbi</span></span>tr<span class="emp2"><span>ary</span></span> whether you choose a fatty sandwich at <span class="emp1"><span>Arby</span></span>'s or a fatty donut at the bak<span class="emp2"><span>ery</span></span>--both will make you fat.
</p>
</div>

<div id='memhook-button-bar'>
<a href="arbitrary#" id="add-public-hook" style="" url="https://membean.com/mywords/arbitrary/memory_hooks">Use other public hook</a>
<a href="arbitrary#" id="memhook-use-own" url="https://membean.com/mywords/arbitrary/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='arbitrary#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The group said last week Nepal "should cease <b>arbitrary</b> arrests and detentions, harassment, and the use of excessive force to silence Tibetan protesters, activists and journalists."
<cite class='attribution'>
&mdash;
Fox News
</cite>
</li>
<li>
Magna Carta, which ended the quarrel between King John and his English barons in 1215, established the rights of subjects to their own property, protecting them from the threat of <b>arbitrary</b> confiscation by the crown.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
An <b>arbitrary</b> approach to water resources could grind to a halt hundreds of millions of dollars in economic investment on Long Island, creating a shudder throughout the region.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
The report was correct in saying that there is a debate in biology between those who think signals such as [brightly colored] flash feathers are essentially <b>arbitrary</b> and those who think they are signs of underlying health and good genes.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/arbitrary/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='arbitrary#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='arbitr_think' data-tree-url='//cdn1.membean.com/public/data/treexml' href='arbitrary#'>
<span class=''></span>
arbitr
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>think, judge</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ary_relates' data-tree-url='//cdn1.membean.com/public/data/treexml' href='arbitrary#'>
<span class=''></span>
-ary
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>An <em>arbitrary</em> decision is one that has been seemingly randomly &#8220;thought about&#8221; or &#8220;judged.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='arbitrary#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Conan</strong><span> Talk about being arbitrary!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/arbitrary.jpg' video_url='examplevids/arbitrary' video_width='350'></span>
<div id='wt-container'>
<img alt="Arbitrary" height="288" src="https://cdn1.membean.com/video/examplevids/arbitrary.jpg" width="350" />
<div class='center'>
<a href="arbitrary#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='arbitrary#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Arbitrary" src="https://cdn1.membean.com/public/images/wordimages/cons2/arbitrary.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='arbitrary#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>autocratic</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>autonomous</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>capricious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>despot</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>erratic</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>gratuitous</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>imperious</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>impetuous</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>impulsive</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>indiscreet</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>petulant</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>potentate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>subjective</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>unfounded</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>unwarranted</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>volition</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>wanton</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='4' class = 'rw-wordform notlearned'><span>dogmatic</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>empirical</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>judicious</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>rubric</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>scrupulous</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>statutory</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="arbitrary" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>arbitrary</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="arbitrary#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

