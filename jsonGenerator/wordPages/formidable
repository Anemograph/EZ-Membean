
<!DOCTYPE html>
<html>
<head>
<title>Word: formidable | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>You accost a stranger when you move towards them and speak in an unpleasant or threatening way.</p>
<p class='rw-defn idx1' style='display:none'>Something that is arduous is extremely difficult; hence, it involves a lot of effort.</p>
<p class='rw-defn idx2' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx3' style='display:none'>Someone who has a bilious personality is highly irritable and bad-tempered.</p>
<p class='rw-defn idx4' style='display:none'>If you describe a person&#8217;s behavior as brazen, you mean that they are not embarrassed by anything they do—they simply don&#8217;t care what other people think about them.</p>
<p class='rw-defn idx5' style='display:none'>Someone who has a bristling personality is easily offended, annoyed, or angered.</p>
<p class='rw-defn idx6' style='display:none'>A chivalrous man behaves in a polite, kind, generous, and honorable way, especially towards women.</p>
<p class='rw-defn idx7' style='display:none'>A conundrum is a problem or puzzle that is difficult or impossible to solve.</p>
<p class='rw-defn idx8' style='display:none'>If something daunts you, it makes you worried about dealing with it because you think it will be very difficult or dangerous.</p>
<p class='rw-defn idx9' style='display:none'>Debility is a state of being physically or mentally weak, usually due to illness.</p>
<p class='rw-defn idx10' style='display:none'>If you describe someone, usually a young woman, as demure, you mean that she is quiet, shy, and always behaves modestly.</p>
<p class='rw-defn idx11' style='display:none'>A despot is a leader who has a lot of power and uses it in a cruel and unreasonable way.</p>
<p class='rw-defn idx12' style='display:none'>Effrontery is very rude behavior that shows a great lack of respect and is often insulting.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is exacting expects others to work very hard and carefully.</p>
<p class='rw-defn idx14' style='display:none'>Something that is imperceptible is either impossible to be perceived by the mind or is very difficult to perceive.</p>
<p class='rw-defn idx15' style='display:none'>Inconsequential matters are unimportant or are of little to no significance.</p>
<p class='rw-defn idx16' style='display:none'>Something that is inconspicuous does not attract attention and is not easily seen or noticed because it is small or ordinary.</p>
<p class='rw-defn idx17' style='display:none'>An indomitable foe cannot be beaten.</p>
<p class='rw-defn idx18' style='display:none'>Something infinitesimal is so extremely small or minute that it is very hard to measure it.</p>
<p class='rw-defn idx19' style='display:none'>A juggernaut is a very powerful force, organization, or group whose influence cannot be stopped; its strength and power can overwhelm or crush any competition or anything else that stands in its way.</p>
<p class='rw-defn idx20' style='display:none'>A laborious job or process takes a long time, requires a lot of effort, and is often boring.</p>
<p class='rw-defn idx21' style='display:none'>A leviathan is something that is very large, powerful, difficult to control, and rather frightening.</p>
<p class='rw-defn idx22' style='display:none'>A modicum is a small amount of something, especially a good quality.</p>
<p class='rw-defn idx23' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx24' style='display:none'>Nominal can refer to someone who is in charge in name only, or it can refer to a very small amount of something; both are related by their relative insignificance.</p>
<p class='rw-defn idx25' style='display:none'>An omnipotent being or entity is unlimited in its power.</p>
<p class='rw-defn idx26' style='display:none'>A task or responsibility is onerous if you dislike having to do it because it is difficult or tiring.</p>
<p class='rw-defn idx27' style='display:none'>If someone is pallid, they look very pale in an unattractive and unhealthy way.</p>
<p class='rw-defn idx28' style='display:none'>Something&#8217;s potency is how powerful or effective it is.</p>
<p class='rw-defn idx29' style='display:none'>Something that is prodigious is very large, impressive, or great.</p>
<p class='rw-defn idx30' style='display:none'>If you describe someone as redoubtable, you have great respect for their power and strength; you may be afraid of them as well.</p>
<p class='rw-defn idx31' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx32' style='display:none'>A stupendous event or accomplishment is amazing, wonderful, or spectacular.</p>
<p class='rw-defn idx33' style='display:none'>To be timorous is to be fearful.</p>
<p class='rw-defn idx34' style='display:none'>Someone is tremulous when they are shaking slightly from nervousness or fear; they may also simply be afraid of something.</p>
<p class='rw-defn idx35' style='display:none'>Something trivial is of little value or is not important.</p>
<p class='rw-defn idx36' style='display:none'>A troglodyte is reclusive, severely lacking in social skills, and is out of step with current times.</p>
<p class='rw-defn idx37' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx38' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>formidable</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='formidable#' id='pronounce-sound' path='audio/words/amy-formidable'></a>
FAWR-mi-duh-buhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='formidable#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='formidable#' id='context-sound' path='audio/wordcontexts/brian-formidable'></a>
I think that the most frightening creature in Greek mythology was Cerberus, a threatening, <em>formidable</em>, monstrous dog that had three enormous heads. When he barked, thunder came from his three mouths; as if that wasn&#8217;t enough, Cerberus possessed <em>formidable</em>, overwhelming size&#8212;much larger than an elephant. Cerberus made the perfect guard dog&#8212;he was so <em>formidable</em> or powerful that it was almost impossible for anyone alive to get in&#8212;or back out of&#8212;the Underworld.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of something <em>formidable</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A lump of clay that an artist skillfully shapes into a beautiful vase.
</li>
<li class='choice '>
<span class='result'></span>
A brick wall that someone builds to separate their yard from their neighbor&#8217;s.
</li>
<li class='choice answer '>
<span class='result'></span>
A well-trained and large army that is known for winning every battle.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='formidable#' id='definition-sound' path='audio/wordmeanings/amy-formidable'></a>
If something is <em>formidable</em>, it is impressive in size, power, or skill; therefore, it can make you feel frightened, alarmed, or in awe because it is so powerful or difficult to deal with.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>impressively powerful</em>
</span>
</span>
</div>
<a class='quick-help' href='formidable#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='formidable#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/formidable/memory_hooks/5420.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Form</span></span>s the <span class="emp1"><span>D</span></span>i<span class="emp1"><span>able</span></span></span></span> My fencing opponent is so <span class="emp3"><span>form</span></span>i<span class="emp1"><span>dable</span></span> that I almost think he takes the <span class="emp3"><span>form</span></span> of the <span class="emp1"><span>d</span></span>i<span class="emp1"><span>able</span></span> in order to frighten me into submission.    (<em><span class="emp1"><span>D</span></span>i<span class="emp1"><span>able</span></span></em> is the French word for "devil.")
</p>
</div>

<div id='memhook-button-bar'>
<a href="formidable#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/formidable/memory_hooks">Use other hook</a>
<a href="formidable#" id="memhook-use-own" url="https://membean.com/mywords/formidable/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='formidable#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The man who led the opposition to a trouncing of Prime Minister Rajiv Gandhi’s forces in the key state of Haryana has emerged as a <b>formidable</b> leader in northern India.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
The first rooms, painted a deep crimson and filled with portraits and furniture, evoke Marie-Antoinette’s childhood in Austria, where she was the 15th of 16 children at the Habsburg court, presided over by the <b>formidable</b> Empress Maria Theresa.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
The fence must be <b>formidable</b> but not lethal; visually imposing but not ugly; durable but environmentally friendly; and economically built but not flimsy.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
Both Mrs. Clinton and Mr. Obama have the necessary attributes to take on Mr. McCain: they are two <b>formidable</b> campaigners who have offered detailed and generally intelligent policy proposals and have been forced to work exceptionally hard for their votes.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/formidable/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='formidable#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>formid</td>
<td>
&rarr;
</td>
<td class='meaning'>dread, be greatly afraid</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='able_handy' data-tree-url='//cdn1.membean.com/public/data/treexml' href='formidable#'>
<span class=''></span>
-able
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>handy, capable of</td>
</tr>
</table>
<p>Something <em>formidable</em> is &#8220;capable of&#8221; causing &#8220;dread or great fear&#8221; in someone.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='formidable#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Back to the Future</strong><span> Biff is a formidable opponent.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/formidable.jpg' video_url='examplevids/formidable' video_width='350'></span>
<div id='wt-container'>
<img alt="Formidable" height="288" src="https://cdn1.membean.com/video/examplevids/formidable.jpg" width="350" />
<div class='center'>
<a href="formidable#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='formidable#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Formidable" src="https://cdn1.membean.com/public/images/wordimages/cons2/formidable.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='formidable#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>accost</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>arduous</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bilious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>brazen</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>bristling</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>conundrum</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>daunt</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>despot</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>effrontery</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>exacting</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>indomitable</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>juggernaut</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>laborious</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>leviathan</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>omnipotent</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>onerous</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>potency</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>prodigious</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>redoubtable</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>stupendous</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>timorous</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>tremulous</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>troglodyte</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>chivalrous</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>debility</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>demure</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>imperceptible</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>inconsequential</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>inconspicuous</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>infinitesimal</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>modicum</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>nominal</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>pallid</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>trivial</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="formidable" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>formidable</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="formidable#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

