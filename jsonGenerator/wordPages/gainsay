
<!DOCTYPE html>
<html>
<head>
<title>Word: gainsay | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abjure a belief or a way of behaving, you state publicly that you will give it up or reject it.</p>
<p class='rw-defn idx1' style='display:none'>When you accede to a demand or proposal, you agree to it, especially after first disagreeing with it.</p>
<p class='rw-defn idx2' style='display:none'>If you acquiesce to something, you allow it to happen by doing what someone wants without putting up a struggle or voicing your own concerns.</p>
<p class='rw-defn idx3' style='display:none'>An altercation is a noisy disagreement or heated argument.</p>
<p class='rw-defn idx4' style='display:none'>When one thing belies a second, it hides the true situation, producing a false idea or impression about that second thing.</p>
<p class='rw-defn idx5' style='display:none'>Someone who has a bilious personality is highly irritable and bad-tempered.</p>
<p class='rw-defn idx6' style='display:none'>Someone who has a bristling personality is easily offended, annoyed, or angered.</p>
<p class='rw-defn idx7' style='display:none'>Someone cedes land or power to someone else by giving it to them, often because of political or military pressure.</p>
<p class='rw-defn idx8' style='display:none'>To contravene a law, rule, or agreement is to do something that is not allowed or is forbidden by that law, rule, or agreement.</p>
<p class='rw-defn idx9' style='display:none'>If someone will countenance something, they will approve, tolerate, or support it.</p>
<p class='rw-defn idx10' style='display:none'>If you espouse an idea, principle, or belief, you support it wholeheartedly.</p>
<p class='rw-defn idx11' style='display:none'>If you impugn someone&#8217;s motives or integrity, you say that they do not deserve to be trusted.</p>
<p class='rw-defn idx12' style='display:none'>People who are ingenuous are excessively trusting and believe almost everything that people tell them, especially because they have not had much life experience.</p>
<p class='rw-defn idx13' style='display:none'>An irrefutable argument or statement cannot be proven wrong; therefore, it must be accepted because it is certain.</p>
<p class='rw-defn idx14' style='display:none'>An irrevocable action or decision is impossible to change, reverse, or stop.</p>
<p class='rw-defn idx15' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx16' style='display:none'>If you recant, you publicly announce that your once firmly held beliefs or statements were wrong and that you no longer agree with them.</p>
<p class='rw-defn idx17' style='display:none'>To remonstrate with someone is to tell that person that you strongly disapprove of something they have said or done.</p>
<p class='rw-defn idx18' style='display:none'>If you repudiate something, you state that you do not accept or agree with it and no longer want to be connected with it in any way.</p>
<p class='rw-defn idx19' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx20' style='display:none'>The verity of something is the truth or reality of it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>gainsay</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='gainsay#' id='pronounce-sound' path='audio/words/amy-gainsay'></a>
GAYN-say
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='gainsay#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='gainsay#' id='context-sound' path='audio/wordcontexts/brian-gainsay'></a>
My mother is considered the top baker in our family, but when Aunt Sully visits she <em>gainsays</em> that widely held opinion by denying that Mom is the best.  During a cake-baking contest, Sully <em><em>gainsaid</em></em> or went against the judge&#8217;s decision that Mom&#8217;s chocolate torte was the winner: Sully vocally voted for her own almond mousse cake instead.  Unfortunately, my aunt embarrassed my mother by <em>gainsaying</em> or opposing the prize in front of everyone.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What statement is an example of someone&#8217;s <em>gainsaying</em> something?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
“This is utterly ridiculous and completely false!”
</li>
<li class='choice '>
<span class='result'></span>
“If you will just give me a moment, I can explain what this means.”
</li>
<li class='choice '>
<span class='result'></span>
“This is definitely something worth supporting.”
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='gainsay#' id='definition-sound' path='audio/wordmeanings/amy-gainsay'></a>
If you <em>gainsay</em> something, you say that it is not true and therefore reject it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>contradict</em>
</span>
</span>
</div>
<a class='quick-help' href='gainsay#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='gainsay#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/gainsay/memory_hooks/4659.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Gains</span></span>? <span class="emp3"><span>Ay</span></span>e!</span></span> Who would <span class="emp2"><span>gains</span></span><span class="emp3"><span>ay</span></span> s<span class="emp3"><span>ay</span></span>ing <span class="emp3"><span>Ay</span></span>e! to soaring stock <span class="emp2"><span>gains</span></span>?
</p>
</div>

<div id='memhook-button-bar'>
<a href="gainsay#" id="add-public-hook" style="" url="https://membean.com/mywords/gainsay/memory_hooks">Use other public hook</a>
<a href="gainsay#" id="memhook-use-own" url="https://membean.com/mywords/gainsay/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='gainsay#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
If you're going to make a mess of the game, you might as well make a Monday-night, prime-time, billion-dollar mess of it. That way no one can <b>gainsay</b> the problem, for it's out in the open for all to see.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Who can <b>gainsay</b> British Prime Minister Tony Blair, who has made aid to Africa a centerpiece of the G8 summit of industrial powers this week, when he calls [the widespread suffering in] Africa a scar on the world’s conscience?
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
But no one could <b>gainsay</b> the fact that most free, industrialized nations stood clear of crucial economic problems—while the Soviet Union's wheat crop failed, Red China's economy continued to falter at bare subsistence levels and Cuba proved a good showcase of how to ruin an economy in a hurry.
<cite class='attribution'>
&mdash;
Time Magazine
</cite>
</li>
<li>
The autocrat of the back stage region at the Globe [Theater] these nights is not [director] Raymond Hitchcock, although it is conceded that there would be none to <b>gainsay</b> him if he chose to exercise the powers of autocracy.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/gainsay/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='gainsay#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>gain</td>
<td>
&rarr;
</td>
<td class='meaning'>against</td>
</tr>
<tr>
<td class='partform'>say</td>
<td>
&rarr;
</td>
<td class='meaning'>say</td>
</tr>
</table>
<p>When you <em>gainsay</em> someone&#8217;s opinion, you &#8220;say&#8221; something &#8220;against&#8221; it.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='gainsay#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Gainsay" src="https://cdn3.membean.com/public/images/wordimages/cons2/gainsay.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='gainsay#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abjure</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>altercation</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>belie</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>bilious</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>bristling</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>contravene</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>impugn</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>recant</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>remonstrate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>repudiate</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>accede</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>acquiesce</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>cede</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>countenance</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>espouse</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>ingenuous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>irrefutable</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>irrevocable</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="gainsay" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>gainsay</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="gainsay#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

