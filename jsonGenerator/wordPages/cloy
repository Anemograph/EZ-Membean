
<!DOCTYPE html>
<html>
<head>
<title>Word: cloy | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Someone who is abstemious avoids doing too much of something enjoyable, such as eating or drinking; rather, they consume in a moderate fashion.</p>
<p class='rw-defn idx1' style='display:none'>Abstinence is the practice of keeping away from or avoiding something you enjoy—such as the physical pleasures of excessive food and drink—usually for health or religious reasons.</p>
<p class='rw-defn idx2' style='display:none'>You allay someone&#8217;s fear or concern by making them feel less afraid or worried.</p>
<p class='rw-defn idx3' style='display:none'>When you are suffering from anguish, you feel extreme pain or severe sorrow.</p>
<p class='rw-defn idx4' style='display:none'>When you assuage an unpleasant feeling, you make it less painful or severe, thereby calming it.</p>
<p class='rw-defn idx5' style='display:none'>Something austere is simple and plain in its style; an austere person is strict and severe with themselves.</p>
<p class='rw-defn idx6' style='display:none'>A copious amount of something is a very large amount of it.</p>
<p class='rw-defn idx7' style='display:none'>A cornucopia is a large quantity and variety of something good and nourishing.</p>
<p class='rw-defn idx8' style='display:none'>When there is a dearth of something, there is a scarcity or lack of it.</p>
<p class='rw-defn idx9' style='display:none'>A decadent person has low moral standards and is more interested in pleasure than serious matters.</p>
<p class='rw-defn idx10' style='display:none'>A deluge is a sudden, heavy downfall of rain; it can also be a large number of things, such as papers or e-mails, that someone gets all at the same time, making them very difficult to handle.</p>
<p class='rw-defn idx11' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx12' style='display:none'>If something discomfits you, it makes you feel embarrassed, confused, uncomfortable, or frustrated.</p>
<p class='rw-defn idx13' style='display:none'>If something disconcerts you, it makes you feel anxious, worried, or confused.</p>
<p class='rw-defn idx14' style='display:none'>When someone feels disquiet about a situation, they feel very worried or nervous.</p>
<p class='rw-defn idx15' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx16' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx17' style='display:none'>Someone who is epicurean derives great pleasure in material and sensual things, especially good food and drink.</p>
<p class='rw-defn idx18' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx19' style='display:none'>Someone who is frugal spends very little money—and even then only on things that are absolutely necessary.</p>
<p class='rw-defn idx20' style='display:none'>Something that is glutinous is thick and very sticky, often in an unpleasant way.</p>
<p class='rw-defn idx21' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx22' style='display:none'>If someone is imperturbable, they are always calm and not easily upset or disturbed by any situation, even dangerous ones.</p>
<p class='rw-defn idx23' style='display:none'>If something is inordinate, it is much larger in amount or degree than is normally expected.</p>
<p class='rw-defn idx24' style='display:none'>When you behave with moderation, you live in a balanced and measured way; you do nothing to excess.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx26' style='display:none'>If someone is being obsequious, they are trying so hard to please someone that they lack sincerity in their actions towards that person.</p>
<p class='rw-defn idx27' style='display:none'>A paucity of something is not enough of it.</p>
<p class='rw-defn idx28' style='display:none'>A plethora of something is too much of it.</p>
<p class='rw-defn idx29' style='display:none'>If you suffer privation, you live without many of the basic things required for a comfortable life.</p>
<p class='rw-defn idx30' style='display:none'>If you are ravenous, you are extremely hungry.</p>
<p class='rw-defn idx31' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx32' style='display:none'>Stores that are replete with goods to sell are well-stocked and completely supplied to satisfy many customers.</p>
<p class='rw-defn idx33' style='display:none'>Repugnance is a strong feeling of dislike for something or someone you find horrible and offensive.</p>
<p class='rw-defn idx34' style='display:none'>If you are acting in a saccharine fashion, you are being way too sugary sweet or are being extremely sentimental, both of which can be irritating to others.</p>
<p class='rw-defn idx35' style='display:none'>A salubrious place or area is pleasant, clean, healthy, and comfortable to live in.</p>
<p class='rw-defn idx36' style='display:none'>A salutary experience is beneficial to you since it strengthens you in some way, although it may be unpleasant as you undergo it; this word also refers to promoting good health.</p>
<p class='rw-defn idx37' style='display:none'>If something, such as food or drink, satiates you, it satisfies your need or desire so completely that you often feel that you have had too much.</p>
<p class='rw-defn idx38' style='display:none'>Something that is superfluous is unnecessary; it is more than what is wanted or needed at the current time.</p>
<p class='rw-defn idx39' style='display:none'>If you have a surfeit of something, you have much more than what you need.</p>
<p class='rw-defn idx40' style='display:none'>A sybarite is very fond of expensive and luxurious things; therefore, they are devoted to a life of pleasure.</p>
<p class='rw-defn idx41' style='display:none'>Sycophants praise people in authority or those who have considerable influence in order to seek some favor from them in return.</p>
<p class='rw-defn idx42' style='display:none'>If you truncate something, you make it shorter or quicker by removing or cutting off part of it.</p>
<p class='rw-defn idx43' style='display:none'>An unctuous person acts in an overly deceptive manner that is obviously insincere because they want to convince you of something.</p>
<p class='rw-defn idx44' style='display:none'>A voracious person has a strong desire to want a lot of something, especially food.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>cloy</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='cloy#' id='pronounce-sound' path='audio/words/amy-cloy'></a>
kloi
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='cloy#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='cloy#' id='context-sound' path='audio/wordcontexts/brian-cloy'></a>
The excessively sweet Turkish Delight that the White Witch fed Edmund did not <em>cloy</em> or sicken him from eating so much, but rather left him wishing for more.  This, of course, was magical food; any dessert of which you might eat too much would <em>cloy</em> or be too much for the body and make it sick.  Indeed, some people&#8217;s <em>cloying</em> personalities are sweet at first but soon become more than anyone wants to deal with.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of something that might <em>cloy</em> after a while?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A thick chocolate pudding that is just too sugary and too tempting.
</li>
<li class='choice '>
<span class='result'></span>
A piece of cheese that has dried out and is now too hard to ingest.
</li>
<li class='choice '>
<span class='result'></span>
Sweet gum that loses its flavor after only a few minutes of chewing.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='cloy#' id='definition-sound' path='audio/wordmeanings/amy-cloy'></a>
Something that is <em>cloying</em> is way too sweet and tempting; therefore, it nauseates you or makes you disgusted after a while because you&#8217;re inclined to have too much of it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>overfill</em>
</span>
</span>
</div>
<a class='quick-help' href='cloy#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='cloy#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/cloy/memory_hooks/3451.json'></span>
<p>
<span class="emp0"><span>J<span class="emp3"><span>oy</span></span> to <span class="emp1"><span>Cl</span></span>early Ann<span class="emp3"><span>oy</span></span></span></span> Something that is sweet at first brings j<span class="emp3"><span>oy</span></span>, but after too much of it can both <span class="emp1"><span>cl</span></span><span class="emp3"><span>oy</span></span> and <span class="emp1"><span>cl</span></span>early ann<span class="emp3"><span>oy</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="cloy#" id="add-public-hook" style="" url="https://membean.com/mywords/cloy/memory_hooks">Use other public hook</a>
<a href="cloy#" id="memhook-use-own" url="https://membean.com/mywords/cloy/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='cloy#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Like the rest of [Chef Jeremiah] Tower's menu, desserts feel vaguely retro but completely now. . . . Chocolate layer cake isn't the diner slab you might picture, but a dense, fudgy rectangle whose sweet intensity, miraculously, doesn't <b>cloy</b>.
<cite class='attribution'>
&mdash;
The New York Daily News
</cite>
</li>
<li>
Sweet enough to deliver a message about creating your own destiny, but with just enough sour grapes not to <b>cloy</b>, the movie [_Megamind_] delivers pure pleasure from start to finish.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
You can smell September in the air. Sunny mornings are dew-drenched and pungent with myrtle and cistus. . . . Daylight is lessening daily, growth is slowed and the scent of decay is beginning to <b>cloy</b>.
<cite class='attribution'>
&mdash;
The London Times
</cite>
</li>
<li>
[Michael] O'Sullivan [movie critic] enjoyed the movie [_The Secret Life of Bees_] but said it would "<b>cloy</b> or delight, depending on your blood sugar level and your tolerance for syrupy sentiment."
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/cloy/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='cloy#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>cloy</td>
<td>
&rarr;
</td>
<td class='meaning'>nail</td>
</tr>
</table>
<p>When something is <em>cloying</em>, it&#8217;s as if it&#8217;s &#8220;nailed&#8221; onto you because you can&#8217;t get rid of it easily.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='cloy#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Cloy" src="https://cdn0.membean.com/public/images/wordimages/cons2/cloy.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='cloy#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>anguish</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>copious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>cornucopia</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>decadent</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>deluge</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>discomfit</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>disconcert</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>disquiet</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>glutinous</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>inordinate</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>obsequious</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>plethora</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>ravenous</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>replete</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>repugnance</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>saccharine</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>satiate</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>superfluous</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>surfeit</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>sybarite</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>sycophant</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>unctuous</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>voracious</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abstemious</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abstinence</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>allay</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>assuage</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>austere</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>dearth</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>epicurean</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>frugal</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>imperturbable</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>moderation</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>paucity</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>privation</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>salubrious</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>salutary</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>truncate</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="cloy" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>cloy</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="cloy#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

