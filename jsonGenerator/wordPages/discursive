
<!DOCTYPE html>
<html>
<head>
<title>Word: discursive | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx1' style='display:none'>When a person is speaking or writing in a bombastic fashion, they are very self-centered, extremely showy, and excessively proud.</p>
<p class='rw-defn idx2' style='display:none'>Brevity is communicating by using just a few words or by taking very little time to do so.</p>
<p class='rw-defn idx3' style='display:none'>A circuitous route, journey, or piece of writing is long and complicated rather than simple and direct.</p>
<p class='rw-defn idx4' style='display:none'>A cogent reason or argument is strong and convincing.</p>
<p class='rw-defn idx5' style='display:none'>Something that is desultory is done in a way that is unplanned, disorganized, and without direction.</p>
<p class='rw-defn idx6' style='display:none'>Errant things or people behave in a way that is unacceptable or wrong, such as a missile that travels in the wrong direction or a student who does not follow classroom rules.</p>
<p class='rw-defn idx7' style='display:none'>To expatiate upon a subject is to speak or write in detail and at length about it.</p>
<p class='rw-defn idx8' style='display:none'>A garrulous person talks a lot, especially about unimportant things.</p>
<p class='rw-defn idx9' style='display:none'>Grandiloquent speech is highly formal, exaggerated, and often seems both silly and hollow because it is expressly used to appear impressive and important.</p>
<p class='rw-defn idx10' style='display:none'>If you describe a situation or process as labyrinthine, you mean that it is very complicated, involved, and difficult to understand.</p>
<p class='rw-defn idx11' style='display:none'>A person who is being laconic uses very few words to say something.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is loquacious talks too much.</p>
<p class='rw-defn idx13' style='display:none'>If someone is lucid, they are able to understand things and think clearly; this adjective also describes writing or speech that is crystal clear and easy to understand.</p>
<p class='rw-defn idx14' style='display:none'>A peregrination is a long journey or act of traveling from place to place, especially by foot.</p>
<p class='rw-defn idx15' style='display:none'>A pithy statement or piece of writing is brief but intelligent; it is also precise and to the point.</p>
<p class='rw-defn idx16' style='display:none'>Prattle is mindless chatter that seems like it will never stop.</p>
<p class='rw-defn idx17' style='display:none'>Something that is prolix, such as a lecture or speech, is excessively wordy; consequently, it can be tiresome to read or listen to.</p>
<p class='rw-defn idx18' style='display:none'>To ramble is to wander about leisurely, with no specific destination in mind; to ramble while speaking is to talk with no particular aim or point intended.</p>
<p class='rw-defn idx19' style='display:none'>If someone is sententious, they are terse or brief in writing and speech; they also often use proverbs to appear morally upright and wise.</p>
<p class='rw-defn idx20' style='display:none'>Something that is succinct is clearly and briefly explained without using any unnecessary words.</p>
<p class='rw-defn idx21' style='display:none'>A taciturn person is quiet or silent by nature.</p>
<p class='rw-defn idx22' style='display:none'>To be terse in speech is to be short and to the point, often in an abrupt manner that may seem unfriendly.</p>
<p class='rw-defn idx23' style='display:none'>Something that is verbose, such as a speech or article, contains too many words.</p>
<p class='rw-defn idx24' style='display:none'>A voluble person can speak easily with speech that naturally flows; voluble people also tend to be quite chatty.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>discursive</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='discursive#' id='pronounce-sound' path='audio/words/amy-discursive'></a>
di-SKUR-siv
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='discursive#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='discursive#' id='context-sound' path='audio/wordcontexts/brian-discursive'></a>
Tolbert&#8217;s writing is excessively lengthy and <em>discursive</em> since he wanders off topic often in his essays.  Most professors frown upon his long-winded, winding, and <em>discursive</em> style.  Tolbert, however, does manage to present interesting material, even if his arguments excessively wander and are endlessly <em>discursive</em>.  Even though his writings are <em>discursive</em>, long, and hard to follow, I find their roaming entertaining.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of <em>discursive</em> writing?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A short and moving poem that contains beautiful descriptions.
</li>
<li class='choice answer '>
<span class='result'></span>
A rambling biography full of stories that don&#8217;t relate to the subject.
</li>
<li class='choice '>
<span class='result'></span>
A persuasive essay that is meant to be read as a speech.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='discursive#' id='definition-sound' path='audio/wordmeanings/amy-discursive'></a>
A piece of writing is <em>discursive</em> if it includes a lot of information that is not relevant to the main subject.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>rambling</em>
</span>
</span>
</div>
<a class='quick-help' href='discursive#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='discursive#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/discursive/memory_hooks/4830.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Div</span></span>ing <span class="emp1"><span>Curs</span></span>e</span></span> Jen's <span class="emp3"><span>div</span></span>ing coach was so <span class="emp3"><span>d</span></span>is<span class="emp1"><span>curs</span></span><span class="emp3"><span>iv</span></span>e in his talking about <span class="emp3"><span>div</span></span>ing that Jen's team never actually did any <span class="emp3"><span>div</span></span>ing until their fifth practice; by then Jen had totally lost interest, and <span class="emp1"><span>curs</span></span>ed his silly <span class="emp3"><span>d</span></span>is<span class="emp1"><span>curs</span></span><span class="emp3"><span>iv</span></span>e coaching style.
</p>
</div>

<div id='memhook-button-bar'>
<a href="discursive#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/discursive/memory_hooks">Use other hook</a>
<a href="discursive#" id="memhook-use-own" url="https://membean.com/mywords/discursive/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='discursive#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Footnotes are reminders that scholarship is an intrinsically communal enterprise—building on, revising or replacing the work of predecessors. . . . Regardless of where the publisher puts them, these <b>discursive</b> notes are like the cloakrooms of Congress: they're the places where the author takes the reader into his confidence, revealing what he really thinks about his colleagues.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
At the end of this diverting, informative and <b>discursive</b> book, her love for crosswords is clear, but her reasons—despite a determined effort on her part to explain them—remain, in the end, a puzzle of their own.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Cats are the vessels for John Gray’s austere worldview in his new book, _Feline Philosophy: Cats and the Meaning of Life._ . . . Cats are a perfectly adequate MacGuffin for this pleasant ramble through what philosophy can and can’t help us with. And it’s hard to resist the usual pleasures of reading Gray. His <b>discursive</b> style is always beguiling. His wide reading yields lovely digressions in the company of Colette, Patricia Highsmith and Mary Gaitskill, among others.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Starring, among others, Donald Glover. Who, you may recall, was the subject of a social media campaign last year. Twitter users busted out the hashtag #donald4spiderman to encourage Hollywood to cast him as Peter Parker in the reboot of the Spider-Man film franchise. (We covered that campaign in this admittedly <b>discursive</b> post last year—scroll down, waaaaay down, past all that pontificating about what Twitter is "for.")
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/discursive/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='discursive#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='dis_apart' data-tree-url='//cdn1.membean.com/public/data/treexml' href='discursive#'>
<span class='common'></span>
dis-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>apart, not, away from</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='curs_ran' data-tree-url='//cdn1.membean.com/public/data/treexml' href='discursive#'>
<span class=''></span>
curs
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>ran, hurried</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ive_does' data-tree-url='//cdn1.membean.com/public/data/treexml' href='discursive#'>
<span class=''></span>
-ive
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or that which does something</td>
</tr>
</table>
<p>A <em>discursive</em> writing style &#8220;runs away or apart from&#8221; its focus, thereby adding a great deal of inessential information.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='discursive#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Monty Python's Flying Circus</strong><span> This man is rather discursive in his speech.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/discursive.jpg' video_url='examplevids/discursive' video_width='350'></span>
<div id='wt-container'>
<img alt="Discursive" height="288" src="https://cdn1.membean.com/video/examplevids/discursive.jpg" width="350" />
<div class='center'>
<a href="discursive#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='discursive#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Discursive" src="https://cdn0.membean.com/public/images/wordimages/cons2/discursive.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='discursive#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>bombastic</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>circuitous</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>desultory</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>errant</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>expatiate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>garrulous</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>grandiloquent</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>labyrinthine</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>loquacious</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>peregrination</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>prattle</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>prolix</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>ramble</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>verbose</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>voluble</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>brevity</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>cogent</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>laconic</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>lucid</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>pithy</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>sententious</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>succinct</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>taciturn</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>terse</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="discursive" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>discursive</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="discursive#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

