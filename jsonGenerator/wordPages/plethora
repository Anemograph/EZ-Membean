
<!DOCTYPE html>
<html>
<head>
<title>Word: plethora | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Plethora-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/plethora-large.jpg?qdep8" />
</div>
<a href="plethora#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>Something that is cloying is way too sweet and tempting; therefore, it nauseates you or makes you disgusted after a while because you&#8217;re inclined to have too much of it.</p>
<p class='rw-defn idx2' style='display:none'>A copious amount of something is a very large amount of it.</p>
<p class='rw-defn idx3' style='display:none'>A cornucopia is a large quantity and variety of something good and nourishing.</p>
<p class='rw-defn idx4' style='display:none'>If something, such as a sound, is undergoing a crescendo, it is getting louder and louder or increasing in intensity and is about to reach its peak strength.</p>
<p class='rw-defn idx5' style='display:none'>When there is a dearth of something, there is a scarcity or lack of it.</p>
<p class='rw-defn idx6' style='display:none'>A deficit occurs when a person or government spends more money than has been received.</p>
<p class='rw-defn idx7' style='display:none'>A deluge is a sudden, heavy downfall of rain; it can also be a large number of things, such as papers or e-mails, that someone gets all at the same time, making them very difficult to handle.</p>
<p class='rw-defn idx8' style='display:none'>When you deplete a supply of something, you use it up or lessen its amount over a given period of time.</p>
<p class='rw-defn idx9' style='display:none'>When an area is devoid of life, it is empty or completely lacking in it.</p>
<p class='rw-defn idx10' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx11' style='display:none'>When an amount of something dwindles, it becomes less and less over time.</p>
<p class='rw-defn idx12' style='display:none'>An exiguous amount of something is meager, small, or limited in nature.</p>
<p class='rw-defn idx13' style='display:none'>An exorbitant price or fee is much higher than what it should be or what is considered reasonable.</p>
<p class='rw-defn idx14' style='display:none'>If something is inordinate, it is much larger in amount or degree than is normally expected.</p>
<p class='rw-defn idx15' style='display:none'>A paucity of something is not enough of it.</p>
<p class='rw-defn idx16' style='display:none'>If something plummets, it falls down from a high position very quickly; for example, a piano can plummet from a window and a stock value can plummet during a market crash.</p>
<p class='rw-defn idx17' style='display:none'>A preponderance of things of a particular type in a group means that there are more of that type than of any other.</p>
<p class='rw-defn idx18' style='display:none'>If you suffer privation, you live without many of the basic things required for a comfortable life.</p>
<p class='rw-defn idx19' style='display:none'>A profusion of something is a very large quantity or variety of it.</p>
<p class='rw-defn idx20' style='display:none'>Something that is prolix, such as a lecture or speech, is excessively wordy; consequently, it can be tiresome to read or listen to.</p>
<p class='rw-defn idx21' style='display:none'>A scant amount of something is a very limited or slight amount of it; hence, it is inadequate or insufficient in size or quantity.</p>
<p class='rw-defn idx22' style='display:none'>Something that is succinct is clearly and briefly explained without using any unnecessary words.</p>
<p class='rw-defn idx23' style='display:none'>Something that is superfluous is unnecessary; it is more than what is wanted or needed at the current time.</p>
<p class='rw-defn idx24' style='display:none'>If you have a surfeit of something, you have much more than what you need.</p>
<p class='rw-defn idx25' style='display:none'>When you are presented with a synoptic view of a written work or subject, you receive a summary or general overview of the entirety of its contents.</p>
<p class='rw-defn idx26' style='display:none'>If you truncate something, you make it shorter or quicker by removing or cutting off part of it.</p>
<p class='rw-defn idx27' style='display:none'>Something that is verbose, such as a speech or article, contains too many words.</p>
<p class='rw-defn idx28' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>plethora</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='plethora#' id='pronounce-sound' path='audio/words/amy-plethora'></a>
PLETH-er-uh
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='plethora#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='plethora#' id='context-sound' path='audio/wordcontexts/brian-plethora'></a>
We have a <em>plethora</em> of tomatoes&#8212;there seems to be no end to these red spheres growing on the vines! This overabundance or <em>plethora</em> has us overrun, and we are finding a need to be creative in their use. We have already given a <em>plethora</em> or excessive amount to the neighbors, who thanked us for several overflowing bags. We have made five gallons of tomato sauce and also a <em>plethora</em> of salsa&#8212;it seems that we could open our own Italian and Mexican restaurants!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>plethora</em> of something?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A rotten crate of apples that leads to a shortage in a store that week.
</li>
<li class='choice '>
<span class='result'></span>
A nice variety of apples in a store from which customers can choose.
</li>
<li class='choice answer '>
<span class='result'></span>
An extra shipment of apples to a store so there are too many to sell that week.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='plethora#' id='definition-sound' path='audio/wordmeanings/amy-plethora'></a>
A <em>plethora</em> of something is too much of it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>excess</em>
</span>
</span>
</div>
<a class='quick-help' href='plethora#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='plethora#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/plethora/memory_hooks/2975.json'></span>
<p>
<span class="emp0"><span>A Plethora of Plethoras</span></span>plethora plethora plethora plethora plethora plethora plethora plethora plethora plethoraplethora plethora plethora plethora plethora plethora plethora plethora plethora plethoraplethora plethora plethora plethora plethora plethora plethora plethora plethora plethoraplethora plethora plethora plethora plethora plethora plethora plethora plethora plethoraplethora plethora plethora plethora plethora plethora plethora plethora plethora plethoraplethora plethora plethora plethora plethora plethora plethora plethora plethora plethoraplethora plethora plethora plethora plethora plethora plethora plethora plethora plethoraplethora plethora plethora plethora plethora plethora plethora plethora plethora plethoraplethora plethora plethora plethora plethora plethora plethora plethora plethora plethoraplethora plethora plethora plethora plethora plethora plethora plethora plethora plethora
</p>
</div>

<div id='memhook-button-bar'>
<a href="plethora#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/plethora/memory_hooks">Use other hook</a>
<a href="plethora#" id="memhook-use-own" url="https://membean.com/mywords/plethora/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='plethora#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
When money was cheap a <b>plethora</b> of companies, many from the former Soviet Union, raised hundreds of millions of pounds by listing their shares in London.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
President-elect Joe Biden has been critical of the <b>plethora</b> of tax giveaways, but he will find that both Democrats and Republicans have been steadfast in their support of certain tax breaks.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
What’s more, a <b>plethora</b> of electronic devices causes new worries, including passengers who are distracted by them during crises and charging cords that could trip evacuees in an emergency.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
So my son’s lessons are instead given analogically, throughout the days, by my wife and me, with a <b>plethora</b> of mistakes and uncertainties that are rapidly eroding any trust our son used to have in us.
<cite class='attribution'>
&mdash;
The New Yorker
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/plethora/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='plethora#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>pleth</td>
<td>
&rarr;
</td>
<td class='meaning'>be full</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='a_noun' data-tree-url='//cdn1.membean.com/public/data/treexml' href='plethora#'>
<span class=''></span>
-a
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>forms a Latin noun</td>
</tr>
</table>
<p>A <em>plethora</em> of something is &#8220;being&#8221; too &#8220;full.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='plethora#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Plethora" src="https://cdn3.membean.com/public/images/wordimages/cons2/plethora.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='plethora#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>cloy</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>copious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>cornucopia</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>crescendo</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>deluge</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>exorbitant</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>inordinate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>preponderance</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>profusion</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>prolix</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>superfluous</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>surfeit</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>verbose</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>dearth</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>deficit</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>deplete</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>devoid</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>dwindle</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>exiguous</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>paucity</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>plummet</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>privation</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>scant</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>succinct</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>synoptic</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>truncate</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="plethora" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>plethora</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="plethora#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

