
<!DOCTYPE html>
<html>
<head>
<title>Word: tantamount | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If one thing is analogous to another, a comparison can be made between the two because they are similar in some way.</p>
<p class='rw-defn idx1' style='display:none'>If you describe a decision, rule, or plan as arbitrary, you think that it was decided without any thought, standard, or system to guide it; therefore, it can seem unfair.</p>
<p class='rw-defn idx2' style='display:none'>When you bandy about ideas with someone, you casually discuss them without caring if the discussion is informed or effective in any way.</p>
<p class='rw-defn idx3' style='display:none'>Bigotry is the expression of strong and unreasonable opinions without accepting or tolerating opposing views.</p>
<p class='rw-defn idx4' style='display:none'>Someone who is capricious changes their mind repeatedly or behaves in unexpected ways; a capricious series of events follows no predictable pattern.</p>
<p class='rw-defn idx5' style='display:none'>Commutation is the act of changing a punishment to one that is less severe.</p>
<p class='rw-defn idx6' style='display:none'>When you correlate two things, you compare, associate, or equate them together in some way.</p>
<p class='rw-defn idx7' style='display:none'>To disburse is to pay out money, usually from a large fund that has been collected for a specific purpose.</p>
<p class='rw-defn idx8' style='display:none'>A small group or issue that is factious is controversial and promotes strong disagreement or dissatisfaction within a larger group.</p>
<p class='rw-defn idx9' style='display:none'>Two items are fungible if one can be exchanged for the other with no loss in inherent value; for example, one $10 bill and two $5 bills are fungible.</p>
<p class='rw-defn idx10' style='display:none'>The word homogeneous is used to describe a group that has members or parts which are similar or are all of the same type.</p>
<p class='rw-defn idx11' style='display:none'>Things that are homologous are similar in structure, function, or value; these qualities may suggest or indicate a common ancestor or origin.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is impetuous does things quickly and rashly without thinking carefully first.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is indiscreet shows lack of judgment, especially because they say or do things in public that should only be said or done privately—if at all.</p>
<p class='rw-defn idx14' style='display:none'>If someone has a jaundiced view of something, they ignore the good parts and can only see the bad aspects of it.</p>
<p class='rw-defn idx15' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx16' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx17' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx18' style='display:none'>If you reciprocate, you give something to someone because they have given something similar to you.</p>
<p class='rw-defn idx19' style='display:none'>When you offer recompense to someone, you give them something, usually money, for the trouble or loss that you have caused them or as payment for their help.</p>
<p class='rw-defn idx20' style='display:none'>To recoup is to get back an amount of money you have lost or spent.</p>
<p class='rw-defn idx21' style='display:none'>If you redress a complaint or a bad situation, you correct or improve it for the person who has been wronged, usually by paying them money or offering an apology.</p>
<p class='rw-defn idx22' style='display:none'>Someone&#8217;s remuneration is the payment or other rewards they receive for work completed, goods provided, or services rendered.</p>
<p class='rw-defn idx23' style='display:none'>Restitution is the formal act of giving something that was taken away back to the rightful owner—or paying them money for the loss of the object.</p>
<p class='rw-defn idx24' style='display:none'>A scrupulous person takes great care to do everything in an extremely honest, conscientious, and fair manner.</p>
<p class='rw-defn idx25' style='display:none'>Something statutory, such as the power given to a governor or president, is created, established, and controlled by rules and laws; hence, it has the full force of the law behind it and must be followed.</p>
<p class='rw-defn idx26' style='display:none'>Synergy is the extra energy or additional effectiveness gained when two groups or organizations combine their efforts instead of working separately.</p>
<p class='rw-defn idx27' style='display:none'>If you transpose two things, you make them change places or reverse their normal order.</p>
<p class='rw-defn idx28' style='display:none'>If you feel unrequited love for another, you love that person, but they don&#8217;t love you in return.</p>
<p class='rw-defn idx29' style='display:none'>A wanton action deliberately harms someone or damages something for no apparent or good reason.</p>
<p class='rw-defn idx30' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>tantamount</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='tantamount#' id='pronounce-sound' path='audio/words/amy-tantamount'></a>
TAN-tuh-mount
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='tantamount#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='tantamount#' id='context-sound' path='audio/wordcontexts/brian-tantamount'></a>
Olympic officials are discussing whether an athlete&#8217;s admitting to using performance enhancing steroids is <em>tantamount</em>, or equal to, a positive drug test.  They argue that admitting to taking drugs is <em>tantamount</em> to or the same as being caught because any use of steroids whatsoever gives an athlete an unfair advantage.  Athletes are realizing that disclosing such information is <em>tantamount</em> or equivalent to being removed from the Olympics, and maybe even professional sports, for good.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of something that is considered <em>tantamount</em> to something else?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A suspect&#8217;s refusal to speak in court that is viewed as an admission of guilt.
</li>
<li class='choice '>
<span class='result'></span>
A boy&#8217;s carelessness that causes him to fall out of a tree and break his arm. 
</li>
<li class='choice '>
<span class='result'></span>
A mother&#8217;s desire to keep her children safe that is stronger than her desire to keep herself safe.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='tantamount#' id='definition-sound' path='audio/wordmeanings/amy-tantamount'></a>
If one thing is <em>tantamount</em> to another thing, it means that it is equivalent to the other.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>same</em>
</span>
</span>
</div>
<a class='quick-help' href='tantamount#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='tantamount#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/tantamount/memory_hooks/3550.json'></span>
<p>
<span class="emp0"><span>Accoun<span class="emp1"><span>tant</span></span> <span class="emp2"><span>Amount</span></span></span></span> It is important that we report the <span class="emp2"><span>amount</span></span> of money accurately from the accoun<span class="emp1"><span>tant</span></span> so that it appears to be <span class="emp1"><span>tant</span></span><span class="emp2"><span>amount</span></span> with the accoun<span class="emp1"><span>tant</span></span>'s figures, or the accoun<span class="emp1"><span>tant</span></span> <span class="emp2"><span>amount</span></span>, as well as our <span class="emp2"><span>amount</span></span>, will be called into question!
</p>
</div>

<div id='memhook-button-bar'>
<a href="tantamount#" id="add-public-hook" style="" url="https://membean.com/mywords/tantamount/memory_hooks">Use other public hook</a>
<a href="tantamount#" id="memhook-use-own" url="https://membean.com/mywords/tantamount/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='tantamount#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Victory attained by violence is <b>tantamount</b> to a defeat, for it is momentary.
<span class='attribution'>&mdash; Mahatma Gandhi, Indian lawyer and political leader, known for his doctrine of nonviolent protest</span>
<img alt="Mahatma gandhi, indian lawyer and political leader, known for his doctrine of nonviolent protest" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Mahatma Gandhi, Indian lawyer and political leader, known for his doctrine of nonviolent protest.jpg?qdep8" width="80" />
</li>
<li>
While for most artists, putting together a greatest hits is <b>tantamount</b> to lining up the No. 1's, Dylan’s body of work is far more complex.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
<li>
Simply put, both sides have committed acts <b>tantamount</b> to "war crimes," and both continue to violate international law repeatedly in this nightmare.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
Making an environmentally conscious building required commitment from the outset, and now that commitment has paid off: on April 16, the African American History Museum was officially awarded a Gold certification by the U.S. Green Building Council’s Leadership in Energy and Environmental Design (LEED) program. In the architecture business, this type of recognition is <b>tantamount</b> to an eco-Oscar.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/tantamount/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='tantamount#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>tant</td>
<td>
&rarr;
</td>
<td class='meaning'>such great, so great</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='a_towards' data-tree-url='//cdn1.membean.com/public/data/treexml' href='tantamount#'>
<span class='common'></span>
a-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to, towards, near</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='mount_mountain' data-tree-url='//cdn1.membean.com/public/data/treexml' href='tantamount#'>
<span class=''></span>
mount
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>mountain</td>
</tr>
</table>
<p>The idea behind <em>tantamount</em> is that one gives &#8220;so great&#8221; an amount &#8220;to (one) mountain&#8221; as to the next. &#8220;Mountain&#8221; in this case refers to a pile of something, like coins.  Making one thing <em>tantamount</em> to another means making them equivalent by giving each of them similar amounts.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='tantamount#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Tantamount" src="https://cdn2.membean.com/public/images/wordimages/cons2/tantamount.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='tantamount#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>analogous</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bandy</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>correlate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>disburse</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>fungible</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>homogeneous</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>homologous</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>reciprocate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>recompense</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>recoup</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>redress</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>remuneration</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>restitution</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>scrupulous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>statutory</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>synergy</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>transpose</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>arbitrary</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bigotry</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>capricious</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>commutation</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>factious</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>impetuous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>indiscreet</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>jaundiced</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>unrequited</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>wanton</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="tantamount" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>tantamount</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="tantamount#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

