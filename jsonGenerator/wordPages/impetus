
<!DOCTYPE html>
<html>
<head>
<title>Word: impetus | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>If you abjure a belief or a way of behaving, you state publicly that you will give it up or reject it.</p>
<p class='rw-defn idx2' style='display:none'>If someone in authority abolishes a law or practice, they formally put an end to it.</p>
<p class='rw-defn idx3' style='display:none'>To abrogate an act is to end it by official and sometimes legal means.</p>
<p class='rw-defn idx4' style='display:none'>When you adjure someone to do something, you persuade, eagerly appeal, or solemnly order them to do it.</p>
<p class='rw-defn idx5' style='display:none'>When you admonish someone, you tell them gently but with seriousness that they have done something wrong; you usually caution and advise them not to do it again.</p>
<p class='rw-defn idx6' style='display:none'>If something is done at someone&#8217;s behest, it is done because they urgently ask for it or authoritatively order it to happen.</p>
<p class='rw-defn idx7' style='display:none'>You cajole people by gradually persuading them to do something, usually by being very nice to them.</p>
<p class='rw-defn idx8' style='display:none'>If something moves or grows with celerity, it does so rapidly.</p>
<p class='rw-defn idx9' style='display:none'>If you circumvent something, such as a rule or restriction, you try to get around it in a clever and perhaps dishonest way.</p>
<p class='rw-defn idx10' style='display:none'>To confute an argument is to prove it to be thoroughly false; to confute a person is to prove them to be wrong.</p>
<p class='rw-defn idx11' style='display:none'>To contravene a law, rule, or agreement is to do something that is not allowed or is forbidden by that law, rule, or agreement.</p>
<p class='rw-defn idx12' style='display:none'>If you exhort someone to do something, you try very hard to persuade them to do it.</p>
<p class='rw-defn idx13' style='display:none'>When you expedite an action or process, you make it happen more quickly.</p>
<p class='rw-defn idx14' style='display:none'>If you expostulate with someone, you express strong disagreement with or disapproval of what that person is doing.</p>
<p class='rw-defn idx15' style='display:none'>If you expunge something, you completely remove it or do away with it.</p>
<p class='rw-defn idx16' style='display:none'>If you extirpate something, you completely destroy it because it is unpleasant or unwanted.</p>
<p class='rw-defn idx17' style='display:none'>If you gainsay something, you say that it is not true and therefore reject it.</p>
<p class='rw-defn idx18' style='display:none'>An injunction is a court order that prevents someone from doing something.</p>
<p class='rw-defn idx19' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx20' style='display:none'>If you recant, you publicly announce that your once firmly held beliefs or statements were wrong and that you no longer agree with them.</p>
<p class='rw-defn idx21' style='display:none'>If you renege on a deal, agreement, or promise, you do not do what you promised or agreed to do.</p>
<p class='rw-defn idx22' style='display:none'>If you repudiate something, you state that you do not accept or agree with it and no longer want to be connected with it in any way.</p>
<p class='rw-defn idx23' style='display:none'>When someone in power rescinds a law or agreement, they officially end it and state that it is no longer valid.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>impetus</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='impetus#' id='pronounce-sound' path='audio/words/amy-impetus'></a>
IM-pi-tuhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='impetus#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='impetus#' id='context-sound' path='audio/wordcontexts/brian-impetus'></a>
Marcel wanted to play in the band, and this hope was the <em>impetus</em> or motivation that made him practice the drums every day.  Marcel&#8217;s mother had never known him to be so dedicated, and even though she hated rock music, she was grateful for the energetic push or <em>impetus</em> that the band had given her son.  As the clash of drums sounded in the garage, Marcel&#8217;s neighbors wondered if the painful noise might become the <em>impetus</em> or stimulus for their departure from the neighborhood!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of an <em>impetus</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A very tasty treat that you get to have for your lunch.
</li>
<li class='choice '>
<span class='result'></span>
A tree that falls onto a mountain road and blocks traffic. 
</li>
<li class='choice answer '>
<span class='result'></span>
A lack of spending money that leads a teenager to find a job.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='impetus#' id='definition-sound' path='audio/wordmeanings/amy-impetus'></a>
An <em>impetus</em> is a force or influence that makes something happen or develop more quickly.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>stimulus</em>
</span>
</span>
</div>
<a class='quick-help' href='impetus#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='impetus#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/impetus/memory_hooks/6255.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Pet</span></span> <span class="emp3"><span>Us</span></span>!</span></span> If my two dogs could talk, they would say nothing but "<span class="emp2"><span>Pet</span></span> <span class="emp3"><span>us</span></span>!  <span class="emp2"><span>Pet</span></span> <span class="emp3"><span>us</span></span>!  <span class="emp2"><span>Pet</span></span> <span class="emp3"><span>us</span></span>!"  They have quite the im<span class="emp2"><span>pet</span></span><span class="emp3"><span>us</span></span> towards being <span class="emp2"><span>pet</span></span>ted!
</p>
</div>

<div id='memhook-button-bar'>
<a href="impetus#" id="add-public-hook" style="" url="https://membean.com/mywords/impetus/memory_hooks">Use other public hook</a>
<a href="impetus#" id="memhook-use-own" url="https://membean.com/mywords/impetus/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='impetus#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
And I think one of the things that people tend to forget is that poets do write out of life. It isn't some set piece that then gets put up on the shelf, but that the <b>impetus</b>, the real instigation for poetry is everything that's happening around us.
<span class='attribution'>&mdash; Rita Dove, American poet and essayist</span>
<img alt="Rita dove, american poet and essayist" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Rita Dove, American poet and essayist.jpg?qdep8" width="80" />
</li>
<li>
The first of the documents released were 300 pages from the official inquiry into the sinking of the USS Thresher on April 10, 1963. The loss of the nuclear-powered submarine and all 129 men aboard during a test dive in the Atlantic Ocean delivered a blow to national pride during the Cold War and became the <b>impetus</b> for safety improvements.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Whatever their outcome, the talks represent something of a change in the administration’s previous position that the <b>impetus</b> for peace in the Mideast must come from the parties themselves.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
The U.S. House passed a bill Thursday to create a landslide hazard reduction program. The <b>impetus</b> was not the destructive slide in Haines this week, but the Oso landslide in Washington state. That one was in 2014, and it killed 43 people. Representative Suzan DelBene of Washington has been working to get the bill passed since she first introduced it in 2016. She says it will improve landslide science and better prepare communities.
<cite class='attribution'>
&mdash;
KTOO
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/impetus/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='impetus#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='impet_impulse' data-tree-url='//cdn1.membean.com/public/data/treexml' href='impetus#'>
<span class=''></span>
impet
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>impulse, attack</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='us_noun' data-tree-url='//cdn1.membean.com/public/data/treexml' href='impetus#'>
<span class=''></span>
-us
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>forms a singular Latin noun</td>
</tr>
</table>
<p>An <em>impetus</em> makes one &#8220;attack&#8221; a project or gives an &#8220;impulse&#8221; to do something.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='impetus#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Impetus" src="https://cdn2.membean.com/public/images/wordimages/cons2/impetus.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='impetus#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='4' class = 'rw-wordform notlearned'><span>adjure</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>admonish</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>behest</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>cajole</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>celerity</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>exhort</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>expedite</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>expostulate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>injunction</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abjure</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abolish</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>abrogate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>circumvent</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>confute</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>contravene</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>expunge</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>extirpate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>gainsay</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>recant</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>renege</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>repudiate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>rescind</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="impetus" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>impetus</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="impetus#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

