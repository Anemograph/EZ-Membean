
<!DOCTYPE html>
<html>
<head>
<title>Word: condole | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>You allay someone&#8217;s fear or concern by making them feel less afraid or worried.</p>
<p class='rw-defn idx2' style='display:none'>If you alleviate pain or suffering, you make it less intense or severe.</p>
<p class='rw-defn idx3' style='display:none'>If your behavior or manner is altruistic, you show you care more about other people and their interests than you care about yourself.</p>
<p class='rw-defn idx4' style='display:none'>When you ameliorate a bad condition or situation, you make it better in some way.</p>
<p class='rw-defn idx5' style='display:none'>An anodyne is a medicine that soothes or relieves pain.</p>
<p class='rw-defn idx6' style='display:none'>Your antagonist is an opponent in a competition or battle.</p>
<p class='rw-defn idx7' style='display:none'>When you assuage an unpleasant feeling, you make it less painful or severe, thereby calming it.</p>
<p class='rw-defn idx8' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx9' style='display:none'>To augment something is to increase its value or effectiveness by adding something to it.</p>
<p class='rw-defn idx10' style='display:none'>A cavalier person does not seem to care about rules, principles, or other people&#8217;s feelings, no matter how dire or serious a situation may be.</p>
<p class='rw-defn idx11' style='display:none'>A chivalrous man behaves in a polite, kind, generous, and honorable way, especially towards women.</p>
<p class='rw-defn idx12' style='display:none'>If you commiserate with someone, you show them pity or sympathy because something bad or unpleasant has happened to them.</p>
<p class='rw-defn idx13' style='display:none'>A convalescent person spends time resting to gain health and strength after a serious illness or operation.</p>
<p class='rw-defn idx14' style='display:none'>If you curtail something, you reduce or limit it.</p>
<p class='rw-defn idx15' style='display:none'>If you treat someone with derision, you mock or make fun of them because you think they are stupid, unimportant, or useless.</p>
<p class='rw-defn idx16' style='display:none'>If you disparage someone or something, you say unpleasant words that show you have no respect for that person or thing.</p>
<p class='rw-defn idx17' style='display:none'>Draconian rules and laws are extremely strict and harsh.</p>
<p class='rw-defn idx18' style='display:none'>Empathy is the ability to understand how people feel because you can imagine what it is to be like them.</p>
<p class='rw-defn idx19' style='display:none'>If something exacerbates a problem or bad situation, it makes it even worse.</p>
<p class='rw-defn idx20' style='display:none'>If circumstances extenuate someone’s actions in a questionable situation, you feel that it was reasonable for someone to break the usual rules; therefore, you partly excuse and sympathize with their wrongdoing.</p>
<p class='rw-defn idx21' style='display:none'>If someone is implacable, they very stubbornly react to situations or the opinions of others because of strong feelings that make them unwilling to change their mind.</p>
<p class='rw-defn idx22' style='display:none'>If someone is inimical, they are unfriendly and will likely cause you harm.</p>
<p class='rw-defn idx23' style='display:none'>If someone exhibits magnanimity towards another, they show them kindness and noble generosity, especially after defeating them in battle or after having been treated badly by them.</p>
<p class='rw-defn idx24' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx25' style='display:none'>If you mollify someone, you say or do something to make that person feel less angry or upset.</p>
<p class='rw-defn idx26' style='display:none'>A palliative action makes a bad situation seem better; nevertheless, it does not actually solve the problem.</p>
<p class='rw-defn idx27' style='display:none'>A panacea is something that people think will solve all problems and make everything better.</p>
<p class='rw-defn idx28' style='display:none'>A punitive action is intended to punish someone.</p>
<p class='rw-defn idx29' style='display:none'>To quell something is to stamp out, quiet, or overcome it.</p>
<p class='rw-defn idx30' style='display:none'>If you behave in a supercilious way, you act as if you were more important or better than everyone else.</p>
<p class='rw-defn idx31' style='display:none'>If you feel unrequited love for another, you love that person, but they don&#8217;t love you in return.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>condole</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='condole#' id='pronounce-sound' path='audio/words/amy-condole'></a>
kuhn-DOHL
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='condole#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='condole#' id='context-sound' path='audio/wordcontexts/brian-condole'></a>
On the day of the funeral, Wanita&#8217;s sister Eleanor offered sympathy by <em><em>condoling</em></em> with Wanita over the loss of her beloved husband Keith.  Friends and relatives poured in to express their sorrow, <em><em>condoling</em></em> with Wanita in her need and loss.  These <em>condolences</em> or sympathetic words helped Wanita during her time of grief and suffering.  Without friends and family members to <em>condole</em> with or comfort her, she doubted she could have survived such a tragedy.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Why would you <em>condole</em> with someone?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
You want to praise them for a job well done.
</li>
<li class='choice answer '>
<span class='result'></span>
You feel sad for them because they are grieving.
</li>
<li class='choice '>
<span class='result'></span>
You need to help them plant a garden.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='condole#' id='definition-sound' path='audio/wordmeanings/amy-condole'></a>
If you <em>condole</em> with someone, you express sympathy or sorrow, usually on the death of someone dear.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>sympathize</em>
</span>
</span>
</div>
<a class='quick-help' href='condole#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='condole#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/condole/memory_hooks/4447.json'></span>
<p>
<img alt="Condole" src="https://cdn1.membean.com/public/images/wordimages/hook/condole.jpg?qdep8" />
<span class="emp0"><span><span class="emp3"><span>Condole</span></span>nce <span class="emp3"><span>Candle</span></span></span></span> At the funeral we lit a <span class="emp3"><span>condole</span></span> <span class="emp3"><span>candle</span></span> to <span class="emp3"><span>condole</span></span> with each other over the loss of our relative.
</p>
</div>

<div id='memhook-button-bar'>
<a href="condole#" id="add-public-hook" style="" url="https://membean.com/mywords/condole/memory_hooks">Use other public hook</a>
<a href="condole#" id="memhook-use-own" url="https://membean.com/mywords/condole/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='condole#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
It started with the death of an uncle two months ago. I called to <b>condole</b> with my cousin.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Strict pandemic burial protocols in the Philippines prevented a service of any kind, and no one family member was allowed to <b>condole</b> the man who had been known affectionately as "Daddy Lolo," "lolo" meaning grandfather in Filipino.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/condole/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='condole#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='con_with' data-tree-url='//cdn1.membean.com/public/data/treexml' href='condole#'>
<span class='common'></span>
con-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>with, together</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='dol_feel' data-tree-url='//cdn1.membean.com/public/data/treexml' href='condole#'>
<span class=''></span>
dol
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>feel pain, grieve, suffer</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='condole#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>To &#8220;feel pain, grieve, or suffer with&#8221; another hurt person is to <em>condole</em> with her.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='condole#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Da Vinci's Inquest</strong><span> Wishing condolences for the loss of his father.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/condole.jpg' video_url='examplevids/condole' video_width='350'></span>
<div id='wt-container'>
<img alt="Condole" height="288" src="https://cdn1.membean.com/video/examplevids/condole.jpg" width="350" />
<div class='center'>
<a href="condole#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='condole#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Condole" src="https://cdn1.membean.com/public/images/wordimages/cons2/condole.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='condole#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>allay</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>alleviate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>altruistic</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>ameliorate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>anodyne</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>assuage</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>chivalrous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>commiserate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>convalescent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>curtail</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>empathy</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>extenuate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>magnanimity</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>mollify</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>palliative</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>panacea</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>quell</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='6' class = 'rw-wordform notlearned'><span>antagonist</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>augment</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>cavalier</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>derision</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>disparage</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>draconian</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>exacerbate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>implacable</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>inimical</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>punitive</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>supercilious</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>unrequited</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='condole#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
condolences
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>fitting expressions of pity for a loss, usually of a loved one</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="condole" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>condole</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="condole#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

