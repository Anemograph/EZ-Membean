
<!DOCTYPE html>
<html>
<head>
<title>Word: potentate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Potentate-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/potentate-large.jpg?qdep8" />
</div>
<a href="potentate#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>If you abase yourself, people respect you less because you act in a way that is beneath you; due to this behavior, you are lowered or reduced in rank, esteem, or reputation. </p>
<p class='rw-defn idx1' style='display:none'>The acme of something is its highest point of achievement or excellence.</p>
<p class='rw-defn idx2' style='display:none'>An acolyte is someone who serves a leader as a devoted assistant or believes in the leader&#8217;s ideas.</p>
<p class='rw-defn idx3' style='display:none'>An adherent is a supporter or follower of a leader or a cause.</p>
<p class='rw-defn idx4' style='display:none'>The apex of anything, such as an organization or system, is its highest part or most important position.</p>
<p class='rw-defn idx5' style='display:none'>An autocratic person rules with complete power; consequently, they make decisions and give orders to people without asking them for their opinion or advice.</p>
<p class='rw-defn idx6' style='display:none'>An autonomous person makes their own decisions without being influenced by anyone else; an autonomous country or organization is independent and has the power to govern itself.</p>
<p class='rw-defn idx7' style='display:none'>A despot is a leader who has a lot of power and uses it in a cruel and unreasonable way.</p>
<p class='rw-defn idx8' style='display:none'>When one country has dominion over another, it rules or controls it absolutely.</p>
<p class='rw-defn idx9' style='display:none'>A figurehead in an organization is the apparent authority or head in name only—the real power lies somewhere else.</p>
<p class='rw-defn idx10' style='display:none'>A hierarchy is a system of organization in a society, company, or other group in which people are divided into different ranks or levels of importance.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is imperious behaves in a proud, overbearing, and highly confident manner that shows they expect to be obeyed without question.</p>
<p class='rw-defn idx12' style='display:none'>A lackey is a person who follows their superior&#8217;s orders so completely that they never openly question those commands.</p>
<p class='rw-defn idx13' style='display:none'>A martinet is a very strict person who demands that people obey rules exactly.</p>
<p class='rw-defn idx14' style='display:none'>A minion is an unimportant person who merely obeys the orders of someone else; they  usually perform boring tasks that require little skill.</p>
<p class='rw-defn idx15' style='display:none'>Nominal can refer to someone who is in charge in name only, or it can refer to a very small amount of something; both are related by their relative insignificance.</p>
<p class='rw-defn idx16' style='display:none'>A plenipotentiary is someone who has full power to take action or make decisions on behalf of their government in a foreign country.</p>
<p class='rw-defn idx17' style='display:none'>If you say that someone is servile, you don&#8217;t respect them because they are too obedient, too agreeable, and too willing to do anything for another person.</p>
<p class='rw-defn idx18' style='display:none'>If you are subservient, you are too eager and willing to do what other people want and often put your own wishes aside.</p>
<p class='rw-defn idx19' style='display:none'>Sycophants praise people in authority or those who have considerable influence in order to seek some favor from them in return.</p>
<p class='rw-defn idx20' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx21' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>
<p class='rw-defn idx22' style='display:none'>In the Middle Ages, a vassal was a worker who served a lord in exchange for land to live upon; today you are a vassal if you serve another and are largely controlled by them.</p>
<p class='rw-defn idx23' style='display:none'>The zenith of something is its highest, most powerful, or most successful point.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>potentate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='potentate#' id='pronounce-sound' path='audio/words/amy-potentate'></a>
POHT-n-tayt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='potentate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='potentate#' id='context-sound' path='audio/wordcontexts/brian-potentate'></a>
As the founder and <span class="caps">CEO</span> of one of the largest companies in the world, Kendall was considered a <em>potentate</em> or powerful chieftain of industry. As the head of the board of directors, Kendall often traveled and had met powerful rulers or <em>potentates</em> of many developing countries. The type of <em>potentate</em> Kendall most valued was a monarch, since they seemed to have more control over their nation&#8217;s markets and resources than nationally elected presidents did.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>potentate</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A founder of a company who works tirelessly to better it.
</li>
<li class='choice '>
<span class='result'></span>
A force described by physics that is very powerful.
</li>
<li class='choice answer '>
<span class='result'></span>
An autocrat who takes full advantage of their position.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='potentate#' id='definition-sound' path='audio/wordmeanings/amy-potentate'></a>
A <em>potentate</em> is a ruler who has great power over people.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>ruler</em>
</span>
</span>
</div>
<a class='quick-help' href='potentate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='potentate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/potentate/memory_hooks/3746.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Potent</span></span> <span class="emp2"><span>Potent</span></span>ial of T<span class="emp3"><span>ate</span></span></span></span> The <span class="emp2"><span>potent</span></span><span class="emp3"><span>ate</span></span> T<span class="emp3"><span>ate</span></span> had such <span class="emp2"><span>potent</span></span>, or powerful, <span class="emp2"><span>potent</span></span>ial that we were all excited that <span class="emp2"><span>potent</span></span><span class="emp3"><span>ate</span></span> T<span class="emp3"><span>ate</span></span> would make some meaningful changes in the way we were treated.
</p>
</div>

<div id='memhook-button-bar'>
<a href="potentate#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/potentate/memory_hooks">Use other hook</a>
<a href="potentate#" id="memhook-use-own" url="https://membean.com/mywords/potentate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='potentate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
It is a characteristic of <b>potentates</b> that they don’t succumb to peaceful retirement. Instead they hold power in their hoary fists as judgment and grip weaken, destroying any successors except family members.
<cite class='attribution'>
&mdash;
Simon Sebag Montefiore, British historian and writer
</cite>
</li>
<li>
[As president of FIFA, the international governing body of soccer,] Mr. Havelange traveled around the world like a <b>potentate</b> on private jets and in limousines, surrounded by an entourage. . . . “Without a doubt, he’s the most powerful man in sports,” Hank Steinbrecher, secretary general of the U.S. Soccer Federation, told the _Chicago Sun-Times_ in 1994.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Imagine you are the tightfisted <b>potentate</b> of a small republic, plotting the least expensive way to care for subjects in fragile health who depend on your beneficence. You could watch while your subjects who are elderly or disabled (or both) scramble to find and pay for healthy meals. . . . But you might just try feeding these needy subjects instead.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
"The [Universal Declaration of Human Rights] sets forth at the very beginning that the foundation of all of our rights is the profound and equal dignity of each and every member of the human family," George argues. "It is not the gift of kings or <b>potentates</b> or presidents or parliaments, but one that is inherent."
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/potentate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='potentate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='potent_capable' data-tree-url='//cdn1.membean.com/public/data/treexml' href='potentate#'>
<span class=''></span>
potent
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>capable, powerful</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ate_possess' data-tree-url='//cdn1.membean.com/public/data/treexml' href='potentate#'>
<span class=''></span>
-ate
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>possessing a certain quality</td>
</tr>
</table>
<p>A <em>potentate</em> possesses the &#8220;quality of being powerful,&#8221; or &#8220;capable&#8221; of ruling others.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='potentate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Potentate" src="https://cdn2.membean.com/public/images/wordimages/cons2/potentate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='potentate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>acme</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>apex</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>autocratic</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>autonomous</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>despot</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>dominion</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>hierarchy</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>imperious</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>martinet</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>plenipotentiary</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>zenith</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abase</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>acolyte</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>adherent</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>figurehead</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>lackey</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>minion</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>nominal</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>servile</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>subservient</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>sycophant</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>vassal</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='potentate#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
impotent
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>powerless</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="potentate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>potentate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="potentate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

