
<!DOCTYPE html>
<html>
<head>
<title>Word: deluge | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An affliction is something that causes pain and mental suffering, especially a medical condition.</p>
<p class='rw-defn idx1' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx2' style='display:none'>A barrage of something, such as words or missiles, is an attacking stream that bursts steadily upon someone.</p>
<p class='rw-defn idx3' style='display:none'>Something that is capacious has a lot of space and can contain a lot of things.</p>
<p class='rw-defn idx4' style='display:none'>A cataclysm is a violent, sudden event that causes great change and/or harm.</p>
<p class='rw-defn idx5' style='display:none'>When there is a dearth of something, there is a scarcity or lack of it.</p>
<p class='rw-defn idx6' style='display:none'>A debacle is something, such as an event or attempt, that fails completely in an embarrassing way.</p>
<p class='rw-defn idx7' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx8' style='display:none'>A fusillade is a rapid burst of gunfire from multiple guns fired at once or in succession; in turn, a fusillade can also be a rapid outburst or discharge of something.</p>
<p class='rw-defn idx9' style='display:none'>An incursion is an unpleasant intrusion, such as a sudden hostile attack or a land being flooded.</p>
<p class='rw-defn idx10' style='display:none'>If you are inundated with something, you have so much of it that you cannot easily deal with it; likewise, if too much rain inundates an area, it causes flooding.</p>
<p class='rw-defn idx11' style='display:none'>A panoply is a large and impressive collection of people or things.</p>
<p class='rw-defn idx12' style='display:none'>A paucity of something is not enough of it.</p>
<p class='rw-defn idx13' style='display:none'>A plethora of something is too much of it.</p>
<p class='rw-defn idx14' style='display:none'>A preponderance of things of a particular type in a group means that there are more of that type than of any other.</p>
<p class='rw-defn idx15' style='display:none'>A spate of things is a sudden series, significant quantity, or outpouring of them.</p>
<p class='rw-defn idx16' style='display:none'>When something surges, it rapidly increases or suddenly rises.</p>
<p class='rw-defn idx17' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx18' style='display:none'>A torrential downpour of rain is very heavy and intense.</p>
<p class='rw-defn idx19' style='display:none'>The adjective torrid can refer to weather that is very hot and dry; it can also refer to earth that has been baked or scorched by such weather.</p>
<p class='rw-defn idx20' style='display:none'>If you refer to something as a trifle, you mean that it is of little or no importance or value.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>deluge</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='deluge#' id='pronounce-sound' path='audio/words/amy-deluge'></a>
DEL-oozh
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='deluge#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='deluge#' id='context-sound' path='audio/wordcontexts/brian-deluge'></a>
It wasn&#8217;t just raining steadily last night&#8212;there was an absolute, pouring <em>deluge</em>.  Our yard turned into a muddy swamp, the puddles from the rain were as large as lakes, and the endless <em>deluge</em> knocked down power lines.  Emergency services were overwhelmed and <em><em>deluged</em></em> with so many reports of accidents and flooding that they had to request support from neighboring counties.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When would you experience a <em>deluge</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
During a basketball game when your team suddenly scores several points.
</li>
<li class='choice '>
<span class='result'></span>
During a slow day at work when you have little to accomplish.
</li>
<li class='choice answer '>
<span class='result'></span>
During a period of heavy rain that leaves you drenched.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='deluge#' id='definition-sound' path='audio/wordmeanings/amy-deluge'></a>
A <em>deluge</em> is a sudden, heavy downfall of rain; it can also be a large number of things, such as papers or e-mails, that someone gets all at the same time, making them very difficult to handle.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>downpour</em>
</span>
</span>
</div>
<a class='quick-help' href='deluge#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='deluge#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/deluge/memory_hooks/4307.json'></span>
<p>
<img alt="Deluge" src="https://cdn0.membean.com/public/images/wordimages/hook/deluge.jpg?qdep8" />
<span class="emp0"><span>Del<span class="emp1"><span>uge</span></span> of Ro<span class="emp1"><span>uge</span></span>!</span></span>  Cherry decided that her wrinkling face did not need a modest amount of ro<span class="emp1"><span>uge</span></span>, but a h<span class="emp1"><span>uge</span></span> del<span class="emp1"><span>uge</span></span> of ro<span class="emp1"><span>uge</span></span>; I saw her yesterday, and her face looked like one bright red apple because she had so much of that <span class="emp1"><span>red</span></span> makeup on it!
</p>
</div>

<div id='memhook-button-bar'>
<a href="deluge#" id="add-public-hook" style="" url="https://membean.com/mywords/deluge/memory_hooks">Use other public hook</a>
<a href="deluge#" id="memhook-use-own" url="https://membean.com/mywords/deluge/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='deluge#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
There has been a <b>deluge</b> of books seeking to explain the country’s transformation: by businessmen, diplomats, journalists, political scientists and the occasional emerging-market opportunist.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
As individuals and companies scramble to block the growing <b>deluge</b> of junk e-mail, known as spam, they're running into the opposite problem.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
Eta unleashed a <b>deluge</b> over the past few days in South Florida that flooded entire neighborhoods and filled some homes with rising water.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/deluge/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='deluge#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='de_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='deluge#'>
<span class='common'></span>
de-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='luv_wash' data-tree-url='//cdn1.membean.com/public/data/treexml' href='deluge#'>
<span class=''></span>
luv
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>wash, bathe</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='deluge#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>A <em>deluge</em> can &#8220;thoroughly wash or bathe&#8221; the land with all the rain that comes with it.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='deluge#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Copenhagen, Denmark</strong><span> This deluge in August of 2010 caused much damage.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/deluge.jpg' video_url='examplevids/deluge' video_width='350'></span>
<div id='wt-container'>
<img alt="Deluge" height="288" src="https://cdn1.membean.com/video/examplevids/deluge.jpg" width="350" />
<div class='center'>
<a href="deluge#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='deluge#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Deluge" src="https://cdn0.membean.com/public/images/wordimages/cons2/deluge.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='deluge#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>affliction</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>barrage</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>capacious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>cataclysm</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>debacle</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>fusillade</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>incursion</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>inundate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>panoply</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>plethora</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>preponderance</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>spate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>surge</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>torrential</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>dearth</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>paucity</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>torrid</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>trifle</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='deluge#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
deluge
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to flood with water or overrun</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="deluge" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>deluge</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="deluge#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

