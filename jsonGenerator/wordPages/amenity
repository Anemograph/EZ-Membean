
<!DOCTYPE html>
<html>
<head>
<title>Word: amenity | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Something antiquated is old-fashioned and not suitable for modern needs or conditions.</p>
<p class='rw-defn idx1' style='display:none'>An appurtenance is a supporting feature, form of equipment, or item associated with a particular activity.</p>
<p class='rw-defn idx2' style='display:none'>A commodious room or house is large and roomy, which makes it convenient and highly suitable for living.</p>
<p class='rw-defn idx3' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx4' style='display:none'>Something that has curative properties can be used for curing people&#8217;s illnesses.</p>
<p class='rw-defn idx5' style='display:none'>Debility is a state of being physically or mentally weak, usually due to illness.</p>
<p class='rw-defn idx6' style='display:none'>Decrepitude is the state of being very old, worn out, or very ill; therefore, something or someone is no longer in good physical condition or good health.</p>
<p class='rw-defn idx7' style='display:none'>If you behave with deference towards someone, you show them respect and accept their opinions or decisions, especially because they have an important position.</p>
<p class='rw-defn idx8' style='display:none'>Something, such as a building, is derelict if it is empty, not used, and in bad condition or disrepair.</p>
<p class='rw-defn idx9' style='display:none'>A dilapidated building, vehicle, etc. is old, broken-down, and in very bad condition.</p>
<p class='rw-defn idx10' style='display:none'>Something luxuriant, such as plants or hair, is growing well and is very healthy.</p>
<p class='rw-defn idx11' style='display:none'>If two people have established a good rapport, their connection is such that they have a good understanding of and can communicate well with one other.</p>
<p class='rw-defn idx12' style='display:none'>A salubrious place or area is pleasant, clean, healthy, and comfortable to live in.</p>
<p class='rw-defn idx13' style='display:none'>A salutary experience is beneficial to you since it strengthens you in some way, although it may be unpleasant as you undergo it; this word also refers to promoting good health.</p>
<p class='rw-defn idx14' style='display:none'>A person who is solicitous behaves in a way that shows great concern about someone&#8217;s health, feelings, safety, etc.</p>
<p class='rw-defn idx15' style='display:none'>An unkempt person or thing is untidy and has not been kept neat.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>amenity</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='amenity#' id='pronounce-sound' path='audio/words/amy-amenity'></a>
uh-MEN-i-tee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='amenity#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='amenity#' id='context-sound' path='audio/wordcontexts/brian-amenity'></a>
My new apartment has a swimming pool, a gym, free cable television, and lots of other similarly convenient <em><em>amenities</em></em>.  I think the owners put in all the <em><em>amenities</em></em> so that they could charge more rent for the extra benefits of living there.  I also think that a home is made more pleasant by all the added <em><em>amenities</em></em> that truly make living more comfortable.  It&#8217;s too bad we don&#8217;t have the one <em>amenity</em> I really would&#8212;a cleaning service!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What are <em>amenities</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Services and facilities that provide comfort and convenience.
</li>
<li class='choice '>
<span class='result'></span>
Bonuses provided to employees who have worked extra hard.
</li>
<li class='choice '>
<span class='result'></span>
Additional deposits that must be made when renting a place.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='amenity#' id='definition-sound' path='audio/wordmeanings/amy-amenity'></a>
<em>Amenities</em> are things that make a place comfortable or easy to live in, such as hot water, updated bathrooms, and sports facilities.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>pleasant items</em>
</span>
</span>
</div>
<a class='quick-help' href='amenity#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='amenity#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/amenity/memory_hooks/5673.json'></span>
<p>
<span class="emp0"><span>Wife's Happy <span class="emp3"><span>Amen</span></span>s</span></span>  Whenever my husband wants to add an <span class="emp3"><span>amen</span></span><span class="emp2"><span>it</span></span>y to make my life easier I say "<span class="emp3"><span>amen</span></span>" to <span class="emp2"><span>it</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="amenity#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/amenity/memory_hooks">Use other hook</a>
<a href="amenity#" id="memhook-use-own" url="https://membean.com/mywords/amenity/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='amenity#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Functioning air conditioning is usually considered an <b>amenity</b> and not a habitability requirement unless you live in an area that experiences fairly high temperatures or you have a medical condition requiring a functioning air conditioner.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Willow Creek [Community Church] has every <b>amenity</b> you can imagine, from food courts to basketball courts, from cafes to video screens, not to mention enough parking spaces for around 4,000 cars.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
The remaining room <b>amenities</b> included a Keurig coffee maker, a small refrigerator, and an HDTV.
<cite class='attribution'>
&mdash;
Business Insider
</cite>
</li>
<li>
Most public services remain neglected and the majority of citizens don't have access to basic <b>amenities</b> like clean and safe drinking water.
<cite class='attribution'>
&mdash;
India Together
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/amenity/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='amenity#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='am_love' data-tree-url='//cdn1.membean.com/public/data/treexml' href='amenity#'>
<span class=''></span>
am
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>love</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ity_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='amenity#'>
<span class=''></span>
-ity
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state or quality</td>
</tr>
</table>
<p>An <em>amenity</em> is characterized by a &#8220;state or condition&#8221; of having a comfort which one &#8220;loves.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='amenity#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Geobeats</strong><span> The best amenities that are offered by airlines.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/amenity.jpg' video_url='examplevids/amenity' video_width='350'></span>
<div id='wt-container'>
<img alt="Amenity" height="288" src="https://cdn1.membean.com/video/examplevids/amenity.jpg" width="350" />
<div class='center'>
<a href="amenity#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='amenity#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Amenity" src="https://cdn3.membean.com/public/images/wordimages/cons2/amenity.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='amenity#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>appurtenance</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>commodious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>curative</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>deference</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>luxuriant</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>rapport</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>salubrious</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>salutary</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>solicitous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>antiquated</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>debility</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>decrepitude</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>derelict</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>dilapidated</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>unkempt</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="amenity" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>amenity</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="amenity#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

