
<!DOCTYPE html>
<html>
<head>
<title>Word: carnage | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abominate something, you hate it because you think it is extremely wrong and unacceptable.</p>
<p class='rw-defn idx1' style='display:none'>An affliction is something that causes pain and mental suffering, especially a medical condition.</p>
<p class='rw-defn idx2' style='display:none'>A bastion is considered an important and effective defense of a way of life or principle; a bastion can also refer to a heavily fortified place, such as a castle.</p>
<p class='rw-defn idx3' style='display:none'>Bedlam is a situation with a great deal of noise and confusion.</p>
<p class='rw-defn idx4' style='display:none'>If you are bellicose, you behave in an aggressive way and are likely to start an argument or fight.</p>
<p class='rw-defn idx5' style='display:none'>A belligerent person or country is aggressive, very unfriendly, and likely to start a fight.</p>
<p class='rw-defn idx6' style='display:none'>When a person is besieged, they are excessively bothered or overwhelmed by questions or other matters requiring their attention; likewise, when a city is besieged, it is surrounded by enemy forces.</p>
<p class='rw-defn idx7' style='display:none'>Something is a blight if it spoils or damages things severely; blight is often used to describe something that ruins or kills crops.</p>
<p class='rw-defn idx8' style='display:none'>A cataclysm is a violent, sudden event that causes great change and/or harm.</p>
<p class='rw-defn idx9' style='display:none'>A chaotic state of affairs is in a state of confusion and complete disorder.</p>
<p class='rw-defn idx10' style='display:none'>If someone is granted clemency, they are punished less severely than they could have been—they have been shown mercy.</p>
<p class='rw-defn idx11' style='display:none'>Something that is deleterious has a harmful effect.</p>
<p class='rw-defn idx12' style='display:none'>If you desecrate something that is considered holy or very special, you deliberately spoil or damage it.</p>
<p class='rw-defn idx13' style='display:none'>A despot is a leader who has a lot of power and uses it in a cruel and unreasonable way.</p>
<p class='rw-defn idx14' style='display:none'>Draconian rules and laws are extremely strict and harsh.</p>
<p class='rw-defn idx15' style='display:none'>If you refer to the enormity of a situation, problem, or event, you mean that it is very evil or morally offensive; this word has nothing to do with physical size.</p>
<p class='rw-defn idx16' style='display:none'>A fortification is a structure or building that is used in defense against an invading army.</p>
<p class='rw-defn idx17' style='display:none'>When there is havoc, there is great disorder, widespread destruction, and much confusion.</p>
<p class='rw-defn idx18' style='display:none'>If you describe something as heinous, you mean that is extremely evil, shocking, and bad.</p>
<p class='rw-defn idx19' style='display:none'>Something that is inconspicuous does not attract attention and is not easily seen or noticed because it is small or ordinary.</p>
<p class='rw-defn idx20' style='display:none'>If someone exhibits magnanimity towards another, they show them kindness and noble generosity, especially after defeating them in battle or after having been treated badly by them.</p>
<p class='rw-defn idx21' style='display:none'>A malevolent person or thing is evil due to deliberate attempts to cause harm.</p>
<p class='rw-defn idx22' style='display:none'>A malignant thing or person, such as a tumor or criminal, is deadly or does great harm.</p>
<p class='rw-defn idx23' style='display:none'>A misanthrope is someone who hates and mistrusts people.</p>
<p class='rw-defn idx24' style='display:none'>A misogynist is someone who hates women or is highly critical about the female gender.</p>
<p class='rw-defn idx25' style='display:none'>If you describe something as moribund, you imply that it is no longer effective; it may also be coming to the end of its useful existence.</p>
<p class='rw-defn idx26' style='display:none'>Pandemonium is a very noisy and uncontrolled situation, especially one that is caused by a lot of angry or excited people.</p>
<p class='rw-defn idx27' style='display:none'>Something that is pernicious is very harmful or evil, often in a way that is hidden or not quickly noticed.</p>
<p class='rw-defn idx28' style='display:none'>Philanthropy is unselfish support in the form of donating money, work, or gifts to positive social purposes; philanthropy is also overall love for humans in general.</p>
<p class='rw-defn idx29' style='display:none'>If soldiers pillage a place, such as a town or museum, they steal a lot of things and damage it using violent methods.</p>
<p class='rw-defn idx30' style='display:none'>When you ravage something, you completely destroy, wreck, or damage it.</p>
<p class='rw-defn idx31' style='display:none'>If you are supine, you are lying on your back with your face upward.</p>
<p class='rw-defn idx32' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>
<p class='rw-defn idx33' style='display:none'>If you say someone&#8217;s behavior is unseemly, you disapprove of it because it is not in good taste or not suitable for a particular situation.</p>
<p class='rw-defn idx34' style='display:none'>If someone is described as vivacious, they are lively and have a happy, lighthearted manner.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>carnage</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='carnage#' id='pronounce-sound' path='audio/words/amy-carnage'></a>
KAR-nij
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='carnage#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='carnage#' id='context-sound' path='audio/wordcontexts/brian-carnage'></a>
Little is more distressing for a farmer than waking up in the morning and finding widespread <em>carnage</em> or slaughter of his livestock.  For example, a farmer walking into his chicken coop to find the <em>carnage</em> from a fox or raccoon attack becomes most unhappy&#8212;seeing those dead, headless chicken&#8217;s bodies strewn across the coop is upsetting to say the least.  One can also imagine the dismay that the sheep farmer experiences when he sees the <em>carnage</em> or slain bodies of sheep dotting his fields.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What could result in <em>carnage</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
An untrained puppy that destroys an entire couch.
</li>
<li class='choice '>
<span class='result'></span>
A heated argument that damages a relationship permanently.
</li>
<li class='choice answer '>
<span class='result'></span>
A large earthquake that causes a great number of deaths.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='carnage#' id='definition-sound' path='audio/wordmeanings/amy-carnage'></a>
Mass <em>carnage</em> is the massacre or slaughter of many people at one time, usually in battle or from an unusually intense natural disaster.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>slaughter</em>
</span>
</span>
</div>
<a class='quick-help' href='carnage#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='carnage#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/carnage/memory_hooks/3412.json'></span>
<p>
<span class="emp0"><span>S<span class="emp1"><span>car</span></span> Ma<span class="emp2"><span>nage</span></span>r</span></span> A s<span class="emp1"><span>car</span></span> ma<span class="emp2"><span>nage</span></span>r might look for a site of <span class="emp1"><span>car</span></span><span class="emp2"><span>nage</span></span> to ma<span class="emp2"><span>nage</span></span> his ghastly work of collecting s<span class="emp1"><span>car</span></span>s.
</p>
</div>

<div id='memhook-button-bar'>
<a href="carnage#" id="add-public-hook" style="" url="https://membean.com/mywords/carnage/memory_hooks">Use other public hook</a>
<a href="carnage#" id="memhook-use-own" url="https://membean.com/mywords/carnage/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='carnage#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
The death toll from small arms dwarfs that of all other weapons systems—and in most years greatly exceeds the toll of the atomic bombs that devastated Hiroshima and Nagasaki. In terms of <b>carnage</b> they cause, small arms, indeed, could well be described as “weapons of mass destruction”.
<span class='attribution'>&mdash; Kofi Annan, former Secretary-General of the United Nations</span>
<img alt="Kofi annan, former secretary-general of the united nations" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Kofi Annan, former Secretary-General of the United Nations.jpg?qdep8" width="80" />
</li>
<li>
In Staten Island, the storm’s impact was particularly deadly. Twenty-one people died in the borough, more than in the states of New Jersey, Connecticut and Pennsylvania combined. The <b>carnage</b> took place along the flood-prone outwash plain on the south and east shores of the island, located at the crest of the New York Bight, where geography funneled the ocean’s angry waters.
<cite class='attribution'>
&mdash;
Discover Magazine
</cite>
</li>
<li>
The violence echoed that of 2002, when Modi was chief minister of Gujarat and authorities there did nothing to stem <b>carnage</b> that killed some 1,000 people, the majority of them Muslims.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
Former Serbian strongman Slobodan Milosevic was regarded as the chief architect of the <b>carnage</b> unleashed during the breakup of Yugoslavia. . . . He was president when hundreds of thousands of people were killed and millions were forced to leave their homes as the fall of communism opened the door to ethnic and religious strife.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/carnage/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='carnage#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='carn_meat' data-tree-url='//cdn1.membean.com/public/data/treexml' href='carnage#'>
<span class=''></span>
carn
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>meat</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='age_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='carnage#'>
<span class=''></span>
-age
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state</td>
</tr>
</table>
<p>The massive slaughter that is indicative of the <em>carnage</em> of war brings about a &#8220;state&#8221; of dead &#8220;meat,&#8221; that is, the corpses of those slain in battle.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='carnage#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>BBC Power of Art Picasso</strong><span> The carnage created by a massacre of rebels.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/carnage.jpg' video_url='examplevids/carnage' video_width='350'></span>
<div id='wt-container'>
<img alt="Carnage" height="288" src="https://cdn1.membean.com/video/examplevids/carnage.jpg" width="350" />
<div class='center'>
<a href="carnage#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='carnage#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Carnage" src="https://cdn3.membean.com/public/images/wordimages/cons2/carnage.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='carnage#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abominate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>affliction</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bedlam</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>bellicose</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>belligerent</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>besiege</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>blight</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>cataclysm</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>chaotic</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>deleterious</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>desecrate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>despot</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>draconian</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>enormity</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>havoc</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>heinous</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>malevolent</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>malignant</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>misanthrope</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>misogynist</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>moribund</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>pandemonium</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>pernicious</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>pillage</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>ravage</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>supine</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>unseemly</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>bastion</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>clemency</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>fortification</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>inconspicuous</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>magnanimity</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>philanthropy</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>vivacious</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="carnage" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>carnage</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="carnage#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

