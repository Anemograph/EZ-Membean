
<!DOCTYPE html>
<html>
<head>
<title>Word: chronic | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Something that is diurnal happens on a daily basis.</p>
<p class='rw-defn idx1' style='display:none'>Something ephemeral, such as some insects or a sunset, lasts for only a short time or has a very short lifespan.</p>
<p class='rw-defn idx2' style='display:none'>Something that is evanescent lasts for only a short time before disappearing from sight or memory.</p>
<p class='rw-defn idx3' style='display:none'>A fleeting moment lasts but a short time before it fades away.</p>
<p class='rw-defn idx4' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx5' style='display:none'>Something that is incessant continues on for a long time without stopping.</p>
<p class='rw-defn idx6' style='display:none'>If someone leaves an indelible impression on you, it will not be forgotten; an indelible mark is permanent or impossible to erase or remove.</p>
<p class='rw-defn idx7' style='display:none'>Something that is interminable continues for a very long time in a boring or annoying way.</p>
<p class='rw-defn idx8' style='display:none'>Something that happens on an intermittent basis happens in irregular intervals, stopping and starting at unpredictable times.</p>
<p class='rw-defn idx9' style='display:none'>An inveterate person is always doing a particular thing, especially something questionable—and they are not likely to stop doing it.</p>
<p class='rw-defn idx10' style='display:none'>Longevity is the life span of a person or object; it can also refer to a particularly long life.</p>
<p class='rw-defn idx11' style='display:none'>Something that is perennial lasts a very long time and is enduring.</p>
<p class='rw-defn idx12' style='display:none'>A provisional measure is temporary or conditional until more permanent action is taken.</p>
<p class='rw-defn idx13' style='display:none'>A reprieve is a temporary relief from or cancellation of a punishment; it can also be a relief from something, such as pain or trouble of some kind.</p>
<p class='rw-defn idx14' style='display:none'>Sporadic occurrences happen from time to time but not at constant or regular intervals.</p>
<p class='rw-defn idx15' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx16' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx17' style='display:none'>Something that is temporal deals with the present and somewhat brief time of this world.</p>
<p class='rw-defn idx18' style='display:none'>Something that has the quality of transience lasts for only a short time or is constantly changing.</p>
<p class='rw-defn idx19' style='display:none'>If you are unflagging while doing a task, you are untiring when working upon it and do not stop until it is finished.</p>
<p class='rw-defn idx20' style='display:none'>If you are unrelenting in your desire to do something, you stop at nothing until you&#8217;ve done it.</p>
<p class='rw-defn idx21' style='display:none'>Something that is volatile can change easily and vary widely.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>chronic</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='chronic#' id='pronounce-sound' path='audio/words/amy-chronic'></a>
KRON-ik
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='chronic#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='chronic#' id='context-sound' path='audio/wordcontexts/brian-chronic'></a>
That winter Mildred found herself suffering from an ongoing, never stopping, and <em>chronic</em> cold that would not get better, no matter what she did.  Her cough, sniffles, and headaches remained with her all the time in an exhausting, <em>chronic</em> illness.  Mildred tried to ease her <em>chronic</em> and persistent suffering with tea, steam, medicine, and rest, but nothing seemed to work.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When is a condition <em>chronic</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
When it causes consistent, long-lasting suffering.
</li>
<li class='choice '>
<span class='result'></span>
When it is imagined or invented by the one who has it.
</li>
<li class='choice '>
<span class='result'></span>
When it involves multiple medical specialists to diagnose it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='chronic#' id='definition-sound' path='audio/wordmeanings/amy-chronic'></a>
A <em>chronic</em> illness or pain is serious and lasts for a long time; a <em>chronic</em> problem is always happening or returning and is very difficult to solve.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>unceasing</em>
</span>
</span>
</div>
<a class='quick-help' href='chronic#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='chronic#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/chronic/memory_hooks/2887.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Ron</span></span>'s <span class="emp3"><span>Ch</span></span><span class="emp2"><span>ron</span></span><span class="emp3"><span>ic</span></span> <span class="emp3"><span>Chic</span></span> Sense</span></span>  <span class="emp2"><span>Ron</span></span> is so stylish, fashionable, and <span class="emp3"><span>chic</span></span> all the time that we're now calling <span class="emp2"><span>Ron</span></span> "<span class="emp3"><span>ch</span></span><span class="emp2"><span>ron</span></span><span class="emp3"><span>ic</span></span> <span class="emp3"><span>chic</span></span> <span class="emp2"><span>Ron</span></span>."
</p>
</div>

<div id='memhook-button-bar'>
<a href="chronic#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/chronic/memory_hooks">Use other hook</a>
<a href="chronic#" id="memhook-use-own" url="https://membean.com/mywords/chronic/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='chronic#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
But with <b>chronic</b> pain, you may feel constantly tense and stressed. Over time, the stress can result in different emotional problems associated with depression.
<cite class='attribution'>
&mdash;
WebMD
</cite>
</li>
<li>
This long-term, multi-million-dollar investment addresses a <b>chronic</b> problem for rural areas: Doctors who get enough experience or accolades often leave for major cities.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
The first of the baby boomers officially become seniors in 2011, and it is clear Medicare needs help, particularly in its <b>chronic</b>-care approach.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
"We found that we could identify children who were about twice as likely to report <b>chronic</b> widespread pain as adults, based on whatever their parents and teachers had said about their behavior when they were 7, 11 and 16," says Gary Macfarlane, an epidemiologist at the University of Aberdeen in Scotland.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/chronic/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='chronic#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='chron_time' data-tree-url='//cdn1.membean.com/public/data/treexml' href='chronic#'>
<span class=''></span>
chron
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>time</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ic_like' data-tree-url='//cdn1.membean.com/public/data/treexml' href='chronic#'>
<span class=''></span>
-ic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>like, characterized by</td>
</tr>
</table>
<p><em>Chronic</em> pain is &#8220;characterized by&#8221; a great deal of &#8220;time&#8221; during which it must be endured.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='chronic#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>ABC Nightline</strong><span> Dr. Gavin Schmidt from NASA observes that climate change is a chronic problem.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/chronic.jpg' video_url='examplevids/chronic' video_width='350'></span>
<div id='wt-container'>
<img alt="Chronic" height="288" src="https://cdn1.membean.com/video/examplevids/chronic.jpg" width="350" />
<div class='center'>
<a href="chronic#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='chronic#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Chronic" src="https://cdn1.membean.com/public/images/wordimages/cons2/chronic.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='chronic#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>diurnal</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>incessant</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>indelible</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>interminable</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>inveterate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>longevity</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>perennial</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>unflagging</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>unrelenting</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>ephemeral</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>evanescent</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>fleeting</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>intermittent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>provisional</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>reprieve</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>sporadic</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>temporal</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>transience</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>volatile</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="chronic" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>chronic</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="chronic#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

