
<!DOCTYPE html>
<html>
<head>
<title>Word: infusion | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Infusion-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/infusion-large.jpg?qdep8" />
</div>
<a href="infusion#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>If you abridge a book or play, you make it shorter by making cuts to the original.</p>
<p class='rw-defn idx2' style='display:none'>An addendum is an additional section added to the end of a book, document, or speech that gives more information.</p>
<p class='rw-defn idx3' style='display:none'>If you adulterate something, you lessen its quality by adding inferior ingredients or elements to it.</p>
<p class='rw-defn idx4' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx5' style='display:none'>Attrition is the process of gradually decreasing the strength of something (such as an enemy force) by continually attacking it.</p>
<p class='rw-defn idx6' style='display:none'>To augment something is to increase its value or effectiveness by adding something to it.</p>
<p class='rw-defn idx7' style='display:none'>To bowdlerize a book, play, or other literary work is to remove parts of it that are considered indecent or unsuitable for family reading.</p>
<p class='rw-defn idx8' style='display:none'>If something, such as a sound, is undergoing a crescendo, it is getting louder and louder or increasing in intensity and is about to reach its peak strength.</p>
<p class='rw-defn idx9' style='display:none'>If you curtail something, you reduce or limit it.</p>
<p class='rw-defn idx10' style='display:none'>When there is a dearth of something, there is a scarcity or lack of it.</p>
<p class='rw-defn idx11' style='display:none'>A deficit occurs when a person or government spends more money than has been received.</p>
<p class='rw-defn idx12' style='display:none'>When you deplete a supply of something, you use it up or lessen its amount over a given period of time.</p>
<p class='rw-defn idx13' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx14' style='display:none'>When an amount of something dwindles, it becomes less and less over time.</p>
<p class='rw-defn idx15' style='display:none'>To efface something is to erase or remove it completely from recognition or memory.</p>
<p class='rw-defn idx16' style='display:none'>If something enervates you, it makes you very tired and weak—almost to the point of collapse.</p>
<p class='rw-defn idx17' style='display:none'>When you excise something, you remove it by cutting it out.</p>
<p class='rw-defn idx18' style='display:none'>To expurgate part of a book, play, or other text is to remove parts of it before publishing because they are considered objectionable, harmful, or offensive.</p>
<p class='rw-defn idx19' style='display:none'>If you extirpate something, you completely destroy it because it is unpleasant or unwanted.</p>
<p class='rw-defn idx20' style='display:none'>To imbue someone with a quality, such as an emotion or idea, is to fill that person completely with it.</p>
<p class='rw-defn idx21' style='display:none'>An impenetrable barrier cannot be gotten through by any means; this word can refer to parts of a building such as walls and doors—or to a problem of some kind that cannot be solved.</p>
<p class='rw-defn idx22' style='display:none'>If you are impervious to things, such as someone&#8217;s actions or words, you are not affected by them or do not notice them.</p>
<p class='rw-defn idx23' style='display:none'>Something that has been ingrained in your mind has been fixed or rooted there permanently.</p>
<p class='rw-defn idx24' style='display:none'>To pare something down is to reduce or lessen it.</p>
<p class='rw-defn idx25' style='display:none'>A paucity of something is not enough of it.</p>
<p class='rw-defn idx26' style='display:none'>When a substance permeates something, it enters into all parts of it.</p>
<p class='rw-defn idx27' style='display:none'>A preponderance of things of a particular type in a group means that there are more of that type than of any other.</p>
<p class='rw-defn idx28' style='display:none'>A profusion of something is a very large quantity or variety of it.</p>
<p class='rw-defn idx29' style='display:none'>To rarefy something is to make it less dense, as in oxygen at high altitudes; this word also refers to purifying or refining something, thereby making it less ordinary or commonplace.</p>
<p class='rw-defn idx30' style='display:none'>When you replenish something, you refill or restock it.</p>
<p class='rw-defn idx31' style='display:none'>Stores that are replete with goods to sell are well-stocked and completely supplied to satisfy many customers.</p>
<p class='rw-defn idx32' style='display:none'>If something, such as warmth, color, or liquid, suffuses a material, it gradually spreads through or covers it; if you are suffused with a feeling, you are full of that feeling.</p>
<p class='rw-defn idx33' style='display:none'>When you supplement, you add on some extra to something to make up for a lack in it.</p>
<p class='rw-defn idx34' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx35' style='display:none'>If you truncate something, you make it shorter or quicker by removing or cutting off part of it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>infusion</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='infusion#' id='pronounce-sound' path='audio/words/amy-infusion'></a>
in-FYOO-zhuhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='infusion#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='infusion#' id='context-sound' path='audio/wordcontexts/brian-infusion'></a>
We really needed an <em>infusion</em> or pouring in of new talent on our soccer team, for we were just awful.  Just before our first game two new players joined the team, who turned out to be just the <em>infusion</em> of new players that we needed.  To help us out as well, the coach had us all drink an <em>infusion</em> of herbal tea at the beginning of each practice that was supposed to give us extra energy and stamina.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What might need an <em>infusion</em> of something?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A child that needs to read a book to be able to fall asleep.
</li>
<li class='choice '>
<span class='result'></span>
A cake that needs frosting to be added on top of it.
</li>
<li class='choice answer '>
<span class='result'></span>
An empty house that needs to be filled with people.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='infusion#' id='definition-sound' path='audio/wordmeanings/amy-infusion'></a>
An <em>infusion</em> is the pouring in or the introduction of something into something else so as to fill it up.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>pouring in to fill</em>
</span>
</span>
</div>
<a class='quick-help' href='infusion#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='infusion#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/infusion/memory_hooks/4782.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Fusion</span></span> Reactor</span></span> The in<span class="emp1"><span>fusion</span></span> of tea gave me so much energy when I drank it that I felt a huge in<span class="emp1"><span>fusion</span></span> of new energy, like the sun being powered by <span class="emp1"><span>fusion</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="infusion#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/infusion/memory_hooks">Use other hook</a>
<a href="infusion#" id="memhook-use-own" url="https://membean.com/mywords/infusion/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='infusion#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Citi also said it received a $12.5 billion <b>infusion</b> from investors in Kuwait, Singapore and the state of New Jersey and intends to raise an additional $2 billion.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
While the term "tea" can be broadly applied to any <b>infusion</b> of herbs, fruit, flowers, or leaves, for the purposes of this article, we're going to focus on true tea, from the _Camellia sinensis_ plant.
<cite class='attribution'>
&mdash;
Huffington Post
</cite>
</li>
<li>
This gives the campaign a cash <b>infusion</b> of up to $21 million that it must believe it needs to stay competitive for the nomination.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/infusion/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='infusion#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='in_in' data-tree-url='//cdn1.membean.com/public/data/treexml' href='infusion#'>
<span class='common'></span>
in-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>in, into</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='fus_pour' data-tree-url='//cdn1.membean.com/public/data/treexml' href='infusion#'>
<span class=''></span>
fus
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>poured, poured out</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ion_act' data-tree-url='//cdn1.membean.com/public/data/treexml' href='infusion#'>
<span class=''></span>
-ion
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>act, state, or result of doing something</td>
</tr>
</table>
<p>An <em>infusion</em> has been &#8220;poured into&#8221; something.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='infusion#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: The Coffee Co-Mission</strong><span> The infusion of milk into coffee.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/infusion.jpg' video_url='examplevids/infusion' video_width='350'></span>
<div id='wt-container'>
<img alt="Infusion" height="288" src="https://cdn1.membean.com/video/examplevids/infusion.jpg" width="350" />
<div class='center'>
<a href="infusion#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='infusion#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Infusion" src="https://cdn1.membean.com/public/images/wordimages/cons2/infusion.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='infusion#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>addendum</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>adulterate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>augment</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>crescendo</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>imbue</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>ingrained</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>permeate</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>preponderance</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>profusion</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>replenish</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>replete</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>suffuse</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>supplement</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abridge</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>attrition</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>bowdlerize</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>curtail</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>dearth</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>deficit</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>deplete</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>dwindle</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>efface</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>enervate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>excise</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>expurgate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>extirpate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>impenetrable</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>impervious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>pare</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>paucity</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>rarefy</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>truncate</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="infusion" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>infusion</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="infusion#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

