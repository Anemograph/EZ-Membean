
<!DOCTYPE html>
<html>
<head>
<title>Word: quintessential | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Quintessential-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/quintessential-large.jpg?qdep8" />
</div>
<a href="quintessential#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>The acme of something is its highest point of achievement or excellence.</p>
<p class='rw-defn idx1' style='display:none'>Apathy is a lack of interest or unenthusiastic involvement in an activity; there is no effort to change or improve it at all.</p>
<p class='rw-defn idx2' style='display:none'>The best or perfect example of something is its apotheosis; likewise, this word can be used to indicate the best or highest point in someone&#8217;s life or job.</p>
<p class='rw-defn idx3' style='display:none'>An archetype is a perfect or typical example of something because it has the most important qualities that belong to that type of thing; it can also describe essential qualities common to a particular class of things.</p>
<p class='rw-defn idx4' style='display:none'>An axiom is a wise saying or widely recognized general truth that is often self-evident; it can also be an established rule or law.</p>
<p class='rw-defn idx5' style='display:none'>When you claim that something is banal, you do not like it because you think it is ordinary, dull, commonplace, and boring.</p>
<p class='rw-defn idx6' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx7' style='display:none'>A cynosure is an object that serves as the center of attention.</p>
<p class='rw-defn idx8' style='display:none'>A definitive opinion on an issue cannot be challenged; therefore, it is the final word or the most authoritative pronouncement on that issue.</p>
<p class='rw-defn idx9' style='display:none'>A deft movement or action is made quickly and with skill.</p>
<p class='rw-defn idx10' style='display:none'>Someone does something in a disinterested way when they have no personal involvement or attachment to the action.</p>
<p class='rw-defn idx11' style='display:none'>If you describe a person, group, or civilization as effete, you mean it is weak, exhausted,  powerless, unproductive, and/or corrupt.</p>
<p class='rw-defn idx12' style='display:none'>If you say that a person or thing is the epitome of something, you mean that they or it is the best possible example of that thing.</p>
<p class='rw-defn idx13' style='display:none'>An exemplary person is one that sets an ideal or praiseworthy example for others to follow.</p>
<p class='rw-defn idx14' style='display:none'>One thing that exemplifies another serves as an example of it, illustrates it, or demonstrates it.</p>
<p class='rw-defn idx15' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx16' style='display:none'>Someone who has had an illustrious professional career is celebrated and outstanding in their given field of expertise.</p>
<p class='rw-defn idx17' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx18' style='display:none'>An indolent person is lazy.</p>
<p class='rw-defn idx19' style='display:none'>When something is inert, it has no power of movement or is inactive.</p>
<p class='rw-defn idx20' style='display:none'>If something is infallible, it is never wrong and so is incapable of making mistakes.</p>
<p class='rw-defn idx21' style='display:none'>Something insipid is dull, boring, and has no interesting features; for example, insipid food has no taste or little flavor.</p>
<p class='rw-defn idx22' style='display:none'>Something that is integral to something else is an essential or necessary part of it.</p>
<p class='rw-defn idx23' style='display:none'>Irrelevant information is unrelated or unconnected to the situation at hand.</p>
<p class='rw-defn idx24' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx25' style='display:none'>If you do something in a lackadaisical way, you do it carelessly and without putting much effort into it—thereby showing that you are not really interested in what you&#8217;re doing.</p>
<p class='rw-defn idx26' style='display:none'>A luminary is someone who is much admired in a particular profession because they are an accomplished expert in their field.</p>
<p class='rw-defn idx27' style='display:none'>Something mediocre is average or ordinary in quality; it’s just OK.</p>
<p class='rw-defn idx28' style='display:none'>If you describe something as moribund, you imply that it is no longer effective; it may also be coming to the end of its useful existence.</p>
<p class='rw-defn idx29' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx30' style='display:none'>Something nonpareil has no equal because it is much better than all others of its kind or type.</p>
<p class='rw-defn idx31' style='display:none'>The best or most highly regarded members of a particular group are known as a pantheon.</p>
<p class='rw-defn idx32' style='display:none'>A paradigm is a model or example of something that shows how it works or how it can be produced; a paradigm shift is a completely new way of thinking about something.</p>
<p class='rw-defn idx33' style='display:none'>A paragon is an example of a thing or person at its or their very best.</p>
<p class='rw-defn idx34' style='display:none'>Something that is of paramount importance or significance is chief or supreme in those things.</p>
<p class='rw-defn idx35' style='display:none'>A perfunctory action is done with little care or interest; consequently, it is only completed because it is expected of someone to do so.</p>
<p class='rw-defn idx36' style='display:none'>When you personify something, such as a quality, you are the perfect example of it.</p>
<p class='rw-defn idx37' style='display:none'>Someone who is phlegmatic stays calm and unemotional even in dangerous or exciting situations.</p>
<p class='rw-defn idx38' style='display:none'>Something predominant is the most important or the most common thing in a group.</p>
<p class='rw-defn idx39' style='display:none'>Something prosaic is dull, boring, and ordinary.</p>
<p class='rw-defn idx40' style='display:none'>When you have been remiss, you have been careless because you did not do something that you should have done.</p>
<p class='rw-defn idx41' style='display:none'>A superlative deed or act is excellent, outstanding, or the best of its kind.</p>
<p class='rw-defn idx42' style='display:none'>A trite remark or idea is so overused that it is no longer interesting or novel.</p>
<p class='rw-defn idx43' style='display:none'>An unparalleled accomplishment has not been equaled by anyone or cannot be compared to anything that anyone else has ever done.</p>
<p class='rw-defn idx44' style='display:none'>If you are unsurpassed in what you do, you are the best—period.</p>
<p class='rw-defn idx45' style='display:none'>Something vapid is dull, boring, and/or tiresome.</p>
<p class='rw-defn idx46' style='display:none'>Something vintage is the best of its kind and of excellent quality; it is often of a past time and is considered a classic example of its type.</p>
<p class='rw-defn idx47' style='display:none'>If someone vitiates something, they make it less effective, weaker, or reduced in quality.</p>
<p class='rw-defn idx48' style='display:none'>The zenith of something is its highest, most powerful, or most successful point.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>quintessential</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='quintessential#' id='pronounce-sound' path='audio/words/amy-quintessential'></a>
kwin-tuh-SEN-shuhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='quintessential#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='quintessential#' id='context-sound' path='audio/wordcontexts/brian-quintessential'></a>
My brother is the <em>quintessential</em>, model athlete: not only is he coordinated, strong, and willing to work hard, but he also has a keen, competitive edge.  His coach calls him the &#8220;<em>quintessential</em> quarterback&#8221; because he possesses all the characteristics to play the position perfectly.  My brother wants to perform like Brett Favre, whom he considers to be the <em>quintessential</em> or ultimate quarterback of all time.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When is something <em>quintessential</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When it becomes famous for something.
</li>
<li class='choice '>
<span class='result'></span>
When it combines many things well.
</li>
<li class='choice answer '>
<span class='result'></span>
When it&#8217;s an ideal representation of its kind.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='quintessential#' id='definition-sound' path='audio/wordmeanings/amy-quintessential'></a>
Something is <em>quintessential</em> when it is a perfect example of its type.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>ultimate</em>
</span>
</span>
</div>
<a class='quick-help' href='quintessential#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='quintessential#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/quintessential/memory_hooks/3199.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Essential</span></span> Not To <span class="emp1"><span>Quit</span></span></span></span> If you want to be the <span class="emp1"><span>qui</span></span>n<span class="emp1"><span>t</span></span><span class="emp3"><span>essential</span></span> musician, it is <span class="emp3"><span>essential</span></span> that you do not <span class="emp1"><span>quit</span></span> just because it's getting a little bit harder!
</p>
</div>

<div id='memhook-button-bar'>
<a href="quintessential#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/quintessential/memory_hooks">Use other hook</a>
<a href="quintessential#" id="memhook-use-own" url="https://membean.com/mywords/quintessential/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='quintessential#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Spassky was a <b>quintessential</b> representative of the vast Soviet chess establishment, which included institutes devoted to the study of the game and that treated its successful players as national heroes.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
Some who didn’t serve now regret having missed a <b>quintessential</b> rite of passage, or feel guilty about dodging their burden of sacrifice.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
A <b>quintessential</b> New York director made this <b>quintessential</b> New York movie in 1973, with Pacino at his best as an honest cop who turns on his crooked colleagues.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
When painting, he chooses what he deems the <b>quintessential</b> photograph of a player—a shot that captures strengths and personality.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/quintessential/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='quintessential#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>quint</td>
<td>
&rarr;
</td>
<td class='meaning'>fifth</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ess_be' data-tree-url='//cdn1.membean.com/public/data/treexml' href='quintessential#'>
<span class=''></span>
ess
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>be</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ent_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='quintessential#'>
<span class=''></span>
-ent
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='al_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='quintessential#'>
<span class=''></span>
-al
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>Alchemists once searched for the &#8220;fifth essence,&#8221; or that perfect substance which was thought not only to form the heavenly bodies, but was believed also to be the most perfect part of all things.  The first four essences were earth, air, fire, and water.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='quintessential#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Quintessential" src="https://cdn1.membean.com/public/images/wordimages/cons2/quintessential.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='quintessential#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>acme</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>apotheosis</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>archetype</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>axiom</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>cynosure</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>definitive</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>deft</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>epitome</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>exemplary</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>exemplify</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>illustrious</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>infallible</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>integral</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>luminary</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>nonpareil</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>pantheon</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>paradigm</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>paragon</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>paramount</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>personify</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>predominant</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>superlative</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>unparalleled</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>unsurpassed</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>vintage</span> &middot;</li><li data-idx='48' class = 'rw-wordform notlearned'><span>zenith</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>apathy</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>banal</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>disinterested</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>effete</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>indolent</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>inert</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>insipid</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>irrelevant</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>lackadaisical</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>mediocre</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>moribund</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>perfunctory</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>phlegmatic</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>prosaic</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>remiss</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>trite</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>vapid</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>vitiate</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='quintessential#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
quintessence
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>the purest or best example of something</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="quintessential" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>quintessential</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="quintessential#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

