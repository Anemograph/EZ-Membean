
<!DOCTYPE html>
<html>
<head>
<title>Word: perspicacity | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Acuity is sharpness or clearness of vision, hearing, or thought.</p>
<p class='rw-defn idx1' style='display:none'>Acumen is the ability to make quick decisions and keen, precise judgments.</p>
<p class='rw-defn idx2' style='display:none'>Someone who is adroit is skillful and clever, especially in thought, behavior, or action.</p>
<p class='rw-defn idx3' style='display:none'>If you describe something as ambiguous, you mean that it is unclear or confusing because it can be understood in multiple ways.</p>
<p class='rw-defn idx4' style='display:none'>Someone&#8217;s aptitude at a skill is their natural ability to perform it well; this word also refers to someone&#8217;s ability to learn something new or become better at a skill.</p>
<p class='rw-defn idx5' style='display:none'>When you describe someone as astute, you think they quickly understand situations or behavior and use it to their advantage.</p>
<p class='rw-defn idx6' style='display:none'>Something that is capacious has a lot of space and can contain a lot of things.</p>
<p class='rw-defn idx7' style='display:none'>A cogent reason or argument is strong and convincing.</p>
<p class='rw-defn idx8' style='display:none'>Cogitating about something is thinking deeply about it for a long time.</p>
<p class='rw-defn idx9' style='display:none'>Cognitive describes those things related to judgment, memory, and other mental processes of knowing.</p>
<p class='rw-defn idx10' style='display:none'>A conundrum is a problem or puzzle that is difficult or impossible to solve.</p>
<p class='rw-defn idx11' style='display:none'>Something convoluted, such as a difficult concept or procedure, is complex and takes many twists and turns.</p>
<p class='rw-defn idx12' style='display:none'>A deft movement or action is made quickly and with skill.</p>
<p class='rw-defn idx13' style='display:none'>When you discern something, you notice, detect, or understand it, often after thinking about it carefully or studying it for some time.  </p>
<p class='rw-defn idx14' style='display:none'>When you act in an imprudent fashion, you do something that is unwise, is lacking in good judgment, or has no forethought.</p>
<p class='rw-defn idx15' style='display:none'>If an idea or thought is incisive, it is expressed in a penetrating and knowledgeable manner that is clear and brief; additionally, it can demonstrate impressive understanding of related ideas or thoughts.</p>
<p class='rw-defn idx16' style='display:none'>Ingenuity in solving a problem uses creativity, intelligence, and cleverness.</p>
<p class='rw-defn idx17' style='display:none'>A judicious person shows good or sound judgment because they are wise and careful in making decisions.</p>
<p class='rw-defn idx18' style='display:none'>If someone is lucid, they are able to understand things and think clearly; this adjective also describes writing or speech that is crystal clear and easy to understand.</p>
<p class='rw-defn idx19' style='display:none'>When something is luminous, it is bright and glowing.</p>
<p class='rw-defn idx20' style='display:none'>A misapprehension is a misunderstanding or false impression that you are under, especially concerning another person&#8217;s intentions towards you.</p>
<p class='rw-defn idx21' style='display:none'>A myopic person is unable to visualize the long-term negative consequences of their current actions; rather, they are narrowly focused upon short-term results.</p>
<p class='rw-defn idx22' style='display:none'>If someone is naive, they are too trusting of others; they don&#8217;t have enough experience in life to know whom to believe.</p>
<p class='rw-defn idx23' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx24' style='display:none'>Someone is obtuse when they are slow to understand things; someone can choose to be obtuse when they deliberately do not try to figure things out.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is omniscient seems to know absolutely everything.</p>
<p class='rw-defn idx26' style='display:none'>Something is opaque if it is either difficult to understand or is not transparent.</p>
<p class='rw-defn idx27' style='display:none'>A paradox is a statement that appears to be self-contradictory or unrealistic but may surprisingly express a possible truth.</p>
<p class='rw-defn idx28' style='display:none'>Something that is pellucid is either extremely clear because it is transparent to the eye or it is very easy for the mind to understand.</p>
<p class='rw-defn idx29' style='display:none'>When a substance permeates something, it enters into all parts of it.</p>
<p class='rw-defn idx30' style='display:none'>A precocious child shows advanced intelligence or skill at an unusually young age.</p>
<p class='rw-defn idx31' style='display:none'>When someone exhibits profundity, they display great intellectual depth and understanding; profundity can also be the depth or complexity of something.</p>
<p class='rw-defn idx32' style='display:none'>A sagacious person is wise, intelligent, and has the ability to make good practical decisions.</p>
<p class='rw-defn idx33' style='display:none'>A savant is a person who knows a lot about a subject, either via a lifetime of learning or by considerable natural ability.</p>
<p class='rw-defn idx34' style='display:none'>If you do something in an unwitting fashion, you didn&#8217;t know that you were doing it; therefore, it was unintentional on your part.</p>
<p class='rw-defn idx35' style='display:none'>Something that is vacuous is empty or blank, such as a mind or stare.</p>
<p class='rw-defn idx36' style='display:none'>Something that is vaporous is not completely formed but foggy and misty; in the same vein, a vaporous idea is insubstantial and vague.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>perspicacity</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='perspicacity#' id='pronounce-sound' path='audio/words/amy-perspicacity'></a>
pur-spi-KAS-i-tee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='perspicacity#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='perspicacity#' id='context-sound' path='audio/wordcontexts/brian-perspicacity'></a>
Samuel&#8217;s <em>perspicacity</em> made him an ideal company employee: he was not only quick to understand the company&#8217;s structure, but could also easily identify ways to succeed.  His <em>perspicacity</em> and clear insight led the board of directors to recommend him early on as a valuable partner.  The board wanted this company to move into the marketplace quickly, and knew that people who exercised <em>perspicacity</em> and clear reasoning were essential in making this happen.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is <em>perspicacity</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A penetrating intelligence.
</li>
<li class='choice '>
<span class='result'></span>
The ability to choose a good business partner.
</li>
<li class='choice '>
<span class='result'></span>
A way of identifying successful business methods.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='perspicacity#' id='definition-sound' path='audio/wordmeanings/amy-perspicacity'></a>
Someone who demonstrates <em>perspicacity</em> notices or understands things very quickly.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>clear understanding</em>
</span>
</span>
</div>
<a class='quick-help' href='perspicacity#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='perspicacity#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/perspicacity/memory_hooks/3051.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Spic</span></span>e <span class="emp2"><span>City</span></span></span></span> "How to bring people to our <span class="emp2"><span>city</span></span>?  Ah, ha, people love <span class="emp3"><span>spic</span></span>e, so we will not only offer them free <span class="emp3"><span>spic</span></span>e when they come to our <span class="emp2"><span>city</span></span>, but we will have the lovely smell of <span class="emp3"><span>spic</span></span>es all about our sweet-smelling <span class="emp2"><span>city</span></span>!" So great was the <span class="emp1"><span>per</span></span><span class="emp3"><span>spic</span></span>a<span class="emp2"><span>city</span></span> of our <span class="emp2"><span>city</span></span>'s marketing ex<span class="emp1"><span>per</span></span>t!
</p>
</div>

<div id='memhook-button-bar'>
<a href="perspicacity#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/perspicacity/memory_hooks">Use other hook</a>
<a href="perspicacity#" id="memhook-use-own" url="https://membean.com/mywords/perspicacity/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='perspicacity#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Heat and animosity, contest and conflict, may sharpen the wits, although they rarely do; they never strengthen the understanding, clear the <b>perspicacity</b>, guide the judgment, or improve the heart.
<span class='attribution'>&mdash; Walter Savage Landor, English writer</span>
<img alt="Walter savage landor, english writer" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Walter Savage Landor, English writer.jpg?qdep8" width="80" />
</li>
<li>
“You have been in Afghanistan, I perceive,” the middle-aged man said in greeting as the doctor entered the room. The doctor, just returned from the second Anglo-Afghan War, was amazed by the man’s <b>perspicacity</b>.
<cite class='attribution'>
&mdash;
The New York Times Magazine
</cite>
</li>
<li>
Brute force isn't the only source of strength, and spiritual devotion isn't a sign of diminished intellectual <b>perspicacity</b>.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
In 57 percent of the tests, the ape chose the box that elicited a smile from the scientist rather than an expression of disgust. Good choice. The box that brought the smile contained a grape, and the ape was rewarded for his <b>perspicacity</b> in reading human facial expressions.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/perspicacity/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='perspicacity#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='per_through' data-tree-url='//cdn1.membean.com/public/data/treexml' href='perspicacity#'>
<span class=''></span>
per-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>through</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='spic_see' data-tree-url='//cdn1.membean.com/public/data/treexml' href='perspicacity#'>
<span class=''></span>
spic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>see, observe, look, watch over</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='acity_characteristics' data-tree-url='//cdn1.membean.com/public/data/treexml' href='perspicacity#'>
<span class=''></span>
-acity
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>characteristics of</td>
</tr>
</table>
<p><em>Perspicacity</em> refers to the &#8220;characteristics of seeing through&#8221; something in order to quickly understand it.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='perspicacity#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Perspicacity" src="https://cdn0.membean.com/public/images/wordimages/cons2/perspicacity.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='perspicacity#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>acuity</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>acumen</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>adroit</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>aptitude</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>astute</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>capacious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>cogent</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>cogitate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>cognitive</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>deft</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>discern</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>incisive</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>ingenuity</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>lucid</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>luminous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>omniscient</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>pellucid</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>permeate</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>precocious</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>profundity</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>sagacious</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>savant</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>ambiguous</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>conundrum</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>convoluted</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>imprudent</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>judicious</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>misapprehension</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>myopic</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>naive</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>obtuse</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>opaque</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>paradox</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>unwitting</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>vacuous</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>vaporous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='perspicacity#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
perspicacious
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>very perceptive; quick to understand</td>
</tr>
<tr>
<td class='wordform'>
<span>
perspicuity
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>clearness of expression; intelligibility</td>
</tr>
<tr>
<td class='wordform'>
<span>
perspicuous
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>clear in expression; highly intelligible</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="perspicacity" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>perspicacity</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="perspicacity#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

