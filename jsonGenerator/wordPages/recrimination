
<!DOCTYPE html>
<html>
<head>
<title>Word: recrimination | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you absolve someone, you publicly and formally say that they are not guilty or responsible for any wrongdoing.</p>
<p class='rw-defn idx1' style='display:none'>When you admonish someone, you tell them gently but with seriousness that they have done something wrong; you usually caution and advise them not to do it again.</p>
<p class='rw-defn idx2' style='display:none'>If one thing is analogous to another, a comparison can be made between the two because they are similar in some way.</p>
<p class='rw-defn idx3' style='display:none'>Badinage is lighthearted conversation that involves teasing, jokes, and humor.</p>
<p class='rw-defn idx4' style='display:none'>Censure is written or verbal disapproval and harsh criticism of something that someone has done.</p>
<p class='rw-defn idx5' style='display:none'>If you chastise someone, you speak to them angrily or punish them for doing something wrong.</p>
<p class='rw-defn idx6' style='display:none'>If someone is granted clemency, they are punished less severely than they could have been—they have been shown mercy.</p>
<p class='rw-defn idx7' style='display:none'>A contemptible act is shameful, disgraceful, and worthy of scorn.</p>
<p class='rw-defn idx8' style='display:none'>When you correlate two things, you compare, associate, or equate them together in some way.</p>
<p class='rw-defn idx9' style='display:none'>If you are culpable for an action, you are held responsible for something wrong or bad that has happened.</p>
<p class='rw-defn idx10' style='display:none'>To decry something is to speak against it and find fault with it.</p>
<p class='rw-defn idx11' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx12' style='display:none'>If you denounce people or actions, you criticize them severely in public because you feel strongly that they are wrong or evil.</p>
<p class='rw-defn idx13' style='display:none'>An encomium strongly praises someone or something via oral or written communication.</p>
<p class='rw-defn idx14' style='display:none'>An exemplary person is one that sets an ideal or praiseworthy example for others to follow.</p>
<p class='rw-defn idx15' style='display:none'>If you expostulate with someone, you express strong disagreement with or disapproval of what that person is doing.</p>
<p class='rw-defn idx16' style='display:none'>If you extol something or someone, you praise it or them very enthusiastically.</p>
<p class='rw-defn idx17' style='display:none'>Two items are fungible if one can be exchanged for the other with no loss in inherent value; for example, one $10 bill and two $5 bills are fungible.</p>
<p class='rw-defn idx18' style='display:none'>A harangue is a long, scolding speech.</p>
<p class='rw-defn idx19' style='display:none'>Things that are homologous are similar in structure, function, or value; these qualities may suggest or indicate a common ancestor or origin.</p>
<p class='rw-defn idx20' style='display:none'>If you describe someone&#8217;s appearance or behavior as impeccable, you mean that it is perfect and therefore impossible to criticize.</p>
<p class='rw-defn idx21' style='display:none'>If you impugn someone&#8217;s motives or integrity, you say that they do not deserve to be trusted.</p>
<p class='rw-defn idx22' style='display:none'>If you impute something, such as blame or a crime, to somebody, you say (usually unfairly) that that person is responsible for it.</p>
<p class='rw-defn idx23' style='display:none'>If something is infallible, it is never wrong and so is incapable of making mistakes.</p>
<p class='rw-defn idx24' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx25' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx26' style='display:none'>Malfeasance is an unlawful act, especially one committed by a trusted public official.</p>
<p class='rw-defn idx27' style='display:none'>Something is obverse when it is facing or turned towards the observer, such as the facing side of a coin.</p>
<p class='rw-defn idx28' style='display:none'>If you receive a plaudit, you receive admiration, praise, and approval from someone.</p>
<p class='rw-defn idx29' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx30' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx31' style='display:none'>A punitive action is intended to punish someone.</p>
<p class='rw-defn idx32' style='display:none'>When you rebuke someone, you harshly scold or criticize them for something they&#8217;ve done.</p>
<p class='rw-defn idx33' style='display:none'>If you reciprocate, you give something to someone because they have given something similar to you.</p>
<p class='rw-defn idx34' style='display:none'>You show rectitude if you behave or conduct yourself in an honest and morally correct manner.</p>
<p class='rw-defn idx35' style='display:none'>To remonstrate with someone is to tell that person that you strongly disapprove of something they have said or done.</p>
<p class='rw-defn idx36' style='display:none'>If you think a type of behavior or idea is reprehensible, you think that it is very bad, morally wrong, and deserves to be strongly criticized.</p>
<p class='rw-defn idx37' style='display:none'>When you are given a reprimand, you are scolded, blamed, or given a talking-to by someone for something wrong that you did.</p>
<p class='rw-defn idx38' style='display:none'>Restitution is the formal act of giving something that was taken away back to the rightful owner—or paying them money for the loss of the object.</p>
<p class='rw-defn idx39' style='display:none'>When you retaliate, you get back at or get even with someone for something that they did to you.</p>
<p class='rw-defn idx40' style='display:none'>If one thing is tantamount to another thing, it means that it is equivalent to the other.</p>
<p class='rw-defn idx41' style='display:none'>If you feel unrequited love for another, you love that person, but they don&#8217;t love you in return.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>recrimination</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='recrimination#' id='pronounce-sound' path='audio/words/amy-recrimination'></a>
ri-krim-uh-NAY-shuhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='recrimination#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='recrimination#' id='context-sound' path='audio/wordcontexts/brian-recrimination'></a>
You had better not accuse me of having stolen your lunch money today, because I will come back with a <em>recrimination</em> or charge of wrongdoing for stealing mine yesterday.  Let&#8217;s just make things even now, and not offer <em>recriminations</em> or allegations against each other.  But if you do challenge me on the wrong that you think I did, don&#8217;t think that I will not come back with a <em>recrimination</em> or countercharge!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If someone says that it isn&#8217;t the time for <em>recriminations</em>, what do they mean?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It&#8217;s not a good time to worry about the cost of things that must be done.
</li>
<li class='choice answer '>
<span class='result'></span>
It&#8217;s not a good time to have everyone blaming each other for what happened.
</li>
<li class='choice '>
<span class='result'></span>
It&#8217;s not a good time to look back at mistakes that could not be avoided.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='recrimination#' id='definition-sound' path='audio/wordmeanings/amy-recrimination'></a>
A <em>recrimination</em> is a retaliatory accusation you make against someone who has accused you of something first.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>accusation in return</em>
</span>
</span>
</div>
<a class='quick-help' href='recrimination#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='recrimination#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/recrimination/memory_hooks/5722.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Crim</span></span>e Elim<span class="emp3"><span>ination</span></span></span></span> If you threaten a re<span class="emp1"><span>crim</span></span><span class="emp3"><span>ination</span></span> against the judge, he'll el<span class="emp3"><span>iminat</span></span>e your <span class="emp1"><span>crim</span></span>e in no time at all so he doesn't get into trouble himself.
</p>
</div>

<div id='memhook-button-bar'>
<a href="recrimination#" id="add-public-hook" style="" url="https://membean.com/mywords/recrimination/memory_hooks">Use other public hook</a>
<a href="recrimination#" id="memhook-use-own" url="https://membean.com/mywords/recrimination/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='recrimination#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
If we fall, we don't need self-<b>recrimination</b> or blame or anger—we need a reawakening of our intention and a willingness to recommit, to be wholehearted once again.
<span class='attribution'>&mdash; Sharon Salzberg, American writer</span>
<img alt="Sharon salzberg, american writer" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Sharon Salzberg, American writer.jpg?qdep8" width="80" />
</li>
<li>
Armstrong expressed concern for the state of cycling and for the future of a sport given to scandal, <b>recrimination</b> and bitter public debate.
<cite class='attribution'>
&mdash;
ESPN
</cite>
</li>
<li>
Deportivo had been beaten 4–0 by Barcelona, but there was no anger, no <b>recrimination</b> and few empty seats, vacated by the disgusted and depressed, determined not to see out the defeat. Instead, there was applause.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
The hazardous-but-high-reward maneuver comes five weeks into the oil spill crisis amid an intensifying atmosphere of political <b>recrimination</b> that has spread from the Gulf Coast to the White House and Congress.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/recrimination/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='recrimination#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='re_back' data-tree-url='//cdn1.membean.com/public/data/treexml' href='recrimination#'>
<span class='common'></span>
re-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>back, again</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='crimin_judgment' data-tree-url='//cdn1.membean.com/public/data/treexml' href='recrimination#'>
<span class=''></span>
crimin
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>judgment, crime</td>
</tr>
<tr>
<td class='partform'>-ation</td>
<td>
&rarr;
</td>
<td class='meaning'>act of doing something</td>
</tr>
</table>
<p><em>Recrimination</em> is the &#8220;act of (passing) judgment back&#8221; on someone else to counter an accusation.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='recrimination#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Recrimination" src="https://cdn2.membean.com/public/images/wordimages/cons2/recrimination.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='recrimination#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>admonish</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>analogous</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>badinage</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>censure</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>chastise</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>contemptible</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>correlate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>culpable</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>decry</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>denounce</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>expostulate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>fungible</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>harangue</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>homologous</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>impugn</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>impute</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>malfeasance</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>obverse</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>punitive</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>rebuke</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>reciprocate</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>remonstrate</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>reprehensible</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>reprimand</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>restitution</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>retaliate</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>tantamount</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>absolve</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>clemency</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>encomium</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>exemplary</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>extol</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>impeccable</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>infallible</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>plaudit</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>rectitude</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>unrequited</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="recrimination" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>recrimination</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="recrimination#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

