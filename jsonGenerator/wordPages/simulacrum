
<!DOCTYPE html>
<html>
<head>
<title>Word: simulacrum | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Simulacrum-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/simulacrum-large.jpg?qdep8" />
</div>
<a href="simulacrum#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>If you have an affiliation with a group or another person, you are officially involved or connected with them.</p>
<p class='rw-defn idx1' style='display:none'>Something that is amorphous has no clear shape, boundaries, or structure.</p>
<p class='rw-defn idx2' style='display:none'>If one thing is analogous to another, a comparison can be made between the two because they are similar in some way.</p>
<p class='rw-defn idx3' style='display:none'>Atavism is the reappearance of a genetic feature that has been missing for generations; it can also refer to a manner or style that resurfaces after a period of absence.</p>
<p class='rw-defn idx4' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx5' style='display:none'>Consanguinity is the state of being related to someone else by blood or having a similar close relationship to them.</p>
<p class='rw-defn idx6' style='display:none'>A credulous person is very ready to believe what people tell them; therefore, they can be easily tricked or cheated.</p>
<p class='rw-defn idx7' style='display:none'>An effigy is a crude likeness or statue of a person, often of someone who is disliked.</p>
<p class='rw-defn idx8' style='display:none'>Empirical evidence or study is based on real experience or scientific experiments rather than on unproven theories.</p>
<p class='rw-defn idx9' style='display:none'>If you describe something as ersatz, you dislike it because it is artificial or fake—and is used in place of a higher quality item.</p>
<p class='rw-defn idx10' style='display:none'>A facade is a false outward appearance or way of behaving that hides what someone or something is really like.</p>
<p class='rw-defn idx11' style='display:none'>A feint is the act of pretending to make a movement in one direction while actually moving in the other, especially to trick an opponent; a feint can also be a deceptive act meant to turn attention away from one&#8217;s true purpose.</p>
<p class='rw-defn idx12' style='display:none'>Things that are homologous are similar in structure, function, or value; these qualities may suggest or indicate a common ancestor or origin.</p>
<p class='rw-defn idx13' style='display:none'>Something that is hypothetical is based on possible situations or events rather than actual ones.</p>
<p class='rw-defn idx14' style='display:none'>A metaphor is a word or phrase that means one thing but is used to describe something else in order to highlight similar qualities; for example, if someone is very brave, you might say that they have the heart of a lion.</p>
<p class='rw-defn idx15' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx16' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx17' style='display:none'>A patina is a smooth, shiny film or surface that gradually develops on things—such as wood, leather, and metal utensils—that have seen a lot of use.</p>
<p class='rw-defn idx18' style='display:none'>The propinquity of a thing is its nearness in location, relationship, or similarity to another thing.</p>
<p class='rw-defn idx19' style='display:none'>A semblance is an outward appearance of what is wanted or expected but is not exactly as hoped for.</p>
<p class='rw-defn idx20' style='display:none'>Something that is specious seems to be good, sound, or correct but is actually wrong or fake.</p>
<p class='rw-defn idx21' style='display:none'>To be tentative is to be hesitant or uncertain about something; an agreement or decision of this kind is likely to have changes before it reaches its final form.</p>
<p class='rw-defn idx22' style='display:none'>A veneer is a thin layer, such as a thin sheet of expensive wood over a cheaper type of wood that gives a false appearance of higher quality; a person can also put forth a false front or veneer.</p>
<p class='rw-defn idx23' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx24' style='display:none'>Verisimilitude is something&#8217;s authenticity or appearance of being real or true.</p>
<p class='rw-defn idx25' style='display:none'>The verity of something is the truth or reality of it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>simulacrum</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='simulacrum#' id='pronounce-sound' path='audio/words/amy-simulacrum'></a>
sim-yuh-LAY-kruhm
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='simulacrum#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='simulacrum#' id='context-sound' path='audio/wordcontexts/brian-simulacrum'></a>
In his vest pocket, Marcus carried a precious <em>simulacrum</em> or painted image of his beloved daughter.  This portrait or <em>simulacrum</em> was kept carefully within the lid of his antique watch.  Marcus had commissioned a famous artist to capture the likeness of his dear girl&#8217;s face, but the result, while a beautiful replica or <em>simulacrum</em>, could not exactly match its original, living model.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>simulacrum</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A precious possession that travels with you.
</li>
<li class='choice '>
<span class='result'></span>
A brilliant piece of visual art that is widely recognized for its ingenuity.
</li>
<li class='choice answer '>
<span class='result'></span>
A duplicate of something else of varying accuracy.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='simulacrum#' id='definition-sound' path='audio/wordmeanings/amy-simulacrum'></a>
A <em>simulacrum</em> is an image or representation of something that can be a true copy or may just have a vague similarity to it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>copy</em>
</span>
</span>
</div>
<a class='quick-help' href='simulacrum#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='simulacrum#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/simulacrum/memory_hooks/3766.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Crum</span></span>b <span class="emp3"><span>Simula</span></span>tion</span></span> My <span class="emp3"><span>simula</span></span><span class="emp1"><span>crum</span></span> of <span class="emp3"><span>simula</span></span>ted <span class="emp1"><span>crum</span></span>bs is so useful!  Just think, instead of having to buy an expensive birthday cake for a party, you can just use my reusable <span class="emp3"><span>simula</span></span>ted <span class="emp1"><span>crum</span></span>bs <span class="emp3"><span>simula</span></span><span class="emp1"><span>crum</span></span>!  Wow!  Everyone will think that the cake has already been served, and may be glad they missed it!
</p>
</div>

<div id='memhook-button-bar'>
<a href="simulacrum#" id="add-public-hook" style="" url="https://membean.com/mywords/simulacrum/memory_hooks">Use other public hook</a>
<a href="simulacrum#" id="memhook-use-own" url="https://membean.com/mywords/simulacrum/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='simulacrum#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Now we could build the bronze version according to Leonardo’s exact specifications. But should we? Experts and art lovers can tell the <b>simulacrum</b> from the authentic work. The rest of the world could, likewise, if they tried, but they may not care to.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
The inflatable jackets are activated when a Facebook friend “likes” a photo or a message. The jacket encompasses its wearer in an embrace—a <b>simulacrum</b> of a hug, sent over the Internet.
<cite class='attribution'>
&mdash;
The Boston Globe
</cite>
</li>
<li>
While clusters of fans have spent hours in the parking lot waiting for Pacquiao, the scene is merely a <b>simulacrum</b> of the madness back home.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/simulacrum/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='simulacrum#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='simul_pretend' data-tree-url='//cdn1.membean.com/public/data/treexml' href='simulacrum#'>
<span class=''></span>
simul
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>pretend, copy, imitate</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='um_noun' data-tree-url='//cdn1.membean.com/public/data/treexml' href='simulacrum#'>
<span class=''></span>
-um
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>forms a singular Latin noun</td>
</tr>
</table>
<p>A <em>simulacrum</em> is a &#8220;pretending, copying, or imitation&#8221; of something else.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='simulacrum#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Simulacrum" src="https://cdn2.membean.com/public/images/wordimages/cons2/simulacrum.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='simulacrum#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>affiliation</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>analogous</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>atavism</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>credulous</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>effigy</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>ersatz</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>facade</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>feint</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>homologous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>hypothetical</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>metaphor</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>patina</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>propinquity</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>semblance</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>specious</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>tentative</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>veneer</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>amorphous</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>consanguinity</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>empirical</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>verisimilitude</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="simulacrum" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>simulacrum</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="simulacrum#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

