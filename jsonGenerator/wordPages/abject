
<!DOCTYPE html>
<html>
<head>
<title>Word: abject | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abase yourself, people respect you less because you act in a way that is beneath you; due to this behavior, you are lowered or reduced in rank, esteem, or reputation. </p>
<p class='rw-defn idx1' style='display:none'>If you abominate something, you hate it because you think it is extremely wrong and unacceptable.</p>
<p class='rw-defn idx2' style='display:none'>A situation or condition that is abysmal is extremely bad or of wretched quality.</p>
<p class='rw-defn idx3' style='display:none'>When you ameliorate a bad condition or situation, you make it better in some way.</p>
<p class='rw-defn idx4' style='display:none'>Something that is apposite is relevant or suitable to what is happening or being discussed.</p>
<p class='rw-defn idx5' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx6' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx7' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx8' style='display:none'>A condign reward or punishment is deserved by and appropriate or fitting for the person who receives it.</p>
<p class='rw-defn idx9' style='display:none'>A contemptible act is shameful, disgraceful, and worthy of scorn.</p>
<p class='rw-defn idx10' style='display:none'>Someone who is crestfallen is severely disappointed, sad, or depressed.</p>
<p class='rw-defn idx11' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx12' style='display:none'>Depravity is behavior that is immoral, corrupt, or evil.</p>
<p class='rw-defn idx13' style='display:none'>If you deprecate something, you disapprove of it strongly.</p>
<p class='rw-defn idx14' style='display:none'>If you desecrate something that is considered holy or very special, you deliberately spoil or damage it.</p>
<p class='rw-defn idx15' style='display:none'>If you say a person&#8217;s actions are despicable, you think they are extremely unpleasant or nasty.</p>
<p class='rw-defn idx16' style='display:none'>If you are despondent, you are extremely unhappy because you are in an unpleasant situation that you do not think will improve.</p>
<p class='rw-defn idx17' style='display:none'>If you extol something or someone, you praise it or them very enthusiastically.</p>
<p class='rw-defn idx18' style='display:none'>An action that is flagrant shows that someone does not care if they obviously break the rules or highly offend people.</p>
<p class='rw-defn idx19' style='display:none'>A hapless person is unlucky, so much so that a stream of unfortunate things has happened and continues to happen to them.</p>
<p class='rw-defn idx20' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx21' style='display:none'>When someone is lionized, they are treated as being very important or famous—although they may not deserve to be.</p>
<p class='rw-defn idx22' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx23' style='display:none'>If you mollify someone, you say or do something to make that person feel less angry or upset.</p>
<p class='rw-defn idx24' style='display:none'>If you nettle someone, you irritate or annoy them.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx26' style='display:none'>If you describe people or things as odious, you think that they are extremely unpleasant.</p>
<p class='rw-defn idx27' style='display:none'>Something that is pristine has not lost any of its original perfection or purity.</p>
<p class='rw-defn idx28' style='display:none'>If something is requisite for a purpose, it is needed, appropriate, or necessary for that specific purpose.</p>
<p class='rw-defn idx29' style='display:none'>The word squalor describes very dirty and unpleasant conditions that people live or work in, usually due to poverty or neglect.</p>
<p class='rw-defn idx30' style='display:none'>When you are stultified by something, you lose interest in it because it is dull, boring, or time-consuming.</p>
<p class='rw-defn idx31' style='display:none'>To taint is to give an undesirable quality that damages a person&#8217;s reputation or otherwise spoils something.</p>
<p class='rw-defn idx32' style='display:none'>Something that is unsullied is unstained and clean.</p>
<p class='rw-defn idx33' style='display:none'>Venerable people command respect because they are old and wise.</p>
<p class='rw-defn idx34' style='display:none'>If someone vitiates something, they make it less effective, weaker, or reduced in quality.</p>
<p class='rw-defn idx35' style='display:none'>Vitriolic words are bitter, unkind, and mean-spirited.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>abject</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='abject#' id='pronounce-sound' path='audio/words/amy-abject'></a>
AB-jekt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='abject#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='abject#' id='context-sound' path='audio/wordcontexts/brian-abject'></a>
As Alicia stood center stage and tried to overcome her stage fright, she hung her head in <em>abject</em>, hopeless humiliation.  The <em>abject</em>, miserable fear twisted her belly into knots and prevented her from speaking.  On the front row of the audience, her mother sat frozen in <em>abject</em>, extreme horror as her daughter&#8217;s worst fear played out on stage.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does <em>abject</em> mean?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It refers to something that is very funny.
</li>
<li class='choice '>
<span class='result'></span>
It is a word that means very low or deep.
</li>
<li class='choice answer '>
<span class='result'></span>
It is a way to describe a horrible situation.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='abject#' id='definition-sound' path='audio/wordmeanings/amy-abject'></a>
The word <em>abject</em> emphasizes a very bad situation or quality, thereby making it even worse.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>thorough</em>
</span>
</span>
</div>
<a class='quick-help' href='abject#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='abject#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/abject/memory_hooks/5392.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Jek</span></span>yll <span class="emp1"><span>Ob</span></span><span class="emp3"><span>ject</span></span>s</span></span> "I <span class="emp1"><span>ob</span></span><span class="emp3"><span>ject</span></span> to being called an <span class="emp1"><span>ab</span></span><span class="emp3"><span>ject</span></span> failure," said Dr. <span class="emp3"><span>Jek</span></span>yll to the snide Mr. Hyde.  "After all, I am you, so I can't be a complete loser."
</p>
</div>

<div id='memhook-button-bar'>
<a href="abject#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/abject/memory_hooks">Use other hook</a>
<a href="abject#" id="memhook-use-own" url="https://membean.com/mywords/abject/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='abject#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
The absence of love is the most <b>abject</b> pain.
<span class='attribution'>&mdash; Nosferatu the Vampyre, remake of a 1922 silent horror film</span>
<img alt="Nosferatu the vampyre, remake of a 1922 silent horror film" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Nosferatu the Vampyre, remake of a 1922 silent horror film.jpg?qdep8" width="80" />
</li>
<li>
If the <b>abject</b> failure of their opposition to Whole Foods makes them think twice, there is always the European Union, which has long been deploying controversial definitions of market abuse against Microsoft.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
In his world-record 200-meter dash, Usain Bolt averaged a little more than [twenty-three] mph for nearly [twenty] seconds. That would make him an <b>abject</b> failure as a savanna hunter because an antelope can double that clip for minutes at a time.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Florida will also entertain offers for star pitcher Dontrelle Willis, whose poor [2007] season should be mitigated by an <b>abject</b> lack of top-flight pitching on the free-agent market.
<cite class='attribution'>
&mdash;
Sox Talk
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/abject/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='abject#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ab_away' data-tree-url='//cdn1.membean.com/public/data/treexml' href='abject#'>
<span class='common'></span>
ab-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>away, from</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ject_thrown' data-tree-url='//cdn1.membean.com/public/data/treexml' href='abject#'>
<span class='common'></span>
ject
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thrown</td>
</tr>
</table>
<p>When one is &#8220;thrown from&#8221; or &#8220;thrown away&#8221; from one&#8217;s normal emotional state, one becomes &#8220;abject.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='abject#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Duck Soup</strong><span> The man being questioned is an abject figure.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/abject.jpg' video_url='examplevids/abject' video_width='350'></span>
<div id='wt-container'>
<img alt="Abject" height="288" src="https://cdn1.membean.com/video/examplevids/abject.jpg" width="350" />
<div class='center'>
<a href="abject#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='abject#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Abject" src="https://cdn0.membean.com/public/images/wordimages/cons2/abject.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='abject#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abase</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abominate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abysmal</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>contemptible</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>crestfallen</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>depravity</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>deprecate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>desecrate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>despicable</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>despondent</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>flagrant</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>hapless</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>nettle</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>odious</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>squalor</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>stultify</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>taint</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>vitiate</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>vitriolic</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>ameliorate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>apposite</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>condign</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>extol</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>lionize</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>mollify</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>pristine</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>requisite</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>unsullied</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>venerable</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="abject" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>abject</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="abject#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

