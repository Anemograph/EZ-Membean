
<!DOCTYPE html>
<html>
<head>
<title>Word: largess | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you are acquisitive, you are driven to pursue and own wealth and possessions—often in a greedy fashion.</p>
<p class='rw-defn idx1' style='display:none'>Affluence is the state of having a lot of money and many possessions, granting one a high economic standard of living.</p>
<p class='rw-defn idx2' style='display:none'>If your behavior or manner is altruistic, you show you care more about other people and their interests than you care about yourself.</p>
<p class='rw-defn idx3' style='display:none'>Avarice is the extremely strong desire to have a lot of money and possessions.</p>
<p class='rw-defn idx4' style='display:none'>A benefaction is a charitable contribution of money or assistance that someone gives to a person or organization.</p>
<p class='rw-defn idx5' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx6' style='display:none'>When you bequeath something, you hand it down to someone in a will or pass it on from one generation to the next.</p>
<p class='rw-defn idx7' style='display:none'>When something is bestowed upon you—usually something valuable—you are given or presented it.</p>
<p class='rw-defn idx8' style='display:none'>If you covet something that someone else has, you have a strong desire to have it for yourself.</p>
<p class='rw-defn idx9' style='display:none'>Depredation can be the act of destroying or robbing a village—or the overall damage that time can do to things held dear.</p>
<p class='rw-defn idx10' style='display:none'>Someone who is frugal spends very little money—and even then only on things that are absolutely necessary.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is impecunious has very little money, especially over a long period of time.</p>
<p class='rw-defn idx12' style='display:none'>Lavish praise, giving, or a meal is rich, plentiful, or very generous; it can sometimes border on being too much.</p>
<p class='rw-defn idx13' style='display:none'>If someone exhibits magnanimity towards another, they show them kindness and noble generosity, especially after defeating them in battle or after having been treated badly by them.</p>
<p class='rw-defn idx14' style='display:none'>A mercenary person is one whose sole interest is in earning money.</p>
<p class='rw-defn idx15' style='display:none'>A misanthrope is someone who hates and mistrusts people.</p>
<p class='rw-defn idx16' style='display:none'>A munificent person is extremely generous, especially with money.</p>
<p class='rw-defn idx17' style='display:none'>A parsimonious person is not willing to give or spend money.</p>
<p class='rw-defn idx18' style='display:none'>Penury is the state of being extremely poor.</p>
<p class='rw-defn idx19' style='display:none'>Philanthropy is unselfish support in the form of donating money, work, or gifts to positive social purposes; philanthropy is also overall love for humans in general.</p>
<p class='rw-defn idx20' style='display:none'>If soldiers pillage a place, such as a town or museum, they steal a lot of things and damage it using violent methods.</p>
<p class='rw-defn idx21' style='display:none'>If you say that you&#8217;ve received a pittance, you mean that you received a small amount of something—and you know that you deserved more.</p>
<p class='rw-defn idx22' style='display:none'>When you plunder, you forcefully take or rob others of their possessions, usually during times of war, natural disaster, or other unrest.</p>
<p class='rw-defn idx23' style='display:none'>If you suffer privation, you live without many of the basic things required for a comfortable life.</p>
<p class='rw-defn idx24' style='display:none'>When you procure something, you obtain or get it in some fashion.</p>
<p class='rw-defn idx25' style='display:none'>When you proffer something to someone, you offer it up or hold it out to them to take.</p>
<p class='rw-defn idx26' style='display:none'>To ransack a place is to thoroughly loot or rob items from it; it can also mean to look through something thoroughly by examining every bit of it.</p>
<p class='rw-defn idx27' style='display:none'>When you ravage something, you completely destroy, wreck, or damage it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>largess</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='largess#' id='pronounce-sound' path='audio/words/amy-largess'></a>
lahr-JES
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='largess#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='largess#' id='context-sound' path='audio/wordcontexts/brian-largess'></a>
But one example of Lawrence&#8217;s <em>largess</em> or generosity is the grand donation he recently made to his alma mater.  In a stunning act of financial <em>largess</em>, Lawrence gave the college $10 million for a new library.  Lawrence&#8217;s former college was, of course, thrilled to benefit from such free giving or honorable <em>largess</em>.  Universities and colleges are often financially strapped, so they depend upon the <em>largess</em> of wealthy donors and thoughtful alumni to help them financially.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What feeling would someone&#8217;s <em>largess</em> most likely invoke?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Humility in comparison to their vast intellect.
</li>
<li class='choice answer '>
<span class='result'></span>
Gratitude for their overwhelming generosity.
</li>
<li class='choice '>
<span class='result'></span>
Awe at the size of their business empire.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='largess#' id='definition-sound' path='audio/wordmeanings/amy-largess'></a>
<em>Largess</em> is the generous act of giving money or presents to a large number of people.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>generosity</em>
</span>
</span>
</div>
<a class='quick-help' href='largess#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='largess#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/largess/memory_hooks/3045.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Larg</span></span>e <span class="emp3"><span>Ess</span></span>ay</span></span> Our prof<span class="emp3"><span>ess</span></span>or told us that he was showing us a great deal of <span class="emp2"><span>larg</span></span><span class="emp3"><span>ess</span></span> by assigning us such a <span class="emp2"><span>larg</span></span>e <span class="emp3"><span>ess</span></span>ay--we did not agree.
</p>
</div>

<div id='memhook-button-bar'>
<a href="largess#" id="add-public-hook" style="" url="https://membean.com/mywords/largess/memory_hooks">Use other public hook</a>
<a href="largess#" id="memhook-use-own" url="https://membean.com/mywords/largess/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='largess#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
[The Roman tribune] Clodius was establishing a welfare state gone wild by passing out grain at no cost to a large portion of the city’s population. A substantial share of the government revenue suddenly shifted to paying for the <b>largesse</b> of Clodius. It was an obvious ploy to garner the favor of the urban masses, but it worked nonetheless.
<span class='attribution'>&mdash; Phillip Freeman, American author and professor of the humanities, from _Julius Caesar_ </span>
<img alt="Phillip freeman, american author and professor of the humanities, from _julius caesar_ " height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Phillip Freeman, American author and professor of the humanities, from _Julius Caesar_ .jpg?qdep8" width="80" />
</li>
<li>
Craig Newmark Philanthropies, has given $6 million to Consumer Reports, allowing it to keep a closer watch over digital products and platforms. It is the <b>largest</b> donation in the history of the organization, which was established in 1936. Past beneficiaries of Mr. Newmark’s <b>largess</b> have included New York Public Radio, a fledgling investigative site called The Markup and a New York-centric journalistic start-up, The City.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
This year’s crop of graduates are going to be raking in a record $4.8 billion in graduation gifts, according to the National Retail Federation’s annual survey. . . . Cash is the most popular gift—more than half plan to give it—so it’s possible some of this <b>largess</b> could be going towards the student loan debts many of these young adults have accumulated over the past four years.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
<li>
In fact, [Bill] Gates may be remembered less for any of that than he is for his philanthropic work—perhaps even as the greatest philanthropist the world has yet to see. Just like Andrew Carnegie, who today is remembered more for his charitable <b>largess</b> than his exploits in the steel industry, Gates' business legacy may fade over time in comparison to his work with the namesake foundation that he set up with his wife Melinda.
<cite class='attribution'>
&mdash;
Computer World
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/largess/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='largess#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>larg</td>
<td>
&rarr;
</td>
<td class='meaning'>generous, bountiful</td>
</tr>
</table>
<p>A donor who exhibits <em>largess</em> is &#8220;generous or bountiful&#8221; on a regular basis.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='largess#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Largess" src="https://cdn0.membean.com/public/images/wordimages/cons2/largess.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='largess#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>affluence</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>altruistic</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>benefaction</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>bequeath</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>bestow</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>lavish</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>magnanimity</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>munificent</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>philanthropy</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>proffer</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>acquisitive</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>avarice</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>covet</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>depredation</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>frugal</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>impecunious</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>mercenary</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>misanthrope</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>parsimonious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>penury</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>pillage</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>pittance</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>plunder</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>privation</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>procure</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>ransack</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>ravage</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='largess#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
largesse
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>alternate spelling of "largess"</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="largess" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>largess</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="largess#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

