
<!DOCTYPE html>
<html>
<head>
<title>Word: ingrained | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>If you abridge a book or play, you make it shorter by making cuts to the original.</p>
<p class='rw-defn idx2' style='display:none'>An addendum is an additional section added to the end of a book, document, or speech that gives more information.</p>
<p class='rw-defn idx3' style='display:none'>When you assimilate an idea, you completely understand it; likewise, when someone is assimilated into a new place, they become part of it by understanding what it is all about and by being accepted by others who live there.</p>
<p class='rw-defn idx4' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx5' style='display:none'>Attrition is the process of gradually decreasing the strength of something (such as an enemy force) by continually attacking it.</p>
<p class='rw-defn idx6' style='display:none'>To augment something is to increase its value or effectiveness by adding something to it.</p>
<p class='rw-defn idx7' style='display:none'>You cajole people by gradually persuading them to do something, usually by being very nice to them.</p>
<p class='rw-defn idx8' style='display:none'>To corroborate something that has been said or reported means to give information or evidence that further supports it.</p>
<p class='rw-defn idx9' style='display:none'>If you curtail something, you reduce or limit it.</p>
<p class='rw-defn idx10' style='display:none'>When you deplete a supply of something, you use it up or lessen its amount over a given period of time.</p>
<p class='rw-defn idx11' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx12' style='display:none'>Someone is being dogmatic when they express opinions in an assertive and often overly self-important manner.</p>
<p class='rw-defn idx13' style='display:none'>When an amount of something dwindles, it becomes less and less over time.</p>
<p class='rw-defn idx14' style='display:none'>To efface something is to erase or remove it completely from recognition or memory.</p>
<p class='rw-defn idx15' style='display:none'>Something ephemeral, such as some insects or a sunset, lasts for only a short time or has a very short lifespan.</p>
<p class='rw-defn idx16' style='display:none'>When you excise something, you remove it by cutting it out.</p>
<p class='rw-defn idx17' style='display:none'>If you extirpate something, you completely destroy it because it is unpleasant or unwanted.</p>
<p class='rw-defn idx18' style='display:none'>When you extricate yourself from a difficult or unpleasant situation, you manage to escape it.</p>
<p class='rw-defn idx19' style='display:none'>To imbue someone with a quality, such as an emotion or idea, is to fill that person completely with it.</p>
<p class='rw-defn idx20' style='display:none'>An immanent quality is present within something and is so much a part of it that the object cannot be imagined without that quality.</p>
<p class='rw-defn idx21' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx22' style='display:none'>An impenetrable barrier cannot be gotten through by any means; this word can refer to parts of a building such as walls and doors—or to a problem of some kind that cannot be solved.</p>
<p class='rw-defn idx23' style='display:none'>If you are impervious to things, such as someone&#8217;s actions or words, you are not affected by them or do not notice them.</p>
<p class='rw-defn idx24' style='display:none'>To inculcate is to fix an idea or belief in someone&#8217;s mind by repeatedly teaching it.</p>
<p class='rw-defn idx25' style='display:none'>If you indoctrinate someone, you teach them a set of beliefs so thoroughly that they reject any other beliefs or ideas without any critical thought.</p>
<p class='rw-defn idx26' style='display:none'>An infusion is the pouring in or the introduction of something into something else so as to fill it up.</p>
<p class='rw-defn idx27' style='display:none'>An inherent characteristic is one that exists in a person at birth or in a thing naturally.</p>
<p class='rw-defn idx28' style='display:none'>An innate quality of someone is present since birth or is a quality of something that is essential to it.</p>
<p class='rw-defn idx29' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx30' style='display:none'>Someone is obtuse when they are slow to understand things; someone can choose to be obtuse when they deliberately do not try to figure things out.</p>
<p class='rw-defn idx31' style='display:none'>To pare something down is to reduce or lessen it.</p>
<p class='rw-defn idx32' style='display:none'>The art of pedagogy is the methods used by a teacher to teach a subject.</p>
<p class='rw-defn idx33' style='display:none'>When a substance permeates something, it enters into all parts of it.</p>
<p class='rw-defn idx34' style='display:none'>If something is pervasive, it appears to be everywhere.</p>
<p class='rw-defn idx35' style='display:none'>A person who proselytizes tries to draw others to adopt their religion, beliefs, or causes.</p>
<p class='rw-defn idx36' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx37' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx38' style='display:none'>If something, such as warmth, color, or liquid, suffuses a material, it gradually spreads through or covers it; if you are suffused with a feeling, you are full of that feeling.</p>
<p class='rw-defn idx39' style='display:none'>A person or subject that is superficial is shallow, without depth, obvious, and concerned only with surface matters.</p>
<p class='rw-defn idx40' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx41' style='display:none'>When something is transfused to another thing, it is given, put, or imparted to it; for example, you can transfuse blood or a love of reading from one person to another.</p>
<p class='rw-defn idx42' style='display:none'>Something that has the quality of transience lasts for only a short time or is constantly changing.</p>
<p class='rw-defn idx43' style='display:none'>If you truncate something, you make it shorter or quicker by removing or cutting off part of it.</p>
<p class='rw-defn idx44' style='display:none'>To be under someone&#8217;s tutelage is to be under their guidance and teaching.</p>
<p class='rw-defn idx45' style='display:none'>Something that is vacuous is empty or blank, such as a mind or stare.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>ingrained</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='ingrained#' id='pronounce-sound' path='audio/words/amy-ingrained'></a>
in-GRAYND
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='ingrained#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='ingrained#' id='context-sound' path='audio/wordcontexts/brian-ingrained'></a>
The honor code rules of my private school have been <em>ingrained</em> or fixed deeply in my mind over the last twelve years.  Even when I find a dollar on the floor, I feel forced to find the owner as the fear of being caught stealing has been <em>ingrained</em> or rooted in my head since I set foot on this campus.  Our advisors speak to us at length each year about the honor code, at which times they <em>ingrain</em> into or confirm upon us its utmost importance, which seems to have worked since we live in a highly trustworthy little community.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an <em>ingrained</em> belief?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It&#8217;s a belief that you&#8217;ve stopped holding on to because it is false.
</li>
<li class='choice '>
<span class='result'></span>
It&#8217;s a new belief that you are exploring and learning more about.
</li>
<li class='choice answer '>
<span class='result'></span>
It&#8217;s a deeply fixed belief that you&#8217;ve had for a very long time.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='ingrained#' id='definition-sound' path='audio/wordmeanings/amy-ingrained'></a>
Something that has been <em>ingrained</em> in your mind has been fixed or rooted there permanently.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>rooted</em>
</span>
</span>
</div>
<a class='quick-help' href='ingrained#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='ingrained#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/ingrained/memory_hooks/4240.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>In</span></span> the B<span class="emp3"><span>rain</span></span></span></span> When a principle or idea has been <span class="emp1"><span>in</span></span>g<span class="emp3"><span>rain</span></span>ed into you, it's <span class="emp1"><span>in</span></span> your b<span class="emp3"><span>rain</span></span> for good.
</p>
</div>

<div id='memhook-button-bar'>
<a href="ingrained#" id="add-public-hook" style="" url="https://membean.com/mywords/ingrained/memory_hooks">Use other public hook</a>
<a href="ingrained#" id="memhook-use-own" url="https://membean.com/mywords/ingrained/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='ingrained#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
It is very much <b>ingrained</b> in me that you do not manage a social wrong. You should be ending it.
<cite class='attribution'>
&mdash;
Philip Mangano, former Executive Director, U.S. Interagency Council on Homelessness
</cite>
</li>
<li>
"The myth that gigantic, 80,000-ton nuclear-powered U.S. aircraft carriers are unsinkable is not believed by any serious naval officer or analyst, but it has become a deeply <b>ingrained</b> assumption in the American public consciousness," Martin Sieff argues in a new series for UPI.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
Rooting out <b>ingrained</b> bias is one of the most challenging aspects of gender equality efforts. We can pass laws and demand reform, but changing minds and mindsets is more subtle.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
<li>
Those born after 1990 have never known a world without the Internet, and it’s clear they're fully <b>ingrained</b> in the culture of "right now."
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/ingrained/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='ingrained#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='in_in' data-tree-url='//cdn1.membean.com/public/data/treexml' href='ingrained#'>
<span class='common'></span>
in-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>in, into</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='gran_grain' data-tree-url='//cdn1.membean.com/public/data/treexml' href='ingrained#'>
<span class=''></span>
gran
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>grain, seed</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ed_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='ingrained#'>
<span class=''></span>
-ed
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>having a particular state</td>
</tr>
</table>
<p>Knowledge or behavior that has been <em>ingrained</em> in a person is like &#8220;seeds or grains (put) in&#8221; them.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='ingrained#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Inside Out</strong><span> The catchy gum song is ingrained in all of their minds.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/ingrained.jpg' video_url='examplevids/ingrained' video_width='350'></span>
<div id='wt-container'>
<img alt="Ingrained" height="288" src="https://cdn1.membean.com/video/examplevids/ingrained.jpg" width="350" />
<div class='center'>
<a href="ingrained#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='ingrained#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Ingrained" src="https://cdn1.membean.com/public/images/wordimages/cons2/ingrained.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='ingrained#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>addendum</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>assimilate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>augment</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>cajole</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>corroborate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>dogmatic</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>imbue</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>immanent</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>inculcate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>indoctrinate</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>infusion</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>inherent</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>innate</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>pedagogy</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>permeate</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>pervasive</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>proselytize</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>suffuse</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>transfuse</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>tutelage</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abridge</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>attrition</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>curtail</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>deplete</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>dwindle</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>efface</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>ephemeral</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>excise</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>extirpate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>extricate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>impenetrable</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>impervious</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>obtuse</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>pare</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>superficial</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>transience</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>truncate</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>vacuous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='ingrained#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
ingrain
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>firmly establish</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="ingrained" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>ingrained</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="ingrained#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

