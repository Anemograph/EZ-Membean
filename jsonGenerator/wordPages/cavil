
<!DOCTYPE html>
<html>
<head>
<title>Word: cavil | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Adulation is praise and admiration for someone that includes more than they deserve, usually for the purposes of flattery.</p>
<p class='rw-defn idx1' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx2' style='display:none'>An aspersion is an unkind remark or unfair judgment attacking someone&#8217;s character or reputation.</p>
<p class='rw-defn idx3' style='display:none'>When you bandy about ideas with someone, you casually discuss them without caring if the discussion is informed or effective in any way.</p>
<p class='rw-defn idx4' style='display:none'>Banter is friendly conversation during which people make friendly jokes and laugh at—and with—one another.</p>
<p class='rw-defn idx5' style='display:none'>If you besmirch someone, you spoil their good reputation by saying bad things about them, usually things that you know are not true.</p>
<p class='rw-defn idx6' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is cantankerous is bad-tempered and always finds things to complain about.</p>
<p class='rw-defn idx8' style='display:none'>A captious person has a fondness for catching others at fault; hence, they are overly critical and raise unwarranted objections too often.</p>
<p class='rw-defn idx9' style='display:none'>To carp at someone is to complain about them to their face or to nag and criticize them in a whining voice.</p>
<p class='rw-defn idx10' style='display:none'>Censure is written or verbal disapproval and harsh criticism of something that someone has done.</p>
<p class='rw-defn idx11' style='display:none'>If someone is granted clemency, they are punished less severely than they could have been—they have been shown mercy.</p>
<p class='rw-defn idx12' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx13' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx14' style='display:none'>When someone is contentious, they are argumentative and likely to provoke a fight.</p>
<p class='rw-defn idx15' style='display:none'>A convivial atmosphere or occasion is friendly, pleasant, cheerful, and relaxed.</p>
<p class='rw-defn idx16' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx17' style='display:none'>If you deprecate something, you disapprove of it strongly.</p>
<p class='rw-defn idx18' style='display:none'>If you disparage someone or something, you say unpleasant words that show you have no respect for that person or thing.</p>
<p class='rw-defn idx19' style='display:none'>An encomium strongly praises someone or something via oral or written communication.</p>
<p class='rw-defn idx20' style='display:none'>A eulogy is a speech or other piece of writing, often part of a funeral, in which someone or something is highly praised.</p>
<p class='rw-defn idx21' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx22' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx23' style='display:none'>A panegyric is a speech or article that praises someone or something a lot.</p>
<p class='rw-defn idx24' style='display:none'>If you refer to something as a trifle, you mean that it is of little or no importance or value.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>cavil</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='cavil#' id='pronounce-sound' path='audio/words/amy-cavil'></a>
KAV-uhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='cavil#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='cavil#' id='context-sound' path='audio/wordcontexts/brian-cavil'></a>
Sheila listened to her mother needlessly complain and <em>cavil</em> about how the cards did not match the flowers at the wedding reception.  Her mother picked at each detail, finding fault with and <em>caviling</em> about perceived oversights.  Sheila could listen to the meaningless <em>caviling</em> no longer; she went to the kitchen to breathe and brew tea.  Sheila finally returned and told her critical mother to cease her <em>caviling</em> and think about what was truly important.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When poet Alexander Pope wrote, &#8220;<em>Cavil</em> you may, but never criticise,&#8221; what did he mean?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Protest and whine all you want about the small stuff in life.
</li>
<li class='choice '>
<span class='result'></span>
Help others through challenges without being judgmental.
</li>
<li class='choice '>
<span class='result'></span>
Carefully assess critical details to discover hidden flaws.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='cavil#' id='definition-sound' path='audio/wordmeanings/amy-cavil'></a>
To <em>cavil</em> is to make unnecessary complaints about things that are unimportant.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>find fault unnecessarily</em>
</span>
</span>
</div>
<a class='quick-help' href='cavil#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='cavil#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/cavil/memory_hooks/5226.json'></span>
<p>
<span class="emp0"><span>W<span class="emp1"><span>il</span></span>l <span class="emp2"><span>C</span></span>r<span class="emp2"><span>av</span></span>e</span></span> When one <span class="emp2"><span>cav</span></span><span class="emp1"><span>il</span></span>s one w<span class="emp1"><span>il</span></span>l <span class="emp2"><span>c</span></span>r<span class="emp2"><span>av</span></span>e useless complaining.
</p>
</div>

<div id='memhook-button-bar'>
<a href="cavil#" id="add-public-hook" style="" url="https://membean.com/mywords/cavil/memory_hooks">Use other public hook</a>
<a href="cavil#" id="memhook-use-own" url="https://membean.com/mywords/cavil/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='cavil#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Naysayers often <b>cavil</b> that California’s economic growth depends almost entirely on Silicon Valley and the capital gains income of its wealthiest residents, but the state’s primacy in high technology encompasses fields outside Northern California, such as biotech.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
Some will <b>cavil</b> at the author’s conclusion that there was no justification either for the panic that preceded the war or for the euphoria that followed it.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
President Roosevelt might not have done all the things he promised to do and all the things he did do might not be for the country's good in the long run—but what he did do seemed so much better than the deeds of any other single citizen in the land that only the narrowest partisan could <b>cavil</b> at his popular selection as The Man of 1934.
<cite class='attribution'>
&mdash;
TIME Magazine
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/cavil/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='cavil#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>cavil</td>
<td>
&rarr;
</td>
<td class='meaning'>jeering, scoffing</td>
</tr>
</table>
<p>When one <em>cavils</em> about things, one &#8220;jeers and scoffs&#8221; by finding unnecessary fault in them.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='cavil#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Cavil" src="https://cdn2.membean.com/public/images/wordimages/cons2/cavil.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='cavil#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>aspersion</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>besmirch</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>cantankerous</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>captious</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>carp</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>censure</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>contentious</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>deprecate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>disparage</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>trifle</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>adulation</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bandy</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>banter</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>clemency</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>convivial</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>encomium</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>eulogy</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>panegyric</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="cavil" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>cavil</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="cavil#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

