
<!DOCTYPE html>
<html>
<head>
<title>Word: boisterous | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>You affront someone by openly and intentionally offending or insulting him.</p>
<p class='rw-defn idx1' style='display:none'>An audacious person acts with great daring and sometimes reckless bravery—despite risks and warnings from other people—in order to achieve something.</p>
<p class='rw-defn idx2' style='display:none'>Braggadocio is an excessively proud way of talking about your achievements or possessions that can annoy others.</p>
<p class='rw-defn idx3' style='display:none'>If you describe a person&#8217;s behavior as brazen, you mean that they are not embarrassed by anything they do—they simply don&#8217;t care what other people think about them.</p>
<p class='rw-defn idx4' style='display:none'>Bumptious people are annoying because they are too proud of their abilities or opinions—they are full of themselves.</p>
<p class='rw-defn idx5' style='display:none'>A chivalrous man behaves in a polite, kind, generous, and honorable way, especially towards women.</p>
<p class='rw-defn idx6' style='display:none'>If one&#8217;s powers or rights are circumscribed, they are limited or restricted.</p>
<p class='rw-defn idx7' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx8' style='display:none'>If you demur, you delay in doing or mildly object to something because you don&#8217;t really want to do it.</p>
<p class='rw-defn idx9' style='display:none'>If you describe someone, usually a young woman, as demure, you mean that she is quiet, shy, and always behaves modestly.</p>
<p class='rw-defn idx10' style='display:none'>Someone who is diffident is shy, does not want to draw notice to themselves, and is lacking in self-confidence.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx12' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx13' style='display:none'>If you show exuberance, you display great excitement, energy, and enthusiasm.</p>
<p class='rw-defn idx14' style='display:none'>If someone behaves in an impertinent way, they behave rudely and disrespectfully.</p>
<p class='rw-defn idx15' style='display:none'>Someone who is obstreperous is noisy, unruly, and difficult to control.</p>
<p class='rw-defn idx16' style='display:none'>To act in a pugnacious manner is to act in a combative and aggressive way.</p>
<p class='rw-defn idx17' style='display:none'>Rambunctious conduct is wild, unruly, very active, and sometimes hard to control.</p>
<p class='rw-defn idx18' style='display:none'>A raucous sound is unpleasantly loud, harsh, and noisy.</p>
<p class='rw-defn idx19' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx20' style='display:none'>Skittish persons or animals are made easily nervous or alarmed; they are likely to change behavior quickly and unpredictably.</p>
<p class='rw-defn idx21' style='display:none'>A stricture is a rule or condition that imposes restrictions or limits on what you can do.</p>
<p class='rw-defn idx22' style='display:none'>A strident person makes their feelings or opinions known in a forceful and strong way that often offends some people; not surprisingly, a strident voice is loud, harsh, and shrill.</p>
<p class='rw-defn idx23' style='display:none'>A tempestuous storm, temper, or crowd is violent, wild, and in an uproar.</p>
<p class='rw-defn idx24' style='display:none'>To be timorous is to be fearful.</p>
<p class='rw-defn idx25' style='display:none'>Someone is tremulous when they are shaking slightly from nervousness or fear; they may also simply be afraid of something.</p>
<p class='rw-defn idx26' style='display:none'>A tumultuous event or period of time is filled with great excitement, confusion, or violence; a tumultuous reaction to something is likewise very loud and noisy because people are happy and excited.</p>
<p class='rw-defn idx27' style='display:none'>Something that is vaunted, such as someone&#8217;s ability, is too highly praised and boasted about.</p>
<p class='rw-defn idx28' style='display:none'>A vibrant person is lively and full of energy in a way that is exciting and attractive.</p>
<p class='rw-defn idx29' style='display:none'>If someone is described as vivacious, they are lively and have a happy, lighthearted manner.</p>
<p class='rw-defn idx30' style='display:none'>Someone who is vociferous expresses their opinions loudly and strongly because they want their views to be heard.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>boisterous</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='boisterous#' id='pronounce-sound' path='audio/words/amy-boisterous'></a>
BOI-ster-uhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='boisterous#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='boisterous#' id='context-sound' path='audio/wordcontexts/brian-boisterous'></a>
The kindergarten classroom was very loud, filled as it was with <em>boisterous</em> and highly energetic children. The teacher attempted to get them to calm down and focus, but their <em>boisterous</em> and wild enthusiasm was unstoppable. At home, the children were far less <em>boisterous</em> than at school; there were fewer of them to get so noisy, so they were much easier to keep engaged in silent activities.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean to be <em>boisterous</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
To be rude and mean toward another person.
</li>
<li class='choice '>
<span class='result'></span>
To be childlike and hopeful about life.
</li>
<li class='choice answer '>
<span class='result'></span>
To be noisy and somewhat lacking in restraint.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='boisterous#' id='definition-sound' path='audio/wordmeanings/amy-boisterous'></a>
Someone who is <em>boisterous</em> is noisy, excitable, and full of boundless energy; therefore, they show a lack of disciplined restraint at times.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>noisy and wild</em>
</span>
</span>
</div>
<a class='quick-help' href='boisterous#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='boisterous#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/boisterous/memory_hooks/4809.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Boys</span></span> <span class="emp2"><span>Ter</span></span>rify <span class="emp3"><span>Us</span></span></span></span>  After the <span class="emp1"><span>bois</span></span><span class="emp2"><span>ter</span></span>o<span class="emp3"><span>us</span></span> <span class="emp1"><span>boys</span></span> <span class="emp2"><span>ter</span></span>rified <span class="emp3"><span>us</span></span> yet again, they smiled knowingly and quipped a <span class="emp1"><span>bois</span></span><span class="emp2"><span>ter</span></span>o<span class="emp3"><span>us</span></span> "<span class="emp1"><span>Boys</span></span> A<span class="emp2"><span>re</span></span> <span class="emp3"><span>Us</span></span>!"
</p>
</div>

<div id='memhook-button-bar'>
<a href="boisterous#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/boisterous/memory_hooks">Use other hook</a>
<a href="boisterous#" id="memhook-use-own" url="https://membean.com/mywords/boisterous/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='boisterous#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Opera singer Paul Lorieau got through only the first stanza of _O Canada_ before holding up his mike and letting the <b>boisterous</b> crowd belt out the rest.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
[Grace] Paley was the Jane Jacobs of fiction, attuned to chance urban encounters but also the pull of family life—<b>boisterous</b> children, bickering husbands and ex-husbands, an elderly father.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
Filling three sections [of the soccer stadium] next to the Barra Brava [fan group] is the equally passionate but less <b>boisterous</b> Screaming Eagles.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Cumming’s antic acting is the only asset of this <b>boisterous</b> comedy about Loki, the mischievous Norse god, looking for a mislaid magical mask, which a young cartoonist has now stumbled on.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/boisterous/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='boisterous#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>boist</td>
<td>
&rarr;
</td>
<td class='meaning'>rude, rough, loud, violent</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ous_full' data-tree-url='//cdn1.membean.com/public/data/treexml' href='boisterous#'>
<span class=''></span>
-ous
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>full of, having</td>
</tr>
</table>
<p>A <em>boisterous</em> person can be &#8220;full of rudeness, roughness, noise, and violence.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='boisterous#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>2010 World Cup</strong><span> This crowd of fans in Shibuya, Japan celebrates in a boisterous fashion.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/boisterous.jpg' video_url='examplevids/boisterous' video_width='350'></span>
<div id='wt-container'>
<img alt="Boisterous" height="288" src="https://cdn1.membean.com/video/examplevids/boisterous.jpg" width="350" />
<div class='center'>
<a href="boisterous#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='boisterous#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Boisterous" src="https://cdn2.membean.com/public/images/wordimages/cons2/boisterous.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='boisterous#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>affront</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>audacious</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>braggadocio</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>brazen</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>bumptious</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>exuberance</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>impertinent</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>obstreperous</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>pugnacious</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>rambunctious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>raucous</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>strident</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>tempestuous</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>tumultuous</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>vaunted</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>vibrant</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>vivacious</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>vociferous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='5' class = 'rw-wordform notlearned'><span>chivalrous</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>circumscribe</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>demur</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>demure</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>diffident</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>skittish</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>stricture</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>timorous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>tremulous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="boisterous" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>boisterous</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="boisterous#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

