
<!DOCTYPE html>
<html>
<head>
<title>Word: irrelevant | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Something that happens unexpectedly and by chance that is not inherent or natural to a given item or situation is adventitious, such as a root appearing in an unusual place on a plant.</p>
<p class='rw-defn idx1' style='display:none'>Something that is apposite is relevant or suitable to what is happening or being discussed.</p>
<p class='rw-defn idx2' style='display:none'>An appurtenance is a supporting feature, form of equipment, or item associated with a particular activity.</p>
<p class='rw-defn idx3' style='display:none'>An auxiliary device is one that acts in support of a main one, such as a backup generator that replaces power in case the main source of that power goes down.</p>
<p class='rw-defn idx4' style='display:none'>A cardinal rule or quality is considered to be the most important or basic in a set of rules or qualities.</p>
<p class='rw-defn idx5' style='display:none'>Chaff is the husks of grain separated from the edible plant; it can also be useless matter in general that is cast aside.</p>
<p class='rw-defn idx6' style='display:none'>A cohesive argument sticks together, working as a consistent, unified whole.</p>
<p class='rw-defn idx7' style='display:none'>The crux of a problem or argument is the most important or difficult part of it that affects everything else.</p>
<p class='rw-defn idx8' style='display:none'>A cynosure is an object that serves as the center of attention.</p>
<p class='rw-defn idx9' style='display:none'>Detritus is the small pieces of waste that remain after something has been destroyed or used.</p>
<p class='rw-defn idx10' style='display:none'>Something described as dross is of very poor quality and has little or no value.</p>
<p class='rw-defn idx11' style='display:none'>Something that is extraneous is not relevant or connected to something else, or it is not essential to a given situation.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx13' style='display:none'>An idea or remark is germane to a situation if it is connected to it in an important or fitting way.</p>
<p class='rw-defn idx14' style='display:none'>If something is gratuitous, it is freely given; nevertheless, it is usually unnecessary and can be harmful or upsetting.</p>
<p class='rw-defn idx15' style='display:none'>An immanent quality is present within something and is so much a part of it that the object cannot be imagined without that quality.</p>
<p class='rw-defn idx16' style='display:none'>An inalienable right is a privilege that cannot be taken away.</p>
<p class='rw-defn idx17' style='display:none'>An indispensable item is absolutely necessary or essential—it cannot be done without.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is ineffectual at a task is useless or ineffective when attempting to do it.</p>
<p class='rw-defn idx19' style='display:none'>An inherent characteristic is one that exists in a person at birth or in a thing naturally.</p>
<p class='rw-defn idx20' style='display:none'>An innate quality of someone is present since birth or is a quality of something that is essential to it.</p>
<p class='rw-defn idx21' style='display:none'>Something that is integral to something else is an essential or necessary part of it.</p>
<p class='rw-defn idx22' style='display:none'>An intrinsic characteristic of something is the basic and essential feature that makes it what it is.</p>
<p class='rw-defn idx23' style='display:none'>Something that is of paramount importance or significance is chief or supreme in those things.</p>
<p class='rw-defn idx24' style='display:none'>The periphery of a place is its boundary or outer edge.</p>
<p class='rw-defn idx25' style='display:none'>Something that is pivotal is more important than anything else in a situation or a system; consequently, it is the key to its success.</p>
<p class='rw-defn idx26' style='display:none'>Something predominant is the most important or the most common thing in a group.</p>
<p class='rw-defn idx27' style='display:none'>Something that is prolix, such as a lecture or speech, is excessively wordy; consequently, it can be tiresome to read or listen to.</p>
<p class='rw-defn idx28' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>
<p class='rw-defn idx29' style='display:none'>Something that is specious seems to be good, sound, or correct but is actually wrong or fake.</p>
<p class='rw-defn idx30' style='display:none'>A spurious statement is false because it is not based on sound thinking; therefore, it is not what it appears to be.</p>
<p class='rw-defn idx31' style='display:none'>A person or subject that is superficial is shallow, without depth, obvious, and concerned only with surface matters.</p>
<p class='rw-defn idx32' style='display:none'>Something that is superfluous is unnecessary; it is more than what is wanted or needed at the current time.</p>
<p class='rw-defn idx33' style='display:none'>If you have a surfeit of something, you have much more than what you need.</p>
<p class='rw-defn idx34' style='display:none'>A tangential point in an argument merely touches upon something that is not really relevant to the conversation at hand; rather, it is a minor or unimportant point.</p>
<p class='rw-defn idx35' style='display:none'>If you refer to something as a trifle, you mean that it is of little or no importance or value.</p>
<p class='rw-defn idx36' style='display:none'>Something trivial is of little value or is not important.</p>
<p class='rw-defn idx37' style='display:none'>Something uncanny is very strange, unnatural, or highly unusual.</p>
<p class='rw-defn idx38' style='display:none'>Something that is verbose, such as a speech or article, contains too many words.</p>
<p class='rw-defn idx39' style='display:none'>A watershed is a crucial event or turning point in either the history of a nation or the life of an individual that brings about a significant change.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>irrelevant</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='irrelevant#' id='pronounce-sound' path='audio/words/amy-irrelevant'></a>
ih-REL-uh-vuhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='irrelevant#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='irrelevant#' id='context-sound' path='audio/wordcontexts/brian-irrelevant'></a>
It sometimes amazes me how much <em>irrelevant</em> or unconnected information my English students will write into their papers.  They do give a thesis, but then proceed to sprinkle <em>irrelevant</em> or unrelated information into the body of the paper to prove the thesis!  They probably add this <em>irrelevant</em> or insignificant information to make their paper longer, but it just ends up giving them a poorer grade.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an <em>irrelevant</em> piece of evidence?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is so significant that it changes the course of an investigation.
</li>
<li class='choice answer '>
<span class='result'></span>
It does not connect in any way to a current investigation.
</li>
<li class='choice '>
<span class='result'></span>
It is the only piece of evidence that relates to a certain investigation.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='irrelevant#' id='definition-sound' path='audio/wordmeanings/amy-irrelevant'></a>
<em>Irrelevant</em> information is unrelated or unconnected to the situation at hand.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>unrelated</em>
</span>
</span>
</div>
<a class='quick-help' href='irrelevant#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='irrelevant#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/irrelevant/memory_hooks/6096.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Reliev</span></span>ed of <span class="emp1"><span>Ir</span></span>rit<span class="emp1"><span>ant</span></span></span></span>  I am <span class="emp3"><span>reliev</span></span>ed that I had no <span class="emp1"><span>ir</span></span><span class="emp3"><span>relev</span></span><span class="emp1"><span>ant</span></span> information in my paper about skin <span class="emp1"><span>ir</span></span>rit<span class="emp1"><span>ant</span></span>s.
</p>
</div>

<div id='memhook-button-bar'>
<a href="irrelevant#" id="add-public-hook" style="" url="https://membean.com/mywords/irrelevant/memory_hooks">Use other public hook</a>
<a href="irrelevant#" id="memhook-use-own" url="https://membean.com/mywords/irrelevant/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='irrelevant#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Communications without intelligence is noise; intelligence without communications is <b>irrelevant</b>.
<span class='attribution'>&mdash; Alfred M. Gray Jr., USMC General</span>
<img alt="Alfred m" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Alfred M. Gray Jr., USMC General.jpg?qdep8" width="80" />
</li>
<li>
Patients do not need to fast before the test is given, and it is far less likely to identify clinically <b>irrelevant</b> fluctuations in blood sugar because it measures average blood glucose levels over several months.
<cite class='attribution'>
&mdash;
WebMD
</cite>
</li>
<li>
"Attention is a two-sided coin," [Adam] Gazzaley [MD, PhD] tells WebMD. "It can be both focused on what's relevant as well as ignoring what's <b>irrelevant</b>. What we found is that in normal aging, focusing on what's relevant is just not enough. You also need to filter out information that's <b>irrelevant</b> or distracting," he continues.
<cite class='attribution'>
&mdash;
WebMD
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/irrelevant/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='irrelevant#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ir_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='irrelevant#'>
<span class=''></span>
ir-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='re_back' data-tree-url='//cdn1.membean.com/public/data/treexml' href='irrelevant#'>
<span class='common'></span>
re-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>back, again</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='lev_light' data-tree-url='//cdn1.membean.com/public/data/treexml' href='irrelevant#'>
<span class='common'></span>
lev
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>light, lessen, raise</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ant_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='irrelevant#'>
<span class=''></span>
-ant
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p>When something <em>irrelevant</em> is voiced, it assumes a &#8220;condition of not being raised again&#8221; since it has nothing to do with the subject at hand, hence does not need to be &#8220;raised again.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='irrelevant#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Duck Soup</strong><span> The witness is giving irrelevant testimony.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/irrelevant.jpg' video_url='examplevids/irrelevant' video_width='350'></span>
<div id='wt-container'>
<img alt="Irrelevant" height="288" src="https://cdn1.membean.com/video/examplevids/irrelevant.jpg" width="350" />
<div class='center'>
<a href="irrelevant#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='irrelevant#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Irrelevant" src="https://cdn0.membean.com/public/images/wordimages/cons2/irrelevant.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='irrelevant#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adventitious</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>chaff</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>detritus</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>dross</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>extraneous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>gratuitous</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>ineffectual</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>periphery</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>prolix</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>specious</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>spurious</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>superficial</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>superfluous</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>surfeit</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>tangential</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>trifle</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>trivial</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>verbose</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>apposite</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>appurtenance</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>auxiliary</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>cardinal</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>cohesive</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>crux</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>cynosure</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>germane</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>immanent</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>inalienable</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>indispensable</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>inherent</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>innate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>integral</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>intrinsic</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>paramount</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>pivotal</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>predominant</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>uncanny</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>watershed</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='irrelevant#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
relevant
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>connected or related to the situation at hand</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="irrelevant" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>irrelevant</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="irrelevant#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

