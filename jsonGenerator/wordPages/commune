
<!DOCTYPE html>
<html>
<head>
<title>Word: commune | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Augury is the process or art of reading omens about the future through specific ceremonies.</p>
<p class='rw-defn idx1' style='display:none'>A clairvoyant person believes that they can predict the future or communicate with the spirits of the dead.</p>
<p class='rw-defn idx2' style='display:none'>A colloquy is a formal conversation.</p>
<p class='rw-defn idx3' style='display:none'>A piece of writing is discursive if it includes a lot of information that is not relevant to the main subject.</p>
<p class='rw-defn idx4' style='display:none'>Empirical evidence or study is based on real experience or scientific experiments rather than on unproven theories.</p>
<p class='rw-defn idx5' style='display:none'>When something is implicit, it is understood without having to say it.</p>
<p class='rw-defn idx6' style='display:none'>The adjective ineffable refers to something that is so impressive and beautiful that you cannot describe it in words.</p>
<p class='rw-defn idx7' style='display:none'>When something is inert, it has no power of movement or is inactive.</p>
<p class='rw-defn idx8' style='display:none'>An interlocutor is the person with whom you are having a (usually formal) conversation or discussion.</p>
<p class='rw-defn idx9' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx10' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx11' style='display:none'>Someone is obtuse when they are slow to understand things; someone can choose to be obtuse when they deliberately do not try to figure things out.</p>
<p class='rw-defn idx12' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx13' style='display:none'>A portent is a warning that indicates what is likely to happen in the future, which is usually something unpleasant.</p>
<p class='rw-defn idx14' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx15' style='display:none'>Stupor is a state in which someone&#8217;s mind and senses are dulled; consequently, they are unable to think clearly or act normally, usually due to the use of alcohol or drugs.</p>
<p class='rw-defn idx16' style='display:none'>A tacit agreement between two people is understood without having to use words to express it.</p>
<p class='rw-defn idx17' style='display:none'>A taciturn person is quiet or silent by nature.</p>
<p class='rw-defn idx18' style='display:none'>If your body is affected by torpor, you are severely lacking in energy; therefore, you are idle—and can even be numb.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>commune</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='commune#' id='pronounce-sound' path='audio/words/amy-commune'></a>
kuh-MYOON
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='commune#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='commune#' id='context-sound' path='audio/wordcontexts/brian-commune'></a>
The highly perceptive Brynna seemed to have the ability to <em>commune</em> with nature, reaching its beauty at a deep level.  A sunset would thrill her soul beyond words, enabling her to <em>commune</em> or <em>communicate</em> intimately with the light.  Her receptiveness to and ability to <em>commune</em> with nature and its wonders gave her a deeper appreciation for her place in the world.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How can you <em>commune</em> with something?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
You live near it in order to fully explore it.
</li>
<li class='choice '>
<span class='result'></span>
You publicly show an appreciation for it.
</li>
<li class='choice answer '>
<span class='result'></span>
You connect to it without using words.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='commune#' id='definition-sound' path='audio/wordmeanings/amy-commune'></a>
If you <em>commune</em> with something, you <em>communicate</em> without using words because you feel especially close or in tune with it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>communicate</em>
</span>
</span>
</div>
<a class='quick-help' href='commune#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='commune#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/commune/memory_hooks/5603.json'></span>
<p>
<span class="emp0"><span>To <span class="emp3"><span>Commune</span></span> Is To <span class="emp3"><span>Commun</span></span>icat<span class="emp3"><span>e</span></span></span></span> When I <span class="emp3"><span>commune</span></span> with nature as I hike the Appalachian Trail, I feel an inner ability to <span class="emp3"><span>commun</span></span>icat<span class="emp3"><span>e</span></span> with the woods and mountains on a deep level that is beyond normal language.
</p>
</div>

<div id='memhook-button-bar'>
<a href="commune#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/commune/memory_hooks">Use other hook</a>
<a href="commune#" id="memhook-use-own" url="https://membean.com/mywords/commune/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='commune#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Ah! I need solitude. I have come forth to this hill at sunset to see the forms of the mountains in the horizon—to behold and <b>commune</b> with something grander than man.
<span class='attribution'>&mdash; Henry David Thoreau, American essayist, poet, and philosopher in his journal of August, 1854</span>
<img alt="Henry david thoreau, american essayist, poet, and philosopher in his journal of august, 1854" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Henry David Thoreau, American essayist, poet, and philosopher in his journal of August, 1854.jpg?qdep8" width="80" />
</li>
<li>
Using the methods she had developed as an animal-loving child in the English countryside, Goodall learned to <b>commune</b> with chimpanzees simply by sitting still and silently gaining their trust.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
The last time that Caity Weaver reported in person, she spent much of her time trying to <b>commune</b> with a horse. . . . Her instructor told her the horses would reproduce “what’s true inside you”—if only she would be receptive to the signals they were sending.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
It took Michelangelo four years to paint the ceiling of the Sistine Chapel, but most visitors spend less than 30 minutes gawking at it, often while pressed against umpteen strangers. . . . Now, for a mere $22,000, it is possible to <b>commune</b> with these Renaissance frescoes in the comfort of your own home.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/commune/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='commune#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='commun_common' data-tree-url='//cdn1.membean.com/public/data/treexml' href='commune#'>
<span class=''></span>
commun
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>common, public, general</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='commune#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>To <em>commune</em> with someone or something is to share a &#8220;common, public, or general&#8221; ability to communicate with them.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='commune#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Kung Fu & Tai Chi Center w/ Jake Mace</strong><span> Hugging a tree is a way to commune with nature.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/commune.jpg' video_url='examplevids/commune' video_width='350'></span>
<div id='wt-container'>
<img alt="Commune" height="198" src="https://cdn1.membean.com/video/examplevids/commune.jpg" width="350" />
<div class='center'>
<a href="commune#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='commune#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Commune" src="https://cdn3.membean.com/public/images/wordimages/cons2/commune.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='commune#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>augury</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>clairvoyant</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>colloquy</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>discursive</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>implicit</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>ineffable</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>interlocutor</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>portent</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>tacit</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='4' class = 'rw-wordform notlearned'><span>empirical</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>inert</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>obtuse</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>stupor</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>taciturn</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>torpor</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="commune" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>commune</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="commune#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

