
<!DOCTYPE html>
<html>
<head>
<title>Word: saturnine | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you have ardor for something, you have an intense feeling of love, excitement, and admiration for it.</p>
<p class='rw-defn idx1' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx2' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx3' style='display:none'>A convivial atmosphere or occasion is friendly, pleasant, cheerful, and relaxed.</p>
<p class='rw-defn idx4' style='display:none'>If you are despondent, you are extremely unhappy because you are in an unpleasant situation that you do not think will improve.</p>
<p class='rw-defn idx5' style='display:none'>A dirge is a slow and sad piece of music often performed at funerals.</p>
<p class='rw-defn idx6' style='display:none'>Something that is dolorous, such as music or news, causes mental pain and sorrow because it itself is full of grief and sorrow.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is dour is serious, stubborn, and unfriendly; they can also be gloomy.</p>
<p class='rw-defn idx8' style='display:none'>Weather that is dreary tends to be depressing and gloomy; a situation or person that is dreary tends to be boring or uninteresting.</p>
<p class='rw-defn idx9' style='display:none'>When something is droll, it is humorous in an odd way.</p>
<p class='rw-defn idx10' style='display:none'>When you are ecstatic about something, you are overjoyed or extremely happy about it.</p>
<p class='rw-defn idx11' style='display:none'>Something that is effulgent is very bright and radiates light.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is effusive expresses happiness, pleasure, admiration, praise, etc., in an extremely enthusiastic way.</p>
<p class='rw-defn idx13' style='display:none'>An elegy is a poem or other piece of writing expressing sadness; it is often about someone who has died.</p>
<p class='rw-defn idx14' style='display:none'>If something enthralls you, it makes you so interested and excited that you give it all your attention.</p>
<p class='rw-defn idx15' style='display:none'>A state of euphoria is one of extreme happiness or overwhelming joy.</p>
<p class='rw-defn idx16' style='display:none'>If you show exuberance, you display great excitement, energy, and enthusiasm.</p>
<p class='rw-defn idx17' style='display:none'>If you exult, you show great pleasure and excitement, especially about something you have achieved.</p>
<p class='rw-defn idx18' style='display:none'>A garrulous person talks a lot, especially about unimportant things.</p>
<p class='rw-defn idx19' style='display:none'>A gregarious person is friendly, highly social, and prefers being with people rather than being alone.</p>
<p class='rw-defn idx20' style='display:none'>Someone who is jocular is cheerful and often makes jokes or tries to make people laugh.</p>
<p class='rw-defn idx21' style='display:none'>Someone who is loquacious talks too much.</p>
<p class='rw-defn idx22' style='display:none'>If someone is lugubrious, they are looking very sad or gloomy.</p>
<p class='rw-defn idx23' style='display:none'>Someone who is morose is unhappy, bad-tempered, and unwilling to talk very much.</p>
<p class='rw-defn idx24' style='display:none'>If you are pensive, you are deeply thoughtful, often in a sad and/or serious way.</p>
<p class='rw-defn idx25' style='display:none'>A plaintive sound or voice expresses sadness.</p>
<p class='rw-defn idx26' style='display:none'>Something that is prolix, such as a lecture or speech, is excessively wordy; consequently, it can be tiresome to read or listen to.</p>
<p class='rw-defn idx27' style='display:none'>If you are sanguine about a situation, especially a difficult one, you are confident and cheerful that everything will work out the way you want it to.</p>
<p class='rw-defn idx28' style='display:none'>A taciturn person is quiet or silent by nature.</p>
<p class='rw-defn idx29' style='display:none'>Something that is verbose, such as a speech or article, contains too many words.</p>
<p class='rw-defn idx30' style='display:none'>If someone is described as vivacious, they are lively and have a happy, lighthearted manner.</p>
<p class='rw-defn idx31' style='display:none'>A voluble person can speak easily with speech that naturally flows; voluble people also tend to be quite chatty.</p>
<p class='rw-defn idx32' style='display:none'>Someone who is woebegone is very sad and filled with grief.</p>
<p class='rw-defn idx33' style='display:none'>Someone who is zealous spends a lot of time, energy, and effort to support something—  notably that of a political or religious nature—because they believe in it very strongly.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>saturnine</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='saturnine#' id='pronounce-sound' path='audio/words/amy-saturnine'></a>
SAT-er-nahyn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='saturnine#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='saturnine#' id='context-sound' path='audio/wordcontexts/brian-saturnine'></a>
The innkeeper was going to welcome the new visitor at the bar, but the fellow looked so <em>saturnine</em> or miserable that Rufus let him be.  Sitting alone by the fire, the <em>saturnine</em> or gloomy stranger stared into the flames and wrinkled his brow in black moodiness.  His face was so overshadowed by cheerlessness and so <em>saturnine</em> in its appearance that the other guests chose to leave rather than remain in his darkly sad presence.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When is someone <em>saturnine</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When they have a great deal of interest in astronomy.
</li>
<li class='choice answer '>
<span class='result'></span>
When they are suffering from melancholy.
</li>
<li class='choice '>
<span class='result'></span>
When they are hoping that something will happen.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='saturnine#' id='definition-sound' path='audio/wordmeanings/amy-saturnine'></a>
Someone who is <em>saturnine</em> is looking miserable and sad, sometimes in a threatening or unfriendly way.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>gloomy</em>
</span>
</span>
</div>
<a class='quick-help' href='saturnine#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='saturnine#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/saturnine/memory_hooks/5705.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Nine</span></span> <span class="emp1"><span>Satur</span></span>ns</span></span> Marge is looking so <span class="emp1"><span>satur</span></span><span class="emp2"><span>nine</span></span> that it is as if she has <span class="emp2"><span>nine</span></span> planet <span class="emp1"><span>Satur</span></span>ns weighing down upon her body and soul.
</p>
</div>

<div id='memhook-button-bar'>
<a href="saturnine#" id="add-public-hook" style="" url="https://membean.com/mywords/saturnine/memory_hooks">Use other public hook</a>
<a href="saturnine#" id="memhook-use-own" url="https://membean.com/mywords/saturnine/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='saturnine#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Somehow his words and his look did not seem to accord, or else it was that his cast of face made his smile look malignant and <b>saturnine</b>.
<span class='attribution'>&mdash; Bram Stoker, Irish-born British writer, from _Dracula_</span>
<img alt="Bram stoker, irish-born british writer, from _dracula_" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Bram Stoker, Irish-born British writer, from _Dracula_.jpg?qdep8" width="80" />
</li>
<li>
Purists may object, but most will find this love story irresistible, thanks to the sizzling chemistry between Keira Knightley’s sprightly Elizabeth Bennett and Matthew Macfadyen’s <b>saturnine</b> Mr. Darcy.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
The characters come with regularity, people with some interest in washing clothes but not necessarily very much interest. Richard Orange sat near the side door one Thursday afternoon. He wore a <b>saturnine</b> expression and ate a peanut butter and jelly sandwich, no soiled clothes in sight.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
They have worked together for 15 years, long enough to become foils: Shipley is sparky and associative, optimistically building theories, and Sevier is <b>saturnine</b> and skeptical, taking them apart.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/saturnine/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='saturnine#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>saturn</td>
<td>
&rarr;
</td>
<td class='meaning'>Saturn</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ine_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='saturnine#'>
<span class=''></span>
-ine
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>People born under the influence of the planet &#8220;Saturn&#8221; were once thought to be &#8220;gloomy.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='saturnine#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Saturnine" src="https://cdn0.membean.com/public/images/wordimages/cons2/saturnine.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='saturnine#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='4' class = 'rw-wordform notlearned'><span>despondent</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>dirge</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>dolorous</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>dour</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>dreary</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>elegy</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>lugubrious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>morose</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>pensive</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>plaintive</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>taciturn</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>woebegone</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>ardor</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>convivial</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>droll</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>ecstatic</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>effulgent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>effusive</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>enthrall</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>euphoria</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>exuberance</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>exult</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>garrulous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>gregarious</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>jocular</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>loquacious</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>prolix</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>sanguine</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>verbose</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>vivacious</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>voluble</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>zealous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="saturnine" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>saturnine</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="saturnine#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

