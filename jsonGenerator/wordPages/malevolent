
<!DOCTYPE html>
<html>
<head>
<title>Word: malevolent | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>A situation or condition that is abysmal is extremely bad or of wretched quality.</p>
<p class='rw-defn idx1' style='display:none'>If your behavior or manner is altruistic, you show you care more about other people and their interests than you care about yourself.</p>
<p class='rw-defn idx2' style='display:none'>If you have animus against someone, you have a strong feeling of dislike or hatred towards them, often without a good reason or based upon your personal prejudices.</p>
<p class='rw-defn idx3' style='display:none'>Antipathy is a strong feeling of dislike or hostility towards someone or something.</p>
<p class='rw-defn idx4' style='display:none'>An atrocious deed is outrageously bad, extremely evil, or shocking in its wrongness.</p>
<p class='rw-defn idx5' style='display:none'>Someone is baleful if they are filled with bad intent, anger, or hatred towards another person.</p>
<p class='rw-defn idx6' style='display:none'>When something bedevils you, it causes you a lot of problems and difficulties over a period of time.</p>
<p class='rw-defn idx7' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx8' style='display:none'>A chivalrous man behaves in a polite, kind, generous, and honorable way, especially towards women.</p>
<p class='rw-defn idx9' style='display:none'>If someone is granted clemency, they are punished less severely than they could have been—they have been shown mercy.</p>
<p class='rw-defn idx10' style='display:none'>Something that is deleterious has a harmful effect.</p>
<p class='rw-defn idx11' style='display:none'>Draconian rules and laws are extremely strict and harsh.</p>
<p class='rw-defn idx12' style='display:none'>If you refer to the enormity of a situation, problem, or event, you mean that it is very evil or morally offensive; this word has nothing to do with physical size.</p>
<p class='rw-defn idx13' style='display:none'>An infamous person has a very bad reputation because they have done disgraceful or dishonorable things.</p>
<p class='rw-defn idx14' style='display:none'>Iniquity is an immoral act, wickedness, or evil.</p>
<p class='rw-defn idx15' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx16' style='display:none'>An action is invidious when it is done with the intention of creating harm or producing envy or hatred in others.</p>
<p class='rw-defn idx17' style='display:none'>Malfeasance is an unlawful act, especially one committed by a trusted public official.</p>
<p class='rw-defn idx18' style='display:none'>Malice is a type of hatred that causes a strong desire to harm others both physically and emotionally.</p>
<p class='rw-defn idx19' style='display:none'>If you malign someone, you say unpleasant things about them to damage their reputation.</p>
<p class='rw-defn idx20' style='display:none'>A malignant thing or person, such as a tumor or criminal, is deadly or does great harm.</p>
<p class='rw-defn idx21' style='display:none'>A notorious person is well-known by the public at large; they are usually famous for doing something bad.</p>
<p class='rw-defn idx22' style='display:none'>Something that is pernicious is very harmful or evil, often in a way that is hidden or not quickly noticed.</p>
<p class='rw-defn idx23' style='display:none'>Philanthropy is unselfish support in the form of donating money, work, or gifts to positive social purposes; philanthropy is also overall love for humans in general.</p>
<p class='rw-defn idx24' style='display:none'>Something that is propitious shows favorable conditions.</p>
<p class='rw-defn idx25' style='display:none'>A salubrious place or area is pleasant, clean, healthy, and comfortable to live in.</p>
<p class='rw-defn idx26' style='display:none'>A salutary experience is beneficial to you since it strengthens you in some way, although it may be unpleasant as you undergo it; this word also refers to promoting good health.</p>
<p class='rw-defn idx27' style='display:none'>The sanctity of something is its holiness or sacred quality.</p>
<p class='rw-defn idx28' style='display:none'>Something sinister makes you feel that an evil, bad, or harmful thing is about to happen.</p>
<p class='rw-defn idx29' style='display:none'>A vicious person is corrupt, violent, aggressive, and/or highly immoral.</p>
<p class='rw-defn idx30' style='display:none'>Something vile is evil, very unpleasant, horrible, or extremely bad.</p>
<p class='rw-defn idx31' style='display:none'>A virulent disease is very dangerous and spreads very quickly.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>malevolent</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='malevolent#' id='pronounce-sound' path='audio/words/amy-malevolent'></a>
muh-LEV-uh-luhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='malevolent#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='malevolent#' id='context-sound' path='audio/wordcontexts/brian-malevolent'></a>
It is interesting that in science fiction aliens are often portrayed as evil, destructive, and <em>malevolent</em> beings who prey on human victims.  Does this view exist because so many fear outsiders and therefore perceive aliens as invaders who act in <em>malevolent</em> ways, bent on injuring us?  Or is it fun to imagine harmful or <em>malevolent</em> forces in the universe, against which humans prevail after victorious but intense struggle?
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How would a <em>malevolent</em> person behave?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They would avoid any task that requires a lot of effort.
</li>
<li class='choice '>
<span class='result'></span>
They would do all they could to help a friend.
</li>
<li class='choice answer '>
<span class='result'></span>
They would deliberately try to hurt other people.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='malevolent#' id='definition-sound' path='audio/wordmeanings/amy-malevolent'></a>
A <em>malevolent</em> person or thing is evil due to deliberate attempts to cause harm.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>evil</em>
</span>
</span>
</div>
<a class='quick-help' href='malevolent#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='malevolent#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/malevolent/memory_hooks/3692.json'></span>
<p>
<img alt="Malevolent" src="https://cdn3.membean.com/public/images/wordimages/hook/malevolent.jpg?qdep8" />
<span class="emp0"><span><span class="emp1"><span>V</span></span>i<span class="emp1"><span>olent</span></span> <span class="emp2"><span>Male</span></span></span></span> The <span class="emp2"><span>male</span></span><span class="emp1"><span>volent</span></span> man is a highly <span class="emp1"><span>v</span></span>i<span class="emp1"><span>olent</span></span> <span class="emp2"><span>male</span></span> who tried to kill us all yesterday in the elevator, but we overpowered him.
</p>
</div>

<div id='memhook-button-bar'>
<a href="malevolent#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/malevolent/memory_hooks">Use other hook</a>
<a href="malevolent#" id="memhook-use-own" url="https://membean.com/mywords/malevolent/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='malevolent#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Americans are benevolently ignorant about Canada, while Canadians are <b>malevolently</b> well informed about the United States.
<span class='attribution'>&mdash; J. Bartlett Brebner</span>
<img alt="J" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/J. Bartlett Brebner.jpg?qdep8" width="80" />
</li>
<li>
The military industrial complex has become a hidden and <b>malevolent</b> force whose real purpose seems to be an ever escalating conglomeration of mysterious test facilities and concealed agendas.
<cite class='attribution'>
&mdash;
Newsvine
</cite>
</li>
<li>
The report itself is a simply amazing document, reflecting a relentless pattern of <b>malevolent</b> intervention designed to frustrate any hopes of peace, to stimulate violence and warfare.
<cite class='attribution'>
&mdash;
Harper's Magazine
</cite>
</li>
<li>
Advised by the good wizard Gandalf, he sets off to rid Middle-earth of the ring by throwing it into the fires of Mordor, the <b>malevolent</b> land where it was made.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/malevolent/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='malevolent#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='mal_bad' data-tree-url='//cdn1.membean.com/public/data/treexml' href='malevolent#'>
<span class='common'></span>
mal
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>bad, evil</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='vol_wish' data-tree-url='//cdn1.membean.com/public/data/treexml' href='malevolent#'>
<span class=''></span>
vol
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>wish, want</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ent_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='malevolent#'>
<span class=''></span>
-ent
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p>A <em>malevolent</em> person is in a &#8220;state or condition of wishing evil&#8221; upon others.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='malevolent#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Sleeping Beauty</strong><span> The malevolent Maleficent gives Princess Aurora,the future Sleeping Beauty, a present as well.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/malevolent.jpg' video_url='examplevids/malevolent' video_width='350'></span>
<div id='wt-container'>
<img alt="Malevolent" height="288" src="https://cdn1.membean.com/video/examplevids/malevolent.jpg" width="350" />
<div class='center'>
<a href="malevolent#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='malevolent#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Malevolent" src="https://cdn3.membean.com/public/images/wordimages/cons2/malevolent.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='malevolent#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abysmal</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>animus</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>antipathy</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>atrocious</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>baleful</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>bedevil</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>deleterious</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>draconian</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>enormity</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>infamous</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>iniquity</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>invidious</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>malfeasance</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>malice</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>malign</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>malignant</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>notorious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>pernicious</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>sinister</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>vicious</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>vile</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>virulent</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>altruistic</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>chivalrous</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>clemency</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>philanthropy</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>propitious</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>salubrious</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>salutary</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>sanctity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="malevolent" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>malevolent</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="malevolent#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

