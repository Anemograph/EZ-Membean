
<!DOCTYPE html>
<html>
<head>
<title>Word: elocution | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you describe someone as articulate, you mean that they are able to express their thoughts, arguments, and ideas clearly and effectively.</p>
<p class='rw-defn idx1' style='display:none'>A clarion call is a stirring, emotional, and strong appeal to people to take action on something.</p>
<p class='rw-defn idx2' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx3' style='display:none'>A deft movement or action is made quickly and with skill.</p>
<p class='rw-defn idx4' style='display:none'>When you speak in an eloquent fashion, you speak beautifully in an expressive way that is convincing to an audience.</p>
<p class='rw-defn idx5' style='display:none'>When you enunciate, you speak or state something clearly so that you can be easily understood.</p>
<p class='rw-defn idx6' style='display:none'>When someone gesticulates, they make movements with their hands and arms when talking, usually because they want to emphasize something or are having difficulty in expressing an idea using words alone.</p>
<p class='rw-defn idx7' style='display:none'>Grandiloquent speech is highly formal, exaggerated, and often seems both silly and hollow because it is expressly used to appear impressive and important.</p>
<p class='rw-defn idx8' style='display:none'>Someone who is speaking in an incoherent fashion cannot be understood or is very hard to understand.</p>
<p class='rw-defn idx9' style='display:none'>A person who is being laconic uses very few words to say something.</p>
<p class='rw-defn idx10' style='display:none'>Someone who is loquacious talks too much.</p>
<p class='rw-defn idx11' style='display:none'>If someone is lucid, they are able to understand things and think clearly; this adjective also describes writing or speech that is crystal clear and easy to understand.</p>
<p class='rw-defn idx12' style='display:none'>A pithy statement or piece of writing is brief but intelligent; it is also precise and to the point.</p>
<p class='rw-defn idx13' style='display:none'>Something that is prolix, such as a lecture or speech, is excessively wordy; consequently, it can be tiresome to read or listen to.</p>
<p class='rw-defn idx14' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>
<p class='rw-defn idx15' style='display:none'>To ramble is to wander about leisurely, with no specific destination in mind; to ramble while speaking is to talk with no particular aim or point intended.</p>
<p class='rw-defn idx16' style='display:none'>To rant is to go on an angry verbal attack.</p>
<p class='rw-defn idx17' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx18' style='display:none'>Rhetoric is the skill or art of using language to persuade or influence people, especially language that sounds impressive but may not be sincere or honest.</p>
<p class='rw-defn idx19' style='display:none'>A stentorian voice is extremely loud and strong.</p>
<p class='rw-defn idx20' style='display:none'>Something that is succinct is clearly and briefly explained without using any unnecessary words.</p>
<p class='rw-defn idx21' style='display:none'>A taciturn person is quiet or silent by nature.</p>
<p class='rw-defn idx22' style='display:none'>To be terse in speech is to be short and to the point, often in an abrupt manner that may seem unfriendly.</p>
<p class='rw-defn idx23' style='display:none'>A truculent person is bad-tempered, easily annoyed, and prone to arguing excessively.</p>
<p class='rw-defn idx24' style='display:none'>If someone is unlettered, they are illiterate, unable to read and write.</p>
<p class='rw-defn idx25' style='display:none'>Something that is verbose, such as a speech or article, contains too many words.</p>
<p class='rw-defn idx26' style='display:none'>A voluble person can speak easily with speech that naturally flows; voluble people also tend to be quite chatty.</p>
<p class='rw-defn idx27' style='display:none'>A yokel is uneducated, naive, and unsophisticated; they do not know much about modern life or ideas because they sequester themselves in a rural setting.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>elocution</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='elocution#' id='pronounce-sound' path='audio/words/amy-elocution'></a>
el-uh-KYOO-shuhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='elocution#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='elocution#' id='context-sound' path='audio/wordcontexts/brian-elocution'></a>
The brilliant politician was famous for his clear and forceful <em>elocution</em> while giving speeches.  His loud and clear voice, thoughtful presentation, and occasional use of hand gestures to emphasize key points contributed to his excellent <em>elocution</em>.  His persuasive <em>elocution</em> or public speaking was delivered so well that he was able to get people to see his point of view.  The stirring strength of his <em>elocution</em> and verbal expression captured the voters&#8217; attention and swept him into office.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When would being skilled in <em>elocution</em> be helpful?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When you are trying to learn a new language.
</li>
<li class='choice '>
<span class='result'></span>
When you want to create a meal without a recipe.
</li>
<li class='choice answer '>
<span class='result'></span>
When you need to give a series of public lectures.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='elocution#' id='definition-sound' path='audio/wordmeanings/amy-elocution'></a>
Someone&#8217;s <em>elocution</em> is their artistic manner of speaking in public, including both the delivery of their voice and gestures.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>public speaking</em>
</span>
</span>
</div>
<a class='quick-help' href='elocution#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='elocution#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/elocution/memory_hooks/5365.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Lotion</span></span> for <span class="emp1"><span>Cu</span></span>tting</span></span> Her e<span class="emp3"><span>lo</span></span><span class="emp1"><span>cu</span></span><span class="emp3"><span>tion</span></span> was so sharp in its delivery that people had to put <span class="emp3"><span>lotion</span></span> on their <span class="emp1"><span>cu</span></span>t ears after her speeches.
</p>
</div>

<div id='memhook-button-bar'>
<a href="elocution#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/elocution/memory_hooks">Use other hook</a>
<a href="elocution#" id="memhook-use-own" url="https://membean.com/mywords/elocution/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='elocution#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
In an era in which radio made the spoken word more important than ever, Olson had a reputation as a fiery, galvanizing orator who blended forceful <b>elocution</b> with a working-class northside accent tinged by the Norwegian of his parents.
<cite class='attribution'>
&mdash;
MinnPost
</cite>
</li>
<li>
Indeed, after speaking with her by phone just a few days before the show’s premiere, I can attest that Doherty’s natural accent is quite different from her distinct <b>elocution</b> as Princess Anne.
<cite class='attribution'>
&mdash;
Town & Country Magazine
</cite>
</li>
<li>
[Maxine] Powell made a career of buffing the rough edges for Motown's stable of stars and polishing them enough to perform before royalty. . . . [her] lessons included everything from <b>elocution—always</b> yes, never yeah—to posture.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Margaret Hamilton, known for her crisp voice and excellent <b>elocution</b>, was another great character actor with more than 122 credits from 1933 through 1982. She performed with Buster Keaton, Bud Abbott and Lou Costello and is mostly associated with her famous role as the Wicked Witch of the West opposite Judy Garland in “The Wizard of Oz.”
<cite class='attribution'>
&mdash;
Green Bay Press-Gazette
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/elocution/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='elocution#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_out' data-tree-url='//cdn1.membean.com/public/data/treexml' href='elocution#'>
<span class='common'></span>
e-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>out of, from</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='locut_having' data-tree-url='//cdn1.membean.com/public/data/treexml' href='elocution#'>
<span class=''></span>
locut
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>having spoken</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ion_act' data-tree-url='//cdn1.membean.com/public/data/treexml' href='elocution#'>
<span class=''></span>
-ion
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>act, state, or result of doing something</td>
</tr>
</table>
<p><em>Elocution</em> is the &#8220;act of having spoken out&#8221; in public.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='elocution#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Accent Softening</strong><span> Lessons in elocution.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/elocution.jpg' video_url='examplevids/elocution' video_width='350'></span>
<div id='wt-container'>
<img alt="Elocution" height="288" src="https://cdn1.membean.com/video/examplevids/elocution.jpg" width="350" />
<div class='center'>
<a href="elocution#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='elocution#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Elocution" src="https://cdn3.membean.com/public/images/wordimages/cons2/elocution.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='elocution#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>articulate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>clarion</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>deft</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>eloquent</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>enunciate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>gesticulate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>grandiloquent</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>loquacious</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>lucid</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>prolix</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>ramble</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>rant</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>rhetoric</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>stentorian</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>verbose</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>voluble</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='8' class = 'rw-wordform notlearned'><span>incoherent</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>laconic</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>pithy</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>succinct</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>taciturn</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>terse</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>truculent</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>unlettered</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>yokel</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='elocution#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
eloquent
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>expressive; being skillful with the spoken word</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="elocution" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>elocution</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="elocution#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

