
<!DOCTYPE html>
<html>
<head>
<title>Word: repercussion | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An aftermath describes the consequences or results that follow a particularly destructive event, such as a natural disaster or war.</p>
<p class='rw-defn idx1' style='display:none'>An antecedent of something, such as an event or organization, has happened or existed before it and can be similar to it.</p>
<p class='rw-defn idx2' style='display:none'>Something is concomitant when it happens at the same time as something else and is connected with it in some fashion.</p>
<p class='rw-defn idx3' style='display:none'>A denouement is the end of a book, play, or series of events when everything is explained and comes to a conclusion.</p>
<p class='rw-defn idx4' style='display:none'>A derivative is something borrowed from something else, such as an English word that comes from another language.</p>
<p class='rw-defn idx5' style='display:none'>When you embark upon a journey, you begin it, such as by boarding a plane or starting to learn something new.</p>
<p class='rw-defn idx6' style='display:none'>If something, such as an idea or plan, comes to fruition, it produces the result you wanted to achieve from it.</p>
<p class='rw-defn idx7' style='display:none'>Gestation is the process by which a new idea,  plan, or piece of work develops in your mind.</p>
<p class='rw-defn idx8' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx9' style='display:none'>An inaugural event celebrates the beginning of something, such as the term of a president or the beginning of a series of meetings.</p>
<p class='rw-defn idx10' style='display:none'>An inception of something is its beginning or start.</p>
<p class='rw-defn idx11' style='display:none'>If an idea, plan, or attitude is inchoate, it is vague and imperfectly formed because it is just starting to develop.</p>
<p class='rw-defn idx12' style='display:none'>An incipient situation or quality is just beginning to appear or develop.</p>
<p class='rw-defn idx13' style='display:none'>When something is inert, it has no power of movement or is inactive.</p>
<p class='rw-defn idx14' style='display:none'>An innovation is something new that is created or is a novel process for doing something.</p>
<p class='rw-defn idx15' style='display:none'>Something that is nascent is just starting to develop and is expected to become stronger and bigger in time.</p>
<p class='rw-defn idx16' style='display:none'>A first event is a precursor to a second event if the first event is responsible for the development or existence of the second.</p>
<p class='rw-defn idx17' style='display:none'>The provenance of something is its birthplace or the place from which it originally came.</p>
<p class='rw-defn idx18' style='display:none'>A ramification from an action is a result or consequence of it—and is often unanticipated.</p>
<p class='rw-defn idx19' style='display:none'>If you reciprocate, you give something to someone because they have given something similar to you.</p>
<p class='rw-defn idx20' style='display:none'>If an action or situation redounds to your credit or discredit, it gives people a good or poor opinion of you and produces a result that benefits or hurts you.</p>
<p class='rw-defn idx21' style='display:none'>When a sound reverberates, it echoes or rebounds continuously from one place to another.</p>
<p class='rw-defn idx22' style='display:none'>A seminal article, book, piece of music or other work has many new ideas; additionally, it has great influence on generating ideas for future work.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>repercussion</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='repercussion#' id='pronounce-sound' path='audio/words/amy-repercussion'></a>
ree-per-KUHSH-uhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='repercussion#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='repercussion#' id='context-sound' path='audio/wordcontexts/brian-repercussion'></a>
My friends did not realize what the <em>repercussions</em> or outcomes of their move across country would truly be.  Margaret decided that she wanted to relocate because of an increased salary at a new job, but she did not realize that one <em>repercussion</em> or result of that move would be her children having to attend a poor school.  Greg also did not realize that yet another <em>repercussion</em> or effect would be isolation in the country, where there was a lack of a good Internet connection and roads that would often flood.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>repercussion</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Choosing to spend all your money on new clothes.
</li>
<li class='choice '>
<span class='result'></span>
Having a headache when you need to take a big test.
</li>
<li class='choice answer '>
<span class='result'></span>
Being tired after staying up late the previous night.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='repercussion#' id='definition-sound' path='audio/wordmeanings/amy-repercussion'></a>
A <em>repercussion</em> of an act is the result or effect of it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>result</em>
</span>
</span>
</div>
<a class='quick-help' href='repercussion#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='repercussion#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/repercussion/memory_hooks/5909.json'></span>
<p>
<span class="emp0"><span>Grim <span class="emp1"><span>Reaper</span></span> <span class="emp3"><span>Cussin</span></span>g</span></span> Little did I know that eating that wild mushroom would have the <span class="emp1"><span>reper</span></span><span class="emp3"><span>cussion</span></span> of the Grim <span class="emp1"><span>Reaper</span></span> <span class="emp3"><span>cussin</span></span>g at me!
</p>
</div>

<div id='memhook-button-bar'>
<a href="repercussion#" id="add-public-hook" style="" url="https://membean.com/mywords/repercussion/memory_hooks">Use other public hook</a>
<a href="repercussion#" id="memhook-use-own" url="https://membean.com/mywords/repercussion/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='repercussion#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Journalists are not in the democracy racket. They’re not in the game of empowering the populace. They are not social engineers. They need not think out the first, second, and third possible <b>repercussion</b>[s] of most stories they write—the impact of disclosing exit-poll numbers being one of them—when they put fingers to keyboard.
<cite class='attribution'>
&mdash;
Slate
</cite>
</li>
<li>
Homework also wasn’t factored into a final grade. . . . And while some kids “kind of abused” the homework policy, [Ethan] Dobbs [a student] said most students got the idea that if they didn’t do the work, they probably weren’t going to know what they were doing on the test. Teachers could also give poor “citizenship” grades, separate from academic marks. “There’s always a <b>repercussion</b> in some way,” said Dobbs, now a junior.
<cite class='attribution'>
&mdash;
St. Louis Post-Dispatch
</cite>
</li>
<li>
The researchers came upon these findings by analyzing the number of men taking paternity leave in Norway before and after a 1993 law that granted paid leave. Immediately after the reform, the percentage of fathers taking leave shot up from three percent to [thirty-five] percent. With this in mind, the researchers could isolate social influence as a reason for further leave-taking: what changed before and after the law wasn't a personal preference shared by family or the encouragement of a particular company, but was rather that those dads were more likely to have seen a peer take time off without <b>repercussion</b>.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/repercussion/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='repercussion#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='re_back' data-tree-url='//cdn1.membean.com/public/data/treexml' href='repercussion#'>
<span class='common'></span>
re-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>back, again</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='per_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='repercussion#'>
<span class='common'></span>
per-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='cuss_strike' data-tree-url='//cdn1.membean.com/public/data/treexml' href='repercussion#'>
<span class=''></span>
cuss
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>strike, beat upon</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ion_act' data-tree-url='//cdn1.membean.com/public/data/treexml' href='repercussion#'>
<span class=''></span>
-ion
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>act, state, or result of doing something</td>
</tr>
</table>
<p>A <em>repercussion</em> is the &#8220;state of thoroughly striking back.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='repercussion#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Fringe</strong><span> There will be repercussions for his actions.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/repercussion.jpg' video_url='examplevids/repercussion' video_width='350'></span>
<div id='wt-container'>
<img alt="Repercussion" height="288" src="https://cdn1.membean.com/video/examplevids/repercussion.jpg" width="350" />
<div class='center'>
<a href="repercussion#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='repercussion#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Repercussion" src="https://cdn1.membean.com/public/images/wordimages/cons2/repercussion.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='repercussion#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>aftermath</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>concomitant</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>denouement</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>derivative</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>fruition</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>innovation</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>ramification</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>reciprocate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>redound</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>reverberate</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>antecedent</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>embark</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>gestation</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>inaugural</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>inception</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>inchoate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>incipient</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>inert</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>nascent</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>precursor</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>provenance</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>seminal</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="repercussion" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>repercussion</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="repercussion#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

