
<!DOCTYPE html>
<html>
<head>
<title>Word: fervid | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you are ambivalent about something, you are uncertain whether you like it or what you should do about it.</p>
<p class='rw-defn idx1' style='display:none'>When you have ardor for something, you have an intense feeling of love, excitement, and admiration for it.</p>
<p class='rw-defn idx2' style='display:none'>A disaffected member of a group or organization is not satisfied with it; consequently, they feel little loyalty towards it.</p>
<p class='rw-defn idx3' style='display:none'>Someone does something in a disinterested way when they have no personal involvement or attachment to the action.</p>
<p class='rw-defn idx4' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx5' style='display:none'>When people equivocate, they avoid making a clear statement; they are deliberately vague in order to avoid giving a direct answer to a question.</p>
<p class='rw-defn idx6' style='display:none'>If you show exuberance, you display great excitement, energy, and enthusiasm.</p>
<p class='rw-defn idx7' style='display:none'>When you are impassioned about a cause or idea, you are very passionate or highly emotionally charged about it.</p>
<p class='rw-defn idx8' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is irresolute is unable to decide what to do.</p>
<p class='rw-defn idx10' style='display:none'>If you are lethargic, you are tired, lack energy, and are unwilling to exert effort.</p>
<p class='rw-defn idx11' style='display:none'>When you are listless, you lack energy and interest and are unwilling to exert any effort.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx13' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx14' style='display:none'>Someone who is pertinacious is determined to continue doing something rather than giving up—even when it gets very difficult.</p>
<p class='rw-defn idx15' style='display:none'>If someone watches or listens to something with rapt attention, they are so involved with it that they do not notice anything else.</p>
<p class='rw-defn idx16' style='display:none'>If your body is affected by torpor, you are severely lacking in energy; therefore, you are idle—and can even be numb.</p>
<p class='rw-defn idx17' style='display:none'>When you have a vehement feeling about something, you feel very strongly or intensely about it.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is zealous spends a lot of time, energy, and effort to support something—  notably that of a political or religious nature—because they believe in it very strongly.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
Middle/High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>fervid</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='fervid#' id='pronounce-sound' path='audio/words/amy-fervid'></a>
FUR-vid
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='fervid#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='fervid#' id='context-sound' path='audio/wordcontexts/brian-fervid'></a>
Hattie was a <em>fervid</em>, passionate volunteer who felt that helping others was essential to life.  Her <em>fervid</em>, enthusiastic energy and attention made the elders at the hospital feel cared for and loved.  Hattie also volunteered at the elementary school, <em>fervidly</em> and eagerly supporting students in their work and boosting their confidence.  Hattie&#8217;s <em>fervid</em> and spirited dedication earned her the service award for the year.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is likely considered <em>fervid</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A piece of cheese that has sat out so long that it is moldy and smelly.
</li>
<li class='choice '>
<span class='result'></span>
A meeting between two people that is held in a top-secret location.
</li>
<li class='choice answer '>
<span class='result'></span>
A debate between two people who feel passionately about an issue.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='fervid#' id='definition-sound' path='audio/wordmeanings/amy-fervid'></a>
A <em>fervid</em> person has strong feelings about something, such as a humanitarian cause; therefore, they are very sincere and enthusiastic about it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>passionate</em>
</span>
</span>
</div>
<a class='quick-help' href='fervid#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='fervid#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/fervid/memory_hooks/5493.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Fer</span></span>ret <span class="emp3"><span>Vid</span></span>eo</span></span> "Have you seen my new <span class="emp1"><span>fer</span></span>ret <span class="emp3"><span>vid</span></span>eo?  It's the best!  It's all about my pet <span class="emp1"><span>fer</span></span>ret who is <em>so</em> funny and I'll bet you anything that it's going to win an Oscar.  It's the best <span class="emp1"><span>fer</span></span>ret <span class="emp3"><span>vid</span></span>eo ever made!"  We all couldn't believe that Bill was so <span class="emp1"><span>fer</span></span><span class="emp3"><span>vid</span></span> about a silly <span class="emp1"><span>fer</span></span>ret home <span class="emp3"><span>vid</span></span>eo.
</p>
</div>

<div id='memhook-button-bar'>
<a href="fervid#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/fervid/memory_hooks">Use other hook</a>
<a href="fervid#" id="memhook-use-own" url="https://membean.com/mywords/fervid/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='fervid#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
When it came to crowd shots at Shea and Yankee stadiums, Fox’s 22 cameras favored <b>fervid</b> fans with hands pressed together in supplication.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
They may gravitate instead to a model cast in the image of [Audrey] Withers, a woman driven less by self-regard and a thirst for fame than by a <b>fervid</b> sense of mission.
<cite class='attribution'>
&mdash;
New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/fervid/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='fervid#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ferv_boil' data-tree-url='//cdn1.membean.com/public/data/treexml' href='fervid#'>
<span class=''></span>
ferv
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>boil, be hot</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='id_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='fervid#'>
<span class=''></span>
-id
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>If someone is <em>fervid</em> about a belief he or she is &#8220;boiling over&#8221; with enthusiasm about it.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='fervid#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Billy on the Street</strong><span> This reporter is fervid about the actress Meryl Streep!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/fervid.jpg' video_url='examplevids/fervid' video_width='350'></span>
<div id='wt-container'>
<img alt="Fervid" height="288" src="https://cdn1.membean.com/video/examplevids/fervid.jpg" width="350" />
<div class='center'>
<a href="fervid#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='fervid#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Fervid" src="https://cdn3.membean.com/public/images/wordimages/cons2/fervid.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='fervid#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>ardor</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>exuberance</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>impassioned</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>pertinacious</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>rapt</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>vehement</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>zealous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>ambivalent</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>disaffected</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>disinterested</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>equivocate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>irresolute</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>lethargic</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>listless</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>torpor</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='fervid#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
fervent
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>showing great enthusiasm</td>
</tr>
<tr>
<td class='wordform'>
<span>
fervor
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>enthusiasm</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="fervid" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>fervid</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="fervid#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

