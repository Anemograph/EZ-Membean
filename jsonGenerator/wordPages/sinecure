
<!DOCTYPE html>
<html>
<head>
<title>Word: sinecure | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Sinecure-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/sinecure-large.jpg?qdep8" />
</div>
<a href="sinecure#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>Affluence is the state of having a lot of money and many possessions, granting one a high economic standard of living.</p>
<p class='rw-defn idx1' style='display:none'>If you describe a decision, rule, or plan as arbitrary, you think that it was decided without any thought, standard, or system to guide it; therefore, it can seem unfair.</p>
<p class='rw-defn idx2' style='display:none'>Something that is arduous is extremely difficult; hence, it involves a lot of effort.</p>
<p class='rw-defn idx3' style='display:none'>An autocratic person rules with complete power; consequently, they make decisions and give orders to people without asking them for their opinion or advice.</p>
<p class='rw-defn idx4' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx5' style='display:none'>Someone who is capricious changes their mind repeatedly or behaves in unexpected ways; a capricious series of events follows no predictable pattern.</p>
<p class='rw-defn idx6' style='display:none'>Consanguinity is the state of being related to someone else by blood or having a similar close relationship to them.</p>
<p class='rw-defn idx7' style='display:none'>When there is a dearth of something, there is a scarcity or lack of it.</p>
<p class='rw-defn idx8' style='display:none'>Someone who is impecunious has very little money, especially over a long period of time.</p>
<p class='rw-defn idx9' style='display:none'>An indigent person is very poor.</p>
<p class='rw-defn idx10' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx11' style='display:none'>If someone has a jaundiced view of something, they ignore the good parts and can only see the bad aspects of it.</p>
<p class='rw-defn idx12' style='display:none'>A laborious job or process takes a long time, requires a lot of effort, and is often boring.</p>
<p class='rw-defn idx13' style='display:none'>Nepotism is the unfair use of power to show special favor, such as giving jobs or benefits to family and friends.</p>
<p class='rw-defn idx14' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx15' style='display:none'>A task or responsibility is onerous if you dislike having to do it because it is difficult or tiring.</p>
<p class='rw-defn idx16' style='display:none'>Something that is opulent is very impressive, grand, and is made from expensive materials.</p>
<p class='rw-defn idx17' style='display:none'>If someone patronizes you, they talk or behave in a way that seems friendly; nevertheless, they also act as if they were more intelligent or important than you are.</p>
<p class='rw-defn idx18' style='display:none'>A paucity of something is not enough of it.</p>
<p class='rw-defn idx19' style='display:none'>Pecuniary means relating to or concerning money.</p>
<p class='rw-defn idx20' style='display:none'>Penury is the state of being extremely poor.</p>
<p class='rw-defn idx21' style='display:none'>A scrupulous person takes great care to do everything in an extremely honest, conscientious, and fair manner.</p>
<p class='rw-defn idx22' style='display:none'>A wanton action deliberately harms someone or damages something for no apparent or good reason.</p>
<p class='rw-defn idx23' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>sinecure</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='sinecure#' id='pronounce-sound' path='audio/words/amy-sinecure'></a>
SAHY-nuh-kyoor
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='sinecure#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='sinecure#' id='context-sound' path='audio/wordcontexts/brian-sinecure'></a>
My uncle offered me a true <em>sinecure</em> at his company: I would be paid a large salary, but would only have to show up at work but once per week.  I, of course, accepted this <em>sinecure</em> or easy job, realizing that my uncle was just doing me a favor because he liked me.  One of my friends, in fact, has a <em>sinecure</em> in which she doesn&#8217;t have to go to work at all, yet still receives a regular salary!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>sinecure</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A favor from a relative that saves someone from getting into trouble.
</li>
<li class='choice '>
<span class='result'></span>
A new job that seems great at first . . . but is revealed to be more and more onerous as time goes on.
</li>
<li class='choice answer '>
<span class='result'></span>
A sinecure is a job for which someone is paid well but is not required to do much work.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='sinecure#' id='definition-sound' path='audio/wordmeanings/amy-sinecure'></a>
A <em>sinecure</em> is a paid job or position that provides a regular income but does not involve much work or responsibility.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>easy job</em>
</span>
</span>
</div>
<a class='quick-help' href='sinecure#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='sinecure#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/sinecure/memory_hooks/6205.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Cure</span></span> for <span class="emp1"><span>Sin</span></span></span></span>  For most of my life I did absolutely nothing since I was so wealthy, and got into quite a bit of trouble; however, when my aunt offered me a <span class="emp1"><span>sin</span></span>e<span class="emp3"><span>cure</span></span> at her company, I accepted, and it proved to be a <span class="emp3"><span>cure</span></span> for my <span class="emp1"><span>sin</span></span>s since I actually found a purpose in my life for the first time, despite the fact that I wasn't really required to do much of anything.
</p>
</div>

<div id='memhook-button-bar'>
<a href="sinecure#" id="add-public-hook" style="" url="https://membean.com/mywords/sinecure/memory_hooks">Use other public hook</a>
<a href="sinecure#" id="memhook-use-own" url="https://membean.com/mywords/sinecure/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='sinecure#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
If, from the outside, the job has the air of a <b>sinecure—just</b> someone to hand out the medals at the end of the season, to smile and to shake hands—and an uncertain, short-lived one at that, he wanted to interpret it slightly differently.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
“Jolie appointed as professor” makes it seem like she has bypassed all of the academic requirements and been granted a cushy academic <b>sinecure</b> because of her non-academic attributes.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Due to a vacuum in leadership, the junior members of the civil services indulge in petty corruption or simply treat their jobs as <b>sinecure</b>[s].
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/sinecure/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='sinecure#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>sine</td>
<td>
&rarr;
</td>
<td class='meaning'>without</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='cur_worry' data-tree-url='//cdn1.membean.com/public/data/treexml' href='sinecure#'>
<span class=''></span>
cur
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>worry, anxiety, attention</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='sinecure#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>A <em>sinecure</em> is a job &#8220;without worry or anxiety&#8221; since one doesn&#8217;t have to do much to still get paid.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='sinecure#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Sinecure" src="https://cdn3.membean.com/public/images/wordimages/cons2/sinecure.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='sinecure#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>affluence</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>arbitrary</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>autocratic</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>capricious</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>consanguinity</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>jaundiced</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>nepotism</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>opulent</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>patronize</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>pecuniary</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>wanton</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>arduous</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>dearth</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>impecunious</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>indigent</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>laborious</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>onerous</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>paucity</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>penury</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>scrupulous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="sinecure" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>sinecure</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="sinecure#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

