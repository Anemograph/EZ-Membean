
<!DOCTYPE html>
<html>
<head>
<title>Word: connote | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An allegorical poem or story employs allegory, that is, a literary device that uses literal events and characters to represent abstract ideas or deeper meanings.</p>
<p class='rw-defn idx1' style='display:none'>When you allude to something or someone, often events or characters from literature or history, you refer to them in an indirect way.</p>
<p class='rw-defn idx2' style='display:none'>When you claim that something is banal, you do not like it because you think it is ordinary, dull, commonplace, and boring.</p>
<p class='rw-defn idx3' style='display:none'>Empirical evidence or study is based on real experience or scientific experiments rather than on unproven theories.</p>
<p class='rw-defn idx4' style='display:none'>Something esoteric is known about or understood by very few people.</p>
<p class='rw-defn idx5' style='display:none'>If you evince particular feelings, qualities, or attitudes, you show them, often clearly.</p>
<p class='rw-defn idx6' style='display:none'>If you use words in a figurative way, they have an abstract or symbolic meaning beyond their literal interpretation.</p>
<p class='rw-defn idx7' style='display:none'>Hackneyed words, images, ideas or sayings have been used so often that they no longer seem interesting or amusing.</p>
<p class='rw-defn idx8' style='display:none'>The word homogeneous is used to describe a group that has members or parts which are similar or are all of the same type.</p>
<p class='rw-defn idx9' style='display:none'>If an idea or thought is incisive, it is expressed in a penetrating and knowledgeable manner that is clear and brief; additionally, it can demonstrate impressive understanding of related ideas or thoughts.</p>
<p class='rw-defn idx10' style='display:none'>If someone makes an insinuation, they say something bad or unpleasant in a sly and indirect way.</p>
<p class='rw-defn idx11' style='display:none'>Something insipid is dull, boring, and has no interesting features; for example, insipid food has no taste or little flavor.</p>
<p class='rw-defn idx12' style='display:none'>If you describe ideas as jejune, you are saying they are childish and uninteresting; the adjective jejune also describes those having little experience, maturity, or knowledge.</p>
<p class='rw-defn idx13' style='display:none'>Something mediocre is average or ordinary in quality; it’s just OK.</p>
<p class='rw-defn idx14' style='display:none'>A metaphor is a word or phrase that means one thing but is used to describe something else in order to highlight similar qualities; for example, if someone is very brave, you might say that they have the heart of a lion.</p>
<p class='rw-defn idx15' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx16' style='display:none'>Something is nondescript when its appearance is ordinary, dull, and not at all interesting or attractive.</p>
<p class='rw-defn idx17' style='display:none'>A nuance is a small difference in something that may be difficult to notice but is fairly important.</p>
<p class='rw-defn idx18' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx19' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx20' style='display:none'>A platitude is an unoriginal statement that has been used so many times that it is considered almost meaningless and pointless, even though it is presented as important.</p>
<p class='rw-defn idx21' style='display:none'>When someone exhibits profundity, they display great intellectual depth and understanding; profundity can also be the depth or complexity of something.</p>
<p class='rw-defn idx22' style='display:none'>Something prosaic is dull, boring, and ordinary.</p>
<p class='rw-defn idx23' style='display:none'>Recondite areas of knowledge are those that are very difficult to understand and/or are not known by many people.</p>
<p class='rw-defn idx24' style='display:none'>Something that is sinuous is shaped or moves like a snake, having many smooth twists and turns that can often be highly graceful.</p>
<p class='rw-defn idx25' style='display:none'>If you refer to something as a trifle, you mean that it is of little or no importance or value.</p>
<p class='rw-defn idx26' style='display:none'>A trite remark or idea is so overused that it is no longer interesting or novel.</p>
<p class='rw-defn idx27' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx28' style='display:none'>Something vapid is dull, boring, and/or tiresome.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>connote</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='connote#' id='pronounce-sound' path='audio/words/amy-connote'></a>
kuh-NOHT
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='connote#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='connote#' id='context-sound' path='audio/wordcontexts/brian-connote'></a>
More and more products are being designated &#8220;green&#8221; or eco-friendly nowadays because these terms give the impression or <em>connote</em> user safety and environmental responsibility. Customers are buying more green products&#8212;such as electric cars and recycled goods&#8212;in response to what these items suggest or <em>connote</em>, including a positive moral position and concern for the future of the planet. Likewise, investors are putting their money into renewable energies because they <em>connote</em> or are associated with promoting a healthier future&#8212;they feel good about their money having a beneficial impact on the world today . . . and tomorrow.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How can a writer <em>connote</em> something?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
By using a word that suggests something beyond what is stated.
</li>
<li class='choice '>
<span class='result'></span>
By providing additional background information in an author&#8217;s note.
</li>
<li class='choice '>
<span class='result'></span>
By referring to musical instruments as a key component in a word.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='connote#' id='definition-sound' path='audio/wordmeanings/amy-connote'></a>
If a word or behavior <em>connotes</em> something, it suggests an additional idea or emotion that is not part of its original literal meaning.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>imply</em>
</span>
</span>
</div>
<a class='quick-help' href='connote#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='connote#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/connote/memory_hooks/3104.json'></span>
<p>
<span class="emp0"><span>Con<span class="emp1"><span>notation</span></span> <span class="emp1"><span>Notation</span></span></span></span> When out on a date, don't just take the literal meaning of what your date says, but also make mental <span class="emp1"><span>not</span></span>es or <span class="emp1"><span>notation</span></span>s of what he or she has suggested between the lines ... for the most important communicative con<span class="emp1"><span>notation</span></span>s lie there.
</p>
</div>

<div id='memhook-button-bar'>
<a href="connote#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/connote/memory_hooks">Use other hook</a>
<a href="connote#" id="memhook-use-own" url="https://membean.com/mywords/connote/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='connote#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
"Washington” has become for many a dirty word that <b>connotes</b> self-serving politicians and devious lobbyists. To be sure, they are there, but I remember . . . being struck by how populated the government was with young people from every corner of the nation, there to do the right thing and serve their country.
<span class='attribution'>&mdash; Dan Rather, American journalist and former national evening news anchor, from _What Unites Us: Reflections on Patriotism_</span>
<img alt="Dan rather, american journalist and former national evening news anchor, from _what unites us: reflections on patriotism_" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Dan Rather, American journalist and former national evening news anchor, from _What Unites Us: Reflections on Patriotism_.jpg?qdep8" width="80" />
</li>
<li>
A large Charlotte-area active-senior community has changed its name from one many of its residents say that <b>connotes</b> racism and slavery. Plantation Estates residents and staff “voted overwhelmingly” to change the name to Matthews Glen, parent company Acts Retirement-Life Communities said Tuesday.
<cite class='attribution'>
&mdash;
The Charlotte Observer
</cite>
</li>
<li>
This month, [Louis Vuitton] introduces its first hip-hop inspired sunglasses collection. . . . The bold, curvy shapes and rich colors, inspired by the gangster movie _Scarface_, honor the Parisian heritage of luxurious detail; the jeweled frames <b>connote</b> power and ambition.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
Recognised around the world in either its blue-white and red-white varieties, this once-nameless bag in West Africa has long been especially popular in markets across the region.. . . . "The bags <b>connote</b> migration not as a word to describe travel but as an actual living thing—with each border crossed—[a migrant] takes something new and leaves something behind," Obioma said.
<cite class='attribution'>
&mdash;
BBC News
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/connote/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='connote#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='con_with' data-tree-url='//cdn1.membean.com/public/data/treexml' href='connote#'>
<span class='common'></span>
con-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>with, together</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='not_mark' data-tree-url='//cdn1.membean.com/public/data/treexml' href='connote#'>
<span class=''></span>
not
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>mark, sign</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='connote#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>When a word <em>connotes</em> something, it implies additional signs that go with its literal surface meaning; for instance, roses are red flowers (main meaning) that <em>connote</em> love and romance, marks or signs that are part and parcel with roses.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='connote#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>One Day</strong><span> "Sacking" connotes something far worse than "a direction away from [him]".</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/connote.jpg' video_url='examplevids/connote' video_width='350'></span>
<div id='wt-container'>
<img alt="Connote" height="288" src="https://cdn1.membean.com/video/examplevids/connote.jpg" width="350" />
<div class='center'>
<a href="connote#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='connote#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Connote" src="https://cdn1.membean.com/public/images/wordimages/cons2/connote.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='connote#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>allegorical</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>allude</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>esoteric</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>evince</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>figurative</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>incisive</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>insinuation</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>metaphor</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>nuance</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>profundity</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>recondite</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>sinuous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>banal</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>empirical</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>hackneyed</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>homogeneous</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>insipid</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>jejune</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>mediocre</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>nondescript</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>platitude</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>prosaic</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>trifle</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>trite</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>vapid</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='connote#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
connotation
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>implication</td>
</tr>
<tr>
<td class='wordform'>
<span>
connotative
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>implying</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="connote" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>connote</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="connote#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

