
<!DOCTYPE html>
<html>
<head>
<title>Word: spurious | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An apocryphal story is widely known but probably not true.</p>
<p class='rw-defn idx1' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx2' style='display:none'>If you employ chicanery, you are devising and carrying out clever plans and trickery to cheat and deceive people.</p>
<p class='rw-defn idx3' style='display:none'>A chimerical idea is unrealistic, imaginary, or unlikely to be fulfilled.</p>
<p class='rw-defn idx4' style='display:none'>Something that is delusive deceives you by giving a false belief about yourself or the situation you are in.</p>
<p class='rw-defn idx5' style='display:none'>If you disabuse someone of an idea or notion, you persuade them that the idea is in fact untrue.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is disingenuous is not straightforward or is dishonest in what they say or do.</p>
<p class='rw-defn idx7' style='display:none'>Empirical evidence or study is based on real experience or scientific experiments rather than on unproven theories.</p>
<p class='rw-defn idx8' style='display:none'>If you describe something as ersatz, you dislike it because it is artificial or fake—and is used in place of a higher quality item.</p>
<p class='rw-defn idx9' style='display:none'>A euphemism is a polite synonym or expression that people use when they want to avoid talking about—or indirectly replace—an unpleasant or embarrassing thing.</p>
<p class='rw-defn idx10' style='display:none'>Something factitious is not genuine or natural; it is made to happen in a forced, artificial way.</p>
<p class='rw-defn idx11' style='display:none'>A fallacy is an idea or belief that is false.</p>
<p class='rw-defn idx12' style='display:none'>If something is gilded, it has been deceptively given a more attractive appearance than it normally possesses; a very thin layer of gold often covers something gilded.</p>
<p class='rw-defn idx13' style='display:none'>Something illusory appears real or possible; in fact, it is neither.</p>
<p class='rw-defn idx14' style='display:none'>If you describe someone&#8217;s appearance or behavior as impeccable, you mean that it is perfect and therefore impossible to criticize.</p>
<p class='rw-defn idx15' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx16' style='display:none'>A judicious person shows good or sound judgment because they are wise and careful in making decisions.</p>
<p class='rw-defn idx17' style='display:none'>A mendacious person does not tell the truth.</p>
<p class='rw-defn idx18' style='display:none'>Something meretricious seems good and useful; in fact, it’s just showy and does not have much value at all.</p>
<p class='rw-defn idx19' style='display:none'>A mountebank is a smooth-talking, dishonest person who tricks and cheats people.</p>
<p class='rw-defn idx20' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx21' style='display:none'>Something that is ostensible appears to be true or is officially declared to be true but is really a cover for the actual truth of a situation.</p>
<p class='rw-defn idx22' style='display:none'>A patina is a smooth, shiny film or surface that gradually develops on things—such as wood, leather, and metal utensils—that have seen a lot of use.</p>
<p class='rw-defn idx23' style='display:none'>If you handle something in a pragmatic way, you deal with it in a realistic and practical manner rather than just following unproven theories or ideas.</p>
<p class='rw-defn idx24' style='display:none'>If you prevaricate, you avoid giving a direct or honest answer, usually because you want to hide the truth or want to delay or avoid making a hard decision.</p>
<p class='rw-defn idx25' style='display:none'>Sophistry is the clever use of arguments that seem correct but are in fact unsound and misleading, used with the intent to deceive people.</p>
<p class='rw-defn idx26' style='display:none'>Something that is specious seems to be good, sound, or correct but is actually wrong or fake.</p>
<p class='rw-defn idx27' style='display:none'>An unfounded claim is not based upon evidence; instead, it is unproven or groundless.</p>
<p class='rw-defn idx28' style='display:none'>Something that is unsullied is unstained and clean.</p>
<p class='rw-defn idx29' style='display:none'>An unwarranted decision cannot be justified because there is no evidence to back it up; therefore, it is needless and uncalled-for.</p>
<p class='rw-defn idx30' style='display:none'>A utilitarian object is useful, serviceable, and practical—rather than fancy or unnecessary.</p>
<p class='rw-defn idx31' style='display:none'>A veneer is a thin layer, such as a thin sheet of expensive wood over a cheaper type of wood that gives a false appearance of higher quality; a person can also put forth a false front or veneer.</p>
<p class='rw-defn idx32' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx33' style='display:none'>Verisimilitude is something&#8217;s authenticity or appearance of being real or true.</p>
<p class='rw-defn idx34' style='display:none'>Something that is veritable is authentic, actual, genuine, or without doubt.</p>
<p class='rw-defn idx35' style='display:none'>The verity of something is the truth or reality of it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
Middle/High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>spurious</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='spurious#' id='pronounce-sound' path='audio/words/amy-spurious'></a>
SPYOOR-ee-uhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='spurious#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='spurious#' id='context-sound' path='audio/wordcontexts/brian-spurious'></a>
Theo considered the senator&#8217;s comments on national education to be <em>spurious</em> or not well-founded, which was not surprising since the lawmaker had never worked in schools.  Since his information for educational reform had no grounding in teaching or learning but was entirely based on economics, it made his so-called good ideas <em>spurious</em> or invalid.  Theo acknowledged that the senator spoke well and presented his concepts persuasively, but ultimately his knowledge of schools was not genuine, but deceptive and <em>spurious</em>.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How might you react to a <em>spurious</em> statement made about you?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
You would be angry since the statement is untrue but many people believe it anyway.
</li>
<li class='choice '>
<span class='result'></span>
You would be grateful since the statement corrects a previous one that was false and damaging.
</li>
<li class='choice '>
<span class='result'></span>
You would be honored that someone made a statement full of such praise and admiration.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='spurious#' id='definition-sound' path='audio/wordmeanings/amy-spurious'></a>
A <em>spurious</em> statement is false because it is not based on sound thinking; therefore, it is not what it appears to be.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>false</em>
</span>
</span>
</div>
<a class='quick-help' href='spurious#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='spurious#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/spurious/memory_hooks/4486.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>S</span></span>er<span class="emp1"><span>ious</span></span>ly <span class="emp3"><span>Pur</span></span>e?</span></span> You say that your comments are <span class="emp1"><span>s</span></span>er<span class="emp1"><span>ious</span></span> and <span class="emp3"><span>pur</span></span>e?  I find them rather to be <span class="emp1"><span>s</span></span><span class="emp3"><span>pur</span></span><span class="emp1"><span>ious</span></span>,  and certainly not <span class="emp1"><span>s</span></span>er<span class="emp1"><span>ious</span></span> or <span class="emp3"><span>pur</span></span>e at all, but rather filled with <span class="emp1"><span>s</span></span><span class="emp3"><span>pur</span></span><span class="emp1"><span>ious</span></span> and fake information, you sly trickster!
</p>
</div>

<div id='memhook-button-bar'>
<a href="spurious#" id="add-public-hook" style="" url="https://membean.com/mywords/spurious/memory_hooks">Use other public hook</a>
<a href="spurious#" id="memhook-use-own" url="https://membean.com/mywords/spurious/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='spurious#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
An excessive use of exclamation marks is a certain indication of an unpracticed writer or of one who wants to add a <b>spurious</b> dash of sensation to something unsensational.
<span class='attribution'>&mdash; Henry Watson Fowler, British lexicographer</span>
<img alt="Henry watson fowler, british lexicographer" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Henry Watson Fowler, British lexicographer.jpg?qdep8" width="80" />
</li>
<li>
Greaves and her team fit the data using a 12th order polynomial, a mathematical expression with 12 variables. According to Snellan and his team, using this polynomial actually introduced <b>spurious</b> results. The way they [analyzed] it, no phosphine was found.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
Researchers trying to understand diseases and find new ways to treat them are running into a serious problem in their labs: One of the most commonly used tools often produces <b>spurious</b> results.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
An Instagram post in March boasted, “Meet Fumi and Cindy! They called us and booked an in-home treatment seeking to bolster their immune system amidst a pandemic. Now, they are wearing their Corona-proof immunity vests!” These <b>spurious</b> claims did not go unnoticed by the Federal Trade Commission. The FTC sent Revival Health a warning letter on May 27.
<cite class='attribution'>
&mdash;
SFGate
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/spurious/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='spurious#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>spuri</td>
<td>
&rarr;
</td>
<td class='meaning'>illegitimate, false</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ous_possessing' data-tree-url='//cdn1.membean.com/public/data/treexml' href='spurious#'>
<span class=''></span>
-ous
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>possessing the nature of</td>
</tr>
</table>
<p>Something <em>spurious</em> &#8220;possesses the nature of being false.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='spurious#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Boss Level 8</strong><span> Evidence presented that the Earth is flat is obviously spurious.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/spurious.jpg' video_url='examplevids/spurious' video_width='350'></span>
<div id='wt-container'>
<img alt="Spurious" height="288" src="https://cdn1.membean.com/video/examplevids/spurious.jpg" width="350" />
<div class='center'>
<a href="spurious#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='spurious#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Spurious" src="https://cdn0.membean.com/public/images/wordimages/cons2/spurious.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='spurious#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>apocryphal</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>chicanery</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>chimerical</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>delusive</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>disingenuous</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>ersatz</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>euphemism</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>factitious</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>fallacy</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>gilded</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>illusory</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>mendacious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>meretricious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>mountebank</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>ostensible</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>patina</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>prevaricate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>sophistry</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>specious</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>unfounded</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>unwarranted</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>veneer</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>disabuse</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>empirical</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>impeccable</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>judicious</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>pragmatic</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>unsullied</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>utilitarian</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>verisimilitude</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>veritable</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="spurious" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>spurious</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="spurious#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

