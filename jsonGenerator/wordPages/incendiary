
<!DOCTYPE html>
<html>
<head>
<title>Word: incendiary | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Incendiary-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/incendiary-large.jpg?qdep8" />
</div>
<a href="incendiary#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>When you abet someone, you help them in a criminal or illegal act.</p>
<p class='rw-defn idx2' style='display:none'>An acrid smell or taste is strong, unpleasant, and stings your nose or throat.</p>
<p class='rw-defn idx3' style='display:none'>You allay someone&#8217;s fear or concern by making them feel less afraid or worried.</p>
<p class='rw-defn idx4' style='display:none'>If you alleviate pain or suffering, you make it less intense or severe.</p>
<p class='rw-defn idx5' style='display:none'>When you assuage an unpleasant feeling, you make it less painful or severe, thereby calming it.</p>
<p class='rw-defn idx6' style='display:none'>Bedlam is a situation with a great deal of noise and confusion.</p>
<p class='rw-defn idx7' style='display:none'>If you are bellicose, you behave in an aggressive way and are likely to start an argument or fight.</p>
<p class='rw-defn idx8' style='display:none'>A belligerent person or country is aggressive, very unfriendly, and likely to start a fight.</p>
<p class='rw-defn idx9' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx10' style='display:none'>A catalyst is an agent that enacts change, such as speeding up a chemical reaction or causing an event to occur.</p>
<p class='rw-defn idx11' style='display:none'>A conflagration is a fire that burns over a large area and is highly destructive.</p>
<p class='rw-defn idx12' style='display:none'>Consternation is the feeling of anxiety or fear, sometimes paralyzing in its effect, and often caused by something unexpected that has happened.</p>
<p class='rw-defn idx13' style='display:none'>Something that is deleterious has a harmful effect.</p>
<p class='rw-defn idx14' style='display:none'>If something discomfits you, it makes you feel embarrassed, confused, uncomfortable, or frustrated.</p>
<p class='rw-defn idx15' style='display:none'>When someone feels disquiet about a situation, they feel very worried or nervous.</p>
<p class='rw-defn idx16' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx17' style='display:none'>If you eschew something, you deliberately avoid doing it, especially for moral reasons.</p>
<p class='rw-defn idx18' style='display:none'>To foment is to encourage people to protest, fight, or cause trouble and violent opposition to something that is viewed by some as undesirable.</p>
<p class='rw-defn idx19' style='display:none'>When you galvanize someone, you cause them to improve a situation, solve a problem, or take some other action by making them feel excited, afraid, or angry.</p>
<p class='rw-defn idx20' style='display:none'>A hindrance is an obstacle or obstruction that gets in your way and prevents you from doing something.</p>
<p class='rw-defn idx21' style='display:none'>An impediment is something that blocks or obstructs progress; it can also be a weakness or disorder, such as having difficulty with speaking.</p>
<p class='rw-defn idx22' style='display:none'>An inflammable substance or person&#8217;s temper is easily set on fire.</p>
<p class='rw-defn idx23' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx24' style='display:none'>When you instigate something, you start it or stir it up, usually for the purpose of causing trouble of some kind.</p>
<p class='rw-defn idx25' style='display:none'>An insurrection is a rebellion or open uprising against an established form of government.</p>
<p class='rw-defn idx26' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx27' style='display:none'>If you mollify someone, you say or do something to make that person feel less angry or upset.</p>
<p class='rw-defn idx28' style='display:none'>If you nettle someone, you irritate or annoy them.</p>
<p class='rw-defn idx29' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx30' style='display:none'>Something that is noxious is harmful, extremely unpleasant, and often poisonous.</p>
<p class='rw-defn idx31' style='display:none'>A palliative action makes a bad situation seem better; nevertheless, it does not actually solve the problem.</p>
<p class='rw-defn idx32' style='display:none'>Something that is pernicious is very harmful or evil, often in a way that is hidden or not quickly noticed.</p>
<p class='rw-defn idx33' style='display:none'>To quell something is to stamp out, quiet, or overcome it.</p>
<p class='rw-defn idx34' style='display:none'>If you refrain from doing something, you do not do it—even though you want to.</p>
<p class='rw-defn idx35' style='display:none'>A salubrious place or area is pleasant, clean, healthy, and comfortable to live in.</p>
<p class='rw-defn idx36' style='display:none'>Sedition is the act of encouraging people to disobey and oppose the government currently in power.</p>
<p class='rw-defn idx37' style='display:none'>A serene place or situation is peaceful and calm.</p>
<p class='rw-defn idx38' style='display:none'>A subversive act destroys or does great harm to a government or other institution.</p>
<p class='rw-defn idx39' style='display:none'>If something is tranquil, it is peaceful, calm, and quiet.</p>
<p class='rw-defn idx40' style='display:none'>A tumultuous event or period of time is filled with great excitement, confusion, or violence; a tumultuous reaction to something is likewise very loud and noisy because people are happy and excited.</p>
<p class='rw-defn idx41' style='display:none'>To undermine a plan or endeavor is to weaken it or make it unstable in a gradual but purposeful fashion.</p>
<p class='rw-defn idx42' style='display:none'>When something whets your appetite, it increases your desire for it, especially by giving you a little idea or pleasing hint of what it is like.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>incendiary</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='incendiary#' id='pronounce-sound' path='audio/words/amy-incendiary'></a>
in-SEN-dee-er-ee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='incendiary#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='incendiary#' id='context-sound' path='audio/wordcontexts/brian-incendiary'></a>
Jonathan&#8217;s <em>incendiary</em> or rebellious comments towards the government caused a riot in the city.  The members of the angry crowd threw <em>incendiary</em> devices that caused many buildings to catch on fire.  His <em>incendiary</em> words had caused the people to catch the fire of rebellion.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is the purpose of an <em>incendiary</em> speech?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
To deny wrongdoing and convince people of someone&#8217;s innocence.
</li>
<li class='choice '>
<span class='result'></span>
To motivate people to work together to support a cause.
</li>
<li class='choice answer '>
<span class='result'></span>
To provoke anger and encourage people to rebel.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='incendiary#' id='definition-sound' path='audio/wordmeanings/amy-incendiary'></a>
An <em>incendiary</em> device causes objects to catch on fire; <em>incendiary</em> comments can cause riots to flare up.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>causing problems</em>
</span>
</span>
</div>
<a class='quick-help' href='incendiary#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='incendiary#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/incendiary/memory_hooks/3534.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Send</span></span> a <span class="emp2"><span>Diary</span></span></span></span> My stupid boyfriend stole my <span class="emp2"><span>diary</span></span> and <span class="emp1"><span>sent</span></span> it to his friends, so I broke up with him in in<span class="emp1"><span>cen</span></span><span class="emp2"><span>diary</span></span> fashion.
</p>
</div>

<div id='memhook-button-bar'>
<a href="incendiary#" id="add-public-hook" style="" url="https://membean.com/mywords/incendiary/memory_hooks">Use other public hook</a>
<a href="incendiary#" id="memhook-use-own" url="https://membean.com/mywords/incendiary/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='incendiary#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Officials also say this device was an <b>incendiary</b> device rather than an explosive device, and that means it would cause a fire as opposed to an explosion.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
So last December, Mr. Bukele went to the haunted village of El Mozote and made an <b>incendiary</b> speech attacking his opponents and dismissing the peace accords. . . . The speech at El Mozote sparked anger among residents who still bear the scars.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
A package ignited at a Washington postal facility Friday, a day after fiery packages sent to Maryland's governor and transportation secretary burned the fingers of workers who opened them. . . . Anyone arrested would be charged with possession and use of an <b>incendiary</b> device, which includes a maximum penalty of 20 years in prison, authorities said.
<cite class='attribution'>
&mdash;
CBS News
</cite>
</li>
<li>
"No religious activity of any kind, either symbolic or actual" would be allowed on the 67-acre holy site in Ayodhya that has, for ten years now, been at the center of the most <b>incendiary</b> issue in Indian politics. . . . As so often in the past, efforts to defuse tensions over Ayodhya have resulted in legal confusion, and may not have averted the feared confrontation.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/incendiary/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='incendiary#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='in_in' data-tree-url='//cdn1.membean.com/public/data/treexml' href='incendiary#'>
<span class='common'></span>
in-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>in, on</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='cand_shine' data-tree-url='//cdn1.membean.com/public/data/treexml' href='incendiary#'>
<span class=''></span>
cand
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>be hot, be on fire</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ary_relates' data-tree-url='//cdn1.membean.com/public/data/treexml' href='incendiary#'>
<span class=''></span>
-ary
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>An <em>incendiary</em> device causes something to &#8220;be hot&#8221; because it sets it &#8220;on fire.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='incendiary#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Shrivenham CFI</strong><span> An incendiary device.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/incendiary.jpg' video_url='examplevids/incendiary' video_width='350'></span>
<div id='wt-container'>
<img alt="Incendiary" height="288" src="https://cdn1.membean.com/video/examplevids/incendiary.jpg" width="350" />
<div class='center'>
<a href="incendiary#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='incendiary#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Incendiary" src="https://cdn2.membean.com/public/images/wordimages/cons2/incendiary.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='incendiary#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>abet</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>acrid</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>bedlam</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>bellicose</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>belligerent</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>catalyst</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>conflagration</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>consternation</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>deleterious</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>discomfit</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>disquiet</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>foment</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>galvanize</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>inflammable</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>instigate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>insurrection</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>nettle</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>noxious</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>pernicious</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>sedition</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>subversive</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>tumultuous</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>undermine</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>whet</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>allay</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>alleviate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>assuage</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>eschew</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>hindrance</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>impediment</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>mollify</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>palliative</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>quell</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>refrain</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>salubrious</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>serene</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>tranquil</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="incendiary" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>incendiary</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="incendiary#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

