
<!DOCTYPE html>
<html>
<head>
<title>Word: taint | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you absolve someone, you publicly and formally say that they are not guilty or responsible for any wrongdoing.</p>
<p class='rw-defn idx1' style='display:none'>If you adulterate something, you lessen its quality by adding inferior ingredients or elements to it.</p>
<p class='rw-defn idx2' style='display:none'>An affliction is something that causes pain and mental suffering, especially a medical condition.</p>
<p class='rw-defn idx3' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx4' style='display:none'>If you besmirch someone, you spoil their good reputation by saying bad things about them, usually things that you know are not true.</p>
<p class='rw-defn idx5' style='display:none'>Something is a blight if it spoils or damages things severely; blight is often used to describe something that ruins or kills crops.</p>
<p class='rw-defn idx6' style='display:none'>Something brackish is distasteful and unpleasant usually because something else is mixed in with it; brackish water, for instance, is a mixture of fresh and salt water and cannot be drunk.</p>
<p class='rw-defn idx7' style='display:none'>A contagion is a disease—or the transmission of that disease—that is easily spread from one person to another through touch or the air.</p>
<p class='rw-defn idx8' style='display:none'>A decadent person has low moral standards and is more interested in pleasure than serious matters.</p>
<p class='rw-defn idx9' style='display:none'>To defile something holy or important is to do something to it or say something about it that is highly offensive to others.</p>
<p class='rw-defn idx10' style='display:none'>Depravity is behavior that is immoral, corrupt, or evil.</p>
<p class='rw-defn idx11' style='display:none'>If you desecrate something that is considered holy or very special, you deliberately spoil or damage it.</p>
<p class='rw-defn idx12' style='display:none'>A eulogy is a speech or other piece of writing, often part of a funeral, in which someone or something is highly praised.</p>
<p class='rw-defn idx13' style='display:none'>If you exculpate someone, you prove that they are not guilty of a crime.</p>
<p class='rw-defn idx14' style='display:none'>When someone is exonerated from guilt, a task, or a responsibility, they are set free or excused from it.</p>
<p class='rw-defn idx15' style='display:none'>If you extol something or someone, you praise it or them very enthusiastically.</p>
<p class='rw-defn idx16' style='display:none'>A foible is a small weakness or character flaw in a person that is considered somewhat unusual; that said, it is also viewed as unimportant and harmless.</p>
<p class='rw-defn idx17' style='display:none'>Idiosyncratic tendencies, behavior, or habits are unusual and strange.</p>
<p class='rw-defn idx18' style='display:none'>Something that is immaculate is very clean, pure, or completely free from error.</p>
<p class='rw-defn idx19' style='display:none'>Someone, such as a performer or athlete, is inimitable when they are so good or unique in their talent that it is unlikely anyone else can be their equal.</p>
<p class='rw-defn idx20' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx21' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx22' style='display:none'>If someone has a jaundiced view of something, they ignore the good parts and can only see the bad aspects of it.</p>
<p class='rw-defn idx23' style='display:none'>When someone is lionized, they are treated as being very important or famous—although they may not deserve to be.</p>
<p class='rw-defn idx24' style='display:none'>Malfeasance is an unlawful act, especially one committed by a trusted public official.</p>
<p class='rw-defn idx25' style='display:none'>A pathogen is an organism, such as a bacterium or virus, that causes disease.</p>
<p class='rw-defn idx26' style='display:none'>Something that is pristine has not lost any of its original perfection or purity.</p>
<p class='rw-defn idx27' style='display:none'>Something that has putrefied has decayed and emits a bad odor.</p>
<p class='rw-defn idx28' style='display:none'>Something rancid is way past fresh; in fact, it is decomposing quickly and has a bad taste or smell.</p>
<p class='rw-defn idx29' style='display:none'>Something that is sacrosanct is considered to be so important, special, or holy that no one is allowed to criticize, tamper with, or change it in any way.</p>
<p class='rw-defn idx30' style='display:none'>A salubrious place or area is pleasant, clean, healthy, and comfortable to live in.</p>
<p class='rw-defn idx31' style='display:none'>A salutary experience is beneficial to you since it strengthens you in some way, although it may be unpleasant as you undergo it; this word also refers to promoting good health.</p>
<p class='rw-defn idx32' style='display:none'>Something that has a stigma is not socially acceptable; therefore, it has a strong feeling of shame or disgrace attached to it.</p>
<p class='rw-defn idx33' style='display:none'>If you describe something as unsavory, you mean that it is unpleasant or morally unacceptable.</p>
<p class='rw-defn idx34' style='display:none'>To come out of something unscathed is to come out of it uninjured.</p>
<p class='rw-defn idx35' style='display:none'>Something that is unsullied is unstained and clean.</p>
<p class='rw-defn idx36' style='display:none'>Venerable people command respect because they are old and wise.</p>
<p class='rw-defn idx37' style='display:none'>If a person is vindicated, their ideas, decisions, or actions—once considered wrong—are proved correct or not to be blamed.</p>
<p class='rw-defn idx38' style='display:none'>If someone vitiates something, they make it less effective, weaker, or reduced in quality.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>taint</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='taint#' id='pronounce-sound' path='audio/words/amy-taint'></a>
taynt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='taint#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='taint#' id='context-sound' path='audio/wordcontexts/brian-taint'></a>
My parents&#8217; announcement that they have decided to move to the other end of the country has <em>tainted</em> or spoiled an otherwise wonderful vacation.  We were all having such a great time, but the <em>taint</em> of this bad news has ruined the trip and left us sad.  My brother agrees that even the most pleasing memories from this beautiful summer seem to be <em>tainted</em> or forever s<em>tained</em> now by this painful, dark, and unpleasant news&#8212;we are simply not wanting to leave all of our friends behind!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How could someone <em>taint</em> drinking water?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They could boil it to get rid of any germs or bacteria.
</li>
<li class='choice answer '>
<span class='result'></span>
They could add something that makes it unsafe to drink.
</li>
<li class='choice '>
<span class='result'></span>
They could add ice cubes to it so it becomes colder.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='taint#' id='definition-sound' path='audio/wordmeanings/amy-taint'></a>
To <em>taint</em> is to give an undesirable quality that damages a person&#8217;s reputation or otherwise spoils something.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>stain</em>
</span>
</span>
</div>
<a class='quick-help' href='taint#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='taint#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/taint/memory_hooks/5344.json'></span>
<p>
<img alt="Taint" src="https://cdn1.membean.com/public/images/wordimages/hook/taint.jpg?qdep8" />
<span class="emp0"><span><span class="emp1"><span>T'ain't</span></span> a <span class="emp1"><span>Tint</span></span></span></span> Woody fired back at the dishonest used car salesman, "That dark stuff on the winda'?  Nah, <span class="emp1"><span>t'ain't</span></span> a <span class="emp1"><span>tint</span></span>--that's just <span class="emp1"><span>taint</span></span>ed with dirt. This car's <span class="emp1"><span>taint</span></span>ed, just like you!"
</p>
</div>

<div id='memhook-button-bar'>
<a href="taint#" id="add-public-hook" style="" url="https://membean.com/mywords/taint/memory_hooks">Use other public hook</a>
<a href="taint#" id="memhook-use-own" url="https://membean.com/mywords/taint/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='taint#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Error-prone or biased artificial-intelligence systems have the potential to <b>taint</b> our social ecosystem in ways that are initially hard to detect, harmful in the long term and expensive—or even impossible—to reverse.
<cite class='attribution'>
&mdash;
The Wall Street Journal
</cite>
</li>
<li>
PFAS have been discovered in the food chain in other states, including in milk from dairy farms in New York and Pennsylvania. Some of the contamination was traced back to firefighting foam that <b>tainted</b> well water.
<cite class='attribution'>
&mdash;
The Colorado Sun
</cite>
</li>
<li>
Despite circumstances that have arguably <b>tainted</b> his reputation, Riverside City Attorney Greg Priamos appears headed for confirmation as Riverside County’s next chief counsel, bringing to the position the type of management that “exudes” confidence and strength, according to Board of Supervisors Chairman Jeff Stone.
<cite class='attribution'>
&mdash;
The Desert Sun
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/taint/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='taint#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='taint_stained' data-tree-url='//cdn1.membean.com/public/data/treexml' href='taint#'>
<span class=''></span>
taint
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>stained, dyed</td>
</tr>
</table>
<p>To <em>taint</em> something pure is to &#8220;stain&#8221; or &#8220;dye&#8221; it.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='taint#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>CBS SF Bay Area</strong><span> A way to hopefully prevent produce from becoming tainted.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/taint.jpg' video_url='examplevids/taint' video_width='350'></span>
<div id='wt-container'>
<img alt="Taint" height="288" src="https://cdn1.membean.com/video/examplevids/taint.jpg" width="350" />
<div class='center'>
<a href="taint#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='taint#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Taint" src="https://cdn3.membean.com/public/images/wordimages/cons2/taint.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='taint#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>adulterate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>affliction</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>besmirch</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>blight</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>brackish</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>contagion</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>decadent</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>defile</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>depravity</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>desecrate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>foible</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>idiosyncratic</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>jaundiced</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>malfeasance</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>pathogen</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>putrefy</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>rancid</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>stigma</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>unsavory</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>vitiate</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>absolve</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>eulogy</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>exculpate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>exonerate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>extol</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>immaculate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>inimitable</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>lionize</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>pristine</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>sacrosanct</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>salubrious</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>salutary</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>unscathed</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>unsullied</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>venerable</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>vindicate</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='taint#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
taint
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>defect; stain; spot</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="taint" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>taint</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="taint#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

