
<!DOCTYPE html>
<html>
<head>
<title>Word: viscid | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Viscid-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/viscid-large.jpg?qdep8" />
</div>
<a href="viscid#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>Accretion is the slow, gradual process by which new things are added and something gets bigger.</p>
<p class='rw-defn idx1' style='display:none'>An adherent is a supporter or follower of a leader or a cause.</p>
<p class='rw-defn idx2' style='display:none'>To agglomerate a group of things is to gather them together without any noticeable order.</p>
<p class='rw-defn idx3' style='display:none'>An aggregate is the final or sum total after many different amounts or scores have been added together.</p>
<p class='rw-defn idx4' style='display:none'>When two or more things, such as organizations, amalgamate, they combine to become one large thing.</p>
<p class='rw-defn idx5' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx6' style='display:none'>If liquid coagulates, it becomes thick and solid.</p>
<p class='rw-defn idx7' style='display:none'>If two or more things coalesce, they come together to form a single larger unit.</p>
<p class='rw-defn idx8' style='display:none'>When a liquid congeals, it becomes very thick and sticky, almost like a solid.</p>
<p class='rw-defn idx9' style='display:none'>A diaphanous cloth is thin enough to see through.</p>
<p class='rw-defn idx10' style='display:none'>Dissolution is the breaking up or official end of a group, such as a couple or institution; it can also be the act of separating something into smaller components.</p>
<p class='rw-defn idx11' style='display:none'>Something ethereal has a delicate beauty that makes it seem not part of the real world.</p>
<p class='rw-defn idx12' style='display:none'>Something that is evanescent lasts for only a short time before disappearing from sight or memory.</p>
<p class='rw-defn idx13' style='display:none'>Something that is glutinous is thick and very sticky, often in an unpleasant way.</p>
<p class='rw-defn idx14' style='display:none'>A gossamer material is very thin, light, and delicate.</p>
<p class='rw-defn idx15' style='display:none'>If you describe something as nebulous, you mean that it is unclear, vague, and not clearly defined; a shape that is nebulous has no clear boundaries.</p>
<p class='rw-defn idx16' style='display:none'>To rarefy something is to make it less dense, as in oxygen at high altitudes; this word also refers to purifying or refining something, thereby making it less ordinary or commonplace.</p>
<p class='rw-defn idx17' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx18' style='display:none'>Something that is vaporous is not completely formed but foggy and misty; in the same vein, a vaporous idea is insubstantial and vague.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>viscid</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='viscid#' id='pronounce-sound' path='audio/words/amy-viscid'></a>
VIS-id
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='viscid#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='viscid#' id='context-sound' path='audio/wordcontexts/brian-viscid'></a>
The recipe called for several <em>viscid</em> ingredients: molasses, honey, and maple syrup.  The mixture of these thick, <em>viscid</em> fluids with nuts, oats, and raisins created a gooey mass, which was spooned out with some difficulty onto the pan to bake.  The gluey, sticky, and sugary composition of the <em>viscid</em> ingredients mixed with the carbohydrates and proteins of the dry foods made a perfect mixture for high energy snacks.  After their time in the oven had hardened them, the treats were still a bit sticky and certainly still thick, but no longer completely <em>viscid</em>.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a description of something <em>viscid</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is overly sweet and full of energy.
</li>
<li class='choice answer '>
<span class='result'></span>
It is thick, slow moving, and syrupy. 
</li>
<li class='choice '>
<span class='result'></span>
It&#8217;s an important part that holds a recipe together.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='viscid#' id='definition-sound' path='audio/wordmeanings/amy-viscid'></a>
Something <em>viscid</em> is thick, sticky, and does not flow easily.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>thick and sticky</em>
</span>
</span>
</div>
<a class='quick-help' href='viscid#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='viscid#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/viscid/memory_hooks/4048.json'></span>
<p>
<span class="emp0"><span>V<span class="emp3"><span>iscous</span></span> L<span class="emp3"><span>isps</span></span></span></span>  If one were to say "<span class="emp3"><span>thick</span></span>y <span class="emp3"><span>stick</span></span> <span class="emp3"><span>stick</span></span>y <span class="emp3"><span>thick</span></span> v<span class="emp3"><span>iscous</span></span> l<span class="emp3"><span>isps</span></span>" ten times in succession, one would feel one's tongue become v<span class="emp3"><span>iscous</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="viscid#" id="add-public-hook" style="" url="https://membean.com/mywords/viscid/memory_hooks">Use other public hook</a>
<a href="viscid#" id="memhook-use-own" url="https://membean.com/mywords/viscid/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='viscid#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Sugar Daddy lollipops, made of <b>viscid</b> caramel, will last through any double feature, and their remains can always be left on the seat, where they are guaranteed to adhere tenaciously to the victim’s bottom.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
<li>
The neighborhood now known as Boerum Hill is a slice of Brooklyn that extends along a few quiet, tree-lined streets layered between the antique stores and Middle Eastern groceries of Atlantic Avenue and the <b>viscid</b> green torpor of the Gowanus Canal.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Algae oil has passed my acid test with its sustainability, health benefits, delicate taste and high smoke point (485 degrees F), higher than peanut and canola oils. Algae oil is fast becoming a beloved staple: it’s excellent for sautéing with a slightly nutty, popcorn flavor . . . and light and delicious on salads, where I use less because of the oil’s silky, less <b>viscid</b> quality.
<cite class='attribution'>
&mdash;
Traverse City Record-Eagle
</cite>
</li>
<li>
The nature of the <b>viscid</b> secretion from these glands has been shown by chemical analysis to correspond with the gastric fluid in animal forms.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/viscid/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='viscid#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='visc_birdlime' data-tree-url='//cdn1.membean.com/public/data/treexml' href='viscid#'>
<span class=''></span>
visc
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>mistletoe, birdlime</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='id_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='viscid#'>
<span class=''></span>
-id
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>Birdlime is a sticky <em>viscid</em> substance used to ensnare birds which can be made from &#8220;mistletoe&#8221; berries; you simply spread the &#8220;birdlime&#8221; on branches, and wait for the bird to land in the <em>viscous</em> substance, to which it sticks.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='viscid#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Viscid" src="https://cdn0.membean.com/public/images/wordimages/cons2/viscid.png?qdep6" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='viscid#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>accretion</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>adherent</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>agglomerate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>aggregate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>amalgamate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>coagulate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>coalesce</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>congeal</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>glutinous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='5' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>diaphanous</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>dissolution</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>ethereal</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>evanescent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>gossamer</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>nebulous</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>rarefy</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>vaporous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='viscid#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
viscous
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>sticky, gooey, syrupy</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="viscid" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>viscid</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="viscid#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

