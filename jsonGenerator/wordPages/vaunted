
<!DOCTYPE html>
<html>
<head>
<title>Word: vaunted | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An audacious person acts with great daring and sometimes reckless bravery—despite risks and warnings from other people—in order to achieve something.</p>
<p class='rw-defn idx1' style='display:none'>When a person is speaking or writing in a bombastic fashion, they are very self-centered, extremely showy, and excessively proud.</p>
<p class='rw-defn idx2' style='display:none'>Braggadocio is an excessively proud way of talking about your achievements or possessions that can annoy others.</p>
<p class='rw-defn idx3' style='display:none'>Bravado is behavior that is deliberately intended to show courage or confidence, sometimes actual but usually pretended, in order to impress other people.</p>
<p class='rw-defn idx4' style='display:none'>Bumptious people are annoying because they are too proud of their abilities or opinions—they are full of themselves.</p>
<p class='rw-defn idx5' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx6' style='display:none'>If you describe someone, usually a young woman, as demure, you mean that she is quiet, shy, and always behaves modestly.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is diffident is shy, does not want to draw notice to themselves, and is lacking in self-confidence.</p>
<p class='rw-defn idx8' style='display:none'>An exorbitant price or fee is much higher than what it should be or what is considered reasonable.</p>
<p class='rw-defn idx9' style='display:none'>If someone or something is flamboyant, the former is trying to show off in a way that deliberately attracts attention, and the latter is brightly colored and highly decorated.</p>
<p class='rw-defn idx10' style='display:none'>Something florid has too much decoration or is too elaborate.</p>
<p class='rw-defn idx11' style='display:none'>Praise, an apology, or gratitude is fulsome if it is so exaggerated and elaborate that it does not seem sincere.</p>
<p class='rw-defn idx12' style='display:none'>Grandiloquent speech is highly formal, exaggerated, and often seems both silly and hollow because it is expressly used to appear impressive and important.</p>
<p class='rw-defn idx13' style='display:none'>Hyperbole is a way of emphasizing something that makes it sound much more impressive or much worse than it actually is.</p>
<p class='rw-defn idx14' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx15' style='display:none'>If something is inordinate, it is much larger in amount or degree than is normally expected.</p>
<p class='rw-defn idx16' style='display:none'>A person who is being laconic uses very few words to say something.</p>
<p class='rw-defn idx17' style='display:none'>An ornate object is heavily or excessively decorated with complicated shapes and patterns.</p>
<p class='rw-defn idx18' style='display:none'>If you describe an action as ostentatious, you think it is an extreme and exaggerated way of impressing people.</p>
<p class='rw-defn idx19' style='display:none'>To be politic in a decision is to be socially wise and diplomatic.</p>
<p class='rw-defn idx20' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx21' style='display:none'>Skittish persons or animals are made easily nervous or alarmed; they are likely to change behavior quickly and unpredictably.</p>
<p class='rw-defn idx22' style='display:none'>Turgid writing or speech is excessively complicated, being filled with too many needlessly difficult words; consequently, such verbiage is boring and difficult to understand.</p>
<p class='rw-defn idx23' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx24' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>
<p class='rw-defn idx25' style='display:none'>A voluble person can speak easily with speech that naturally flows; voluble people also tend to be quite chatty.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>vaunted</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='vaunted#' id='pronounce-sound' path='audio/words/amy-vaunted'></a>
VAWN-tid
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='vaunted#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='vaunted#' id='context-sound' path='audio/wordcontexts/brian-vaunted'></a>
The <em>vaunted</em>, highly celebrated soccer team took center stage at the parade where they boldly strutted their stuff.  The highly <em>vaunted</em> and bragged about team had swept the World Cup tournament, and thus were soaking in all the praise from their adoring fans.  Some thought that the players were a bit too <em>vaunted</em> or boasted about, but one had to admit that they had won the biggest sporting event on the planet.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of something&#8217;s being <em>vaunted</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
An old and abandoned house that many neighbors believe might have a ghost living in it.
</li>
<li class='choice answer '>
<span class='result'></span>
A boy&#8217;s spelling bee victory that his parents can&#8217;t stop talking about on social media.
</li>
<li class='choice '>
<span class='result'></span>
A large oak tree in a park that everyone in the neighborhood likes to sit underneath in the summer.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='vaunted#' id='definition-sound' path='audio/wordmeanings/amy-vaunted'></a>
Something that is <em>vaunted</em>, such as someone&#8217;s ability, is too highly praised and boasted about.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>boasted about</em>
</span>
</span>
</div>
<a class='quick-help' href='vaunted#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='vaunted#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/vaunted/memory_hooks/3280.json'></span>
<p>
<span class="emp0"><span>H<span class="emp3"><span>aunted</span></span> House V<span class="emp3"><span>aunted</span></span></span></span> There is a new Halloween H<span class="emp3"><span>aunted</span></span> House in our town that is much v<span class="emp3"><span>aunted</span></span> by the owners and people alike; I went, and its v<span class="emp3"><span>aunted</span></span> h<span class="emp3"><span>aunted</span></span> state is right on--I was scared out of my wits!
</p>
</div>

<div id='memhook-button-bar'>
<a href="vaunted#" id="add-public-hook" style="" url="https://membean.com/mywords/vaunted/memory_hooks">Use other public hook</a>
<a href="vaunted#" id="memhook-use-own" url="https://membean.com/mywords/vaunted/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='vaunted#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Freedom is that great <b>vaunted</b> word that's always applied to our country - and rightly so.
<span class='attribution'>&mdash; Hampton Sides — American historian, author, and journalist</span>
<img alt="Hampton sides — american historian, author, and journalist" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Hampton Sides — American historian, author, and journalist.jpg?qdep8" width="80" />
</li>
<li>
"Scream"[:] The much-<b>vaunted</b> collaboration between Michael and sister Janet was viewed by some as a disappointment when it was unveiled in 1995 — bolstered by what was, at the time, the most expensive music video ever shot.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
<li>
Apple’s <b>vaunted</b> wall of secrecy was smashed wide open when one of its developers lost a next-generation iPhone prototype sometime in March at a bar in Redwood City, California.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/vaunted/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='vaunted#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='van_empty' data-tree-url='//cdn1.membean.com/public/data/treexml' href='vaunted#'>
<span class=''></span>
van
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>empty, hollow, illusory</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ed_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='vaunted#'>
<span class=''></span>
-ed
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>having a particular state</td>
</tr>
</table>
<p>Something <em>vaunted</em>, while supposedly having great qualities, eventually proves to be &#8220;illusory, hollow, or empty&#8221; once its true nature has been revealed.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='vaunted#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Star Trek Voyager</strong><span> Janeway references the vaunted Q Continuum, a species that thinks very highly of itself.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/vaunted.jpg' video_url='examplevids/vaunted' video_width='350'></span>
<div id='wt-container'>
<img alt="Vaunted" height="288" src="https://cdn1.membean.com/video/examplevids/vaunted.jpg" width="350" />
<div class='center'>
<a href="vaunted#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='vaunted#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Vaunted" src="https://cdn2.membean.com/public/images/wordimages/cons2/vaunted.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='vaunted#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>audacious</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>bombastic</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>braggadocio</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bravado</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>bumptious</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>exorbitant</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>flamboyant</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>florid</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>fulsome</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>grandiloquent</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>hyperbole</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>inordinate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>ornate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>ostentatious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>turgid</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>voluble</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='5' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>demure</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>diffident</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>laconic</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>politic</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>skittish</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='vaunted#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
vaunt
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>brag </td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="vaunted" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>vaunted</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="vaunted#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

