
<!DOCTYPE html>
<html>
<head>
<title>Word: astringent | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you take an acerbic tone with someone, you are criticizing them in a clever but critical and mean way.</p>
<p class='rw-defn idx1' style='display:none'>An acrid smell or taste is strong, unpleasant, and stings your nose or throat.</p>
<p class='rw-defn idx2' style='display:none'>An acrimonious meeting or discussion is bitter, resentful, and filled with angry contention.</p>
<p class='rw-defn idx3' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx4' style='display:none'>An aspersion is an unkind remark or unfair judgment attacking someone&#8217;s character or reputation.</p>
<p class='rw-defn idx5' style='display:none'>A chivalrous man behaves in a polite, kind, generous, and honorable way, especially towards women.</p>
<p class='rw-defn idx6' style='display:none'>If someone is granted clemency, they are punished less severely than they could have been—they have been shown mercy.</p>
<p class='rw-defn idx7' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx8' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx9' style='display:none'>A man who is debonair wears fashionable clothes and is sophisticated, charming, friendly, and confident.</p>
<p class='rw-defn idx10' style='display:none'>Someone who is dour is serious, stubborn, and unfriendly; they can also be gloomy.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is exacting expects others to work very hard and carefully.</p>
<p class='rw-defn idx12' style='display:none'>If you hurl invective at another person, you are verbally abusing or highly criticizing them.</p>
<p class='rw-defn idx13' style='display:none'>Something rancid is way past fresh; in fact, it is decomposing quickly and has a bad taste or smell.</p>
<p class='rw-defn idx14' style='display:none'>A strident person makes their feelings or opinions known in a forceful and strong way that often offends some people; not surprisingly, a strident voice is loud, harsh, and shrill.</p>
<p class='rw-defn idx15' style='display:none'>Trenchant comments or criticisms are expressed forcefully, directly, and clearly, even though they may be hurtful to the receiver.</p>
<p class='rw-defn idx16' style='display:none'>A truculent person is bad-tempered, easily annoyed, and prone to arguing excessively.</p>
<p class='rw-defn idx17' style='display:none'>To come out of something unscathed is to come out of it uninjured.</p>
<p class='rw-defn idx18' style='display:none'>When you have a vehement feeling about something, you feel very strongly or intensely about it.</p>
<p class='rw-defn idx19' style='display:none'>A virulent disease is very dangerous and spreads very quickly.</p>
<p class='rw-defn idx20' style='display:none'>Vitriolic words are bitter, unkind, and mean-spirited.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>astringent</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='astringent#' id='pronounce-sound' path='audio/words/amy-astringent'></a>
uh-STRIN-juhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='astringent#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='astringent#' id='context-sound' path='audio/wordcontexts/brian-astringent'></a>
The film critic was famously <em>astringent</em> in her sharp opinions and criticisms towards new movies.  Many directors dreaded the release of her column because they knew that her words were sure to be harsh, biting, and <em>astringent</em>.  While the critic&#8217;s <em>astringent</em> and cutting remarks negatively influenced the opinions of audiences nationwide, her scarce compliments were highly sought after and could make a film famous.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When would you most likely use an <em>astringent</em> manner to address someone?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
If you felt a great amount of respect toward them and wanted to convey that.
</li>
<li class='choice '>
<span class='result'></span>
If you didn&#8217;t know them well and wanted to reveal as little as possible about yourself.
</li>
<li class='choice answer '>
<span class='result'></span>
If you were frustrated by their incompetence and wanted them to know it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='astringent#' id='definition-sound' path='audio/wordmeanings/amy-astringent'></a>
When you are <em>astringent</em> towards someone, you speak to or write about them in a critical and hurtful manner.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>harsh</em>
</span>
</span>
</div>
<a class='quick-help' href='astringent#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='astringent#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/astringent/memory_hooks/3458.json'></span>
<p>
<span class="emp0"><span>Tigh<span class="emp1"><span>ten</span></span>ing <span class="emp2"><span>String</span></span></span></span> I felt like a <span class="emp2"><span>string</span></span> was being tigh<span class="emp1"><span>ten</span></span>ed around my neck when I heard her critical, a<span class="emp2"><span>string</span></span><span class="emp1"><span>ent</span></span> remarks; I not only thought my writing had been great, but I was counting on selling it!
</p>
</div>

<div id='memhook-button-bar'>
<a href="astringent#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/astringent/memory_hooks">Use other hook</a>
<a href="astringent#" id="memhook-use-own" url="https://membean.com/mywords/astringent/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='astringent#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
[Anyone] at all familiar with the dour, <b>astringent</b> work of Handke knows that the least likely adjective to describe anything he touched would be "effervescent."
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
Disillusionment with the land of my birth has been my life’s constant, but an animosity this <b>astringent</b> is new.
<cite class='attribution'>
&mdash;
The New Yorker
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/astringent/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='astringent#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep5" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='a_towards' data-tree-url='//cdn1.membean.com/public/data/treexml' href='astringent#'>
<span class='common'></span>
a-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to, towards</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='string_draw' data-tree-url='//cdn1.membean.com/public/data/treexml' href='astringent#'>
<span class=''></span>
string
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>draw tight, tighten, confine</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ent_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='astringent#'>
<span class=''></span>
-ent
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p><em>Astringent</em> comments make a person feel constricted, as if he were in a &#8220;state or condition&#8221; of being &#8220;drawn tight, confined, or tightened&#8221; by the harsh words coming &#8220;towards&#8221; him.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='astringent#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep5" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>ABC News: Guy Fieri's New York Times Food Critic Review Slams New Eatery</strong><span> An astringent review.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/astringent.jpg' video_url='examplevids/astringent' video_width='350'></span>
<div id='wt-container'>
<img alt="Astringent" height="288" src="https://cdn1.membean.com/video/examplevids/astringent.jpg" width="350" />
<div class='center'>
<a href="astringent#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='astringent#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep5" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Astringent" src="https://cdn0.membean.com/public/images/wordimages/cons2/astringent.png?qdep5" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='astringent#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>acerbic</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>acrid</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>acrimonious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>aspersion</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>dour</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>exacting</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>invective</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>rancid</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>strident</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>trenchant</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>truculent</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>vehement</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>virulent</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>vitriolic</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>chivalrous</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>clemency</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>debonair</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>unscathed</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="astringent" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>astringent</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="astringent#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

