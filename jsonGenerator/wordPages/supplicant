
<!DOCTYPE html>
<html>
<head>
<title>Word: supplicant | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>You accost a stranger when you move towards them and speak in an unpleasant or threatening way.</p>
<p class='rw-defn idx1' style='display:none'>When you adjure someone to do something, you persuade, eagerly appeal, or solemnly order them to do it.</p>
<p class='rw-defn idx2' style='display:none'>Affluence is the state of having a lot of money and many possessions, granting one a high economic standard of living.</p>
<p class='rw-defn idx3' style='display:none'>If your behavior or manner is altruistic, you show you care more about other people and their interests than you care about yourself.</p>
<p class='rw-defn idx4' style='display:none'>Apathy is a lack of interest or unenthusiastic involvement in an activity; there is no effort to change or improve it at all.</p>
<p class='rw-defn idx5' style='display:none'>If you are beset by something, you are experiencing serious problems or dangers because of it.</p>
<p class='rw-defn idx6' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx7' style='display:none'>You cajole people by gradually persuading them to do something, usually by being very nice to them.</p>
<p class='rw-defn idx8' style='display:none'>A chivalrous man behaves in a polite, kind, generous, and honorable way, especially towards women.</p>
<p class='rw-defn idx9' style='display:none'>If someone is granted clemency, they are punished less severely than they could have been—they have been shown mercy.</p>
<p class='rw-defn idx10' style='display:none'>If you are contrite, you are very sorry or ashamed that you have done something bad.</p>
<p class='rw-defn idx11' style='display:none'>Someone does something in a disinterested way when they have no personal involvement or attachment to the action.</p>
<p class='rw-defn idx12' style='display:none'>If you exhort someone to do something, you try very hard to persuade them to do it.</p>
<p class='rw-defn idx13' style='display:none'>An exigent situation is urgent and demands a lot of attention, often in ways that are expensive in time, cost, and effort.</p>
<p class='rw-defn idx14' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx15' style='display:none'>Someone who is impecunious has very little money, especially over a long period of time.</p>
<p class='rw-defn idx16' style='display:none'>If you do something in a lackadaisical way, you do it carelessly and without putting much effort into it—thereby showing that you are not really interested in what you&#8217;re doing.</p>
<p class='rw-defn idx17' style='display:none'>Largess is the generous act of giving money or presents to a large number of people.</p>
<p class='rw-defn idx18' style='display:none'>If someone exhibits magnanimity towards another, they show them kindness and noble generosity, especially after defeating them in battle or after having been treated badly by them.</p>
<p class='rw-defn idx19' style='display:none'>A mendicant is a beggar who asks for money by day on the streets.</p>
<p class='rw-defn idx20' style='display:none'>A misanthrope is someone who hates and mistrusts people.</p>
<p class='rw-defn idx21' style='display:none'>A munificent person is extremely generous, especially with money.</p>
<p class='rw-defn idx22' style='display:none'>If you nettle someone, you irritate or annoy them.</p>
<p class='rw-defn idx23' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx24' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx25' style='display:none'>If something obtrudes, it becomes noticeable or attracts attention in a way that is not pleasant or welcome.</p>
<p class='rw-defn idx26' style='display:none'>Penury is the state of being extremely poor.</p>
<p class='rw-defn idx27' style='display:none'>Philanthropy is unselfish support in the form of donating money, work, or gifts to positive social purposes; philanthropy is also overall love for humans in general.</p>
<p class='rw-defn idx28' style='display:none'>A person who is solicitous behaves in a way that shows great concern about someone&#8217;s health, feelings, safety, etc.</p>
<p class='rw-defn idx29' style='display:none'>If you have a surfeit of something, you have much more than what you need.</p>
<p class='rw-defn idx30' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx31' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>
<p class='rw-defn idx32' style='display:none'>If you say someone&#8217;s behavior is unseemly, you disapprove of it because it is not in good taste or not suitable for a particular situation.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>supplicant</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='supplicant#' id='pronounce-sound' path='audio/words/amy-supplicant'></a>
SUHP-li-kuhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='supplicant#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='supplicant#' id='context-sound' path='audio/wordcontexts/brian-supplicant'></a>
The old beggar sat by the side of the path, a poor <em>supplicant</em> to all who passed his way.  With his empty, wooden bowl in front of his thin ankles, the poor <em>supplicant</em> reached out, asking travelers for money and food.  His heartfelt and needy <em>supplication</em> or request moved many compassionate souls to give the wretched beggar what they could as they walked by his dwelling place along the road.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>supplicant</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A college student who must attend an extra year to earn enough credits to graduate.
</li>
<li class='choice '>
<span class='result'></span>
A college student who writes essays for other students as a way to earn money.
</li>
<li class='choice answer '>
<span class='result'></span>
A college student who humbly asks a professor for a deadline extension on a paper.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='supplicant#' id='definition-sound' path='audio/wordmeanings/amy-supplicant'></a>
A <em>supplicant</em> is someone who humbly and respectfully asks for something from another who is powerful enough to grant the request.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>petitioner</em>
</span>
</span>
</div>
<a class='quick-help' href='supplicant#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='supplicant#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/supplicant/memory_hooks/3511.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Can't</span></span> <span class="emp3"><span>Supply</span></span></span></span>  A <span class="emp3"><span>suppli</span></span><span class="emp1"><span>cant</span></span> usually <span class="emp1"><span>can't</span></span> <span class="emp3"><span>supply</span></span> something for herself, and so must ask for it from someone who <span class="emp1"><span>can</span></span> <span class="emp3"><span>supply</span></span> it.
</p>
</div>

<div id='memhook-button-bar'>
<a href="supplicant#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/supplicant/memory_hooks">Use other hook</a>
<a href="supplicant#" id="memhook-use-own" url="https://membean.com/mywords/supplicant/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='supplicant#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
A <b>supplicant</b> climbs to the top floor of City Hall, appeals to a stony-eyed secretary for a session with Mayor Important, then gets deposited on a stiff sofa for a long, fidgety wait.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
"Poland no longer appears in the role of a <b>supplicant</b>, humbly admitted to the table," wrote Poland’s Trybuna daily in an editorial. "Now its position not only must be taken into account, it can also be made good use of."
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
As the team bus left the Providence Civic Center, several players noticed that a young girl, near tears at the prospect of not getting any autographs, had chased their motor coach for two blocks. The players ordered the bus stopped, piled dutifully out and didn’t resume their trip until this lone <b>supplicant</b> had been sated with signatures.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Finally the massive wooden door swings open, the <b>supplicant</b> crosses a cavernous stateroom and stands meekly before His Honor, who is sitting as serious and confident as a king 10 feet away in his plush high-back behind an acre of a desk.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/supplicant/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='supplicant#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='sup_under' data-tree-url='//cdn1.membean.com/public/data/treexml' href='supplicant#'>
<span class=''></span>
sup-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>under</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='plic_fold' data-tree-url='//cdn1.membean.com/public/data/treexml' href='supplicant#'>
<span class='common'></span>
plic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>fold, bend</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ant_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='supplicant#'>
<span class=''></span>
-ant
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p>A <em>supplicant</em> is in the &#8220;state or condition of bending or folding under&#8221; another person for his or her charity, in the sense of asking for aid on &#8220;bended&#8221; knees.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='supplicant#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>The Wizard of OZ</strong><span> The four supplicants have come to OZ to ask for the things they need.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/supplicant.jpg' video_url='examplevids/supplicant' video_width='350'></span>
<div id='wt-container'>
<img alt="Supplicant" height="288" src="https://cdn1.membean.com/video/examplevids/supplicant.jpg" width="350" />
<div class='center'>
<a href="supplicant#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='supplicant#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Supplicant" src="https://cdn0.membean.com/public/images/wordimages/cons2/supplicant.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='supplicant#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>accost</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>adjure</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>beset</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>cajole</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>contrite</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>exhort</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>exigent</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>impecunious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>mendicant</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>nettle</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>obtrude</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>penury</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>solicitous</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>unseemly</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>affluence</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>altruistic</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>apathy</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>chivalrous</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>clemency</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>disinterested</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>lackadaisical</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>largess</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>magnanimity</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>misanthrope</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>munificent</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>philanthropy</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>surfeit</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='supplicant#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
supplication
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>the act of making a humble appeal for help</td>
</tr>
<tr>
<td class='wordform'>
<span>
supplicate
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to make a humble appeal for help</td>
</tr>
<tr>
<td class='wordform'>
<span>
suppliant
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>one who makes a humble appeal for help</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="supplicant" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>supplicant</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="supplicant#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

