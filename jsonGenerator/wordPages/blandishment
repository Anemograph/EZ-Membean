
<!DOCTYPE html>
<html>
<head>
<title>Word: blandishment | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abase yourself, people respect you less because you act in a way that is beneath you; due to this behavior, you are lowered or reduced in rank, esteem, or reputation. </p>
<p class='rw-defn idx1' style='display:none'>Someone who has an abrasive manner is unkind and rude, wearing away at you in an irritating fashion.</p>
<p class='rw-defn idx2' style='display:none'>Adulation is praise and admiration for someone that includes more than they deserve, usually for the purposes of flattery.</p>
<p class='rw-defn idx3' style='display:none'>If someone beguiles you, you are charmed by and attracted to them; you can also be tricked or deceived by someone who beguiles you.</p>
<p class='rw-defn idx4' style='display:none'>You cajole people by gradually persuading them to do something, usually by being very nice to them.</p>
<p class='rw-defn idx5' style='display:none'>A cavalier person does not seem to care about rules, principles, or other people&#8217;s feelings, no matter how dire or serious a situation may be.</p>
<p class='rw-defn idx6' style='display:none'>Censure is written or verbal disapproval and harsh criticism of something that someone has done.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is contumacious is purposely stubborn, contrary, or disobedient.</p>
<p class='rw-defn idx8' style='display:none'>To decry something is to speak against it and find fault with it.</p>
<p class='rw-defn idx9' style='display:none'>If you behave with deference towards someone, you show them respect and accept their opinions or decisions, especially because they have an important position.</p>
<p class='rw-defn idx10' style='display:none'>To defile something holy or important is to do something to it or say something about it that is highly offensive to others.</p>
<p class='rw-defn idx11' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx12' style='display:none'>If you deprecate something, you disapprove of it strongly.</p>
<p class='rw-defn idx13' style='display:none'>If you treat someone with derision, you mock or make fun of them because you think they are stupid, unimportant, or useless.</p>
<p class='rw-defn idx14' style='display:none'>If you disparage someone or something, you say unpleasant words that show you have no respect for that person or thing.</p>
<p class='rw-defn idx15' style='display:none'>If you exhort someone to do something, you try very hard to persuade them to do it.</p>
<p class='rw-defn idx16' style='display:none'>If you inveigle somebody, you cleverly manipulate them—usually by flattery or persuasion—to do something for you that they don&#8217;t want to do.</p>
<p class='rw-defn idx17' style='display:none'>A lackey is a person who follows their superior&#8217;s orders so completely that they never openly question those commands.</p>
<p class='rw-defn idx18' style='display:none'>If you malign someone, you say unpleasant things about them to damage their reputation.</p>
<p class='rw-defn idx19' style='display:none'>A minion is an unimportant person who merely obeys the orders of someone else; they  usually perform boring tasks that require little skill.</p>
<p class='rw-defn idx20' style='display:none'>If someone is being obsequious, they are trying so hard to please someone that they lack sincerity in their actions towards that person.</p>
<p class='rw-defn idx21' style='display:none'>Someone who is obstreperous is noisy, unruly, and difficult to control.</p>
<p class='rw-defn idx22' style='display:none'>To pander is to cater or suck up to a person&#8217;s baser instincts in order to influence or exploit them in some way.</p>
<p class='rw-defn idx23' style='display:none'>If someone is <em>pilloried</em>, they are publicly criticized or ridiculed by a lot of people, especially in the media.</p>
<p class='rw-defn idx24' style='display:none'>If you receive a plaudit, you receive admiration, praise, and approval from someone.</p>
<p class='rw-defn idx25' style='display:none'>A recalcitrant animal or person is difficult to control and refuses to obey orders—even after stiff punishment.</p>
<p class='rw-defn idx26' style='display:none'>Refractory people deliberately don&#8217;t obey someone in authority and so are difficult to deal with or control.</p>
<p class='rw-defn idx27' style='display:none'>If you say that someone is servile, you don&#8217;t respect them because they are too obedient, too agreeable, and too willing to do anything for another person.</p>
<p class='rw-defn idx28' style='display:none'>Something that has a stigma is not socially acceptable; therefore, it has a strong feeling of shame or disgrace attached to it.</p>
<p class='rw-defn idx29' style='display:none'>If you are subservient, you are too eager and willing to do what other people want and often put your own wishes aside.</p>
<p class='rw-defn idx30' style='display:none'>If you behave in a supercilious way, you act as if you were more important or better than everyone else.</p>
<p class='rw-defn idx31' style='display:none'>Sycophants praise people in authority or those who have considerable influence in order to seek some favor from them in return.</p>
<p class='rw-defn idx32' style='display:none'>To taint is to give an undesirable quality that damages a person&#8217;s reputation or otherwise spoils something.</p>
<p class='rw-defn idx33' style='display:none'>If you traduce someone, you deliberately say hurtful and untrue things to damage their reputation.</p>
<p class='rw-defn idx34' style='display:none'>In the Middle Ages, a vassal was a worker who served a lord in exchange for land to live upon; today you are a vassal if you serve another and are largely controlled by them.</p>
<p class='rw-defn idx35' style='display:none'>If you vilify people, you write or say bad things about them in an attempt to make them unpopular.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>blandishment</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='blandishment#' id='pronounce-sound' path='audio/words/amy-blandishment'></a>
BLAN-dish-muhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='blandishment#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='blandishment#' id='context-sound' path='audio/wordcontexts/brian-blandishment'></a>
Although Flora&#8217;s <em>blandishments</em> seemed sincere, Phil did not trust her constant compliments of his artistic work.  Phil had been persuaded in the past by pretty <em>blandishments</em> to reveal his artistic secrets, and so he was being careful this time around.  Flora was upset that he should be so suspicious and think that her <em>blandishments</em>, while indeed designed to flatter him, were not at least a little bit true.  Flora resented the fact that he perceived her <em>blandishments</em> as purely a way of getting something from him.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When are <em>blandishments</em> useful?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
When you need to convince someone to do something for you.
</li>
<li class='choice '>
<span class='result'></span>
When you need to protect yourself from a possible attack.
</li>
<li class='choice '>
<span class='result'></span>
When you are writing a story and need to make it more descriptive.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='blandishment#' id='definition-sound' path='audio/wordmeanings/amy-blandishment'></a>
<em>Blandishments</em> are words or actions that are pleasant and complimentary, intended to persuade someone to do something via a use of flattery.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>flattery</em>
</span>
</span>
</div>
<a class='quick-help' href='blandishment#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='blandishment#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/blandishment/memory_hooks/3399.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Bland</span></span> <span class="emp2"><span>Dish</span></span> Doesn't Work</span></span>  A <span class="emp3"><span>bland</span></span> <span class="emp2"><span>dish</span></span> of unsalted, tasteless food isn't <span class="emp1"><span>meant</span></span> to be used as a <span class="emp3"><span>bland</span></span><span class="emp2"><span>ish</span></span><span class="emp1"><span>ment</span></span> because it won't flatter anyone's taste buds, and certainly won't help to make anyone pleased or particularly inclined to help you.
</p>
</div>

<div id='memhook-button-bar'>
<a href="blandishment#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/blandishment/memory_hooks">Use other hook</a>
<a href="blandishment#" id="memhook-use-own" url="https://membean.com/mywords/blandishment/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='blandishment#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
An outstanding football player, Rahe turned a deaf ear to the <b>blandishments</b> of college recruiters and decided to attend the academy instead, even though he had played only a few games of baseball in his life.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Administration officials insisted, though, that the tank sale had been agreed upon in advance of Hussein's visit and did not represent a <b>blandishment</b> held out by Carter in an effort to make Hussein more cooperative with the Camp David process.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Although much that was sought lay beyond his powers, the 40-year-old Mayor survived by <b>blandishment</b> and promise, scribbling his name on scraps of paper thrust forward by the crowd.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
He made the same decision as the Faithful Five, resisting the <b>blandishments</b> of the USA Hockey development program to be, well, a stay-at-home defenseman.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/blandishment/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='blandishment#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>bland</td>
<td>
&rarr;
</td>
<td class='meaning'>flattering, pleasant</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ish_do' data-tree-url='//cdn1.membean.com/public/data/treexml' href='blandishment#'>
<span class=''></span>
-ish
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to do something</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ment_quality' data-tree-url='//cdn1.membean.com/public/data/treexml' href='blandishment#'>
<span class=''></span>
-ment
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>quality, condition</td>
</tr>
</table>
<p>A <em>blandishment</em> is something you&#8217;ve &#8220;done&#8221; which is intended to be perceived as &#8220;flattering or pleasant&#8221; by another.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='blandishment#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Blandishment" src="https://cdn2.membean.com/public/images/wordimages/cons2/blandishment.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='blandishment#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abase</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>adulation</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>beguile</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>cajole</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>deference</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>exhort</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>inveigle</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>lackey</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>minion</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>obsequious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>pander</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>plaudit</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>servile</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>subservient</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>sycophant</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>vassal</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>abrasive</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>cavalier</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>censure</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>contumacious</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>decry</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>defile</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>deprecate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>derision</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>disparage</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>malign</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>obstreperous</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>pillory</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>recalcitrant</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>refractory</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>stigma</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>supercilious</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>taint</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>traduce</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>vilify</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='blandishment#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
blandish
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to flatter someone into doing something</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="blandishment" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>blandishment</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="blandishment#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

