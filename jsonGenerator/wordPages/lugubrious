
<!DOCTYPE html>
<html>
<head>
<title>Word: lugubrious | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Lugubrious-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/lugubrious-large.jpg?qdep8" />
</div>
<a href="lugubrious#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>A bereaved person is someone whose close friend or relative has recently died.</p>
<p class='rw-defn idx1' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx2' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx3' style='display:none'>When you cavort, you jump and dance around in a playful, excited, or physically lively way.</p>
<p class='rw-defn idx4' style='display:none'>Someone who is crestfallen is severely disappointed, sad, or depressed.</p>
<p class='rw-defn idx5' style='display:none'>A dirge is a slow and sad piece of music often performed at funerals.</p>
<p class='rw-defn idx6' style='display:none'>Something that is dolorous, such as music or news, causes mental pain and sorrow because it itself is full of grief and sorrow.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is dour is serious, stubborn, and unfriendly; they can also be gloomy.</p>
<p class='rw-defn idx8' style='display:none'>Weather that is dreary tends to be depressing and gloomy; a situation or person that is dreary tends to be boring or uninteresting.</p>
<p class='rw-defn idx9' style='display:none'>When something is droll, it is humorous in an odd way.</p>
<p class='rw-defn idx10' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx11' style='display:none'>When you are ecstatic about something, you are overjoyed or extremely happy about it.</p>
<p class='rw-defn idx12' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx13' style='display:none'>An elegy is a poem or other piece of writing expressing sadness; it is often about someone who has died.</p>
<p class='rw-defn idx14' style='display:none'>A state of euphoria is one of extreme happiness or overwhelming joy.</p>
<p class='rw-defn idx15' style='display:none'>If you exult, you show great pleasure and excitement, especially about something you have achieved.</p>
<p class='rw-defn idx16' style='display:none'>If someone gambols, they run, jump, and play like a young child or animal.</p>
<p class='rw-defn idx17' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is inconsolable has been so devastated by a terrible event that no one can help them feel better about it.</p>
<p class='rw-defn idx19' style='display:none'>Someone who is jovial is in a good humor, lighthearted, and jolly.</p>
<p class='rw-defn idx20' style='display:none'>If someone is lachrymose, they tend to cry often; if something, such as a song, is lachrymose, it tends to cause tears because it is so sad.</p>
<p class='rw-defn idx21' style='display:none'>If you are melancholy, you look and feel sad.</p>
<p class='rw-defn idx22' style='display:none'>Someone who is morose is unhappy, bad-tempered, and unwilling to talk very much.</p>
<p class='rw-defn idx23' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx24' style='display:none'>A plaintive sound or voice expresses sadness.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is saturnine is looking miserable and sad, sometimes in a threatening or unfriendly way.</p>
<p class='rw-defn idx26' style='display:none'>A stoic person does not show their emotions and does not complain when bad things happen to them.</p>
<p class='rw-defn idx27' style='display:none'>Someone who is woebegone is very sad and filled with grief.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>lugubrious</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='lugubrious#' id='pronounce-sound' path='audio/words/amy-lugubrious'></a>
loo-GOO-bree-uhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='lugubrious#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='lugubrious#' id='context-sound' path='audio/wordcontexts/brian-lugubrious'></a>
The music at the memorial service for our neighbor Ned was so <em>lugubrious</em> or sad that mourners had tears in their eyes.  Although funeral services are often emotionally difficult, the heartfelt remembrances of Ned&#8217;s friends and family made us all so full of woe or so <em>lugubrious</em> that we could not listen without weeping.  The death of a loved one is always a sorrowful event, but I thought that this service was particularly gloomy and <em>lugubrious</em>.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When is something <em>lugubrious</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
When it is immensely mournful.
</li>
<li class='choice '>
<span class='result'></span>
When it is extremely long.
</li>
<li class='choice '>
<span class='result'></span>
When it honors the death of a beloved.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='lugubrious#' id='definition-sound' path='audio/wordmeanings/amy-lugubrious'></a>
If someone is <em>lugubrious</em>, they are looking very sad or gloomy.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>very mournful</em>
</span>
</span>
</div>
<a class='quick-help' href='lugubrious#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='lugubrious#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/lugubrious/memory_hooks/6268.json'></span>
<p>
<span class="emp0"><span>Say <span class="emp3"><span>Lugu</span></span>brious</span></span> Doesn't just slowly saying the word <span class="emp3"><span>LOO</span></span> <span class="emp3"><span>GOO</span></span> BREE US make you feel <span class="emp3"><span>GLOO</span></span>my?
</p>
</div>

<div id='memhook-button-bar'>
<a href="lugubrious#" id="add-public-hook" style="" url="https://membean.com/mywords/lugubrious/memory_hooks">Use other public hook</a>
<a href="lugubrious#" id="memhook-use-own" url="https://membean.com/mywords/lugubrious/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='lugubrious#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
I'm afraid that the act of writing is so scary and anxiety-filled that I never laugh at all. In fact, when people tell me that such and such a scene or story is comical, I tend to gape. I did not intend comedy—ever, as far as I know. It's probably all a mistake. I am essentially a <b>lugubrious</b> writer. Ha ha!
<span class='attribution'>&mdash; Cynthia Ozick, American writer</span>
<img alt="Cynthia ozick, american writer" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Cynthia Ozick, American writer.jpg?qdep8" width="80" />
</li>
<li>
Not a single story works tonight as the episode lurches between forced attempts at humor and dramatic scenes that are <b>lugubrious</b> when not ridiculous.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
The story is too slender for its two-hour running time, and the pace is <b>lugubrious</b>, as though everyone in front and behind the camera were depressed.
<cite class='attribution'>
&mdash;
San Francisco Chronicle
</cite>
</li>
<li>
Though some may have been chased away by the <b>lugubrious</b> melodies and lackluster staging, others no doubt settled on an escape route after realizing they'd been, sort of, er, hoodwinked. Here's a thought: Why not just call an opera an opera?
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/lugubrious/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='lugubrious#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>lug</td>
<td>
&rarr;
</td>
<td class='meaning'>mourn, lament</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ious_nature' data-tree-url='//cdn1.membean.com/public/data/treexml' href='lugubrious#'>
<span class=''></span>
-ious
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of the nature of</td>
</tr>
</table>
<p>Someone who is <em>lugubrious</em> is &#8220;mournful&#8221; or &#8220;lamenting.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='lugubrious#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Lugubrious" src="https://cdn0.membean.com/public/images/wordimages/cons2/lugubrious.png?qdep6" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='lugubrious#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>bereaved</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>crestfallen</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>dirge</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>dolorous</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>dour</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>dreary</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>elegy</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>inconsolable</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>lachrymose</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>melancholy</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>morose</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>plaintive</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>saturnine</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>woebegone</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>cavort</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>droll</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>ecstatic</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>euphoria</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>exult</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>gambol</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>jovial</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>stoic</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="lugubrious" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>lugubrious</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="lugubrious#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

