
<!DOCTYPE html>
<html>
<head>
<title>Word: intrusive | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you act in an aloof fashion towards others, you purposely do not participate in activities with them; instead, you remain distant and detached.</p>
<p class='rw-defn idx1' style='display:none'>If you are amenable to doing something, you willingly accept it without arguing.</p>
<p class='rw-defn idx2' style='display:none'>An audacious person acts with great daring and sometimes reckless bravery—despite risks and warnings from other people—in order to achieve something.</p>
<p class='rw-defn idx3' style='display:none'>If you describe a person&#8217;s behavior as brazen, you mean that they are not embarrassed by anything they do—they simply don&#8217;t care what other people think about them.</p>
<p class='rw-defn idx4' style='display:none'>When you broach a subject, especially one that may be embarrassing or unpleasant, you mention it in order to begin a discussion about it.</p>
<p class='rw-defn idx5' style='display:none'>Bumptious people are annoying because they are too proud of their abilities or opinions—they are full of themselves.</p>
<p class='rw-defn idx6' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx7' style='display:none'>A man who is debonair wears fashionable clothes and is sophisticated, charming, friendly, and confident.</p>
<p class='rw-defn idx8' style='display:none'>If you describe someone, usually a young woman, as demure, you mean that she is quiet, shy, and always behaves modestly.</p>
<p class='rw-defn idx9' style='display:none'>Effrontery is very rude behavior that shows a great lack of respect and is often insulting.</p>
<p class='rw-defn idx10' style='display:none'>When you are guilty of encroachment, you intrude upon or invade another person&#8217;s private space.</p>
<p class='rw-defn idx11' style='display:none'>An action that is flagrant shows that someone does not care if they obviously break the rules or highly offend people.</p>
<p class='rw-defn idx12' style='display:none'>When someone flaunts their good looks, they show them off or boast about them in a very proud and shameless way.</p>
<p class='rw-defn idx13' style='display:none'>If you foist a job or task upon someone, you force them to deal with or experience it despite the fact that it is undesirable.</p>
<p class='rw-defn idx14' style='display:none'>Something that is imperceptible is either impossible to be perceived by the mind or is very difficult to perceive.</p>
<p class='rw-defn idx15' style='display:none'>If someone behaves in an impertinent way, they behave rudely and disrespectfully.</p>
<p class='rw-defn idx16' style='display:none'>If something impinges on you, it affects you in some negative way.</p>
<p class='rw-defn idx17' style='display:none'>An imposition is giving someone an additional duty or extra work that is not welcomed by that person.</p>
<p class='rw-defn idx18' style='display:none'>Something that is inconspicuous does not attract attention and is not easily seen or noticed because it is small or ordinary.</p>
<p class='rw-defn idx19' style='display:none'>An incursion is an unpleasant intrusion, such as a sudden hostile attack or a land being flooded.</p>
<p class='rw-defn idx20' style='display:none'>When a spy infiltrates enemy lines, they creep in or penetrate them so as to gather information.</p>
<p class='rw-defn idx21' style='display:none'>To infringe on another person&#8217;s rights is to violate or intrude upon those rights.</p>
<p class='rw-defn idx22' style='display:none'>To interject is to insert a comment during a conversation that interrupts its flow.</p>
<p class='rw-defn idx23' style='display:none'>An interloper is someone who barges into a place where they are not welcome and interferes with what is going on, often for personal gain.</p>
<p class='rw-defn idx24' style='display:none'>When you interpose, you interrupt or interfere in some fashion.</p>
<p class='rw-defn idx25' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx26' style='display:none'>When you jostle another person, you bump against or push them, usually because you are in a crowded area.</p>
<p class='rw-defn idx27' style='display:none'>An officious person acts in a self-important manner; therefore, they are very eager to offer unwanted advice or services—which makes them annoying.</p>
<p class='rw-defn idx28' style='display:none'>Someone is overweening when they are not modest; rather, they think way too much of themselves and let everyone know about it.</p>
<p class='rw-defn idx29' style='display:none'>To be politic in a decision is to be socially wise and diplomatic.</p>
<p class='rw-defn idx30' style='display:none'>When you are presumptuous, you act improperly, rudely, or without respect, especially while attempting to do something that is not socially acceptable or that you are not qualified to do.</p>
<p class='rw-defn idx31' style='display:none'>Something that protrudes is sticking or pushing outward from something else.</p>
<p class='rw-defn idx32' style='display:none'>The salient qualities of an issue or feature are those that are most important and noticeable.</p>
<p class='rw-defn idx33' style='display:none'>If you sidle, you walk slowly, cautiously and often sideways in a particular direction, usually because you do not want to be noticed.</p>
<p class='rw-defn idx34' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx35' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>intrusive</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='intrusive#' id='pronounce-sound' path='audio/words/amy-intrusive'></a>
in-TROO-siv
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='intrusive#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='intrusive#' id='context-sound' path='audio/wordcontexts/brian-intrusive'></a>
We found the wait staff at the fancy restaurant to be <em>intrusive</em> or annoyingly attentive as they kept on asking us questions.  Just as we were getting our conversation going, they were right there with water, bread, fresh flowers, and finger towels, and way too many <em>intrusive</em> questions that intruded upon our privacy.  As soon as one would leave, another would show up and <em>intrusively</em> ask if we needed anything, thus continuing to interfere with our dinner.  It seemed we had no privacy, and I wondered why we were paying so much money to eat in such an <em>intrusive</em> environment where people kept bothering us.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What might an <em>intrusive</em> person say?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
&#8220;I hope you don&#8217;t mind that I invited myself along on your date tonight.&#8221;
</li>
<li class='choice '>
<span class='result'></span>
&#8220;I can&#8217;t believe you think you can draw because you&#8217;re terrible!&#8221;
</li>
<li class='choice '>
<span class='result'></span>
&#8220;I can&#8217;t wait to fill you in on every detail of my trip to the beach.&#8221;
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='intrusive#' id='definition-sound' path='audio/wordmeanings/amy-intrusive'></a>
An <em>intrusive</em> person intrudes, butts in, or interferes where they are not welcome.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>intruding</em>
</span>
</span>
</div>
<a class='quick-help' href='intrusive#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='intrusive#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/intrusive/memory_hooks/5258.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Intru</span></span>de<span class="emp1"><span>s</span></span> F<span class="emp2"><span>ive</span></span> Times</span></span> We had <span class="emp1"><span>intrus</span></span><span class="emp2"><span>ive</span></span> salesmen <span class="emp1"><span>intru</span></span>de f<span class="emp2"><span>ive</span></span> times during our Thanksgiving dinner this year!  We shouldn't have answered the phone or the door!
</p>
</div>

<div id='memhook-button-bar'>
<a href="intrusive#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/intrusive/memory_hooks">Use other hook</a>
<a href="intrusive#" id="memhook-use-own" url="https://membean.com/mywords/intrusive/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='intrusive#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
"With scuba gear, it's so noisy and <b>intrusive</b>, you're just a visitor," says Skiles. "But free-diving is so quiet, you feel like you're part of the ocean."
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
The U.S. Supreme Court has upheld the government's right to conduct broad and sometimes <b>intrusive</b> background checks on employees working for government contractors in nonsensitive, low-risk jobs. . . . The scientists noted that their jobs did not give them access to classified information and contended that these open-ended background questions were so unnecessarily <b>intrusive</b> as to be a violation of their right to privacy.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Prince Harry and Meghan Markle filed a legal complaint alleging that paparazzi have used “<b>intrusive”</b> means like drones and helicopters to get photographs of them and their 1-year-old son, Archie, at home since they moved to Los Angeles, a signal the intense press scrutiny that contributed to them leaving the U.K. seems to have followed them stateside.
<cite class='attribution'>
&mdash;
Forbes
</cite>
</li>
<li>
American and foreign firms alike see Sarbanes-Oxley [Act] which was passed in the wake of the Enron scandal, as <b>intrusive</b>, expensive and heavy-handed. Critics accuse it of causing foreign firms to list their shares in London rather than New York; others whinge about its onerous rules on internal controls used for financial reporting.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/intrusive/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='intrusive#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='in_in' data-tree-url='//cdn1.membean.com/public/data/treexml' href='intrusive#'>
<span class='common'></span>
in-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>in, into</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='trus_thrusted' data-tree-url='//cdn1.membean.com/public/data/treexml' href='intrusive#'>
<span class=''></span>
trus
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thrust, push</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ive_does' data-tree-url='//cdn1.membean.com/public/data/treexml' href='intrusive#'>
<span class=''></span>
-ive
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or that which does something</td>
</tr>
</table>
<p>An <em>intrusive</em> person &#8220;thrusts himself into&#8221; other people&#8217;s business.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='intrusive#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Today Show</strong><span> Kanye West's intrusive behavior during the 2009 Music Video Awards ceremony impacted artists Beyonce and Taylor Swift, who both advocate for racial and gender justice and equity.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/intrusive.jpg' video_url='examplevids/intrusive' video_width='350'></span>
<div id='wt-container'>
<img alt="Intrusive" height="288" src="https://cdn1.membean.com/video/examplevids/intrusive.jpg" width="350" />
<div class='center'>
<a href="intrusive#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='intrusive#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Intrusive" src="https://cdn0.membean.com/public/images/wordimages/cons2/intrusive.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='intrusive#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>audacious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>brazen</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>broach</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>bumptious</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>effrontery</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>encroachment</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>flagrant</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>flaunt</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>foist</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>impertinent</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>impinge</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>imposition</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>incursion</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>infiltrate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>infringe</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>interject</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>interloper</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>interpose</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>jostle</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>officious</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>overweening</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>presumptuous</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>protrude</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>salient</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>aloof</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>amenable</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>debonair</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>demure</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>imperceptible</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>inconspicuous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>politic</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>sidle</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="intrusive" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>intrusive</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="intrusive#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

