
<!DOCTYPE html>
<html>
<head>
<title>Word: consternation | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you abash someone, you make them feel uncomfortable, ashamed, embarrassed, or inferior.</p>
<p class='rw-defn idx1' style='display:none'>If someone is addled by something, they are confused by it and unable to think properly.</p>
<p class='rw-defn idx2' style='display:none'>An audacious person acts with great daring and sometimes reckless bravery—despite risks and warnings from other people—in order to achieve something.</p>
<p class='rw-defn idx3' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx4' style='display:none'>If you describe a person&#8217;s behavior as brazen, you mean that they are not embarrassed by anything they do—they simply don&#8217;t care what other people think about them.</p>
<p class='rw-defn idx5' style='display:none'>Complacent persons are too confident and relaxed because they think that they can deal with a situation easily; however, in many circumstances, this is not the case.</p>
<p class='rw-defn idx6' style='display:none'>If something confounds you, it makes you feel surprised and confused, often because it does not meet your predefined expectations.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is craven is very cowardly.</p>
<p class='rw-defn idx8' style='display:none'>If something daunts you, it makes you worried about dealing with it because you think it will be very difficult or dangerous.</p>
<p class='rw-defn idx9' style='display:none'>If something discomfits you, it makes you feel embarrassed, confused, uncomfortable, or frustrated.</p>
<p class='rw-defn idx10' style='display:none'>If something disconcerts you, it makes you feel anxious, worried, or confused.</p>
<p class='rw-defn idx11' style='display:none'>When someone feels disquiet about a situation, they feel very worried or nervous.</p>
<p class='rw-defn idx12' style='display:none'>If you are distraught about a situation, you are very upset or worried about it.</p>
<p class='rw-defn idx13' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx14' style='display:none'>A harrowing experience is highly distressing, terrifying, or very disturbing.</p>
<p class='rw-defn idx15' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx16' style='display:none'>An intrepid person is willing to do dangerous things or go to dangerous places because they are brave.</p>
<p class='rw-defn idx17' style='display:none'>Malaise is a feeling of discontent, general unease, or even illness in a person or group for which there does not seem to be a quick and easy solution because the cause of it is not clear.</p>
<p class='rw-defn idx18' style='display:none'>If you mollify someone, you say or do something to make that person feel less angry or upset.</p>
<p class='rw-defn idx19' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx20' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx21' style='display:none'>To be pusillanimous is to be cowardly.</p>
<p class='rw-defn idx22' style='display:none'>To quaver is to shake or tremble, especially when speaking.</p>
<p class='rw-defn idx23' style='display:none'>A state of quiescence is one of quiet and restful inaction.</p>
<p class='rw-defn idx24' style='display:none'>If you are sanguine about a situation, especially a difficult one, you are confident and cheerful that everything will work out the way you want it to.</p>
<p class='rw-defn idx25' style='display:none'>A serene place or situation is peaceful and calm.</p>
<p class='rw-defn idx26' style='display:none'>To be timorous is to be fearful.</p>
<p class='rw-defn idx27' style='display:none'>If something is tranquil, it is peaceful, calm, and quiet.</p>
<p class='rw-defn idx28' style='display:none'>Someone is tremulous when they are shaking slightly from nervousness or fear; they may also simply be afraid of something.</p>
<p class='rw-defn idx29' style='display:none'>Trepidation is fear or uneasiness about something that is going to happen.</p>
<p class='rw-defn idx30' style='display:none'>A tumultuous event or period of time is filled with great excitement, confusion, or violence; a tumultuous reaction to something is likewise very loud and noisy because people are happy and excited.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>consternation</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='consternation#' id='pronounce-sound' path='audio/words/amy-consternation'></a>
kon-ster-NAY-shuhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='consternation#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='consternation#' id='context-sound' path='audio/wordcontexts/brian-consternation'></a>
I was filled with <em>consternation</em> and alarmed concern when my brother Phil told me that he was dropping out of college.  Phil has always been such a focused and successful student that I felt immense confusion and <em>consternation</em> at his sudden decision to leave.  It turns out that his girlfriend is moving, so he is determined to go to California with her, much to my shocked parents&#8217; <em>consternation</em> at this unexpected development.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a situation that might cause <em>consternation</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A tornado is approaching your house.
</li>
<li class='choice '>
<span class='result'></span>
A hard, well-considered decision has finally been made.
</li>
<li class='choice '>
<span class='result'></span>
Feeling just a little too full after a holiday celebration.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='consternation#' id='definition-sound' path='audio/wordmeanings/amy-consternation'></a>
<em>Consternation</em> is the feeling of anxiety or fear, sometimes paralyzing in its effect, and often caused by something unexpected that has happened.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>considerable alarm</em>
</span>
</span>
</div>
<a class='quick-help' href='consternation#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='consternation#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/consternation/memory_hooks/5927.json'></span>
<p>
<img alt="Consternation" src="https://cdn1.membean.com/public/images/wordimages/hook/consternation.jpg?qdep8" />
<span class="emp0"><span><span class="emp2"><span>Stern</span></span> N<span class="emp1"><span>ation</span></span></span></span> The <span class="emp2"><span>stern</span></span> dictator of my n<span class="emp1"><span>ation</span></span> causes con<span class="emp2"><span>stern</span></span><span class="emp1"><span>ation</span></span> in my n<span class="emp1"><span>ation</span></span>'s people because he is so horribly and dreadfully <span class="emp2"><span>stern</span></span> in his actions.
</p>
</div>

<div id='memhook-button-bar'>
<a href="consternation#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/consternation/memory_hooks">Use other hook</a>
<a href="consternation#" id="memhook-use-own" url="https://membean.com/mywords/consternation/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='consternation#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Americans using expressions like “You are killing me” or “Say that once again and I’ll walk away from this deal” will cause great <b>consternation</b> among their Japanese partners.
<span class='attribution'>&mdash; Richard D. Lewis, English communications consultant and writer, from _When Cultures Collide_</span>
<img alt="Richard d" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Richard D. Lewis, English communications consultant and writer, from _When Cultures Collide_.jpg?qdep8" width="80" />
</li>
<li>
For reasons that Mr Willetts could not quite fathom at the time, a speech he was making in the Commons began to cause visible <b>consternation</b> not only to Mr Blunkett himself but also to the whole Labour front bench. . . . The <b>consternation</b> opposite had been caused not by Mr Willetts's debating shafts but by the fact that Mr Blunkett's guide dog had just been sick on the Commons floor.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
The wordless [Spanish national] anthem has often caused <b>consternation</b> among onlookers from other nations at international events such as soccer matches and Olympics because all Spaniards can do is hum along to its tune. "It gives me a very odd feeling that people should sing 'La, la, la,' or 'Chunda, chunda, chunda,'" said Alejandro Blanco, president of Spain's Olympic Committee.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
The idea that Stan may wind up as a curio in someone’s home has been a persistent worry of paleontologists as it seems that every year another significant skeleton goes to auction. . . . To the <b>consternation</b> of paleontologists, the sale of Stan and the recent Discovery Channel show _Dino Hunters_ are highlighting the price tag of fossils once again.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/consternation/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='consternation#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='con_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='consternation#'>
<span class='common'></span>
con-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td class='partform'>stern</td>
<td>
&rarr;
</td>
<td class='meaning'>knock down, strike down</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ation_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='consternation#'>
<span class=''></span>
-ation
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state or quality of</td>
</tr>
</table>
<p>A person suffering from <em>consternation</em> has the &#8220;state or condition&#8221; of having been &#8220;thoroughly knocked or struck down&#8221; with fear or alarm.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='consternation#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Consternation" src="https://cdn2.membean.com/public/images/wordimages/cons2/consternation.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='consternation#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abash</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>addle</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>confound</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>craven</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>daunt</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>discomfit</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>disconcert</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>disquiet</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>distraught</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>harrowing</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>malaise</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>pusillanimous</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>quaver</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>timorous</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>tremulous</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>trepidation</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>tumultuous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>audacious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>brazen</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>complacent</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>intrepid</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>mollify</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>quiescence</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>sanguine</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>serene</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>tranquil</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='consternation#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
consternate
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to distress or alarm</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="consternation" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>consternation</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="consternation#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

