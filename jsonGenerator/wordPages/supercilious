
<!DOCTYPE html>
<html>
<head>
<title>Word: supercilious | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Someone who has an abrasive manner is unkind and rude, wearing away at you in an irritating fashion.</p>
<p class='rw-defn idx1' style='display:none'>If you take an acerbic tone with someone, you are criticizing them in a clever but critical and mean way.</p>
<p class='rw-defn idx2' style='display:none'>An acrimonious meeting or discussion is bitter, resentful, and filled with angry contention.</p>
<p class='rw-defn idx3' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx4' style='display:none'>Amity is a peaceful friendship between two parties, particularly between two countries.</p>
<p class='rw-defn idx5' style='display:none'>An autocratic person rules with complete power; consequently, they make decisions and give orders to people without asking them for their opinion or advice.</p>
<p class='rw-defn idx6' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx7' style='display:none'>When a person is speaking or writing in a bombastic fashion, they are very self-centered, extremely showy, and excessively proud.</p>
<p class='rw-defn idx8' style='display:none'>Braggadocio is an excessively proud way of talking about your achievements or possessions that can annoy others.</p>
<p class='rw-defn idx9' style='display:none'>If you describe a person&#8217;s behavior or speech as brusque, you mean that they say or do things abruptly or too quickly in a somewhat rude and impolite way.</p>
<p class='rw-defn idx10' style='display:none'>Bumptious people are annoying because they are too proud of their abilities or opinions—they are full of themselves.</p>
<p class='rw-defn idx11' style='display:none'>A captious person has a fondness for catching others at fault; hence, they are overly critical and raise unwarranted objections too often.</p>
<p class='rw-defn idx12' style='display:none'>A cavalier person does not seem to care about rules, principles, or other people&#8217;s feelings, no matter how dire or serious a situation may be.</p>
<p class='rw-defn idx13' style='display:none'>To cavil is to make unnecessary complaints about things that are unimportant.</p>
<p class='rw-defn idx14' style='display:none'>If you commiserate with someone, you show them pity or sympathy because something bad or unpleasant has happened to them.</p>
<p class='rw-defn idx15' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx16' style='display:none'>When people condescend, they behave in ways that show that they are supposedly more important or intelligent than other people.</p>
<p class='rw-defn idx17' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx18' style='display:none'>A convivial atmosphere or occasion is friendly, pleasant, cheerful, and relaxed.</p>
<p class='rw-defn idx19' style='display:none'>If you behave with deference towards someone, you show them respect and accept their opinions or decisions, especially because they have an important position.</p>
<p class='rw-defn idx20' style='display:none'>If someone deigns to do something, they agree to do it—but in a way that shows that they are lowering or humbling themselves to do so or that it is a very great favor.</p>
<p class='rw-defn idx21' style='display:none'>You show disdain towards another person when you despise what they do, or you regard them as unworthy of your notice and attention.</p>
<p class='rw-defn idx22' style='display:none'>An egotistical person thinks about or is concerned with no one else other than themselves.</p>
<p class='rw-defn idx23' style='display:none'>Empathy is the ability to understand how people feel because you can imagine what it is to be like them.</p>
<p class='rw-defn idx24' style='display:none'>When someone flaunts their good looks, they show them off or boast about them in a very proud and shameless way.</p>
<p class='rw-defn idx25' style='display:none'>When a person is being flippant, they are not taking something as seriously as they should be; therefore, they tend to dismiss things they should respectfully pay attention to.</p>
<p class='rw-defn idx26' style='display:none'>If you flout a rule or custom, you deliberately refuse to conform to it.</p>
<p class='rw-defn idx27' style='display:none'>A gregarious person is friendly, highly social, and prefers being with people rather than being alone.</p>
<p class='rw-defn idx28' style='display:none'>Someone who is imperious behaves in a proud, overbearing, and highly confident manner that shows they expect to be obeyed without question.</p>
<p class='rw-defn idx29' style='display:none'>Someone is overweening when they are not modest; rather, they think way too much of themselves and let everyone know about it.</p>
<p class='rw-defn idx30' style='display:none'>If someone patronizes you, they talk or behave in a way that seems friendly; nevertheless, they also act as if they were more intelligent or important than you are.</p>
<p class='rw-defn idx31' style='display:none'>If you are pompous, you think that you are better than other people; therefore, you tend to show off and be highly egocentric.</p>
<p class='rw-defn idx32' style='display:none'>When someone pontificates, they give their opinions in a heavy-handed way that shows they think they are always right.</p>
<p class='rw-defn idx33' style='display:none'>When you are presumptuous, you act improperly, rudely, or without respect, especially while attempting to do something that is not socially acceptable or that you are not qualified to do.</p>
<p class='rw-defn idx34' style='display:none'>If you are pretentious, you think you are really great in some way and let everyone know about it, despite the fact that it&#8217;s not the case at all.</p>
<p class='rw-defn idx35' style='display:none'>To remonstrate with someone is to tell that person that you strongly disapprove of something they have said or done.</p>
<p class='rw-defn idx36' style='display:none'>Someone&#8217;s sardonic smile, expression, or comment shows a lack of respect for what someone else has said or done; this occurs because the former thinks they are too important to consider or discuss such a supposedly trivial matter of the latter.</p>
<p class='rw-defn idx37' style='display:none'>If someone is sententious, they are terse or brief in writing and speech; they also often use proverbs to appear morally upright and wise.</p>
<p class='rw-defn idx38' style='display:none'>If you are vainglorious, you are very proud of yourself and let other people know about it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>supercilious</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='supercilious#' id='pronounce-sound' path='audio/words/amy-supercilious'></a>
soo-per-SIL-ee-uhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='supercilious#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='supercilious#' id='context-sound' path='audio/wordcontexts/brian-supercilious'></a>
Lana was a <em>supercilious</em> or stuck-up young woman who believed that everyone should cater to her every wish.  She behaved in a rude, proud, and <em>supercilious</em> fashion towards her companions by leaving restaurant checks for them alone to pay.  Lana&#8217;s <em>supercilious</em>, self-centered, and superior airs annoyed so many that she soon was shunned by all of her acquaintances.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean when someone is described as <em>supercilious</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
They act as if they are superior to others.
</li>
<li class='choice '>
<span class='result'></span>
They are silently observing what&#8217;s going on around them.
</li>
<li class='choice '>
<span class='result'></span>
They are the most popular person in a group.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='supercilious#' id='definition-sound' path='audio/wordmeanings/amy-supercilious'></a>
If you behave in a <em>supercilious</em> way, you act as if you were more important or better than everyone else.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>pompous</em>
</span>
</span>
</div>
<a class='quick-help' href='supercilious#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='supercilious#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/supercilious/memory_hooks/5408.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Super</span></span>man's <span class="emp2"><span>Silly</span></span> for <span class="emp3"><span>Us</span></span>!</span></span> When I said that I liked <span class="emp1"><span>Super</span></span>man, Janie raised her eyebrow in a <span class="emp1"><span>super</span></span><span class="emp2"><span>cili</span></span>o<span class="emp3"><span>us</span></span> way and said to all our friends that <span class="emp1"><span>Super</span></span>man is <span class="emp2"><span>silly</span></span> for <span class="emp3"><span>us</span></span> and walked away, making me feel <span class="emp2"><span>silly</span></span> and depressed by her <span class="emp1"><span>super</span></span><span class="emp2"><span>cili</span></span>o<span class="emp3"><span>us</span></span> way of making me look bad.
</p>
</div>

<div id='memhook-button-bar'>
<a href="supercilious#" id="add-public-hook" style="" url="https://membean.com/mywords/supercilious/memory_hooks">Use other public hook</a>
<a href="supercilious#" id="memhook-use-own" url="https://membean.com/mywords/supercilious/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='supercilious#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
He was one of those <b>supercilious</b> striplings who give you the impression that you went to the wrong school and that your clothes don't fit.
<span class='attribution'>&mdash; P.G. Wodehouse, English author and humorist</span>
<img alt="P" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/P.G. Wodehouse, English author and humorist.jpg?qdep8" width="80" />
</li>
<li>
There’s also an extended cameo from Ryan Reynolds, an actor whose faintly <b>supercilious</b> good looks have graced innumerable dumb slacker comedies, including _Van Wilder_, _Harold & Kumar Go to White Castle_ and _Waiting._
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
Star Kelsey Grammer might not be the most polished singer, but his charming delivery of “Tossed Salads and Scrambled Eggs,” played over the closing credits, gives a show about two <b>supercilious</b> psychiatrists (Frasier also a talk radio star) a surprising Everyman bent.
<cite class='attribution'>
&mdash;
Atlanta Journal-Constitution
</cite>
</li>
<li>
Daniel Mooney as Louis gives a fascinating, if one-dimensional, portrait of a <b>supercilious</b> mock tyrant saved from silliness by a hint of self-ridicule.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/supercilious/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='supercilious#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='super_over' data-tree-url='//cdn1.membean.com/public/data/treexml' href='supercilious#'>
<span class='common'></span>
super-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>over, above</td>
</tr>
<tr>
<td class='partform'>cili</td>
<td>
&rarr;
</td>
<td class='meaning'>eyebrow</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ous_possessing' data-tree-url='//cdn1.membean.com/public/data/treexml' href='supercilious#'>
<span class=''></span>
-ous
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>possessing the nature of</td>
</tr>
</table>
<p>When someone is being <em>supercilious</em> in a conversation, he is raising his &#8220;eyebrow over&#8221; another in a superior attitude since he thinks he&#8217;s better than the person with whom he is conversing.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='supercilious#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Supercilious" src="https://cdn3.membean.com/public/images/wordimages/cons2/supercilious.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='supercilious#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abrasive</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>acerbic</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>acrimonious</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>autocratic</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>bombastic</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>braggadocio</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>brusque</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>bumptious</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>captious</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>cavalier</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>cavil</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>condescend</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>deign</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>disdain</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>egotistical</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>flaunt</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>flippant</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>flout</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>imperious</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>overweening</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>patronize</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>pompous</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>pontificate</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>presumptuous</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>pretentious</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>remonstrate</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>sardonic</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>sententious</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>vainglorious</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>amity</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>commiserate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>convivial</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>deference</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>empathy</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>gregarious</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="supercilious" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>supercilious</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="supercilious#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

