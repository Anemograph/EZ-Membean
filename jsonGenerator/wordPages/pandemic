
<!DOCTYPE html>
<html>
<head>
<title>Word: pandemic | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Pandemic-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/pandemic-large.jpg?qdep8" />
</div>
<a href="pandemic#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx2' style='display:none'>A contagion is a disease—or the transmission of that disease—that is easily spread from one person to another through touch or the air.</p>
<p class='rw-defn idx3' style='display:none'>A copious amount of something is a very large amount of it.</p>
<p class='rw-defn idx4' style='display:none'>Something that has curative properties can be used for curing people&#8217;s illnesses.</p>
<p class='rw-defn idx5' style='display:none'>When there is a dearth of something, there is a scarcity or lack of it.</p>
<p class='rw-defn idx6' style='display:none'>When you deplete a supply of something, you use it up or lessen its amount over a given period of time.</p>
<p class='rw-defn idx7' style='display:none'>When an area is devoid of life, it is empty or completely lacking in it.</p>
<p class='rw-defn idx8' style='display:none'>When something is diffuse, it is widely spread out and scattered.</p>
<p class='rw-defn idx9' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx10' style='display:none'>An exiguous amount of something is meager, small, or limited in nature.</p>
<p class='rw-defn idx11' style='display:none'>A gamut is a complete range of things of a particular type or the full extent of possibilities of some experience or action.</p>
<p class='rw-defn idx12' style='display:none'>Something infinitesimal is so extremely small or minute that it is very hard to measure it.</p>
<p class='rw-defn idx13' style='display:none'>If something is inordinate, it is much larger in amount or degree than is normally expected.</p>
<p class='rw-defn idx14' style='display:none'>A macrocosm is a large, complex, and organized system or structure that is made of many small parts that form one unit, such as a society or the universe.</p>
<p class='rw-defn idx15' style='display:none'>A malignant thing or person, such as a tumor or criminal, is deadly or does great harm.</p>
<p class='rw-defn idx16' style='display:none'>Something that is omnipresent appears to be everywhere at the same time or is ever-present.</p>
<p class='rw-defn idx17' style='display:none'>A pathogen is an organism, such as a bacterium or virus, that causes disease.</p>
<p class='rw-defn idx18' style='display:none'>If you have a pathological condition, you are extreme or unreasonable in something that you do.</p>
<p class='rw-defn idx19' style='display:none'>A paucity of something is not enough of it.</p>
<p class='rw-defn idx20' style='display:none'>When a substance permeates something, it enters into all parts of it.</p>
<p class='rw-defn idx21' style='display:none'>If something is pervasive, it appears to be everywhere.</p>
<p class='rw-defn idx22' style='display:none'>A pestilence is either an infectious disease that is deadly or an agent of some kind that is destructive or dangerous.</p>
<p class='rw-defn idx23' style='display:none'>A plethora of something is too much of it.</p>
<p class='rw-defn idx24' style='display:none'>A preponderance of things of a particular type in a group means that there are more of that type than of any other.</p>
<p class='rw-defn idx25' style='display:none'>A profusion of something is a very large quantity or variety of it.</p>
<p class='rw-defn idx26' style='display:none'>When people or animals are quarantined, they are put in isolation so that they will not spread disease.</p>
<p class='rw-defn idx27' style='display:none'>If something bad, such as crime or disease, is rampant, there is a lot of it—and it is increasing in a way that is difficult to control.</p>
<p class='rw-defn idx28' style='display:none'>To rarefy something is to make it less dense, as in oxygen at high altitudes; this word also refers to purifying or refining something, thereby making it less ordinary or commonplace.</p>
<p class='rw-defn idx29' style='display:none'>If you say that something bad or unpleasant is rife somewhere, you mean that it is very common.</p>
<p class='rw-defn idx30' style='display:none'>A salubrious place or area is pleasant, clean, healthy, and comfortable to live in.</p>
<p class='rw-defn idx31' style='display:none'>A scourge was originally a whip used for torture and now refers to something that torments or causes serious trouble or devastation.</p>
<p class='rw-defn idx32' style='display:none'>If something, such as warmth, color, or liquid, suffuses a material, it gradually spreads through or covers it; if you are suffused with a feeling, you are full of that feeling.</p>
<p class='rw-defn idx33' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx34' style='display:none'>If something is ubiquitous, it seems to be everywhere.</p>
<p class='rw-defn idx35' style='display:none'>A feeling that is unbridled is enthusiastic and unlimited in its expression.</p>
<p class='rw-defn idx36' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>pandemic</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='pandemic#' id='pronounce-sound' path='audio/words/amy-pandemic'></a>
pan-DEM-ik
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='pandemic#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='pandemic#' id='context-sound' path='audio/wordcontexts/brian-pandemic'></a>
The Black Death was a well-known <em>pandemic</em> disease that spread all throughout Europe in the 14th century.  This <em>pandemic</em> or universal plague affected people all over the continent, with a very high death rate.  The disease affected people in a <em>pandemic</em> or common fashion, sparing absolutely no one.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>pandemic</em> event?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
The devastating earthquake in Japan in 2011 that caused widespread destruction.
</li>
<li class='choice answer '>
<span class='result'></span>
The Spanish flu that killed millions of people worldwide in the early 1900s.
</li>
<li class='choice '>
<span class='result'></span>
The many species of animals that are slowly becoming endangered around the world.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='pandemic#' id='definition-sound' path='audio/wordmeanings/amy-pandemic'></a>
A <em>pandemic</em> disease is a far-reaching epidemic that affects people in a very wide geographic area.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>widespread</em>
</span>
</span>
</div>
<a class='quick-help' href='pandemic#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='pandemic#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/pandemic/memory_hooks/5159.json'></span>
<p>
<span class="emp0"><span>Epi<span class="emp3"><span>demic</span></span></span></span> A pan<span class="emp3"><span>demic</span></span> disease is a very widespread epi<span class="emp3"><span>demic</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="pandemic#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/pandemic/memory_hooks">Use other hook</a>
<a href="pandemic#" id="memhook-use-own" url="https://membean.com/mywords/pandemic/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='pandemic#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
There is no shortage of potential <b>pandemic</b> diseases, and the probability of a spillover event has not meaningfully changed since COVID-19 emerged in Wuhan, China.
<cite class='attribution'>
&mdash;
The Hill
</cite>
</li>
<li>
The last <b>pandemic</b> flu the world encountered—the swine flu outbreak of 2009—was less deadly than initially feared, largely because many older people had some immunity to it, probably because of its similarity to other flu viruses that had circulated years before.
<cite class='attribution'>
&mdash;
BBC News
</cite>
</li>
<li>
Meanwhile, food-access organizations are unsure how long they will need to maintain <b>pandemic</b> levels of support—or even how long they can, as private donations dry up and demand increases.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
High vaccination rates this flu season, especially among children and young adults, might even drive the <b>pandemic</b> bug to extinction, speculate top researchers at the National Institutes of Health.
<cite class='attribution'>
&mdash;
WebMD
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/pandemic/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='pandemic#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pan_all' data-tree-url='//cdn1.membean.com/public/data/treexml' href='pandemic#'>
<span class=''></span>
pan-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>all</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='dem_people' data-tree-url='//cdn1.membean.com/public/data/treexml' href='pandemic#'>
<span class=''></span>
dem
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>people</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ic_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='pandemic#'>
<span class=''></span>
-ic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>one relating to</td>
</tr>
</table>
<p>A <em>pandemic</em> is a disease &#8220;relating to all the people.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='pandemic#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Test Tube News</strong><span> The Zika virus is causing a pandemic.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/pandemic.jpg' video_url='examplevids/pandemic' video_width='350'></span>
<div id='wt-container'>
<img alt="Pandemic" height="288" src="https://cdn1.membean.com/video/examplevids/pandemic.jpg" width="350" />
<div class='center'>
<a href="pandemic#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='pandemic#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Pandemic" src="https://cdn1.membean.com/public/images/wordimages/cons2/pandemic.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='pandemic#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>contagion</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>copious</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>diffuse</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>gamut</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>inordinate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>macrocosm</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>malignant</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>omnipresent</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>pathogen</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>pathological</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>permeate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>pervasive</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>pestilence</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>plethora</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>preponderance</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>profusion</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>quarantine</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>rampant</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>rife</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>scourge</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>suffuse</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>ubiquitous</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>unbridled</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>curative</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>dearth</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>deplete</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>devoid</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>exiguous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>infinitesimal</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>paucity</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>rarefy</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>salubrious</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='pandemic#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
pandemic
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>widespread epidemic</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="pandemic" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>pandemic</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="pandemic#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

