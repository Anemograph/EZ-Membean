
<!DOCTYPE html>
<html>
<head>
<title>Word: diatribe | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Adulation is praise and admiration for someone that includes more than they deserve, usually for the purposes of flattery.</p>
<p class='rw-defn idx1' style='display:none'>Approbation is official praise or approval of something.</p>
<p class='rw-defn idx2' style='display:none'>Someone who has a bilious personality is highly irritable and bad-tempered.</p>
<p class='rw-defn idx3' style='display:none'>Someone who has a bristling personality is easily offended, annoyed, or angered.</p>
<p class='rw-defn idx4' style='display:none'>When you castigate someone, you criticize or punish them severely.</p>
<p class='rw-defn idx5' style='display:none'>To decry something is to speak against it and find fault with it.</p>
<p class='rw-defn idx6' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx7' style='display:none'>If you disparage someone or something, you say unpleasant words that show you have no respect for that person or thing.</p>
<p class='rw-defn idx8' style='display:none'>An elegy is a poem or other piece of writing expressing sadness; it is often about someone who has died.</p>
<p class='rw-defn idx9' style='display:none'>An encomium strongly praises someone or something via oral or written communication.</p>
<p class='rw-defn idx10' style='display:none'>A eulogy is a speech or other piece of writing, often part of a funeral, in which someone or something is highly praised.</p>
<p class='rw-defn idx11' style='display:none'>If you exhort someone to do something, you try very hard to persuade them to do it.</p>
<p class='rw-defn idx12' style='display:none'>If you expostulate with someone, you express strong disagreement with or disapproval of what that person is doing.</p>
<p class='rw-defn idx13' style='display:none'>If you extol something or someone, you praise it or them very enthusiastically.</p>
<p class='rw-defn idx14' style='display:none'>If you fulminate against someone, you speak or write angrily and with heavy criticism about them.</p>
<p class='rw-defn idx15' style='display:none'>A harangue is a long, scolding speech.</p>
<p class='rw-defn idx16' style='display:none'>If you inveigh against something, you criticize it very strongly.</p>
<p class='rw-defn idx17' style='display:none'>A laudatory article or speech expresses praise or admiration for someone or something.</p>
<p class='rw-defn idx18' style='display:none'>A paean is a piece of writing, verbal expression, or song that expresses enthusiastic praise, admiration, or joy.</p>
<p class='rw-defn idx19' style='display:none'>A panegyric is a speech or article that praises someone or something a lot.</p>
<p class='rw-defn idx20' style='display:none'>A peroration is a long speech that sounds impressive but does not have much substance.</p>
<p class='rw-defn idx21' style='display:none'>If you receive a plaudit, you receive admiration, praise, and approval from someone.</p>
<p class='rw-defn idx22' style='display:none'>A tirade is a prolonged, verbal outburst that severely criticizes someone or something.</p>
<p class='rw-defn idx23' style='display:none'>If you vilify people, you write or say bad things about them in an attempt to make them unpopular.</p>
<p class='rw-defn idx24' style='display:none'>Vituperative remarks are full of hate, anger, and cruel criticism.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>diatribe</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='diatribe#' id='pronounce-sound' path='audio/words/amy-diatribe'></a>
DAHY-uh-trahyb
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='diatribe#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='diatribe#' id='context-sound' path='audio/wordcontexts/brian-diatribe'></a>
Geraldine believed that it was her civic duty to protest some new construction, so she wrote a <em>diatribe</em> in her long and angry letter to the editor of the local paper.  Her strong disapproval of the land development caught everyone&#8217;s attention&#8212;such a fierce <em>diatribe</em> or written criticism surprised even the editor himself.  Geraldine&#8217;s furious letter of protest or <em>diatribe</em> against excessive development influenced many other locals in the area, who filed complaints as well.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>diatribe</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A letter to an editor that examines an important and somewhat heated local issue.
</li>
<li class='choice '>
<span class='result'></span>
A complaint that conveys anger in a short burst—and then quickly becomes apologetic.
</li>
<li class='choice answer '>
<span class='result'></span>
A speech or piece of writing that attacks an idea or activity.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='diatribe#' id='definition-sound' path='audio/wordmeanings/amy-diatribe'></a>
A <em>diatribe</em> is a speech or piece of writing that angrily attacks someone&#8217;s ideas or activities at length.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>extended criticism</em>
</span>
</span>
</div>
<a class='quick-help' href='diatribe#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='diatribe#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/diatribe/memory_hooks/4169.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Dia</span></span>l a <span class="emp3"><span>Tribe</span></span></span></span> If you don't like a particular <span class="emp3"><span>tribe</span></span>, you could <span class="emp1"><span>dia</span></span>l that <span class="emp3"><span>tribe</span></span> and speak a <span class="emp1"><span>dia</span></span><span class="emp3"><span>tribe</span></span> against it, all from the convenience of your own home!
</p>
</div>

<div id='memhook-button-bar'>
<a href="diatribe#" id="add-public-hook" style="" url="https://membean.com/mywords/diatribe/memory_hooks">Use other public hook</a>
<a href="diatribe#" id="memhook-use-own" url="https://membean.com/mywords/diatribe/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='diatribe#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Rather than bothering to even thank his mother, [Michael] Moore launched into an anti-war <b>diatribe</b> that started by asserting that President Bush was an illegitimate president and ended by saying the entire battle in Iraq is phony.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
The futility of efforts to permanently silence Bin Laden was brought home Friday when he released his first video message since 2004, a 26-minute, anti-U.S. <b>diatribe</b>.
<cite class='attribution'>
&mdash;
Harper's Magazine
</cite>
</li>
<li>
When he launched into a <b>diatribe</b> and used a vulgar expression, the mic was cut off and he was carried off to the applause of many in the audience, all the while resisting the police.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/diatribe/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='diatribe#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>dia-</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td class='partform'>trib</td>
<td>
&rarr;
</td>
<td class='meaning'>rub, thresh, pound</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='diatribe#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>To level a <em>diatribe</em> against another person is to &#8220;thoroughly rub or pound&#8221; them with angry words, trying to drive home the point that you are very displeased.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='diatribe#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Howard Stern on The Late Show with David Letterman</strong><span> Howard Stern delivers a diatribe.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/diatribe.jpg' video_url='examplevids/diatribe' video_width='350'></span>
<div id='wt-container'>
<img alt="Diatribe" height="288" src="https://cdn1.membean.com/video/examplevids/diatribe.jpg" width="350" />
<div class='center'>
<a href="diatribe#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='diatribe#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Diatribe" src="https://cdn0.membean.com/public/images/wordimages/cons2/diatribe.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='diatribe#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>bilious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bristling</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>castigate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>decry</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>disparage</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>exhort</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>expostulate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>fulminate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>harangue</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>inveigh</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>peroration</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>tirade</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>vilify</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>vituperative</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>adulation</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>approbation</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>elegy</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>encomium</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>eulogy</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>extol</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>laudatory</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>paean</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>panegyric</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>plaudit</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="diatribe" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>diatribe</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="diatribe#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

