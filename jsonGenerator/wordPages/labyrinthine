
<!DOCTYPE html>
<html>
<head>
<title>Word: labyrinthine | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Labyrinthine-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/labyrinthine-large.jpg?qdep8" />
</div>
<a href="labyrinthine#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx1' style='display:none'>If you describe something as ambiguous, you mean that it is unclear or confusing because it can be understood in multiple ways.</p>
<p class='rw-defn idx2' style='display:none'>A circuitous route, journey, or piece of writing is long and complicated rather than simple and direct.</p>
<p class='rw-defn idx3' style='display:none'>If something confounds you, it makes you feel surprised and confused, often because it does not meet your predefined expectations.</p>
<p class='rw-defn idx4' style='display:none'>A conundrum is a problem or puzzle that is difficult or impossible to solve.</p>
<p class='rw-defn idx5' style='display:none'>Something convoluted, such as a difficult concept or procedure, is complex and takes many twists and turns.</p>
<p class='rw-defn idx6' style='display:none'>Something that is desultory is done in a way that is unplanned, disorganized, and without direction.</p>
<p class='rw-defn idx7' style='display:none'>A diaphanous cloth is thin enough to see through.</p>
<p class='rw-defn idx8' style='display:none'>A piece of writing is discursive if it includes a lot of information that is not relevant to the main subject.</p>
<p class='rw-defn idx9' style='display:none'>When you disentangle a knot or a problem, you untie the knot or get yourself out of the problem.</p>
<p class='rw-defn idx10' style='display:none'>Errant things or people behave in a way that is unacceptable or wrong, such as a missile that travels in the wrong direction or a student who does not follow classroom rules.</p>
<p class='rw-defn idx11' style='display:none'>If you criticize someone&#8217;s arguments as being facile, you consider their ideas simplistic and not well thought through.</p>
<p class='rw-defn idx12' style='display:none'>If you fathom something complicated or mysterious, you are able to understand it.</p>
<p class='rw-defn idx13' style='display:none'>If an idea or thought is incisive, it is expressed in a penetrating and knowledgeable manner that is clear and brief; additionally, it can demonstrate impressive understanding of related ideas or thoughts.</p>
<p class='rw-defn idx14' style='display:none'>An intricate design or problem can be both complex and filled with elaborate detail.</p>
<p class='rw-defn idx15' style='display:none'>Limpid speech or prose is clear, simple, and easily understood; things, such as pools of water or eyes, are limpid if they are clear or transparent.</p>
<p class='rw-defn idx16' style='display:none'>If someone is lucid, they are able to understand things and think clearly; this adjective also describes writing or speech that is crystal clear and easy to understand.</p>
<p class='rw-defn idx17' style='display:none'>When something is luminous, it is bright and glowing.</p>
<p class='rw-defn idx18' style='display:none'>Something that is multifarious is made up of many kinds of different things.</p>
<p class='rw-defn idx19' style='display:none'>Something is opaque if it is either difficult to understand or is not transparent.</p>
<p class='rw-defn idx20' style='display:none'>If someone is pedantic, they give too much importance to unimportant details and formal rules.</p>
<p class='rw-defn idx21' style='display:none'>Something that is pellucid is either extremely clear because it is transparent to the eye or it is very easy for the mind to understand.</p>
<p class='rw-defn idx22' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx23' style='display:none'>Something that is sinuous is shaped or moves like a snake, having many smooth twists and turns that can often be highly graceful.</p>
<p class='rw-defn idx24' style='display:none'>Something that is tortuous, such as a piece of writing, is long and complicated with many twists and turns in direction; a tortuous argument can be deceitful because it twists or turns the truth.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>labyrinthine</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='labyrinthine#' id='pronounce-sound' path='audio/words/amy-labyrinthine'></a>
lab-uh-RIN-thin
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='labyrinthine#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='labyrinthine#' id='context-sound' path='audio/wordcontexts/brian-labyrinthine'></a>
I love Barcelona, yet the winding, complex, and <em>labyrinthine</em> streets always confuse me.  Even when I follow a map carefully, the mazelike ways of this <em>labyrinthine</em> city get me lost, and I end up in an unfamiliar square with no concept of how to get home.  Although I am often frustrated by the tangled, twisted, and <em>labyrinthine</em> layout of the city, it is also a part of what draws me there.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How would you feel in a <em>labyrinthine</em> situation?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Frustrated by how complex and involved it is.
</li>
<li class='choice '>
<span class='result'></span>
Nervous that other people might find out what you&#8217;re doing.
</li>
<li class='choice '>
<span class='result'></span>
Energized by working with others who share your passions.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='labyrinthine#' id='definition-sound' path='audio/wordmeanings/amy-labyrinthine'></a>
If you describe a situation or process as <em>labyrinthine</em>, you mean that it is very complicated, involved, and difficult to understand.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>like a maze</em>
</span>
</span>
</div>
<a class='quick-help' href='labyrinthine#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='labyrinthine#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/labyrinthine/memory_hooks/5149.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Thine</span></span> M<span class="emp2"><span>in</span></span>d F<span class="emp1"><span>labby</span></span>?</span></span> Is <span class="emp3"><span>thine</span></span> m<span class="emp2"><span>in</span></span>d f<span class="emp1"><span>labby</span></span>?  If so, do not get involved in a <span class="emp1"><span>laby</span></span>r<span class="emp2"><span>in</span></span><span class="emp3"><span>thine</span></span> academic problem because you will be eaten by a m<span class="emp2"><span>in</span></span>d M<span class="emp2"><span>in</span></span>otaur and so miserably fail.
</p>
</div>

<div id='memhook-button-bar'>
<a href="labyrinthine#" id="add-public-hook" style="" url="https://membean.com/mywords/labyrinthine/memory_hooks">Use other public hook</a>
<a href="labyrinthine#" id="memhook-use-own" url="https://membean.com/mywords/labyrinthine/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='labyrinthine#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Every voracious reader knows that there is no Dewey system for the Babel of the mind. You walk amid the <b>labyrinthine</b> stacks and ideas leap at you like dust bunnies drawn from the motes that cover a great many different books read long ago.
<span class='attribution'>&mdash; Maria Popova, Bulgarian-born, American-based writer, from _Figuring_</span>
<img alt="Maria popova, bulgarian-born, american-based writer, from _figuring_" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Maria Popova, Bulgarian-born, American-based writer, from _Figuring_.jpg?qdep8" width="80" />
</li>
<li>
Bees, ants and cockroaches build <b>labyrinthine</b> hives with thousands of workers performing highly specialized tasks, all done without a foreman or vice president or even a blueprint in sight.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
Jobless people stymied by the system describe a <b>labyrinthine</b> process filled with computer glitches and a call center that is constantly busy and offers scant help when they’re able to talk to someone.
<cite class='attribution'>
&mdash;
Orlando Sentinel
</cite>
</li>
<li>
The cobbled, <b>labyrinthine</b> streets of the adjoining Cavern Quarter area include dozens of lively bars, clubs and restaurants, plus plenty of Beatles souvenir shops.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/labyrinthine/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='labyrinthine#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>labyrinth</td>
<td>
&rarr;
</td>
<td class='meaning'>maze</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ine_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='labyrinthine#'>
<span class=''></span>
-ine
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>Something <em>labyrinthine</em> in structure is &#8220;relating to a maze.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='labyrinthine#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Science Squad</strong><span> Quite the labyrinthine path!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/labyrinthine.jpg' video_url='examplevids/labyrinthine' video_width='350'></span>
<div id='wt-container'>
<img alt="Labyrinthine" height="288" src="https://cdn1.membean.com/video/examplevids/labyrinthine.jpg" width="350" />
<div class='center'>
<a href="labyrinthine#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='labyrinthine#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Labyrinthine" src="https://cdn1.membean.com/public/images/wordimages/cons2/labyrinthine.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='labyrinthine#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>ambiguous</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>circuitous</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>confound</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>conundrum</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>convoluted</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>desultory</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>discursive</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>errant</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>intricate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>multifarious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>opaque</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>pedantic</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>sinuous</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>tortuous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='7' class = 'rw-wordform notlearned'><span>diaphanous</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>disentangle</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>facile</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>fathom</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>incisive</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>limpid</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>lucid</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>luminous</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>pellucid</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='labyrinthine#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
labyrinth
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>maze</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="labyrinthine" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>labyrinthine</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="labyrinthine#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

