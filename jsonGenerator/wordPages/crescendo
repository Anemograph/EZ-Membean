
<!DOCTYPE html>
<html>
<head>
<title>Word: crescendo | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Accretion is the slow, gradual process by which new things are added and something gets bigger.</p>
<p class='rw-defn idx1' style='display:none'>The acme of something is its highest point of achievement or excellence.</p>
<p class='rw-defn idx2' style='display:none'>The apex of anything, such as an organization or system, is its highest part or most important position.</p>
<p class='rw-defn idx3' style='display:none'>To augment something is to increase its value or effectiveness by adding something to it.</p>
<p class='rw-defn idx4' style='display:none'>If you curtail something, you reduce or limit it.</p>
<p class='rw-defn idx5' style='display:none'>When you deplete a supply of something, you use it up or lessen its amount over a given period of time.</p>
<p class='rw-defn idx6' style='display:none'>When an amount of something dwindles, it becomes less and less over time.</p>
<p class='rw-defn idx7' style='display:none'>If you describe a person, group, or civilization as effete, you mean it is weak, exhausted,  powerless, unproductive, and/or corrupt.</p>
<p class='rw-defn idx8' style='display:none'>If something is your forte, you are very good at it or know a lot about it.</p>
<p class='rw-defn idx9' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx10' style='display:none'>If you mollify someone, you say or do something to make that person feel less angry or upset.</p>
<p class='rw-defn idx11' style='display:none'>If something bad, such as crime or disease, is rampant, there is a lot of it—and it is increasing in a way that is difficult to control.</p>
<p class='rw-defn idx12' style='display:none'>To rarefy something is to make it less dense, as in oxygen at high altitudes; this word also refers to purifying or refining something, thereby making it less ordinary or commonplace.</p>
<p class='rw-defn idx13' style='display:none'>When something, such as a disease or storm, is in remission, its force is lessening or decreasing.</p>
<p class='rw-defn idx14' style='display:none'>If you say that something, such as an event or a message, resonates with you, you mean that it has an emotional effect or a special meaning for you that is significant.</p>
<p class='rw-defn idx15' style='display:none'>A resounding success, victory, or defeat is very great or complete, whereas a noise of this kind is loud, powerful, and ringing.</p>
<p class='rw-defn idx16' style='display:none'>A sonorous sound is pleasantly full, strong, and rich.</p>
<p class='rw-defn idx17' style='display:none'>A stentorian voice is extremely loud and strong.</p>
<p class='rw-defn idx18' style='display:none'>When something subsides, it begins to go away, lessen, or decrease in some way.</p>
<p class='rw-defn idx19' style='display:none'>When something surges, it rapidly increases or suddenly rises.</p>
<p class='rw-defn idx20' style='display:none'>If you surmount a problem or difficulty, you get the better of it by conquering or overcoming it.</p>
<p class='rw-defn idx21' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>
<p class='rw-defn idx22' style='display:none'>If you waver, you cannot decide between two things because you have serious doubts about which choice is better.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>crescendo</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='crescendo#' id='pronounce-sound' path='audio/words/amy-crescendo'></a>
kri-SHEN-doh
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='crescendo#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='crescendo#' id='context-sound' path='audio/wordcontexts/brian-crescendo'></a>
The crowd of workers was already upset over perceived injustices by their company, but their displeasure reached a <em>crescendo</em> when the company director announced that he was moving the headquarters overseas.  The chanting against the decision grew louder and louder in a vast, increasing <em>crescendo</em> as I worked my way up to the front of the crowd.  The <em>crescendo</em> finally exploded into a huge uncontrolled booing, screaming, and hissing that hurt everyone&#8217;s ears and drowned out the words of the company spokesperson.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>crescendo</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
When a song continues to get louder.
</li>
<li class='choice '>
<span class='result'></span>
When a bite of something has a spicy aftertaste.
</li>
<li class='choice '>
<span class='result'></span>
When a group of people gathers to protest something.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='crescendo#' id='definition-sound' path='audio/wordmeanings/amy-crescendo'></a>
If something, such as a sound, is undergoing a <em>crescendo</em>, it is getting louder and louder or increasing in intensity and is about to reach its peak strength.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>increasing</em>
</span>
</span>
</div>
<a class='quick-help' href='crescendo#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='crescendo#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/crescendo/memory_hooks/2864.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>End</span></span>s at <span class="emp2"><span>Cres</span></span>t</span></span> The rock band's <span class="emp2"><span>cres</span></span>c<span class="emp1"><span>end</span></span>o of sound only <span class="emp1"><span>end</span></span>ed when the guitarists reached the <span class="emp2"><span>cres</span></span>t of the greatest sound that the sound equipment could produce, a very loud sound indeed.
</p>
</div>

<div id='memhook-button-bar'>
<a href="crescendo#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/crescendo/memory_hooks">Use other hook</a>
<a href="crescendo#" id="memhook-use-own" url="https://membean.com/mywords/crescendo/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='crescendo#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Then, as the band and choir swell up behind her, she erupts into a soaring <b>crescendo</b> before suddenly dropping into a low, smoky alto for the amen.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
<li>
Although the Heathrow experience has never been particularly good, mainly because the airport crams 67m passengers a year through facilities designed for 45m, the <b>crescendo</b> of complaints suggests that it is going from bad to worse.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
If Bear Stearns were to go under, "it has the potential of bringing down the whole market," said Richard Bove, an analyst at Punk, Ziegel & Co. "This is the <b>crescendo</b> of the crisis."
<cite class='attribution'>
&mdash;
NBC News
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/crescendo/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='crescendo#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='cresc_grow' data-tree-url='//cdn1.membean.com/public/data/treexml' href='crescendo#'>
<span class=''></span>
cresc
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>grow, arise, increase</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='endo_doing' data-tree-url='//cdn1.membean.com/public/data/treexml' href='crescendo#'>
<span class=''></span>
-endo
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>doing something</td>
</tr>
</table>
<p>A <em>crescendo</em> is a &#8220;growing, arising, or increasing&#8221; of sound to its highest pitch.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='crescendo#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Edvard Grieg</strong><span> A crescendo from Grieg's "In the Hall of the Mountain King."</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/crescendo.jpg' video_url='examplevids/crescendo' video_width='350'></span>
<div id='wt-container'>
<img alt="Crescendo" height="288" src="https://cdn1.membean.com/video/examplevids/crescendo.jpg" width="350" />
<div class='center'>
<a href="crescendo#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='crescendo#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Crescendo" src="https://cdn0.membean.com/public/images/wordimages/cons2/crescendo.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='crescendo#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>accretion</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>acme</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>apex</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>augment</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>forte</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>rampant</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>resonate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>resounding</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>sonorous</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>stentorian</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>surge</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>surmount</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='4' class = 'rw-wordform notlearned'><span>curtail</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>deplete</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>dwindle</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>effete</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>mollify</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>rarefy</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>remission</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>subside</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>waver</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='crescendo#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
decrescendo
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>a gradual lowering in volume or intensity</td>
</tr>
<tr>
<td class='wordform'>
<span>
crescendo
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to build up gradually to a great intensity</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="crescendo" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>crescendo</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="crescendo#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

