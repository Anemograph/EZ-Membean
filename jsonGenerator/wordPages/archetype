
<!DOCTYPE html>
<html>
<head>
<title>Word: archetype | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Something that happens unexpectedly and by chance that is not inherent or natural to a given item or situation is adventitious, such as a root appearing in an unusual place on a plant.</p>
<p class='rw-defn idx1' style='display:none'>An axiom is a wise saying or widely recognized general truth that is often self-evident; it can also be an established rule or law.</p>
<p class='rw-defn idx2' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx3' style='display:none'>The crux of a problem or argument is the most important or difficult part of it that affects everything else.</p>
<p class='rw-defn idx4' style='display:none'>A situation or thing that is discordant does not fit in with other things; therefore, it is disagreeable, strange, or unpleasant.</p>
<p class='rw-defn idx5' style='display:none'>Things that are disparate are clearly different from each other and belong to different groups or classes.</p>
<p class='rw-defn idx6' style='display:none'>A dissident is someone who disagrees publicly with a government, especially in a country where this is not allowed.</p>
<p class='rw-defn idx7' style='display:none'>An embodiment of something, such as a quality or idea, is a visible representation or concrete expression of it.</p>
<p class='rw-defn idx8' style='display:none'>If something engenders a particular feeling or attitude, it causes that condition.</p>
<p class='rw-defn idx9' style='display:none'>If you say that a person or thing is the epitome of something, you mean that they or it is the best possible example of that thing.</p>
<p class='rw-defn idx10' style='display:none'>An exemplary person is one that sets an ideal or praiseworthy example for others to follow.</p>
<p class='rw-defn idx11' style='display:none'>One thing that exemplifies another serves as an example of it, illustrates it, or demonstrates it.</p>
<p class='rw-defn idx12' style='display:none'>An intrinsic characteristic of something is the basic and essential feature that makes it what it is.</p>
<p class='rw-defn idx13' style='display:none'>A paradigm is a model or example of something that shows how it works or how it can be produced; a paradigm shift is a completely new way of thinking about something.</p>
<p class='rw-defn idx14' style='display:none'>A paragon is an example of a thing or person at its or their very best.</p>
<p class='rw-defn idx15' style='display:none'>When you personify something, such as a quality, you are the perfect example of it.</p>
<p class='rw-defn idx16' style='display:none'>Something that is pivotal is more important than anything else in a situation or a system; consequently, it is the key to its success.</p>
<p class='rw-defn idx17' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>
<p class='rw-defn idx18' style='display:none'>Something that is superfluous is unnecessary; it is more than what is wanted or needed at the current time.</p>
<p class='rw-defn idx19' style='display:none'>Something vintage is the best of its kind and of excellent quality; it is often of a past time and is considered a classic example of its type.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>archetype</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='archetype#' id='pronounce-sound' path='audio/words/amy-archetype'></a>
AHR-ki-tahyp
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='archetype#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='archetype#' id='context-sound' path='audio/wordcontexts/brian-archetype'></a>
Human beings are brought together by the <em><em>archetypes</em></em> or essential aspects that they all share, such as being born, falling in love, maturing, grieving those who have passed on, and death itself.  Besides the <em><em>archetypes</em></em> of basic, human universal elements that we all experience, <em>archetypes</em> of kinds of people are also commonly shared among humans, such as the hero, the villain, the ambitious businessman, the teenage rebel, the fish out of water, and the damsel in distress.  An example of an architectural <em><em>archetype</em></em>, or basic model from which copies are made, is the western ranch, the style of which has been copied many times to form the ranch-style house.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of an <em>archetype</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Someone who corrects any errors in a book.
</li>
<li class='choice '>
<span class='result'></span>
An unusual villain who is both evil and likable.
</li>
<li class='choice answer '>
<span class='result'></span>
A typical bully who intimidates their classmates.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='archetype#' id='definition-sound' path='audio/wordmeanings/amy-archetype'></a>
An <em>archetype</em> is a perfect or typical example of something because it has the most important qualities that belong to that type of thing; it can also describe essential qualities common to a particular class of things.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>typical example</em>
</span>
</span>
</div>
<a class='quick-help' href='archetype#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='archetype#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/archetype/memory_hooks/3843.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Arch</span></span>i<span class="emp1"><span>e</span></span>'s <span class="emp2"><span>Typ</span></span>ing</span></span>  <span class="emp1"><span>Arc</span></span>hi<span class="emp1"><span>e</span></span> has been <span class="emp2"><span>typ</span></span>ing for many years now, having begun on the <span class="emp1"><span>arche</span></span><span class="emp2"><span>typ</span></span>al <span class="emp2"><span>type</span></span>writer, the source or origin of numerous <span class="emp2"><span>typ</span></span>ing devices, such as keyboards for computers, keypads for Blackberry devices, and touch screens for the iPod and iPad.
</p>
</div>

<div id='memhook-button-bar'>
<a href="archetype#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/archetype/memory_hooks">Use other hook</a>
<a href="archetype#" id="memhook-use-own" url="https://membean.com/mywords/archetype/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='archetype#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Toyota, regarded as an <b>archetype</b> of corporate Japan in many respects, is a typical example: it has switched from bank financing to bonds, has a high level of foreign ownership and has introduced stock options.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Booker has not discovered <b>archetypes</b>, hard-wired blueprints, for story plots, though he has identified the deep themes that fascinate us in fictions.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Unions have also been slow to reach out to what many consider the <b>archetype</b> of the new recruit: young "knowledge" workers who have individualized jobs.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
A lot of female voters, however, may be factoring in a whole other kind of female <b>archetype</b>, whose wet eyes do not signal weakness and whose flashes of anger do not signal coldness, only pragmatic perseverance.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/archetype/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='archetype#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='arch_chief' data-tree-url='//cdn1.membean.com/public/data/treexml' href='archetype#'>
<span class=''></span>
arch-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>chief, principal</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='typ_blow' data-tree-url='//cdn1.membean.com/public/data/treexml' href='archetype#'>
<span class=''></span>
typ
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>blow, impression, form</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='archetype#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>An <em>archetype</em> is a beginning &#8220;form&#8221;; it can be a prototype, or example from which others are derived, or an ideal type or &#8220;form&#8221; that is viewed as the &#8220;chief or principal&#8221; example or &#8220;impression&#8221; of its class.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='archetype#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Nicole Rivera</strong><span> Modern film and TV archetypes.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/archetype.jpg' video_url='examplevids/archetype' video_width='350'></span>
<div id='wt-container'>
<img alt="Archetype" height="288" src="https://cdn1.membean.com/video/examplevids/archetype.jpg" width="350" />
<div class='center'>
<a href="archetype#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='archetype#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Archetype" src="https://cdn3.membean.com/public/images/wordimages/cons2/archetype.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='archetype#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>axiom</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>crux</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>embodiment</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>engender</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>epitome</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>exemplary</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>exemplify</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>intrinsic</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>paradigm</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>paragon</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>personify</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>pivotal</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>vintage</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>adventitious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>discordant</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>disparate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>dissident</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>superfluous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="archetype" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>archetype</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="archetype#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

