
<!DOCTYPE html>
<html>
<head>
<title>Word: predicament | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you abash someone, you make them feel uncomfortable, ashamed, embarrassed, or inferior.</p>
<p class='rw-defn idx1' style='display:none'>If you alleviate pain or suffering, you make it less intense or severe.</p>
<p class='rw-defn idx2' style='display:none'>When you ameliorate a bad condition or situation, you make it better in some way.</p>
<p class='rw-defn idx3' style='display:none'>The adjective auspicious describes a positive beginning of something, such as a new business, or a certain time that looks to have a good chance of success or prosperity.</p>
<p class='rw-defn idx4' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx5' style='display:none'>If something bewilders you, you are very confused or puzzled by it.</p>
<p class='rw-defn idx6' style='display:none'>If something confounds you, it makes you feel surprised and confused, often because it does not meet your predefined expectations.</p>
<p class='rw-defn idx7' style='display:none'>Consternation is the feeling of anxiety or fear, sometimes paralyzing in its effect, and often caused by something unexpected that has happened.</p>
<p class='rw-defn idx8' style='display:none'>A conundrum is a problem or puzzle that is difficult or impossible to solve.</p>
<p class='rw-defn idx9' style='display:none'>A debacle is something, such as an event or attempt, that fails completely in an embarrassing way.</p>
<p class='rw-defn idx10' style='display:none'>A dilemma is a difficult situation or problem that consists of a choice between two equally disagreeable or unfavorable alternatives.</p>
<p class='rw-defn idx11' style='display:none'>When you disentangle a knot or a problem, you untie the knot or get yourself out of the problem.</p>
<p class='rw-defn idx12' style='display:none'>If you elucidate something, you make it easier to understand by giving more relevant information.</p>
<p class='rw-defn idx13' style='display:none'>Someone or something that is enigmatic is mysterious and difficult to understand.</p>
<p class='rw-defn idx14' style='display:none'>When someone is exonerated from guilt, a task, or a responsibility, they are set free or excused from it.</p>
<p class='rw-defn idx15' style='display:none'>When you extricate yourself from a difficult or unpleasant situation, you manage to escape it.</p>
<p class='rw-defn idx16' style='display:none'>If you fathom something complicated or mysterious, you are able to understand it.</p>
<p class='rw-defn idx17' style='display:none'>If you are experiencing felicity about something, you are very happy about it.</p>
<p class='rw-defn idx18' style='display:none'>If an event becomes a fiasco, it is a complete and embarrassing failure.</p>
<p class='rw-defn idx19' style='display:none'>Something fortuitous happens purely by chance and produces a successful or pleasant result.</p>
<p class='rw-defn idx20' style='display:none'>An imbroglio is a very difficult, confusing, and complicated situation.</p>
<p class='rw-defn idx21' style='display:none'>An impasse is a difficult situation in which progress is not possible, usually because none of the people involved are willing to agree.</p>
<p class='rw-defn idx22' style='display:none'>Something inscrutable is very hard to figure out, discover, or understand what it is all about.</p>
<p class='rw-defn idx23' style='display:none'>When you are in jeopardy, you are in danger or trouble of some kind.</p>
<p class='rw-defn idx24' style='display:none'>If you describe a situation or process as labyrinthine, you mean that it is very complicated, involved, and difficult to understand.</p>
<p class='rw-defn idx25' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx26' style='display:none'>If you mollify someone, you say or do something to make that person feel less angry or upset.</p>
<p class='rw-defn idx27' style='display:none'>To obviate a problem is to eliminate it or do something that makes solving the problem unnecessary.</p>
<p class='rw-defn idx28' style='display:none'>If you are in a plight, you are in trouble of some kind or in a state of unfortunate circumstances.</p>
<p class='rw-defn idx29' style='display:none'>Something that is propitious shows favorable conditions.</p>
<p class='rw-defn idx30' style='display:none'>A providential event is a very lucky one because it happens at exactly the right time and often when it is needed most.</p>
<p class='rw-defn idx31' style='display:none'>A quagmire is a difficult and complicated situation that is not easy to avoid or get out of; this word can also refer to a swamp that is hard to travel through.</p>
<p class='rw-defn idx32' style='display:none'>If you are in a quandary, you are in a difficult situation in which you have to make a decision but don&#8217;t know what to do.</p>
<p class='rw-defn idx33' style='display:none'>If you are sanguine about a situation, especially a difficult one, you are confident and cheerful that everything will work out the way you want it to.</p>
<p class='rw-defn idx34' style='display:none'>Serendipity is the good fortune that some people encounter in finding or making interesting and valuable discoveries purely by luck.</p>
<p class='rw-defn idx35' style='display:none'>Severe problems, suffering, or difficulties experienced in a particular situation are all examples of tribulations.</p>
<p class='rw-defn idx36' style='display:none'>Vicissitudes can be unexpected or simply normal changes and variations that happen during the course of people&#8217;s lives.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>predicament</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='predicament#' id='pronounce-sound' path='audio/words/amy-predicament'></a>
pri-DIK-uh-muhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='predicament#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='predicament#' id='context-sound' path='audio/wordcontexts/brian-predicament'></a>
Was I ever in a <em>predicament</em> or difficult situation when my boss saw me out running when I had called in sick!  I had never been in such a <em>predicament</em> or fix before&#8212;how on Earth was I going to explain myself?  I realized that there was no easy way out of this <em>predicament</em> or jam, so I just decided to own up and tell the truth.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How might you feel if you are in a <em>predicament</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Briefly confused by something you didn&#8217;t understand at first.
</li>
<li class='choice '>
<span class='result'></span>
Bored during an uneventful time in your life.
</li>
<li class='choice answer '>
<span class='result'></span>
Unsure how you will ever get out of trouble.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='predicament#' id='definition-sound' path='audio/wordmeanings/amy-predicament'></a>
If you are in a <em>predicament</em>, you are in a difficult situation or unpleasant mess that is hard to get out of.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>difficulty</em>
</span>
</span>
</div>
<a class='quick-help' href='predicament#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='predicament#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/predicament/memory_hooks/2832.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Pre</span></span>ju<span class="emp1"><span>dic</span></span>i<span class="emp1"><span>a</span></span>l Com<span class="emp3"><span>ment</span></span></span></span> The politician in a big <span class="emp1"><span>predica</span></span><span class="emp3"><span>ment</span></span> after he made a <span class="emp1"><span>pre</span></span>ju<span class="emp1"><span>dic</span></span>i<span class="emp1"><span>a</span></span>l com<span class="emp3"><span>ment</span></span> that was caught on tape.
</p>
</div>

<div id='memhook-button-bar'>
<a href="predicament#" id="add-public-hook" style="" url="https://membean.com/mywords/predicament/memory_hooks">Use other public hook</a>
<a href="predicament#" id="memhook-use-own" url="https://membean.com/mywords/predicament/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='predicament#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
The reflection upon my situation, and that of this army, produces many an uneasy hour, when all around me are wrapped in sleep. Few people know the <b>predicament</b> we are in, on a thousand accounts; fewer still will believe, if any disaster happens to these lines, from what cause it flows.
<span class='attribution'>&mdash; George Washington, first president of the United States</span>
<img alt="George washington, first president of the united states" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/George Washington, first president of the United States.jpg?qdep8" width="80" />
</li>
<li>
Imagine a stock-car driver who has lost his steering and is about to lose his brakes, and you have a pretty fair idea of Charmayne’s <b>predicament</b>.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
In case you missed them, let me mention three quite different recent items that all bear on the <b>predicament</b> of journalism. Not the "modern" <b>predicament—the</b> struggle to stay in business—but the longer-term issues of trying to represent the "truth."
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
To think about it this way is to understand why this is a bigger <b>predicament</b> than any we’ve ever faced. Our other dramas—wars, revolutions—have played out against the backdrop of an essentially stable planet. But now that planet has become the main actor in our affairs, and more so every second.
<cite class='attribution'>
&mdash;
The New Yorker
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/predicament/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='predicament#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pre_before' data-tree-url='//cdn1.membean.com/public/data/treexml' href='predicament#'>
<span class='common'></span>
pre-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>before, in front</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='dic_say' data-tree-url='//cdn1.membean.com/public/data/treexml' href='predicament#'>
<span class='common'></span>
dic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>say, talk</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ment_quality' data-tree-url='//cdn1.membean.com/public/data/treexml' href='predicament#'>
<span class=''></span>
-ment
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>quality, condition</td>
</tr>
</table>
<p>When one &#8220;speaks before&#8221; thinking, one can land in quite the <em>predicament</em>.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='predicament#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Dirty Work (Laurel and Hardy)</strong><span> These chimney sweeps get in quite the predicament.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/predicament.jpg' video_url='examplevids/predicament' video_width='350'></span>
<div id='wt-container'>
<img alt="Predicament" height="288" src="https://cdn1.membean.com/video/examplevids/predicament.jpg" width="350" />
<div class='center'>
<a href="predicament#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='predicament#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Predicament" src="https://cdn3.membean.com/public/images/wordimages/cons2/predicament.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='predicament#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abash</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>bewilder</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>confound</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>consternation</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>conundrum</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>debacle</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>dilemma</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>enigmatic</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>fiasco</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>imbroglio</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>impasse</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>inscrutable</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>jeopardy</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>labyrinthine</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>plight</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>quagmire</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>quandary</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>tribulation</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>vicissitude</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>alleviate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>ameliorate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>auspicious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>disentangle</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>elucidate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>exonerate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>extricate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>fathom</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>felicity</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>fortuitous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>mollify</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>obviate</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>propitious</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>providential</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>sanguine</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>serendipity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="predicament" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>predicament</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="predicament#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

