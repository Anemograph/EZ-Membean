
<!DOCTYPE html>
<html>
<head>
<title>Word: quell | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>When you abet someone, you help them in a criminal or illegal act.</p>
<p class='rw-defn idx2' style='display:none'>An abortive attempt or action is cut short before it is finished; hence, it is unsuccessful.</p>
<p class='rw-defn idx3' style='display:none'>You allay someone&#8217;s fear or concern by making them feel less afraid or worried.</p>
<p class='rw-defn idx4' style='display:none'>If you alleviate pain or suffering, you make it less intense or severe.</p>
<p class='rw-defn idx5' style='display:none'>When you assuage an unpleasant feeling, you make it less painful or severe, thereby calming it.</p>
<p class='rw-defn idx6' style='display:none'>To augment something is to increase its value or effectiveness by adding something to it.</p>
<p class='rw-defn idx7' style='display:none'>Consternation is the feeling of anxiety or fear, sometimes paralyzing in its effect, and often caused by something unexpected that has happened.</p>
<p class='rw-defn idx8' style='display:none'>If something, such as a sound, is undergoing a crescendo, it is getting louder and louder or increasing in intensity and is about to reach its peak strength.</p>
<p class='rw-defn idx9' style='display:none'>If you curtail something, you reduce or limit it.</p>
<p class='rw-defn idx10' style='display:none'>If you decimate something, you destroy a large part of it, reducing its size and effectiveness greatly.</p>
<p class='rw-defn idx11' style='display:none'>If something discomfits you, it makes you feel embarrassed, confused, uncomfortable, or frustrated.</p>
<p class='rw-defn idx12' style='display:none'>When someone feels disquiet about a situation, they feel very worried or nervous.</p>
<p class='rw-defn idx13' style='display:none'>If a fact or idea eludes you, you cannot remember or understand it; if you elude someone, you manage to escape or hide from them.</p>
<p class='rw-defn idx14' style='display:none'>If you eschew something, you deliberately avoid doing it, especially for moral reasons.</p>
<p class='rw-defn idx15' style='display:none'>If something fetters you, it restricts you from doing something you want to do.</p>
<p class='rw-defn idx16' style='display:none'>To foment is to encourage people to protest, fight, or cause trouble and violent opposition to something that is viewed by some as undesirable.</p>
<p class='rw-defn idx17' style='display:none'>When you galvanize someone, you cause them to improve a situation, solve a problem, or take some other action by making them feel excited, afraid, or angry.</p>
<p class='rw-defn idx18' style='display:none'>If you incarcerate someone, you put them in prison or jail.</p>
<p class='rw-defn idx19' style='display:none'>An incendiary device causes objects to catch on fire; incendiary comments can cause riots to flare up.</p>
<p class='rw-defn idx20' style='display:none'>To induce a state is to bring it about; to induce someone to do something is to persuade them to do it.</p>
<p class='rw-defn idx21' style='display:none'>An inflammable substance or person&#8217;s temper is easily set on fire.</p>
<p class='rw-defn idx22' style='display:none'>When you instigate something, you start it or stir it up, usually for the purpose of causing trouble of some kind.</p>
<p class='rw-defn idx23' style='display:none'>A manacle is a circular, usually metallic device used to chain someone&#8217;s wrists and/or ankles together.</p>
<p class='rw-defn idx24' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx25' style='display:none'>If you mollify someone, you say or do something to make that person feel less angry or upset.</p>
<p class='rw-defn idx26' style='display:none'>If you nettle someone, you irritate or annoy them.</p>
<p class='rw-defn idx27' style='display:none'>A palliative action makes a bad situation seem better; nevertheless, it does not actually solve the problem.</p>
<p class='rw-defn idx28' style='display:none'>A prophylactic is used as a preventative or protective agent to keep someone free from disease or infection.</p>
<p class='rw-defn idx29' style='display:none'>A quagmire is a difficult and complicated situation that is not easy to avoid or get out of; this word can also refer to a swamp that is hard to travel through.</p>
<p class='rw-defn idx30' style='display:none'>If you are in a quandary, you are in a difficult situation in which you have to make a decision but don&#8217;t know what to do.</p>
<p class='rw-defn idx31' style='display:none'>Refractory people deliberately don&#8217;t obey someone in authority and so are difficult to deal with or control.</p>
<p class='rw-defn idx32' style='display:none'>If you refrain from doing something, you do not do it—even though you want to.</p>
<p class='rw-defn idx33' style='display:none'>Sedition is the act of encouraging people to disobey and oppose the government currently in power.</p>
<p class='rw-defn idx34' style='display:none'>When you stifle someone&#8217;s creativity or inner drive, you prevent it from being expressed.</p>
<p class='rw-defn idx35' style='display:none'>If someone subjugates a group of people or country, they conquer and bring it under control by force.</p>
<p class='rw-defn idx36' style='display:none'>When you trammel something or someone, you bring about limits to freedom or hinder movements or activities in some fashion.</p>
<p class='rw-defn idx37' style='display:none'>A tumultuous event or period of time is filled with great excitement, confusion, or violence; a tumultuous reaction to something is likewise very loud and noisy because people are happy and excited.</p>
<p class='rw-defn idx38' style='display:none'>An unquenchable desire or thirst cannot be satisfied or gotten rid of.</p>
<p class='rw-defn idx39' style='display:none'>When something whets your appetite, it increases your desire for it, especially by giving you a little idea or pleasing hint of what it is like.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>quell</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='quell#' id='pronounce-sound' path='audio/words/amy-quell'></a>
kwel
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='quell#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='quell#' id='context-sound' path='audio/wordcontexts/brian-quell'></a>
We heard the slow creak of the cabin door, but Samantha <em>quelled</em> or quieted our fears.  She calmed or <em>quelled</em> our imaginations by saying that it was just the wind.  We felt peaceful again until we heard a soft tapping at the window, but again she <em>quelled</em> or overcame our anxiety by pointing to a tree branch.  When we saw the ghost, however, even Samantha&#8217;s fear could not be <em>quelled</em> or put down!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a way you could <em>quell</em> hunger?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
You could eat something so you get rid of the hunger.
</li>
<li class='choice '>
<span class='result'></span>
You could avoid all food so that your hunger increases.
</li>
<li class='choice '>
<span class='result'></span>
You could study the reasons why people feel hunger.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='quell#' id='definition-sound' path='audio/wordmeanings/amy-quell'></a>
To <em>quell</em> something is to stamp out, quiet, or overcome it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>suppress</em>
</span>
</span>
</div>
<a class='quick-help' href='quell#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='quell#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/quell/memory_hooks/3977.json'></span>
<p>
<span class="emp0"><span>Qu<span class="emp2"><span>ell</span></span> Those Y<span class="emp2"><span>ell</span></span>s!</span></span>  "Be quiet!  Qu<span class="emp2"><span>ell</span></span> your y<span class="emp2"><span>ell</span></span>s!  Then ring the b<span class="emp2"><span>ell</span></span>s to let me know!"
</p>
</div>

<div id='memhook-button-bar'>
<a href="quell#" id="add-public-hook" style="" url="https://membean.com/mywords/quell/memory_hooks">Use other public hook</a>
<a href="quell#" id="memhook-use-own" url="https://membean.com/mywords/quell/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='quell#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
South Africa has contributed admirably to the cause of peace in Africa’s bleaker reaches, helping to negotiate agreements to <b>quell</b> civil strife and bolster democracy.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Lavender may also help with dogs' motion sickness, Wells says, citing a study that found it helps <b>quell</b> motion sickness in pigs.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
Southwest’s point, which the airline tried to clarify with a lengthy statement intended to <b>quell</b> what it called the "sensationalism" over the issue, is that airlines are low-margin businesses and, increasingly, every seat counts.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Two years into his retirement, Tom McCarthy felt like he hadn't accomplished much. . . . [S]omething was missing. Hiking was sometimes the answer. He'd put in a few excursions here and there. They seemed to <b>quell</b> the feeling. Maybe he'd try the Appalachian Trail, he figured.
<cite class='attribution'>
&mdash;
Tampa Bay Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/quell/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='quell#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>From a root word meaning &#8220;to kill.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='quell#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Quell" src="https://cdn0.membean.com/public/images/wordimages/cons2/quell.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='quell#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abortive</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>allay</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>alleviate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>assuage</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>curtail</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>decimate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>elude</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>eschew</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>fetter</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>incarcerate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>manacle</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>mollify</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>palliative</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>prophylactic</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>refrain</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>stifle</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>subjugate</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>trammel</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>abet</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>augment</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>consternation</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>crescendo</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>discomfit</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>disquiet</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>foment</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>galvanize</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>incendiary</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>induce</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>inflammable</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>instigate</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>nettle</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>quagmire</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>quandary</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>refractory</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>sedition</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>tumultuous</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>unquenchable</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>whet</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="quell" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>quell</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="quell#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

