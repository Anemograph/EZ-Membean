
<!DOCTYPE html>
<html>
<head>
<title>Word: sublime | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Sublime-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/sublime-large.jpg?qdep8" />
</div>
<a href="sublime#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>The acme of something is its highest point of achievement or excellence.</p>
<p class='rw-defn idx1' style='display:none'>Someone or something that is august is impressive, dignified, and highly respected.</p>
<p class='rw-defn idx2' style='display:none'>When you claim that something is banal, you do not like it because you think it is ordinary, dull, commonplace, and boring.</p>
<p class='rw-defn idx3' style='display:none'>A beatific expression, look, or smile shows great peace and happiness—it is angelic and saintly.</p>
<p class='rw-defn idx4' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx5' style='display:none'>The word corporeal refers to the physical or material world rather than the spiritual; it also means characteristic of the body rather than the mind or feelings.</p>
<p class='rw-defn idx6' style='display:none'>Ennui is the feeling of being bored, tired, and dissatisfied with life.</p>
<p class='rw-defn idx7' style='display:none'>Something ethereal has a delicate beauty that makes it seem not part of the real world.</p>
<p class='rw-defn idx8' style='display:none'>Limpid speech or prose is clear, simple, and easily understood; things, such as pools of water or eyes, are limpid if they are clear or transparent.</p>
<p class='rw-defn idx9' style='display:none'>Something that is lustrous shines in a soft and gentle way by reflecting light.</p>
<p class='rw-defn idx10' style='display:none'>Something mediocre is average or ordinary in quality; it’s just OK.</p>
<p class='rw-defn idx11' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx12' style='display:none'>Something that is pellucid is either extremely clear because it is transparent to the eye or it is very easy for the mind to understand.</p>
<p class='rw-defn idx13' style='display:none'>If someone reaches a pinnacle of something—such as a career or mountain—they have arrived at the highest point of it.</p>
<p class='rw-defn idx14' style='display:none'>Something that is pristine has not lost any of its original perfection or purity.</p>
<p class='rw-defn idx15' style='display:none'>When someone exhibits profundity, they display great intellectual depth and understanding; profundity can also be the depth or complexity of something.</p>
<p class='rw-defn idx16' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>
<p class='rw-defn idx17' style='display:none'>To rarefy something is to make it less dense, as in oxygen at high altitudes; this word also refers to purifying or refining something, thereby making it less ordinary or commonplace.</p>
<p class='rw-defn idx18' style='display:none'>Recondite areas of knowledge are those that are very difficult to understand and/or are not known by many people.</p>
<p class='rw-defn idx19' style='display:none'>People or things that are resplendent are beautiful, bright, and impressive in appearance.</p>
<p class='rw-defn idx20' style='display:none'>Secular viewpoints are not religious or spiritual but pertain to worldly or material aspects of life.</p>
<p class='rw-defn idx21' style='display:none'>To taint is to give an undesirable quality that damages a person&#8217;s reputation or otherwise spoils something.</p>
<p class='rw-defn idx22' style='display:none'>If you are suffering from tedium, you are bored.</p>
<p class='rw-defn idx23' style='display:none'>The word terrestrial refers to living or growing on land; it can also refer to planet Earth as a whole in comparison with other planets.</p>
<p class='rw-defn idx24' style='display:none'>A trite remark or idea is so overused that it is no longer interesting or novel.</p>
<p class='rw-defn idx25' style='display:none'>Something uncanny is very strange, unnatural, or highly unusual.</p>
<p class='rw-defn idx26' style='display:none'>To act in an uncouth manner is to be awkward and unmannerly.</p>
<p class='rw-defn idx27' style='display:none'>An unkempt person or thing is untidy and has not been kept neat.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>sublime</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='sublime#' id='pronounce-sound' path='audio/words/amy-sublime'></a>
suh-BLAHYM
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='sublime#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='sublime#' id='context-sound' path='audio/wordcontexts/brian-sublime'></a>
Stepping quietly through the massive, carved doors of the Gothic cathedral, Mira gazed in awe at the <em>sublime</em>, stunning beauty that surrounded her.  The giant stone pillars sprung up like the majestic trunks of trees, helping support the <em>sublime</em>, heavenly, light-filled stained glass windows.  Even the sunlight itself seemed divinely sent, inspiring Mira to think high, <em>sublime</em> thoughts that stretched beyond the everyday world.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of something that might be considered <em>sublime</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A challenging medical textbook that is difficult for even experienced doctors to understand.
</li>
<li class='choice '>
<span class='result'></span>
A sweet note that your grandmother wrote to your grandfather when they were younger.
</li>
<li class='choice answer '>
<span class='result'></span>
A breathtaking sunset that fills the sky with a blend of pink, orange, and red streaks.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='sublime#' id='definition-sound' path='audio/wordmeanings/amy-sublime'></a>
Something that is <em>sublime</em> is so strikingly beautiful that it seems not of this world; therefore, it affects the emotions deeply.
</li>
<li class='def-text'>
The adjective <em>sublime</em> is also used to describe a quality or feeling that is complete or of high value, such as &#8220;a person of <em>sublime</em> taste.&#8221;
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>divine</em>
</span>
</span>
</div>
<a class='quick-help' href='sublime#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='sublime#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/sublime/memory_hooks/3857.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Lime</span></span> <span class="emp3"><span>Sub</span></span>s</span></span> For me, <span class="emp2"><span>lime</span></span> <span class="emp3"><span>sub</span></span>marine sandwiches have a simply <span class="emp3"><span>sub</span></span><span class="emp2"><span>lime</span></span> taste--they are oh so good!
</p>
</div>

<div id='memhook-button-bar'>
<a href="sublime#" id="add-public-hook" style="" url="https://membean.com/mywords/sublime/memory_hooks">Use other public hook</a>
<a href="sublime#" id="memhook-use-own" url="https://membean.com/mywords/sublime/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='sublime#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
To experience <b>sublime</b> natural beauty is to confront the total inadequacy of language to describe what you see. Words cannot convey the scale of a view that is so stunning it is felt.
<span class='attribution'>&mdash; Eleanor Catton, New Zealand writer, from "The Land of The Long White Cloud"</span>
<img alt="Eleanor catton, new zealand writer, from &quot;the land of the long white cloud&quot;" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Eleanor Catton, New Zealand writer, from &quot;The Land of The Long White Cloud&quot;.jpg?qdep8" width="80" />
</li>
<li>
The shoreline, partly preserved in a 6,300-square-mile national park, evokes a feeling perhaps better described as “<b>sublime</b>,” with all the attendant awe and grandeur and fear that the word implies.
<cite class='attribution'>
&mdash;
The New York Times Magazine
</cite>
</li>
<li>
[T]he showstopper is the eastern approach called the Knife Edge, a boulder-strewn ridge walk of just over a mile where the mountain’s spine is sometimes all of three feet wide with a 2,000-foot drop on either side. Less exhilarating but equally <b>sublime</b> is Chimney Pond . . . .
<cite class='attribution'>
&mdash;
Outside Magazine
</cite>
</li>
<li>
The sound of chewing can send tingles racing through my skull, and pens whispering against paper are mysteriously relaxing. And where I can’t be with my family when they’re eating salads, a stranger munching lettuce right next to me is oddly <b>sublime</b>.
<cite class='attribution'>
&mdash;
Discover Magazine
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/sublime/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='sublime#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='sublim_uplifted' data-tree-url='//cdn1.membean.com/public/data/treexml' href='sublime#'>
<span class=''></span>
sublim
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>uplifted, raised aloft</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='sublime#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>Something that is <em>sublime</em> is &#8220;uplifted or raised aloft&#8221; from common, ordinary, everyday things.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='sublime#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Sublime" src="https://cdn0.membean.com/public/images/wordimages/cons2/sublime.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='sublime#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>acme</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>august</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>beatific</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>ethereal</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>limpid</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>lustrous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>pellucid</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>pinnacle</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>pristine</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>profundity</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>rarefy</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>recondite</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>resplendent</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>uncanny</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>banal</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>corporeal</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>ennui</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>mediocre</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>secular</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>taint</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>tedium</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>terrestrial</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>trite</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>uncouth</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>unkempt</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="sublime" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>sublime</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="sublime#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

