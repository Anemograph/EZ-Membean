
<!DOCTYPE html>
<html>
<head>
<title>Word: scintillating | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Someone who is adroit is skillful and clever, especially in thought, behavior, or action.</p>
<p class='rw-defn idx1' style='display:none'>Apathy is a lack of interest or unenthusiastic involvement in an activity; there is no effort to change or improve it at all.</p>
<p class='rw-defn idx2' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx3' style='display:none'>A convivial atmosphere or occasion is friendly, pleasant, cheerful, and relaxed.</p>
<p class='rw-defn idx4' style='display:none'>A man who is dapper has a very neat and clean appearance; he is both fashionable and stylish.</p>
<p class='rw-defn idx5' style='display:none'>A man who is debonair wears fashionable clothes and is sophisticated, charming, friendly, and confident.</p>
<p class='rw-defn idx6' style='display:none'>Something that is dolorous, such as music or news, causes mental pain and sorrow because it itself is full of grief and sorrow.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is dour is serious, stubborn, and unfriendly; they can also be gloomy.</p>
<p class='rw-defn idx8' style='display:none'>When something is droll, it is humorous in an odd way.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx10' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx11' style='display:none'>Something that is effulgent is very bright and radiates light.</p>
<p class='rw-defn idx12' style='display:none'>Someone&#8217;s elocution is their artistic manner of speaking in public, including both the delivery of their voice and gestures.</p>
<p class='rw-defn idx13' style='display:none'>Ennui is the feeling of being bored, tired, and dissatisfied with life.</p>
<p class='rw-defn idx14' style='display:none'>If something enthralls you, it makes you so interested and excited that you give it all your attention.</p>
<p class='rw-defn idx15' style='display:none'>If you show exuberance, you display great excitement, energy, and enthusiasm.</p>
<p class='rw-defn idx16' style='display:none'>If you exult, you show great pleasure and excitement, especially about something you have achieved.</p>
<p class='rw-defn idx17' style='display:none'>Something humdrum is dull, boring, or tiresome.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is jocular is cheerful and often makes jokes or tries to make people laugh.</p>
<p class='rw-defn idx19' style='display:none'>A languid person is slow, relaxed, and shows little energy or interest in doing anything.</p>
<p class='rw-defn idx20' style='display:none'>Lassitude is a state of tiredness, lack of energy, and having little interest in what&#8217;s going on around you.</p>
<p class='rw-defn idx21' style='display:none'>If you are lethargic, you are tired, lack energy, and are unwilling to exert effort.</p>
<p class='rw-defn idx22' style='display:none'>If someone is lugubrious, they are looking very sad or gloomy.</p>
<p class='rw-defn idx23' style='display:none'>Something that is lustrous shines in a soft and gentle way by reflecting light.</p>
<p class='rw-defn idx24' style='display:none'>Malaise is a feeling of discontent, general unease, or even illness in a person or group for which there does not seem to be a quick and easy solution because the cause of it is not clear.</p>
<p class='rw-defn idx25' style='display:none'>If you are melancholy, you look and feel sad.</p>
<p class='rw-defn idx26' style='display:none'>A monotonous activity is so repetitious that it quickly becomes boring and dull.</p>
<p class='rw-defn idx27' style='display:none'>Someone who is morose is unhappy, bad-tempered, and unwilling to talk very much.</p>
<p class='rw-defn idx28' style='display:none'>If someone is pallid, they look very pale in an unattractive and unhealthy way.</p>
<p class='rw-defn idx29' style='display:none'>A plaintive sound or voice expresses sadness.</p>
<p class='rw-defn idx30' style='display:none'>Someone who is saturnine is looking miserable and sad, sometimes in a threatening or unfriendly way.</p>
<p class='rw-defn idx31' style='display:none'>If you are somnolent, you are sleepy.</p>
<p class='rw-defn idx32' style='display:none'>Stupor is a state in which someone&#8217;s mind and senses are dulled; consequently, they are unable to think clearly or act normally, usually due to the use of alcohol or drugs.</p>
<p class='rw-defn idx33' style='display:none'>If you are suffering from tedium, you are bored.</p>
<p class='rw-defn idx34' style='display:none'>If your body is affected by torpor, you are severely lacking in energy; therefore, you are idle—and can even be numb.</p>
<p class='rw-defn idx35' style='display:none'>A trite remark or idea is so overused that it is no longer interesting or novel.</p>
<p class='rw-defn idx36' style='display:none'>If someone is described as vivacious, they are lively and have a happy, lighthearted manner.</p>
<p class='rw-defn idx37' style='display:none'>Someone who is woebegone is very sad and filled with grief.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>scintillating</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='scintillating#' id='pronounce-sound' path='audio/words/amy-scintillating'></a>
SIN-tl-ay-ting
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='scintillating#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='scintillating#' id='context-sound' path='audio/wordcontexts/brian-scintillating'></a>
Gretta was known for her clever, stimulating, and <em>scintillating</em> conversation on all topics.  As the wife of the governor, her <em>scintillating</em> or brilliant talent for conversing easily and well proved useful.  Visiting officials and guests from all over the nation welcomed Gretta&#8217;s <em>scintillating</em>, interesting, and lively company while staying at the governor&#8217;s mansion.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>scintillating</em> conversation?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
One that makes you feel welcome.
</li>
<li class='choice '>
<span class='result'></span>
One that is cut short by something.
</li>
<li class='choice answer '>
<span class='result'></span>
One that is entertaining and engaging.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='scintillating#' id='definition-sound' path='audio/wordmeanings/amy-scintillating'></a>
A <em>scintillating</em> conversation, speech, or performance is brilliantly clever, interesting, and lively.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>stimulating</em>
</span>
</span>
</div>
<a class='quick-help' href='scintillating#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='scintillating#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/scintillating/memory_hooks/4806.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Intell</span></span>i<span class="emp1"><span>g</span></span>e<span class="emp1"><span>n</span></span>t <span class="emp2"><span>Cat</span></span></span></span> My <span class="emp2"><span>cat</span></span> is so <span class="emp1"><span>intell</span></span>i<span class="emp1"><span>g</span></span>e<span class="emp1"><span>n</span></span>t and interesting that my <span class="emp1"><span>intell</span></span>i<span class="emp1"><span>g</span></span>e<span class="emp1"><span>n</span></span>t <span class="emp2"><span>cat</span></span>'s meowing is simply s<span class="emp2"><span>c</span></span><span class="emp1"><span>intill</span></span><span class="emp2"><span>at</span></span>i<span class="emp1"><span>ng</span></span>!
</p>
</div>

<div id='memhook-button-bar'>
<a href="scintillating#" id="add-public-hook" style="" url="https://membean.com/mywords/scintillating/memory_hooks">Use other public hook</a>
<a href="scintillating#" id="memhook-use-own" url="https://membean.com/mywords/scintillating/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='scintillating#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
New York musicians rarely have the time for idle chat and conversation after a gig. Despite popular assumption of our <b>scintillating</b> after-hours, that illusion is overtaken by the constant hustle to juggle a part-time or full-time job, a myriad of errands, a second or third gig of the day, and perhaps a child or two somewhere.
<span class='attribution'>&mdash; Kat Edmonson, American musician</span>
<img alt="Kat edmonson, american musician" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Kat Edmonson, American musician.jpg?qdep8" width="80" />
</li>
<li>
As a general rule, budget hearings for government agencies do not make for <b>scintillating</b> news. But this year, the Spokane county commissioners are bringing a little drama to the process.
<cite class='attribution'>
&mdash;
Spokane Public Radio
</cite>
</li>
<li>
Allen West was the guest speaker with a <b>scintillating</b> talk on the topic, which had guests glued to their seats as they raised $60,000 for the folks at CareNet to continue their work.
<cite class='attribution'>
&mdash;
Albuquerque Journal
</cite>
</li>
<li>
Kahneman uses this scheme to frame a <b>scintillating</b> discussion of his findings in cognitive psychology and behavioral economics, and of the ingenious experiments that tease out the irrational, self-contradictory logics that underlie our choices.
<cite class='attribution'>
&mdash;
Publishers Weekly
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/scintillating/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='scintillating#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>scintill</td>
<td>
&rarr;
</td>
<td class='meaning'>spark, glittering spot</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ing_action' data-tree-url='//cdn1.membean.com/public/data/treexml' href='scintillating#'>
<span class=''></span>
-ing
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>a or of a continuous action</td>
</tr>
</table>
<p>Something <em>scintillating</em> is like a &#8220;glittering spot&#8221; or a &#8220;sparking&#8221; which &#8220;sparks&#8221; interest in it.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='scintillating#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Scintillating" src="https://cdn0.membean.com/public/images/wordimages/cons2/scintillating.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='scintillating#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adroit</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>convivial</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>dapper</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>debonair</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>droll</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>effulgent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>elocution</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>enthrall</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>exuberance</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>exult</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>jocular</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>lustrous</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>vivacious</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>apathy</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>dolorous</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>dour</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>ennui</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>humdrum</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>languid</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>lassitude</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>lethargic</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>lugubrious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>malaise</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>melancholy</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>monotonous</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>morose</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>pallid</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>plaintive</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>saturnine</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>somnolent</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>stupor</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>tedium</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>torpor</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>trite</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>woebegone</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='scintillating#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
scintillate
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to be brilliant, animated, and lively</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="scintillating" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>scintillating</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="scintillating#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

