
<!DOCTYPE html>
<html>
<head>
<title>Word: phantasmagoria | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx1' style='display:none'>If something goes awry, it does not happen in the way that was planned.</p>
<p class='rw-defn idx2' style='display:none'>A chimerical idea is unrealistic, imaginary, or unlikely to be fulfilled.</p>
<p class='rw-defn idx3' style='display:none'>Something that is delusive deceives you by giving a false belief about yourself or the situation you are in.</p>
<p class='rw-defn idx4' style='display:none'>Errant things or people behave in a way that is unacceptable or wrong, such as a missile that travels in the wrong direction or a student who does not follow classroom rules.</p>
<p class='rw-defn idx5' style='display:none'>Something ethereal has a delicate beauty that makes it seem not part of the real world.</p>
<p class='rw-defn idx6' style='display:none'>Something that is evanescent lasts for only a short time before disappearing from sight or memory.</p>
<p class='rw-defn idx7' style='display:none'>A fallacy is an idea or belief that is false.</p>
<p class='rw-defn idx8' style='display:none'>If you think something, such as a statement or idea, is fatuous, you consider it stupid or extremely silly; a fatuous hope is unrealistic.</p>
<p class='rw-defn idx9' style='display:none'>If you describe someone&#8217;s behavior as inane, you think it is completely stupid or without much meaning.</p>
<p class='rw-defn idx10' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx11' style='display:none'>If a mood or feeling is palpable, it is so strong and intense that it is easily noticed and is almost able to be physically felt.</p>
<p class='rw-defn idx12' style='display:none'>To rarefy something is to make it less dense, as in oxygen at high altitudes; this word also refers to purifying or refining something, thereby making it less ordinary or commonplace.</p>
<p class='rw-defn idx13' style='display:none'>Something that is specious seems to be good, sound, or correct but is actually wrong or fake.</p>
<p class='rw-defn idx14' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx15' style='display:none'>Something that is vaporous is not completely formed but foggy and misty; in the same vein, a vaporous idea is insubstantial and vague.</p>
<p class='rw-defn idx16' style='display:none'>A veneer is a thin layer, such as a thin sheet of expensive wood over a cheaper type of wood that gives a false appearance of higher quality; a person can also put forth a false front or veneer.</p>
<p class='rw-defn idx17' style='display:none'>Verisimilitude is something&#8217;s authenticity or appearance of being real or true.</p>
<p class='rw-defn idx18' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 6
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>phantasmagoria</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='phantasmagoria#' id='pronounce-sound' path='audio/words/amy-phantasmagoria'></a>
fan-taz-muh-GOHR-ee-uh
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='phantasmagoria#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='phantasmagoria#' id='context-sound' path='audio/wordcontexts/brian-phantasmagoria'></a>
In the latter years of her life my mentally ill grandmother was afflicted by a weekly <em>phantasmagoria</em> which caused strange dreamlike hallucinations that would appear to her out of thin air.  A <em>phantasmagoria</em> of visions would manifest around her, including little men, her dead father, and the dragon from her favorite childhood storybook.  This weekly <em>phantasmagoria</em> or imaginary fantasy would trouble her greatly; after a few days she would calm down, but then the dreamlike apparitions would come again, throwing her into wild-eyed confusion and frightened despair.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What might a phantasmagoria do?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It would destroy someone mentally because of severe depression.
</li>
<li class='choice '>
<span class='result'></span>
It would make someone fall asleep and have strange dreams.
</li>
<li class='choice answer '>
<span class='result'></span>
It would cause deceptive appearances to arise as if out of nowhere. 
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='phantasmagoria#' id='definition-sound' path='audio/wordmeanings/amy-phantasmagoria'></a>
A <em>phantasmagoria</em> is a shifting scene of fantastic—and immaterial—imagery often associated with a dream, fever, or neurological disorder.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>illusory scene</em>
</span>
</span>
</div>
<a class='quick-help' href='phantasmagoria#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='phantasmagoria#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/phantasmagoria/memory_hooks/116641.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Phant</span></span>om <span class="emp1"><span>Smaug</span></span></span></span> During a particularly vivid <span class="emp3"><span>phant</span></span>a<span class="emp1"><span>smag</span></span>oria the other day I had a vision of a <span class="emp3"><span>phant</span></span>om <span class="emp1"><span>Smaug</span></span> breathing fire at me.
</p>
</div>

<div id='memhook-button-bar'>
<a href="phantasmagoria#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/phantasmagoria/memory_hooks">Use other hook</a>
<a href="phantasmagoria#" id="memhook-use-own" url="https://membean.com/mywords/phantasmagoria/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='phantasmagoria#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Despair, feeding, as it always does, on <b>phantasmagoria</b>, is imperturbably leading literature to the rejection, en masse, of all divine and social laws, towards practical and theoretical evil.
<span class='attribution'>&mdash; Comte de Lautréamont, pseudonym of Isidore Lucien Ducasse, a French poet born in Uruguay</span>
<img alt="Comte de lautréamont, pseudonym of isidore lucien ducasse, a french poet born in uruguay" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Comte de Lautréamont, pseudonym of Isidore Lucien Ducasse, a French poet born in Uruguay.jpg?qdep8" width="80" />
</li>
<li>
Boys and girls, I now give you the _Swamp Thing_ of silent movies. In 2006, a quick-cut <b>phantasmagoria</b> known only as _Brand Upon The Brain!_ clawed its way out of a sinister ooze to wreak havoc on our most delicate memories.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Jack Levine’s “Inauguration,” painted in 1956–58, is a happy discovery; an unresolved, over-painted, expressionist view of American politics in which three presidents (Wilson, Truman, and Eisenhower) have been mashed up in a <b>phantasmagoria</b> of American symbols, urban clutter, and art historical allusion.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/phantasmagoria/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='phantasmagoria#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>phantas</td>
<td>
&rarr;
</td>
<td class='meaning'>imagination, apparition</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='egor_assembly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='phantasmagoria#'>
<span class=''></span>
egor
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>assembly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ia_belonging' data-tree-url='//cdn1.membean.com/public/data/treexml' href='phantasmagoria#'>
<span class=''></span>
-ia
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>things belonging to or relating to</td>
</tr>
</table>
<p>A <em>phantasmagoria</em> includes &#8220;things relating to an assembly of apparitions or (visions of) the imagination.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='phantasmagoria#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Phantasmagoria" src="https://cdn3.membean.com/public/images/wordimages/cons2/phantasmagoria.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='phantasmagoria#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>awry</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>chimerical</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>delusive</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>errant</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>ethereal</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>evanescent</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>fallacy</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>fatuous</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>inane</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>palpable</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>rarefy</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>specious</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>vaporous</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>veneer</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>verisimilitude</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-ants'></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="phantasmagoria" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>phantasmagoria</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="phantasmagoria#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

