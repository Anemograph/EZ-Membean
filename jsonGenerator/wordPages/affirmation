
<!DOCTYPE html>
<html>
<head>
<title>Word: affirmation | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>The abnegation of something is someone&#8217;s giving up their rights or claim to it or denying themselves of it; this action may or may not be in their best interest.</p>
<p class='rw-defn idx1' style='display:none'>If someone in authority abolishes a law or practice, they formally put an end to it.</p>
<p class='rw-defn idx2' style='display:none'>To abrogate an act is to end it by official and sometimes legal means.</p>
<p class='rw-defn idx3' style='display:none'>If you aver that something is the case, you say firmly and strongly that you believe it is true.</p>
<p class='rw-defn idx4' style='display:none'>A conjecture is a theory or guess that is based on information that is not certain or complete.</p>
<p class='rw-defn idx5' style='display:none'>To contravene a law, rule, or agreement is to do something that is not allowed or is forbidden by that law, rule, or agreement.</p>
<p class='rw-defn idx6' style='display:none'>To corroborate something that has been said or reported means to give information or evidence that further supports it.</p>
<p class='rw-defn idx7' style='display:none'>Empirical evidence or study is based on real experience or scientific experiments rather than on unproven theories.</p>
<p class='rw-defn idx8' style='display:none'>When you eradicate something, you tear it up by the roots or remove it completely.</p>
<p class='rw-defn idx9' style='display:none'>If you expostulate with someone, you express strong disagreement with or disapproval of what that person is doing.</p>
<p class='rw-defn idx10' style='display:none'>A fallacy is an idea or belief that is false.</p>
<p class='rw-defn idx11' style='display:none'>Something that is hypothetical is based on possible situations or events rather than actual ones.</p>
<p class='rw-defn idx12' style='display:none'>If something, such as a product or book, has an official&#8217;s imprimatur, that authority has given it their official approval.</p>
<p class='rw-defn idx13' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx14' style='display:none'>To nullify something is to cancel or negate it.</p>
<p class='rw-defn idx15' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx16' style='display:none'>If you postulate something, you assert that it is true without proof; therefore, it can be used as a basis for argument or reasoning—even though there is no factual basis for the assumption.</p>
<p class='rw-defn idx17' style='display:none'>To promulgate something is to officially announce it in order to make it widely known or more specifically to let the public know that a new law has been put into effect.</p>
<p class='rw-defn idx18' style='display:none'>To quell something is to stamp out, quiet, or overcome it.</p>
<p class='rw-defn idx19' style='display:none'>The ratification of a measure or agreement is its official approval or confirmation by all involved.</p>
<p class='rw-defn idx20' style='display:none'>To remonstrate with someone is to tell that person that you strongly disapprove of something they have said or done.</p>
<p class='rw-defn idx21' style='display:none'>If you renege on a deal, agreement, or promise, you do not do what you promised or agreed to do.</p>
<p class='rw-defn idx22' style='display:none'>When someone in power rescinds a law or agreement, they officially end it and state that it is no longer valid.</p>
<p class='rw-defn idx23' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx24' style='display:none'>When you validate something, you confirm that it is sound, true, legal, or worthwhile.</p>
<p class='rw-defn idx25' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx26' style='display:none'>Verisimilitude is something&#8217;s authenticity or appearance of being real or true.</p>
<p class='rw-defn idx27' style='display:none'>The verity of something is the truth or reality of it.</p>
<p class='rw-defn idx28' style='display:none'>If someone vitiates something, they make it less effective, weaker, or reduced in quality.</p>
<p class='rw-defn idx29' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>affirmation</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='affirmation#' id='pronounce-sound' path='audio/words/amy-affirmation'></a>
af-er-MAY-shun
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='affirmation#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='affirmation#' id='context-sound' path='audio/wordcontexts/brian-affirmation'></a>
When Rupert Dill was young, he was asked to make an <em>affirmation</em> or declaration of allegiance to the Pickle Party.  Rupert said that he would remain true to this <em>affirmation</em> or assertion that he would always support the Pickle Party only.  When he became older, however, he started to question that positive statement or <em>affirmation</em> about such a lifelong commitment, for he was beginning to think that the Cucumber Party was far less sour in nature.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an <em>affirmation</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
It is someone&#8217;s strong statement of the truth of something.
</li>
<li class='choice '>
<span class='result'></span>
It is an unpopular opinion that is starting to gain followers.
</li>
<li class='choice '>
<span class='result'></span>
It is a reckless act that has unfortunate consequences.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='affirmation#' id='definition-sound' path='audio/wordmeanings/amy-affirmation'></a>
An <em>affirmation</em> is a declaration or confirmation that something is true.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>confirmation</em>
</span>
</span>
</div>
<a class='quick-help' href='affirmation#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='affirmation#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/affirmation/memory_hooks/3859.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Affirm</span></span>ing <span class="emp2"><span>Motion</span></span></span></span> An <span class="emp1"><span>affir</span></span><span class="emp2"><span>mation</span></span> is an <span class="emp1"><span>affirm</span></span>ing <span class="emp2"><span>motion</span></span> towards a cause or idea.
</p>
</div>

<div id='memhook-button-bar'>
<a href="affirmation#" id="add-public-hook" style="" url="https://membean.com/mywords/affirmation/memory_hooks">Use other public hook</a>
<a href="affirmation#" id="memhook-use-own" url="https://membean.com/mywords/affirmation/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='affirmation#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
<b>Affirmations</b> are like prescriptions for certain aspects of yourself you want to change.
<span class='attribution'>&mdash; Jerry Frankhauser</span>
<img alt="Jerry frankhauser" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Jerry Frankhauser.jpg?qdep8" width="80" />
</li>
<li>
This visual <b>affirmation</b> of accomplishment is soothing, and learning how to tackle stress before it paralyzes you is your ticket to overcoming it.
<cite class='attribution'>
&mdash;
WebMD
</cite>
</li>
<li>
Horton, who had been trying to reach the seven-day <b>affirmation</b> mark for nearly nine months, attributes his success to his refusal to give up.
<cite class='attribution'>
&mdash;
The Onion
</cite>
</li>
<li>
Also at the table, listening and nodding <b>affirmation</b> from time to time as they pecked away at their grilled chicken, were four of their Dream Team mates — Larry Bird, Clyde Drexler, Chris Mullin and John Stockton.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/affirmation/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='affirmation#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='af_towards' data-tree-url='//cdn1.membean.com/public/data/treexml' href='affirmation#'>
<span class=''></span>
af-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to, towards</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='firm_stable' data-tree-url='//cdn1.membean.com/public/data/treexml' href='affirmation#'>
<span class=''></span>
firm
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>strong, stable</td>
</tr>
<tr>
<td class='partform'>-ation</td>
<td>
&rarr;
</td>
<td class='meaning'>act of doing something</td>
</tr>
</table>
<p>When one speaks an <em>affirmation</em> about something, one &#8220;acts strongly towards&#8221; that statement.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='affirmation#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>A Nero Wolfe Mystery</strong><span> The witness gives his affirmation to the bailiff.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/affirmation.jpg' video_url='examplevids/affirmation' video_width='350'></span>
<div id='wt-container'>
<img alt="Affirmation" height="288" src="https://cdn1.membean.com/video/examplevids/affirmation.jpg" width="350" />
<div class='center'>
<a href="affirmation#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='affirmation#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Affirmation" src="https://cdn1.membean.com/public/images/wordimages/cons2/affirmation.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='affirmation#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>aver</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>corroborate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>empirical</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>expostulate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>imprimatur</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>postulate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>promulgate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>ratification</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>remonstrate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>validate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>verisimilitude</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abnegation</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abolish</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abrogate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>conjecture</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>contravene</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>eradicate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>fallacy</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>hypothetical</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>nullify</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>quell</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>renege</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>rescind</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>vitiate</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='affirmation#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
affirmative
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>asserting that something it true</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="affirmation" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>affirmation</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="affirmation#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

