
<!DOCTYPE html>
<html>
<head>
<title>Word: concurrent | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An addendum is an additional section added to the end of a book, document, or speech that gives more information.</p>
<p class='rw-defn idx1' style='display:none'>An adjunct is something that is added to or joined to something else that is larger or more important.</p>
<p class='rw-defn idx2' style='display:none'>When you act in an aloof fashion towards others, you purposely do not participate in activities with them; instead, you remain distant and detached.</p>
<p class='rw-defn idx3' style='display:none'>An appurtenance is a supporting feature, form of equipment, or item associated with a particular activity.</p>
<p class='rw-defn idx4' style='display:none'>An autonomous person makes their own decisions without being influenced by anyone else; an autonomous country or organization is independent and has the power to govern itself.</p>
<p class='rw-defn idx5' style='display:none'>A chronicle is a record of historical events that arranges those events in the correct order in which they happened.</p>
<p class='rw-defn idx6' style='display:none'>A chronological history arranges events in the order that they happened.</p>
<p class='rw-defn idx7' style='display:none'>If you concatenate two or more things, you join them together by linking them one after the other.</p>
<p class='rw-defn idx8' style='display:none'>A concerted effort is intensive and determined work that is performed by two people or more to complete a task.</p>
<p class='rw-defn idx9' style='display:none'>Something is concomitant when it happens at the same time as something else and is connected with it in some fashion.</p>
<p class='rw-defn idx10' style='display:none'>A confluence is a situation where two or more things meet or flow together at a single point or area; a confluence usually refers to two streams joining together.</p>
<p class='rw-defn idx11' style='display:none'>A contemporary object exists at the same time as something else or exists at the current time.</p>
<p class='rw-defn idx12' style='display:none'>Contiguous things are in contact with or near each other; contiguous events happen one right after the other without a break.</p>
<p class='rw-defn idx13' style='display:none'>Two things that are convergent are meeting or coming together at one point in time or space.</p>
<p class='rw-defn idx14' style='display:none'>A situation or thing that is discordant does not fit in with other things; therefore, it is disagreeable, strange, or unpleasant.</p>
<p class='rw-defn idx15' style='display:none'>Things that are disparate are clearly different from each other and belong to different groups or classes.</p>
<p class='rw-defn idx16' style='display:none'>A dissident is someone who disagrees publicly with a government, especially in a country where this is not allowed.</p>
<p class='rw-defn idx17' style='display:none'>Dissonance is an unpleasant situation of opposition in which ideas or actions are not in agreement or harmony; dissonance also refers to a harsh combination of sounds.</p>
<p class='rw-defn idx18' style='display:none'>Divergent opinions differ from each other; divergent paths move apart from one another.</p>
<p class='rw-defn idx19' style='display:none'>An entourage is a group of assistants, servants, and other people who tag along with an important person.</p>
<p class='rw-defn idx20' style='display:none'>Two irreconcilable opinions or points of view are so opposed to each other that it is not possible to accept both of them or reach a settlement between them.</p>
<p class='rw-defn idx21' style='display:none'>The juxtaposition of two objects is the act of positioning them side by side so that the differences between them are more readily visible.</p>
<p class='rw-defn idx22' style='display:none'>Liaison is the cooperation and exchange of information between people or organizations in order to facilitate their joint efforts; this word also refers to a person who coordinates a connection between people or organizations.</p>
<p class='rw-defn idx23' style='display:none'>A nexus is a connection or a series of connections between a number of people, things, or ideas that often form the center of a system or situation.</p>
<p class='rw-defn idx24' style='display:none'>The propinquity of a thing is its nearness in location, relationship, or similarity to another thing.</p>
<p class='rw-defn idx25' style='display:none'>Proximity is how close or near one thing is to another.</p>
<p class='rw-defn idx26' style='display:none'>A schism causes a group or organization to divide into two groups as a result of differences in their aims and beliefs.</p>
<p class='rw-defn idx27' style='display:none'>Two acts that are synchronous occur at the same time.</p>
<p class='rw-defn idx28' style='display:none'>Synergy is the extra energy or additional effectiveness gained when two groups or organizations combine their efforts instead of working separately.</p>
<p class='rw-defn idx29' style='display:none'>Something that is temporal deals with the present and somewhat brief time of this world.</p>
<p class='rw-defn idx30' style='display:none'>Doing something in unison is doing it all together at one time.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>concurrent</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='concurrent#' id='pronounce-sound' path='audio/words/amy-concurrent'></a>
kuhn-KUR-uhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='concurrent#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='concurrent#' id='context-sound' path='audio/wordcontexts/brian-concurrent'></a>
I am taking both painting and creative writing <em>concurrently</em> or simultaneously this year; by taking them during the same semester, I will have two different ways to explore my creative side.  Then, at the end of the semester, an art show will be running <em>concurrently</em> or at the same time as a book festival, which will feature both student art and writing.  These <em>concurrent</em> or parallel events will both happen on the weekend before Christmas break, adding to the excitement.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of <em>concurrent</em> events?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Two dinners on two different nights with the same people.
</li>
<li class='choice '>
<span class='result'></span>
Basketball games that are played one right after the other.
</li>
<li class='choice answer '>
<span class='result'></span>
Two parties happening at the same time.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='concurrent#' id='definition-sound' path='audio/wordmeanings/amy-concurrent'></a>
<em>Concurrent</em> events happen at the same time.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>at the same time</em>
</span>
</span>
</div>
<a class='quick-help' href='concurrent#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='concurrent#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/concurrent/memory_hooks/5941.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Con</span></span>dor <span class="emp3"><span>Current</span></span></span></span> Wouldn't it be wonderful to see a <span class="emp3"><span>current</span></span> of <span class="emp1"><span>con</span></span>dors flying on a <span class="emp3"><span>current</span></span> of air <span class="emp1"><span>con</span></span><span class="emp3"><span>current</span></span>ly in the sky?
</p>
</div>

<div id='memhook-button-bar'>
<a href="concurrent#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/concurrent/memory_hooks">Use other hook</a>
<a href="concurrent#" id="memhook-use-own" url="https://membean.com/mywords/concurrent/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='concurrent#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The increasingly popular practice of taking college courses while in high school—an umbrella that includes dual credit, <b>concurrent</b> enrollment and early college programs—is often a free or low-cost way of accruing college credits, sometimes shaving two years off the time it takes to get an undergraduate degree.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
But to the art world, it introduces two warm points of light in <b>concurrent</b> shows by the Indigenous American artist and filmmaker Sky Hopinka. One, in a new Manhattan gallery, is his New York City solo debut; the other, at Bard College in upstate New York, his first museum survey anywhere.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/concurrent/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='concurrent#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='con_with' data-tree-url='//cdn1.membean.com/public/data/treexml' href='concurrent#'>
<span class='common'></span>
con-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>with, together</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='curr_run' data-tree-url='//cdn1.membean.com/public/data/treexml' href='concurrent#'>
<span class=''></span>
curr
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>run</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ent_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='concurrent#'>
<span class=''></span>
-ent
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p><em>Concurrent</em> events are in the &#8220;state or condition of running together&#8221; in time.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='concurrent#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Monty Python Live at the Hollywood Bowl</strong><span> Although these sprinters and swimmers begin concurrently, they don't do anything else right!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/concurrent.jpg' video_url='examplevids/concurrent' video_width='350'></span>
<div id='wt-container'>
<img alt="Concurrent" height="288" src="https://cdn1.membean.com/video/examplevids/concurrent.jpg" width="350" />
<div class='center'>
<a href="concurrent#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='concurrent#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Concurrent" src="https://cdn3.membean.com/public/images/wordimages/cons2/concurrent.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='concurrent#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>addendum</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>adjunct</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>appurtenance</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>chronicle</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>chronological</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>concatenate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>concerted</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>concomitant</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>confluence</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>contemporary</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>contiguous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>convergent</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>entourage</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>juxtaposition</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>liaison</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>nexus</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>propinquity</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>proximity</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>synchronous</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>synergy</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>temporal</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>unison</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>aloof</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>autonomous</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>discordant</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>disparate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>dissident</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>dissonance</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>divergent</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>irreconcilable</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>schism</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="concurrent" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>concurrent</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="concurrent#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

