
<!DOCTYPE html>
<html>
<head>
<title>Word: zealous | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Zealous-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/zealous-large.jpg?qdep8" />
</div>
<a href="zealous#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When you have ardor for something, you have an intense feeling of love, excitement, and admiration for it.</p>
<p class='rw-defn idx1' style='display:none'>An assiduous person works hard to ensure that something is done properly and completely.</p>
<p class='rw-defn idx2' style='display:none'>If you demur, you delay in doing or mildly object to something because you don&#8217;t really want to do it.</p>
<p class='rw-defn idx3' style='display:none'>Someone or something that is dilatory is slow and causes delay.</p>
<p class='rw-defn idx4' style='display:none'>An animal or person that is docile is not aggressive; rather, they are well-behaved, quiet, and easy to control.</p>
<p class='rw-defn idx5' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx6' style='display:none'>If something enthralls you, it makes you so interested and excited that you give it all your attention.</p>
<p class='rw-defn idx7' style='display:none'>If you show exuberance, you display great excitement, energy, and enthusiasm.</p>
<p class='rw-defn idx8' style='display:none'>Fanaticism is the condition of being overly enthusiastic or eager about a cause to the point of being extreme and unreasonable about it.</p>
<p class='rw-defn idx9' style='display:none'>Febrile behavior is full of nervous energy and activity; a sick person can be febrile as well, that is, feverish or hot.</p>
<p class='rw-defn idx10' style='display:none'>A fervid person has strong feelings about something, such as a humanitarian cause; therefore, they are very sincere and enthusiastic about it.</p>
<p class='rw-defn idx11' style='display:none'>Frenetic activity is done quickly with lots of energy but is also uncontrolled and disorganized; someone who is in a huge hurry often displays this type of behavior.</p>
<p class='rw-defn idx12' style='display:none'>When you are impassioned about a cause or idea, you are very passionate or highly emotionally charged about it.</p>
<p class='rw-defn idx13' style='display:none'>If someone is indefatigable, they never shows signs of getting tired.</p>
<p class='rw-defn idx14' style='display:none'>If you are indifferent about something, you are uninterested or neutral about it, not caring either in a positive or negative way.</p>
<p class='rw-defn idx15' style='display:none'>Someone who is irresolute is unable to decide what to do.</p>
<p class='rw-defn idx16' style='display:none'>Someone who is jaded is bored with or weary of the world because they have had too much experience with it.</p>
<p class='rw-defn idx17' style='display:none'>Lassitude is a state of tiredness, lack of energy, and having little interest in what&#8217;s going on around you.</p>
<p class='rw-defn idx18' style='display:none'>If you are lethargic, you are tired, lack energy, and are unwilling to exert effort.</p>
<p class='rw-defn idx19' style='display:none'>When you are listless, you lack energy and interest and are unwilling to exert any effort.</p>
<p class='rw-defn idx20' style='display:none'>Malaise is a feeling of discontent, general unease, or even illness in a person or group for which there does not seem to be a quick and easy solution because the cause of it is not clear.</p>
<p class='rw-defn idx21' style='display:none'>Someone who is phlegmatic stays calm and unemotional even in dangerous or exciting situations.</p>
<p class='rw-defn idx22' style='display:none'>To be punctilious is to pay precise attention to detail.</p>
<p class='rw-defn idx23' style='display:none'>A recalcitrant animal or person is difficult to control and refuses to obey orders—even after stiff punishment.</p>
<p class='rw-defn idx24' style='display:none'>Someone who is sedulous works hard and performs tasks very carefully and thoroughly, not stopping their work until it is completely accomplished.</p>
<p class='rw-defn idx25' style='display:none'>If your body is affected by torpor, you are severely lacking in energy; therefore, you are idle—and can even be numb.</p>
<p class='rw-defn idx26' style='display:none'>If you are unflagging while doing a task, you are untiring when working upon it and do not stop until it is finished.</p>
<p class='rw-defn idx27' style='display:none'>When you have a vehement feeling about something, you feel very strongly or intensely about it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>zealous</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='zealous#' id='pronounce-sound' path='audio/words/amy-zealous'></a>
ZEL-uhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='zealous#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='zealous#' id='context-sound' path='audio/wordcontexts/brian-zealous'></a>
Zacchariah, a <em>zealous</em> and highly enthusiastic soccer fan, wore his team colors from head to toe every game&#8212;even his face was painted green and blue!  This fully dedicated and <em>zealous</em> sports fan could be seen running up and down the sidelines every game, earnestly yelling out passionate pleas for his team to score.  The only person who did not appreciate Zacchariah&#8217;s fired-up nature was Zacchariah Jr., the team&#8217;s goalie, who found his father&#8217;s <em>zeal</em> and excessive enthusiasm rather embarrassing.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What action might a <em>zealous</em> person take?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Cheer for their friend&#8217;s team during a game.
</li>
<li class='choice '>
<span class='result'></span>
Try to calm someone down during a fight.
</li>
<li class='choice answer '>
<span class='result'></span>
Devote themselves to volunteering for a cause.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='zealous#' id='definition-sound' path='audio/wordmeanings/amy-zealous'></a>
Someone who is <em>zealous</em> spends a lot of time, energy, and effort to support something—  notably that of a political or religious nature—because they believe in it very strongly.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>highly enthusiastic about</em>
</span>
</span>
</div>
<a class='quick-help' href='zealous#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='zealous#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/zealous/memory_hooks/5541.json'></span>
<p>
<span class="emp0"><span>N<span class="emp3"><span>eil</span></span>'s S<span class="emp3"><span>eal</span></span> Z<span class="emp3"><span>eal</span></span></span></span>  N<span class="emp3"><span>eil</span></span> has z<span class="emp3"><span>eal</span></span> for s<span class="emp3"><span>eal</span></span>s!  He has fifty pet s<span class="emp3"><span>eal</span></span>s, all honking around his house.  All his spare money goes towards supporting s<span class="emp3"><span>eal</span></span>s, making N<span class="emp3"><span>eil</span></span>'s s<span class="emp3"><span>eal</span></span> z<span class="emp3"><span>eal</span></span> legendary.
</p>
</div>

<div id='memhook-button-bar'>
<a href="zealous#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/zealous/memory_hooks">Use other hook</a>
<a href="zealous#" id="memhook-use-own" url="https://membean.com/mywords/zealous/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='zealous#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
While the converted are often the most <b>zealous</b> agents for change, one Alabama resident who triumphed against the scale finds the state requirements somewhat troubling.
<cite class='attribution'>
&mdash;
WebMD
</cite>
</li>
<li>
These <b>zealous</b> wizards handed Xerox an astounding lead in information technology in the early 1980s, but by the end of the decade, Xerox watched as upstarts like Apple and Microsoft grew wealthy off Xerox’s discoveries.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
Other cities also face tension over a lack of adequate parking, but officials say they don’t endorse the use of specific performance targets because they can lead to overly <b>zealous</b> ticket writing.
<cite class='attribution'>
&mdash;
Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/zealous/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='zealous#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='zeal_eager' data-tree-url='//cdn1.membean.com/public/data/treexml' href='zealous#'>
<span class=''></span>
zeal
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>eager rivalry, fierceness</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ous_full' data-tree-url='//cdn1.membean.com/public/data/treexml' href='zealous#'>
<span class=''></span>
-ous
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>full of, having</td>
</tr>
</table>
<p>A <em>zealous</em> person is &#8220;full of eager rivalry&#8221; in regards to competition, and &#8220;has fierceness&#8221; when it comes to supporting a cause or something one truly believes in.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='zealous#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Billy in the Street</strong><span> This guy is really zealous about Madonna, perhaps overly so.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/zealous.jpg' video_url='examplevids/zealous' video_width='350'></span>
<div id='wt-container'>
<img alt="Zealous" height="288" src="https://cdn1.membean.com/video/examplevids/zealous.jpg" width="350" />
<div class='center'>
<a href="zealous#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='zealous#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Zealous" src="https://cdn3.membean.com/public/images/wordimages/cons2/zealous.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='zealous#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>ardor</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>assiduous</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>enthrall</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>exuberance</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>fanaticism</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>febrile</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>fervid</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>frenetic</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>impassioned</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>indefatigable</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>punctilious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>sedulous</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>unflagging</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>vehement</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>demur</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>dilatory</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>docile</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>indifferent</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>irresolute</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>jaded</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>lassitude</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>lethargic</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>listless</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>malaise</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>phlegmatic</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>recalcitrant</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>torpor</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='zealous#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
zeal
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>enthusiasm for a cause</td>
</tr>
<tr>
<td class='wordform'>
<span>
zealot
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>one who is overly enthusiastic for a cause</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="zealous" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>zealous</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="zealous#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

