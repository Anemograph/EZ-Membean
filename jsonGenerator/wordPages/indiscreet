
<!DOCTYPE html>
<html>
<head>
<title>Word: indiscreet | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you describe a decision, rule, or plan as arbitrary, you think that it was decided without any thought, standard, or system to guide it; therefore, it can seem unfair.</p>
<p class='rw-defn idx1' style='display:none'>An audacious person acts with great daring and sometimes reckless bravery—despite risks and warnings from other people—in order to achieve something.</p>
<p class='rw-defn idx2' style='display:none'>If you describe a person&#8217;s behavior as brazen, you mean that they are not embarrassed by anything they do—they simply don&#8217;t care what other people think about them.</p>
<p class='rw-defn idx3' style='display:none'>A cavalier person does not seem to care about rules, principles, or other people&#8217;s feelings, no matter how dire or serious a situation may be.</p>
<p class='rw-defn idx4' style='display:none'>If you are chary of doing something, you are cautious or timid about doing it because you are afraid that something bad will happen.</p>
<p class='rw-defn idx5' style='display:none'>A chivalrous man behaves in a polite, kind, generous, and honorable way, especially towards women.</p>
<p class='rw-defn idx6' style='display:none'>If you are ___, you are cautious; you think carefully about something before saying or doing it.</p>
<p class='rw-defn idx7' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx8' style='display:none'>If you deliberate, you think about or discuss something very carefully, especially before making an important decision.</p>
<p class='rw-defn idx9' style='display:none'>If you refer to the enormity of a situation, problem, or event, you mean that it is very evil or morally offensive; this word has nothing to do with physical size.</p>
<p class='rw-defn idx10' style='display:none'>Someone who is frugal spends very little money—and even then only on things that are absolutely necessary.</p>
<p class='rw-defn idx11' style='display:none'>If you think someone is showing hubris, you think that they are demonstrating excessive pride and vanity.</p>
<p class='rw-defn idx12' style='display:none'>Something that is imperceptible is either impossible to be perceived by the mind or is very difficult to perceive.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is impetuous does things quickly and rashly without thinking carefully first.</p>
<p class='rw-defn idx14' style='display:none'>When you act in an imprudent fashion, you do something that is unwise, is lacking in good judgment, or has no forethought.</p>
<p class='rw-defn idx15' style='display:none'>If someone demonstrates impudence, they behave in a very rude or disrespectful way.</p>
<p class='rw-defn idx16' style='display:none'>If an idea or thought is incisive, it is expressed in a penetrating and knowledgeable manner that is clear and brief; additionally, it can demonstrate impressive understanding of related ideas or thoughts.</p>
<p class='rw-defn idx17' style='display:none'>Something that is inconspicuous does not attract attention and is not easily seen or noticed because it is small or ordinary.</p>
<p class='rw-defn idx18' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx19' style='display:none'>A judicious person shows good or sound judgment because they are wise and careful in making decisions.</p>
<p class='rw-defn idx20' style='display:none'>Malfeasance is an unlawful act, especially one committed by a trusted public official.</p>
<p class='rw-defn idx21' style='display:none'>A myopic person is unable to visualize the long-term negative consequences of their current actions; rather, they are narrowly focused upon short-term results.</p>
<p class='rw-defn idx22' style='display:none'>Someone is overweening when they are not modest; rather, they think way too much of themselves and let everyone know about it.</p>
<p class='rw-defn idx23' style='display:none'>A peccadillo is a slight or minor offense that can be overlooked.</p>
<p class='rw-defn idx24' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx25' style='display:none'>To be politic in a decision is to be socially wise and diplomatic.</p>
<p class='rw-defn idx26' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx27' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx28' style='display:none'>Protocol is the rules of conduct or proper behavior that a social situation demands.</p>
<p class='rw-defn idx29' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx30' style='display:none'>A sagacious person is wise, intelligent, and has the ability to make good practical decisions.</p>
<p class='rw-defn idx31' style='display:none'>If you commit a solecism, you make an error of some kind, such as one of a grammatical or social nature.</p>
<p class='rw-defn idx32' style='display:none'>To act with temerity is to act in a carelessly and irresponsibly bold way.</p>
<p class='rw-defn idx33' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx34' style='display:none'>If you say someone&#8217;s behavior is unseemly, you disapprove of it because it is not in good taste or not suitable for a particular situation.</p>
<p class='rw-defn idx35' style='display:none'>If you are being wary about a situation, you are being careful, cautious, or on your guard because you are concerned about something.</p>
<p class='rw-defn idx36' style='display:none'>A yokel is uneducated, naive, and unsophisticated; they do not know much about modern life or ideas because they sequester themselves in a rural setting.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>indiscreet</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='indiscreet#' id='pronounce-sound' path='audio/words/amy-indiscreet'></a>
in-di-SKREET
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='indiscreet#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='indiscreet#' id='context-sound' path='audio/wordcontexts/brian-indiscreet'></a>
While on break at the office, Penny was <em>indiscreet</em> or careless in speaking about Bart, not knowing that she was being overheard.  After hearing her speak publicly in such a way about him, Bart yelled at Penny for being so <em>indiscreet</em> and insensitive.  Penny&#8217;s lack of judgment or <em>indiscreet</em> behavior caused her to be labeled as an empty-headed and inconsiderate gossip.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of <em>indiscreet</em> behavior?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Sharing a friend&#8217;s secret at a dinner party.
</li>
<li class='choice '>
<span class='result'></span>
Arriving late to a formal cocktail party.
</li>
<li class='choice '>
<span class='result'></span>
Not offering to help clean up after dinner.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='indiscreet#' id='definition-sound' path='audio/wordmeanings/amy-indiscreet'></a>
Someone who is <em>indiscreet</em> shows lack of judgment, especially because they say or do things in public that should only be said or done privately—if at all.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>careless</em>
</span>
</span>
</div>
<a class='quick-help' href='indiscreet#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='indiscreet#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/indiscreet/memory_hooks/4726.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Dis</span></span>s<span class="emp2"><span>in</span></span>g the People of <span class="emp3"><span>Crete</span></span></span></span> I cannot believe that you would be so <span class="emp2"><span>in</span></span><span class="emp1"><span>dis</span></span><span class="emp3"><span>creet</span></span> as to <span class="emp1"><span>dis</span></span>respectfully <span class="emp1"><span>dis</span></span> the people of <span class="emp3"><span>Crete</span></span>, especially right <span class="emp2"><span>in</span></span> front of them!  You make us look bad!
</p>
</div>

<div id='memhook-button-bar'>
<a href="indiscreet#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/indiscreet/memory_hooks">Use other hook</a>
<a href="indiscreet#" id="memhook-use-own" url="https://membean.com/mywords/indiscreet/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='indiscreet#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
All enterprises that are entered into with <b>indiscreet</b> zeal may be pursued with great vigor at first, but are sure to collapse in the end.
<span class='attribution'>&mdash; Tacitus, Roman historian and politician</span>
<img alt="Tacitus, roman historian and politician" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Tacitus, Roman historian and politician.jpg?qdep8" width="80" />
</li>
<li>
Hoping that my reasons for not accepting your kind offer may prove satisfactory, and that you will excuse any <b>indiscreet</b> remark I may have made, I remain, your obedient servant.
<cite class='attribution'>
&mdash;
NPR, from _The Southern Journey of a Civil War Marine_
</cite>
</li>
<li>
I think it was <b>indiscreet</b> and showed poor judgement on the part of the outgoing mayor to bring forth information about an unresolved, ongoing investigation and to make it public.
<cite class='attribution'>
&mdash;
WTRF-TV
</cite>
</li>
<li>
In a dream I had the other night, an <b>indiscreet</b> White House aide dropped on my desk the memos the President is getting from various interested parties.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/indiscreet/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='indiscreet#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='in_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='indiscreet#'>
<span class='common'></span>
in-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='dis_apart' data-tree-url='//cdn1.membean.com/public/data/treexml' href='indiscreet#'>
<span class='common'></span>
dis-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>apart</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='cret_sift' data-tree-url='//cdn1.membean.com/public/data/treexml' href='indiscreet#'>
<span class=''></span>
cret
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>sift, discern, perceive</td>
</tr>
</table>
<p>When someone makes an <em>indiscreet</em> comment, he is &#8220;not perceiving&#8221; what to keep &#8220;apart&#8221; from public ears, or &#8220;not sifting&#8221; what he should say for what he should not.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='indiscreet#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Indiscreet" src="https://cdn2.membean.com/public/images/wordimages/cons2/indiscreet.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='indiscreet#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>arbitrary</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>audacious</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>brazen</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>cavalier</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>enormity</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>hubris</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>impetuous</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>imprudent</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>impudence</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>malfeasance</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>myopic</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>overweening</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>peccadillo</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>solecism</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>temerity</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>unseemly</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>yokel</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='4' class = 'rw-wordform notlearned'><span>chary</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>chivalrous</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>circumspect</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>deliberate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>frugal</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>imperceptible</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>incisive</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>inconspicuous</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>judicious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>politic</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>protocol</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>sagacious</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>wary</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='indiscreet#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
indiscretion
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>lack of good judgment in what is publicly said</td>
</tr>
<tr>
<td class='wordform'>
<span>
discretion
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>freedom of choice; quality of being careful in action</td>
</tr>
<tr>
<td class='wordform'>
<span>
discretionary
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>free to use in the way one wants</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="indiscreet" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>indiscreet</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="indiscreet#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

