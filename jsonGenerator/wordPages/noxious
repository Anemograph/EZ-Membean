
<!DOCTYPE html>
<html>
<head>
<title>Word: noxious | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Noxious-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/noxious-large.jpg?qdep8" />
</div>
<a href="noxious#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>If you abhor something, you dislike it very much, usually because you think it&#8217;s immoral.</p>
<p class='rw-defn idx1' style='display:none'>If you abominate something, you hate it because you think it is extremely wrong and unacceptable.</p>
<p class='rw-defn idx2' style='display:none'>The adjective auspicious describes a positive beginning of something, such as a new business, or a certain time that looks to have a good chance of success or prosperity.</p>
<p class='rw-defn idx3' style='display:none'>Someone is baleful if they are filled with bad intent, anger, or hatred towards another person.</p>
<p class='rw-defn idx4' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx5' style='display:none'>Something brackish is distasteful and unpleasant usually because something else is mixed in with it; brackish water, for instance, is a mixture of fresh and salt water and cannot be drunk.</p>
<p class='rw-defn idx6' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx7' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx8' style='display:none'>A contagion is a disease—or the transmission of that disease—that is easily spread from one person to another through touch or the air.</p>
<p class='rw-defn idx9' style='display:none'>Something that is deleterious has a harmful effect.</p>
<p class='rw-defn idx10' style='display:none'>Fetid water or air has a bad odor, usually caused by decay.</p>
<p class='rw-defn idx11' style='display:none'>If you describe something as heinous, you mean that is extremely evil, shocking, and bad.</p>
<p class='rw-defn idx12' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx13' style='display:none'>If you describe ideas as jejune, you are saying they are childish and uninteresting; the adjective jejune also describes those having little experience, maturity, or knowledge.</p>
<p class='rw-defn idx14' style='display:none'>Something loathsome is offensive, disgusting, and brings about intense dislike.</p>
<p class='rw-defn idx15' style='display:none'>A malevolent person or thing is evil due to deliberate attempts to cause harm.</p>
<p class='rw-defn idx16' style='display:none'>Malfeasance is an unlawful act, especially one committed by a trusted public official.</p>
<p class='rw-defn idx17' style='display:none'>Malice is a type of hatred that causes a strong desire to harm others both physically and emotionally.</p>
<p class='rw-defn idx18' style='display:none'>A malignant thing or person, such as a tumor or criminal, is deadly or does great harm.</p>
<p class='rw-defn idx19' style='display:none'>A miasma is a harmful influence or evil feeling that seems to surround a person or place.</p>
<p class='rw-defn idx20' style='display:none'>If something is noisome, it is extremely unpleasant, especially because it is very dirty or has an awful smell.</p>
<p class='rw-defn idx21' style='display:none'>If you describe people or things as odious, you think that they are extremely unpleasant.</p>
<p class='rw-defn idx22' style='display:none'>A pathogen is an organism, such as a bacterium or virus, that causes disease.</p>
<p class='rw-defn idx23' style='display:none'>A perilous situation is highly dangerous or extremely risky.</p>
<p class='rw-defn idx24' style='display:none'>Something that is pernicious is very harmful or evil, often in a way that is hidden or not quickly noticed.</p>
<p class='rw-defn idx25' style='display:none'>A pestilence is either an infectious disease that is deadly or an agent of some kind that is destructive or dangerous.</p>
<p class='rw-defn idx26' style='display:none'>Potable water is clean and safe to drink.</p>
<p class='rw-defn idx27' style='display:none'>Something that is propitious shows favorable conditions.</p>
<p class='rw-defn idx28' style='display:none'>Something rancid is way past fresh; in fact, it is decomposing quickly and has a bad taste or smell.</p>
<p class='rw-defn idx29' style='display:none'>Something that is repulsive is offensive, highly unpleasant, or just plain disgusting.</p>
<p class='rw-defn idx30' style='display:none'>When you experience revulsion, you feel a great deal of disgust or extreme dislike for something.</p>
<p class='rw-defn idx31' style='display:none'>A salubrious place or area is pleasant, clean, healthy, and comfortable to live in.</p>
<p class='rw-defn idx32' style='display:none'>A salutary experience is beneficial to you since it strengthens you in some way, although it may be unpleasant as you undergo it; this word also refers to promoting good health.</p>
<p class='rw-defn idx33' style='display:none'>To taint is to give an undesirable quality that damages a person&#8217;s reputation or otherwise spoils something.</p>
<p class='rw-defn idx34' style='display:none'>Something vile is evil, very unpleasant, horrible, or extremely bad.</p>
<p class='rw-defn idx35' style='display:none'>A virulent disease is very dangerous and spreads very quickly.</p>
<p class='rw-defn idx36' style='display:none'>The adjective wholesome can refer to something that is good for the health of your body and also for your moral health.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>noxious</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='noxious#' id='pronounce-sound' path='audio/words/amy-noxious'></a>
NOK-shuhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='noxious#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='noxious#' id='context-sound' path='audio/wordcontexts/brian-noxious'></a>
In the comic book &#8220;Darth Onion Attacks,&#8221; the brilliant villain formulated a harmful, <em>noxious</em> plan to wipe out all the computers in the city.  He plotted to fry them with surges of electricity, and hoped that the <em>noxious</em> or toxic vapors from the frying computers would not let their users stop coughing.  This destructive and <em>noxious</em> plan could have brought the city to a standstill.  Luckily detective Welkin Walker stopped Darth Onion from succeeding in his <em>noxious</em>, foul, and injurious plot.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What would you do with a <em>noxious</em> substance?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
You would keep it away from an open flame since it catches fire easily.
</li>
<li class='choice answer '>
<span class='result'></span>
You would handle it carefully since it could be poisonous.
</li>
<li class='choice '>
<span class='result'></span>
You would use very little of it while cooking since it has a strong flavor.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='noxious#' id='definition-sound' path='audio/wordmeanings/amy-noxious'></a>
Something that is <em>noxious</em> is harmful, extremely unpleasant, and often poisonous.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>harmful</em>
</span>
</span>
</div>
<a class='quick-help' href='noxious#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='noxious#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/noxious/memory_hooks/3073.json'></span>
<p>
<span class="emp0"><span>Inge<span class="emp3"><span>nious</span></span> T<span class="emp1"><span>ox</span></span>in</span></span> The <span class="emp3"><span>n</span></span><span class="emp1"><span>ox</span></span><span class="emp3"><span>ious</span></span> t<span class="emp1"><span>ox</span></span>in from the inge<span class="emp3"><span>nious</span></span> mastermind would have destroyed every living thing in the entire solar system if the inge<span class="emp3"><span>nious</span></span> criminal had not been forced to inhale his own <span class="emp3"><span>n</span></span><span class="emp1"><span>ox</span></span><span class="emp3"><span>ious</span></span> inge<span class="emp3"><span>nious</span></span> t<span class="emp1"><span>ox</span></span>in first.
</p>
</div>

<div id='memhook-button-bar'>
<a href="noxious#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/noxious/memory_hooks">Use other hook</a>
<a href="noxious#" id="memhook-use-own" url="https://membean.com/mywords/noxious/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='noxious#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
The way to combat <b>noxious</b> ideas is with other ideas. The way to combat falsehoods is with truth.
<span class='attribution'>&mdash; William O. Douglas, former US Supreme Court Justice</span>
<img alt="William o" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/William O. Douglas, former US Supreme Court Justice.jpg?qdep8" width="80" />
</li>
<li>
The fuel is abundant, the process safe and the waste, though radioactive, is far less <b>noxious</b> than the stuff left over from fission.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Arne Ljungqvist, chairman of the International Olympic Committee’s medical commission, said Monday an analysis by four independent experts of data supplied by Beijing organizers found heat and humidity might be a greater threat to athletes than the city’s <b>noxious</b> air.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/noxious/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='noxious#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='noc_harm' data-tree-url='//cdn1.membean.com/public/data/treexml' href='noxious#'>
<span class=''></span>
noc
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>hurt, damage</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ious_nature' data-tree-url='//cdn1.membean.com/public/data/treexml' href='noxious#'>
<span class=''></span>
-ious
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of the nature of</td>
</tr>
</table>
<p>Something that is <em>noxious</em> causes &#8220;hurt&#8221; or &#8220;damage.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='noxious#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: KaFaDoKyA NEWS</strong><span> A noxious gas in Moscow.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/noxious.jpg' video_url='examplevids/noxious' video_width='350'></span>
<div id='wt-container'>
<img alt="Noxious" height="288" src="https://cdn1.membean.com/video/examplevids/noxious.jpg" width="350" />
<div class='center'>
<a href="noxious#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='noxious#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Noxious" src="https://cdn2.membean.com/public/images/wordimages/cons2/noxious.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='noxious#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abhor</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abominate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>baleful</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>brackish</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>contagion</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>deleterious</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>fetid</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>heinous</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>loathsome</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>malevolent</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>malfeasance</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>malice</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>malignant</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>miasma</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>noisome</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>odious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>pathogen</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>perilous</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>pernicious</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>pestilence</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>rancid</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>repulsive</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>revulsion</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>taint</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>vile</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>virulent</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>auspicious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>jejune</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>potable</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>propitious</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>salubrious</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>salutary</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>wholesome</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='noxious#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
obnoxious
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>very annoying, offensive, and unpleasant</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="noxious" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>noxious</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="noxious#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

