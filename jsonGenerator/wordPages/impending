
<!DOCTYPE html>
<html>
<head>
<title>Word: impending | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Impending-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/impending-large.jpg?qdep8" />
</div>
<a href="impending#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When something bedevils you, it causes you a lot of problems and difficulties over a period of time.</p>
<p class='rw-defn idx1' style='display:none'>When you beleaguer someone, you act with the intent to annoy or harass that person repeatedly until they finally give you what you want.</p>
<p class='rw-defn idx2' style='display:none'>If you are beset by something, you are experiencing serious problems or dangers because of it.</p>
<p class='rw-defn idx3' style='display:none'>A cataclysm is a violent, sudden event that causes great change and/or harm.</p>
<p class='rw-defn idx4' style='display:none'>If you circumvent something, such as a rule or restriction, you try to get around it in a clever and perhaps dishonest way.</p>
<p class='rw-defn idx5' style='display:none'>To contravene a law, rule, or agreement is to do something that is not allowed or is forbidden by that law, rule, or agreement.</p>
<p class='rw-defn idx6' style='display:none'>Duress is the application or threat of force to compel someone to act in a particular way.</p>
<p class='rw-defn idx7' style='display:none'>If a fact or idea eludes you, you cannot remember or understand it; if you elude someone, you manage to escape or hide from them.</p>
<p class='rw-defn idx8' style='display:none'>If something encumbers you, it makes it difficult for you to move freely or do what you want.</p>
<p class='rw-defn idx9' style='display:none'>A halcyon time is calm, peaceful, and undisturbed.</p>
<p class='rw-defn idx10' style='display:none'>When there is havoc, there is great disorder, widespread destruction, and much confusion.</p>
<p class='rw-defn idx11' style='display:none'>An imminent event, especially an event that is unpleasant, is almost certain to happen within the very near future.</p>
<p class='rw-defn idx12' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx13' style='display:none'>An impediment is something that blocks or obstructs progress; it can also be a weakness or disorder, such as having difficulty with speaking.</p>
<p class='rw-defn idx14' style='display:none'>Something that is ineluctable is impossible to avoid or escape, however much you try.</p>
<p class='rw-defn idx15' style='display:none'>The adjective inexorable describes a process that is impossible to stop once it has begun.</p>
<p class='rw-defn idx16' style='display:none'>An irrevocable action or decision is impossible to change, reverse, or stop.</p>
<p class='rw-defn idx17' style='display:none'>When you are in jeopardy, you are in danger or trouble of some kind.</p>
<p class='rw-defn idx18' style='display:none'>Something that is latent, such as an undiscovered talent or undiagnosed disease, exists but is not active or has not developed at the moment—it may develop or become active in the future.</p>
<p class='rw-defn idx19' style='display:none'>To obviate a problem is to eliminate it or do something that makes solving the problem unnecessary.</p>
<p class='rw-defn idx20' style='display:none'>If something, such as a road, is occluded, it has been closed off or blocked; therefore, cars are prevented from continuing on their way until the road is opened once again.</p>
<p class='rw-defn idx21' style='display:none'>An ominous sign predicts or shows that something bad will happen in the near future.</p>
<p class='rw-defn idx22' style='display:none'>To parry is to ward something off or deflect it.</p>
<p class='rw-defn idx23' style='display:none'>A portent is a warning that indicates what is likely to happen in the future, which is usually something unpleasant.</p>
<p class='rw-defn idx24' style='display:none'>To presage a future event is to give a sign or warning that something (usually) bad is about to happen.</p>
<p class='rw-defn idx25' style='display:none'>Something or someone that is prophetic has the quality or ability to predict the future.</p>
<p class='rw-defn idx26' style='display:none'>A state of quiescence is one of quiet and restful inaction.</p>
<p class='rw-defn idx27' style='display:none'>A serene place or situation is peaceful and calm.</p>
<p class='rw-defn idx28' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx29' style='display:none'>If something is tranquil, it is peaceful, calm, and quiet.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>impending</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='impending#' id='pronounce-sound' path='audio/words/amy-impending'></a>
im-PEN-ding
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='impending#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='impending#' id='context-sound' path='audio/wordcontexts/brian-impending'></a>
The <em>impending</em> snowfall has us worried, so we are running to the store before it hits, which could be within the next few hours.  The <em>impending</em> or approaching danger is that the snow may be too heavy for the trees, which may snap and break power lines.  With an <em>impending</em> or threatening risk of power outages, people are checking to make sure their generators are in good working order and that they have plenty of fuel.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an <em>impending</em> event?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A joyful event that only recently happened.
</li>
<li class='choice '>
<span class='result'></span>
A planned event that may not happen this year.
</li>
<li class='choice answer '>
<span class='result'></span>
A negative event that is going to happen soon.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='impending#' id='definition-sound' path='audio/wordmeanings/amy-impending'></a>
An <em>impending</em> event is approaching fast or is about to occur; this word usually has a negative implication, referring to something threatening or harmful coming.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>approaching quickly</em>
</span>
</span>
</div>
<a class='quick-help' href='impending#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='impending#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/impending/memory_hooks/2882.json'></span>
<p>
<span class="emp0"><span>Hurricane Sus<span class="emp1"><span>pend</span></span>s</span></span> The im<span class="emp1"><span>pending</span></span> hurricane is sus<span class="emp1"><span>pending</span></span> all activities for the next day to allow people to prepare for its arrival.
</p>
</div>

<div id='memhook-button-bar'>
<a href="impending#" id="add-public-hook" style="" url="https://membean.com/mywords/impending/memory_hooks">Use other public hook</a>
<a href="impending#" id="memhook-use-own" url="https://membean.com/mywords/impending/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='impending#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
My feelings would never change, but . . . now she was gone, and this thought meant more to me than the <b>impending</b> destruction of the world.
<span class='attribution'>&mdash; Roger Zelazny, American poet and fantasy/science fiction writer, from _The Courts of Chaos_</span>
<img alt="Science fiction writer, from _the courts of chaos_" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Roger Zelazny, American poet and fantasy/science fiction writer, from _The Courts of Chaos_.jpg?qdep8" width="80" />
</li>
<li>
As giant Saudi Arabia tenses for what some believe is an <b>impending</b> civil war between autocratic rulers and Islamist fanatics, little Bahrain is cutting a different course.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Insects are among Earth’s most abundant life forms, representing a staggering 80 percent of all animal species. But in recent years, reports of dwindling bug populations have led some experts to warn of an <b>impending</b> “insect apocalypse.”
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
The story tracks Adam and Miranda’s star-crossed romance against the backdrop of the tragedy they know and expect—Adam’s <b>impending</b> deployment to serve in World War I—and the pandemic that takes them by surprise.
<cite class='attribution'>
&mdash;
Texas Monthly
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/impending/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='impending#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='im_in' data-tree-url='//cdn1.membean.com/public/data/treexml' href='impending#'>
<span class='common'></span>
im-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>in, on</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pend_hang' data-tree-url='//cdn1.membean.com/public/data/treexml' href='impending#'>
<span class='common'></span>
pend
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>hang, weigh</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ing_action' data-tree-url='//cdn1.membean.com/public/data/treexml' href='impending#'>
<span class=''></span>
-ing
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>a or of a continuous action</td>
</tr>
</table>
<p><em>Impending</em> doom is &#8220;continuously hanging on&#8221; you.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='impending#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Futurama: Killer App</strong><span> Impending news!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/impending.jpg' video_url='examplevids/impending' video_width='350'></span>
<div id='wt-container'>
<img alt="Impending" height="288" src="https://cdn1.membean.com/video/examplevids/impending.jpg" width="350" />
<div class='center'>
<a href="impending#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='impending#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Impending" src="https://cdn2.membean.com/public/images/wordimages/cons2/impending.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='impending#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>bedevil</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>beleaguer</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>beset</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>cataclysm</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>duress</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>encumber</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>havoc</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>imminent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>ineluctable</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>inexorable</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>irrevocable</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>jeopardy</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>ominous</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>portent</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>presage</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>prophetic</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='4' class = 'rw-wordform notlearned'><span>circumvent</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>contravene</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>elude</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>halcyon</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>impediment</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>latent</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>obviate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>occlude</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>parry</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>quiescence</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>serene</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>tranquil</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="impending" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>impending</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="impending#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

