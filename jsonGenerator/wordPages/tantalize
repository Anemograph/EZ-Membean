
<!DOCTYPE html>
<html>
<head>
<title>Word: tantalize | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abhor something, you dislike it very much, usually because you think it&#8217;s immoral.</p>
<p class='rw-defn idx1' style='display:none'>Aliment is something, usually food, that feeds, nourishes, or supports someone or something else.</p>
<p class='rw-defn idx2' style='display:none'>Antipathy is a strong feeling of dislike or hostility towards someone or something.</p>
<p class='rw-defn idx3' style='display:none'>When something bedevils you, it causes you a lot of problems and difficulties over a period of time.</p>
<p class='rw-defn idx4' style='display:none'>When you beleaguer someone, you act with the intent to annoy or harass that person repeatedly until they finally give you what you want.</p>
<p class='rw-defn idx5' style='display:none'>Something brackish is distasteful and unpleasant usually because something else is mixed in with it; brackish water, for instance, is a mixture of fresh and salt water and cannot be drunk.</p>
<p class='rw-defn idx6' style='display:none'>A person who is comely is attractive; this adjective is usually used with females, but not always.</p>
<p class='rw-defn idx7' style='display:none'>A comestible is something that can be eaten.</p>
<p class='rw-defn idx8' style='display:none'>If you describe something, especially food and drink, as delectable, you mean that it is very pleasant, tasty, or attractive.</p>
<p class='rw-defn idx9' style='display:none'>If you say a person&#8217;s actions are despicable, you think they are extremely unpleasant or nasty.</p>
<p class='rw-defn idx10' style='display:none'>If something enthralls you, it makes you so interested and excited that you give it all your attention.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is epicurean derives great pleasure in material and sensual things, especially good food and drink.</p>
<p class='rw-defn idx12' style='display:none'>Hedonism is the belief that pleasure is important, so much so that life should be spent doing only things that one enjoys.</p>
<p class='rw-defn idx13' style='display:none'>If you describe something as heinous, you mean that is extremely evil, shocking, and bad.</p>
<p class='rw-defn idx14' style='display:none'>Something humdrum is dull, boring, or tiresome.</p>
<p class='rw-defn idx15' style='display:none'>If you imbibe ideas, values, or qualities, you absorb them into your mind.</p>
<p class='rw-defn idx16' style='display:none'>A miasma is a harmful influence or evil feeling that seems to surround a person or place.</p>
<p class='rw-defn idx17' style='display:none'>If something is noisome, it is extremely unpleasant, especially because it is very dirty or has an awful smell.</p>
<p class='rw-defn idx18' style='display:none'>Something that is noxious is harmful, extremely unpleasant, and often poisonous.</p>
<p class='rw-defn idx19' style='display:none'>If you describe people or things as odious, you think that they are extremely unpleasant.</p>
<p class='rw-defn idx20' style='display:none'>If you describe something as palatable, such as an idea or suggestion, you believe that people will accept it; palatable food or drink is agreeable to the taste.</p>
<p class='rw-defn idx21' style='display:none'>Something that is piquant is interesting and exciting; food that is piquant is pleasantly spicy.</p>
<p class='rw-defn idx22' style='display:none'>Potable water is clean and safe to drink.</p>
<p class='rw-defn idx23' style='display:none'>If something is redolent of something else, it has features that make you think of it; this word also refers to a particular odor or scent that can be pleasantly strong.</p>
<p class='rw-defn idx24' style='display:none'>If you repudiate something, you state that you do not accept or agree with it and no longer want to be connected with it in any way.</p>
<p class='rw-defn idx25' style='display:none'>Repugnance is a strong feeling of dislike for something or someone you find horrible and offensive.</p>
<p class='rw-defn idx26' style='display:none'>A sybarite is very fond of expensive and luxurious things; therefore, they are devoted to a life of pleasure.</p>
<p class='rw-defn idx27' style='display:none'>Something is toothsome when it is tasty, attractive, or pleasing in some way; this adjective can apply to food, people, or objects.</p>
<p class='rw-defn idx28' style='display:none'>If you describe something as unsavory, you mean that it is unpleasant or morally unacceptable.</p>
<p class='rw-defn idx29' style='display:none'>An unsightly person presents an ugly, unattractive, or disagreeable face to the world.</p>
<p class='rw-defn idx30' style='display:none'>A virulent disease is very dangerous and spreads very quickly.</p>
<p class='rw-defn idx31' style='display:none'>Vitriolic words are bitter, unkind, and mean-spirited.</p>
<p class='rw-defn idx32' style='display:none'>Someone who is winsome is attractive and charming.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>tantalize</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='tantalize#' id='pronounce-sound' path='audio/words/amy-tantalize'></a>
TAN-tl-ahyz
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='tantalize#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='tantalize#' id='context-sound' path='audio/wordcontexts/brian-tantalize'></a>
Thomas found the travel brochure <em><em>tantalizing</em></em>; he longed for a trip to England, but realistically knew he couldn&#8217;t afford such a journey.  In talking with the travel agent, he found the idea of traveling even more <em><em>tantalizing</em></em> or exciting.  The agent told him that the airfares were at a special and <em><em>tantalizing</em></em> low price, but Thomas realized, with frustration, that the trip was still beyond his reach.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If you are <em>tantalized</em> by something, how might you feel?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Frustrated, since you want it but probably can&#8217;t have it.
</li>
<li class='choice '>
<span class='result'></span>
Excited, since you are experiencing something new and different.
</li>
<li class='choice '>
<span class='result'></span>
Nervous, since you are being chased by something that could hurt you. 
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='tantalize#' id='definition-sound' path='audio/wordmeanings/amy-tantalize'></a>
Something that is <em>tantalizing</em> is desirable and exciting; nevertheless, it may be out of reach, perhaps due to being too expensive.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>tease</em>
</span>
</span>
</div>
<a class='quick-help' href='tantalize#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='tantalize#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/tantalize/memory_hooks/4597.json'></span>
<p>
<img alt="Tantalize" src="https://cdn0.membean.com/public/images/wordimages/hook/tantalize.jpg?qdep8" />
<span class="emp0"><span><span class="emp1"><span>Tan</span></span>, <span class="emp2"><span>Tall</span></span>, and Gorgeous <span class="emp3"><span>Eyes</span></span></span></span>  She's <span class="emp1"><span>tan</span></span>, she's <span class="emp2"><span>tall</span></span>, with gorgeous blue <span class="emp3"><span>eyes</span></span>, and does nothing but <span class="emp1"><span>tan</span></span><span class="emp2"><span>tal</span></span><span class="emp3"><span>ize</span></span> me--she's way out of my league!  How I wish I were Tom Cruise!
</p>
</div>

<div id='memhook-button-bar'>
<a href="tantalize#" id="add-public-hook" style="" url="https://membean.com/mywords/tantalize/memory_hooks">Use other public hook</a>
<a href="tantalize#" id="memhook-use-own" url="https://membean.com/mywords/tantalize/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='tantalize#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
To torment and <b>tantalize</b> oneself with hopes of possible fortune is so sweet, so thrilling!
<span class='attribution'>&mdash; Anton Chekhov, Russian playwright and short-story writer, from “The Lottery Ticket”</span>
<img alt="Anton chekhov, russian playwright and short-story writer, from “the lottery ticket”" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Anton Chekhov, Russian playwright and short-story writer, from “The Lottery Ticket”.jpg?qdep8" width="80" />
</li>
<li>
Financial advisers like to <b>tantalize</b> us by explaining how a tiny investment can grow into a startling sum through the exponential magic of compound interest.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
After a nine-year, three-billion-mile journey—and a brief technical hiccup on July 4—the New Horizons probe will get within 7,750 miles of the mysterious world during a flyby on July 14. The spacecraft has been making observations as it closes in on its target, and already it's found things that <b>tantalize</b> the science teams at NASA.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
ABCNews.com looked at the background and restaurants of the five young chefs nominated for the Rising Star award and located some fun things to do and see around their restaurant. And we also found out their signature dishes to further <b>tantalize</b> your taste buds.
<cite class='attribution'>
&mdash;
ABC News
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/tantalize/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='tantalize#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ize_make' data-tree-url='//cdn1.membean.com/public/data/treexml' href='tantalize#'>
<span class=''></span>
-ize
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>treat in a certain way</td>
</tr>
</table>
<p><em>Tantalize</em> comes from the Greek mythological character &#8220;Tantalus,&#8221; whose punishment in the Underworld did not allow him to eat or drink, despite the fact that he was standing in water and grapes hung over his head; hence, to <em>tantalize</em> is to &#8220;treat like Tantalus.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='tantalize#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Shabam School: The Fox and the Grapes</strong><span> The grapes are tantalizing the fox.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/tantalize.jpg' video_url='examplevids/tantalize' video_width='350'></span>
<div id='wt-container'>
<img alt="Tantalize" height="288" src="https://cdn1.membean.com/video/examplevids/tantalize.jpg" width="350" />
<div class='center'>
<a href="tantalize#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='tantalize#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Tantalize" src="https://cdn1.membean.com/public/images/wordimages/cons2/tantalize.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='tantalize#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>aliment</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bedevil</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>beleaguer</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>comely</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>comestible</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>delectable</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>enthrall</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>epicurean</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>hedonism</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>imbibe</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>palatable</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>piquant</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>potable</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>redolent</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>sybarite</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>toothsome</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>winsome</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abhor</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>antipathy</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>brackish</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>despicable</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>heinous</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>humdrum</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>miasma</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>noisome</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>noxious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>odious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>repudiate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>repugnance</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>unsavory</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>unsightly</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>virulent</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>vitriolic</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="tantalize" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>tantalize</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="tantalize#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

