
<!DOCTYPE html>
<html>
<head>
<title>Word: plaintive | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx1' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx2' style='display:none'>If you are despondent, you are extremely unhappy because you are in an unpleasant situation that you do not think will improve.</p>
<p class='rw-defn idx3' style='display:none'>A dirge is a slow and sad piece of music often performed at funerals.</p>
<p class='rw-defn idx4' style='display:none'>Something that is dolorous, such as music or news, causes mental pain and sorrow because it itself is full of grief and sorrow.</p>
<p class='rw-defn idx5' style='display:none'>Someone who is dour is serious, stubborn, and unfriendly; they can also be gloomy.</p>
<p class='rw-defn idx6' style='display:none'>When something is droll, it is humorous in an odd way.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx8' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx9' style='display:none'>An elegy is a poem or other piece of writing expressing sadness; it is often about someone who has died.</p>
<p class='rw-defn idx10' style='display:none'>If you show exuberance, you display great excitement, energy, and enthusiasm.</p>
<p class='rw-defn idx11' style='display:none'>If you exult, you show great pleasure and excitement, especially about something you have achieved.</p>
<p class='rw-defn idx12' style='display:none'>The gravity of a situation or event is its seriousness or importance.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is jovial is in a good humor, lighthearted, and jolly.</p>
<p class='rw-defn idx14' style='display:none'>Levity is an amusing way of speaking or behaving during a serious situation; it can lighten the moment but can also be considered inappropriate.</p>
<p class='rw-defn idx15' style='display:none'>If someone is lugubrious, they are looking very sad or gloomy.</p>
<p class='rw-defn idx16' style='display:none'>If you are melancholy, you look and feel sad.</p>
<p class='rw-defn idx17' style='display:none'>A petulant person behaves in an unreasonable and childish way, especially because they cannot get their own way or what they want.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is querulous often complains about things in an annoying way.</p>
<p class='rw-defn idx19' style='display:none'>Someone who is saturnine is looking miserable and sad, sometimes in a threatening or unfriendly way.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>plaintive</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='plaintive#' id='pronounce-sound' path='audio/words/amy-plaintive'></a>
PLAYN-tiv
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='plaintive#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='plaintive#' id='context-sound' path='audio/wordcontexts/brian-plaintive'></a>
The <em>plaintive</em>, pitiful cries of the puppy were difficult to endure, so since the puppy continued to whine and moan where we kept him outside, we brought him in.  Then his <em>plaintive</em> and sorrowful wails filled the walls of the house, so we brought him up from the basement.  The <em>plaintive</em> children unhappily sobbed for their parents to let the puppy sleep in their room.  All was well, until the puppy woke everyone very early with his <em>plaintive</em>, grief-stricken plea to be let back outside.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When is someone being <em>plaintive</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When they show compassion for others.
</li>
<li class='choice answer '>
<span class='result'></span>
When they are mournful or sorrowful.
</li>
<li class='choice '>
<span class='result'></span>
When they desire a simple life.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='plaintive#' id='definition-sound' path='audio/wordmeanings/amy-plaintive'></a>
A <em>plaintive</em> sound or voice expresses sadness.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>sad</em>
</span>
</span>
</div>
<a class='quick-help' href='plaintive#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='plaintive#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/plaintive/memory_hooks/3457.json'></span>
<p>
<span class="emp0"><span>Com<span class="emp1"><span>plaint</span></span>s L<span class="emp2"><span>ive</span></span>!</span></span> My friend Liza com<span class="emp1"><span>plain</span></span>s so much in such a <span class="emp1"><span>plaint</span></span><span class="emp2"><span>ive</span></span>, whining voice that whenever I hear her coming in the distance, I think, "Straight from New York City, it's Com<span class="emp1"><span>plain</span></span>ing L<span class="emp2"><span>ive</span></span>, with your <span class="emp1"><span>plaint</span></span><span class="emp2"><span>ive</span></span> host, Liza Simmons!"
</p>
</div>

<div id='memhook-button-bar'>
<a href="plaintive#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/plaintive/memory_hooks">Use other hook</a>
<a href="plaintive#" id="memhook-use-own" url="https://membean.com/mywords/plaintive/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='plaintive#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
“For the rest of the country, Exxon happened a long time ago,” Maxwell says, a <b>plaintive</b> crack in his voice. “For me, for the people I grew up with, the oil is still spilling. We’re still waiting for the end.”
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
A <b>plaintive</b> lament about Mexicans in search of an American paradise, the song is one of Bruce’s current passions . . . .
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
<li>
A reporter once wrote a euphoric story about Nepal, ending with the <b>plaintive</b> line: "I don’t know why they would ever want to leave such a beautiful spot."
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/plaintive/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='plaintive#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='plaint_mourn' data-tree-url='//cdn1.membean.com/public/data/treexml' href='plaintive#'>
<span class=''></span>
plaint
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>mourn for</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ive_does' data-tree-url='//cdn1.membean.com/public/data/treexml' href='plaintive#'>
<span class=''></span>
-ive
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or that which does something</td>
</tr>
</table>
<p>Being <em>plaintive</em> is &#8220;mourning&#8221; for something.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='plaintive#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Plaintive" src="https://cdn0.membean.com/public/images/wordimages/cons2/plaintive.png?qdep6" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='plaintive#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>despondent</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>dirge</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>dolorous</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>dour</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>elegy</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>gravity</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>lugubrious</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>melancholy</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>petulant</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>querulous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>saturnine</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>droll</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>exuberance</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>exult</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>jovial</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>levity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="plaintive" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>plaintive</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="plaintive#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

