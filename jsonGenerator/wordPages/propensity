
<!DOCTYPE html>
<html>
<head>
<title>Word: propensity | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abhor something, you dislike it very much, usually because you think it&#8217;s immoral.</p>
<p class='rw-defn idx1' style='display:none'>If you have an affiliation with a group or another person, you are officially involved or connected with them.</p>
<p class='rw-defn idx2' style='display:none'>If something is anathema to you, such as a cursed object or idea, you very strongly dislike it or even hate it.</p>
<p class='rw-defn idx3' style='display:none'>If you have animus against someone, you have a strong feeling of dislike or hatred towards them, often without a good reason or based upon your personal prejudices.</p>
<p class='rw-defn idx4' style='display:none'>Your antagonist is an opponent in a competition or battle.</p>
<p class='rw-defn idx5' style='display:none'>Antipathy is a strong feeling of dislike or hostility towards someone or something.</p>
<p class='rw-defn idx6' style='display:none'>Someone does something in a disinterested way when they have no personal involvement or attachment to the action.</p>
<p class='rw-defn idx7' style='display:none'>When you are disposed towards a particular action or thing, you are inclined or partial towards it.</p>
<p class='rw-defn idx8' style='display:none'>Idiosyncratic tendencies, behavior, or habits are unusual and strange.</p>
<p class='rw-defn idx9' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx10' style='display:none'>An intrinsic characteristic of something is the basic and essential feature that makes it what it is.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx12' style='display:none'>If you describe people or things as odious, you think that they are extremely unpleasant.</p>
<p class='rw-defn idx13' style='display:none'>If you have a penchant for some activity, you have a strong fondness for it and thus find it enjoyable.</p>
<p class='rw-defn idx14' style='display:none'>If you have a predilection for something, you have a preference for it.</p>
<p class='rw-defn idx15' style='display:none'>If someone is predisposed to something, they are made favorable or inclined to it in advance, or they are made susceptible to something, such as a disease.</p>
<p class='rw-defn idx16' style='display:none'>A proclivity is the tendency to behave in a particular way or to like a particular thing.</p>
<p class='rw-defn idx17' style='display:none'>If you are prone to doing something, you are likely or inclined to do it.</p>
<p class='rw-defn idx18' style='display:none'>The propinquity of a thing is its nearness in location, relationship, or similarity to another thing.</p>
<p class='rw-defn idx19' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx20' style='display:none'>Repugnance is a strong feeling of dislike for something or someone you find horrible and offensive.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>propensity</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='propensity#' id='pronounce-sound' path='audio/words/amy-propensity'></a>
pruh-PEN-si-tee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='propensity#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='propensity#' id='context-sound' path='audio/wordcontexts/brian-propensity'></a>
Keegan has a <em>propensity</em> for running, so it seems like he does it any chance he gets.  In school, his teachers ask him to stop, but his <em>propensity</em> or tendency to run kicks in once again as soon as he leaves their sight.  Running just feels more natural to Keegan, so his <em>propensity</em> or leaning to do it encourages him to run whenever he gets the chance.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>propensity</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A boy who naturally tends to be shy in large groups.
</li>
<li class='choice '>
<span class='result'></span>
A boy who gets cut from the football team. 
</li>
<li class='choice '>
<span class='result'></span>
A boy who finds a twenty-dollar bill on the ground.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='propensity#' id='definition-sound' path='audio/wordmeanings/amy-propensity'></a>
A <em>propensity</em> is a natural tendency towards a particular behavior.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>liking</em>
</span>
</span>
</div>
<a class='quick-help' href='propensity#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='propensity#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/propensity/memory_hooks/4855.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Prop</span></span> D<span class="emp2"><span>ensity</span></span></span></span>  Our drama teacher is pretty crazy; she doesn't have a <span class="emp1"><span>prop</span></span><span class="emp2"><span>ensity</span></span> for acting, but she does have a <span class="emp1"><span>prop</span></span><span class="emp2"><span>ensity</span></span> for <span class="emp1"><span>prop</span></span>s, so her <span class="emp1"><span>prop</span></span><span class="emp2"><span>ensity</span></span> for <span class="emp1"><span>prop</span></span> d<span class="emp2"><span>ensity</span></span> makes it very hard to move around the stage.
</p>
</div>

<div id='memhook-button-bar'>
<a href="propensity#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/propensity/memory_hooks">Use other hook</a>
<a href="propensity#" id="memhook-use-own" url="https://membean.com/mywords/propensity/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='propensity#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
According to the Brookings Institution, millennial and some Generation Z voters make up about 37% of eligible voters but have traditionally had a lower <b>propensity</b> to turn out than other groups.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Other experts speculate that the whole American diet may be calibrated to an artificially high level of sweetness, and that we may be in danger of generalizing our <b>propensity</b> for sweets—artificial or otherwise—to everything we eat.
<cite class='attribution'>
&mdash;
Time
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/propensity/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='propensity#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pro_forward' data-tree-url='//cdn1.membean.com/public/data/treexml' href='propensity#'>
<span class='common'></span>
pro-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>forward, forth</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pens_hang' data-tree-url='//cdn1.membean.com/public/data/treexml' href='propensity#'>
<span class='common'></span>
pens
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>hang, weigh</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ity_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='propensity#'>
<span class=''></span>
-ity
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state or quality</td>
</tr>
</table>
<p>A <em>propensity</em> for something, like eating dark chocolate, is the &#8220;state of hanging forward or weighing forth&#8221; towards the doing of it.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='propensity#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Psych</strong><span> These two not only have a propensity for being in the wrong place at the wrong time, but they also have a propensity to overreact.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/propensity.jpg' video_url='examplevids/propensity' video_width='350'></span>
<div id='wt-container'>
<img alt="Propensity" height="288" src="https://cdn1.membean.com/video/examplevids/propensity.jpg" width="350" />
<div class='center'>
<a href="propensity#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='propensity#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Propensity" src="https://cdn3.membean.com/public/images/wordimages/cons2/propensity.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='propensity#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>affiliation</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>disposed</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>idiosyncratic</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>intrinsic</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>penchant</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>predilection</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>predispose</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>proclivity</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>prone</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>propinquity</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abhor</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>anathema</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>animus</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>antagonist</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>antipathy</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>disinterested</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>odious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>repugnance</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="propensity" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>propensity</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="propensity#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

