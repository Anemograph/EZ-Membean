
<!DOCTYPE html>
<html>
<head>
<title>Word: statutory | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Statutory-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/statutory-large.jpg?qdep8" />
</div>
<a href="statutory#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When you act with abandon, you give in to the impulse of the moment and behave in a wild, uncontrolled way.</p>
<p class='rw-defn idx1' style='display:none'>If you describe a decision, rule, or plan as arbitrary, you think that it was decided without any thought, standard, or system to guide it; therefore, it can seem unfair.</p>
<p class='rw-defn idx2' style='display:none'>An axiom is a wise saying or widely recognized general truth that is often self-evident; it can also be an established rule or law.</p>
<p class='rw-defn idx3' style='display:none'>A cardinal rule or quality is considered to be the most important or basic in a set of rules or qualities.</p>
<p class='rw-defn idx4' style='display:none'>A codicil is a supplement, usually to a will, that is added after the main part has been written.</p>
<p class='rw-defn idx5' style='display:none'>If you comport yourself in a particular way, you behave in that way.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is contumacious is purposely stubborn, contrary, or disobedient.</p>
<p class='rw-defn idx7' style='display:none'>A decadent person has low moral standards and is more interested in pleasure than serious matters.</p>
<p class='rw-defn idx8' style='display:none'>If you delineate something, such as an idea or situation or border, you describe it in great detail.</p>
<p class='rw-defn idx9' style='display:none'>A dictum is a saying that people often repeat because it says something interesting or wise about a subject.</p>
<p class='rw-defn idx10' style='display:none'>A situation or thing that is discordant does not fit in with other things; therefore, it is disagreeable, strange, or unpleasant.</p>
<p class='rw-defn idx11' style='display:none'>An edict is an official order or command given by a government or someone in authority.</p>
<p class='rw-defn idx12' style='display:none'>If you eschew something, you deliberately avoid doing it, especially for moral reasons.</p>
<p class='rw-defn idx13' style='display:none'>A fiat is an official order from a person or group in authority.</p>
<p class='rw-defn idx14' style='display:none'>If someone is fractious, they are easily upset or annoyed over unimportant things.</p>
<p class='rw-defn idx15' style='display:none'>To gerrymander a voting district is to change its physical boundaries in order to include more people who vote in a particular way.</p>
<p class='rw-defn idx16' style='display:none'>An injunction is a court order that prevents someone from doing something.</p>
<p class='rw-defn idx17' style='display:none'>An interdict is an official order that prevents someone from doing something.</p>
<p class='rw-defn idx18' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx19' style='display:none'>Malfeasance is an unlawful act, especially one committed by a trusted public official.</p>
<p class='rw-defn idx20' style='display:none'>A mandate is an official order or command issued by a government or other authorized body.</p>
<p class='rw-defn idx21' style='display:none'>A social norm is the standard, model, or rule by which people conduct themselves.</p>
<p class='rw-defn idx22' style='display:none'>Someone who is obstreperous is noisy, unruly, and difficult to control.</p>
<p class='rw-defn idx23' style='display:none'>A precept is a rule or principle that teaches correct behavior.</p>
<p class='rw-defn idx24' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is profligate is lacking in restraint, which can manifest in carelessly and wastefully using resources, such as energy or money; this kind of person can also act in an immoral way.</p>
<p class='rw-defn idx26' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx27' style='display:none'>When someone proscribes an activity, they prohibit it by establishing a rule against it.</p>
<p class='rw-defn idx28' style='display:none'>To be punctilious is to pay precise attention to detail.</p>
<p class='rw-defn idx29' style='display:none'>A punitive action is intended to punish someone.</p>
<p class='rw-defn idx30' style='display:none'>A recalcitrant animal or person is difficult to control and refuses to obey orders—even after stiff punishment.</p>
<p class='rw-defn idx31' style='display:none'>If you refrain from doing something, you do not do it—even though you want to.</p>
<p class='rw-defn idx32' style='display:none'>A rubric is a set of instructions at the beginning of a document, such as an examination or term paper, that is usually printed in a different style so as to highlight its importance.</p>
<p class='rw-defn idx33' style='display:none'>A scrupulous person takes great care to do everything in an extremely honest, conscientious, and fair manner.</p>
<p class='rw-defn idx34' style='display:none'>A stricture is a rule or condition that imposes restrictions or limits on what you can do.</p>
<p class='rw-defn idx35' style='display:none'>A tenet is a belief held by a group, organization, or person.</p>
<p class='rw-defn idx36' style='display:none'>A touchstone is the standard or best example by which the value or quality of something else is judged.</p>
<p class='rw-defn idx37' style='display:none'>An action or deed is unconscionable if it is excessively shameful, unfair, or unjust and its effects are more severe than is reasonable or acceptable.</p>
<p class='rw-defn idx38' style='display:none'>Something that is volatile can change easily and vary widely.</p>
<p class='rw-defn idx39' style='display:none'>A wanton action deliberately harms someone or damages something for no apparent or good reason.</p>
<p class='rw-defn idx40' style='display:none'>A writ is an official document that orders a person to do something.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>statutory</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='statutory#' id='pronounce-sound' path='audio/words/amy-statutory'></a>
STACH-oo-tohr-ee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='statutory#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='statutory#' id='context-sound' path='audio/wordcontexts/brian-statutory'></a>
Stella knew that the <em>statutory</em>, legal age for a driver&#8217;s license was sixteen, but she wanted her son Syd to wait a year nevertheless.  Stella thought that if lawmakers knew firsthand about her teenage son&#8217;s immaturity, there would be no way that the <em>statutory</em> or lawful regulations would permit Syd to drive.  The official, judicial, and <em>statutory</em> laws might allow Syd his wheels, but the domestic law of his mother had other, more cautious plans in place.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If something is <em>statutory</em>, what is it?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is helpful to a company.
</li>
<li class='choice answer '>
<span class='result'></span>
It is related to and set by laws.
</li>
<li class='choice '>
<span class='result'></span>
It is considered bad by society.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='statutory#' id='definition-sound' path='audio/wordmeanings/amy-statutory'></a>
Something <em>statutory</em>, such as the power given to a governor or president, is created, established, and controlled by rules and laws; hence, it has the full force of the law behind it and must be followed.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>legal</em>
</span>
</span>
</div>
<a class='quick-help' href='statutory#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='statutory#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/statutory/memory_hooks/3444.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Statut</span></span>e St<span class="emp2"><span>ory</span></span></span></span> When one goes through law school, one learns the <span class="emp3"><span>statut</span></span>e st<span class="emp2"><span>ory</span></span>, or the st<span class="emp2"><span>ory</span></span> of legal <span class="emp3"><span>statut</span></span><span class="emp2"><span>ory</span></span> creation by legislatures throughout history.
</p>
</div>

<div id='memhook-button-bar'>
<a href="statutory#" id="add-public-hook" style="" url="https://membean.com/mywords/statutory/memory_hooks">Use other public hook</a>
<a href="statutory#" id="memhook-use-own" url="https://membean.com/mywords/statutory/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='statutory#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
<b>Statutory</b> authority to improve fuel economy has existed for 35 years at the Transportation Department, and it still exists today.
<cite class='attribution'>
&mdash;
Lisa Murkowski, American politician, Senator from Alaska
</cite>
</li>
<li>
In sales cases something close to a scientific appraisal of the facts is possible; there are strong mercantile interests favoring certainty, and future litigation can be reduced by strict adherence to carefully prescribed <b>statutory</b> standards.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
[H]e advocates something closer to the definition in the Wilderness Act, in which wilderness areas are given <b>statutory</b> protection and are managed to preserve their wild characteristics and other goals decided by society at large.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
The effort picked up steam the next year after the House of Representatives passed what is known as cap-and-trade legislation . . . . The idea was to create a <b>statutory</b> limit, or cap, on the overall amount of a certain type of pollution that could be emitted.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/statutory/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='statutory#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='stat_stands' data-tree-url='//cdn1.membean.com/public/data/treexml' href='statutory#'>
<span class='common'></span>
stat
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>stands</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ory_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='statutory#'>
<span class=''></span>
-ory
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>A <em>statute</em> or <em>statutory</em> law &#8220;stands&#8221; by virtue of being legally authorized by a legislature.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='statutory#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Bones</strong><span> A little dinner conversation about statutory laws.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/statutory.jpg' video_url='examplevids/statutory' video_width='350'></span>
<div id='wt-container'>
<img alt="Statutory" height="288" src="https://cdn1.membean.com/video/examplevids/statutory.jpg" width="350" />
<div class='center'>
<a href="statutory#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='statutory#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Statutory" src="https://cdn0.membean.com/public/images/wordimages/cons2/statutory.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='statutory#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>axiom</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>cardinal</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>codicil</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>comport</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>delineate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>dictum</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>edict</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>fiat</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>injunction</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>interdict</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>mandate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>norm</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>precept</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>proscribe</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>punctilious</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>punitive</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>rubric</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>scrupulous</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>stricture</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>tenet</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>touchstone</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>writ</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abandon</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>arbitrary</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>contumacious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>decadent</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>discordant</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>eschew</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>fractious</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>gerrymander</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>malfeasance</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>obstreperous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>profligate</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>recalcitrant</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>refrain</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>unconscionable</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>volatile</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>wanton</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='statutory#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
statute
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>a law or decree</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="statutory" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>statutory</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="statutory#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

