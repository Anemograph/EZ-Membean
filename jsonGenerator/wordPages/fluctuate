
<!DOCTYPE html>
<html>
<head>
<title>Word: fluctuate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you describe something as ambiguous, you mean that it is unclear or confusing because it can be understood in multiple ways.</p>
<p class='rw-defn idx1' style='display:none'>An assiduous person works hard to ensure that something is done properly and completely.</p>
<p class='rw-defn idx2' style='display:none'>Something ephemeral, such as some insects or a sunset, lasts for only a short time or has a very short lifespan.</p>
<p class='rw-defn idx3' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx4' style='display:none'>Erratic behavior is irregular, unpredictable, and unusual.</p>
<p class='rw-defn idx5' style='display:none'>The word homogeneous is used to describe a group that has members or parts which are similar or are all of the same type.</p>
<p class='rw-defn idx6' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx7' style='display:none'>If someone is imperturbable, they are always calm and not easily upset or disturbed by any situation, even dangerous ones.</p>
<p class='rw-defn idx8' style='display:none'>Someone who is impulsive tends to do things without thinking about them ahead of time; therefore, their actions can be unpredictable.</p>
<p class='rw-defn idx9' style='display:none'>An inalienable right is a privilege that cannot be taken away.</p>
<p class='rw-defn idx10' style='display:none'>If someone is indefatigable, they never shows signs of getting tired.</p>
<p class='rw-defn idx11' style='display:none'>If someone leaves an indelible impression on you, it will not be forgotten; an indelible mark is permanent or impossible to erase or remove.</p>
<p class='rw-defn idx12' style='display:none'>The adjective inexorable describes a process that is impossible to stop once it has begun.</p>
<p class='rw-defn idx13' style='display:none'>An inflection is a change or variation, such as in a person&#8217;s voice or in the form a noun or verb takes.</p>
<p class='rw-defn idx14' style='display:none'>Something that happens on an intermittent basis happens in irregular intervals, stopping and starting at unpredictable times.</p>
<p class='rw-defn idx15' style='display:none'>An inveterate person is always doing a particular thing, especially something questionable—and they are not likely to stop doing it.</p>
<p class='rw-defn idx16' style='display:none'>Someone who is irresolute is unable to decide what to do.</p>
<p class='rw-defn idx17' style='display:none'>A mercurial person&#8217;s mind or mood changes often, quickly, and unpredictably.</p>
<p class='rw-defn idx18' style='display:none'>When someone or something undergoes the process of metamorphosis, there is a change in appearance, character, or even physical shape.</p>
<p class='rw-defn idx19' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx20' style='display:none'>Someone who is obdurate is stubborn, unreasonable, or uncontrollable; hence, they simply refuse to change their behavior, actions, or beliefs to fit any situation whatsoever.</p>
<p class='rw-defn idx21' style='display:none'>An obstinate person refuses to change their mind, even when other people think they are being highly unreasonable.</p>
<p class='rw-defn idx22' style='display:none'>If an object oscillates, it moves repeatedly from one point to another and then back again; if you oscillate between two moods or attitudes, you keep changing from one to the other.</p>
<p class='rw-defn idx23' style='display:none'>Something that is perennial lasts a very long time and is enduring.</p>
<p class='rw-defn idx24' style='display:none'>A permutation is a complete change or total transformation.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is pertinacious is determined to continue doing something rather than giving up—even when it gets very difficult.</p>
<p class='rw-defn idx26' style='display:none'>Something or someone that is protean is adaptable, variable, or changeable in nature.</p>
<p class='rw-defn idx27' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx28' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx29' style='display:none'>Something that is temporal deals with the present and somewhat brief time of this world.</p>
<p class='rw-defn idx30' style='display:none'>To be tentative is to be hesitant or uncertain about something; an agreement or decision of this kind is likely to have changes before it reaches its final form.</p>
<p class='rw-defn idx31' style='display:none'>Something that has the quality of transience lasts for only a short time or is constantly changing.</p>
<p class='rw-defn idx32' style='display:none'>Something that undulates moves or is shaped like waves with gentle curves that smoothly rise and fall.</p>
<p class='rw-defn idx33' style='display:none'>If you are unflagging while doing a task, you are untiring when working upon it and do not stop until it is finished.</p>
<p class='rw-defn idx34' style='display:none'>When someone vacillates, they go back and forth about a choice or opinion, unable to make a firm decision.</p>
<p class='rw-defn idx35' style='display:none'>A vagary is an unpredictable or unexpected change in action or behavior.</p>
<p class='rw-defn idx36' style='display:none'>Something that is variegated has various tones or colors; it can also mean filled with variety.</p>
<p class='rw-defn idx37' style='display:none'>Vicissitudes can be unexpected or simply normal changes and variations that happen during the course of people&#8217;s lives.</p>
<p class='rw-defn idx38' style='display:none'>When you are vigilant, you are keenly watchful, careful, or attentive to a task or situation.</p>
<p class='rw-defn idx39' style='display:none'>Something that is volatile can change easily and vary widely.</p>
<p class='rw-defn idx40' style='display:none'>If you waver, you cannot decide between two things because you have serious doubts about which choice is better.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>fluctuate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='fluctuate#' id='pronounce-sound' path='audio/words/amy-fluctuate'></a>
FLUHK-choo-ayt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='fluctuate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='fluctuate#' id='context-sound' path='audio/wordcontexts/brian-fluctuate'></a>
Many things in life <em>fluctuate</em> or change widely.  For instance, one&#8217;s moods can <em>fluctuate</em> or rise and fall between happiness and depression.  The stock market can widely <em>fluctuate</em> as well, shooting up one day and decreasing in value the next.  Perhaps the only thing that does not <em>fluctuate</em> or shift constantly in life is the rising and falling itself.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What happens when something <em>fluctuates</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It slowly gets worse despite attempts to make it better.
</li>
<li class='choice '>
<span class='result'></span>
It remains the same for long periods of time.
</li>
<li class='choice answer '>
<span class='result'></span>
It changes a lot while not following any pattern.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='fluctuate#' id='definition-sound' path='audio/wordmeanings/amy-fluctuate'></a>
Things that <em>fluctuate</em> vary or change often, rising or falling seemingly at random.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>vary</em>
</span>
</span>
</div>
<a class='quick-help' href='fluctuate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='fluctuate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/fluctuate/memory_hooks/5450.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Flu</span></span> and F<span class="emp3"><span>ate</span></span></span></span> Both <span class="emp1"><span>flu</span></span> and F<span class="emp3"><span>ate</span></span> fluctuate; the <span class="emp1"><span>flu</span></span> comes and goes, as does each and every human's f<span class="emp3"><span>ate</span></span> change as life goes on.
</p>
</div>

<div id='memhook-button-bar'>
<a href="fluctuate#" id="add-public-hook" style="" url="https://membean.com/mywords/fluctuate/memory_hooks">Use other public hook</a>
<a href="fluctuate#" id="memhook-use-own" url="https://membean.com/mywords/fluctuate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='fluctuate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The cost for Airtime minutes can <b>fluctuate</b>, presumably according to promotions and market factors, so topping off becomes an exercise comparable to fuel hedging. Buy a big block of minutes when you think they're at their cheapest and you look smart, unless the price drops again the next day. Then again, it might go up.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
Most data measurements—heart rate, blood pressure, glucose levels—reveal only a momentary snapshot of a person and don’t capture how those variables <b>fluctuate</b>. Devices that remain attached to the body, outfitted with an unending power source, can observe a stream of data, which Dagdeviren says could make medicine truly personalized.
<cite class='attribution'>
&mdash;
Discover Magazine
</cite>
</li>
<li>
The value of a bond fund—and the income it generates—will always <b>fluctuate</b>, since it’s constantly buying and selling hundreds of securities of varying maturities.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
By the College Board’s own numbers, a student’s section score will <b>fluctuate</b> an average 20 to 30 points between testing sessions. In fact, it warns specifically against reading too much into small differences: “There must be a 60-point difference between reading, mathematics, or writing scores before more skill can be assumed in one area than another.”
<cite class='attribution'>
&mdash;
Slate
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/fluctuate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='fluctuate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='flu_flow' data-tree-url='//cdn1.membean.com/public/data/treexml' href='fluctuate#'>
<span class='common'></span>
flu
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>flow</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ate_quality' data-tree-url='//cdn1.membean.com/public/data/treexml' href='fluctuate#'>
<span class=''></span>
-ate
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make something have a certain quality</td>
</tr>
</table>
<p>Anything that <em>fluctuates</em> has the unstable &#8220;quality of flowing&#8221; quickly from one thing to the next.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='fluctuate#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Bloomberg</strong><span> Oil fluctuates between gains and losses.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/fluctuate.jpg' video_url='examplevids/fluctuate' video_width='350'></span>
<div id='wt-container'>
<img alt="Fluctuate" height="288" src="https://cdn1.membean.com/video/examplevids/fluctuate.jpg" width="350" />
<div class='center'>
<a href="fluctuate#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='fluctuate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Fluctuate" src="https://cdn1.membean.com/public/images/wordimages/cons2/fluctuate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='fluctuate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>ambiguous</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>ephemeral</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>erratic</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>impulsive</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>inflection</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>intermittent</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>irresolute</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>mercurial</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>metamorphosis</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>oscillate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>permutation</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>protean</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>temporal</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>tentative</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>transience</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>undulate</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>vacillate</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>vagary</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>variegated</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>vicissitude</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>volatile</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>waver</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>assiduous</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>homogeneous</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>imperturbable</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>inalienable</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>indefatigable</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>indelible</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>inexorable</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>inveterate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>obdurate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>obstinate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>perennial</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>pertinacious</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>unflagging</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>vigilant</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="fluctuate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>fluctuate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="fluctuate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

