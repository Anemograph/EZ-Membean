
<!DOCTYPE html>
<html>
<head>
<title>Word: effete | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Acumen is the ability to make quick decisions and keen, precise judgments.</p>
<p class='rw-defn idx1' style='display:none'>The caliber of a person is the level or quality of their ability, intelligence, and other talents.</p>
<p class='rw-defn idx2' style='display:none'>A decadent person has low moral standards and is more interested in pleasure than serious matters.</p>
<p class='rw-defn idx3' style='display:none'>Something that is defunct is no longer in existence or does not function.</p>
<p class='rw-defn idx4' style='display:none'>A degenerate person is immoral, wicked, or corrupt.</p>
<p class='rw-defn idx5' style='display:none'>Dissolution is the breaking up or official end of a group, such as a couple or institution; it can also be the act of separating something into smaller components.</p>
<p class='rw-defn idx6' style='display:none'>If something is done for someone&#8217;s edification, it is done to benefit that person by teaching them something that improves or enlightens their knowledge or character.</p>
<p class='rw-defn idx7' style='display:none'>Efficacy is the ability or power to produce an expected effect or result.</p>
<p class='rw-defn idx8' style='display:none'>If something enervates you, it makes you very tired and weak—almost to the point of collapse.</p>
<p class='rw-defn idx9' style='display:none'>If you think something, such as a statement or idea, is fatuous, you consider it stupid or extremely silly; a fatuous hope is unrealistic.</p>
<p class='rw-defn idx10' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx11' style='display:none'>If someone, such as a writer, is described as having fecundity, they have the ability to produce many original and different ideas.</p>
<p class='rw-defn idx12' style='display:none'>Something is flaccid when it is unpleasantly soft and weak, hangs limply, or lacks vigor and energy.</p>
<p class='rw-defn idx13' style='display:none'>If something is formidable, it is impressive in size, power, or skill; therefore, it can make you feel frightened, alarmed, or in awe because it is so powerful or difficult to deal with.</p>
<p class='rw-defn idx14' style='display:none'>If something is your forte, you are very good at it or know a lot about it.</p>
<p class='rw-defn idx15' style='display:none'>If you describe someone&#8217;s behavior as inane, you think it is completely stupid or without much meaning.</p>
<p class='rw-defn idx16' style='display:none'>If someone is indefatigable, they never shows signs of getting tired.</p>
<p class='rw-defn idx17' style='display:none'>Someone who is ineffectual at a task is useless or ineffective when attempting to do it.</p>
<p class='rw-defn idx18' style='display:none'>If something is infallible, it is never wrong and so is incapable of making mistakes.</p>
<p class='rw-defn idx19' style='display:none'>Something insipid is dull, boring, and has no interesting features; for example, insipid food has no taste or little flavor.</p>
<p class='rw-defn idx20' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx21' style='display:none'>If you describe ideas as jejune, you are saying they are childish and uninteresting; the adjective jejune also describes those having little experience, maturity, or knowledge.</p>
<p class='rw-defn idx22' style='display:none'>A languid person is slow, relaxed, and shows little energy or interest in doing anything.</p>
<p class='rw-defn idx23' style='display:none'>When you are listless, you lack energy and interest and are unwilling to exert any effort.</p>
<p class='rw-defn idx24' style='display:none'>Something luxuriant, such as plants or hair, is growing well and is very healthy.</p>
<p class='rw-defn idx25' style='display:none'>Something mediocre is average or ordinary in quality; it’s just OK.</p>
<p class='rw-defn idx26' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx27' style='display:none'>A person&#8217;s morale is their current state of self-confidence, how they feel emotionally, and how motivated they are to complete tasks.</p>
<p class='rw-defn idx28' style='display:none'>An omnipotent being or entity is unlimited in its power.</p>
<p class='rw-defn idx29' style='display:none'>If someone is pallid, they look very pale in an unattractive and unhealthy way.</p>
<p class='rw-defn idx30' style='display:none'>Something&#8217;s potency is how powerful or effective it is.</p>
<p class='rw-defn idx31' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx32' style='display:none'>When someone is rehabilitated, they are restored to a more normal way of life, such as returning to good health or a crime-free life.</p>
<p class='rw-defn idx33' style='display:none'>A resurgence is a rising again or comeback of something.</p>
<p class='rw-defn idx34' style='display:none'>A stentorian voice is extremely loud and strong.</p>
<p class='rw-defn idx35' style='display:none'>Something that is superannuated is so old and worn out that it is no longer working or useful.</p>
<p class='rw-defn idx36' style='display:none'>Something vapid is dull, boring, and/or tiresome.</p>
<p class='rw-defn idx37' style='display:none'>If someone vitiates something, they make it less effective, weaker, or reduced in quality.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>effete</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='effete#' id='pronounce-sound' path='audio/words/amy-effete'></a>
i-FEET
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='effete#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='effete#' id='context-sound' path='audio/wordcontexts/brian-effete'></a>
Josh is the head scholar of an <em>effete</em> and exhausted group of intellectuals who were once very enthusiastic about claiming that Shakespeare was actually Roger Bacon.  When overwhelming evidence arose that Shakespeare was actually Shakespeare, the now <em>effete</em> Josh, tired and worn out by his attempts to champion Roger Bacon, called for one last-ditch attempt to prove his weakening position.  The last evidence that Josh gave was simply laughed at by literary critics and historians alike, which effectively made Josh and his literary group <em>effete</em>, burnt out, and drained to the point of no return.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Who is an example of an <em>effete</em> ruler in history?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Queen Elizabeth I, who is considered one of the most successful monarchs of all time.
</li>
<li class='choice answer '>
<span class='result'></span>
King Edward II, who is known as one of the most incompetent kings ever to rule England.
</li>
<li class='choice '>
<span class='result'></span>
Richard the Lionheart of England, a great military king who was killed by a crossbow bolt.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='effete#' id='definition-sound' path='audio/wordmeanings/amy-effete'></a>
If you describe a person, group, or civilization as <em>effete</em>, you mean it is weak, exhausted,  powerless, unproductive, and/or corrupt.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>exhausted</em>
</span>
</span>
</div>
<a class='quick-help' href='effete#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='effete#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/effete/memory_hooks/4363.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Feet</span></span> De<span class="emp2"><span>feat</span></span></span></span> My association of golden soccer <span class="emp2"><span>feet</span></span> has been at long last de<span class="emp2"><span>feat</span></span>ed and tired out by the association of silver soccer <span class="emp2"><span>feet</span></span>; the ef<span class="emp2"><span>fete</span></span> status of our <span class="emp2"><span>feet</span></span> is now well known, and we no longer feel golden but simply more like tin.
</p>
</div>

<div id='memhook-button-bar'>
<a href="effete#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/effete/memory_hooks">Use other hook</a>
<a href="effete#" id="memhook-use-own" url="https://membean.com/mywords/effete/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='effete#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Minnesotans have long looked down their noses at eastern hockey, which is alleged to be an <b>effete</b>, watered-down version of the game. Eastern schedules are shorter; eastern players are smaller and, well, softer.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
In both cases, their disillusioned response to the hustle and bustle of daily life says something about their <b>effete</b> culture. But Moshfegh's narrator isn't lazy like Oblomov — in fact, she's single-mindedly goal-directed: She wants to sleep and sleep and sleep.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
But did “Happy to help you and Nathan connect” maybe sound slightly grudging? With that phrasing, it was easy to envision Mr. Blake as a twenty-four-year-old Bard graduate, cooler and smarter than me, returning to his three roommates each night with stories of his <b>effete</b> boss, Nathan, who was incapable of scheduling his own meetings and who _needed to have everything printed out_.
<cite class='attribution'>
&mdash;
The New Yorker
</cite>
</li>
<li>
Although Notre Dame had popularized the forward pass in 1913, most eastern teams regarded the weapon as a cowardly stratagem employed in desperation by <b>effete</b> Midwesterners.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/effete/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='effete#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ef_out' data-tree-url='//cdn1.membean.com/public/data/treexml' href='effete#'>
<span class=''></span>
ef-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>out of, from</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='fet_pregnancy' data-tree-url='//cdn1.membean.com/public/data/treexml' href='effete#'>
<span class=''></span>
fet
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>pregnancy, offspring</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='effete#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>If someone has become <em>effete</em>, he is now &#8220;from&#8221; the power of creating &#8220;offspring,&#8221; or is &#8220;out of&#8221; the ability to become &#8220;pregnant&#8221; with ideas; the primary idea here is that one who is <em>effete</em> is simply worn out, and thus is too tired to be productive in any way at all.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='effete#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Effete" src="https://cdn1.membean.com/public/images/wordimages/cons2/effete.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='effete#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>decadent</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>defunct</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>degenerate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>dissolution</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>enervate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>fatuous</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>flaccid</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>inane</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>ineffectual</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>insipid</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>jejune</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>languid</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>listless</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>mediocre</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>pallid</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>superannuated</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>vapid</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>vitiate</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>acumen</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>caliber</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>edification</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>efficacy</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>fecundity</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>formidable</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>forte</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>indefatigable</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>infallible</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>luxuriant</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>morale</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>omnipotent</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>potency</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>rehabilitate</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>resurgence</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>stentorian</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="effete" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>effete</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="effete#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

