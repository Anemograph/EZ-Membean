
<!DOCTYPE html>
<html>
<head>
<title>Word: dubious | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you describe something as ambiguous, you mean that it is unclear or confusing because it can be understood in multiple ways.</p>
<p class='rw-defn idx1' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx2' style='display:none'>If you employ chicanery, you are devising and carrying out clever plans and trickery to cheat and deceive people.</p>
<p class='rw-defn idx3' style='display:none'>If you are ___, you are cautious; you think carefully about something before saying or doing it.</p>
<p class='rw-defn idx4' style='display:none'>A confidant is a close and trusted friend to whom you can tell secret matters of importance and remain assured that they will be kept safe.</p>
<p class='rw-defn idx5' style='display:none'>A conjecture is a theory or guess that is based on information that is not certain or complete.</p>
<p class='rw-defn idx6' style='display:none'>To corroborate something that has been said or reported means to give information or evidence that further supports it.</p>
<p class='rw-defn idx7' style='display:none'>Credence is the mental act of believing or accepting that something is true.</p>
<p class='rw-defn idx8' style='display:none'>If you act in a credible fashion, you are easy to believe or trust.</p>
<p class='rw-defn idx9' style='display:none'>A credulous person is very ready to believe what people tell them; therefore, they can be easily tricked or cheated.</p>
<p class='rw-defn idx10' style='display:none'>A cynical person thinks that people in general are most often motivated by selfish concerns; therefore, they doubt, mistrust, and question what people do.</p>
<p class='rw-defn idx11' style='display:none'>When you debunk someone&#8217;s statement, you show that it is false, thereby exposing the truth of the matter.</p>
<p class='rw-defn idx12' style='display:none'>Something that is delusive deceives you by giving a false belief about yourself or the situation you are in.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is diffident is shy, does not want to draw notice to themselves, and is lacking in self-confidence.</p>
<p class='rw-defn idx14' style='display:none'>Someone who is disingenuous is not straightforward or is dishonest in what they say or do.</p>
<p class='rw-defn idx15' style='display:none'>Empirical evidence or study is based on real experience or scientific experiments rather than on unproven theories.</p>
<p class='rw-defn idx16' style='display:none'>When people equivocate, they avoid making a clear statement; they are deliberately vague in order to avoid giving a direct answer to a question.</p>
<p class='rw-defn idx17' style='display:none'>If you describe something as ersatz, you dislike it because it is artificial or fake—and is used in place of a higher quality item.</p>
<p class='rw-defn idx18' style='display:none'>A euphemism is a polite synonym or expression that people use when they want to avoid talking about—or indirectly replace—an unpleasant or embarrassing thing.</p>
<p class='rw-defn idx19' style='display:none'>A fallacy is an idea or belief that is false.</p>
<p class='rw-defn idx20' style='display:none'>Fidelity towards something, such as a marriage or promise, is faithfulness or loyalty towards it.</p>
<p class='rw-defn idx21' style='display:none'>A gullible person is easy to trick because they are too trusting of other people.</p>
<p class='rw-defn idx22' style='display:none'>Something that is hypothetical is based on possible situations or events rather than actual ones.</p>
<p class='rw-defn idx23' style='display:none'>Something illusory appears real or possible; in fact, it is neither.</p>
<p class='rw-defn idx24' style='display:none'>Something that is implausible is unlikely to be true or hard to believe that it&#8217;s true.</p>
<p class='rw-defn idx25' style='display:none'>Something that is inconceivable cannot be imagined or thought of; that is, it is beyond reason or unbelievable.</p>
<p class='rw-defn idx26' style='display:none'>Incontrovertible facts are certain, unquestionably true, and impossible to doubt however hard you might try to convince yourself otherwise.</p>
<p class='rw-defn idx27' style='display:none'>An irrefutable argument or statement cannot be proven wrong; therefore, it must be accepted because it is certain.</p>
<p class='rw-defn idx28' style='display:none'>A mendacious person does not tell the truth.</p>
<p class='rw-defn idx29' style='display:none'>A mountebank is a smooth-talking, dishonest person who tricks and cheats people.</p>
<p class='rw-defn idx30' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx31' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx32' style='display:none'>A poseur pretends to have a certain quality or social position, usually because they want to influence others in some way.</p>
<p class='rw-defn idx33' style='display:none'>If you prevaricate, you avoid giving a direct or honest answer, usually because you want to hide the truth or want to delay or avoid making a hard decision.</p>
<p class='rw-defn idx34' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx35' style='display:none'>Something that is specious seems to be good, sound, or correct but is actually wrong or fake.</p>
<p class='rw-defn idx36' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx37' style='display:none'>Substantive issues are the most important, serious, and real issues of a subject.</p>
<p class='rw-defn idx38' style='display:none'>An unctuous person acts in an overly deceptive manner that is obviously insincere because they want to convince you of something.</p>
<p class='rw-defn idx39' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx40' style='display:none'>Verisimilitude is something&#8217;s authenticity or appearance of being real or true.</p>
<p class='rw-defn idx41' style='display:none'>Something that is veritable is authentic, actual, genuine, or without doubt.</p>
<p class='rw-defn idx42' style='display:none'>The verity of something is the truth or reality of it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>dubious</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='dubious#' id='pronounce-sound' path='audio/words/amy-dubious'></a>
DOO-bee-uhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='dubious#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='dubious#' id='context-sound' path='audio/wordcontexts/brian-dubious'></a>
I was <em>dubious</em> or doubtful when Jeanette told me that she wanted to be my friend.  I had been trying to get her to be friends with me for five years, so you can imagine why my response to her sudden declaration was so <em>dubious</em> or suspicious.  I had even more reason to be <em>dubious</em> or unsure about her true intentions when I learned that she found out that I had won the lottery.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>dubious</em> story?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is one that reporters have researched and found to be true.
</li>
<li class='choice '>
<span class='result'></span>
It is one that is familiar because it has been told so many times.
</li>
<li class='choice answer '>
<span class='result'></span>
It is one that most people don&#8217;t think really happened.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='dubious#' id='definition-sound' path='audio/wordmeanings/amy-dubious'></a>
If someone makes a <em>dubious</em> claim, there is a great deal of disbelief or doubt about it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>doubtful</em>
</span>
</span>
</div>
<a class='quick-help' href='dubious#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='dubious#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/dubious/memory_hooks/3461.json'></span>
<p>
<span class="emp0"><span>Ser<span class="emp3"><span>ious</span></span> <span class="emp1"><span>D</span></span>o<span class="emp1"><span>ub</span></span>t</span></span>  There is ser<span class="emp3"><span>ious</span></span> <span class="emp1"><span>d</span></span>o<span class="emp1"><span>ub</span></span>t about a <span class="emp1"><span>dub</span></span><span class="emp3"><span>ious</span></span> statement.
</p>
</div>

<div id='memhook-button-bar'>
<a href="dubious#" id="add-public-hook" style="" url="https://membean.com/mywords/dubious/memory_hooks">Use other public hook</a>
<a href="dubious#" id="memhook-use-own" url="https://membean.com/mywords/dubious/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='dubious#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Since 1962, _Esquire’s_ <b>Dubious</b> Achievement Awards have honored the most preposterous, egomaniacal, and monstrous behavior of the past year.
<cite class='attribution'>
&mdash;
Esquire
</cite>
</li>
<li>
Then suddenly Mr. Biskupic got deeply engaged in a series of truly <b>dubious</b> cases, all of which had a distinctly [Karl] Rovian political flavor.
<cite class='attribution'>
&mdash;
Harper's Magazine
</cite>
</li>
<li>
Darren McKinney, 48, a renter in the District, said he has been waiting for housing prices to fall so he can buy a condo without resorting to a <b>dubious</b> loan.
<cite class='attribution'>
&mdash;
Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/dubious/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='dubious#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='dubi_doubt' data-tree-url='//cdn1.membean.com/public/data/treexml' href='dubious#'>
<span class=''></span>
dubi
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>doubt</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ous_full' data-tree-url='//cdn1.membean.com/public/data/treexml' href='dubious#'>
<span class=''></span>
-ous
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>full of, having</td>
</tr>
</table>
<p>A <em>dubious</em> person is &#8220;full of doubt&#8221; about something.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='dubious#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Dubious" src="https://cdn3.membean.com/public/images/wordimages/cons2/dubious.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='dubious#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>ambiguous</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>chicanery</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>circumspect</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>conjecture</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>cynical</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>delusive</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>diffident</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>disingenuous</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>equivocate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>ersatz</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>euphemism</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>fallacy</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>hypothetical</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>illusory</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>implausible</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>inconceivable</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>mendacious</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>mountebank</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>poseur</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>prevaricate</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>specious</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>unctuous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>confidant</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>corroborate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>credence</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>credible</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>credulous</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>debunk</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>empirical</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>fidelity</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>gullible</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>incontrovertible</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>irrefutable</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>substantive</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>verisimilitude</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>veritable</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='dubious#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
indubitable
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>unable to be doubted; unquestionable</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="dubious" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>dubious</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="dubious#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

