
<!DOCTYPE html>
<html>
<head>
<title>Word: argot | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>You can describe something as abstruse if you find it highly complicated and difficult to understand.</p>
<p class='rw-defn idx1' style='display:none'>A cabal is a group of people who secretly meet to plan things in order to gain power.</p>
<p class='rw-defn idx2' style='display:none'>A cadre is a group specifically trained to lead, formalize, and accomplish a particular task or to train others as part of a larger organization; it can also be used to describe an elite military force.</p>
<p class='rw-defn idx3' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx4' style='display:none'>Something that is clandestine is hidden or kept a secret, often because it is illegal.</p>
<p class='rw-defn idx5' style='display:none'>A clique is a group of people that spends a lot of time together and tends to be unfriendly towards people who are not part of the group.</p>
<p class='rw-defn idx6' style='display:none'>Complicity is the involvement in or knowledge of a situation that is illegal or bad.</p>
<p class='rw-defn idx7' style='display:none'>A conclave is a meeting between a group of people who discuss something secretly.</p>
<p class='rw-defn idx8' style='display:none'>If one person connives with another, they secretly plan to achieve something of mutual benefit, usually a thing that is illegal or immoral.</p>
<p class='rw-defn idx9' style='display:none'>A coterie is a small group of people who are close friends; therefore, when its members do things together, they do not want other people to join them.</p>
<p class='rw-defn idx10' style='display:none'>Covert activities or situations are secret and hidden.</p>
<p class='rw-defn idx11' style='display:none'>Dialectic is a dialogue between two people who present logical arguments in order to persuade each other of the truth of their opinion; during this interchange, the debaters arrive at a truth, which often involves parts of each debater&#8217;s opinion.</p>
<p class='rw-defn idx12' style='display:none'>To divulge something is to reveal information that was once private or secret.</p>
<p class='rw-defn idx13' style='display:none'>If you exude a quality or feeling, people easily notice that you have a large quantity of it because it flows from you; if a smell or liquid exudes from something, it flows steadily and slowly from it.</p>
<p class='rw-defn idx14' style='display:none'>If someone behaves in a furtive way, they do things sneakily and secretly in order to avoid being noticed.</p>
<p class='rw-defn idx15' style='display:none'>Idiosyncratic tendencies, behavior, or habits are unusual and strange.</p>
<p class='rw-defn idx16' style='display:none'>Jargon is language or terminology used by a specific group that might not be understandable to those people who are not of the group.</p>
<p class='rw-defn idx17' style='display:none'>Something that is latent, such as an undiscovered talent or undiagnosed disease, exists but is not active or has not developed at the moment—it may develop or become active in the future.</p>
<p class='rw-defn idx18' style='display:none'>A machination is a secretive plan or clever plot that is carefully designed to control events or people.</p>
<p class='rw-defn idx19' style='display:none'>If a mood or feeling is palpable, it is so strong and intense that it is easily noticed and is almost able to be physically felt.</p>
<p class='rw-defn idx20' style='display:none'>You are applying parlance when you use words or expressions that are used by a particular group of people who have a unique way of speaking; for example, &#8220;descendants&#8221; is often used in place of &#8220;children&#8221; in legal parlance.</p>
<p class='rw-defn idx21' style='display:none'>A patois is a form of language used by a people in a small area that is different from the national or standard language.</p>
<p class='rw-defn idx22' style='display:none'>A surreptitious deed is done secretly to avoid bringing any attention to it.</p>
<p class='rw-defn idx23' style='display:none'>Something that is tangible is able to be touched and thus is considered real.</p>
<p class='rw-defn idx24' style='display:none'>A vernacular is the ordinary and everyday language spoken by people in a particular country or region that differs from the literary or standard written language of that area.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>argot</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='argot#' id='pronounce-sound' path='audio/words/amy-argot'></a>
AHR-goh
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='argot#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='argot#' id='context-sound' path='audio/wordcontexts/brian-argot'></a>
When my dad is around me and my friends, he is often confused by our <em>argot</em>, the special language we use with one another. Last week we said a soccer game was GOAT—meaning the Greatest Of All Time in teen <em>argot—and</em> he had no idea what we meant. Since he is not part of our group, the <em>argot</em> or j<em>argon</em> we use is hard for him to understand. To be honest, I’m thankful he doesn’t try to flex by pretending he knows our <em>argot</em>, or how we communicate—that would just make me salty!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How is an <em>argot</em> useful?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It used to allow wanderers to sail very long distances across vast bodies of water.
</li>
<li class='choice '>
<span class='result'></span>
It allows metal to be located within the rock walls of a mine before digging begins.
</li>
<li class='choice answer '>
<span class='result'></span>
It allows a group to safely speak because it uses words that are known only to the members of that group.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='argot#' id='definition-sound' path='audio/wordmeanings/amy-argot'></a>
An <em>argot</em> is a special language or set of expressions used by a particular group of people that those outside the group find hard to understand.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>jargon</em>
</span>
</span>
</div>
<a class='quick-help' href='argot#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='argot#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/argot/memory_hooks/5129.json'></span>
<p>
<span class="emp0"><span>Ecretsay Odecay</span></span> <span class="emp1"><span>Argotway</span></span> isway away ecialspay ordway usedway ybay eakysnay igspay.
</p>
</div>

<div id='memhook-button-bar'>
<a href="argot#" id="add-public-hook" style="" url="https://membean.com/mywords/argot/memory_hooks">Use other public hook</a>
<a href="argot#" id="memhook-use-own" url="https://membean.com/mywords/argot/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='argot#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Baseball's "three-bagger," football's "first and 10," basketball's "pick and roll" and ice hockey's "changing on the fly" are examples. Those who do not play the game or watch it avidly are apt to be confused by this <b>argot</b>.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Every industry has its <b>argot</b>, but people who work in the advertising “space” seem to love insider language more than most.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
This "dense sprawl," as it is known in the <b>argot</b> of urban planners, has helped shift public opinion toward support for public transportation.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/argot/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='argot#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>From the root <em>argot</em> which means &#8220;slang.&#8221;</p>
<a href="argot#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> French</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='argot#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Livestock Auctioneers</strong><span> Two examples of the argot of the auctioneer.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/argot.jpg' video_url='examplevids/argot' video_width='350'></span>
<div id='wt-container'>
<img alt="Argot" height="288" src="https://cdn1.membean.com/video/examplevids/argot.jpg" width="350" />
<div class='center'>
<a href="argot#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='argot#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Argot" src="https://cdn3.membean.com/public/images/wordimages/cons2/argot.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='argot#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abstruse</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>cabal</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>cadre</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>clandestine</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>clique</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>complicity</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>conclave</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>connive</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>coterie</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>covert</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>dialectic</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>furtive</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>idiosyncratic</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>jargon</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>latent</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>machination</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>parlance</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>patois</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>surreptitious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>vernacular</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>divulge</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>exude</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>palpable</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>tangible</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="argot" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>argot</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="argot#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

