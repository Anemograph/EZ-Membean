
<!DOCTYPE html>
<html>
<head>
<title>Word: inquisitive | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you do something with alacrity, you do it eagerly and quickly.</p>
<p class='rw-defn idx1' style='display:none'>When you act in an aloof fashion towards others, you purposely do not participate in activities with them; instead, you remain distant and detached.</p>
<p class='rw-defn idx2' style='display:none'>Apathy is a lack of interest or unenthusiastic involvement in an activity; there is no effort to change or improve it at all.</p>
<p class='rw-defn idx3' style='display:none'>When you have ardor for something, you have an intense feeling of love, excitement, and admiration for it.</p>
<p class='rw-defn idx4' style='display:none'>When you broach a subject, especially one that may be embarrassing or unpleasant, you mention it in order to begin a discussion about it.</p>
<p class='rw-defn idx5' style='display:none'>A captious person has a fondness for catching others at fault; hence, they are overly critical and raise unwarranted objections too often.</p>
<p class='rw-defn idx6' style='display:none'>To cavil is to make unnecessary complaints about things that are unimportant.</p>
<p class='rw-defn idx7' style='display:none'>If you describe someone, usually a young woman, as demure, you mean that she is quiet, shy, and always behaves modestly.</p>
<p class='rw-defn idx8' style='display:none'>Someone does something in a disinterested way when they have no personal involvement or attachment to the action.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is dour is serious, stubborn, and unfriendly; they can also be gloomy.</p>
<p class='rw-defn idx10' style='display:none'>Someone who is effusive expresses happiness, pleasure, admiration, praise, etc., in an extremely enthusiastic way.</p>
<p class='rw-defn idx11' style='display:none'>Someone&#8217;s elocution is their artistic manner of speaking in public, including both the delivery of their voice and gestures.</p>
<p class='rw-defn idx12' style='display:none'>Ennui is the feeling of being bored, tired, and dissatisfied with life.</p>
<p class='rw-defn idx13' style='display:none'>If you expostulate with someone, you express strong disagreement with or disapproval of what that person is doing.</p>
<p class='rw-defn idx14' style='display:none'>If you show exuberance, you display great excitement, energy, and enthusiasm.</p>
<p class='rw-defn idx15' style='display:none'>A garrulous person talks a lot, especially about unimportant things.</p>
<p class='rw-defn idx16' style='display:none'>A gregarious person is friendly, highly social, and prefers being with people rather than being alone.</p>
<p class='rw-defn idx17' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is impetuous does things quickly and rashly without thinking carefully first.</p>
<p class='rw-defn idx19' style='display:none'>If you are indifferent about something, you are uninterested or neutral about it, not caring either in a positive or negative way.</p>
<p class='rw-defn idx20' style='display:none'>Something that inhibits you from doing something restricts or keeps you from doing it.</p>
<p class='rw-defn idx21' style='display:none'>Insouciance is a lack of concern or worry for something that should be shown more careful attention or consideration.</p>
<p class='rw-defn idx22' style='display:none'>Someone who is jaded is bored with or weary of the world because they have had too much experience with it.</p>
<p class='rw-defn idx23' style='display:none'>If you do something in a lackadaisical way, you do it carelessly and without putting much effort into it—thereby showing that you are not really interested in what you&#8217;re doing.</p>
<p class='rw-defn idx24' style='display:none'>A person who is being laconic uses very few words to say something.</p>
<p class='rw-defn idx25' style='display:none'>Lassitude is a state of tiredness, lack of energy, and having little interest in what&#8217;s going on around you.</p>
<p class='rw-defn idx26' style='display:none'>If you are lethargic, you are tired, lack energy, and are unwilling to exert effort.</p>
<p class='rw-defn idx27' style='display:none'>When you are listless, you lack energy and interest and are unwilling to exert any effort.</p>
<p class='rw-defn idx28' style='display:none'>Someone who is loquacious talks too much.</p>
<p class='rw-defn idx29' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx30' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx31' style='display:none'>An overt act is not hidden or secret but is done in an open and public way.</p>
<p class='rw-defn idx32' style='display:none'>The art of pedagogy is the methods used by a teacher to teach a subject.</p>
<p class='rw-defn idx33' style='display:none'>If you have a penchant for some activity, you have a strong fondness for it and thus find it enjoyable.</p>
<p class='rw-defn idx34' style='display:none'>Prattle is mindless chatter that seems like it will never stop.</p>
<p class='rw-defn idx35' style='display:none'>To remonstrate with someone is to tell that person that you strongly disapprove of something they have said or done.</p>
<p class='rw-defn idx36' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx37' style='display:none'>If you are somnolent, you are sleepy.</p>
<p class='rw-defn idx38' style='display:none'>A tacit agreement between two people is understood without having to use words to express it.</p>
<p class='rw-defn idx39' style='display:none'>A taciturn person is quiet or silent by nature.</p>
<p class='rw-defn idx40' style='display:none'>If you are suffering from tedium, you are bored.</p>
<p class='rw-defn idx41' style='display:none'>If your body is affected by torpor, you are severely lacking in energy; therefore, you are idle—and can even be numb.</p>
<p class='rw-defn idx42' style='display:none'>To be under someone&#8217;s tutelage is to be under their guidance and teaching.</p>
<p class='rw-defn idx43' style='display:none'>A voluble person can speak easily with speech that naturally flows; voluble people also tend to be quite chatty.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>inquisitive</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='inquisitive#' id='pronounce-sound' path='audio/words/amy-inquisitive'></a>
in-KWIS-i-tiv
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='inquisitive#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='inquisitive#' id='context-sound' path='audio/wordcontexts/brian-inquisitive'></a>
Since I am a teacher I love <em>inquisitive</em> students who are most eager to learn.  All I require of my <em>inquisitive</em> students is that they raise their hands before asking questions, instead of blurting them out.  Every now and then I have a student who is a bit too curious or <em>inquisitive</em>, and sometimes I have to ignore their constantly raised hand.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If you are an <em>inquisitive</em> person, what would be the best job to have?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A pastry chef because they create delicious and appealing desserts.
</li>
<li class='choice answer '>
<span class='result'></span>
An investigator because they ask questions in order to solve a crime.
</li>
<li class='choice '>
<span class='result'></span>
A teacher because they share their knowledge with students. 
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='inquisitive#' id='definition-sound' path='audio/wordmeanings/amy-inquisitive'></a>
If you are <em>inquisitive</em>, you are eager to learn and highly curious—sometimes too much so.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>very curious</em>
</span>
</span>
</div>
<a class='quick-help' href='inquisitive#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='inquisitive#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/inquisitive/memory_hooks/5274.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Inqui</span></span>re Po<span class="emp3"><span>sitive</span></span>ly</span></span> An <span class="emp2"><span>inqui</span></span><span class="emp3"><span>sitive</span></span> person <span class="emp2"><span>inqui</span></span>res in a po<span class="emp3"><span>sitive</span></span> fashion because she is highly eager to learn.
</p>
</div>

<div id='memhook-button-bar'>
<a href="inquisitive#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/inquisitive/memory_hooks">Use other hook</a>
<a href="inquisitive#" id="memhook-use-own" url="https://membean.com/mywords/inquisitive/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='inquisitive#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Third, although leaders might say they treasure <b>inquisitive</b> minds, in fact, most stifle curiosity, fearing it will increase risk and inefficiency.
<cite class='attribution'>
&mdash;
Harvard Business Review
</cite>
</li>
<li>
On one side, without a basic level of data literacy, people won’t understand the data sufficiently to be <b>inquisitive</b> with the numbers (at least not in meaningful ways).
<cite class='attribution'>
&mdash;
Forbes
</cite>
</li>
<li>
Iowa plays an important role in the process of selecting presidential candidates, and its electorate has earned its reputation—along with New Hampshire’s—for being studious, attentive, <b>inquisitive</b> and engaged.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/inquisitive/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='inquisitive#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='in_in' data-tree-url='//cdn1.membean.com/public/data/treexml' href='inquisitive#'>
<span class='common'></span>
in-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>in, within</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='quisit_seek' data-tree-url='//cdn1.membean.com/public/data/treexml' href='inquisitive#'>
<span class='common'></span>
quisit
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>seek, ask, strive for</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ive_does' data-tree-url='//cdn1.membean.com/public/data/treexml' href='inquisitive#'>
<span class=''></span>
-ive
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or that which does something</td>
</tr>
</table>
<p>Someone who is <em>inquisitive</em> loves to &#8220;seek within&#8221; things to find out about them.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='inquisitive#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Louis CK Comedy Concert</strong><span> This comedian has an extremely inquisitive daughter.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/inquisitive.jpg' video_url='examplevids/inquisitive' video_width='350'></span>
<div id='wt-container'>
<img alt="Inquisitive" height="198" src="https://cdn1.membean.com/video/examplevids/inquisitive.jpg" width="350" />
<div class='center'>
<a href="inquisitive#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='inquisitive#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Inquisitive" src="https://cdn3.membean.com/public/images/wordimages/cons2/inquisitive.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='inquisitive#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>alacrity</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>ardor</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>broach</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>captious</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>cavil</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>effusive</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>elocution</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>expostulate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>exuberance</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>garrulous</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>gregarious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>impetuous</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>loquacious</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>overt</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>pedagogy</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>penchant</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>prattle</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>remonstrate</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>tutelage</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>voluble</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>aloof</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>apathy</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>demure</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>disinterested</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>dour</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>ennui</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>indifferent</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>inhibit</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>insouciance</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>jaded</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>lackadaisical</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>laconic</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>lassitude</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>lethargic</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>listless</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>somnolent</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>tacit</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>taciturn</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>tedium</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>torpor</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="inquisitive" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>inquisitive</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="inquisitive#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

