
<!DOCTYPE html>
<html>
<head>
<title>Word: paramount | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Something that happens unexpectedly and by chance that is not inherent or natural to a given item or situation is adventitious, such as a root appearing in an unusual place on a plant.</p>
<p class='rw-defn idx1' style='display:none'>The apex of anything, such as an organization or system, is its highest part or most important position.</p>
<p class='rw-defn idx2' style='display:none'>The best or perfect example of something is its apotheosis; likewise, this word can be used to indicate the best or highest point in someone&#8217;s life or job.</p>
<p class='rw-defn idx3' style='display:none'>When you claim that something is banal, you do not like it because you think it is ordinary, dull, commonplace, and boring.</p>
<p class='rw-defn idx4' style='display:none'>A cardinal rule or quality is considered to be the most important or basic in a set of rules or qualities.</p>
<p class='rw-defn idx5' style='display:none'>Chaff is the husks of grain separated from the edible plant; it can also be useless matter in general that is cast aside.</p>
<p class='rw-defn idx6' style='display:none'>A chronicle is a record of historical events that arranges those events in the correct order in which they happened.</p>
<p class='rw-defn idx7' style='display:none'>When you commemorate a person, you honor them or cause them to be remembered in some way.</p>
<p class='rw-defn idx8' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx9' style='display:none'>The crux of a problem or argument is the most important or difficult part of it that affects everything else.</p>
<p class='rw-defn idx10' style='display:none'>A cynosure is an object that serves as the center of attention.</p>
<p class='rw-defn idx11' style='display:none'>If someone is deified, they have been either made into a god or are adored like one.</p>
<p class='rw-defn idx12' style='display:none'>Detritus is the small pieces of waste that remain after something has been destroyed or used.</p>
<p class='rw-defn idx13' style='display:none'>When one country has dominion over another, it rules or controls it absolutely.</p>
<p class='rw-defn idx14' style='display:none'>Something described as dross is of very poor quality and has little or no value.</p>
<p class='rw-defn idx15' style='display:none'>Something that is epochal is highly significant or important; this adjective often refers to the bringing about or marking of the beginning of a new era.</p>
<p class='rw-defn idx16' style='display:none'>Something that is extraneous is not relevant or connected to something else, or it is not essential to a given situation.</p>
<p class='rw-defn idx17' style='display:none'>If you think something, such as a statement or idea, is fatuous, you consider it stupid or extremely silly; a fatuous hope is unrealistic.</p>
<p class='rw-defn idx18' style='display:none'>Something frivolous is not worth taking seriously or considering because it is silly or childish.</p>
<p class='rw-defn idx19' style='display:none'>If something is gratuitous, it is freely given; nevertheless, it is usually unnecessary and can be harmful or upsetting.</p>
<p class='rw-defn idx20' style='display:none'>Hegemony manifests when a country, group, or organization has more political control or influence than others.</p>
<p class='rw-defn idx21' style='display:none'>Inconsequential matters are unimportant or are of little to no significance.</p>
<p class='rw-defn idx22' style='display:none'>An indispensable item is absolutely necessary or essential—it cannot be done without.</p>
<p class='rw-defn idx23' style='display:none'>Something insipid is dull, boring, and has no interesting features; for example, insipid food has no taste or little flavor.</p>
<p class='rw-defn idx24' style='display:none'>Something that is integral to something else is an essential or necessary part of it.</p>
<p class='rw-defn idx25' style='display:none'>Irrelevant information is unrelated or unconnected to the situation at hand.</p>
<p class='rw-defn idx26' style='display:none'>If you describe ideas as jejune, you are saying they are childish and uninteresting; the adjective jejune also describes those having little experience, maturity, or knowledge.</p>
<p class='rw-defn idx27' style='display:none'>When someone is lionized, they are treated as being very important or famous—although they may not deserve to be.</p>
<p class='rw-defn idx28' style='display:none'>A luminary is someone who is much admired in a particular profession because they are an accomplished expert in their field.</p>
<p class='rw-defn idx29' style='display:none'>A momentous occurrence is very important, significant, or vital in some way.</p>
<p class='rw-defn idx30' style='display:none'>A monumental event is very great, impressive, or extremely important in some way.</p>
<p class='rw-defn idx31' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx32' style='display:none'>Nominal can refer to someone who is in charge in name only, or it can refer to a very small amount of something; both are related by their relative insignificance.</p>
<p class='rw-defn idx33' style='display:none'>Something that is paltry is practically worthless or insignificant.</p>
<p class='rw-defn idx34' style='display:none'>Anything picayune is unimportant, insignificant, or minor.</p>
<p class='rw-defn idx35' style='display:none'>Something that is pivotal is more important than anything else in a situation or a system; consequently, it is the key to its success.</p>
<p class='rw-defn idx36' style='display:none'>Something predominant is the most important or the most common thing in a group.</p>
<p class='rw-defn idx37' style='display:none'>A preponderance of things of a particular type in a group means that there are more of that type than of any other.</p>
<p class='rw-defn idx38' style='display:none'>Something that is prodigious is very large, impressive, or great.</p>
<p class='rw-defn idx39' style='display:none'>If you have prowess, you possess considerable skill or ability in something.</p>
<p class='rw-defn idx40' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>
<p class='rw-defn idx41' style='display:none'>The salient qualities of an issue or feature are those that are most important and noticeable.</p>
<p class='rw-defn idx42' style='display:none'>A stupendous event or accomplishment is amazing, wonderful, or spectacular.</p>
<p class='rw-defn idx43' style='display:none'>Something that is superfluous is unnecessary; it is more than what is wanted or needed at the current time.</p>
<p class='rw-defn idx44' style='display:none'>A tangential point in an argument merely touches upon something that is not really relevant to the conversation at hand; rather, it is a minor or unimportant point.</p>
<p class='rw-defn idx45' style='display:none'>If you refer to something as a trifle, you mean that it is of little or no importance or value.</p>
<p class='rw-defn idx46' style='display:none'>Something trivial is of little value or is not important.</p>
<p class='rw-defn idx47' style='display:none'>Something in the vanguard is in the leading spot of something, such as an army or institution.</p>
<p class='rw-defn idx48' style='display:none'>Venerable people command respect because they are old and wise.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>paramount</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='paramount#' id='pronounce-sound' path='audio/words/amy-paramount'></a>
PAR-uh-mount
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='paramount#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='paramount#' id='context-sound' path='audio/wordcontexts/brian-paramount'></a>
It is of <em>paramount</em> or the greatest importance that you get your passport before you travel abroad.  If you do not do this task of <em>paramount</em> or greatest significance, you will not be allowed within any other country&#8217;s borders.  As soon as you obtain this <em>paramount</em> or principal identification you can attend to other important traveling duties, such as packing clothing and making sure you have enough funding.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If something is <em>paramount</em> to your success, what is it?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is more important than your success and will take priority.
</li>
<li class='choice '>
<span class='result'></span>
It is something that is preventing you from succeeding.
</li>
<li class='choice answer '>
<span class='result'></span>
It is the most important thing needed for your success.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='paramount#' id='definition-sound' path='audio/wordmeanings/amy-paramount'></a>
Something that is of <em>paramount</em> importance or significance is chief or supreme in those things.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>chief</em>
</span>
</span>
</div>
<a class='quick-help' href='paramount#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='paramount#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/paramount/memory_hooks/3585.json'></span>
<p>
<img alt="Paramount" src="https://cdn1.membean.com/public/images/wordimages/hook/paramount.jpg?qdep8" />
<span class="emp0"><span><span class="emp1"><span>Paramount</span></span> Pictures</span></span> <span class="emp1"><span>Paramount</span></span> Pictures would like its audiences to believe that they are the <span class="emp1"><span>paramount</span></span> film makers out of all other film companies.
</p>
</div>

<div id='memhook-button-bar'>
<a href="paramount#" id="add-public-hook" style="" url="https://membean.com/mywords/paramount/memory_hooks">Use other public hook</a>
<a href="paramount#" id="memhook-use-own" url="https://membean.com/mywords/paramount/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='paramount#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
But while the consequences of the election are of <b>paramount</b> significance, the challenge posed by these elections provides valuable insight into why voting is so fundamentally important to the fabric of our country in the first place.
<cite class='attribution'>
&mdash;
The Highlander
</cite>
</li>
<li>
Welcoming today's vote to ban the substances, the EU's Commissioner for Health and Food Safety Vytenis Andriukaitis said, "Bee health remains of <b>paramount</b> importance for me since it concerns biodiversity, food production and the environment."
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
We have found it of <b>paramount</b> importance that in order to progress, we must recognize our ignorance and leave room for doubt.
<cite class='attribution'>
&mdash;
Richard P. Feynman, 20th century American theoretical physicist
</cite>
</li>
<li>
The trials will be conducted in “secure clinical research facilities" that are "specifically designed to contain the virus,” according to the government, which stressed that the safety of trial participants was <b>paramount</b>.
<cite class='attribution'>
&mdash;
ABC News
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/paramount/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='paramount#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='per_through' data-tree-url='//cdn1.membean.com/public/data/treexml' href='paramount#'>
<span class=''></span>
per-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>through, by</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='a_towards' data-tree-url='//cdn1.membean.com/public/data/treexml' href='paramount#'>
<span class='common'></span>
a-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to, towards</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='mount_mountain' data-tree-url='//cdn1.membean.com/public/data/treexml' href='paramount#'>
<span class=''></span>
mount
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>mountain</td>
</tr>
</table>
<p>When something is <em>paramount</em> in importance, it has gone &#8220;by towards the mountain,&#8221; that is, it has traveled &#8220;towards&#8221; the highest places, and hence is of great importance.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='paramount#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Star Trek the Next Generation</strong><span> Worf must go back to the Enterprise because its security is of paramount importance.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/paramount.jpg' video_url='examplevids/paramount' video_width='350'></span>
<div id='wt-container'>
<img alt="Paramount" height="288" src="https://cdn1.membean.com/video/examplevids/paramount.jpg" width="350" />
<div class='center'>
<a href="paramount#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='paramount#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Paramount" src="https://cdn0.membean.com/public/images/wordimages/cons2/paramount.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='paramount#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>apex</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>apotheosis</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>cardinal</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>chronicle</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>commemorate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>crux</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>cynosure</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>deify</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>dominion</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>epochal</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>hegemony</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>indispensable</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>integral</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>lionize</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>luminary</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>momentous</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>monumental</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>pivotal</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>predominant</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>preponderance</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>prodigious</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>prowess</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>salient</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>stupendous</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>vanguard</span> &middot;</li><li data-idx='48' class = 'rw-wordform notlearned'><span>venerable</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>adventitious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>banal</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>chaff</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>detritus</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>dross</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>extraneous</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>fatuous</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>frivolous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>gratuitous</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>inconsequential</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>insipid</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>irrelevant</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>jejune</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>nominal</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>paltry</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>picayune</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>superfluous</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>tangential</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>trifle</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>trivial</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="paramount" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>paramount</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="paramount#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

