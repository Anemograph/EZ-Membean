
<!DOCTYPE html>
<html>
<head>
<title>Word: disinterested | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If your behavior or manner is altruistic, you show you care more about other people and their interests than you care about yourself.</p>
<p class='rw-defn idx1' style='display:none'>If you have animus against someone, you have a strong feeling of dislike or hatred towards them, often without a good reason or based upon your personal prejudices.</p>
<p class='rw-defn idx2' style='display:none'>Antipathy is a strong feeling of dislike or hostility towards someone or something.</p>
<p class='rw-defn idx3' style='display:none'>Apathy is a lack of interest or unenthusiastic involvement in an activity; there is no effort to change or improve it at all.</p>
<p class='rw-defn idx4' style='display:none'>Bigotry is the expression of strong and unreasonable opinions without accepting or tolerating opposing views.</p>
<p class='rw-defn idx5' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx6' style='display:none'>If one&#8217;s powers or rights are circumscribed, they are limited or restricted.</p>
<p class='rw-defn idx7' style='display:none'>If you delineate something, such as an idea or situation or border, you describe it in great detail.</p>
<p class='rw-defn idx8' style='display:none'>When you are disposed towards a particular action or thing, you are inclined or partial towards it.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx10' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx11' style='display:none'>If you expostulate with someone, you express strong disagreement with or disapproval of what that person is doing.</p>
<p class='rw-defn idx12' style='display:none'>Fanaticism is the condition of being overly enthusiastic or eager about a cause to the point of being extreme and unreasonable about it.</p>
<p class='rw-defn idx13' style='display:none'>When you are impassioned about a cause or idea, you are very passionate or highly emotionally charged about it.</p>
<p class='rw-defn idx14' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx15' style='display:none'>If someone is imperturbable, they are always calm and not easily upset or disturbed by any situation, even dangerous ones.</p>
<p class='rw-defn idx16' style='display:none'>If you are indifferent about something, you are uninterested or neutral about it, not caring either in a positive or negative way.</p>
<p class='rw-defn idx17' style='display:none'>If you are inquisitive, you are eager to learn and highly curious—sometimes too much so.</p>
<p class='rw-defn idx18' style='display:none'>Insouciance is a lack of concern or worry for something that should be shown more careful attention or consideration.</p>
<p class='rw-defn idx19' style='display:none'>An intransigent person is stubborn; thus, they refuse to change their ideas or behavior, often in a way that seems unreasonable.</p>
<p class='rw-defn idx20' style='display:none'>Someone who is jaded is bored with or weary of the world because they have had too much experience with it.</p>
<p class='rw-defn idx21' style='display:none'>If someone has a jaundiced view of something, they ignore the good parts and can only see the bad aspects of it.</p>
<p class='rw-defn idx22' style='display:none'>If you do something in a lackadaisical way, you do it carelessly and without putting much effort into it—thereby showing that you are not really interested in what you&#8217;re doing.</p>
<p class='rw-defn idx23' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx24' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is obstreperous is noisy, unruly, and difficult to control.</p>
<p class='rw-defn idx26' style='display:none'>If someone is predisposed to something, they are made favorable or inclined to it in advance, or they are made susceptible to something, such as a disease.</p>
<p class='rw-defn idx27' style='display:none'>If you are prone to doing something, you are likely or inclined to do it.</p>
<p class='rw-defn idx28' style='display:none'>A recalcitrant animal or person is difficult to control and refuses to obey orders—even after stiff punishment.</p>
<p class='rw-defn idx29' style='display:none'>To remonstrate with someone is to tell that person that you strongly disapprove of something they have said or done.</p>
<p class='rw-defn idx30' style='display:none'>A schism causes a group or organization to divide into two groups as a result of differences in their aims and beliefs.</p>
<p class='rw-defn idx31' style='display:none'>A stricture is a rule or condition that imposes restrictions or limits on what you can do.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>disinterested</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='disinterested#' id='pronounce-sound' path='audio/words/amy-disinterested'></a>
dis-IN-tri-stid
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='disinterested#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='disinterested#' id='context-sound' path='audio/wordcontexts/brian-disinterested'></a>
As Roger considered the applicants, he ended up choosing an employee in whom he was personally <em>disinterested</em> so he would be completely free of self-interest in the matter.  Even though his own brother had applied for the job, Roger felt that it was best for him to have no connection to his staff, thereby remaining personally <em>disinterested</em> and totally unbiased during the hiring process.  Roger appointed Jennifer to do his training because he wanted to remain remote, detached, and <em>disinterested</em> while the new employee began his work to give him as fair a chance as possible.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>disinterested</em> party to a decision?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Someone who is informed of the decision after the fact.
</li>
<li class='choice answer '>
<span class='result'></span>
Someone who has no personal involvement in the decision.
</li>
<li class='choice '>
<span class='result'></span>
Someone who is not allowed to take part in the decision.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='disinterested#' id='definition-sound' path='audio/wordmeanings/amy-disinterested'></a>
Someone does something in a <em>disinterested</em> way when they have no personal involvement or attachment to the action.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>unbiased</em>
</span>
</span>
</div>
<a class='quick-help' href='disinterested#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='disinterested#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/disinterested/memory_hooks/4927.json'></span>
<p>
<span class="emp0"><span>Showing No <span class="emp3"><span>Interest</span></span>, One Way or the Other</span></span> If one is truly <span class="emp1"><span>d</span></span>i<span class="emp1"><span>s</span></span><span class="emp3"><span>interest</span></span>ed in a procedure, one <span class="emp1"><span>d</span></span>oe<span class="emp1"><span>s</span></span>n't show <span class="emp3"><span>interest</span></span> in its outcome at all, not caring one way or the other, because one has no personal involvement in it.
</p>
</div>

<div id='memhook-button-bar'>
<a href="disinterested#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/disinterested/memory_hooks">Use other hook</a>
<a href="disinterested#" id="memhook-use-own" url="https://membean.com/mywords/disinterested/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='disinterested#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Neiman Marcus attorneys had presented [the two independent directors] to the court as “<b>disinterested</b> managers” who would investigate the transfer. . . . The bondholder’s attorney presented arguments that those board members were not truly independent and testimony revealed so, according to the judge.
<cite class='attribution'>
&mdash;
The Dallas Morning News
</cite>
</li>
<li>
“From legacy costs, to health-care costs, to (fuel efficiency) costs, to embedded taxes, Detroit can only thrive if Washington is an engaged partner, not a <b>disinterested</b> observer,” [Mitt Romney] plans to say, according to the excerpts.
<cite class='attribution'>
&mdash;
Reuters
</cite>
</li>
<li>
He comes back to The Microphones not as some nostalgist but a time traveler attempting to make sense of the "<b>disinterested</b> sun" that still rises and sets everyday, unaware of its participants.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
[U]nlike the Times, the Journal, and the Post, Knight Ridder doesn't have the protection of a two-tiered ownership system that gives voting control to insiders—usually members of the founders' families, who agree with the journalism-comes-first credo. So Ridder is completely exposed to the pressure of <b>disinterested</b> shareholders—<b>disinterested</b>, that is, in anything but the best return possible.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/disinterested/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='disinterested#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='dis_apart' data-tree-url='//cdn1.membean.com/public/data/treexml' href='disinterested#'>
<span class='common'></span>
dis-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>apart, not, away from</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='inter_between' data-tree-url='//cdn1.membean.com/public/data/treexml' href='disinterested#'>
<span class='common'></span>
inter-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>between, within, among</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='est_be' data-tree-url='//cdn1.membean.com/public/data/treexml' href='disinterested#'>
<span class=''></span>
est
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>be</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ed_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='disinterested#'>
<span class=''></span>
-ed
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>having a particular state</td>
</tr>
</table>
<p>To be <em>disinterested</em> is to &#8220;not be within&#8221; a situation, or to &#8220;be apart from between&#8221; a situation.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='disinterested#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Witness for the Prosecution</strong><span> His wife isn't exactly disinterested.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/disinterested.jpg' video_url='examplevids/disinterested' video_width='350'></span>
<div id='wt-container'>
<img alt="Disinterested" height="288" src="https://cdn1.membean.com/video/examplevids/disinterested.jpg" width="350" />
<div class='center'>
<a href="disinterested#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='disinterested#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Disinterested" src="https://cdn3.membean.com/public/images/wordimages/cons2/disinterested.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='disinterested#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>apathy</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>imperturbable</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>indifferent</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>insouciance</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>jaded</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>lackadaisical</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>altruistic</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>animus</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>antipathy</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>bigotry</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>circumscribe</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>delineate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>disposed</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>expostulate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>fanaticism</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>impassioned</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>inquisitive</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>intransigent</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>jaundiced</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>obstreperous</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>predispose</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>prone</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>recalcitrant</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>remonstrate</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>schism</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>stricture</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="disinterested" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>disinterested</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="disinterested#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

