
<!DOCTYPE html>
<html>
<head>
<title>Word: exemplify | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you describe something as ambiguous, you mean that it is unclear or confusing because it can be understood in multiple ways.</p>
<p class='rw-defn idx1' style='display:none'>The best or perfect example of something is its apotheosis; likewise, this word can be used to indicate the best or highest point in someone&#8217;s life or job.</p>
<p class='rw-defn idx2' style='display:none'>An archetype is a perfect or typical example of something because it has the most important qualities that belong to that type of thing; it can also describe essential qualities common to a particular class of things.</p>
<p class='rw-defn idx3' style='display:none'>A bravura performance, such as of a highly technical work for the violin, is done with consummate skill.</p>
<p class='rw-defn idx4' style='display:none'>If something confounds you, it makes you feel surprised and confused, often because it does not meet your predefined expectations.</p>
<p class='rw-defn idx5' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx6' style='display:none'>A definitive opinion on an issue cannot be challenged; therefore, it is the final word or the most authoritative pronouncement on that issue.</p>
<p class='rw-defn idx7' style='display:none'>If something is done for someone&#8217;s edification, it is done to benefit that person by teaching them something that improves or enlightens their knowledge or character.</p>
<p class='rw-defn idx8' style='display:none'>When you speak in an eloquent fashion, you speak beautifully in an expressive way that is convincing to an audience.</p>
<p class='rw-defn idx9' style='display:none'>If you elucidate something, you make it easier to understand by giving more relevant information.</p>
<p class='rw-defn idx10' style='display:none'>An embodiment of something, such as a quality or idea, is a visible representation or concrete expression of it.</p>
<p class='rw-defn idx11' style='display:none'>If you emulate someone, you try to behave the same way they do because you admire them a great deal.</p>
<p class='rw-defn idx12' style='display:none'>Someone or something that is enigmatic is mysterious and difficult to understand.</p>
<p class='rw-defn idx13' style='display:none'>If you say that a person or thing is the epitome of something, you mean that they or it is the best possible example of that thing.</p>
<p class='rw-defn idx14' style='display:none'>When people equivocate, they avoid making a clear statement; they are deliberately vague in order to avoid giving a direct answer to a question.</p>
<p class='rw-defn idx15' style='display:none'>An exemplary person is one that sets an ideal or praiseworthy example for others to follow.</p>
<p class='rw-defn idx16' style='display:none'>To explicate an idea or plan is to make it clear by explaining it.</p>
<p class='rw-defn idx17' style='display:none'>When you expound something, you explain it in great detail, often taking a while to do so.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx19' style='display:none'>If you describe someone&#8217;s appearance or behavior as impeccable, you mean that it is perfect and therefore impossible to criticize.</p>
<p class='rw-defn idx20' style='display:none'>Someone who is ineffectual at a task is useless or ineffective when attempting to do it.</p>
<p class='rw-defn idx21' style='display:none'>Someone, such as a performer or athlete, is inimitable when they are so good or unique in their talent that it is unlikely anyone else can be their equal.</p>
<p class='rw-defn idx22' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx23' style='display:none'>If someone is lucid, they are able to understand things and think clearly; this adjective also describes writing or speech that is crystal clear and easy to understand.</p>
<p class='rw-defn idx24' style='display:none'>A luminary is someone who is much admired in a particular profession because they are an accomplished expert in their field.</p>
<p class='rw-defn idx25' style='display:none'>Something mediocre is average or ordinary in quality; it’s just OK.</p>
<p class='rw-defn idx26' style='display:none'>Someone who is meritorious is worthy of receiving recognition or is praiseworthy because of what they have accomplished.</p>
<p class='rw-defn idx27' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx28' style='display:none'>If you describe something as nebulous, you mean that it is unclear, vague, and not clearly defined; a shape that is nebulous has no clear boundaries.</p>
<p class='rw-defn idx29' style='display:none'>Something nonpareil has no equal because it is much better than all others of its kind or type.</p>
<p class='rw-defn idx30' style='display:none'>To obfuscate something is to make it deliberately unclear, confusing, and difficult to understand.</p>
<p class='rw-defn idx31' style='display:none'>Something is opaque if it is either difficult to understand or is not transparent.</p>
<p class='rw-defn idx32' style='display:none'>A paradigm is a model or example of something that shows how it works or how it can be produced; a paradigm shift is a completely new way of thinking about something.</p>
<p class='rw-defn idx33' style='display:none'>A paragon is an example of a thing or person at its or their very best.</p>
<p class='rw-defn idx34' style='display:none'>The art of pedagogy is the methods used by a teacher to teach a subject.</p>
<p class='rw-defn idx35' style='display:none'>When you personify something, such as a quality, you are the perfect example of it.</p>
<p class='rw-defn idx36' style='display:none'>Something prosaic is dull, boring, and ordinary.</p>
<p class='rw-defn idx37' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>exemplify</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='exemplify#' id='pronounce-sound' path='audio/words/amy-exemplify'></a>
ig-ZEM-pluh-fahy
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='exemplify#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='exemplify#' id='context-sound' path='audio/wordcontexts/brian-exemplify'></a>
When you create your portfolio to get into art school, make sure that the portfolio is filled with the art that best <em><em>exemplifies</em></em> or illustrates your unique talent.  I would suggest including your two recent pieces, which certainly <em>exemplify</em> or demonstrate your ability with watercolors.  You might want to put those still life paintings in there as well that you did during your junior year, which most certainly <em>exemplify</em> or serve as an example of your ability in that artistic style.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Why would your teacher <em>exemplify</em> something?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
To allow you to discuss your opinion about it.
</li>
<li class='choice '>
<span class='result'></span>
To see how much you have learned about it.
</li>
<li class='choice answer '>
<span class='result'></span>
To show you exactly how they want it done.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='exemplify#' id='definition-sound' path='audio/wordmeanings/amy-exemplify'></a>
One thing that <em>exemplifies</em> another serves as an example of it, illustrates it, or demonstrates it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>demonstrate</em>
</span>
</span>
</div>
<a class='quick-help' href='exemplify#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='exemplify#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/exemplify/memory_hooks/5337.json'></span>
<p>
<span class="emp0"><span>Cert<span class="emp3"><span>ify</span></span> an <span class="emp1"><span>Exampl</span></span>e</span></span> When you <span class="emp1"><span>exempl</span></span>ify good manners, you make an <span class="emp1"><span>exampl</span></span>e of them by cert<span class="emp3"><span>ify</span></span>ing or representing good manners yourself.
</p>
</div>

<div id='memhook-button-bar'>
<a href="exemplify#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/exemplify/memory_hooks">Use other hook</a>
<a href="exemplify#" id="memhook-use-own" url="https://membean.com/mywords/exemplify/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='exemplify#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
“There are three qualities a leader must <b>exemplify</b> to build trust: competence, connection, and character.”
<span class='attribution'>&mdash; John C. Maxwell—American  Author</span>
<img alt="John c" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/John C. Maxwell—American  Author.jpg?qdep8" width="80" />
</li>
<li>
Rockman’s three monumental paintings — Evolution, Manifest Destiny, and South — <b>exemplify</b> the boundless imagination and extraordinary skill that go into every painting.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
The challenges Feeding America faces today lay bare the soft underbelly of the US food system and <b>exemplify</b> the agility our current food system lacks to meet the challenges of the future.
<cite class='attribution'>
&mdash;
Forbes
</cite>
</li>
<li>
The award honors jockeys "who <b>exemplify</b> extraordinary sportsmanship and citizenship," in the memory of Mike Venezia, who was killed in a spill at Belmont Park on Oct. 13, 1988.
<cite class='attribution'>
&mdash;
ESPN
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/exemplify/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='exemplify#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='exempl_example' data-tree-url='//cdn1.membean.com/public/data/treexml' href='exemplify#'>
<span class=''></span>
exempl
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>example</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ify_make' data-tree-url='//cdn1.membean.com/public/data/treexml' href='exemplify#'>
<span class=''></span>
-ify
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make, cause to become</td>
</tr>
</table>
<p>When one <em>exemplifies</em> proper conduct, one &#8220;becomes an example&#8221; of that conduct.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='exemplify#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Glee</strong><span> One student who exemplifies the best of the New York Academy of Dramatic Arts.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/exemplify.jpg' video_url='examplevids/exemplify' video_width='350'></span>
<div id='wt-container'>
<img alt="Exemplify" height="288" src="https://cdn1.membean.com/video/examplevids/exemplify.jpg" width="350" />
<div class='center'>
<a href="exemplify#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='exemplify#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Exemplify" src="https://cdn1.membean.com/public/images/wordimages/cons2/exemplify.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='exemplify#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>apotheosis</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>archetype</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bravura</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>definitive</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>edification</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>eloquent</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>elucidate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>embodiment</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>emulate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>epitome</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>exemplary</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>explicate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>expound</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>impeccable</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>inimitable</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>lucid</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>luminary</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>meritorious</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>nonpareil</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>paradigm</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>paragon</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>pedagogy</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>personify</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>ambiguous</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>confound</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>enigmatic</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>equivocate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>ineffectual</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>mediocre</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>nebulous</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>obfuscate</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>opaque</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>prosaic</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="exemplify" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>exemplify</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="exemplify#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

