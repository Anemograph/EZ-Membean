
<!DOCTYPE html>
<html>
<head>
<title>Word: tether | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Tether-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/tether-large.jpg?qdep8" />
</div>
<a href="tether#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>If you alleviate pain or suffering, you make it less intense or severe.</p>
<p class='rw-defn idx1' style='display:none'>Ambulatory activities involve walking or moving around.</p>
<p class='rw-defn idx2' style='display:none'>When you ameliorate a bad condition or situation, you make it better in some way.</p>
<p class='rw-defn idx3' style='display:none'>When you cavort, you jump and dance around in a playful, excited, or physically lively way.</p>
<p class='rw-defn idx4' style='display:none'>If one&#8217;s powers or rights are circumscribed, they are limited or restricted.</p>
<p class='rw-defn idx5' style='display:none'>If you concatenate two or more things, you join them together by linking them one after the other.</p>
<p class='rw-defn idx6' style='display:none'>If you delineate something, such as an idea or situation or border, you describe it in great detail.</p>
<p class='rw-defn idx7' style='display:none'>If a fact or idea eludes you, you cannot remember or understand it; if you elude someone, you manage to escape or hide from them.</p>
<p class='rw-defn idx8' style='display:none'>If something encumbers you, it makes it difficult for you to move freely or do what you want.</p>
<p class='rw-defn idx9' style='display:none'>When someone is exonerated from guilt, a task, or a responsibility, they are set free or excused from it.</p>
<p class='rw-defn idx10' style='display:none'>When you extricate yourself from a difficult or unpleasant situation, you manage to escape it.</p>
<p class='rw-defn idx11' style='display:none'>If something fetters you, it restricts you from doing something you want to do.</p>
<p class='rw-defn idx12' style='display:none'>If someone gambols, they run, jump, and play like a young child or animal.</p>
<p class='rw-defn idx13' style='display:none'>If you incarcerate someone, you put them in prison or jail.</p>
<p class='rw-defn idx14' style='display:none'>When someone is described as itinerant, they are characterized by traveling or moving about from place to place.</p>
<p class='rw-defn idx15' style='display:none'>A manacle is a circular, usually metallic device used to chain someone&#8217;s wrists and/or ankles together.</p>
<p class='rw-defn idx16' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx17' style='display:none'>If you mollify someone, you say or do something to make that person feel less angry or upset.</p>
<p class='rw-defn idx18' style='display:none'>A nexus is a connection or a series of connections between a number of people, things, or ideas that often form the center of a system or situation.</p>
<p class='rw-defn idx19' style='display:none'>To obviate a problem is to eliminate it or do something that makes solving the problem unnecessary.</p>
<p class='rw-defn idx20' style='display:none'>A task or responsibility is onerous if you dislike having to do it because it is difficult or tiring.</p>
<p class='rw-defn idx21' style='display:none'>If someone leads a peripatetic life, they travel from place to place, living and working only for a short time in each place before moving on.</p>
<p class='rw-defn idx22' style='display:none'>If someone is <em>pilloried</em>, they are publicly criticized or ridiculed by a lot of people, especially in the media.</p>
<p class='rw-defn idx23' style='display:none'>Retention is the act or condition of keeping or holding on to something, including the ability to remember things.</p>
<p class='rw-defn idx24' style='display:none'>A stricture is a rule or condition that imposes restrictions or limits on what you can do.</p>
<p class='rw-defn idx25' style='display:none'>When you trammel something or someone, you bring about limits to freedom or hinder movements or activities in some fashion.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>tether</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='tether#' id='pronounce-sound' path='audio/words/amy-tether'></a>
TETH-er
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='tether#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='tether#' id='context-sound' path='audio/wordcontexts/brian-tether'></a>
The dog was tied to the tree with a <em>tether</em> or leash.  Because the <em>tether</em> or rope was made of fabric, she easily chewed through it and got loose.  Her owner got her a new steel cable <em>tether</em> so that the dog could not break free again.  She has been securely <em>tethered</em> or fastened ever since.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>tether</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A signed agreement that requires you to pay back a loan. 
</li>
<li class='choice '>
<span class='result'></span>
An instrument used to shave off a beard or mustache.
</li>
<li class='choice answer '>
<span class='result'></span>
A leash that keeps your dog from running away.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='tether#' id='definition-sound' path='audio/wordmeanings/amy-tether'></a>
A <em>tether</em> is a restraint, such as a leash, rope, or chain, that holds something in place.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>fastening</em>
</span>
</span>
</div>
<a class='quick-help' href='tether#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='tether#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/tether/memory_hooks/4365.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Tether</span></span>ed <span class="emp2"><span>T</span></span>og<span class="emp2"><span>ether</span></span> with H<span class="emp2"><span>eather</span></span></span></span> Boy would I ever like to be <span class="emp2"><span>tether</span></span>ed <span class="emp2"><span>t</span></span>og<span class="emp2"><span>ether</span></span> with H<span class="emp2"><span>eather</span></span>--she's so gorgeous!
</p>
</div>

<div id='memhook-button-bar'>
<a href="tether#" id="add-public-hook" style="" url="https://membean.com/mywords/tether/memory_hooks">Use other public hook</a>
<a href="tether#" id="memhook-use-own" url="https://membean.com/mywords/tether/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='tether#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
I admire idealists, but they're usually enabled by someone who holds the <b>tether</b> on their balloon, who pays the bills and sweeps up after them.
<span class='attribution'>&mdash; Geraldine Brooks, Australian-American journalist and novelist</span>
<img alt="Geraldine brooks, australian-american journalist and novelist" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Geraldine Brooks, Australian-American journalist and novelist.jpg?qdep8" width="80" />
</li>
<li>
The Makani kite hangs from the tower. When wind conditions are right, the propellers buzz to life and the wing lifts off, a <b>tether</b> unspooling behind it, and helicopters to an altitude of 400 meters. There it catches the crosswind and commences to fly in giant circles.
<cite class='attribution'>
&mdash;
Forbes 
</cite>
</li>
<li>
Then you go out through that back puka, around the back of the navigator’s platform, and onto the catwalk on the outside edge of the hull. Here you clip a <b>tether</b> from your harness onto the safety rope that runs all the way around the outside of the canoe. If you fall off, at least you will be dragged along rather than left behind.
<cite class='attribution'>
&mdash;
 Smithsonian Magazine
</cite>
</li>
<li>
A <b>tether</b> holding a half-ton satellite to the space shuttle Columbia broke Sunday, hurling the multi-million dollar device into space. . . . An earlier try in 1992 failed when the <b>tether</b> got caught on a safety bolt in the reel after extending to a length of only about 800 feet.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/tether/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='tether#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>From a root word meaning &#8220;fastening device,&#8221; such as a rope or chain.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='tether#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Tether" src="https://cdn3.membean.com/public/images/wordimages/cons2/tether.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='tether#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='4' class = 'rw-wordform notlearned'><span>circumscribe</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>concatenate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>delineate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>encumber</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>fetter</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>incarcerate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>manacle</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>nexus</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>onerous</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>pillory</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>retention</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>stricture</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>trammel</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>alleviate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>ambulatory</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>ameliorate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>cavort</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>elude</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>exonerate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>extricate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>gambol</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>itinerant</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>mollify</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>obviate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>peripatetic</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='tether#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
tether
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to tie to</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="tether" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>tether</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="tether#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

