
<!DOCTYPE html>
<html>
<head>
<title>Word: transfix | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Abstinence is the practice of keeping away from or avoiding something you enjoy—such as the physical pleasures of excessive food and drink—usually for health or religious reasons.</p>
<p class='rw-defn idx1' style='display:none'>To accentuate something is to emphasize it or make it more noticeable.</p>
<p class='rw-defn idx2' style='display:none'>Something that happens unexpectedly and by chance that is not inherent or natural to a given item or situation is adventitious, such as a root appearing in an unusual place on a plant.</p>
<p class='rw-defn idx3' style='display:none'>You allay someone&#8217;s fear or concern by making them feel less afraid or worried.</p>
<p class='rw-defn idx4' style='display:none'>If you alleviate pain or suffering, you make it less intense or severe.</p>
<p class='rw-defn idx5' style='display:none'>Antipathy is a strong feeling of dislike or hostility towards someone or something.</p>
<p class='rw-defn idx6' style='display:none'>If someone beguiles you, you are charmed by and attracted to them; you can also be tricked or deceived by someone who beguiles you.</p>
<p class='rw-defn idx7' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx8' style='display:none'>When you are captivated by someone or something, you are enchanted, fascinated, or delighted by them or it.</p>
<p class='rw-defn idx9' style='display:none'>A person who is comely is attractive; this adjective is usually used with females, but not always.</p>
<p class='rw-defn idx10' style='display:none'>Someone does something in a disinterested way when they have no personal involvement or attachment to the action.</p>
<p class='rw-defn idx11' style='display:none'>If something enthralls you, it makes you so interested and excited that you give it all your attention.</p>
<p class='rw-defn idx12' style='display:none'>An epiphany is the moment when someone suddenly realizes or understands something of great significance or importance.</p>
<p class='rw-defn idx13' style='display:none'>If you eschew something, you deliberately avoid doing it, especially for moral reasons.</p>
<p class='rw-defn idx14' style='display:none'>When you galvanize someone, you cause them to improve a situation, solve a problem, or take some other action by making them feel excited, afraid, or angry.</p>
<p class='rw-defn idx15' style='display:none'>Something that is inconspicuous does not attract attention and is not easily seen or noticed because it is small or ordinary.</p>
<p class='rw-defn idx16' style='display:none'>Insouciance is a lack of concern or worry for something that should be shown more careful attention or consideration.</p>
<p class='rw-defn idx17' style='display:none'>If something mesmerizes you, it attracts or holds your interest so much that you do not pay attention to anything else.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx19' style='display:none'>Something is nondescript when its appearance is ordinary, dull, and not at all interesting or attractive.</p>
<p class='rw-defn idx20' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx21' style='display:none'>To obviate a problem is to eliminate it or do something that makes solving the problem unnecessary.</p>
<p class='rw-defn idx22' style='display:none'>If you describe people or things as odious, you think that they are extremely unpleasant.</p>
<p class='rw-defn idx23' style='display:none'>Something that is piquant is interesting and exciting; food that is piquant is pleasantly spicy.</p>
<p class='rw-defn idx24' style='display:none'>To quell something is to stamp out, quiet, or overcome it.</p>
<p class='rw-defn idx25' style='display:none'>If you refrain from doing something, you do not do it—even though you want to.</p>
<p class='rw-defn idx26' style='display:none'>Repugnance is a strong feeling of dislike for something or someone you find horrible and offensive.</p>
<p class='rw-defn idx27' style='display:none'>If something is reviled, it is intensely hated and criticized.</p>
<p class='rw-defn idx28' style='display:none'>A scintillating conversation, speech, or performance is brilliantly clever, interesting, and lively.</p>
<p class='rw-defn idx29' style='display:none'>Stupor is a state in which someone&#8217;s mind and senses are dulled; consequently, they are unable to think clearly or act normally, usually due to the use of alcohol or drugs.</p>
<p class='rw-defn idx30' style='display:none'>Something that is tantalizing is desirable and exciting; nevertheless, it may be out of reach, perhaps due to being too expensive.</p>
<p class='rw-defn idx31' style='display:none'>When something whets your appetite, it increases your desire for it, especially by giving you a little idea or pleasing hint of what it is like.</p>
<p class='rw-defn idx32' style='display:none'>Someone who is winsome is attractive and charming.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>transfix</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='transfix#' id='pronounce-sound' path='audio/words/amy-transfix'></a>
trans-FIKS
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='transfix#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='transfix#' id='context-sound' path='audio/wordcontexts/brian-transfix'></a>
The enormous, high-definition movie screen <em>transfixed</em> Robert&#8217;s gaze&#8212;he could look nowhere else. Robert remained completely <em>transfixed</em> or held by the colorful images moving across the television. Robert was so <em>transfixed</em> or extremely interested in his huge screen that he did not hear his mother telling him that it was time for dinner. His mother decided to pour a bucket of ice water on Robert&#8217;s head to get his attention&#8212;now Robert is <em>transfixed</em> by or completely involved in the tasty pizza he&#8217;s having for dinner!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean to be <em>transfixed</em> by something?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
To be annoyed by it.
</li>
<li class='choice '>
<span class='result'></span>
To remain ignorant of it.
</li>
<li class='choice answer '>
<span class='result'></span>
To be completely drawn to it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='transfix#' id='definition-sound' path='audio/wordmeanings/amy-transfix'></a>
If something <em>transfixes</em> you, it surprises, interests, or shocks you so much that you are unable to move or think of anything else.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>fascinate</em>
</span>
</span>
</div>
<a class='quick-help' href='transfix#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='transfix#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/transfix/memory_hooks/4634.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Train</span></span> <span class="emp1"><span>Fix</span></span>ed Him</span></span> The villain was tied to the <span class="emp3"><span>train</span></span> tracks so that when the <span class="emp3"><span>train</span></span> rolled over his body, he remained <span class="emp3"><span>tran</span></span>s<span class="emp1"><span>fix</span></span>ed for good--problem <span class="emp1"><span>fix</span></span>ed.
</p>
</div>

<div id='memhook-button-bar'>
<a href="transfix#" id="add-public-hook" style="" url="https://membean.com/mywords/transfix/memory_hooks">Use other public hook</a>
<a href="transfix#" id="memhook-use-own" url="https://membean.com/mywords/transfix/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='transfix#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Norris’s allegations, which spilled forth in a packed Washington courtroom, appeared to <b>transfix</b> U.S. District Judge Thomas Penfield Jackson, who studied documents carefully as they were introduced, piped in with questions and scribbled copious notes.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
[A scandal] must shock and appall and <b>transfix</b> on a regular basis, tumbling in time with the news cycle.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
The fifth _Transformers_ movie has failed to <b>transfix</b> American audiences.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/transfix/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='transfix#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='trans_across' data-tree-url='//cdn1.membean.com/public/data/treexml' href='transfix#'>
<span class='common'></span>
trans-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>across, through</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='fix_fastened' data-tree-url='//cdn1.membean.com/public/data/treexml' href='transfix#'>
<span class=''></span>
fix
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>fastened, nailed, secured</td>
</tr>
</table>
<p>When one remains <em>transfixed</em> by something interesting, it is as if one is &#8220;nailed through, fastened, or secured&#8221; by it.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='transfix#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Transfix" src="https://cdn0.membean.com/public/images/wordimages/cons2/transfix.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='transfix#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>accentuate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>beguile</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>captivate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>comely</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>enthrall</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>epiphany</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>galvanize</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>mesmerize</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>piquant</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>scintillating</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>stupor</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>tantalize</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>whet</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>winsome</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abstinence</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>adventitious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>allay</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>alleviate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>antipathy</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>disinterested</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>eschew</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>inconspicuous</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>insouciance</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>nondescript</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>obviate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>odious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>quell</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>refrain</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>repugnance</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>revile</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="transfix" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>transfix</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="transfix#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

