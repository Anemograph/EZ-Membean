
<!DOCTYPE html>
<html>
<head>
<title>Word: disaffected | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you acquiesce to something, you allow it to happen by doing what someone wants without putting up a struggle or voicing your own concerns.</p>
<p class='rw-defn idx1' style='display:none'>Your antagonist is an opponent in a competition or battle.</p>
<p class='rw-defn idx2' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx3' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx4' style='display:none'>Something is concomitant when it happens at the same time as something else and is connected with it in some fashion.</p>
<p class='rw-defn idx5' style='display:none'>Someone who is contumacious is purposely stubborn, contrary, or disobedient.</p>
<p class='rw-defn idx6' style='display:none'>A situation or thing that is discordant does not fit in with other things; therefore, it is disagreeable, strange, or unpleasant.</p>
<p class='rw-defn idx7' style='display:none'>Someone does something in a disinterested way when they have no personal involvement or attachment to the action.</p>
<p class='rw-defn idx8' style='display:none'>Things that are disparate are clearly different from each other and belong to different groups or classes.</p>
<p class='rw-defn idx9' style='display:none'>When someone feels disquiet about a situation, they feel very worried or nervous.</p>
<p class='rw-defn idx10' style='display:none'>Dissonance is an unpleasant situation of opposition in which ideas or actions are not in agreement or harmony; dissonance also refers to a harsh combination of sounds.</p>
<p class='rw-defn idx11' style='display:none'>A small group or issue that is factious is controversial and promotes strong disagreement or dissatisfaction within a larger group.</p>
<p class='rw-defn idx12' style='display:none'>If you show fealty to a queen, you show your loyalty to her.</p>
<p class='rw-defn idx13' style='display:none'>Fidelity towards something, such as a marriage or promise, is faithfulness or loyalty towards it.</p>
<p class='rw-defn idx14' style='display:none'>The word homogeneous is used to describe a group that has members or parts which are similar or are all of the same type.</p>
<p class='rw-defn idx15' style='display:none'>If someone is implacable, they very stubbornly react to situations or the opinions of others because of strong feelings that make them unwilling to change their mind.</p>
<p class='rw-defn idx16' style='display:none'>One thing that is incommensurate with another is different in its level, size, or quality from the second; this may lead to an unfair situation.</p>
<p class='rw-defn idx17' style='display:none'>Something that is incongruous is strange when considered along with its group because it seems very different from the other things in its group.</p>
<p class='rw-defn idx18' style='display:none'>Two irreconcilable opinions or points of view are so opposed to each other that it is not possible to accept both of them or reach a settlement between them.</p>
<p class='rw-defn idx19' style='display:none'>A person&#8217;s morale is their current state of self-confidence, how they feel emotionally, and how motivated they are to complete tasks.</p>
<p class='rw-defn idx20' style='display:none'>A person who is plastic can be easily influenced and molded by others.</p>
<p class='rw-defn idx21' style='display:none'>Rapprochement is the development of greater understanding and friendliness between two countries or groups of people after a period of unfriendly relations.</p>
<p class='rw-defn idx22' style='display:none'>If you are sanguine about a situation, especially a difficult one, you are confident and cheerful that everything will work out the way you want it to.</p>
<p class='rw-defn idx23' style='display:none'>A schism causes a group or organization to divide into two groups as a result of differences in their aims and beliefs.</p>
<p class='rw-defn idx24' style='display:none'>Sedition is the act of encouraging people to disobey and oppose the government currently in power.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>disaffected</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='disaffected#' id='pronounce-sound' path='audio/words/amy-disaffected'></a>
dis-uh-FEK-tid
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='disaffected#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='disaffected#' id='context-sound' path='audio/wordcontexts/brian-disaffected'></a>
Selma has been a member of the garden club for twenty years, but recently has become a discontented, <em>disaffected</em> participant and so has not attended any meetings this spring.  Selma&#8217;s sharp dissatisfaction began when Daisy bragged about her prize roses when Selma was talking about her petunias; from that moment, Selma considered herself a <em>disaffected</em> and detached member of the club.  Why should she remain loyal to a gathering that tolerated rude behavior?  Anyhow, the <em>disaffected</em> and alienated lady thought, her petunias were better than Daisy&#8217;s roses any day.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Why would someone be <em>disaffected</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When they&#8217;ve grown so angry that they shut down emotionally.
</li>
<li class='choice answer '>
<span class='result'></span>
When they&#8217;ve grown upset with the people in power and no longer wish to support them.
</li>
<li class='choice '>
<span class='result'></span>
When they&#8217;ve done something that requires them to be removed from a group.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='disaffected#' id='definition-sound' path='audio/wordmeanings/amy-disaffected'></a>
A <em>disaffected</em> member of a group or organization is not satisfied with it; consequently, they feel little loyalty towards it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>alienated</em>
</span>
</span>
</div>
<a class='quick-help' href='disaffected#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='disaffected#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/disaffected/memory_hooks/2809.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Affected</span></span> by <span class="emp1"><span>Dis</span></span></span></span> After my best friend <span class="emp1"><span>dis</span></span>sed me by telling me that I just wasn't very smart, it <span class="emp3"><span>affected</span></span> my attitude so much that I became <span class="emp1"><span>dis</span></span><span class="emp3"><span>affected</span></span> towards her and no longer considered her my friend.
</p>
</div>

<div id='memhook-button-bar'>
<a href="disaffected#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/disaffected/memory_hooks">Use other hook</a>
<a href="disaffected#" id="memhook-use-own" url="https://membean.com/mywords/disaffected/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='disaffected#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
_The Phantom Tollbooth_ told the story of Milo, a young boy utterly bored and <b>disaffected</b> with the world around him until a mysterious and magical tollbooth shows up.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
The main challengers include a <b>disaffected</b> former leader of Mr Lee’s Grand National Party, Lee Hoi-chang, who has twice failed to win the presidency.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/disaffected/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='disaffected#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='dis_apart' data-tree-url='//cdn1.membean.com/public/data/treexml' href='disaffected#'>
<span class='common'></span>
dis-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>apart, not, away from</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='affect_influenced' data-tree-url='//cdn1.membean.com/public/data/treexml' href='disaffected#'>
<span class=''></span>
affect
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>influenced, strongly moved</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ed_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='disaffected#'>
<span class=''></span>
-ed
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>having a particular state</td>
</tr>
</table>
<p>When someone becomes <em>disaffected</em> about something she used to like, she is &#8220;not strongly moved&#8221; by it anymore; she becomes &#8220;strongly moved apart&#8221; from it or is &#8220;away from its influence.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='disaffected#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Disaffected" src="https://cdn1.membean.com/public/images/wordimages/cons2/disaffected.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='disaffected#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>antagonist</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>contumacious</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>discordant</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>disinterested</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>disparate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>disquiet</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>dissonance</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>factious</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>implacable</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>incommensurate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>incongruous</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>irreconcilable</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>schism</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>sedition</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>acquiesce</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>concomitant</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>fealty</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>fidelity</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>homogeneous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>morale</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>plastic</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>rapprochement</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>sanguine</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="disaffected" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>disaffected</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="disaffected#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

