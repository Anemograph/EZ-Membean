
<!DOCTYPE html>
<html>
<head>
<title>Word: remission | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>If something is in abeyance, it has been put on hold and is not proceeding or being used at the present time.</p>
<p class='rw-defn idx2' style='display:none'>If someone in authority abolishes a law or practice, they formally put an end to it.</p>
<p class='rw-defn idx3' style='display:none'>When you absolve someone, you publicly and formally say that they are not guilty or responsible for any wrongdoing.</p>
<p class='rw-defn idx4' style='display:none'>You allay someone&#8217;s fear or concern by making them feel less afraid or worried.</p>
<p class='rw-defn idx5' style='display:none'>If you alleviate pain or suffering, you make it less intense or severe.</p>
<p class='rw-defn idx6' style='display:none'>When you ameliorate a bad condition or situation, you make it better in some way.</p>
<p class='rw-defn idx7' style='display:none'>Atrophy is a process by which parts of the body, such as muscles and organs, lessen in size or weaken in strength due to internal nerve damage, poor nutrition, or aging.</p>
<p class='rw-defn idx8' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx9' style='display:none'>Attrition is the process of gradually decreasing the strength of something (such as an enemy force) by continually attacking it.</p>
<p class='rw-defn idx10' style='display:none'>To augment something is to increase its value or effectiveness by adding something to it.</p>
<p class='rw-defn idx11' style='display:none'>A contagion is a disease—or the transmission of that disease—that is easily spread from one person to another through touch or the air.</p>
<p class='rw-defn idx12' style='display:none'>A convalescent person spends time resting to gain health and strength after a serious illness or operation.</p>
<p class='rw-defn idx13' style='display:none'>A copious amount of something is a very large amount of it.</p>
<p class='rw-defn idx14' style='display:none'>If something, such as a sound, is undergoing a crescendo, it is getting louder and louder or increasing in intensity and is about to reach its peak strength.</p>
<p class='rw-defn idx15' style='display:none'>Something that has curative properties can be used for curing people&#8217;s illnesses.</p>
<p class='rw-defn idx16' style='display:none'>If you curtail something, you reduce or limit it.</p>
<p class='rw-defn idx17' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx18' style='display:none'>When an amount of something dwindles, it becomes less and less over time.</p>
<p class='rw-defn idx19' style='display:none'>If something exacerbates a problem or bad situation, it makes it even worse.</p>
<p class='rw-defn idx20' style='display:none'>When someone is exonerated from guilt, a task, or a responsibility, they are set free or excused from it.</p>
<p class='rw-defn idx21' style='display:none'>If you extirpate something, you completely destroy it because it is unpleasant or unwanted.</p>
<p class='rw-defn idx22' style='display:none'>A hiatus is a period of time when there is a break or interruption in some activity.</p>
<p class='rw-defn idx23' style='display:none'>Something that is latent, such as an undiscovered talent or undiagnosed disease, exists but is not active or has not developed at the moment—it may develop or become active in the future.</p>
<p class='rw-defn idx24' style='display:none'>A malignant thing or person, such as a tumor or criminal, is deadly or does great harm.</p>
<p class='rw-defn idx25' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx26' style='display:none'>If you mollify someone, you say or do something to make that person feel less angry or upset.</p>
<p class='rw-defn idx27' style='display:none'>If something is obsolescent, it is slowly becoming no longer needed because something newer or more effective has been invented.</p>
<p class='rw-defn idx28' style='display:none'>If you have a pathological condition, you are extreme or unreasonable in something that you do.</p>
<p class='rw-defn idx29' style='display:none'>To quell something is to stamp out, quiet, or overcome it.</p>
<p class='rw-defn idx30' style='display:none'>A state of quiescence is one of quiet and restful inaction.</p>
<p class='rw-defn idx31' style='display:none'>Regression is the falling back or return to a previous state.</p>
<p class='rw-defn idx32' style='display:none'>When someone is rehabilitated, they are restored to a more normal way of life, such as returning to good health or a crime-free life.</p>
<p class='rw-defn idx33' style='display:none'>A reprieve is a temporary relief from or cancellation of a punishment; it can also be a relief from something, such as pain or trouble of some kind.</p>
<p class='rw-defn idx34' style='display:none'>A respite is a short period of rest from work or something troubling.</p>
<p class='rw-defn idx35' style='display:none'>When something subsides, it begins to go away, lessen, or decrease in some way.</p>
<p class='rw-defn idx36' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx37' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>remission</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='remission#' id='pronounce-sound' path='audio/words/amy-remission'></a>
re-MISH-uhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='remission#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='remission#' id='context-sound' path='audio/wordcontexts/brian-remission'></a>
My disease, after a long time, finally went into <em>remission</em> or began to decrease in intensity.  I could hardly believe that it was in <em>remission</em> or was lessening, as it had been with me for such a long time.  Hopefully it will remain in <em>remission</em> or in a state of reduction to the point where it will no longer bother me at all.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of something that&#8217;s in <em>remission</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Actors in a play that stop for a short break between the two acts.
</li>
<li class='choice '>
<span class='result'></span>
A house that has just been sold and is therefore taken off the market.
</li>
<li class='choice answer '>
<span class='result'></span>
A tumor that&#8217;s decreasing in size with the help of medicine.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='remission#' id='definition-sound' path='audio/wordmeanings/amy-remission'></a>
When something, such as a disease or storm, is in <em>remission</em>, its force is lessening or decreasing.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>decrease</em>
</span>
</span>
</div>
<a class='quick-help' href='remission#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='remission#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/remission/memory_hooks/5650.json'></span>
<p>
<span class="emp0"><span>P<span class="emp3"><span>ermission</span></span> to Live</span></span> When my cancer went into <span class="emp3"><span>remission</span></span> it gave me p<span class="emp3"><span>ermission</span></span> to live.
</p>
</div>

<div id='memhook-button-bar'>
<a href="remission#" id="add-public-hook" style="" url="https://membean.com/mywords/remission/memory_hooks">Use other public hook</a>
<a href="remission#" id="memhook-use-own" url="https://membean.com/mywords/remission/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='remission#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Rheumatoid arthritis affects everyone differently. For some, joint symptoms happen gradually over several years. In others, it may come on quickly. Some people may have rheumatoid arthritis for a short time and then go into <b>remission</b>, which means they don’t have symptoms.
<cite class='attribution'>
&mdash;
WebMD
</cite>
</li>
<li>
Hayward, the top signee in 49ers' basketball history, missed his freshman season in 1997–98 because of the disease and chemotherapy. Once in <b>remission</b>, he rebuilt his strength and rejoined the team for the beginning of the 1998–99 season.
<cite class='attribution'>
&mdash;
New York Times
</cite>
</li>
<li>
“The US has been down this road before with Al Qaeda [in Yemen], where they essentially bombed it into submission, arrested all the individuals, and by the end of November 2003 thought they’d destroyed the organization,” Johnsen says. “Of course, the organization didn’t go away, it just went into <b>remission</b> and then a couple years later came back. If the United States continues to rely solely on military strikes on Al Qaeda in Yemen then every few years it will be fighting a different incarnation of Al Qaeda.”
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/remission/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='remission#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='re_back' data-tree-url='//cdn1.membean.com/public/data/treexml' href='remission#'>
<span class='common'></span>
re-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>back, again</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='miss_sent' data-tree-url='//cdn1.membean.com/public/data/treexml' href='remission#'>
<span class='common'></span>
miss
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>sent</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ion_act' data-tree-url='//cdn1.membean.com/public/data/treexml' href='remission#'>
<span class=''></span>
-ion
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>act, state, or result of doing something</td>
</tr>
</table>
<p>A tumor in <em>remission</em> has been &#8220;sent back.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='remission#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Breaking Bad</strong><span> His cancer is in remission.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/remission.jpg' video_url='examplevids/remission' video_width='350'></span>
<div id='wt-container'>
<img alt="Remission" height="288" src="https://cdn1.membean.com/video/examplevids/remission.jpg" width="350" />
<div class='center'>
<a href="remission#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='remission#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Remission" src="https://cdn0.membean.com/public/images/wordimages/cons2/remission.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='remission#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abeyance</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abolish</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>absolve</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>allay</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>alleviate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>ameliorate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>attrition</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>convalescent</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>curative</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>curtail</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>dwindle</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>exonerate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>extirpate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>hiatus</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>latent</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>mollify</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>obsolescent</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>quell</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>quiescence</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>rehabilitate</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>reprieve</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>respite</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>subside</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='7' class = 'rw-wordform notlearned'><span>atrophy</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>augment</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>contagion</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>copious</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>crescendo</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>exacerbate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>malignant</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>pathological</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>regression</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='remission#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
remit
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to give money as payment</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="remission" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>remission</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="remission#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

