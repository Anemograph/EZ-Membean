
<!DOCTYPE html>
<html>
<head>
<title>Word: reprimand | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx1' style='display:none'>When you absolve someone, you publicly and formally say that they are not guilty or responsible for any wrongdoing.</p>
<p class='rw-defn idx2' style='display:none'>When you admonish someone, you tell them gently but with seriousness that they have done something wrong; you usually caution and advise them not to do it again.</p>
<p class='rw-defn idx3' style='display:none'>Approbation is official praise or approval of something.</p>
<p class='rw-defn idx4' style='display:none'>When you castigate someone, you criticize or punish them severely.</p>
<p class='rw-defn idx5' style='display:none'>Censure is written or verbal disapproval and harsh criticism of something that someone has done.</p>
<p class='rw-defn idx6' style='display:none'>If you chastise someone, you speak to them angrily or punish them for doing something wrong.</p>
<p class='rw-defn idx7' style='display:none'>If someone is granted clemency, they are punished less severely than they could have been—they have been shown mercy.</p>
<p class='rw-defn idx8' style='display:none'>A condemnation of someone is strong disapproval for what they have done; it can result in a conviction of wrongdoing in court.</p>
<p class='rw-defn idx9' style='display:none'>A contemptible act is shameful, disgraceful, and worthy of scorn.</p>
<p class='rw-defn idx10' style='display:none'>If you are culpable for an action, you are held responsible for something wrong or bad that has happened.</p>
<p class='rw-defn idx11' style='display:none'>To decry something is to speak against it and find fault with it.</p>
<p class='rw-defn idx12' style='display:none'>A degenerate person is immoral, wicked, or corrupt.</p>
<p class='rw-defn idx13' style='display:none'>If you denounce people or actions, you criticize them severely in public because you feel strongly that they are wrong or evil.</p>
<p class='rw-defn idx14' style='display:none'>If you deprecate something, you disapprove of it strongly.</p>
<p class='rw-defn idx15' style='display:none'>If you say a person&#8217;s actions are despicable, you think they are extremely unpleasant or nasty.</p>
<p class='rw-defn idx16' style='display:none'>An egregious mistake, failure, or problem is an extremely bad and very noticeable one.</p>
<p class='rw-defn idx17' style='display:none'>An encomium strongly praises someone or something via oral or written communication.</p>
<p class='rw-defn idx18' style='display:none'>Errant things or people behave in a way that is unacceptable or wrong, such as a missile that travels in the wrong direction or a student who does not follow classroom rules.</p>
<p class='rw-defn idx19' style='display:none'>A eulogy is a speech or other piece of writing, often part of a funeral, in which someone or something is highly praised.</p>
<p class='rw-defn idx20' style='display:none'>If you excoriate someone, you express very strong disapproval of something they did.</p>
<p class='rw-defn idx21' style='display:none'>If you exculpate someone, you prove that they are not guilty of a crime.</p>
<p class='rw-defn idx22' style='display:none'>An exemplary person is one that sets an ideal or praiseworthy example for others to follow.</p>
<p class='rw-defn idx23' style='display:none'>When someone is exonerated from guilt, a task, or a responsibility, they are set free or excused from it.</p>
<p class='rw-defn idx24' style='display:none'>If you expostulate with someone, you express strong disagreement with or disapproval of what that person is doing.</p>
<p class='rw-defn idx25' style='display:none'>If you extol something or someone, you praise it or them very enthusiastically.</p>
<p class='rw-defn idx26' style='display:none'>An action that is flagrant shows that someone does not care if they obviously break the rules or highly offend people.</p>
<p class='rw-defn idx27' style='display:none'>A harangue is a long, scolding speech.</p>
<p class='rw-defn idx28' style='display:none'>If you describe someone&#8217;s appearance or behavior as impeccable, you mean that it is perfect and therefore impossible to criticize.</p>
<p class='rw-defn idx29' style='display:none'>When you act in an imprudent fashion, you do something that is unwise, is lacking in good judgment, or has no forethought.</p>
<p class='rw-defn idx30' style='display:none'>If you impute something, such as blame or a crime, to somebody, you say (usually unfairly) that that person is responsible for it.</p>
<p class='rw-defn idx31' style='display:none'>If something is infallible, it is never wrong and so is incapable of making mistakes.</p>
<p class='rw-defn idx32' style='display:none'>Iniquity is an immoral act, wickedness, or evil.</p>
<p class='rw-defn idx33' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx34' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx35' style='display:none'>A laudatory article or speech expresses praise or admiration for someone or something.</p>
<p class='rw-defn idx36' style='display:none'>Malfeasance is an unlawful act, especially one committed by a trusted public official.</p>
<p class='rw-defn idx37' style='display:none'>If someone is <em>pilloried</em>, they are publicly criticized or ridiculed by a lot of people, especially in the media.</p>
<p class='rw-defn idx38' style='display:none'>If you receive a plaudit, you receive admiration, praise, and approval from someone.</p>
<p class='rw-defn idx39' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx40' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx41' style='display:none'>A rapscallion is a young rascal, naughty child, or an older person who is dishonest and a scoundrel.</p>
<p class='rw-defn idx42' style='display:none'>When you rebuke someone, you harshly scold or criticize them for something they&#8217;ve done.</p>
<p class='rw-defn idx43' style='display:none'>You show rectitude if you behave or conduct yourself in an honest and morally correct manner.</p>
<p class='rw-defn idx44' style='display:none'>To remonstrate with someone is to tell that person that you strongly disapprove of something they have said or done.</p>
<p class='rw-defn idx45' style='display:none'>If you think a type of behavior or idea is reprehensible, you think that it is very bad, morally wrong, and deserves to be strongly criticized.</p>
<p class='rw-defn idx46' style='display:none'>A reprobate has a bad character and behaves in an immoral or improper way.</p>
<p class='rw-defn idx47' style='display:none'>When you validate something, you confirm that it is sound, true, legal, or worthwhile.</p>
<p class='rw-defn idx48' style='display:none'>If a person is vindicated, their ideas, decisions, or actions—once considered wrong—are proved correct or not to be blamed.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>reprimand</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='reprimand#' id='pronounce-sound' path='audio/words/amy-reprimand'></a>
REP-ruh-mand
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='reprimand#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='reprimand#' id='context-sound' path='audio/wordcontexts/brian-reprimand'></a>
When I got stopped by the policeman, I was happy to just get a <em>reprimand</em> or verbal correction for my speeding.  If I had gotten a ticket, I might have needed to go to driving school besides paying the high fine, so merely getting a <em>reprimand</em> or scolding was a relief.  You can believe that I was very attentive and obedient as the officer <em>reprimanded</em> or criticized me, and I heartily promised I would never speed again.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How would you likely feel about being given a <em>reprimand</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
You would feel proud to have been praised for doing such good work.
</li>
<li class='choice '>
<span class='result'></span>
You would feel relieved that you have completed the task you were assigned.
</li>
<li class='choice answer '>
<span class='result'></span>
You would feel embarrassed at having been told you did something wrong.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='reprimand#' id='definition-sound' path='audio/wordmeanings/amy-reprimand'></a>
When you are given a <em>reprimand</em>, you are scolded, blamed, or given a talking-to by someone for something wrong that you did.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>scolding</em>
</span>
</span>
</div>
<a class='quick-help' href='reprimand#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='reprimand#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/reprimand/memory_hooks/19830.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Rep</span></span>o <span class="emp2"><span>Man</span></span> <span class="emp1"><span>Rep</span></span>ri<span class="emp2"><span>man</span></span>d</span></span> The <span class="emp1"><span>rep</span></span>o <span class="emp2"><span>man</span></span> took away my car; what a <span class="emp1"><span>rep</span></span>ri<span class="emp2"><span>man</span></span>d that was!
</p>
</div>

<div id='memhook-button-bar'>
<a href="reprimand#" id="add-public-hook" style="" url="https://membean.com/mywords/reprimand/memory_hooks">Use other public hook</a>
<a href="reprimand#" id="memhook-use-own" url="https://membean.com/mywords/reprimand/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='reprimand#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Barber said a <b>reprimand</b> would simply lead to a mark on Hicks’ record. He, however, said further violations could in fact “result in further <b>reprimand</b>, censure or even expulsion from council.”
<cite class='attribution'>
&mdash;
The Roanoke Times
</cite>
</li>
<li>
Two Air Force officers involved in a scuffle with a Blackwater contractor in Afghanistan have been cleared of charges, but given an official <b>reprimand</b>.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/reprimand/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='reprimand#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='re_back' data-tree-url='//cdn1.membean.com/public/data/treexml' href='reprimand#'>
<span class='common'></span>
re-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>back, again</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='prim_press' data-tree-url='//cdn1.membean.com/public/data/treexml' href='reprimand#'>
<span class=''></span>
prim
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>press</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='and_to_be' data-tree-url='//cdn1.membean.com/public/data/treexml' href='reprimand#'>
<span class=''></span>
-and
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>that which is to be ...ed</td>
</tr>
</table>
<p>A <em>reprimand</em>  is in response to an act that should have been &#8220;pressed back&#8221; or &#8220;repressed&#8221; in the first place.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='reprimand#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Parks and Recreation</strong><span> Should she be reprimanded?</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/reprimand.jpg' video_url='examplevids/reprimand' video_width='350'></span>
<div id='wt-container'>
<img alt="Reprimand" height="288" src="https://cdn1.membean.com/video/examplevids/reprimand.jpg" width="350" />
<div class='center'>
<a href="reprimand#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='reprimand#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Reprimand" src="https://cdn1.membean.com/public/images/wordimages/cons2/reprimand.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='reprimand#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>admonish</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>castigate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>censure</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>chastise</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>condemnation</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>contemptible</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>culpable</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>decry</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>degenerate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>denounce</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>deprecate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>despicable</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>egregious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>errant</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>excoriate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>expostulate</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>flagrant</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>harangue</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>imprudent</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>impute</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>iniquity</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>malfeasance</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>pillory</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>rapscallion</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>rebuke</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>remonstrate</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>reprehensible</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>reprobate</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>absolve</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>approbation</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>clemency</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>encomium</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>eulogy</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>exculpate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>exemplary</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>exonerate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>extol</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>impeccable</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>infallible</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>laudatory</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>plaudit</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>rectitude</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>validate</span> &middot;</li><li data-idx='48' class = 'rw-wordform notlearned'><span>vindicate</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='reprimand#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
reprimand
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>scold, blame, criticize</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="reprimand" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>reprimand</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="reprimand#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

