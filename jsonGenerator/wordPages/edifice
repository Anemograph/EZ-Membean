
<!DOCTYPE html>
<html>
<head>
<title>Word: edifice | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Edifice-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/edifice-large.jpg?qdep8" />
</div>
<a href="edifice#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>To cloister someone is to remove and isolate them from the rest of the world.</p>
<p class='rw-defn idx1' style='display:none'>A denizen is a person who inhabits a particular place; a denizen can also be a person who visits a place often, such as a restaurant or library.</p>
<p class='rw-defn idx2' style='display:none'>Your domicile is the place where you live or your home.</p>
<p class='rw-defn idx3' style='display:none'>An enclave is a small area within a larger area of a city or country in which people of a different nationality or culture live.</p>
<p class='rw-defn idx4' style='display:none'>If you ensconce yourself somewhere, you put yourself into a comfortable place or safe position and have no intention of moving or leaving for quite some time.</p>
<p class='rw-defn idx5' style='display:none'>Errant things or people behave in a way that is unacceptable or wrong, such as a missile that travels in the wrong direction or a student who does not follow classroom rules.</p>
<p class='rw-defn idx6' style='display:none'>A fortification is a structure or building that is used in defense against an invading army.</p>
<p class='rw-defn idx7' style='display:none'>When someone is described as itinerant, they are characterized by traveling or moving about from place to place.</p>
<p class='rw-defn idx8' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx9' style='display:none'>If someone is ostracized from a group, its members deliberately refuse to talk or listen to them and do not allow them to take part in any of their social activities.</p>
<p class='rw-defn idx10' style='display:none'>A pariah is a social outcast.</p>
<p class='rw-defn idx11' style='display:none'>If someone leads a peripatetic life, they travel from place to place, living and working only for a short time in each place before moving on.</p>
<p class='rw-defn idx12' style='display:none'>When people or animals are quarantined, they are put in isolation so that they will not spread disease.</p>
<p class='rw-defn idx13' style='display:none'>If you sequester someone, you keep them separate from other people.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>edifice</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='edifice#' id='pronounce-sound' path='audio/words/amy-edifice'></a>
ED-uh-fis
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='edifice#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='edifice#' id='context-sound' path='audio/wordcontexts/brian-edifice'></a>
The palace, which has been converted into a museum, is a huge <em>edifice</em> that was built over the course of three centuries.  From the top towers of this massive <em>edifice</em> you can see one-hundred miles around on a clear day.  The entire monumental structure or <em>edifice</em> covers over three acres of ground and has about two-hundred rooms.  When I first saw this grand <em>edifice</em> or huge building I was so impressed that I stood staring at it for a long time, mouth open in wonder.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an <em>edifice</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is a row of identical houses that share common walls.
</li>
<li class='choice '>
<span class='result'></span>
It is a large sculpture that is often in a prominent place.
</li>
<li class='choice answer '>
<span class='result'></span>
It is a large building, such as a courthouse or cathedral.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='edifice#' id='definition-sound' path='audio/wordmeanings/amy-edifice'></a>
An <em>edifice</em> is a large or impressive building, such as a church, palace, temple, or fortress.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>large building</em>
</span>
</span>
</div>
<a class='quick-help' href='edifice#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='edifice#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/edifice/memory_hooks/3070.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>If</span></span> <span class="emp1"><span>Ed</span></span> Makes D<span class="emp3"><span>ice</span></span></span></span> <span class="emp2"><span>If</span></span> <span class="emp1"><span>Ed</span></span> makes his d<span class="emp3"><span>ice</span></span> roll double sixes, he'll win a life-sized replica of the Taj Mahal, one of the world's most stunning <span class="emp1"><span>ed</span></span><span class="emp2"><span>if</span></span><span class="emp3"><span>ice</span></span>s.
</p>
</div>

<div id='memhook-button-bar'>
<a href="edifice#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/edifice/memory_hooks">Use other hook</a>
<a href="edifice#" id="memhook-use-own" url="https://membean.com/mywords/edifice/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='edifice#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Rarely will you see a museum where exhibit design and building design are so intimately interwoven. Credit for this beautiful, costly <b>edifice</b> also goes to clients and patrons . . . who helped pay for the $450 million project.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
He says when tourists visit Mumbai, they marvel at its big <b>edifices—grand</b> Gothic architecture with gargoyles and sleek Art Deco buildings—but neglect the small structures that also make the city beautiful.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Schock-Werner has a professional versatility that cut her out for the challenging job of caring for the 753-year-old sandstone colossus . . . Reconciling the demands of an <b>edifice</b> that is simultaneously "a house of God, a historical monument and a major tourist attraction" is one of the most difficult challenges Schock-Werner faces.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
<li>
Many of the state’s cultural quirks are represented by the trash collection station here, a blue-roofed <b>edifice</b> that, like the topography of the state itself, seems to have been designed to maximize hardship.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/edifice/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='edifice#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ed_temple' data-tree-url='//cdn1.membean.com/public/data/treexml' href='edifice#'>
<span class=''></span>
ed
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>temple, house</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='i_connective' data-tree-url='//cdn1.membean.com/public/data/treexml' href='edifice#'>
<span class=''></span>
-i-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>connective</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='fic_make' data-tree-url='//cdn1.membean.com/public/data/treexml' href='edifice#'>
<span class='common'></span>
fic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make, do</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='edifice#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>An <em>edifice</em> is a &#8220;temple or house&#8221; that has been &#8220;made.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='edifice#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>The Idiot Abroad</strong><span> The Taj Mahal is the ultimate edifice.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/edifice.jpg' video_url='examplevids/edifice' video_width='350'></span>
<div id='wt-container'>
<img alt="Edifice" height="288" src="https://cdn1.membean.com/video/examplevids/edifice.jpg" width="350" />
<div class='center'>
<a href="edifice#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='edifice#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Edifice" src="https://cdn3.membean.com/public/images/wordimages/cons2/edifice.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='edifice#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>cloister</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>denizen</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>domicile</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>enclave</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>ensconce</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>fortification</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>quarantine</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>sequester</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='5' class = 'rw-wordform notlearned'><span>errant</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>itinerant</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>ostracize</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>pariah</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>peripatetic</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="edifice" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>edifice</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="edifice#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

