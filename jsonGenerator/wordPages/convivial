
<!DOCTYPE html>
<html>
<head>
<title>Word: convivial | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx1' style='display:none'>Amity is a peaceful friendship between two parties, particularly between two countries.</p>
<p class='rw-defn idx2' style='display:none'>When you claim that something is banal, you do not like it because you think it is ordinary, dull, commonplace, and boring.</p>
<p class='rw-defn idx3' style='display:none'>When you bandy about ideas with someone, you casually discuss them without caring if the discussion is informed or effective in any way.</p>
<p class='rw-defn idx4' style='display:none'>Banter is friendly conversation during which people make friendly jokes and laugh at—and with—one another.</p>
<p class='rw-defn idx5' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx6' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx7' style='display:none'>A captious person has a fondness for catching others at fault; hence, they are overly critical and raise unwarranted objections too often.</p>
<p class='rw-defn idx8' style='display:none'>To cavil is to make unnecessary complaints about things that are unimportant.</p>
<p class='rw-defn idx9' style='display:none'>When you cavort, you jump and dance around in a playful, excited, or physically lively way.</p>
<p class='rw-defn idx10' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx11' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx12' style='display:none'>If you are despondent, you are extremely unhappy because you are in an unpleasant situation that you do not think will improve.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is dour is serious, stubborn, and unfriendly; they can also be gloomy.</p>
<p class='rw-defn idx14' style='display:none'>Weather that is dreary tends to be depressing and gloomy; a situation or person that is dreary tends to be boring or uninteresting.</p>
<p class='rw-defn idx15' style='display:none'>A forlorn person is lonely because they have been abandoned; a forlorn home has been deserted.</p>
<p class='rw-defn idx16' style='display:none'>A gregarious person is friendly, highly social, and prefers being with people rather than being alone.</p>
<p class='rw-defn idx17' style='display:none'>An introvert is someone who primarily prefers being by themselves instead of hanging out with others socially; nevertheless, they still enjoy spending time with friends.</p>
<p class='rw-defn idx18' style='display:none'>If someone is lugubrious, they are looking very sad or gloomy.</p>
<p class='rw-defn idx19' style='display:none'>Someone who is morose is unhappy, bad-tempered, and unwilling to talk very much.</p>
<p class='rw-defn idx20' style='display:none'>A petulant person behaves in an unreasonable and childish way, especially because they cannot get their own way or what they want.</p>
<p class='rw-defn idx21' style='display:none'>Someone who is saturnine is looking miserable and sad, sometimes in a threatening or unfriendly way.</p>
<p class='rw-defn idx22' style='display:none'>If you are suffering from tedium, you are bored.</p>
<p class='rw-defn idx23' style='display:none'>A troglodyte is reclusive, severely lacking in social skills, and is out of step with current times.</p>
<p class='rw-defn idx24' style='display:none'>A truculent person is bad-tempered, easily annoyed, and prone to arguing excessively.</p>
<p class='rw-defn idx25' style='display:none'>Something vapid is dull, boring, and/or tiresome.</p>
<p class='rw-defn idx26' style='display:none'>If someone is described as vivacious, they are lively and have a happy, lighthearted manner.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>convivial</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='convivial#' id='pronounce-sound' path='audio/words/amy-convivial'></a>
kuhn-VIV-ee-uhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='convivial#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='convivial#' id='context-sound' path='audio/wordcontexts/brian-convivial'></a>
The joyous wedding brought together friends and family for a <em>convivial</em>, lively, and cheerful celebration.  After the ceremony, the bride and groom welcomed their guests into the reception hall with <em>convivial</em> hugs and happy invitations to dance, eat, and have fun.  Everyone had a good time at such a <em>convivial</em> and fun-loving event.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>convivial</em> event?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A funeral at which family members and friends are tearful and grieving.
</li>
<li class='choice '>
<span class='result'></span>
A large family picnic that gets cancelled due to bad weather.
</li>
<li class='choice answer '>
<span class='result'></span>
A baby shower at which the guests are happy for the new parents.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='convivial#' id='definition-sound' path='audio/wordmeanings/amy-convivial'></a>
A <em>convivial</em> atmosphere or occasion is friendly, pleasant, cheerful, and relaxed.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>cheerful</em>
</span>
</span>
</div>
<a class='quick-help' href='convivial#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='convivial#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/convivial/memory_hooks/5738.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Al</span></span>l Great <span class="emp2"><span>Vibe</span></span>s from <span class="emp2"><span>Vivi</span></span>an</span></span> <span class="emp2"><span>Vivi</span></span>an's party was so <span class="emp3"><span>con</span></span><span class="emp2"><span>vivi</span></span><span class="emp1"><span>al</span></span> that we <span class="emp3"><span>co</span></span>uld<span class="emp3"><span>n</span></span>'t leave; great "<span class="emp2"><span>vibes</span></span>" from <span class="emp2"><span>Vivi</span></span>an, or great "<span class="emp2"><span>viv</span></span>es" as we coined them, made <span class="emp2"><span>Vivi</span></span>an's parties the most fun and <span class="emp3"><span>con</span></span><span class="emp2"><span>vivi</span></span><span class="emp1"><span>al</span></span> of <span class="emp1"><span>al</span></span>l!
</p>
</div>

<div id='memhook-button-bar'>
<a href="convivial#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/convivial/memory_hooks">Use other hook</a>
<a href="convivial#" id="memhook-use-own" url="https://membean.com/mywords/convivial/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='convivial#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
But there's comfort in the unbending traditions that endure in China. Until now, I counted among them family mealtimes, the clatter of chopsticks over communal plates. . . . Scoffing down a burger at the wheel is a poor substitute, and I found it odd to imagine a nation of <b>convivial</b> diners surrendering their birthright.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
The two men traded angry, long-distance insults that signaled an abrupt end to the <b>convivial</b> Christmas messages that Republican hopefuls offered voters last week.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/convivial/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='convivial#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='con_with' data-tree-url='//cdn1.membean.com/public/data/treexml' href='convivial#'>
<span class='common'></span>
con-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>with, together</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='viv_live' data-tree-url='//cdn1.membean.com/public/data/treexml' href='convivial#'>
<span class=''></span>
viv
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>live</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='al_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='convivial#'>
<span class=''></span>
-al
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>When a party is <em>convivial</em>, it is easy and fun for people to &#8220;live together.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='convivial#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Harry Potter and the Sorcerer's Stone</strong><span> Quite the convivial atmosphere.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/convivial.jpg' video_url='examplevids/convivial' video_width='350'></span>
<div id='wt-container'>
<img alt="Convivial" height="288" src="https://cdn1.membean.com/video/examplevids/convivial.jpg" width="350" />
<div class='center'>
<a href="convivial#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='convivial#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Convivial" src="https://cdn3.membean.com/public/images/wordimages/cons2/convivial.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='convivial#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>amity</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bandy</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>banter</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>cavort</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>gregarious</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>vivacious</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>banal</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>captious</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>cavil</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>despondent</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>dour</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>dreary</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>forlorn</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>introvert</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>lugubrious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>morose</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>petulant</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>saturnine</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>tedium</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>troglodyte</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>truculent</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>vapid</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="convivial" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>convivial</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="convivial#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

