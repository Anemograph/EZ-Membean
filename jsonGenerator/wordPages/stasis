
<!DOCTYPE html>
<html>
<head>
<title>Word: stasis | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If something is in abeyance, it has been put on hold and is not proceeding or being used at the present time.</p>
<p class='rw-defn idx1' style='display:none'>Bedlam is a situation with a great deal of noise and confusion.</p>
<p class='rw-defn idx2' style='display:none'>Someone who is capricious changes their mind repeatedly or behaves in unexpected ways; a capricious series of events follows no predictable pattern.</p>
<p class='rw-defn idx3' style='display:none'>Something that is desultory is done in a way that is unplanned, disorganized, and without direction.</p>
<p class='rw-defn idx4' style='display:none'>Entropy is the lack of organization or measure of disorder currently in a system.</p>
<p class='rw-defn idx5' style='display:none'>Something ephemeral, such as some insects or a sunset, lasts for only a short time or has a very short lifespan.</p>
<p class='rw-defn idx6' style='display:none'>Erratic behavior is irregular, unpredictable, and unusual.</p>
<p class='rw-defn idx7' style='display:none'>A fleeting moment lasts but a short time before it fades away.</p>
<p class='rw-defn idx8' style='display:none'>Things that fluctuate vary or change often, rising or falling seemingly at random.</p>
<p class='rw-defn idx9' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx10' style='display:none'>An impending event is approaching fast or is about to occur; this word usually has a negative implication, referring to something threatening or harmful coming.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is impulsive tends to do things without thinking about them ahead of time; therefore, their actions can be unpredictable.</p>
<p class='rw-defn idx12' style='display:none'>Something inanimate is dead, motionless, or not living; therefore, it does not display the same traits as an active and alive organism.</p>
<p class='rw-defn idx13' style='display:none'>Something that is incessant continues on for a long time without stopping.</p>
<p class='rw-defn idx14' style='display:none'>When something is inert, it has no power of movement or is inactive.</p>
<p class='rw-defn idx15' style='display:none'>Something that happens on an intermittent basis happens in irregular intervals, stopping and starting at unpredictable times.</p>
<p class='rw-defn idx16' style='display:none'>When someone is described as itinerant, they are characterized by traveling or moving about from place to place.</p>
<p class='rw-defn idx17' style='display:none'>Something kinetic is moving, active, and using energy.</p>
<p class='rw-defn idx18' style='display:none'>A languid person is slow, relaxed, and shows little energy or interest in doing anything.</p>
<p class='rw-defn idx19' style='display:none'>Something that is latent, such as an undiscovered talent or undiagnosed disease, exists but is not active or has not developed at the moment—it may develop or become active in the future.</p>
<p class='rw-defn idx20' style='display:none'>A monochrome painting is comprised of different shades of only one color or is done in black and white.</p>
<p class='rw-defn idx21' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx22' style='display:none'>An ominous sign predicts or shows that something bad will happen in the near future.</p>
<p class='rw-defn idx23' style='display:none'>Pandemonium is a very noisy and uncontrolled situation, especially one that is caused by a lot of angry or excited people.</p>
<p class='rw-defn idx24' style='display:none'>A paroxysm is a sudden uncontrolled expression of emotion or a short attack of pain, coughing, or shaking.</p>
<p class='rw-defn idx25' style='display:none'>A peregrination is a long journey or act of traveling from place to place, especially by foot.</p>
<p class='rw-defn idx26' style='display:none'>If someone leads a peripatetic life, they travel from place to place, living and working only for a short time in each place before moving on.</p>
<p class='rw-defn idx27' style='display:none'>A permutation is a complete change or total transformation.</p>
<p class='rw-defn idx28' style='display:none'>Someone who is phlegmatic stays calm and unemotional even in dangerous or exciting situations.</p>
<p class='rw-defn idx29' style='display:none'>A provisional measure is temporary or conditional until more permanent action is taken.</p>
<p class='rw-defn idx30' style='display:none'>A state of quiescence is one of quiet and restful inaction.</p>
<p class='rw-defn idx31' style='display:none'>Someone who has a sedentary habit, job, or lifestyle spends a lot of time sitting down without moving or exercising often.</p>
<p class='rw-defn idx32' style='display:none'>If you are somnolent, you are sleepy.</p>
<p class='rw-defn idx33' style='display:none'>Something that is stagnant is not moving; therefore, it is not growing, progressing, or acting as it should.</p>
<p class='rw-defn idx34' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx35' style='display:none'>If your body is affected by torpor, you are severely lacking in energy; therefore, you are idle—and can even be numb.</p>
<p class='rw-defn idx36' style='display:none'>Something that has the quality of transience lasts for only a short time or is constantly changing.</p>
<p class='rw-defn idx37' style='display:none'>When you traverse something, such as the land or sea, you go across or travel through it.</p>
<p class='rw-defn idx38' style='display:none'>Something that undulates moves or is shaped like waves with gentle curves that smoothly rise and fall.</p>
<p class='rw-defn idx39' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>stasis</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='stasis#' id='pronounce-sound' path='audio/words/amy-stasis'></a>
STAY-sis
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='stasis#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='stasis#' id='context-sound' path='audio/wordcontexts/brian-stasis'></a>
Once again the politicians were locked in a state of <em>stasis</em>, as neither side was willing to budge in their opinions. This unfortunate unmoving balance or <em>stasis</em> between the two sides was caused by the fact that neither side held a clear majority. We were hoping that their <em>stasis</em> or unchanging state would be broken soon, as we really wanted the law on financial security to pass!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Which is the state of <em>stasis</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A chemical that remains stable over time.
</li>
<li class='choice '>
<span class='result'></span>
A crisis caused by an unexpected change.
</li>
<li class='choice '>
<span class='result'></span>
An unusual amount of stubbornness in an individual.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='stasis#' id='definition-sound' path='audio/wordmeanings/amy-stasis'></a>
<em>Stasis</em> is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>stagnation</em>
</span>
</span>
</div>
<a class='quick-help' href='stasis#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='stasis#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/stasis/memory_hooks/6055.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Sis</span></span> <span class="emp3"><span>Sta</span></span>ys</span></span> My <span class="emp1"><span>sis</span></span>ter decided not to change her life plans after all, so my <span class="emp1"><span>sis</span></span>ter is <span class="emp3"><span>sta</span></span>ying in <span class="emp3"><span>sta</span></span><span class="emp1"><span>sis</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="stasis#" id="add-public-hook" style="" url="https://membean.com/mywords/stasis/memory_hooks">Use other public hook</a>
<a href="stasis#" id="memhook-use-own" url="https://membean.com/mywords/stasis/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='stasis#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
How we feel about the evolving future tells us who we are as individuals and as a civilization: Do we search for <b>stasis—a</b> regulated, engineered world? Or do we embrace dynamism—a world of constant creation, discovery, and competition?
<span class='attribution'>&mdash; Virginia Postrel, American political and cultural writer, from _The Future and Its Enemies_</span>
<img alt="Virginia postrel, american political and cultural writer, from _the future and its enemies_" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Virginia Postrel, American political and cultural writer, from _The Future and Its Enemies_.jpg?qdep8" width="80" />
</li>
<li>
As Hollywood tries to stave off commercial <b>stasis</b>, the industry has been undergoing another chapter in its love affair with foreign writers and directors, particularly those from the Far East and Latin America.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
_Bee Gees' 1st, Horizontal_, and _Idea_ can easily be considered as a group, for one of the Bee Gees' essential dynamics as a group is <b>stasis</b>.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
<li>
The result: jazz that’s also true to the spirit of the music as an ever-evolving art form, never in <b>stasis</b>, drawing on both its own traditions and the currents of popular culture.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/stasis/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='stasis#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='stas_standing' data-tree-url='//cdn1.membean.com/public/data/treexml' href='stasis#'>
<span class=''></span>
stas
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>standing, standing still</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='is_noun' data-tree-url='//cdn1.membean.com/public/data/treexml' href='stasis#'>
<span class=''></span>
-is
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>forms a Greek noun</td>
</tr>
</table>
<p>If something is in <em>stasis</em>, it is &#8220;standing&#8221; or &#8220;standing still.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='stasis#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Stasis" src="https://cdn1.membean.com/public/images/wordimages/cons2/stasis.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='stasis#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abeyance</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>inanimate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>incessant</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>inert</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>languid</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>latent</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>monochrome</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>phlegmatic</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>quiescence</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>sedentary</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>somnolent</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>stagnant</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>torpor</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>bedlam</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>capricious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>desultory</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>entropy</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>ephemeral</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>erratic</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>fleeting</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>fluctuate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>impending</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>impulsive</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>intermittent</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>itinerant</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>kinetic</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>ominous</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>pandemonium</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>paroxysm</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>peregrination</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>peripatetic</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>permutation</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>provisional</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>transience</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>traverse</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>undulate</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='stasis#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
static
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>motionless</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="stasis" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>stasis</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="stasis#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

