
<!DOCTYPE html>
<html>
<head>
<title>Word: clarion | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>To accentuate something is to emphasize it or make it more noticeable.</p>
<p class='rw-defn idx1' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx2' style='display:none'>To augment something is to increase its value or effectiveness by adding something to it.</p>
<p class='rw-defn idx3' style='display:none'>If something, such as a sound, is undergoing a crescendo, it is getting louder and louder or increasing in intensity and is about to reach its peak strength.</p>
<p class='rw-defn idx4' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx5' style='display:none'>A dulcet sound is soft and pleasant in a gentle way.</p>
<p class='rw-defn idx6' style='display:none'>If you describe a person, group, or civilization as effete, you mean it is weak, exhausted,  powerless, unproductive, and/or corrupt.</p>
<p class='rw-defn idx7' style='display:none'>When you make an emphatic declaration, you are insistent and absolute about it.</p>
<p class='rw-defn idx8' style='display:none'>Someone or something that is enigmatic is mysterious and difficult to understand.</p>
<p class='rw-defn idx9' style='display:none'>When people equivocate, they avoid making a clear statement; they are deliberately vague in order to avoid giving a direct answer to a question.</p>
<p class='rw-defn idx10' style='display:none'>Something is flaccid when it is unpleasantly soft and weak, hangs limply, or lacks vigor and energy.</p>
<p class='rw-defn idx11' style='display:none'>If something is your forte, you are very good at it or know a lot about it.</p>
<p class='rw-defn idx12' style='display:none'>To imbue someone with a quality, such as an emotion or idea, is to fill that person completely with it.</p>
<p class='rw-defn idx13' style='display:none'>When you are impassioned about a cause or idea, you are very passionate or highly emotionally charged about it.</p>
<p class='rw-defn idx14' style='display:none'>When something is implicit, it is understood without having to say it.</p>
<p class='rw-defn idx15' style='display:none'>A languid person is slow, relaxed, and shows little energy or interest in doing anything.</p>
<p class='rw-defn idx16' style='display:none'>When you are listless, you lack energy and interest and are unwilling to exert any effort.</p>
<p class='rw-defn idx17' style='display:none'>If you describe something as nebulous, you mean that it is unclear, vague, and not clearly defined; a shape that is nebulous has no clear boundaries.</p>
<p class='rw-defn idx18' style='display:none'>Something is nondescript when its appearance is ordinary, dull, and not at all interesting or attractive.</p>
<p class='rw-defn idx19' style='display:none'>Something is opaque if it is either difficult to understand or is not transparent.</p>
<p class='rw-defn idx20' style='display:none'>When a substance permeates something, it enters into all parts of it.</p>
<p class='rw-defn idx21' style='display:none'>If you say that something, such as an event or a message, resonates with you, you mean that it has an emotional effect or a special meaning for you that is significant.</p>
<p class='rw-defn idx22' style='display:none'>A resounding success, victory, or defeat is very great or complete, whereas a noise of this kind is loud, powerful, and ringing.</p>
<p class='rw-defn idx23' style='display:none'>A sonorous sound is pleasantly full, strong, and rich.</p>
<p class='rw-defn idx24' style='display:none'>A strident person makes their feelings or opinions known in a forceful and strong way that often offends some people; not surprisingly, a strident voice is loud, harsh, and shrill.</p>
<p class='rw-defn idx25' style='display:none'>When you have a vehement feeling about something, you feel very strongly or intensely about it.</p>
<p class='rw-defn idx26' style='display:none'>A vibrant person is lively and full of energy in a way that is exciting and attractive.</p>
<p class='rw-defn idx27' style='display:none'>Someone who is vociferous expresses their opinions loudly and strongly because they want their views to be heard.</p>
<p class='rw-defn idx28' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>clarion</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='clarion#' id='pronounce-sound' path='audio/words/amy-clarion'></a>
KLAR-ee-uhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='clarion#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='clarion#' id='context-sound' path='audio/wordcontexts/brian-clarion'></a>
The village doctor had heard the moving, <em>clarion</em> call to service as a young man.  The generous doctor himself had a loud voice and a <em>clarion</em>, trumpet-like laugh.  Some villagers considered his voice too sharp or loud, but his <em>clarion</em>, persuasive, and inspiring enthusiasm, which encouraged all to help their neighbors, endeared him to most people who lived there.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>clarion</em> call?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
One that is harsh, jarring, and startles people.
</li>
<li class='choice answer '>
<span class='result'></span>
One that is loud, clear, and motivates people to act.
</li>
<li class='choice '>
<span class='result'></span>
One that is eerie, mysterious, and disturbs people.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='clarion#' id='definition-sound' path='audio/wordmeanings/amy-clarion'></a>
A <em>clarion</em> call is a stirring, emotional, and strong appeal to people to take action on something.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>loud and clear</em>
</span>
</span>
</div>
<a class='quick-help' href='clarion#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='clarion#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/clarion/memory_hooks/4673.json'></span>
<p>
<img alt="Clarion" src="https://cdn1.membean.com/public/images/wordimages/hook/clarion.jpg?qdep8" />
<span class="emp0"><span>L<span class="emp1"><span>ion</span></span>'s <span class="emp3"><span>Clar</span></span>ity</span></span> Often in the middle of the jungle night I would hear the l<span class="emp1"><span>ion</span></span>'s deep <span class="emp3"><span>clar</span></span><span class="emp1"><span>ion</span></span> call piercing the night loud and <span class="emp3"><span>cl</span></span>e<span class="emp3"><span>ar</span></span>, hushing all noise and widening all eyes.
</p>
</div>

<div id='memhook-button-bar'>
<a href="clarion#" id="add-public-hook" style="" url="https://membean.com/mywords/clarion/memory_hooks">Use other public hook</a>
<a href="clarion#" id="memhook-use-own" url="https://membean.com/mywords/clarion/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='clarion#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
"Economic revival is our most urgent task," Lee declared in his inaugural address, which swung between <b>clarion</b> calls to create a more compassionate society and a lecture on how South Korea can remain competitive in a globalizing economy.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
This is a reminder to everyone, a <b>clarion</b> call; you don’t need to eat junk food to eat a tasty dessert.
<cite class='attribution'>
&mdash;
Epicurious
</cite>
</li>
<li>
Aretha’s voice rings out like a <b>clarion</b> call to righteousness, as she hits miraculous note after miraculous note, holding and unbending with a freedom that seems in itself holy.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/clarion/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='clarion#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='clar_clear' data-tree-url='//cdn1.membean.com/public/data/treexml' href='clarion#'>
<span class=''></span>
clar
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>clear, bright</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ion_act' data-tree-url='//cdn1.membean.com/public/data/treexml' href='clarion#'>
<span class=''></span>
-ion
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>act, state, or result of doing something</td>
</tr>
</table>
<p>A <em>clarion</em> call is a &#8220;clear and bright&#8221; sound, much like the piercing notes of a <em>clarion</em>, a medieval trumpet that possessed a loud, ringing, and piercing tone.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='clarion#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Lord of the Rings</strong><span> The clarion call of Aragorn moves the Men of the West to the conclusive battle against Mordor.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/clarion.jpg' video_url='examplevids/clarion' video_width='350'></span>
<div id='wt-container'>
<img alt="Clarion" height="288" src="https://cdn1.membean.com/video/examplevids/clarion.jpg" width="350" />
<div class='center'>
<a href="clarion#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='clarion#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Clarion" src="https://cdn3.membean.com/public/images/wordimages/cons2/clarion.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='clarion#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>accentuate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>augment</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>crescendo</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>emphatic</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>forte</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>imbue</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>impassioned</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>permeate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>resonate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>resounding</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>sonorous</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>strident</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>vehement</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>vibrant</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>vociferous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>dulcet</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>effete</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>enigmatic</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>equivocate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>flaccid</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>implicit</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>languid</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>listless</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>nebulous</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>nondescript</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>opaque</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="clarion" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>clarion</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="clarion#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

