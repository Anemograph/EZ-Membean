
<!DOCTYPE html>
<html>
<head>
<title>Word: introspective | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you are bemused, you are puzzled and confused; hence, you are lost or absorbed in puzzling thought.</p>
<p class='rw-defn idx1' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx2' style='display:none'>When you cavort, you jump and dance around in a playful, excited, or physically lively way.</p>
<p class='rw-defn idx3' style='display:none'>Cognitive describes those things related to judgment, memory, and other mental processes of knowing.</p>
<p class='rw-defn idx4' style='display:none'>A convivial atmosphere or occasion is friendly, pleasant, cheerful, and relaxed.</p>
<p class='rw-defn idx5' style='display:none'>Someone who is effusive expresses happiness, pleasure, admiration, praise, etc., in an extremely enthusiastic way.</p>
<p class='rw-defn idx6' style='display:none'>A garrulous person talks a lot, especially about unimportant things.</p>
<p class='rw-defn idx7' style='display:none'>A gregarious person is friendly, highly social, and prefers being with people rather than being alone.</p>
<p class='rw-defn idx8' style='display:none'>Someone who is loquacious talks too much.</p>
<p class='rw-defn idx9' style='display:none'>If you are melancholy, you look and feel sad.</p>
<p class='rw-defn idx10' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx11' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx12' style='display:none'>If you are pensive, you are deeply thoughtful, often in a sad and/or serious way.</p>
<p class='rw-defn idx13' style='display:none'>Something that is poignant affects you deeply and makes you feel sad or full of pity.</p>
<p class='rw-defn idx14' style='display:none'>Prattle is mindless chatter that seems like it will never stop.</p>
<p class='rw-defn idx15' style='display:none'>If someone watches or listens to something with rapt attention, they are so involved with it that they do not notice anything else.</p>
<p class='rw-defn idx16' style='display:none'>A reverie is a state of pleasant dreamlike thoughts that makes you forget what you are doing and what is happening around you.</p>
<p class='rw-defn idx17' style='display:none'>If you ruminate on something, you think carefully and deeply about it over a long period of time.</p>
<p class='rw-defn idx18' style='display:none'>Something that is specious seems to be good, sound, or correct but is actually wrong or fake.</p>
<p class='rw-defn idx19' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>
<p class='rw-defn idx20' style='display:none'>People who are wistful are rather sad because they want something but know that they cannot have it, especially something that they used to have in the past.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>introspective</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='introspective#' id='pronounce-sound' path='audio/words/amy-introspective'></a>
in-truh-SPEKT-iv
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='introspective#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='introspective#' id='context-sound' path='audio/wordcontexts/brian-introspective'></a>
Laura was thoughtful, sensitive, and <em>introspective</em> by nature; she liked to keep a daily journal to record her thoughts and feelings.  She filled pages and pages with <em>introspective</em> or reflective words about how she saw and thought about the world.  The philosophical Laura, a deep thinker with an <em>introspective</em> personality, always looked closely at her own emotions and reactions in order to learn more about who she was.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of being <em>introspective</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Noticing that your friend hasn&#8217;t slept in weeks.
</li>
<li class='choice '>
<span class='result'></span>
Sharing feedback with a coworker about their work habits.
</li>
<li class='choice answer '>
<span class='result'></span>
Taking time to journal about how you are feeling each day.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='introspective#' id='definition-sound' path='audio/wordmeanings/amy-introspective'></a>
If someone is <em>introspective</em>, they spend a lot of time examining their own feelings, thoughts, or ideas.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>looking within</em>
</span>
</span>
</div>
<a class='quick-help' href='introspective#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='introspective#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/introspective/memory_hooks/4667.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Inte</span></span>rior <span class="emp2"><span>Spect</span></span>at<span class="emp2"><span>or</span></span></span></span> Richard loved to watch his own mind, with all its tricks, random thoughts, and emotional highs and lows; the <span class="emp3"><span>int</span></span><span class="emp2"><span>rospect</span></span>iv<span class="emp3"><span>e</span></span> Rich considered himself to be an <span class="emp3"><span>inte</span></span>rior <span class="emp2"><span>spect</span></span>at<span class="emp2"><span>or</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="introspective#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/introspective/memory_hooks">Use other hook</a>
<a href="introspective#" id="memhook-use-own" url="https://membean.com/mywords/introspective/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='introspective#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Social media technology allows and encourages us to publicly document our private lives, which means that quiet moments and private thoughts that were once <b>introspective</b> and personally significant can, for the first time, be judged on a public platform.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
Considered one of the top 10 films of 1987 by Japanese film critics, _The Wings of Honneamise_ is a bittersweet, <b>introspective</b> tale of an incompetent space program staffed by slacker astronauts who are despised by society at large.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/introspective/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='introspective#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='intro_within' data-tree-url='//cdn1.membean.com/public/data/treexml' href='introspective#'>
<span class=''></span>
intro-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>within</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='spect_see' data-tree-url='//cdn1.membean.com/public/data/treexml' href='introspective#'>
<span class='common'></span>
spect
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>see, observe, look, watch over</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ive_does' data-tree-url='//cdn1.membean.com/public/data/treexml' href='introspective#'>
<span class=''></span>
-ive
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or that which does something</td>
</tr>
</table>
<p>A person with an <em>introspective</em> nature likes to &#8220;look or observe within&#8221; the mind.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='introspective#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Visioneers</strong><span> This is an introspective guy.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/introspective.jpg' video_url='examplevids/introspective' video_width='350'></span>
<div id='wt-container'>
<img alt="Introspective" height="288" src="https://cdn1.membean.com/video/examplevids/introspective.jpg" width="350" />
<div class='center'>
<a href="introspective#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='introspective#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Introspective" src="https://cdn3.membean.com/public/images/wordimages/cons2/introspective.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='introspective#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>bemused</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>cognitive</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>melancholy</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>pensive</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>poignant</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>rapt</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>reverie</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>ruminate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>wistful</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>cavort</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>convivial</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>effusive</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>garrulous</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>gregarious</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>loquacious</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>prattle</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>specious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="introspective" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>introspective</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="introspective#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

