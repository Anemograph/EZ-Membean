
<!DOCTYPE html>
<html>
<head>
<title>Word: fugue | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Circumlocution is a way of saying or writing something that uses too many words, especially in order to avoid stating the true meaning clearly.</p>
<p class='rw-defn idx1' style='display:none'>When something is diffuse, it is widely spread out and scattered.</p>
<p class='rw-defn idx2' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx3' style='display:none'>Something ephemeral, such as some insects or a sunset, lasts for only a short time or has a very short lifespan.</p>
<p class='rw-defn idx4' style='display:none'>To imbue someone with a quality, such as an emotion or idea, is to fill that person completely with it.</p>
<p class='rw-defn idx5' style='display:none'>If you describe a situation or process as labyrinthine, you mean that it is very complicated, involved, and difficult to understand.</p>
<p class='rw-defn idx6' style='display:none'>If something is obsolescent, it is slowly becoming no longer needed because something newer or more effective has been invented.</p>
<p class='rw-defn idx7' style='display:none'>A profusion of something is a very large quantity or variety of it.</p>
<p class='rw-defn idx8' style='display:none'>When someone recapitulates, they summarize material or content of some kind by repeating the most important points.</p>
<p class='rw-defn idx9' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx10' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx11' style='display:none'>If something, such as warmth, color, or liquid, suffuses a material, it gradually spreads through or covers it; if you are suffused with a feeling, you are full of that feeling.</p>
<p class='rw-defn idx12' style='display:none'>When you employ a tautology, you needlessly and often unintentionally repeat a similar sense of one word when using other words to describe it, such as in the redundant phrases &#8220;free gift&#8221; or &#8220;usual custom.&#8221;</p>
<p class='rw-defn idx13' style='display:none'>Something that has the quality of transience lasts for only a short time or is constantly changing.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>fugue</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='fugue#' id='pronounce-sound' path='audio/words/amy-fugue'></a>
fyoog
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='fugue#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='fugue#' id='context-sound' path='audio/wordcontexts/brian-fugue'></a>
The overlapping, musical strains of the Bach <em>fugue</em>, which began with a simple melody that repeated itself, filled the church.  Each separate pattern of the theme within the <em>fugue</em> could be heard by the congregation as it rose and repeated, growing louder and more complex each time.  I believed, as I listened, that Bach&#8217;s <em>fugue</em> for organ represented all that was best about this musical form: pattern, simplicity, increasing complexity, and graceful return to the original musical theme.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>fugue</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A musical form written for sacred worship that is very rarely performed today.
</li>
<li class='choice answer '>
<span class='result'></span>
A musical form that repeats a simple theme via multiple variations of that theme.
</li>
<li class='choice '>
<span class='result'></span>
A musical form that is easy to play and compose due to its simple melodic line.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='fugue#' id='definition-sound' path='audio/wordmeanings/amy-fugue'></a>
A <em>fugue</em> is a piece of classical music that starts with a simple thematic melody; subsequently, it is developed into a complicated pattern by repeating that melody with different instruments, voices, or keys.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>musical pattern</em>
</span>
</span>
</div>
<a class='quick-help' href='fugue#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='fugue#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/fugue/memory_hooks/3498.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Fu</span></span>r <span class="emp2"><span>G</span></span>l<span class="emp2"><span>ue</span></span></span></span> Although a <span class="emp1"><span>fu</span></span><span class="emp2"><span>gue</span></span> begins with a catchy melody that seems to disappear, it soon reappears again in a different way and never leaves the music, much in the same way that <span class="emp2"><span>g</span></span>l<span class="emp2"><span>ue</span></span> sticks to <span class="emp1"><span>fu</span></span>r.
</p>
</div>

<div id='memhook-button-bar'>
<a href="fugue#" id="add-public-hook" style="" url="https://membean.com/mywords/fugue/memory_hooks">Use other public hook</a>
<a href="fugue#" id="memhook-use-own" url="https://membean.com/mywords/fugue/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='fugue#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The keyboard preludes and <b>fugues</b> of “The Well-Tempered Clavier” have been orchestrated for many chamber ensembles: brass quintets, saxophone quartet, trombone trios, woodwind quintets and so on.
<cite class='attribution'>
&mdash;
Yakima Herald-Republic
</cite>
</li>
<li>
Whether Bach ever envisaged such a concert presentation is highly unlikely, since this systematic, 75-minute cycle of <b>fugues</b> and canons is all constructed on one theme and all in the key of D minor.
<cite class='attribution'>
&mdash;
The Independent
</cite>
</li>
<li>
On a snowy day in Berlin, two days after Christmas 1841, Franz Liszt strode out onto the stage at the Berliner Singakademie concert hall. He sat at his grand piano in profile, beads of sweat forming on his forehead. . . . Using his whole body—his undulating eyebrows, his wild arms, even his swaying hips—Liszt dove into Händel's “<b>Fugue</b> in E minor” with vigor and unfettered confidence, keeping perfect tempo and playing entirely from memory.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
In Variation No. 7, the Baroque dance character of Bach’s music, with its clipped ornaments and jagged dotted-note rhythms, is almost more pronounced in the upside-down version. . . . In its new version, Bach’s Variation No. 10, a short <b>fugue</b>, may be the strongest example of the “prism” effect Mr. Tepfer described. You feel you’ve heard this music before. Yet the counterpoint seems to roam all over the place and the harmonic language is vaguely like Hindemith in his Neo-Classical vein.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/fugue/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='fugue#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='fug_flee' data-tree-url='//cdn1.membean.com/public/data/treexml' href='fugue#'>
<span class=''></span>
fug
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>flee</td>
</tr>
</table>
<p>The musical idea behind a <em>fugue</em> is that it begins with a simple melody, which &#8220;flees,&#8221; only to return, &#8220;flee&#8221; again, and return, etc.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='fugue#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Vanessa-Mae</strong><span> Masterfully playing Toccata and Fugue, her debut pop single.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/fugue.jpg' video_url='examplevids/fugue' video_width='350'></span>
<div id='wt-container'>
<img alt="Fugue" height="288" src="https://cdn1.membean.com/video/examplevids/fugue.jpg" width="350" />
<div class='center'>
<a href="fugue#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='fugue#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Fugue" src="https://cdn0.membean.com/public/images/wordimages/cons2/fugue.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='fugue#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>circumlocution</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>diffuse</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>imbue</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>labyrinthine</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>profusion</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>recapitulate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>suffuse</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>tautology</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>ephemeral</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>obsolescent</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>transience</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="fugue" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>fugue</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="fugue#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

