
<!DOCTYPE html>
<html>
<head>
<title>Word: prophylactic | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Prophylactic-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/prophylactic-large.jpg?qdep8" />
</div>
<a href="prophylactic#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>If someone in authority abolishes a law or practice, they formally put an end to it.</p>
<p class='rw-defn idx1' style='display:none'>Something done under the aegis of an organization or person is done with their support, backing, and protection.</p>
<p class='rw-defn idx2' style='display:none'>A bastion is considered an important and effective defense of a way of life or principle; a bastion can also refer to a heavily fortified place, such as a castle.</p>
<p class='rw-defn idx3' style='display:none'>When you buttress an argument, idea, or even a building, you make it stronger by providing support.</p>
<p class='rw-defn idx4' style='display:none'>A carapace is the protective shell on the back of some animals, such as the tortoise or crab; by extension, if you build a carapace around yourself, you adopt a behavior or attitude designed to protect yourself.</p>
<p class='rw-defn idx5' style='display:none'>A cataclysm is a violent, sudden event that causes great change and/or harm.</p>
<p class='rw-defn idx6' style='display:none'>If something confounds you, it makes you feel surprised and confused, often because it does not meet your predefined expectations.</p>
<p class='rw-defn idx7' style='display:none'>A contagion is a disease—or the transmission of that disease—that is easily spread from one person to another through touch or the air.</p>
<p class='rw-defn idx8' style='display:none'>If you decimate something, you destroy a large part of it, reducing its size and effectiveness greatly.</p>
<p class='rw-defn idx9' style='display:none'>If you extirpate something, you completely destroy it because it is unpleasant or unwanted.</p>
<p class='rw-defn idx10' style='display:none'>A pathogen is an organism, such as a bacterium or virus, that causes disease.</p>
<p class='rw-defn idx11' style='display:none'>A pestilence is either an infectious disease that is deadly or an agent of some kind that is destructive or dangerous.</p>
<p class='rw-defn idx12' style='display:none'>If someone is predisposed to something, they are made favorable or inclined to it in advance, or they are made susceptible to something, such as a disease.</p>
<p class='rw-defn idx13' style='display:none'>To quell something is to stamp out, quiet, or overcome it.</p>
<p class='rw-defn idx14' style='display:none'>A salubrious place or area is pleasant, clean, healthy, and comfortable to live in.</p>
<p class='rw-defn idx15' style='display:none'>A salutary experience is beneficial to you since it strengthens you in some way, although it may be unpleasant as you undergo it; this word also refers to promoting good health.</p>
<p class='rw-defn idx16' style='display:none'>A scourge was originally a whip used for torture and now refers to something that torments or causes serious trouble or devastation.</p>
<p class='rw-defn idx17' style='display:none'>When you succumb to something, you give in to, yield to, or die from it.</p>
<p class='rw-defn idx18' style='display:none'>If you are susceptible to something, such as a disease or emotion, you are likely or inclined to be affected by it.</p>
<p class='rw-defn idx19' style='display:none'>To be under someone&#8217;s tutelage is to be under their guidance and teaching.</p>
<p class='rw-defn idx20' style='display:none'>When you are vigilant, you are keenly watchful, careful, or attentive to a task or situation.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>prophylactic</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='prophylactic#' id='pronounce-sound' path='audio/words/amy-prophylactic'></a>
proh-fuh-LAK-tik
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='prophylactic#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='prophylactic#' id='context-sound' path='audio/wordcontexts/brian-prophylactic'></a>
With swine flu becoming a worldwide issue, drug companies are scurrying to find a vaccine to serve as a <em>prophylactic</em> to prevent its spread.  Although the <em>prophylactic</em> will not destroy the disease, it will help keep those who have gotten the shot from getting infected.  Other suggested <em>prophylactic</em> procedures to prevent the spread of disease are hand washing, covering the mouth and nose when coughing, and keeping infected people at home while they are ill.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>prophylactic</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A test that is used to detect the presence of a disease.
</li>
<li class='choice answer '>
<span class='result'></span>
A vaccine used to help stop the spread of a disease.
</li>
<li class='choice '>
<span class='result'></span>
A disease, like <span class="caps">COVID</span>-19, that can easily spread.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='prophylactic#' id='definition-sound' path='audio/wordmeanings/amy-prophylactic'></a>
A <em>prophylactic</em> is used as a preventative or protective agent to keep someone free from disease or infection.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>preventative</em>
</span>
</span>
</div>
<a class='quick-help' href='prophylactic#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='prophylactic#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/prophylactic/memory_hooks/4050.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Pla</span></span>s<span class="emp1"><span>tic</span></span> T<span class="emp3"><span>rophy</span></span></span></span>  To prevent my two boys from killing each other over who won their game, I gave them each a <span class="emp1"><span>p</span></span><span class="emp3"><span>rophy</span></span><span class="emp1"><span>la</span></span>c<span class="emp1"><span>tic</span></span> in the form of a <span class="emp1"><span>pla</span></span>s<span class="emp1"><span>tic</span></span> t<span class="emp3"><span>rophy</span></span>, which kept them from taking out their aggression on each other.
</p>
</div>

<div id='memhook-button-bar'>
<a href="prophylactic#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/prophylactic/memory_hooks">Use other hook</a>
<a href="prophylactic#" id="memhook-use-own" url="https://membean.com/mywords/prophylactic/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='prophylactic#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Nerds don't just happen to dress informally. They do it too consistently. Consciously or not, they dress informally as a <b>prophylactic</b> measure against stupidity.
<span class='attribution'>&mdash; Paul Graham, American computer scientist</span>
<img alt="Paul graham, american computer scientist" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Paul Graham, American computer scientist.jpg?qdep8" width="80" />
</li>
<li>
Now in his 50s, Martin has reached "the <b>prophylactic</b> age; the age of work-outs in the gym and check-ups of the colon".
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Camp Modin brought the outbreak to a dead halt by offering <b>prophylactic</b> Tamiflu to all of the campers and counselors, a move it took on the advice of one of the parents, Dr. Marc Siegel.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
As the annual summer epidemic of infantile paralysis was about to break upon the country last week, expert <b>prophylactic</b> teams deployed over the nation to prevent it by spraying the noses of children with zinc sulphate.
<cite class='attribution'>
&mdash;
Time
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/prophylactic/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='prophylactic#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pro_before' data-tree-url='//cdn1.membean.com/public/data/treexml' href='prophylactic#'>
<span class=''></span>
pro-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>before, in front</td>
</tr>
<tr>
<td class='partform'>phylact</td>
<td>
&rarr;
</td>
<td class='meaning'>watcher, guard, sentinel</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ic_like' data-tree-url='//cdn1.membean.com/public/data/treexml' href='prophylactic#'>
<span class=''></span>
-ic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>like</td>
</tr>
</table>
<p>A <em>prophylactic</em> is &#8220;like a watcher or guard before or in front of&#8221; you which helps prevent something from happening.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='prophylactic#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Prophylactic" src="https://cdn0.membean.com/public/images/wordimages/cons2/prophylactic.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='prophylactic#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>aegis</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bastion</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>buttress</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>carapace</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>quell</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>salubrious</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>salutary</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>tutelage</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>vigilant</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abolish</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>cataclysm</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>confound</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>contagion</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>decimate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>extirpate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>pathogen</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>pestilence</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>predispose</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>scourge</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>succumb</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>susceptible</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='prophylactic#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
prophylactic
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>protective; acting to defend against something</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="prophylactic" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>prophylactic</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="prophylactic#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

