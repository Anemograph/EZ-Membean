
<!DOCTYPE html>
<html>
<head>
<title>Word: antithesis | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When two or more things, such as organizations, amalgamate, they combine to become one large thing.</p>
<p class='rw-defn idx1' style='display:none'>Your antagonist is an opponent in a competition or battle.</p>
<p class='rw-defn idx2' style='display:none'>Antipathy is a strong feeling of dislike or hostility towards someone or something.</p>
<p class='rw-defn idx3' style='display:none'>Two points or places are antipodal if they are directly opposite each other, such as the north and south poles; likewise, ideas can be antipodal if they are direct opposites.</p>
<p class='rw-defn idx4' style='display:none'>When you assimilate an idea, you completely understand it; likewise, when someone is assimilated into a new place, they become part of it by understanding what it is all about and by being accepted by others who live there.</p>
<p class='rw-defn idx5' style='display:none'>If two or more things coalesce, they come together to form a single larger unit.</p>
<p class='rw-defn idx6' style='display:none'>When someone is contentious, they are argumentative and likely to provoke a fight.</p>
<p class='rw-defn idx7' style='display:none'>If there is a dichotomy between two things, there is a division of great difference or opposition between them.</p>
<p class='rw-defn idx8' style='display:none'>A situation or thing that is discordant does not fit in with other things; therefore, it is disagreeable, strange, or unpleasant.</p>
<p class='rw-defn idx9' style='display:none'>Things that are disparate are clearly different from each other and belong to different groups or classes.</p>
<p class='rw-defn idx10' style='display:none'>Divergent opinions differ from each other; divergent paths move apart from one another.</p>
<p class='rw-defn idx11' style='display:none'>Ecumenical activities and ideas encourage different religions or congregations to work and worship together in order to unite them in friendship.</p>
<p class='rw-defn idx12' style='display:none'>The word homogeneous is used to describe a group that has members or parts which are similar or are all of the same type.</p>
<p class='rw-defn idx13' style='display:none'>If two people are incompatible, they do not get along, tend to disagree, and are unable to cooperate with one another.</p>
<p class='rw-defn idx14' style='display:none'>Something that is incongruous is strange when considered along with its group because it seems very different from the other things in its group.</p>
<p class='rw-defn idx15' style='display:none'>Two irreconcilable opinions or points of view are so opposed to each other that it is not possible to accept both of them or reach a settlement between them.</p>
<p class='rw-defn idx16' style='display:none'>Something is obverse when it is facing or turned towards the observer, such as the facing side of a coin.</p>
<p class='rw-defn idx17' style='display:none'>Proximity is how close or near one thing is to another.</p>
<p class='rw-defn idx18' style='display:none'>Restitution is the formal act of giving something that was taken away back to the rightful owner—or paying them money for the loss of the object.</p>
<p class='rw-defn idx19' style='display:none'>A schism causes a group or organization to divide into two groups as a result of differences in their aims and beliefs.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>antithesis</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='antithesis#' id='pronounce-sound' path='audio/words/amy-antithesis'></a>
an-TITH-uh-sis
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='antithesis#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='antithesis#' id='context-sound' path='audio/wordcontexts/brian-antithesis'></a>
Barry was the opposite or <em>antithesis</em> of a good waiter. He moved slowly and heavily through the restaurant, which was the <em>antithesis</em> of the efficiency of  good waitstaff. Likewise, Barry knew very little about the menu, so his lack of knowledge was <em>antithetical</em> to the needs of the restaurant that relied upon customers&#8217; knowing the food options. When Barry finally realized that his skills were <em>antithetical</em> to his job&#8217;s requirements, he made a change and became a parking attendant.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Which two words form the <em>antithesis</em> of one another?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Hot and cold.
</li>
<li class='choice '>
<span class='result'></span>
Loving and liking.
</li>
<li class='choice '>
<span class='result'></span>
Darkness and twilight.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='antithesis#' id='definition-sound' path='audio/wordmeanings/amy-antithesis'></a>
The <em>antithesis</em> of something is its opposite.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>opposite</em>
</span>
</span>
</div>
<a class='quick-help' href='antithesis#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='antithesis#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/antithesis/memory_hooks/2828.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>An</span></span>ne's <span class="emp2"><span>Tithe</span></span></span></span> <span class="emp3"><span>Sis</span></span>ter <span class="emp1"><span>An</span></span>ne got her hand slapped as she reached into the <span class="emp2"><span>tithe</span></span> basket to <em>take out</em> money while everyone else's <span class="emp2"><span>tithe</span></span> was going <em>into</em> the basket, for <span class="emp3"><span>Sis</span></span>ter <span class="emp1"><span>An</span></span>ne was doing the <span class="emp1"><span>an</span></span><span class="emp2"><span>tithe</span></span><span class="emp3"><span>sis</span></span> of everyone else.
</p>
</div>

<div id='memhook-button-bar'>
<a href="antithesis#" id="add-public-hook" style="" url="https://membean.com/mywords/antithesis/memory_hooks">Use other public hook</a>
<a href="antithesis#" id="memhook-use-own" url="https://membean.com/mywords/antithesis/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='antithesis#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Cheating is the <b>antithesis</b> of equal opportunity—the notion that we all should have a fair shot at success and that the people who get rewarded are the people who deserve those rewards because they worked the hardest.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
Inspiration may be a form of superconsciousness, or perhaps of subconsciousness—I wouldn't know. But I am sure it is the <b>antithesis</b> of self-consciousness.
<cite class='attribution'>
&mdash;
Aaron Copland, American composer
</cite>
</li>
<li>
Plain-spoken and lacking in pretension, Mangino is the <b>antithesis</b> of the single-minded coaches who see life as a metaphor for football instead of the other way around.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
Farming was the <b>antithesis</b> to the nomadic way of life, bringing with it a domesticated landscape, a settled existence, ownership, and laws to protect it.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/antithesis/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='antithesis#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='anti_opposite' data-tree-url='//cdn1.membean.com/public/data/treexml' href='antithesis#'>
<span class=''></span>
anti-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>opposite, against</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='thes_put' data-tree-url='//cdn1.membean.com/public/data/treexml' href='antithesis#'>
<span class=''></span>
thes
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>put, place</td>
</tr>
</table>
<p>The <em>antithesis</em> of one thing in relation to another is the &#8220;putting&#8221; or &#8220;arranging&#8221; of it &#8220;opposite or against&#8221; the other.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='antithesis#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Best Friends for Never</strong><span> It's rudely explained to Georgia that magic is the antithesis of science.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/antithesis.jpg' video_url='examplevids/antithesis' video_width='350'></span>
<div id='wt-container'>
<img alt="Antithesis" height="288" src="https://cdn1.membean.com/video/examplevids/antithesis.jpg" width="350" />
<div class='center'>
<a href="antithesis#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='antithesis#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Antithesis" src="https://cdn0.membean.com/public/images/wordimages/cons2/antithesis.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='antithesis#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>antagonist</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>antipathy</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>antipodal</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>contentious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>dichotomy</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>discordant</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>disparate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>divergent</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>incompatible</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>incongruous</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>irreconcilable</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>obverse</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>schism</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>amalgamate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>assimilate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>coalesce</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>ecumenical</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>homogeneous</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>proximity</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>restitution</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='antithesis#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
antithetical
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>exactly opposite to</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="antithesis" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>antithesis</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="antithesis#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

