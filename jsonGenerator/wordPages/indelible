
<!DOCTYPE html>
<html>
<head>
<title>Word: indelible | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Indelible-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/indelible-large.jpg?qdep8" />
</div>
<a href="indelible#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>If you describe a person&#8217;s behavior as brazen, you mean that they are not embarrassed by anything they do—they simply don&#8217;t care what other people think about them.</p>
<p class='rw-defn idx1' style='display:none'>A cardinal rule or quality is considered to be the most important or basic in a set of rules or qualities.</p>
<p class='rw-defn idx2' style='display:none'>A chronic illness or pain is serious and lasts for a long time; a chronic problem is always happening or returning and is very difficult to solve.</p>
<p class='rw-defn idx3' style='display:none'>If you expunge something, you completely remove it or do away with it.</p>
<p class='rw-defn idx4' style='display:none'>If you extirpate something, you completely destroy it because it is unpleasant or unwanted.</p>
<p class='rw-defn idx5' style='display:none'>When you extricate yourself from a difficult or unpleasant situation, you manage to escape it.</p>
<p class='rw-defn idx6' style='display:none'>A fleeting moment lasts but a short time before it fades away.</p>
<p class='rw-defn idx7' style='display:none'>To imbue someone with a quality, such as an emotion or idea, is to fill that person completely with it.</p>
<p class='rw-defn idx8' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx9' style='display:none'>An impregnable fortress or castle is very difficult to defeat or overcome; an opinion or argument of that same quality is almost impossible to successfully change or challenge.</p>
<p class='rw-defn idx10' style='display:none'>Someone who is incorrigible has bad habits or does bad things and is unlikely to ever change; this word is often used in a humorous way.</p>
<p class='rw-defn idx11' style='display:none'>An inveterate person is always doing a particular thing, especially something questionable—and they are not likely to stop doing it.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is irresolute is unable to decide what to do.</p>
<p class='rw-defn idx13' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx14' style='display:none'>Someone who is obdurate is stubborn, unreasonable, or uncontrollable; hence, they simply refuse to change their behavior, actions, or beliefs to fit any situation whatsoever.</p>
<p class='rw-defn idx15' style='display:none'>If an object oscillates, it moves repeatedly from one point to another and then back again; if you oscillate between two moods or attitudes, you keep changing from one to the other.</p>
<p class='rw-defn idx16' style='display:none'>Something that is perennial lasts a very long time and is enduring.</p>
<p class='rw-defn idx17' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx18' style='display:none'>If something, such as warmth, color, or liquid, suffuses a material, it gradually spreads through or covers it; if you are suffused with a feeling, you are full of that feeling.</p>
<p class='rw-defn idx19' style='display:none'>When something supersedes another thing, it takes the place of or succeeds it.</p>
<p class='rw-defn idx20' style='display:none'>When someone vacillates, they go back and forth about a choice or opinion, unable to make a firm decision.</p>
<p class='rw-defn idx21' style='display:none'>A vagary is an unpredictable or unexpected change in action or behavior.</p>
<p class='rw-defn idx22' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>
<p class='rw-defn idx23' style='display:none'>If you waver, you cannot decide between two things because you have serious doubts about which choice is better.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>indelible</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='indelible#' id='pronounce-sound' path='audio/words/amy-indelible'></a>
in-DEL-uh-buhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='indelible#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='indelible#' id='context-sound' path='audio/wordcontexts/brian-indelible'></a>
When Brigit left to study at the conservatory, she knew that her mentor had left an <em>indelible</em>, permanent mark on her.  Studying cello for twelve years with Dr. Bell had influenced more than her musical talent&#8212;it had <em><em>indelibly</em></em> and memorably shaped her character.  Brigit believed that lessons from the master cellist would continue to guide her life in unforgettable and <em>indelible</em> ways.  Using the <em>indelible</em>, lasting skills that Dr. Bell had taught her, she went on to pursue a brilliant musical career.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When would you consider someone&#8217;s influence <em>indelible</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When it is unwanted and given by someone you don&#8217;t trust.
</li>
<li class='choice answer '>
<span class='result'></span>
When it has a long-lasting and unforgettable impact on you. 
</li>
<li class='choice '>
<span class='result'></span>
When it leads you to make decisions that you later regret.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='indelible#' id='definition-sound' path='audio/wordmeanings/amy-indelible'></a>
If someone leaves an <em>indelible</em> impression on you, it will not be forgotten; an <em>indelible</em> mark is permanent or impossible to erase or remove.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>permanent</em>
</span>
</span>
</div>
<a class='quick-help' href='indelible#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='indelible#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/indelible/memory_hooks/5725.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Del</span></span>l's In<span class="emp3"><span>del</span></span>ible Influence</span></span> The computer company <span class="emp3"><span>Del</span></span>l has had an in<span class="emp3"><span>del</span></span>ible influence on the computer market.
</p>
</div>

<div id='memhook-button-bar'>
<a href="indelible#" id="add-public-hook" style="" url="https://membean.com/mywords/indelible/memory_hooks">Use other public hook</a>
<a href="indelible#" id="memhook-use-own" url="https://membean.com/mywords/indelible/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='indelible#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Character is the <b>indelible</b> mark that determines the only true value of all people and all their work.
<span class='attribution'>&mdash; Dr. Orison Swett Marden, American writer</span>
<img alt="Dr" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Dr. Orison Swett Marden, American writer.jpg?qdep8" width="80" />
</li>
<li>
But everyone knows Sting or, rather, his name and his music. The Police left an <b>indelible</b> imprint on the early 1980s, before the immensely talented musician became one of the best-known names in pop music on his own.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
The checkered pattern helped hide speck-sized food stains, but it was rendered mostly unusable when a surly porter dabbed me with <b>indelible</b> India ink in the Victoria railway station in Delhi in 2003. You can still see the stain on the lower right front and on the right sleeve near the inside of the elbow.
<cite class='attribution'>
&mdash;
Epicurious
</cite>
</li>
<li>
As the band prepares to release _Dreaming Out Loud_, the quintet is confident that sticking to its original guns—<b>indelible</b> hooks, vague but enormous emotional crescendos and Tedder’s flexibly soulful voice—was the right decision.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/indelible/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='indelible#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='in_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='indelible#'>
<span class='common'></span>
in-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='del_destroy' data-tree-url='//cdn1.membean.com/public/data/treexml' href='indelible#'>
<span class=''></span>
del
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>destroy, efface, wipe out</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ible_handy' data-tree-url='//cdn1.membean.com/public/data/treexml' href='indelible#'>
<span class=''></span>
-ible
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>handy, capable of</td>
</tr>
</table>
<p>An <em>indelible</em> mark is &#8220;not capable of being destroyed or wiped out.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='indelible#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>A Christmas Story</strong><span> This boy really wants something for Christmas, and is trying to indelibly implant that wish in his parents' brains.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/indelible.jpg' video_url='examplevids/indelible' video_width='350'></span>
<div id='wt-container'>
<img alt="Indelible" height="288" src="https://cdn1.membean.com/video/examplevids/indelible.jpg" width="350" />
<div class='center'>
<a href="indelible#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='indelible#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Indelible" src="https://cdn1.membean.com/public/images/wordimages/cons2/indelible.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='indelible#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>brazen</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>cardinal</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>chronic</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>imbue</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>impregnable</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>incorrigible</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>inveterate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>obdurate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>perennial</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>suffuse</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>expunge</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>extirpate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>extricate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>fleeting</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>irresolute</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>oscillate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>supersede</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>vacillate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>vagary</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>waver</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="indelible" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>indelible</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="indelible#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

