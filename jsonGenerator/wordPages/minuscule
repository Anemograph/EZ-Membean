
<!DOCTYPE html>
<html>
<head>
<title>Word: minuscule | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abridge a book or play, you make it shorter by making cuts to the original.</p>
<p class='rw-defn idx1' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx2' style='display:none'>Attrition is the process of gradually decreasing the strength of something (such as an enemy force) by continually attacking it.</p>
<p class='rw-defn idx3' style='display:none'>To augment something is to increase its value or effectiveness by adding something to it.</p>
<p class='rw-defn idx4' style='display:none'>A cavernous space is very large and empty; it is both hollow and huge.</p>
<p class='rw-defn idx5' style='display:none'>A commodious room or house is large and roomy, which makes it convenient and highly suitable for living.</p>
<p class='rw-defn idx6' style='display:none'>A copious amount of something is a very large amount of it.</p>
<p class='rw-defn idx7' style='display:none'>When there is a dearth of something, there is a scarcity or lack of it.</p>
<p class='rw-defn idx8' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx9' style='display:none'>An exiguous amount of something is meager, small, or limited in nature.</p>
<p class='rw-defn idx10' style='display:none'>If something is formidable, it is impressive in size, power, or skill; therefore, it can make you feel frightened, alarmed, or in awe because it is so powerful or difficult to deal with.</p>
<p class='rw-defn idx11' style='display:none'>Something gargantuan is extremely large.</p>
<p class='rw-defn idx12' style='display:none'>A granule is a small particle or tiny grain of something.</p>
<p class='rw-defn idx13' style='display:none'>Something that is imperceptible is either impossible to be perceived by the mind or is very difficult to perceive.</p>
<p class='rw-defn idx14' style='display:none'>Something infinitesimal is so extremely small or minute that it is very hard to measure it.</p>
<p class='rw-defn idx15' style='display:none'>A leviathan is something that is very large, powerful, difficult to control, and rather frightening.</p>
<p class='rw-defn idx16' style='display:none'>Something luxuriant, such as plants or hair, is growing well and is very healthy.</p>
<p class='rw-defn idx17' style='display:none'>A macrocosm is a large, complex, and organized system or structure that is made of many small parts that form one unit, such as a society or the universe.</p>
<p class='rw-defn idx18' style='display:none'>A microcosm is a small group, place, or activity that has all the same qualities as a much larger one; therefore, it seems like a smaller version of it.</p>
<p class='rw-defn idx19' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx20' style='display:none'>Nominal can refer to someone who is in charge in name only, or it can refer to a very small amount of something; both are related by their relative insignificance.</p>
<p class='rw-defn idx21' style='display:none'>A panoramic view of a landscape is a sweeping or wide-ranging view of it.</p>
<p class='rw-defn idx22' style='display:none'>A paucity of something is not enough of it.</p>
<p class='rw-defn idx23' style='display:none'>A pithy statement or piece of writing is brief but intelligent; it is also precise and to the point.</p>
<p class='rw-defn idx24' style='display:none'>If you say that you&#8217;ve received a pittance, you mean that you received a small amount of something—and you know that you deserved more.</p>
<p class='rw-defn idx25' style='display:none'>A portent is a warning that indicates what is likely to happen in the future, which is usually something unpleasant.</p>
<p class='rw-defn idx26' style='display:none'>A preponderance of things of a particular type in a group means that there are more of that type than of any other.</p>
<p class='rw-defn idx27' style='display:none'>Something that is prodigious is very large, impressive, or great.</p>
<p class='rw-defn idx28' style='display:none'>When someone exhibits profundity, they display great intellectual depth and understanding; profundity can also be the depth or complexity of something.</p>
<p class='rw-defn idx29' style='display:none'>A scant amount of something is a very limited or slight amount of it; hence, it is inadequate or insufficient in size or quantity.</p>
<p class='rw-defn idx30' style='display:none'>Something that is voluminous is long, large, or vast in size.</p>
<p class='rw-defn idx31' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>minuscule</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='minuscule#' id='pronounce-sound' path='audio/words/amy-minuscule'></a>
MIN-uh-skyool
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='minuscule#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='minuscule#' id='context-sound' path='audio/wordcontexts/brian-minuscule'></a>
&#8220;Just a <em>minuscule</em> or tiny spoonful of sugar in my tea, please,&#8221; requested Mrs. Featherbit.  &#8220;I watch my health carefully and do not consume more than a slight, little, or <em>minuscule</em> portion of sweets.&#8221;  Watching her bird-like friend across the table, Mrs. Threadvatt (who was not even close to being physically small or <em>minuscule</em>) enjoyed a large bite of raspberry truffle in a silent response.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is something of which most people would prefer a <em>minuscule</em> amount?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Parking tickets.
</li>
<li class='choice '>
<span class='result'></span>
Job offers.
</li>
<li class='choice '>
<span class='result'></span>
Gold coins.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='minuscule#' id='definition-sound' path='audio/wordmeanings/amy-minuscule'></a>
Something <em>minuscule</em> is extremely small in size or amount.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>tiny</em>
</span>
</span>
</div>
<a class='quick-help' href='minuscule#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='minuscule#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/minuscule/memory_hooks/3640.json'></span>
<p>
<img alt="Minuscule" src="https://cdn1.membean.com/public/images/wordimages/hook/minuscule.jpg?qdep8" />
<span class="emp0"><span>The <span class="emp1"><span>M</span></span>inus<span class="emp1"><span>cule</span></span> <span class="emp1"><span>M</span></span>ole<span class="emp1"><span>cule</span></span></span></span> A <span class="emp1"><span>m</span></span>ole<span class="emp1"><span>cule</span></span> is so <span class="emp1"><span>m</span></span>inus<span class="emp1"><span>cule</span></span> that you can't even see it with a high-powered normal microscope.
</p>
</div>

<div id='memhook-button-bar'>
<a href="minuscule#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/minuscule/memory_hooks">Use other hook</a>
<a href="minuscule#" id="memhook-use-own" url="https://membean.com/mywords/minuscule/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='minuscule#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Iceland’s economy is <b>minuscule</b>, and its population is smaller than that of Pittsburgh, but its banks are deeply involved in global markets.
<cite class='attribution'>
&mdash;
ABC News
</cite>
</li>
<li>
In the midst of a 13 game losing streak heading into All-Star break, the Rockets decided it was time to start evaluating their youngsters for the future since they had a <b>minuscule</b> chance of competing for the postseason.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/minuscule/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='minuscule#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='min_lessen' data-tree-url='//cdn1.membean.com/public/data/treexml' href='minuscule#'>
<span class=''></span>
min
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>lessen, make smaller</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ul_little' data-tree-url='//cdn1.membean.com/public/data/treexml' href='minuscule#'>
<span class=''></span>
-ul-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>little, small</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='minuscule#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>Something <em>minuscule</em> is something &#8220;very small.&#8221;  Note that the &#8220;ul&#8221; acts to add emphasis, that is, something <em>minuscule</em> has not just been &#8220;made smaller,&#8221; but has been &#8220;made small&#8221; and then &#8220;smaller&#8221; still.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='minuscule#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Powers of Ten</strong><span> How minuscule can you go?</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/minuscule.jpg' video_url='examplevids/minuscule' video_width='350'></span>
<div id='wt-container'>
<img alt="Minuscule" height="288" src="https://cdn1.membean.com/video/examplevids/minuscule.jpg" width="350" />
<div class='center'>
<a href="minuscule#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='minuscule#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Minuscule" src="https://cdn0.membean.com/public/images/wordimages/cons2/minuscule.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='minuscule#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abridge</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>attrition</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>dearth</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>exiguous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>granule</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>imperceptible</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>infinitesimal</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>microcosm</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>nominal</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>paucity</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>pithy</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>pittance</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>scant</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>augment</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>cavernous</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>commodious</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>copious</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>formidable</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>gargantuan</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>leviathan</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>luxuriant</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>macrocosm</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>panoramic</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>portent</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>preponderance</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>prodigious</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>profundity</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>voluminous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="minuscule" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>minuscule</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="minuscule#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

