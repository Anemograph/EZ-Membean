
<!DOCTYPE html>
<html>
<head>
<title>Word: misanthrope | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abhor something, you dislike it very much, usually because you think it&#8217;s immoral.</p>
<p class='rw-defn idx1' style='display:none'>If you abominate something, you hate it because you think it is extremely wrong and unacceptable.</p>
<p class='rw-defn idx2' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx3' style='display:none'>If your behavior or manner is altruistic, you show you care more about other people and their interests than you care about yourself.</p>
<p class='rw-defn idx4' style='display:none'>If you have animus against someone, you have a strong feeling of dislike or hatred towards them, often without a good reason or based upon your personal prejudices.</p>
<p class='rw-defn idx5' style='display:none'>Antipathy is a strong feeling of dislike or hostility towards someone or something.</p>
<p class='rw-defn idx6' style='display:none'>An autonomous person makes their own decisions without being influenced by anyone else; an autonomous country or organization is independent and has the power to govern itself.</p>
<p class='rw-defn idx7' style='display:none'>Someone is baleful if they are filled with bad intent, anger, or hatred towards another person.</p>
<p class='rw-defn idx8' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx9' style='display:none'>Bigotry is the expression of strong and unreasonable opinions without accepting or tolerating opposing views.</p>
<p class='rw-defn idx10' style='display:none'>Chauvinism is unreasonable devotion to one&#8217;s country or the aggressive belief that one&#8217;s race or gender is superior to that of another.</p>
<p class='rw-defn idx11' style='display:none'>A chivalrous man behaves in a polite, kind, generous, and honorable way, especially towards women.</p>
<p class='rw-defn idx12' style='display:none'>If someone is granted clemency, they are punished less severely than they could have been—they have been shown mercy.</p>
<p class='rw-defn idx13' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx14' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx15' style='display:none'>A convivial atmosphere or occasion is friendly, pleasant, cheerful, and relaxed.</p>
<p class='rw-defn idx16' style='display:none'>A garrulous person talks a lot, especially about unimportant things.</p>
<p class='rw-defn idx17' style='display:none'>A genteel person is well-mannered, has good taste, and is very polite in social situations; a genteel person is often from an upper-class background.</p>
<p class='rw-defn idx18' style='display:none'>A gregarious person is friendly, highly social, and prefers being with people rather than being alone.</p>
<p class='rw-defn idx19' style='display:none'>If someone is insular, they are either unwilling to meet anyone outside their own small group or they are not interested in learning new ideas.</p>
<p class='rw-defn idx20' style='display:none'>An introvert is someone who primarily prefers being by themselves instead of hanging out with others socially; nevertheless, they still enjoy spending time with friends.</p>
<p class='rw-defn idx21' style='display:none'>If someone has a jaundiced view of something, they ignore the good parts and can only see the bad aspects of it.</p>
<p class='rw-defn idx22' style='display:none'>Someone who is loquacious talks too much.</p>
<p class='rw-defn idx23' style='display:none'>If someone exhibits magnanimity towards another, they show them kindness and noble generosity, especially after defeating them in battle or after having been treated badly by them.</p>
<p class='rw-defn idx24' style='display:none'>A malevolent person or thing is evil due to deliberate attempts to cause harm.</p>
<p class='rw-defn idx25' style='display:none'>Malice is a type of hatred that causes a strong desire to harm others both physically and emotionally.</p>
<p class='rw-defn idx26' style='display:none'>If you malign someone, you say unpleasant things about them to damage their reputation.</p>
<p class='rw-defn idx27' style='display:none'>A misogynist is someone who hates women or is highly critical about the female gender.</p>
<p class='rw-defn idx28' style='display:none'>A munificent person is extremely generous, especially with money.</p>
<p class='rw-defn idx29' style='display:none'>If you describe people or things as odious, you think that they are extremely unpleasant.</p>
<p class='rw-defn idx30' style='display:none'>Philanthropy is unselfish support in the form of donating money, work, or gifts to positive social purposes; philanthropy is also overall love for humans in general.</p>
<p class='rw-defn idx31' style='display:none'>A recluse is someone who chooses to live alone and deliberately avoids other people.</p>
<p class='rw-defn idx32' style='display:none'>Repugnance is a strong feeling of dislike for something or someone you find horrible and offensive.</p>
<p class='rw-defn idx33' style='display:none'>A troglodyte is reclusive, severely lacking in social skills, and is out of step with current times.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>misanthrope</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='misanthrope#' id='pronounce-sound' path='audio/words/amy-misanthrope'></a>
MIS-uhn-throhp
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='misanthrope#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='misanthrope#' id='context-sound' path='audio/wordcontexts/brian-misanthrope'></a>
Bernard was a true <em>misanthrope</em> who avoided social gatherings of every kind because he just did not like people.  His <em><em>misanthropic</em></em> nature led him to lead a solitary life since he just couldn&#8217;t stand being with people.  <em><em>Misanthropic</em></em> Bernard didn&#8217;t understand what purpose people served, and he remained doubtful that the human race even deserved to remain in existence.  Bernard the <em>misanthrope</em> was happy to spend evenings alone by the fire, reviewing in his mind all the ways in which he truly did not like humankind.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>misanthrope</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
One who feels clumsy at social gatherings.
</li>
<li class='choice answer '>
<span class='result'></span>
One who hates people in general.
</li>
<li class='choice '>
<span class='result'></span>
One who is angry or grumpy always.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='misanthrope#' id='definition-sound' path='audio/wordmeanings/amy-misanthrope'></a>
A <em>misanthrope</em> is someone who hates and mistrusts people.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>hater of humankind</em>
</span>
</span>
</div>
<a class='quick-help' href='misanthrope#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='misanthrope#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/misanthrope/memory_hooks/5750.json'></span>
<p>
<span class="emp0"><span>Socially <span class="emp1"><span>Mis</span></span>led <span class="emp2"><span>A</span></span>u<span class="emp2"><span>nt</span></span> Ties <span class="emp3"><span>Rope</span></span>s</span></span> I say that my socially <span class="emp1"><span>mis</span></span>led <span class="emp2"><span>A</span></span>u<span class="emp2"><span>nt</span></span> Mildred should tie knots in <span class="emp3"><span>rope</span></span>s for three years for being such a <span class="emp1"><span>mis</span></span>guided <span class="emp1"><span>mis</span></span><span class="emp2"><span>ant</span></span>h<span class="emp3"><span>rope</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="misanthrope#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/misanthrope/memory_hooks">Use other hook</a>
<a href="misanthrope#" id="memhook-use-own" url="https://membean.com/mywords/misanthrope/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='misanthrope#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Clearly, no one but a <b>misanthrope</b> wishes serious misfortune on another person.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
I'm not normally a <b>misanthrope</b> but the expanding virus has made me regard everyone as an inbound missile and super spreader.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
No <b>misanthrope</b> directed the lovely scene in which the servants, standing outside the drawing room, crane to hear snatches of the tune Novello sings to the distracted guests.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/misanthrope/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='misanthrope#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='mis_hatred' data-tree-url='//cdn1.membean.com/public/data/treexml' href='misanthrope#'>
<span class=''></span>
mis-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>hatred</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='anthrop_human' data-tree-url='//cdn1.membean.com/public/data/treexml' href='misanthrope#'>
<span class=''></span>
anthrop
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>human</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='misanthrope#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>A <em>misanthrope</em> possesses &#8220;hatred for humans.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='misanthrope#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Misanthrope" src="https://cdn0.membean.com/public/images/wordimages/cons2/misanthrope.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='misanthrope#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abhor</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abominate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>animus</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>antipathy</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>autonomous</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>baleful</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>bigotry</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>chauvinism</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>insular</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>introvert</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>jaundiced</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>malevolent</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>malice</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>malign</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>misogynist</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>odious</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>recluse</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>repugnance</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>troglodyte</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>altruistic</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>chivalrous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>clemency</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>convivial</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>garrulous</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>genteel</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>gregarious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>loquacious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>magnanimity</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>munificent</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>philanthropy</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="misanthrope" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>misanthrope</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="misanthrope#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

