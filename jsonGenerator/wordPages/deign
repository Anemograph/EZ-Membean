
<!DOCTYPE html>
<html>
<head>
<title>Word: deign | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abase yourself, people respect you less because you act in a way that is beneath you; due to this behavior, you are lowered or reduced in rank, esteem, or reputation. </p>
<p class='rw-defn idx1' style='display:none'>When you accede to a demand or proposal, you agree to it, especially after first disagreeing with it.</p>
<p class='rw-defn idx2' style='display:none'>If you acquiesce to something, you allow it to happen by doing what someone wants without putting up a struggle or voicing your own concerns.</p>
<p class='rw-defn idx3' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx4' style='display:none'>An autocratic person rules with complete power; consequently, they make decisions and give orders to people without asking them for their opinion or advice.</p>
<p class='rw-defn idx5' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx6' style='display:none'>A captious person has a fondness for catching others at fault; hence, they are overly critical and raise unwarranted objections too often.</p>
<p class='rw-defn idx7' style='display:none'>A cavalier person does not seem to care about rules, principles, or other people&#8217;s feelings, no matter how dire or serious a situation may be.</p>
<p class='rw-defn idx8' style='display:none'>To cavil is to make unnecessary complaints about things that are unimportant.</p>
<p class='rw-defn idx9' style='display:none'>Someone is considered cherubic when they appear angelic in the natural and slightly fat way of a small child or baby.</p>
<p class='rw-defn idx10' style='display:none'>If you commiserate with someone, you show them pity or sympathy because something bad or unpleasant has happened to them.</p>
<p class='rw-defn idx11' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx12' style='display:none'>When people condescend, they behave in ways that show that they are supposedly more important or intelligent than other people.</p>
<p class='rw-defn idx13' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx14' style='display:none'>A convivial atmosphere or occasion is friendly, pleasant, cheerful, and relaxed.</p>
<p class='rw-defn idx15' style='display:none'>If you behave with deference towards someone, you show them respect and accept their opinions or decisions, especially because they have an important position.</p>
<p class='rw-defn idx16' style='display:none'>If you demur, you delay in doing or mildly object to something because you don&#8217;t really want to do it.</p>
<p class='rw-defn idx17' style='display:none'>Empathy is the ability to understand how people feel because you can imagine what it is to be like them.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is imperious behaves in a proud, overbearing, and highly confident manner that shows they expect to be obeyed without question.</p>
<p class='rw-defn idx19' style='display:none'>If someone exhibits magnanimity towards another, they show them kindness and noble generosity, especially after defeating them in battle or after having been treated badly by them.</p>
<p class='rw-defn idx20' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx21' style='display:none'>If someone patronizes you, they talk or behave in a way that seems friendly; nevertheless, they also act as if they were more intelligent or important than you are.</p>
<p class='rw-defn idx22' style='display:none'>When someone pontificates, they give their opinions in a heavy-handed way that shows they think they are always right.</p>
<p class='rw-defn idx23' style='display:none'>When you are presumptuous, you act improperly, rudely, or without respect, especially while attempting to do something that is not socially acceptable or that you are not qualified to do.</p>
<p class='rw-defn idx24' style='display:none'>If you have qualms about doing something, you are having doubts about it because you are worried that it might be the wrong thing to do.</p>
<p class='rw-defn idx25' style='display:none'>Someone&#8217;s sardonic smile, expression, or comment shows a lack of respect for what someone else has said or done; this occurs because the former thinks they are too important to consider or discuss such a supposedly trivial matter of the latter.</p>
<p class='rw-defn idx26' style='display:none'>A scrupulous person takes great care to do everything in an extremely honest, conscientious, and fair manner.</p>
<p class='rw-defn idx27' style='display:none'>If you behave in a supercilious way, you act as if you were more important or better than everyone else.</p>
<p class='rw-defn idx28' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>deign</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='deign#' id='pronounce-sound' path='audio/words/amy-deign'></a>
dayn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='deign#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='deign#' id='context-sound' path='audio/wordcontexts/brian-deign'></a>
Even though she only wanted her close friends at her birthday party, Amber reluctantly <em>deigned</em> to invite the entire class although she really didn&#8217;t want them all there.  She <em>deigned</em> or humbled herself to invite so many guests because her parents forced her to do so.  Although Amber finally <em>deigned</em> and consented to include sixty people, she wouldn&#8217;t lower herself to greet them all at the door nor to say thank you to all of them on their way out.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What happens when someone <em>deigns</em> to do a task?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They think the task will benefit them somehow.
</li>
<li class='choice '>
<span class='result'></span>
They hope that they will be able to do the task in the future.
</li>
<li class='choice answer '>
<span class='result'></span>
They believe the task is below them but do it anyway.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='deign#' id='definition-sound' path='audio/wordmeanings/amy-deign'></a>
If someone <em>deigns</em> to do something, they agree to do it—but in a way that shows that they are lowering or humbling themselves to do so or that it is a very great favor.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>reluctantly agree</em>
</span>
</span>
</div>
<a class='quick-help' href='deign#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='deign#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/deign/memory_hooks/4579.json'></span>
<p>
<img alt="Deign" src="https://cdn3.membean.com/public/images/wordimages/hook/deign.jpg?qdep8" />
<span class="emp0"><span>Haughty For<span class="emp2"><span>eign</span></span> Queen D<span class="emp2"><span>eign</span></span>s to R<span class="emp2"><span>eign</span></span></span></span> "Yes, yes, I will d<span class="emp2"><span>eign</span></span> to r<span class="emp2"><span>eign</span></span> over these miserable and backward for<span class="emp2"><span>eign</span></span> peasants, but I will not allow myself to be seen by them!" snarled the haughty Queen.
</p>
</div>

<div id='memhook-button-bar'>
<a href="deign#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/deign/memory_hooks">Use other hook</a>
<a href="deign#" id="memhook-use-own" url="https://membean.com/mywords/deign/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='deign#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Other kids were too cool for school and would not <b>deign</b> to answer such a ridiculous question that was so far behind the times.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
I would lean over, groaning, lift the bowl of kibble, and give it a ceremonial shake. Then Gizmo would <b>deign</b> to eat. Maybe. Sometimes [the cat] would just sniff and turn away.
<cite class='attribution'>
&mdash;
Chicago Sun Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/deign/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='deign#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>From a root word meaning &#8220;worthy, deserving.&#8221;  If you <em>deign</em> to speak to another person, you consider that person to be &#8220;worthy&#8221; or &#8220;deserving&#8221; of your time&#8212;although most likely barely so.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='deign#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Deign" src="https://cdn3.membean.com/public/images/wordimages/cons2/deign.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='deign#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abase</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>accede</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>acquiesce</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>autocratic</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>captious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>cavalier</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>cavil</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>condescend</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>imperious</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>patronize</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>pontificate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>presumptuous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>sardonic</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>supercilious</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>cherubic</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>commiserate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>convivial</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>deference</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>demur</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>empathy</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>magnanimity</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>qualm</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>scrupulous</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="deign" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>deign</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="deign#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

