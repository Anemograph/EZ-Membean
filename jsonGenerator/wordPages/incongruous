
<!DOCTYPE html>
<html>
<head>
<title>Word: incongruous | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Incongruous-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/incongruous-large.jpg?qdep8" />
</div>
<a href="incongruous#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx1' style='display:none'>If one thing is analogous to another, a comparison can be made between the two because they are similar in some way.</p>
<p class='rw-defn idx2' style='display:none'>If something is an anomaly, it is different from what is usual or expected; hence, it is highly noticeable.</p>
<p class='rw-defn idx3' style='display:none'>Something that is apposite is relevant or suitable to what is happening or being discussed.</p>
<p class='rw-defn idx4' style='display:none'>Something is concomitant when it happens at the same time as something else and is connected with it in some fashion.</p>
<p class='rw-defn idx5' style='display:none'>A condign reward or punishment is deserved by and appropriate or fitting for the person who receives it.</p>
<p class='rw-defn idx6' style='display:none'>A confluence is a situation where two or more things meet or flow together at a single point or area; a confluence usually refers to two streams joining together.</p>
<p class='rw-defn idx7' style='display:none'>Contiguous things are in contact with or near each other; contiguous events happen one right after the other without a break.</p>
<p class='rw-defn idx8' style='display:none'>If there is a dichotomy between two things, there is a division of great difference or opposition between them.</p>
<p class='rw-defn idx9' style='display:none'>A situation or thing that is discordant does not fit in with other things; therefore, it is disagreeable, strange, or unpleasant.</p>
<p class='rw-defn idx10' style='display:none'>When there is a discrepancy between two sets of data, there is a difference or disagreement among them—despite the fact that they should be the same.</p>
<p class='rw-defn idx11' style='display:none'>Things that are disparate are clearly different from each other and belong to different groups or classes.</p>
<p class='rw-defn idx12' style='display:none'>When there is a disparity between two things, they are not of equal status; therefore, they are different or unlike in some way.</p>
<p class='rw-defn idx13' style='display:none'>A dissident is someone who disagrees publicly with a government, especially in a country where this is not allowed.</p>
<p class='rw-defn idx14' style='display:none'>Dissonance is an unpleasant situation of opposition in which ideas or actions are not in agreement or harmony; dissonance also refers to a harsh combination of sounds.</p>
<p class='rw-defn idx15' style='display:none'>Divergent opinions differ from each other; divergent paths move apart from one another.</p>
<p class='rw-defn idx16' style='display:none'>If someone is eccentric, they behave in a strange and unusual way that is different from most people.</p>
<p class='rw-defn idx17' style='display:none'>Empathy is the ability to understand how people feel because you can imagine what it is to be like them.</p>
<p class='rw-defn idx18' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx19' style='display:none'>A fallacy is an idea or belief that is false.</p>
<p class='rw-defn idx20' style='display:none'>Two items are fungible if one can be exchanged for the other with no loss in inherent value; for example, one $10 bill and two $5 bills are fungible.</p>
<p class='rw-defn idx21' style='display:none'>When two people are in a harmonious state, they are in agreement with each other; when a sound is harmonious, it is pleasant or agreeable to the ear.</p>
<p class='rw-defn idx22' style='display:none'>A heretic is someone who doubts or acts in opposition to commonly or generally accepted beliefs.</p>
<p class='rw-defn idx23' style='display:none'>The word homogeneous is used to describe a group that has members or parts which are similar or are all of the same type.</p>
<p class='rw-defn idx24' style='display:none'>One thing that is incommensurate with another is different in its level, size, or quality from the second; this may lead to an unfair situation.</p>
<p class='rw-defn idx25' style='display:none'>Two irreconcilable opinions or points of view are so opposed to each other that it is not possible to accept both of them or reach a settlement between them.</p>
<p class='rw-defn idx26' style='display:none'>The juxtaposition of two objects is the act of positioning them side by side so that the differences between them are more readily visible.</p>
<p class='rw-defn idx27' style='display:none'>If you describe something as ludicrous, you mean that it is extremely silly, stupid, or just plain ridiculous.</p>
<p class='rw-defn idx28' style='display:none'>A collection of things is motley if its parts are so different that they do not seem to belong in the same space; groups of widely variegated people can also be described as motley.</p>
<p class='rw-defn idx29' style='display:none'>A paradox is a statement that appears to be self-contradictory or unrealistic but may surprisingly express a possible truth.</p>
<p class='rw-defn idx30' style='display:none'>A schism causes a group or organization to divide into two groups as a result of differences in their aims and beliefs.</p>
<p class='rw-defn idx31' style='display:none'>If you say someone&#8217;s behavior is unseemly, you disapprove of it because it is not in good taste or not suitable for a particular situation.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>incongruous</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='incongruous#' id='pronounce-sound' path='audio/words/amy-incongruous'></a>
in-KONG-groo-uhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='incongruous#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='incongruous#' id='context-sound' path='audio/wordcontexts/brian-incongruous'></a>
Loretta suspected that something was bothering her husband Lyle when he made the <em>incongruous</em> or inappropriate purchase of a Ferrari.  As a predictable accountant, Lyle had never bought anything beyond the set budget, so this <em>incongruous</em>, shocking use of money they did not have simply did not fit.  Knowing that Lyle had recently celebrated his fiftieth birthday, Loretta thought that the <em>incongruous</em>, red Ferrari clashing next to their old, rusty Volvo might be a sign of either a crisis or a new beginning.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of something that is <em>incongruous</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A lot of rain on a day that is supposed to be sunny.
</li>
<li class='choice answer '>
<span class='result'></span>
A bright pink house on a street with all brown houses.
</li>
<li class='choice '>
<span class='result'></span>
A blue backpack that is left on a bus-stop bench all night.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='incongruous#' id='definition-sound' path='audio/wordmeanings/amy-incongruous'></a>
Something that is <em>incongruous</em> is strange when considered along with its group because it seems very different from the other things in its group.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>out of place</em>
</span>
</span>
</div>
<a class='quick-help' href='incongruous#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='incongruous#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/incongruous/memory_hooks/6017.json'></span>
<p>
<span class="emp0"><span>B<span class="emp1"><span>ongo</span></span> Drums <span class="emp2"><span>Ruin</span></span> <span class="emp3"><span>Us</span></span></span></span> Why did you play those b<span class="emp1"><span>ongo</span></span> drums when the head of the board was speaking during the meeting?  I know it was boring, but your <span class="emp2"><span>in</span></span>c<span class="emp1"><span>ong</span></span><span class="emp2"><span>ru</span></span>o<span class="emp3"><span>us</span></span> act of playing b<span class="emp1"><span>ongo</span></span> drums has completely <span class="emp2"><span>ruin</span></span>ed <span class="emp3"><span>us</span></span>!
</p>
</div>

<div id='memhook-button-bar'>
<a href="incongruous#" id="add-public-hook" style="" url="https://membean.com/mywords/incongruous/memory_hooks">Use other public hook</a>
<a href="incongruous#" id="memhook-use-own" url="https://membean.com/mywords/incongruous/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='incongruous#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
To me, Leon Russell’s performance represents the one <b>incongruous</b> note in the program. . . . With the exception of Russell, nobody did a piece from their live sets in most instances because the artist doesn’t do regular live performances.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
<li>
Employees' emotional health, a topic that once seemed <b>incongruous</b> with the survival-of-the-fittest corporate arena, is getting attention as a real bottom-line issue.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
[T]he political system that General Musharraf devised to keep himself in power has all but collapsed. An <b>incongruous</b> hybrid of democracy and dictatorship, it resembled the cartoons of Heath Robinson, who drew preposterous assemblages of levers, cranks and pulleys, kept running by the tinkering of small bald men in spectacles.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
The new Palm boasts a prime location: right across from Staples Center, in a grand edifice with soaring ceilings and gold-tipped columns. . . . The walls, of course, are plastered with the Palm’s signature caricatures, though they're rather <b>incongruous</b> in this elegant setting and spaced farther apart than at any of the other Palms I've visited.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/incongruous/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='incongruous#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='in_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='incongruous#'>
<span class='common'></span>
in-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='congru_come' data-tree-url='//cdn1.membean.com/public/data/treexml' href='incongruous#'>
<span class=''></span>
congru
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>come together, agree</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ous_possessing' data-tree-url='//cdn1.membean.com/public/data/treexml' href='incongruous#'>
<span class=''></span>
-ous
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>possessing the nature of</td>
</tr>
</table>
<p>Anything <em>incongruous</em> does &#8220;not agree&#8221; with those things surrounding it.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='incongruous#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Go On</strong><span> The bride-to-be feels that the wedding is incongruous to who she really is.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/incongruous.jpg' video_url='examplevids/incongruous' video_width='350'></span>
<div id='wt-container'>
<img alt="Incongruous" height="288" src="https://cdn1.membean.com/video/examplevids/incongruous.jpg" width="350" />
<div class='center'>
<a href="incongruous#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='incongruous#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Incongruous" src="https://cdn1.membean.com/public/images/wordimages/cons2/incongruous.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='incongruous#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>anomaly</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>dichotomy</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>discordant</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>discrepancy</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>disparate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>disparity</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>dissident</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>dissonance</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>divergent</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>eccentric</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>fallacy</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>heretic</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>incommensurate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>irreconcilable</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>ludicrous</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>motley</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>paradox</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>schism</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>unseemly</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>analogous</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>apposite</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>concomitant</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>condign</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>confluence</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>contiguous</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>empathy</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>fungible</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>harmonious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>homogeneous</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>juxtaposition</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='incongruous#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
incongruity
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>something that is strange because it seems different than other things</td>
</tr>
<tr>
<td class='wordform'>
<span>
congruent
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>agreeable; identical</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="incongruous" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>incongruous</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="incongruous#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

