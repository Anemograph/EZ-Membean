
<!DOCTYPE html>
<html>
<head>
<title>Word: assay | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Assay-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/assay-large.jpg?qdep8" />
</div>
<a href="assay#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>If you adjudicate a competition or dispute, you officially decide who is right or what should be done concerning any difficulties that may arise.</p>
<p class='rw-defn idx1' style='display:none'>When someone offers an appraisal of something, they indicate the value or worth of it.</p>
<p class='rw-defn idx2' style='display:none'>Cogitating about something is thinking deeply about it for a long time.</p>
<p class='rw-defn idx3' style='display:none'>To confute an argument is to prove it to be thoroughly false; to confute a person is to prove them to be wrong.</p>
<p class='rw-defn idx4' style='display:none'>If you deliberate, you think about or discuss something very carefully, especially before making an important decision.</p>
<p class='rw-defn idx5' style='display:none'>If you delineate something, such as an idea or situation or border, you describe it in great detail.</p>
<p class='rw-defn idx6' style='display:none'>If you demur, you delay in doing or mildly object to something because you don&#8217;t really want to do it.</p>
<p class='rw-defn idx7' style='display:none'>If you fathom something complicated or mysterious, you are able to understand it.</p>
<p class='rw-defn idx8' style='display:none'>If an idea or thought is incisive, it is expressed in a penetrating and knowledgeable manner that is clear and brief; additionally, it can demonstrate impressive understanding of related ideas or thoughts.</p>
<p class='rw-defn idx9' style='display:none'>Inductive reasoning involves the observation of data to arrive at a general conclusion or principle.</p>
<p class='rw-defn idx10' style='display:none'>An inference is a conclusion that is reached using available data.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is irresolute is unable to decide what to do.</p>
<p class='rw-defn idx12' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx13' style='display:none'>To be punctilious is to pay precise attention to detail.</p>
<p class='rw-defn idx14' style='display:none'>If you ruminate on something, you think carefully and deeply about it over a long period of time.</p>
<p class='rw-defn idx15' style='display:none'>A scrupulous person takes great care to do everything in an extremely honest, conscientious, and fair manner.</p>
<p class='rw-defn idx16' style='display:none'>To be tentative is to be hesitant or uncertain about something; an agreement or decision of this kind is likely to have changes before it reaches its final form.</p>
<p class='rw-defn idx17' style='display:none'>A touchstone is the standard or best example by which the value or quality of something else is judged.</p>
<p class='rw-defn idx18' style='display:none'>If you waver, you cannot decide between two things because you have serious doubts about which choice is better.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>assay</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='assay#' id='pronounce-sound' path='audio/words/amy-assay'></a>
AS-ay
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='assay#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='assay#' id='context-sound' path='audio/wordcontexts/brian-assay'></a>
At his lab, Todd worked to <em>assay</em> or analyze the compounds within the unknown substance that had been left in his mailbox.  Using the lab&#8217;s cutting-edge technology, Todd was able to <em>assay</em> or test the chemical components of the liquid in the glass container.  By <em>assaying</em> and examining the elements within the mystery substance, Todd was relieved to find out that it was not dangerous.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Why would someone <em>assay</em> something?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
To explain its value to others.
</li>
<li class='choice '>
<span class='result'></span>
To disguise its true purpose.
</li>
<li class='choice answer '>
<span class='result'></span>
To determine its parts and ingredients.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='assay#' id='definition-sound' path='audio/wordmeanings/amy-assay'></a>
To <em>assay</em> something, such as a substance, is to analyze the chemicals present in it; it can also refer to putting something to the test.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>analyze</em>
</span>
</span>
</div>
<a class='quick-help' href='assay#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='assay#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/assay/memory_hooks/3662.json'></span>
<p>
<span class="emp0"><span>Donkey Farmer Bewildered by Br<span class="emp2"><span>ay</span></span>ing Jack<span class="emp1"><span>ass</span></span></span></span>  Don the donkey farmer hired a vet to <span class="emp1"><span>ass</span></span><span class="emp2"><span>ay</span></span> why his jack<span class="emp1"><span>ass</span></span> br<span class="emp2"><span>ay</span></span>ed all night, which he had never done before; the <span class="emp1"><span>ass</span></span> <span class="emp1"><span>a</span></span>naly<span class="emp1"><span>s</span></span>i<span class="emp1"><span>s</span></span> was inconclusive.
</p>
</div>

<div id='memhook-button-bar'>
<a href="assay#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/assay/memory_hooks">Use other hook</a>
<a href="assay#" id="memhook-use-own" url="https://membean.com/mywords/assay/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='assay#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
[A rent-to-own contract] could also appeal to those who, prior to buying, prefer to <b>assay</b> living in a neighborhood, which offers few other rental possibilities.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Alzheimer's disease can be tough to diagnose, especially early on. Doctors can order brain scans and <b>assay</b> spinal fluids. But existing tests are imperfect and some can be invasive.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Clients pay a fee and spit into a vial to provide a few cells. The [genetic-testing] firms <b>assay</b> the DNA in those cells, looking for millions of tiny hereditary variations.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/assay/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='assay#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>Via the root word <em>essayer</em>: to try.</p>
<a href="assay#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> French</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='assay#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: EdChatTM TV</strong><span> Forensic scientists assay evidence to prove or disprove a case.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/assay.jpg' video_url='examplevids/assay' video_width='350'></span>
<div id='wt-container'>
<img alt="Assay" height="288" src="https://cdn1.membean.com/video/examplevids/assay.jpg" width="350" />
<div class='center'>
<a href="assay#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='assay#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Assay" src="https://cdn0.membean.com/public/images/wordimages/cons2/assay.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='assay#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adjudicate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>appraisal</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>cogitate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>deliberate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>delineate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>fathom</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>incisive</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>inductive</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>inference</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>punctilious</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>ruminate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>scrupulous</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>touchstone</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>confute</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>demur</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>irresolute</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>tentative</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>waver</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='assay#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
assay
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>an analysis of a substance</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="assay" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>assay</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="assay#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

