
<!DOCTYPE html>
<html>
<head>
<title>Word: whet | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>Someone who is abstemious avoids doing too much of something enjoyable, such as eating or drinking; rather, they consume in a moderate fashion.</p>
<p class='rw-defn idx2' style='display:none'>Abstinence is the practice of keeping away from or avoiding something you enjoy—such as the physical pleasures of excessive food and drink—usually for health or religious reasons.</p>
<p class='rw-defn idx3' style='display:none'>You allay someone&#8217;s fear or concern by making them feel less afraid or worried.</p>
<p class='rw-defn idx4' style='display:none'>If you alleviate pain or suffering, you make it less intense or severe.</p>
<p class='rw-defn idx5' style='display:none'>When you assuage an unpleasant feeling, you make it less painful or severe, thereby calming it.</p>
<p class='rw-defn idx6' style='display:none'>To augment something is to increase its value or effectiveness by adding something to it.</p>
<p class='rw-defn idx7' style='display:none'>A catalyst is an agent that enacts change, such as speeding up a chemical reaction or causing an event to occur.</p>
<p class='rw-defn idx8' style='display:none'>If you covet something that someone else has, you have a strong desire to have it for yourself.</p>
<p class='rw-defn idx9' style='display:none'>If something, such as a sound, is undergoing a crescendo, it is getting louder and louder or increasing in intensity and is about to reach its peak strength.</p>
<p class='rw-defn idx10' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx11' style='display:none'>To foment is to encourage people to protest, fight, or cause trouble and violent opposition to something that is viewed by some as undesirable.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is frugal spends very little money—and even then only on things that are absolutely necessary.</p>
<p class='rw-defn idx13' style='display:none'>When you galvanize someone, you cause them to improve a situation, solve a problem, or take some other action by making them feel excited, afraid, or angry.</p>
<p class='rw-defn idx14' style='display:none'>Something that inhibits you from doing something restricts or keeps you from doing it.</p>
<p class='rw-defn idx15' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx16' style='display:none'>A parsimonious person is not willing to give or spend money.</p>
<p class='rw-defn idx17' style='display:none'>Something that is piquant is interesting and exciting; food that is piquant is pleasantly spicy.</p>
<p class='rw-defn idx18' style='display:none'>To quell something is to stamp out, quiet, or overcome it.</p>
<p class='rw-defn idx19' style='display:none'>If you describe a person&#8217;s behavior as rapacious, you disapprove of them because they always want more money, goods, or possessions than they really need.</p>
<p class='rw-defn idx20' style='display:none'>If you are ravenous, you are extremely hungry.</p>
<p class='rw-defn idx21' style='display:none'>If someone regales you, they tell you stories and jokes to entertain you— and they could also serve you a wonderful feast.</p>
<p class='rw-defn idx22' style='display:none'>If something, such as food or drink, satiates you, it satisfies your need or desire so completely that you often feel that you have had too much.</p>
<p class='rw-defn idx23' style='display:none'>If you have a surfeit of something, you have much more than what you need.</p>
<p class='rw-defn idx24' style='display:none'>When you thwart someone&#8217;s ambitions, you obstruct or stop them from happening.</p>
<p class='rw-defn idx25' style='display:none'>A voracious person has a strong desire to want a lot of something, especially food.</p>
<p class='rw-defn idx26' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>whet</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='whet#' id='pronounce-sound' path='audio/words/amy-whet'></a>
hwet
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='whet#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='whet#' id='context-sound' path='audio/wordcontexts/brian-whet'></a>
Reading the review of this new restaurant <em>whetted</em> our appetites, as the reviewer made every dish sound simply delicious.  Our interest was <em>whetted</em> by the fact that reservations were not available for three months from the opening date, which clearly aroused our curiosity.  Apparently the chef is so good and creates such unusual dishes that he has <em>whetted</em>, stirred, and awakened the appetite of every diner in the city.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of something that <em>whets</em> your appetite?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
The first taste of a delicious dish that increases your desire for it.
</li>
<li class='choice '>
<span class='result'></span>
The foul smell of something that discourages you from eating anything.
</li>
<li class='choice '>
<span class='result'></span>
The last bites of large meal that makes you feel satisfied and rather full.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='whet#' id='definition-sound' path='audio/wordmeanings/amy-whet'></a>
When something <em>whets</em> your appetite, it increases your desire for it, especially by giving you a little idea or pleasing hint of what it is like.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>stimulate</em>
</span>
</span>
</div>
<a class='quick-help' href='whet#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='whet#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/whet/memory_hooks/3187.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Wet</span></span> Clothes <span class="emp3"><span>W</span></span><span class="emp1"><span>h</span></span><span class="emp3"><span>et</span></span><span class="emp1"><span>s</span></span> <span class="emp1"><span>H</span></span>i<span class="emp1"><span>s</span></span> Fire Desire</span></span>  If Greg gets <span class="emp3"><span>wet</span></span> clothes in the winter, it <span class="emp3"><span>w</span></span><span class="emp1"><span>h</span></span><span class="emp3"><span>et</span></span><span class="emp1"><span>s</span></span> <span class="emp1"><span>h</span></span>i<span class="emp1"><span>s</span></span> desire for fire to warm and dry them.
</p>
</div>

<div id='memhook-button-bar'>
<a href="whet#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/whet/memory_hooks">Use other hook</a>
<a href="whet#" id="memhook-use-own" url="https://membean.com/mywords/whet/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='whet#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
That only serves to <b>whet</b> the racing appetites of fans in the heavily populated areas of southern Michigan, Ontario, northern Ohio, Indiana and Illinois.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
That clip is embedded at the bottom of this page in order for you to judge the quality of the video, as well as <b>whet</b> your appetite.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
The underlying groove is impossibly funky, and the jazz blowing on top of it should be enough to <b>whet</b> any R&B fan’s appetite for more.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
The pan had originally cost a nickel, the stranger offered a quarter—and that exchange was enough to <b>whet</b> Mr. Morrison’s entrepreneurial appetite.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/whet/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='whet#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>From a root word meaning &#8220;to incite, urge on.&#8221;</p>
<a href="whet#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> Old English</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='whet#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>The Spy Who Loved Me</strong><span> The useless information was only meant to whet their appetites, not satisfy them.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/whet.jpg' video_url='examplevids/whet' video_width='350'></span>
<div id='wt-container'>
<img alt="Whet" height="288" src="https://cdn1.membean.com/video/examplevids/whet.jpg" width="350" />
<div class='center'>
<a href="whet#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='whet#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Whet" src="https://cdn1.membean.com/public/images/wordimages/cons2/whet.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='whet#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='6' class = 'rw-wordform notlearned'><span>augment</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>catalyst</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>covet</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>crescendo</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>foment</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>galvanize</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>piquant</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>rapacious</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>ravenous</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>regale</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>voracious</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abstemious</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abstinence</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>allay</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>alleviate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>assuage</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>frugal</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>inhibit</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>parsimonious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>quell</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>satiate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>surfeit</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>thwart</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="whet" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>whet</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="whet#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

