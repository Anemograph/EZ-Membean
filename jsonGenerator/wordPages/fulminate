
<!DOCTYPE html>
<html>
<head>
<title>Word: fulminate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Approbation is official praise or approval of something.</p>
<p class='rw-defn idx1' style='display:none'>Someone who has a bilious personality is highly irritable and bad-tempered.</p>
<p class='rw-defn idx2' style='display:none'>A captious person has a fondness for catching others at fault; hence, they are overly critical and raise unwarranted objections too often.</p>
<p class='rw-defn idx3' style='display:none'>To cavil is to make unnecessary complaints about things that are unimportant.</p>
<p class='rw-defn idx4' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx5' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx6' style='display:none'>If someone will countenance something, they will approve, tolerate, or support it.</p>
<p class='rw-defn idx7' style='display:none'>If you behave with deference towards someone, you show them respect and accept their opinions or decisions, especially because they have an important position.</p>
<p class='rw-defn idx8' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx9' style='display:none'>If you denounce people or actions, you criticize them severely in public because you feel strongly that they are wrong or evil.</p>
<p class='rw-defn idx10' style='display:none'>A diatribe is a speech or piece of writing that angrily attacks someone&#8217;s ideas or activities at length.</p>
<p class='rw-defn idx11' style='display:none'>A eulogy is a speech or other piece of writing, often part of a funeral, in which someone or something is highly praised.</p>
<p class='rw-defn idx12' style='display:none'>If you extol something or someone, you praise it or them very enthusiastically.</p>
<p class='rw-defn idx13' style='display:none'>A harangue is a long, scolding speech.</p>
<p class='rw-defn idx14' style='display:none'>If you inveigh against something, you criticize it very strongly.</p>
<p class='rw-defn idx15' style='display:none'>An irascible person becomes angry very easily.</p>
<p class='rw-defn idx16' style='display:none'>A laudatory article or speech expresses praise or admiration for someone or something.</p>
<p class='rw-defn idx17' style='display:none'>When someone is lionized, they are treated as being very important or famous—although they may not deserve to be.</p>
<p class='rw-defn idx18' style='display:none'>A paean is a piece of writing, verbal expression, or song that expresses enthusiastic praise, admiration, or joy.</p>
<p class='rw-defn idx19' style='display:none'>If someone is <em>pilloried</em>, they are publicly criticized or ridiculed by a lot of people, especially in the media.</p>
<p class='rw-defn idx20' style='display:none'>If you receive a plaudit, you receive admiration, praise, and approval from someone.</p>
<p class='rw-defn idx21' style='display:none'>To rant is to go on an angry verbal attack.</p>
<p class='rw-defn idx22' style='display:none'>A tempestuous storm, temper, or crowd is violent, wild, and in an uproar.</p>
<p class='rw-defn idx23' style='display:none'>A tirade is a prolonged, verbal outburst that severely criticizes someone or something.</p>
<p class='rw-defn idx24' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx25' style='display:none'>To come out of something unscathed is to come out of it uninjured.</p>
<p class='rw-defn idx26' style='display:none'>Venerable people command respect because they are old and wise.</p>
<p class='rw-defn idx27' style='display:none'>If you vilify people, you write or say bad things about them in an attempt to make them unpopular.</p>
<p class='rw-defn idx28' style='display:none'>Vituperative remarks are full of hate, anger, and cruel criticism.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>fulminate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='fulminate#' id='pronounce-sound' path='audio/words/amy-fulminate'></a>
FUHL-muh-nayt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='fulminate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='fulminate#' id='context-sound' path='audio/wordcontexts/brian-fulminate'></a>
The group of angry citizens wrote a letter to the editor, <em>fulminating</em> and harshly complaining about the mayor&#8217;s current policies.  This detailed, critical <em><em>fulminating</em></em> blamed the mayor for the economic downturn in the area, which had made the writers extremely upset.  When the mayor heard about the people who had <em>fulminated</em> and protested against him, he claimed that it simply went with the political territory.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If someone is <em>fulminating</em>, what are they doing?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Attacking another person verbally.
</li>
<li class='choice '>
<span class='result'></span>
Running against someone in a political contest.
</li>
<li class='choice '>
<span class='result'></span>
Debating seriously with another person.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='fulminate#' id='definition-sound' path='audio/wordmeanings/amy-fulminate'></a>
If you <em>fulminate</em> against someone, you speak or write angrily and with heavy criticism about them.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>heavily criticize</em>
</span>
</span>
</div>
<a class='quick-help' href='fulminate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='fulminate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/fulminate/memory_hooks/5186.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Ful</span></span>l <span class="emp3"><span>Min</span></span>o<span class="emp3"><span>t</span></span>aur</span></span> Gabriel's <span class="emp1"><span>ful</span></span><span class="emp3"><span>min</span></span>a<span class="emp3"><span>t</span></span>ing against the unjust criminal was like a verbal <span class="emp1"><span>ful</span></span>l head-butting by the <span class="emp3"><span>Min</span></span>o<span class="emp3"><span>t</span></span>aur, the horrifying and powerful half-man, half-bull of Greek mythology.
</p>
</div>

<div id='memhook-button-bar'>
<a href="fulminate#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/fulminate/memory_hooks">Use other hook</a>
<a href="fulminate#" id="memhook-use-own" url="https://membean.com/mywords/fulminate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='fulminate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
The hiss of the quenched element, the breakage of the pitcher which I had flung from my hand when I had emptied it, and, above all, the splash of the shower-bath I had liberally bestowed, roused Mr. Rochester at last though it was dark, I knew he was awake; because I heard him <b>fulminating</b> strange anathemas at finding himself lying in a pool of water. "Is there a flood?" he cried.
<span class='attribution'>&mdash; Charlotte Brontë, 19th century English novelist, from _Jane Eyre_</span>
<img alt="Charlotte brontë, 19th century english novelist, from _jane eyre_" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Charlotte Brontë, 19th century English novelist, from _Jane Eyre_.jpg?qdep8" width="80" />
</li>
<li>
Twelve years we’ve done this silly thing. Twelve years, a bunch of works of fiction are read, compared, discussed. Twelve years, judges judge, commentators comment, readers cry, scream, rage, celebrate, <b>fulminate</b>, call each other names and worse things, and send us hate mail. Twelve years, people also come up to us in April and tell us it was the most fun they’d had all year long, at least when it came to contemporary fiction.
<cite class='attribution'>
&mdash;
The Morning News
</cite>
</li>
<li>
"Politicians will <b>fulminate</b> about things, but it’s the market that sets the price," said John Felmy, chief economist of the American Petroleum Institute.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
What is clearly needed is the establishment of an independent expert agency or commission in the U.S., at the federal level, that has statutory authority to create and enforce rules pertaining to the handling of consumer data by social media platforms. Without such an authority these companies and their activities . . . will remain in a blissful limbo, picking and choosing by which rules to abide and against which to <b>fulminate</b> and lobby.
<cite class='attribution'>
&mdash;
TechCrunch
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/fulminate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='fulminate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>fulmin</td>
<td>
&rarr;
</td>
<td class='meaning'>lightning that strikes</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ate_quality' data-tree-url='//cdn1.membean.com/public/data/treexml' href='fulminate#'>
<span class=''></span>
-ate
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make something have a certain quality</td>
</tr>
</table>
<p>When one <em>fulminates</em> against another, one verbally &#8220;strikes&#8221; that person &#8220;like lightning.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='fulminate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Fulminate" src="https://cdn3.membean.com/public/images/wordimages/cons2/fulminate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='fulminate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>bilious</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>captious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>cavil</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>denounce</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>diatribe</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>harangue</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>inveigh</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>irascible</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>pillory</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>rant</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>tempestuous</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>tirade</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>vilify</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>vituperative</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>approbation</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>countenance</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>deference</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>eulogy</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>extol</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>laudatory</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>lionize</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>paean</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>plaudit</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>unscathed</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>venerable</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="fulminate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>fulminate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="fulminate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

