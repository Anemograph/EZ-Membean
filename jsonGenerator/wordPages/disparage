
<!DOCTYPE html>
<html>
<head>
<title>Word: disparage | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx1' style='display:none'>When something bedevils you, it causes you a lot of problems and difficulties over a period of time.</p>
<p class='rw-defn idx2' style='display:none'>When you beleaguer someone, you act with the intent to annoy or harass that person repeatedly until they finally give you what you want.</p>
<p class='rw-defn idx3' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx4' style='display:none'>If you are beset by something, you are experiencing serious problems or dangers because of it.</p>
<p class='rw-defn idx5' style='display:none'>If you commiserate with someone, you show them pity or sympathy because something bad or unpleasant has happened to them.</p>
<p class='rw-defn idx6' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx7' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx8' style='display:none'>To decry something is to speak against it and find fault with it.</p>
<p class='rw-defn idx9' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx10' style='display:none'>If you deprecate something, you disapprove of it strongly.</p>
<p class='rw-defn idx11' style='display:none'>If you treat someone with derision, you mock or make fun of them because you think they are stupid, unimportant, or useless.</p>
<p class='rw-defn idx12' style='display:none'>A derogatory statement about another person is highly offensive, critical, uncomplimentary, and often insulting.</p>
<p class='rw-defn idx13' style='display:none'>You show disdain towards another person when you despise what they do, or you regard them as unworthy of your notice and attention.</p>
<p class='rw-defn idx14' style='display:none'>A eulogy is a speech or other piece of writing, often part of a funeral, in which someone or something is highly praised.</p>
<p class='rw-defn idx15' style='display:none'>When you exalt another person, you either praise them highly or promote them to a higher position in an organization.</p>
<p class='rw-defn idx16' style='display:none'>If you exhort someone to do something, you try very hard to persuade them to do it.</p>
<p class='rw-defn idx17' style='display:none'>If you extol something or someone, you praise it or them very enthusiastically.</p>
<p class='rw-defn idx18' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx19' style='display:none'>If you hurl invective at another person, you are verbally abusing or highly criticizing them.</p>
<p class='rw-defn idx20' style='display:none'>A laudatory article or speech expresses praise or admiration for someone or something.</p>
<p class='rw-defn idx21' style='display:none'>When someone is lionized, they are treated as being very important or famous—although they may not deserve to be.</p>
<p class='rw-defn idx22' style='display:none'>If you malign someone, you say unpleasant things about them to damage their reputation.</p>
<p class='rw-defn idx23' style='display:none'>If someone is <em>pilloried</em>, they are publicly criticized or ridiculed by a lot of people, especially in the media.</p>
<p class='rw-defn idx24' style='display:none'>If you receive a plaudit, you receive admiration, praise, and approval from someone.</p>
<p class='rw-defn idx25' style='display:none'>Something that is propitious shows favorable conditions.</p>
<p class='rw-defn idx26' style='display:none'>Someone who is sanctimonious endeavors to show that they are morally superior to others.</p>
<p class='rw-defn idx27' style='display:none'>Someone&#8217;s sardonic smile, expression, or comment shows a lack of respect for what someone else has said or done; this occurs because the former thinks they are too important to consider or discuss such a supposedly trivial matter of the latter.</p>
<p class='rw-defn idx28' style='display:none'>To taint is to give an undesirable quality that damages a person&#8217;s reputation or otherwise spoils something.</p>
<p class='rw-defn idx29' style='display:none'>If you vilify people, you write or say bad things about them in an attempt to make them unpopular.</p>
<p class='rw-defn idx30' style='display:none'>Vituperative remarks are full of hate, anger, and cruel criticism.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>disparage</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='disparage#' id='pronounce-sound' path='audio/words/amy-disparage'></a>
di-SPAR-ij
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='disparage#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='disparage#' id='context-sound' path='audio/wordcontexts/brian-disparage'></a>
After Mildred ended the call with her son Jim, she began to loudly complain to her husband, <em><em>disparaging</em></em> and criticizing their son&#8217;s neglect and lack of focus.  &#8220;Jim is nothing like us,&#8221; Mildred said <em><em>disparagingly</em></em> and abusively, &#8220;for he does nothing all day and doesn&#8217;t even call to say hello!&#8221;  Since her husband was used to similar <em><em>disparaging</em></em> and negative words, he just nodded and shrugged.  &#8220;How such a lazy and careless young man could be in our family, I just don&#8217;t know,&#8221; Mildred whined as she <em>disparaged</em> and put down her only son.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of someone&#8217;s <em>disparaging</em> something?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A man who cuts down his neighbor&#8217;s tree when the latter is on vacation.
</li>
<li class='choice answer '>
<span class='result'></span>
A book reviewer who strongly criticizes an author&#8217;s newest book.
</li>
<li class='choice '>
<span class='result'></span>
An actor who purposely thanks their agent first after winning an award.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='disparage#' id='definition-sound' path='audio/wordmeanings/amy-disparage'></a>
If you <em>disparage</em> someone or something, you say unpleasant words that show you have no respect for that person or thing.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>criticize</em>
</span>
</span>
</div>
<a class='quick-help' href='disparage#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='disparage#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/disparage/memory_hooks/4019.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Age</span></span> Not <span class="emp3"><span>Spar</span></span>ed</span></span> "Just because Marge is <span class="emp2"><span>age</span></span>d doesn't give you the right to di<span class="emp3"><span>spar</span></span><span class="emp2"><span>age</span></span> her ... <span class="emp3"><span>spar</span></span>e Marge the di<span class="emp3"><span>spar</span></span><span class="emp2"><span>aging</span></span> comments about her <span class="emp2"><span>aging</span></span>!"
</p>
</div>

<div id='memhook-button-bar'>
<a href="disparage#" id="add-public-hook" style="" url="https://membean.com/mywords/disparage/memory_hooks">Use other public hook</a>
<a href="disparage#" id="memhook-use-own" url="https://membean.com/mywords/disparage/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='disparage#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Democracy has fallen on hard times in Taiwan, and it's been a long while since its citizens felt good about their government. . . . Both major political parties appear mired in the special interests that <b>disparage</b> openness and accountability and bend the rules of fairness and due process.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
Lyons said, "Coach Pruitt and I look forward to defending any allegation that he has engaged in any NCAA wrongdoing, as well as examining the University's intent to <b>disparage</b> and destroy Coach Pruitt's reputation in an effort to avoid paying his contractual liquidated damages."
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
After an outcry from advertisers in 2017, YouTube banned ads from appearing alongside content that promotes hate or discrimination or <b>disparages</b> protected groups.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
These are the people, the authors believe, who <b>disparage</b> others to feel superior, perhaps stemming back to a childhood in which they themselves were laughed at by their peers.
<cite class='attribution'>
&mdash;
Psychology Today
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/disparage/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='disparage#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='dis_apart' data-tree-url='//cdn1.membean.com/public/data/treexml' href='disparage#'>
<span class='common'></span>
dis-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>apart, not, away from</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='par_equal' data-tree-url='//cdn1.membean.com/public/data/treexml' href='disparage#'>
<span class=''></span>
par
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>equal</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='age_do' data-tree-url='//cdn1.membean.com/public/data/treexml' href='disparage#'>
<span class=''></span>
-age
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state</td>
</tr>
</table>
<p>If you <em>disparage</em> another, you suggest that that person has the &#8220;state of not being equal&#8221; to you.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='disparage#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Red Letter Media</strong><span> Disparaging one of the Star Wars films.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/disparage.jpg' video_url='examplevids/disparage' video_width='350'></span>
<div id='wt-container'>
<img alt="Disparage" height="288" src="https://cdn1.membean.com/video/examplevids/disparage.jpg" width="350" />
<div class='center'>
<a href="disparage#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='disparage#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Disparage" src="https://cdn3.membean.com/public/images/wordimages/cons2/disparage.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='disparage#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>bedevil</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>beleaguer</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>beset</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>decry</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>deprecate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>derision</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>derogatory</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>disdain</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>invective</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>malign</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>pillory</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>sanctimonious</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>sardonic</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>taint</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>vilify</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>vituperative</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>commiserate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>eulogy</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>exalt</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>exhort</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>extol</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>laudatory</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>lionize</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>plaudit</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>propitious</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="disparage" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>disparage</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="disparage#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

