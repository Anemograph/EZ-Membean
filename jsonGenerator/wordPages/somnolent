
<!DOCTYPE html>
<html>
<head>
<title>Word: somnolent | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Apathy is a lack of interest or unenthusiastic involvement in an activity; there is no effort to change or improve it at all.</p>
<p class='rw-defn idx1' style='display:none'>Something that is arduous is extremely difficult; hence, it involves a lot of effort.</p>
<p class='rw-defn idx2' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx3' style='display:none'>Cogitating about something is thinking deeply about it for a long time.</p>
<p class='rw-defn idx4' style='display:none'>A convivial atmosphere or occasion is friendly, pleasant, cheerful, and relaxed.</p>
<p class='rw-defn idx5' style='display:none'>When something is dormant, it is in a state of sleep or temporarily not active.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx7' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx8' style='display:none'>If something enervates you, it makes you very tired and weak—almost to the point of collapse.</p>
<p class='rw-defn idx9' style='display:none'>Ennui is the feeling of being bored, tired, and dissatisfied with life.</p>
<p class='rw-defn idx10' style='display:none'>If something enthralls you, it makes you so interested and excited that you give it all your attention.</p>
<p class='rw-defn idx11' style='display:none'>A garrulous person talks a lot, especially about unimportant things.</p>
<p class='rw-defn idx12' style='display:none'>If someone is indefatigable, they never shows signs of getting tired.</p>
<p class='rw-defn idx13' style='display:none'>An indolent person is lazy.</p>
<p class='rw-defn idx14' style='display:none'>When something is inert, it has no power of movement or is inactive.</p>
<p class='rw-defn idx15' style='display:none'>Something kinetic is moving, active, and using energy.</p>
<p class='rw-defn idx16' style='display:none'>A laborious job or process takes a long time, requires a lot of effort, and is often boring.</p>
<p class='rw-defn idx17' style='display:none'>If you do something in a lackadaisical way, you do it carelessly and without putting much effort into it—thereby showing that you are not really interested in what you&#8217;re doing.</p>
<p class='rw-defn idx18' style='display:none'>A languid person is slow, relaxed, and shows little energy or interest in doing anything.</p>
<p class='rw-defn idx19' style='display:none'>Lassitude is a state of tiredness, lack of energy, and having little interest in what&#8217;s going on around you.</p>
<p class='rw-defn idx20' style='display:none'>If you are lethargic, you are tired, lack energy, and are unwilling to exert effort.</p>
<p class='rw-defn idx21' style='display:none'>When you are listless, you lack energy and interest and are unwilling to exert any effort.</p>
<p class='rw-defn idx22' style='display:none'>Someone who is loquacious talks too much.</p>
<p class='rw-defn idx23' style='display:none'>Someone is considered meticulous when they act with careful attention to detail.</p>
<p class='rw-defn idx24' style='display:none'>If you describe something as moribund, you imply that it is no longer effective; it may also be coming to the end of its useful existence.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is phlegmatic stays calm and unemotional even in dangerous or exciting situations.</p>
<p class='rw-defn idx26' style='display:none'>To be punctilious is to pay precise attention to detail.</p>
<p class='rw-defn idx27' style='display:none'>Someone who has a sedentary habit, job, or lifestyle spends a lot of time sitting down without moving or exercising often.</p>
<p class='rw-defn idx28' style='display:none'>Someone who is sedulous works hard and performs tasks very carefully and thoroughly, not stopping their work until it is completely accomplished.</p>
<p class='rw-defn idx29' style='display:none'>Something soporific makes you feel sleepy or drowsy.</p>
<p class='rw-defn idx30' style='display:none'>Stupor is a state in which someone&#8217;s mind and senses are dulled; consequently, they are unable to think clearly or act normally, usually due to the use of alcohol or drugs.</p>
<p class='rw-defn idx31' style='display:none'>If you are supine, you are lying on your back with your face upward.</p>
<p class='rw-defn idx32' style='display:none'>If your body is affected by torpor, you are severely lacking in energy; therefore, you are idle—and can even be numb.</p>
<p class='rw-defn idx33' style='display:none'>Trenchant comments or criticisms are expressed forcefully, directly, and clearly, even though they may be hurtful to the receiver.</p>
<p class='rw-defn idx34' style='display:none'>A vibrant person is lively and full of energy in a way that is exciting and attractive.</p>
<p class='rw-defn idx35' style='display:none'>If someone is described as vivacious, they are lively and have a happy, lighthearted manner.</p>
<p class='rw-defn idx36' style='display:none'>Someone who is zealous spends a lot of time, energy, and effort to support something—  notably that of a political or religious nature—because they believe in it very strongly.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>somnolent</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='somnolent#' id='pronounce-sound' path='audio/words/amy-somnolent'></a>
SOM-nuh-luhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='somnolent#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='somnolent#' id='context-sound' path='audio/wordcontexts/brian-somnolent'></a>
In the heat of summer we often feel <em>somnolent</em> or sleepy.  Even usually energetic children lie around, <em>somnolent</em> or dozing off.  I remember one particularly hot day I roused myself enough from my <em>somnolent</em> or tired state to ask if anyone wanted to go for a swim in the lake.  Although the cool water was inviting, we allowed ourselves to remain <em>somnolent</em> and unmoving in the shade rather than exert the effort to walk to the water.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What could cause you to feel <em>somnolent</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Losing a tennis match to an opponent you expected to beat.
</li>
<li class='choice answer '>
<span class='result'></span>
Relaxing by the fire with some tea after a long day at work.
</li>
<li class='choice '>
<span class='result'></span>
Watching a good friend deal with a very difficult situation.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='somnolent#' id='definition-sound' path='audio/wordmeanings/amy-somnolent'></a>
If you are <em>somnolent</em>, you are sleepy.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>sleepy</em>
</span>
</span>
</div>
<a class='quick-help' href='somnolent#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='somnolent#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/somnolent/memory_hooks/3328.json'></span>
<p>
<img alt="Somnolent" src="https://cdn0.membean.com/public/images/wordimages/hook/somnolent.jpg?qdep8" />
<span class="emp0"><span>She <span class="emp2"><span>Lent</span></span> Me Her <span class="emp3"><span>Som</span></span>i<span class="emp3"><span>n</span></span>ex</span></span> Suzy <span class="emp2"><span>lent</span></span> me her <span class="emp3"><span>Som</span></span>i<span class="emp3"><span>n</span></span>ex because I couldn't sleep; <span class="emp3"><span>Som</span></span>i<span class="emp3"><span>n</span></span>ex is a sleeping aid that helps makes Suzy <span class="emp3"><span>som</span></span>no<span class="emp2"><span>lent</span></span> so she can get a good night's sleep.
</p>
</div>

<div id='memhook-button-bar'>
<a href="somnolent#" id="add-public-hook" style="" url="https://membean.com/mywords/somnolent/memory_hooks">Use other public hook</a>
<a href="somnolent#" id="memhook-use-own" url="https://membean.com/mywords/somnolent/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='somnolent#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
[Drew Ackerman] started recording his “boring bedtime” episodes five years ago. His <b>somnolent</b> storytelling skills date back to his childhood.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Among its offerings [was] an eight-hour overnight “sleep concert” played by the meditative composer Laraaji for a mostly <b>somnolent</b> audience, hearing the music through their dreams.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
County Road 442, the only road into the park, takes you on a winding ride through the tiny town of Bend (last stop for necessities), over lazy Cherokee Creek (which can rage in heavy rains, keeping wannabe park visitors out and actual visitors in), past <b>somnolent</b> black cattle, and into a wilderness that was formerly ranchland and fishing camps.
<cite class='attribution'>
&mdash;
Texas Monthly
</cite>
</li>
<li>
The painting is typical of Smither's hard-edged realism and depicts his <b>somnolent</b> children awaiting breakfast.
<cite class='attribution'>
&mdash;
Otago Daily Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/somnolent/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='somnolent#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='somn_sleep' data-tree-url='//cdn1.membean.com/public/data/treexml' href='somnolent#'>
<span class=''></span>
somn
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>sleep</td>
</tr>
<tr>
<td class='partform'>-olent</td>
<td>
&rarr;
</td>
<td class='meaning'>rich in, full of</td>
</tr>
</table>
<p>A <em>somnolent</em> person is &#8220;full of sleep.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='somnolent#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Espander (Girl Falling Asleep In School)</strong><span> This girl is giving in to her somnolence.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/somnolent.jpg' video_url='examplevids/somnolent' video_width='350'></span>
<div id='wt-container'>
<img alt="Somnolent" height="288" src="https://cdn1.membean.com/video/examplevids/somnolent.jpg" width="350" />
<div class='center'>
<a href="somnolent#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='somnolent#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Somnolent" src="https://cdn3.membean.com/public/images/wordimages/cons2/somnolent.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='somnolent#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>apathy</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>dormant</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>enervate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>ennui</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>indolent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>inert</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>lackadaisical</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>languid</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>lassitude</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>lethargic</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>listless</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>moribund</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>phlegmatic</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>sedentary</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>soporific</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>stupor</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>supine</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>torpor</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>arduous</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>cogitate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>convivial</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>enthrall</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>garrulous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>indefatigable</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>kinetic</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>laborious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>loquacious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>meticulous</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>punctilious</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>sedulous</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>trenchant</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>vibrant</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>vivacious</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>zealous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="somnolent" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>somnolent</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="somnolent#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

