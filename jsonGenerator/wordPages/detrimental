
<!DOCTYPE html>
<html>
<head>
<title>Word: detrimental | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An acrid smell or taste is strong, unpleasant, and stings your nose or throat.</p>
<p class='rw-defn idx1' style='display:none'>If you undergo adversity in life, you struggle with trouble, bad luck, and difficult times.</p>
<p class='rw-defn idx2' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx3' style='display:none'>The adjective auspicious describes a positive beginning of something, such as a new business, or a certain time that looks to have a good chance of success or prosperity.</p>
<p class='rw-defn idx4' style='display:none'>Someone is baleful if they are filled with bad intent, anger, or hatred towards another person.</p>
<p class='rw-defn idx5' style='display:none'>When something bedevils you, it causes you a lot of problems and difficulties over a period of time.</p>
<p class='rw-defn idx6' style='display:none'>A benefaction is a charitable contribution of money or assistance that someone gives to a person or organization.</p>
<p class='rw-defn idx7' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx8' style='display:none'>If you besmirch someone, you spoil their good reputation by saying bad things about them, usually things that you know are not true.</p>
<p class='rw-defn idx9' style='display:none'>When you are granted a boon, you are given a special gift or favor that is of great benefit to you.</p>
<p class='rw-defn idx10' style='display:none'>Something brackish is distasteful and unpleasant usually because something else is mixed in with it; brackish water, for instance, is a mixture of fresh and salt water and cannot be drunk.</p>
<p class='rw-defn idx11' style='display:none'>A comestible is something that can be eaten.</p>
<p class='rw-defn idx12' style='display:none'>A conducive agent is something that is favorable or helpful in getting something to happen.</p>
<p class='rw-defn idx13' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx14' style='display:none'>A contagion is a disease—or the transmission of that disease—that is easily spread from one person to another through touch or the air.</p>
<p class='rw-defn idx15' style='display:none'>Something that has curative properties can be used for curing people&#8217;s illnesses.</p>
<p class='rw-defn idx16' style='display:none'>If you describe something, especially food and drink, as delectable, you mean that it is very pleasant, tasty, or attractive.</p>
<p class='rw-defn idx17' style='display:none'>Something that is deleterious has a harmful effect.</p>
<p class='rw-defn idx18' style='display:none'>Draconian rules and laws are extremely strict and harsh.</p>
<p class='rw-defn idx19' style='display:none'>Fetid water or air has a bad odor, usually caused by decay.</p>
<p class='rw-defn idx20' style='display:none'>If you describe something as heinous, you mean that is extremely evil, shocking, and bad.</p>
<p class='rw-defn idx21' style='display:none'>If someone is inimical, they are unfriendly and will likely cause you harm.</p>
<p class='rw-defn idx22' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx23' style='display:none'>Something that is insidious is dangerous because it seems harmless or not important; nevertheless, over time it gradually develops the capacity to cause harm and damage.</p>
<p class='rw-defn idx24' style='display:none'>An internecine conflict or quarrel takes place between people who belong to the same group, organization, country, etc.</p>
<p class='rw-defn idx25' style='display:none'>When you are in jeopardy, you are in danger or trouble of some kind.</p>
<p class='rw-defn idx26' style='display:none'>Malaise is a feeling of discontent, general unease, or even illness in a person or group for which there does not seem to be a quick and easy solution because the cause of it is not clear.</p>
<p class='rw-defn idx27' style='display:none'>A malevolent person or thing is evil due to deliberate attempts to cause harm.</p>
<p class='rw-defn idx28' style='display:none'>If you malign someone, you say unpleasant things about them to damage their reputation.</p>
<p class='rw-defn idx29' style='display:none'>A malignant thing or person, such as a tumor or criminal, is deadly or does great harm.</p>
<p class='rw-defn idx30' style='display:none'>A miasma is a harmful influence or evil feeling that seems to surround a person or place.</p>
<p class='rw-defn idx31' style='display:none'>If something is noisome, it is extremely unpleasant, especially because it is very dirty or has an awful smell.</p>
<p class='rw-defn idx32' style='display:none'>Something that is noxious is harmful, extremely unpleasant, and often poisonous.</p>
<p class='rw-defn idx33' style='display:none'>A pathogen is an organism, such as a bacterium or virus, that causes disease.</p>
<p class='rw-defn idx34' style='display:none'>Something that is pernicious is very harmful or evil, often in a way that is hidden or not quickly noticed.</p>
<p class='rw-defn idx35' style='display:none'>A pestilence is either an infectious disease that is deadly or an agent of some kind that is destructive or dangerous.</p>
<p class='rw-defn idx36' style='display:none'>Potable water is clean and safe to drink.</p>
<p class='rw-defn idx37' style='display:none'>Something that is propitious shows favorable conditions.</p>
<p class='rw-defn idx38' style='display:none'>When you ravage something, you completely destroy, wreck, or damage it.</p>
<p class='rw-defn idx39' style='display:none'>Sabotage is the deliberate or intentional destruction or damage of property or equipment in order to defeat or slow down a cause or other endeavor you don&#8217;t agree with.</p>
<p class='rw-defn idx40' style='display:none'>A salubrious place or area is pleasant, clean, healthy, and comfortable to live in.</p>
<p class='rw-defn idx41' style='display:none'>A salutary experience is beneficial to you since it strengthens you in some way, although it may be unpleasant as you undergo it; this word also refers to promoting good health.</p>
<p class='rw-defn idx42' style='display:none'>Something sinister makes you feel that an evil, bad, or harmful thing is about to happen.</p>
<p class='rw-defn idx43' style='display:none'>A virulent disease is very dangerous and spreads very quickly.</p>
<p class='rw-defn idx44' style='display:none'>The adjective wholesome can refer to something that is good for the health of your body and also for your moral health.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>detrimental</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='detrimental#' id='pronounce-sound' path='audio/words/amy-detrimental'></a>
de-truh-MEN-tl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='detrimental#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='detrimental#' id='context-sound' path='audio/wordcontexts/brian-detrimental'></a>
Eating too much sugar can be <em>detrimental</em> or harmful to your health.  It has been shown that cancer tumors feed on sugars, allowing them to grow and become <em>detrimental</em> or damaging to the body.  Another <em>detrimental</em> or destructive effect from eating too much sugar can be increased weight gain that can lead to obesity, which is very hard on one&#8217;s heart and overall good health.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If something is <em>detrimental</em> to your health, what is it?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is recommended for good health.
</li>
<li class='choice '>
<span class='result'></span>
It is unclear what effect it will have on your health.
</li>
<li class='choice answer '>
<span class='result'></span>
It is harmful to your health.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='detrimental#' id='definition-sound' path='audio/wordmeanings/amy-detrimental'></a>
Something <em>detrimental</em> causes damage, harm, or loss to someone or something.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>harmful</em>
</span>
</span>
</div>
<a class='quick-help' href='detrimental#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='detrimental#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/detrimental/memory_hooks/2841.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Detr</span></span>acts <span class="emp3"><span>Mental</span></span> Health</span></span> Too much alcohol <span class="emp1"><span>detr</span></span>acts from <span class="emp3"><span>mental</span></span> health.
</p>
</div>

<div id='memhook-button-bar'>
<a href="detrimental#" id="add-public-hook" style="" url="https://membean.com/mywords/detrimental/memory_hooks">Use other public hook</a>
<a href="detrimental#" id="memhook-use-own" url="https://membean.com/mywords/detrimental/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='detrimental#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Beyond reducing individual use, one of our top priorities must be to move from fossil fuels to energy that has fewer <b>detrimental</b> effects on water supplies and fewer environmental impacts overall.
<span class='attribution'>&mdash; David Suzuki, Canadian scientist and environmental activist</span>
<img alt="David suzuki, canadian scientist and environmental activist" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/David Suzuki, Canadian scientist and environmental activist.jpg?qdep8" width="80" />
</li>
<li>
Whether you're talking or just listening, using a cell phone may make it harder to drive a car safely. . . . Researchers say that before this study, it had been expected that speaking would be more <b>detrimental</b> than listening because speaking is often thought to be a more challenging task.
<cite class='attribution'>
&mdash;
WebMD
</cite>
</li>
<li>
[T]he research team found that with every hour per day spent watching baby DVDs and videos, infants learned six to eight fewer new vocabulary words than babies who never watched the videos. These products had the strongest <b>detrimental</b> effect on babies 8 to 16 months old, the age at which language skills are starting to form.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
<li>
While these plans are meant to offer a cheaper alternative to the plans offered in the Health Insurance Marketplace—where plans must comply with the ACA’s consumer protections — their restrictions could be <b>detrimental</b> to Missourians’ health.
<cite class='attribution'>
&mdash;
St. Louis Post-Dispatch
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/detrimental/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='detrimental#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='de_off' data-tree-url='//cdn1.membean.com/public/data/treexml' href='detrimental#'>
<span class='common'></span>
de-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>off, from</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='trit_rubbed' data-tree-url='//cdn1.membean.com/public/data/treexml' href='detrimental#'>
<span class=''></span>
trit
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>rubbed, worn away</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ment_quality' data-tree-url='//cdn1.membean.com/public/data/treexml' href='detrimental#'>
<span class=''></span>
-ment
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>quality, condition</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='al_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='detrimental#'>
<span class=''></span>
-al
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>Something <em>detrimental</em> to one&#8217;s health &#8220;rubs from&#8221; it or &#8220;wears (it) away.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='detrimental#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Joe Martin Minute</strong><span> That which is most detrimental to a fitness program.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/detrimental.jpg' video_url='examplevids/detrimental' video_width='350'></span>
<div id='wt-container'>
<img alt="Detrimental" height="288" src="https://cdn1.membean.com/video/examplevids/detrimental.jpg" width="350" />
<div class='center'>
<a href="detrimental#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='detrimental#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Detrimental" src="https://cdn3.membean.com/public/images/wordimages/cons2/detrimental.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='detrimental#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>acrid</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>adversity</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>baleful</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>bedevil</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>besmirch</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>brackish</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>contagion</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>deleterious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>draconian</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>fetid</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>heinous</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>inimical</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>insidious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>internecine</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>jeopardy</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>malaise</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>malevolent</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>malign</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>malignant</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>miasma</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>noisome</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>noxious</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>pathogen</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>pernicious</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>pestilence</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>ravage</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>sabotage</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>sinister</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>virulent</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>auspicious</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>benefaction</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>boon</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>comestible</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>conducive</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>curative</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>delectable</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>potable</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>propitious</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>salubrious</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>salutary</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>wholesome</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='detrimental#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
detriment
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>harm or loss; something that causes damage</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="detrimental" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>detrimental</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="detrimental#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

