
<!DOCTYPE html>
<html>
<head>
<title>Word: luminary | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>The acme of something is its highest point of achievement or excellence.</p>
<p class='rw-defn idx1' style='display:none'>An acolyte is someone who serves a leader as a devoted assistant or believes in the leader&#8217;s ideas.</p>
<p class='rw-defn idx2' style='display:none'>The apogee of something is its highest or greatest point, especially in reference to a culture or career.</p>
<p class='rw-defn idx3' style='display:none'>The best or perfect example of something is its apotheosis; likewise, this word can be used to indicate the best or highest point in someone&#8217;s life or job.</p>
<p class='rw-defn idx4' style='display:none'>A bagatelle is something that is not important or of very little value or significance.</p>
<p class='rw-defn idx5' style='display:none'>A bravura performance, such as of a highly technical work for the violin, is done with consummate skill.</p>
<p class='rw-defn idx6' style='display:none'>A cardinal rule or quality is considered to be the most important or basic in a set of rules or qualities.</p>
<p class='rw-defn idx7' style='display:none'>Chaff is the husks of grain separated from the edible plant; it can also be useless matter in general that is cast aside.</p>
<p class='rw-defn idx8' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx9' style='display:none'>A cynosure is an object that serves as the center of attention.</p>
<p class='rw-defn idx10' style='display:none'>If someone is deified, they have been either made into a god or are adored like one.</p>
<p class='rw-defn idx11' style='display:none'>An entourage is a group of assistants, servants, and other people who tag along with an important person.</p>
<p class='rw-defn idx12' style='display:none'>If you say that a person or thing is the epitome of something, you mean that they or it is the best possible example of that thing.</p>
<p class='rw-defn idx13' style='display:none'>An exemplary person is one that sets an ideal or praiseworthy example for others to follow.</p>
<p class='rw-defn idx14' style='display:none'>One thing that exemplifies another serves as an example of it, illustrates it, or demonstrates it.</p>
<p class='rw-defn idx15' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx16' style='display:none'>When you pay homage to another person, you show them great admiration or respect; you might even worship them.</p>
<p class='rw-defn idx17' style='display:none'>Someone who has had an illustrious professional career is celebrated and outstanding in their given field of expertise.</p>
<p class='rw-defn idx18' style='display:none'>When someone is lionized, they are treated as being very important or famous—although they may not deserve to be.</p>
<p class='rw-defn idx19' style='display:none'>A magnate is a rich and powerful person in an industry or business.</p>
<p class='rw-defn idx20' style='display:none'>Something mediocre is average or ordinary in quality; it’s just OK.</p>
<p class='rw-defn idx21' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx22' style='display:none'>If you are a novice at an activity, you have just begun or started doing it.</p>
<p class='rw-defn idx23' style='display:none'>The best or most highly regarded members of a particular group are known as a pantheon.</p>
<p class='rw-defn idx24' style='display:none'>A paragon is an example of a thing or person at its or their very best.</p>
<p class='rw-defn idx25' style='display:none'>Something that is pivotal is more important than anything else in a situation or a system; consequently, it is the key to its success.</p>
<p class='rw-defn idx26' style='display:none'>Something prosaic is dull, boring, and ordinary.</p>
<p class='rw-defn idx27' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>
<p class='rw-defn idx28' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx29' style='display:none'>If you refer to something as a trifle, you mean that it is of little or no importance or value.</p>
<p class='rw-defn idx30' style='display:none'>A trite remark or idea is so overused that it is no longer interesting or novel.</p>
<p class='rw-defn idx31' style='display:none'>A virtuoso is someone who is very skillful at something, especially playing a musical instrument.</p>
<p class='rw-defn idx32' style='display:none'>The zenith of something is its highest, most powerful, or most successful point.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>luminary</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='luminary#' id='pronounce-sound' path='audio/words/amy-luminary'></a>
LOO-muh-ner-ee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='luminary#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='luminary#' id='context-sound' path='audio/wordcontexts/brian-luminary'></a>
John wrote a paper on one of the most inspiring and accomplished <em><em>luminaries</em></em> of jazz, the musician Louis Armstrong. Mikhail researched author Toni Morrison, another <em><em>luminary</em></em> respected for her literary achievements and leadership for human rights. Another classmate gave a presentation about Mary Jackson, a distinguished mathematician and <em><em>luminary</em></em> now known for her crucial role in the success of the United States&#8217; space program. Their teacher was glad her students chose to research such highly influential people or <em><em>luminaries</em></em> from so many different professions!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Who might be considered a <em>luminary</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
One of the nation&#8217;s top scientists who is leading cancer research teams.
</li>
<li class='choice '>
<span class='result'></span>
A fictional superhero who was made famous in movies and beloved by fans.
</li>
<li class='choice '>
<span class='result'></span>
A young, still unknown singer whose songs are slowly growing in popularity.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='luminary#' id='definition-sound' path='audio/wordmeanings/amy-luminary'></a>
A <em>luminary</em> is someone who is much admired in a particular profession because they are an accomplished expert in their field.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>famous and admired person</em>
</span>
</span>
</div>
<a class='quick-help' href='luminary#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='luminary#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/luminary/memory_hooks/3108.json'></span>
<p>
<img alt="Luminary" src="https://cdn3.membean.com/public/images/wordimages/hook/luminary.jpg?qdep8" />
<span class="emp0"><span><span class="emp1"><span>Loo</span></span> <span class="emp2"><span>Men</span></span> <span class="emp1"><span>Lu</span></span><span class="emp2"><span>min</span></span>aries</span></span> The "<span class="emp1"><span>Loo</span></span> <span class="emp2"><span>Men</span></span>" are the famous <span class="emp1"><span>lu</span></span><span class="emp2"><span>min</span></span>ary inventors of the <span class="emp1"><span>loo</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="luminary#" id="add-public-hook" style="" url="https://membean.com/mywords/luminary/memory_hooks">Use other public hook</a>
<a href="luminary#" id="memhook-use-own" url="https://membean.com/mywords/luminary/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='luminary#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
After turning out culinary masterpieces for more than 40 years, the kitchen of [e]picurean <b>luminary</b> Julia Child has found its way into the Smithsonian.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
Stephen Burrows is hoping for that kind of staying power this time around. One of the first black designers to earn mainstream recognition, the '70s <b>luminary</b> dropped off the radar decades ago when his disco designs and lifestyle fell out of favor.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/luminary/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='luminary#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='lumin_light' data-tree-url='//cdn1.membean.com/public/data/treexml' href='luminary#'>
<span class=''></span>
lumin
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>light, glow</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ary_person' data-tree-url='//cdn1.membean.com/public/data/treexml' href='luminary#'>
<span class=''></span>
-ary
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>person belonging to</td>
</tr>
</table>
<p>A <em>luminary</em> is an eminent or outstanding member of a specific field of study who has had or has such influence that her &#8220;glow&#8221; or &#8220;light&#8221; reaches many others upon whom she shines.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='luminary#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube</strong><span> Maya Angelou Receives Arts</span>
Humanities Award from President: Maya Angelou is a great luminary from the United States.
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/luminary.jpg' video_url='examplevids/luminary' video_width='350'></span>
<div id='wt-container'>
<img alt="Luminary" height="288" src="https://cdn1.membean.com/video/examplevids/luminary.jpg" width="350" />
<div class='center'>
<a href="luminary#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='luminary#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Luminary" src="https://cdn0.membean.com/public/images/wordimages/cons2/luminary.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='luminary#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>acme</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>apogee</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>apotheosis</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>bravura</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>cardinal</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>cynosure</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>deify</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>epitome</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>exemplary</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>exemplify</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>homage</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>illustrious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>lionize</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>magnate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>pantheon</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>paragon</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>pivotal</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>virtuoso</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>zenith</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>acolyte</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>bagatelle</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>chaff</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>entourage</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>mediocre</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>novice</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>prosaic</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>trifle</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>trite</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="luminary" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>luminary</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="luminary#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

