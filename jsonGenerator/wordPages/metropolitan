
<!DOCTYPE html>
<html>
<head>
<title>Word: metropolitan | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Metropolitan-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/metropolitan-large.jpg?qdep8" />
</div>
<a href="metropolitan#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>The adjective agrarian is used to describe something that is related to farmland or the economy that is concerned with agriculture.</p>
<p class='rw-defn idx1' style='display:none'>The adjective bucolic is used to describe things that are related to or characteristic of rural or country life.</p>
<p class='rw-defn idx2' style='display:none'>A cosmopolitan area represents large cultural diversity and people from around the globe; a cosmopolitan person is broad-minded and wise in the ways of the world.</p>
<p class='rw-defn idx3' style='display:none'>A demographic study describes the characteristics of part of a population that is usually used for purposes of marketing.</p>
<p class='rw-defn idx4' style='display:none'>A desolate area is unused, empty of life, deserted, and lonely.</p>
<p class='rw-defn idx5' style='display:none'>An eclectic assortment of things or people comes from many varied sources; additionally, it usually includes the best of those sources.</p>
<p class='rw-defn idx6' style='display:none'>Ecumenical activities and ideas encourage different religions or congregations to work and worship together in order to unite them in friendship.</p>
<p class='rw-defn idx7' style='display:none'>A forlorn person is lonely because they have been abandoned; a forlorn home has been deserted.</p>
<p class='rw-defn idx8' style='display:none'>A gregarious person is friendly, highly social, and prefers being with people rather than being alone.</p>
<p class='rw-defn idx9' style='display:none'>The adjective hermetic describes something that is set apart, isolated, or separate from the influence or interference of society at large.</p>
<p class='rw-defn idx10' style='display:none'>If someone is insular, they are either unwilling to meet anyone outside their own small group or they are not interested in learning new ideas.</p>
<p class='rw-defn idx11' style='display:none'>A macrocosm is a large, complex, and organized system or structure that is made of many small parts that form one unit, such as a society or the universe.</p>
<p class='rw-defn idx12' style='display:none'>A pariah is a social outcast.</p>
<p class='rw-defn idx13' style='display:none'>Someone has a parochial outlook when they are mostly concerned with narrow or limited issues that affect a small, local area or a very specific cause; they are also unconcerned with wider or more general issues.</p>
<p class='rw-defn idx14' style='display:none'>A pastoral environment is rural, peaceful, simple, and natural.</p>
<p class='rw-defn idx15' style='display:none'>A provincial outlook is narrow-minded and unsophisticated, limited to the opinions of a relatively local area.</p>
<p class='rw-defn idx16' style='display:none'>A recluse is someone who chooses to live alone and deliberately avoids other people.</p>
<p class='rw-defn idx17' style='display:none'>A rustic setting is rural or placed in the country.</p>
<p class='rw-defn idx18' style='display:none'>A troglodyte is reclusive, severely lacking in social skills, and is out of step with current times.</p>
<p class='rw-defn idx19' style='display:none'>If you behave in an urbane way, you are behaving in a polite, refined, and civilized fashion in social situations.</p>
<p class='rw-defn idx20' style='display:none'>Something that is variegated has various tones or colors; it can also mean filled with variety.</p>
<p class='rw-defn idx21' style='display:none'>A vernacular is the ordinary and everyday language spoken by people in a particular country or region that differs from the literary or standard written language of that area.</p>
<p class='rw-defn idx22' style='display:none'>A yokel is uneducated, naive, and unsophisticated; they do not know much about modern life or ideas because they sequester themselves in a rural setting.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>metropolitan</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='metropolitan#' id='pronounce-sound' path='audio/words/amy-metropolitan'></a>
me-truh-POL-i-tn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='metropolitan#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='metropolitan#' id='context-sound' path='audio/wordcontexts/brian-metropolitan'></a>
I really want to live in a <em>metropolitan</em> setting or large city when I grow up.  I just think that <em>metropolitan</em> or heavily urbanized areas offer such great cultural opportunities.  I also love people, so the larger the <em>metropolitan</em> or urban public area the better!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What might you find in a <em>metropolitan</em> area?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Rugged mountains that hikers love to visit.
</li>
<li class='choice answer '>
<span class='result'></span>
Tall buildings and streets full of cars and people.
</li>
<li class='choice '>
<span class='result'></span>
Large farms surrounding a small town.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='metropolitan#' id='definition-sound' path='audio/wordmeanings/amy-metropolitan'></a>
A <em>metropolitan</em> area contains a very large city.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>of a major city</em>
</span>
</span>
</div>
<a class='quick-help' href='metropolitan#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='metropolitan#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/metropolitan/memory_hooks/5680.json'></span>
<p>
<span class="emp0"><span>Nea<span class="emp2"><span>politan</span></span></span></span> Nea<span class="emp2"><span>politans</span></span> are from Naples, the second largest metro<span class="emp2"><span>politan</span></span> area in Italy.
</p>
</div>

<div id='memhook-button-bar'>
<a href="metropolitan#" id="add-public-hook" style="" url="https://membean.com/mywords/metropolitan/memory_hooks">Use other public hook</a>
<a href="metropolitan#" id="memhook-use-own" url="https://membean.com/mywords/metropolitan/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='metropolitan#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Sager said he expects wide-scale expansion of safe, efficient, and economical mass-transit systems to reduce traffic congestion in all major <b>metropolitan</b> areas in the coming decades.
<cite class='attribution'>
&mdash;
The Onion
</cite>
</li>
<li>
Handsets and streaming data plans still remain an expensive commodity for many, though, and reception can of course wash out, especially in rural or highly-trafficked <b>metropolitan</b> areas.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
<li>
More than 2 million flights pass over the city every year, most traveling to and from the <b>metropolitan</b> area’s three busiest airports: John F. Kennedy, Newark, and LaGuardia.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
Take any one of a half-dozen large <b>metropolitan</b> areas and add an energy industry and you get the performance you have in Dallas or Houston.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/metropolitan/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='metropolitan#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='metr_mother' data-tree-url='//cdn1.membean.com/public/data/treexml' href='metropolitan#'>
<span class=''></span>
metr
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>mother, womb</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='o_connective' data-tree-url='//cdn1.membean.com/public/data/treexml' href='metropolitan#'>
<span class=''></span>
-o-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>connective</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='polit_citizen' data-tree-url='//cdn1.membean.com/public/data/treexml' href='metropolitan#'>
<span class=''></span>
polit
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>citizen</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='an_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='metropolitan#'>
<span class=''></span>
-an
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>relating to</td>
</tr>
</table>
<p>A <em>metropolitan</em> setting refers to where the &#8220;citizens of a mother (city)&#8221; live.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='metropolitan#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Manhattan</strong><span> Metropolitan scenes.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/metropolitan.jpg' video_url='examplevids/metropolitan' video_width='350'></span>
<div id='wt-container'>
<img alt="Metropolitan" height="288" src="https://cdn1.membean.com/video/examplevids/metropolitan.jpg" width="350" />
<div class='center'>
<a href="metropolitan#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='metropolitan#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Metropolitan" src="https://cdn0.membean.com/public/images/wordimages/cons2/metropolitan.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='metropolitan#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>cosmopolitan</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>demographic</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>eclectic</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>ecumenical</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>gregarious</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>macrocosm</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>urbane</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>variegated</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>agrarian</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>bucolic</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>desolate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>forlorn</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>hermetic</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>insular</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>pariah</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>parochial</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>pastoral</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>provincial</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>recluse</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>rustic</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>troglodyte</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>vernacular</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>yokel</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='metropolitan#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
metropolis
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>a very large city</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="metropolitan" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>metropolitan</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="metropolitan#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

