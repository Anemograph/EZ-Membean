
<!DOCTYPE html>
<html>
<head>
<title>Word: malfeasance | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Malfeasance-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/malfeasance-large.jpg?qdep8" />
</div>
<a href="malfeasance#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx1' style='display:none'>When you absolve someone, you publicly and formally say that they are not guilty or responsible for any wrongdoing.</p>
<p class='rw-defn idx2' style='display:none'>When you arraign someone, you make them come to court and answer criminal charges made against them.</p>
<p class='rw-defn idx3' style='display:none'>If you describe a person&#8217;s behavior as brazen, you mean that they are not embarrassed by anything they do—they simply don&#8217;t care what other people think about them.</p>
<p class='rw-defn idx4' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx5' style='display:none'>If you demur, you delay in doing or mildly object to something because you don&#8217;t really want to do it.</p>
<p class='rw-defn idx6' style='display:none'>If you describe someone, usually a young woman, as demure, you mean that she is quiet, shy, and always behaves modestly.</p>
<p class='rw-defn idx7' style='display:none'>Something, such as a building, is derelict if it is empty, not used, and in bad condition or disrepair.</p>
<p class='rw-defn idx8' style='display:none'>If you refer to the enormity of a situation, problem, or event, you mean that it is very evil or morally offensive; this word has nothing to do with physical size.</p>
<p class='rw-defn idx9' style='display:none'>Errant things or people behave in a way that is unacceptable or wrong, such as a missile that travels in the wrong direction or a student who does not follow classroom rules.</p>
<p class='rw-defn idx10' style='display:none'>Something that is inconspicuous does not attract attention and is not easily seen or noticed because it is small or ordinary.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is indiscreet shows lack of judgment, especially because they say or do things in public that should only be said or done privately—if at all.</p>
<p class='rw-defn idx12' style='display:none'>Iniquity is an immoral act, wickedness, or evil.</p>
<p class='rw-defn idx13' style='display:none'>Someone is overweening when they are not modest; rather, they think way too much of themselves and let everyone know about it.</p>
<p class='rw-defn idx14' style='display:none'>A peccadillo is a slight or minor offense that can be overlooked.</p>
<p class='rw-defn idx15' style='display:none'>If you perpetrate something, you commit a crime or do some other bad thing for which you are responsible.</p>
<p class='rw-defn idx16' style='display:none'>To be politic in a decision is to be socially wise and diplomatic.</p>
<p class='rw-defn idx17' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx18' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx19' style='display:none'>If you commit a solecism, you make an error of some kind, such as one of a grammatical or social nature.</p>
<p class='rw-defn idx20' style='display:none'>Something statutory, such as the power given to a governor or president, is created, established, and controlled by rules and laws; hence, it has the full force of the law behind it and must be followed.</p>
<p class='rw-defn idx21' style='display:none'>If you say someone&#8217;s behavior is unseemly, you disapprove of it because it is not in good taste or not suitable for a particular situation.</p>
<p class='rw-defn idx22' style='display:none'>Something that is unsullied is unstained and clean.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>malfeasance</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='malfeasance#' id='pronounce-sound' path='audio/words/amy-malfeasance'></a>
mal-FEE-zuhns
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='malfeasance#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='malfeasance#' id='context-sound' path='audio/wordcontexts/brian-malfeasance'></a>
The dean of the public university was placed on paid leave in response to some mysterious, illegal act or <em>malfeasance</em>.  No one is sure exactly what happened, but it is assumed that he&#8217;s guilty of some sort of misconduct or <em>malfeasance</em>.  Once the dean is brought to trial for his <em>malfeasance</em> we may learn about his wrongdoing, but we&#8217;ll have to wait until that point.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Who is likely to engage in <em>malfeasance</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A writer who does not check the source of some information.
</li>
<li class='choice '>
<span class='result'></span>
A child who is extremely upset at a parent about a punishment.
</li>
<li class='choice answer '>
<span class='result'></span>
An elected official who doesn&#8217;t care about obeying the law.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='malfeasance#' id='definition-sound' path='audio/wordmeanings/amy-malfeasance'></a>
<em>Malfeasance</em> is an unlawful act, especially one committed by a trusted public official.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>wrongdoing</em>
</span>
</span>
</div>
<a class='quick-help' href='malfeasance#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='malfeasance#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/malfeasance/memory_hooks/6287.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Ma</span></span>u<span class="emp1"><span>l</span></span> Not a Pl<span class="emp2"><span>easan</span></span>t <span class="emp3"><span>F</span></span>or<span class="emp3"><span>ce</span></span></span></span> Would you consider the chief librarian taking a <span class="emp1"><span>ma</span></span>u<span class="emp1"><span>l</span></span> and using unpl<span class="emp2"><span>easan</span></span>t <span class="emp3"><span>f</span></span>or<span class="emp3"><span>ce</span></span> to break all the library's windows an act of <span class="emp1"><span>mal</span></span><span class="emp3"><span>f</span></span><span class="emp2"><span>easan</span></span><span class="emp3"><span>ce</span></span>?
</p>
</div>

<div id='memhook-button-bar'>
<a href="malfeasance#" id="add-public-hook" style="" url="https://membean.com/mywords/malfeasance/memory_hooks">Use other public hook</a>
<a href="malfeasance#" id="memhook-use-own" url="https://membean.com/mywords/malfeasance/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='malfeasance#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Public officials absolutely should be subject to rigorous scrutiny and criticism by constituents, but the recall power must be resorted to only in cases of <b>malfeasance</b>. Otherwise, critics will likely resort to it too readily, sowing further discord and eroding the importance of elections.
<cite class='attribution'>
&mdash;
Omaha World-Herald
</cite>
</li>
<li>
The Nassau County Association, through its counsel, this afternoon filed formal charges of <b>malfeasance</b> in office against Sheriff Phineas A. Seaman and asked Governor Smith to remove him from office.
<cite class='attribution'>
&mdash;
The New York Times, from 1919
</cite>
</li>
<li>
“For seven years, the Pittsburgh Water and Sewer Authority has failed to meet its public trust obligations in complying with the Clean Water Act during the production of drinking water for the citizens of Pittsburgh,” said U.S. Attorney Scott Brady in a statement. “Today’s criminal charges shed light on years of mismanagement and <b>malfeasance</b>.”
<cite class='attribution'>
&mdash;
KDKA, CBS Pittsburgh
</cite>
</li>
<li>
Impeachment is a basic constitutional safeguard, designed both to correct harms to the system of government itself and to protect the people from ongoing <b>malfeasance</b>.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/malfeasance/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='malfeasance#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='mal_bad' data-tree-url='//cdn1.membean.com/public/data/treexml' href='malfeasance#'>
<span class='common'></span>
mal
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>bad, evil</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='feas_make' data-tree-url='//cdn1.membean.com/public/data/treexml' href='malfeasance#'>
<span class=''></span>
feas
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make, do</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ance_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='malfeasance#'>
<span class=''></span>
-ance
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state or condition</td>
</tr>
</table>
<p>An act of <em>malfeasance</em> is the &#8220;state or condition of the doing of something bad.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='malfeasance#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>The Office</strong><span> Someone is caught in an act of pretty minor business malfeasance.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/malfeasance.jpg' video_url='examplevids/malfeasance' video_width='350'></span>
<div id='wt-container'>
<img alt="Malfeasance" height="288" src="https://cdn1.membean.com/video/examplevids/malfeasance.jpg" width="350" />
<div class='center'>
<a href="malfeasance#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='malfeasance#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Malfeasance" src="https://cdn0.membean.com/public/images/wordimages/cons2/malfeasance.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='malfeasance#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>arraign</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>brazen</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>derelict</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>enormity</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>errant</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>indiscreet</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>iniquity</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>overweening</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>peccadillo</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>perpetrate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>solecism</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>unseemly</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>absolve</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>demur</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>demure</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>inconspicuous</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>politic</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>statutory</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>unsullied</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="malfeasance" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>malfeasance</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="malfeasance#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

