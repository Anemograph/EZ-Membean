
<!DOCTYPE html>
<html>
<head>
<title>Word: abdicate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abjure a belief or a way of behaving, you state publicly that you will give it up or reject it.</p>
<p class='rw-defn idx1' style='display:none'>The abnegation of something is someone&#8217;s giving up their rights or claim to it or denying themselves of it; this action may or may not be in their best interest.</p>
<p class='rw-defn idx2' style='display:none'>When you arrogate something, such as a position or privilege, you take it even though you don&#8217;t have the legal right to it.</p>
<p class='rw-defn idx3' style='display:none'>When you capitulate, you accept or agree to do something after having resisted doing so for a long time.</p>
<p class='rw-defn idx4' style='display:none'>Someone cedes land or power to someone else by giving it to them, often because of political or military pressure.</p>
<p class='rw-defn idx5' style='display:none'>When something devolves, such as a responsibility or a person&#8217;s status, it passes along to another person.</p>
<p class='rw-defn idx6' style='display:none'>If you expropriate something, you take it away for your own use although it does not belong to you; governments frequently expropriate private land to use for public purposes.</p>
<p class='rw-defn idx7' style='display:none'>If you jettison something, you get rid of it because you think it is not useful or appropriate.</p>
<p class='rw-defn idx8' style='display:none'>If you recant, you publicly announce that your once firmly held beliefs or statements were wrong and that you no longer agree with them.</p>
<p class='rw-defn idx9' style='display:none'>When you relinquish something, you give it up or let it go.</p>
<p class='rw-defn idx10' style='display:none'>To renounce something, such as a position or practice, is to let go of it or reject it.</p>
<p class='rw-defn idx11' style='display:none'>If governments, companies, or other institutions retrench, they reduce costs and/or decrease the amount that they spend in order to save money.</p>
<p class='rw-defn idx12' style='display:none'>If someone subjugates a group of people or country, they conquer and bring it under control by force.</p>
<p class='rw-defn idx13' style='display:none'>When something supersedes another thing, it takes the place of or succeeds it.</p>
<p class='rw-defn idx14' style='display:none'>When you usurp someone else&#8217;s power, position, or role, you take it from them although you do not have the right to do so.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>abdicate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='abdicate#' id='pronounce-sound' path='audio/words/amy-abdicate'></a>
AB-di-kayt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='abdicate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='abdicate#' id='context-sound' path='audio/wordcontexts/brian-abdicate'></a>
The king was tired of ruling and felt that it was time for him to <em>abdicate</em> the throne, allowing his son to become king.  The prince believed his father should remain in power and should not <em>abdicate</em> until he was older.  The king reconsidered, and decided that while he was weary of his position, it wouldn&#8217;t be right to <em>abdicate</em> or surrender his responsibility when the citizens still needed him.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does <em>abdicate</em> mean?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
To present a large sum of money to another.
</li>
<li class='choice '>
<span class='result'></span>
To hire someone to help with a large task.
</li>
<li class='choice answer '>
<span class='result'></span>
To discard a position of power.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='abdicate#' id='definition-sound' path='audio/wordmeanings/amy-abdicate'></a>
If someone <em>abdicates</em>, they give up their responsibility for something, such as a king&#8217;s transfer of power when he gives up his throne.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>give up a right</em>
</span>
</span>
</div>
<a class='quick-help' href='abdicate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='abdicate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/abdicate/memory_hooks/5341.json'></span>
<p>
<img alt="Abdicate" src="https://cdn3.membean.com/public/images/wordimages/hook/abdicate.jpg?qdep8" />
<span class="emp0"><span><span class="emp1"><span>Ab</span></span>by, <span class="emp2"><span>Dic</span></span>k, and <span class="emp3"><span>Cate</span></span></span></span> <span class="emp2"><span>Dic</span></span>k was dating <span class="emp1"><span>Ab</span></span>by, but gorgeous <span class="emp3"><span>Cate</span></span> came along so <span class="emp1"><span>Ab</span></span>by had to <span class="emp1"><span>ab</span></span><span class="emp2"><span>dic</span></span><span class="emp3"><span>ate</span></span> her primary position to <span class="emp3"><span>Cate</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="abdicate#" id="add-public-hook" style="" url="https://membean.com/mywords/abdicate/memory_hooks">Use other public hook</a>
<a href="abdicate#" id="memhook-use-own" url="https://membean.com/mywords/abdicate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='abdicate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The researchers note that some scholars think Charles V decided to <b>abdicate</b> after a gout attack in 1552 forced him to postpone an attempt to recapture the French city of Metz, where he was later defeated.
<cite class='attribution'>
&mdash;
Associated Press
</cite>
</li>
<li>
James had assumed the throne of Scotland in 1567 when his mother, Mary, Queen of Scots, was forced to <b>abdicate</b>, and he became king of England after his cousin Elizabeth died in 1603.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Mr. Badawi, an Islamic scholar . . . said at a UN conference . . . that Islam respected cultural and religious diversity, and that Muslim governments should put social justice before popularity. ‘A true Muslim will also not <b>abdicate</b> the principle of fairness in managing ethnic relations even if it makes him somewhat unpopular within his own ethnic community,’ he said.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/abdicate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='abdicate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ab_away' data-tree-url='//cdn1.membean.com/public/data/treexml' href='abdicate#'>
<span class='common'></span>
ab-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>away, from</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='dic_say' data-tree-url='//cdn1.membean.com/public/data/treexml' href='abdicate#'>
<span class='common'></span>
dic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>say, talk, proclaim</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ate_quality' data-tree-url='//cdn1.membean.com/public/data/treexml' href='abdicate#'>
<span class=''></span>
-ate
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to make something have a certain quality</td>
</tr>
</table>
<p>To &#8220;abdicate&#8221; one &#8220;proclaims&#8221; that one is &#8220;away from&#8221; a given responsibility.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='abdicate#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Superman 2</strong><span> He abdicates his authority to General Zod.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/abdicate.jpg' video_url='examplevids/abdicate' video_width='350'></span>
<div id='wt-container'>
<img alt="Abdicate" height="288" src="https://cdn1.membean.com/video/examplevids/abdicate.jpg" width="350" />
<div class='center'>
<a href="abdicate#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='abdicate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Abdicate" src="https://cdn0.membean.com/public/images/wordimages/cons2/abdicate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='abdicate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abjure</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abnegation</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>capitulate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>cede</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>devolve</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>jettison</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>recant</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>relinquish</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>renounce</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>arrogate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>expropriate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>retrench</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>subjugate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>supersede</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>usurp</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='abdicate#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
abdication
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>the giving up of a responsibility</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="abdicate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>abdicate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="abdicate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

