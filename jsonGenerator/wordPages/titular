
<!DOCTYPE html>
<html>
<head>
<title>Word: titular | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you describe a decision, rule, or plan as arbitrary, you think that it was decided without any thought, standard, or system to guide it; therefore, it can seem unfair.</p>
<p class='rw-defn idx1' style='display:none'>An autocratic person rules with complete power; consequently, they make decisions and give orders to people without asking them for their opinion or advice.</p>
<p class='rw-defn idx2' style='display:none'>A chimerical idea is unrealistic, imaginary, or unlikely to be fulfilled.</p>
<p class='rw-defn idx3' style='display:none'>A conjecture is a theory or guess that is based on information that is not certain or complete.</p>
<p class='rw-defn idx4' style='display:none'>Credence is the mental act of believing or accepting that something is true.</p>
<p class='rw-defn idx5' style='display:none'>A despot is a leader who has a lot of power and uses it in a cruel and unreasonable way.</p>
<p class='rw-defn idx6' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx7' style='display:none'>An echelon is one level of status or rank in an organization.</p>
<p class='rw-defn idx8' style='display:none'>Empirical evidence or study is based on real experience or scientific experiments rather than on unproven theories.</p>
<p class='rw-defn idx9' style='display:none'>A figurehead in an organization is the apparent authority or head in name only—the real power lies somewhere else.</p>
<p class='rw-defn idx10' style='display:none'>A hierarchy is a system of organization in a society, company, or other group in which people are divided into different ranks or levels of importance.</p>
<p class='rw-defn idx11' style='display:none'>Something that is hypothetical is based on possible situations or events rather than actual ones.</p>
<p class='rw-defn idx12' style='display:none'>If you describe something as nebulous, you mean that it is unclear, vague, and not clearly defined; a shape that is nebulous has no clear boundaries.</p>
<p class='rw-defn idx13' style='display:none'>Nominal can refer to someone who is in charge in name only, or it can refer to a very small amount of something; both are related by their relative insignificance.</p>
<p class='rw-defn idx14' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx15' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx16' style='display:none'>Someone is overweening when they are not modest; rather, they think way too much of themselves and let everyone know about it.</p>
<p class='rw-defn idx17' style='display:none'>A plenipotentiary is someone who has full power to take action or make decisions on behalf of their government in a foreign country.</p>
<p class='rw-defn idx18' style='display:none'>If you postulate something, you assert that it is true without proof; therefore, it can be used as a basis for argument or reasoning—even though there is no factual basis for the assumption.</p>
<p class='rw-defn idx19' style='display:none'>A potentate is a ruler who has great power over people.</p>
<p class='rw-defn idx20' style='display:none'>If you handle something in a pragmatic way, you deal with it in a realistic and practical manner rather than just following unproven theories or ideas.</p>
<p class='rw-defn idx21' style='display:none'>Something putative is supposed to be real; for example, a putative leader is one who everyone assumes is the leader—even though they may not be in reality.</p>
<p class='rw-defn idx22' style='display:none'>Substantive issues are the most important, serious, and real issues of a subject.</p>
<p class='rw-defn idx23' style='display:none'>If you behave in a supercilious way, you act as if you were more important or better than everyone else.</p>
<p class='rw-defn idx24' style='display:none'>To be tentative is to be hesitant or uncertain about something; an agreement or decision of this kind is likely to have changes before it reaches its final form.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx26' style='display:none'>A utilitarian object is useful, serviceable, and practical—rather than fancy or unnecessary.</p>
<p class='rw-defn idx27' style='display:none'>Something that is vaporous is not completely formed but foggy and misty; in the same vein, a vaporous idea is insubstantial and vague.</p>
<p class='rw-defn idx28' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx29' style='display:none'>Verisimilitude is something&#8217;s authenticity or appearance of being real or true.</p>
<p class='rw-defn idx30' style='display:none'>The verity of something is the truth or reality of it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>titular</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='titular#' id='pronounce-sound' path='audio/words/amy-titular'></a>
TICH-uh-ler
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='titular#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='titular#' id='context-sound' path='audio/wordcontexts/brian-titular'></a>
Since Grandpa Joe was really in charge, Uncle Jack was only the <em>titular</em> head of his company, that is, in name only.  Uncle Jack&#8217;s Chicken Shack&#8217;s logo featured a picture of Uncle Jack surrounded by chickens, but since this was really the only contribution he had made, his position was a <em>titular</em> or powerless one only.  Grandpa Joe handled all aspects of the business workings, whereas Uncle Jack bore an impressive but nevertheless merely <em>titular</em> designation of President and <span class="caps">CEO</span>, titles which meant next to nothing.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>titular</em> position?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
One that is first in a group of many.
</li>
<li class='choice answer '>
<span class='result'></span>
One whose name bears little authority.
</li>
<li class='choice '>
<span class='result'></span>
One that has a meaningful mission.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='titular#' id='definition-sound' path='audio/wordmeanings/amy-titular'></a>
If a person holds a <em>titular</em> position, they have a title but no real power.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>in name only</em>
</span>
</span>
</div>
<a class='quick-help' href='titular#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='titular#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/titular/memory_hooks/3362.json'></span>
<p>
<span class="emp0"><span>Circ<span class="emp3"><span>ular</span></span> <span class="emp1"><span>Tit</span></span>le</span></span> When I was promoted to Assistant to the Assistant Who Assists the Vice-President, I realized that this was nothing but a circ<span class="emp3"><span>ular</span></span> <span class="emp1"><span>tit</span></span>le that was <span class="emp1"><span>tit</span></span><span class="emp3"><span>ular</span></span> in nature, and held no real power or influence at all.
</p>
</div>

<div id='memhook-button-bar'>
<a href="titular#" id="add-public-hook" style="" url="https://membean.com/mywords/titular/memory_hooks">Use other public hook</a>
<a href="titular#" id="memhook-use-own" url="https://membean.com/mywords/titular/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='titular#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
The Sioux did not delegate real power to an individual. . . . As Lowie puts it, “in normal times the chief was not a supreme executive, but a peacemaker and an orator.” Chiefs—all chiefs—were <b>titular</b>, “and any power exercised within the tribe was exercised by the total body of responsible men who had qualified for social eminence by their war record and their generosity.”
<span class='attribution'>&mdash; Stephen E. Ambrose, American historian and biographer, from _Crazy Horse and Custer: The Parallel Lives of Two American Warriors_</span>
<img alt="Stephen e" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Stephen E. Ambrose, American historian and biographer, from _Crazy Horse and Custer: The Parallel Lives of Two American Warriors_.jpg?qdep8" width="80" />
</li>
<li>
It was Barmore's first national title—officially, anyway. As Tech's associate head coach in 1981 and '82, he had masterminded those two championship teams, but <b>titular</b> head Sonja Hogg got the credit.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
The date was September 1, 1923, and the event was the Great Kanto Earthquake, at the time considered the worst natural disaster ever to strike quake-prone Japan. . . . From Washington, President Calvin Coolidge took the lead in rallying the United States. . . . The American Red Cross, of which Coolidge was the <b>titular</b> head, initiated a national relief drive, raising $12 million for victims.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
That would make Aramco the most valuable company ever listed on a public exchange. The Saudi government owns 100 percent of the company, and the Saudi royal family owns 100 percent of the government. King Salman is the <b>titular</b> leader, but due to his poor health, his son, the crown prince, is in charge.
<cite class='attribution'>
&mdash;
Houston Chronicle
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/titular/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='titular#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='titul_label' data-tree-url='//cdn1.membean.com/public/data/treexml' href='titular#'>
<span class=''></span>
titul
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>label, title, description</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ar_pertaining' data-tree-url='//cdn1.membean.com/public/data/treexml' href='titular#'>
<span class=''></span>
-ar
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>pertaining to or resembling</td>
</tr>
</table>
<p>The word <em>titular</em> is &#8220;pertaining to a label, title, or description.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='titular#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Titular" src="https://cdn0.membean.com/public/images/wordimages/cons2/titular.png?qdep6" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='titular#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>arbitrary</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>chimerical</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>conjecture</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>credence</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>figurehead</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>hypothetical</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>nebulous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>nominal</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>postulate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>putative</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>tentative</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>vaporous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>autocratic</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>despot</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>echelon</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>empirical</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>hierarchy</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>overweening</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>plenipotentiary</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>potentate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>pragmatic</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>substantive</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>supercilious</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>utilitarian</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>verisimilitude</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="titular" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>titular</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="titular#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

