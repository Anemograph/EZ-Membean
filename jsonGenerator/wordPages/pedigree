
<!DOCTYPE html>
<html>
<head>
<title>Word: pedigree | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Pedigree-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/pedigree-large.jpg?qdep8" />
</div>
<a href="pedigree#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>An antecedent of something, such as an event or organization, has happened or existed before it and can be similar to it.</p>
<p class='rw-defn idx1' style='display:none'>When you beget something, you cause it to happen or create it.</p>
<p class='rw-defn idx2' style='display:none'>The adjective conjugal refers to marriage or the relationship between two married people.</p>
<p class='rw-defn idx3' style='display:none'>Consanguinity is the state of being related to someone else by blood or having a similar close relationship to them.</p>
<p class='rw-defn idx4' style='display:none'>An echelon is one level of status or rank in an organization.</p>
<p class='rw-defn idx5' style='display:none'>If something engenders a particular feeling or attitude, it causes that condition.</p>
<p class='rw-defn idx6' style='display:none'>Gestation is the process by which a new idea,  plan, or piece of work develops in your mind.</p>
<p class='rw-defn idx7' style='display:none'>A matriarch is an older and powerful woman who controls a family, community, or other social group.</p>
<p class='rw-defn idx8' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx9' style='display:none'>Nomenclature is a specialized form of vocabulary that classifies or organizes things in the sciences or the arts into a clear and usable system.</p>
<p class='rw-defn idx10' style='display:none'>A patriarch is a male leader of a family or tribe; a patriarch can also be a man who is the founder of a group or organization.</p>
<p class='rw-defn idx11' style='display:none'>A first event is a precursor to a second event if the first event is responsible for the development or existence of the second.</p>
<p class='rw-defn idx12' style='display:none'>A progenitor is someone&#8217;s ancestor; they can also be the originator of something.</p>
<p class='rw-defn idx13' style='display:none'>Progeny are children or descendants.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>pedigree</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='pedigree#' id='pronounce-sound' path='audio/words/amy-pedigree'></a>
PED-i-gree
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='pedigree#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='pedigree#' id='context-sound' path='audio/wordcontexts/brian-pedigree'></a>
My dog has a great <em>pedigree</em>, for his pure-blood family tree is strong and celebrated.  His father&#8217;s <em>pedigree</em> or ancestry goes back to the first Golden Retriever to ever win Best of Show in a famous dog show.  Their <em>pedigree</em> or pure-bred line is also celebrated for good health, having no line of cancer, eye problems, or hip issues.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What might you find in a <em>pedigree</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
The amount of money you would have if you sell everything you own.
</li>
<li class='choice '>
<span class='result'></span>
The items that a family member has left you after their death.
</li>
<li class='choice answer '>
<span class='result'></span>
The names of all the people to whom you are related.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='pedigree#' id='definition-sound' path='audio/wordmeanings/amy-pedigree'></a>
A <em>pedigree</em> is your list of ancestors or family tree; it can also be the lineage of a purebred animal.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>ancestry</em>
</span>
</span>
</div>
<a class='quick-help' href='pedigree#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='pedigree#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/pedigree/memory_hooks/3339.json'></span>
<p>
<img alt="Pedigree" src="https://cdn3.membean.com/public/images/wordimages/hook/pedigree.jpg?qdep8" />
<span class="emp0"><span><span class="emp1"><span>Pedigree</span></span> Dog Food</span></span> The <span class="emp1"><span>Pedigree</span></span> company would like you to believe that its food is the best food for the best <span class="emp1"><span>pedigree</span></span> dogs.
</p>
</div>

<div id='memhook-button-bar'>
<a href="pedigree#" id="add-public-hook" style="" url="https://membean.com/mywords/pedigree/memory_hooks">Use other public hook</a>
<a href="pedigree#" id="memhook-use-own" url="https://membean.com/mywords/pedigree/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='pedigree#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
There are more than 1,000 varieties, and many are named for the Native American tribes that first harvested them, attesting to pecans' genuine Thanksgiving <b>pedigree</b>.
<cite class='attribution'>
&mdash;
WebMD
</cite>
</li>
<li>
Forty miles west of Washington, this is the stratified, socially correct home of the red fox, the <b>pedigreed</b> horse, the <b>pedigreed</b> hound and the <b>pedigreed</b> person.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
That musical <b>pedigree</b> was further extended when [director Brad] Bird signed on Harry Connick, Jr. to voice one of the film’s main characters, Dean.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
More intriguingly, the volunteers were unable to make reliable pairs in a follow-up study in which the dogs involved were mongrels rather than <b>pedigree</b>.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/pedigree/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='pedigree#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ped_foot' data-tree-url='//cdn1.membean.com/public/data/treexml' href='pedigree#'>
<span class='common'></span>
ped
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>foot</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='i_connective' data-tree-url='//cdn1.membean.com/public/data/treexml' href='pedigree#'>
<span class=''></span>
-i-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>connective</td>
</tr>
<tr>
<td class='partform'>gree</td>
<td>
&rarr;
</td>
<td class='meaning'>crane</td>
</tr>
</table>
<p><em>Pedigree</em> means the &#8220;foot of a crane;&#8221; genealogical charts often contain lines that look remarkably similar to a &#8220;crane&#8217;s foot.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='pedigree#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Jennifer DesRochers: Pedigree Charts</strong><span> One way to represent a pedigree.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/pedigree.jpg' video_url='examplevids/pedigree' video_width='350'></span>
<div id='wt-container'>
<img alt="Pedigree" height="288" src="https://cdn1.membean.com/video/examplevids/pedigree.jpg" width="350" />
<div class='center'>
<a href="pedigree#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='pedigree#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Pedigree" src="https://cdn1.membean.com/public/images/wordimages/cons2/pedigree.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='pedigree#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>antecedent</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>beget</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>conjugal</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>consanguinity</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>echelon</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>engender</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>gestation</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>matriarch</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>nomenclature</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>patriarch</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>precursor</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>progenitor</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>progeny</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='8' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="pedigree" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>pedigree</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="pedigree#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

