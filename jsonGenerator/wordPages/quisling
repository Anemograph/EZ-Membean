
<!DOCTYPE html>
<html>
<head>
<title>Word: quisling | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An apostate has abandoned their religious faith, political party, or devotional cause.</p>
<p class='rw-defn idx1' style='display:none'>If you employ chicanery, you are devising and carrying out clever plans and trickery to cheat and deceive people.</p>
<p class='rw-defn idx2' style='display:none'>When people collaborate, they work together, usually to solve some problem or issue.</p>
<p class='rw-defn idx3' style='display:none'>Someone who is contumacious is purposely stubborn, contrary, or disobedient.</p>
<p class='rw-defn idx4' style='display:none'>A disaffected member of a group or organization is not satisfied with it; consequently, they feel little loyalty towards it.</p>
<p class='rw-defn idx5' style='display:none'>Someone who is disingenuous is not straightforward or is dishonest in what they say or do.</p>
<p class='rw-defn idx6' style='display:none'>If you accuse someone of duplicity, you think that they are dishonest and are intending to trick you.</p>
<p class='rw-defn idx7' style='display:none'>A small group or issue that is factious is controversial and promotes strong disagreement or dissatisfaction within a larger group.</p>
<p class='rw-defn idx8' style='display:none'>To foment is to encourage people to protest, fight, or cause trouble and violent opposition to something that is viewed by some as undesirable.</p>
<p class='rw-defn idx9' style='display:none'>An infamous person has a very bad reputation because they have done disgraceful or dishonorable things.</p>
<p class='rw-defn idx10' style='display:none'>Something that is insidious is dangerous because it seems harmless or not important; nevertheless, over time it gradually develops the capacity to cause harm and damage.</p>
<p class='rw-defn idx11' style='display:none'>Malfeasance is an unlawful act, especially one committed by a trusted public official.</p>
<p class='rw-defn idx12' style='display:none'>A notorious person is well-known by the public at large; they are usually famous for doing something bad.</p>
<p class='rw-defn idx13' style='display:none'>Refractory people deliberately don&#8217;t obey someone in authority and so are difficult to deal with or control.</p>
<p class='rw-defn idx14' style='display:none'>A reprobate has a bad character and behaves in an immoral or improper way.</p>
<p class='rw-defn idx15' style='display:none'>Sedition is the act of encouraging people to disobey and oppose the government currently in power.</p>
<p class='rw-defn idx16' style='display:none'>To undermine a plan or endeavor is to weaken it or make it unstable in a gradual but purposeful fashion.</p>
<p class='rw-defn idx17' style='display:none'>Someone who is venal is dishonest, corrupt, and ready to do anything for money.</p>
<p class='rw-defn idx18' style='display:none'>Wiles are clever tricks or cunning schemes that are used to persuade someone to do what you want.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 6
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>quisling</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='quisling#' id='pronounce-sound' path='audio/words/amy-quisling'></a>
KWIZ-ling
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='quisling#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='quisling#' id='context-sound' path='audio/wordcontexts/brian-quisling'></a>
Oftentimes <em>quislings</em> or traitors will see the writing on the wall and realize that they have to betray their own country in order to save themselves, and perhaps their country as well.  A <em>quisling</em> or turncoat sees no alternative but to aid the enemy; as a reward for this service, he is often asked to serve in a puppet government created by that enemy while it occupies the conquered territory.  No matter how supposedly altruistic the <em>quisling</em> or back-stabber believes his motives to be, no one in the conquered nation stoops so low as to sympathize with him.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>quisling</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A person who sympathizes with and aids enemy forces.
</li>
<li class='choice '>
<span class='result'></span>
A person who really cares about their own country and tries to save it.
</li>
<li class='choice '>
<span class='result'></span>
A person who sees what will happen in the future and tries to stop it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='quisling#' id='definition-sound' path='audio/wordmeanings/amy-quisling'></a>
A <em>quisling</em> is a traitor, in this case someone who collaborates with an invading force and later serves as a leader in a puppet government established by the occupying enemy.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>traitor</em>
</span>
</span>
</div>
<a class='quick-help' href='quisling#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='quisling#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/quisling/memory_hooks/116659.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Ques</span></span>tioning Duck<span class="emp2"><span>ling</span></span></span></span> The <span class="emp1"><span>ques</span></span>tioning duck<span class="emp2"><span>ling</span></span> who constantly <span class="emp1"><span>ques</span></span>tioned his mother was viewed by the other duck<span class="emp2"><span>ling</span></span>s as a <span class="emp1"><span>quis</span></span><span class="emp2"><span>ling</span></span>, especially when he left to live somewhere else.
</p>
</div>

<div id='memhook-button-bar'>
<a href="quisling#" id="add-public-hook" style="" url="https://membean.com/mywords/quisling/memory_hooks">Use other public hook</a>
<a href="quisling#" id="memhook-use-own" url="https://membean.com/mywords/quisling/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='quisling#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
In June, 1955, a Jerusalem district court ruled that Dr. Kastner had acted as a <b>quisling</b>. It said he had known that by his action he was sacrificing a large number of Jews to save a small group of his friends and relatives.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
There are still people in my party who believe in consensus politics. I regard them as <b>quislings</b>, as traitors . . . I mean it.
<cite class='attribution'>
&mdash;
Margaret Thatcher, British stateswoman and former Prime Minister
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/quisling/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='quisling#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>The word <em>quisling</em> is an eponym derived from Vidkun Quisling, a Norwegian politician who betrayed the country of Norway to the Nazis, and later headed Norway&#8217;s puppet government while it was under the control of the German army.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='quisling#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Quisling" src="https://cdn3.membean.com/public/images/wordimages/cons2/quisling.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='quisling#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>apostate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>chicanery</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>collaborate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>contumacious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>disaffected</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>disingenuous</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>duplicity</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>factious</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>foment</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>infamous</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>insidious</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>malfeasance</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>notorious</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>refractory</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>reprobate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>sedition</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>undermine</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>venal</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>wile</span> &middot;</li></ul>
<ul class='related-ants'></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="quisling" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>quisling</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="quisling#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

