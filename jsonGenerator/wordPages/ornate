
<!DOCTYPE html>
<html>
<head>
<title>Word: ornate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Ornate-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/ornate-large.jpg?qdep8" />
</div>
<a href="ornate#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>Something austere is simple and plain in its style; an austere person is strict and severe with themselves.</p>
<p class='rw-defn idx1' style='display:none'>Something, such as a building, is derelict if it is empty, not used, and in bad condition or disrepair.</p>
<p class='rw-defn idx2' style='display:none'>A dilapidated building, vehicle, etc. is old, broken-down, and in very bad condition.</p>
<p class='rw-defn idx3' style='display:none'>Someone who is epicurean derives great pleasure in material and sensual things, especially good food and drink.</p>
<p class='rw-defn idx4' style='display:none'>If someone or something is flamboyant, the former is trying to show off in a way that deliberately attracts attention, and the latter is brightly colored and highly decorated.</p>
<p class='rw-defn idx5' style='display:none'>Something florid has too much decoration or is too elaborate.</p>
<p class='rw-defn idx6' style='display:none'>If something is gilded, it has been deceptively given a more attractive appearance than it normally possesses; a very thin layer of gold often covers something gilded.</p>
<p class='rw-defn idx7' style='display:none'>Hedonism is the belief that pleasure is important, so much so that life should be spent doing only things that one enjoys.</p>
<p class='rw-defn idx8' style='display:none'>The word homogeneous is used to describe a group that has members or parts which are similar or are all of the same type.</p>
<p class='rw-defn idx9' style='display:none'>Something luxuriant, such as plants or hair, is growing well and is very healthy.</p>
<p class='rw-defn idx10' style='display:none'>Something mediocre is average or ordinary in quality; it’s just OK.</p>
<p class='rw-defn idx11' style='display:none'>Something minuscule is extremely small in size or amount.</p>
<p class='rw-defn idx12' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx13' style='display:none'>Something that is opulent is very impressive, grand, and is made from expensive materials.</p>
<p class='rw-defn idx14' style='display:none'>If you describe an action as ostentatious, you think it is an extreme and exaggerated way of impressing people.</p>
<p class='rw-defn idx15' style='display:none'>Animals preen when they smooth out their fur or their feathers; humans preen by making themselves beautiful in front of a mirror.</p>
<p class='rw-defn idx16' style='display:none'>Something prosaic is dull, boring, and ordinary.</p>
<p class='rw-defn idx17' style='display:none'>People or things that are resplendent are beautiful, bright, and impressive in appearance.</p>
<p class='rw-defn idx18' style='display:none'>A scintillating conversation, speech, or performance is brilliantly clever, interesting, and lively.</p>
<p class='rw-defn idx19' style='display:none'>Something that is sumptuous is impressive, grand, and very expensive.</p>
<p class='rw-defn idx20' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx21' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>ornate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='ornate#' id='pronounce-sound' path='audio/words/amy-ornate'></a>
awr-NAYT
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='ornate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='ornate#' id='context-sound' path='audio/wordcontexts/brian-ornate'></a>
I like furnishings that are simple and plain, while my sister&#8217;s elaborate house is full of <em>ornate</em> or highly decorative chairs and rugs.  She loves the <em>ornate</em> and excessively layered Victorian style, in which small rugs are laid on larger ones, and rich cloths beautify fancy furniture.  She loves <em>ornate</em>, expensive, and gold-framed pictures that cover her richly papered walls.  In my house, the only <em>ornate</em> or showy piece is a small, carved table that I inherited from my grandmother.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If someone has an <em>ornate</em> style of decorating, how do they decorate?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They use warm colors to create a cozy, welcoming environment.
</li>
<li class='choice '>
<span class='result'></span>
They avoid bright colors and use simple, plain patterns.
</li>
<li class='choice answer '>
<span class='result'></span>
They use fancy decorations and detailed, elaborate patterns.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='ornate#' id='definition-sound' path='audio/wordmeanings/amy-ornate'></a>
An <em>ornate</em> object is heavily or excessively decorated with complicated shapes and patterns.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>very fancy</em>
</span>
</span>
</div>
<a class='quick-help' href='ornate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='ornate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/ornate/memory_hooks/3424.json'></span>
<p>
<span class="emp0"><span>B<span class="emp1"><span>orn</span></span> First R<span class="emp3"><span>ate</span></span> and Elabor<span class="emp3"><span>ate</span></span></span></span> I think that my brother was b<span class="emp1"><span>orn</span></span> to love things <span class="emp1"><span>orn</span></span><span class="emp3"><span>ate</span></span>; every <span class="emp1"><span>orn</span></span>ament that ad<span class="emp1"><span>orn</span></span>s his house and office must be first r<span class="emp3"><span>ate</span></span>, elabor<span class="emp3"><span>ate</span></span>, and <span class="emp1"><span>orn</span></span><span class="emp3"><span>ate</span></span>, or he will turn up his nose at it.
</p>
</div>

<div id='memhook-button-bar'>
<a href="ornate#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/ornate/memory_hooks">Use other hook</a>
<a href="ornate#" id="memhook-use-own" url="https://membean.com/mywords/ornate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='ornate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
A simple bamboo cage may cost about 20, while a detailed cage with <b>ornate</b> Chinese carving can fetch as much as 600.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
Kultida was even prouder after the tournament, when Woods was given honorary citizenship by Thai prime minister Chavalit Yongchaiyudh on the floor of Bangkok’s <b>ornate</b> Government House.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Circling back to the Gaslamp Quarter, we jog past the <b>ornate</b> Victorian-era Horton Grand Hotel and the William Heath Davis House, one of the oldest wooden structures in San Diego.
<cite class='attribution'>
&mdash;
Seattle Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/ornate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='ornate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='orn_decorate' data-tree-url='//cdn1.membean.com/public/data/treexml' href='ornate#'>
<span class=''></span>
orn
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>decorate, beautify</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='ornate#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>An <em>ornate</em> palace has been greatly &#8220;beautified&#8221; or highly &#8220;decorated.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='ornate#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Baroquen Nails</strong><span> These are some ornate fingernails!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/ornate.jpg' video_url='examplevids/ornate' video_width='350'></span>
<div id='wt-container'>
<img alt="Ornate" height="288" src="https://cdn1.membean.com/video/examplevids/ornate.jpg" width="350" />
<div class='center'>
<a href="ornate#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='ornate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Ornate" src="https://cdn3.membean.com/public/images/wordimages/cons2/ornate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='ornate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>epicurean</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>flamboyant</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>florid</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>gilded</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>hedonism</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>luxuriant</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>opulent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>ostentatious</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>preen</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>resplendent</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>scintillating</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>sumptuous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>austere</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>derelict</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>dilapidated</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>homogeneous</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>mediocre</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>minuscule</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>prosaic</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="ornate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>ornate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="ornate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

