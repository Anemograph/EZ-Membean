
<!DOCTYPE html>
<html>
<head>
<title>Word: hermetic | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>You can describe something as abstruse if you find it highly complicated and difficult to understand.</p>
<p class='rw-defn idx1' style='display:none'>When you are apprised of something, you are given information about it.</p>
<p class='rw-defn idx2' style='display:none'>Something that is arcane is mysterious and secret, known and understood by only a few people.</p>
<p class='rw-defn idx3' style='display:none'>When you broach a subject, especially one that may be embarrassing or unpleasant, you mention it in order to begin a discussion about it.</p>
<p class='rw-defn idx4' style='display:none'>To cloister someone is to remove and isolate them from the rest of the world.</p>
<p class='rw-defn idx5' style='display:none'>A conundrum is a problem or puzzle that is difficult or impossible to solve.</p>
<p class='rw-defn idx6' style='display:none'>Covert activities or situations are secret and hidden.</p>
<p class='rw-defn idx7' style='display:none'>A desolate area is unused, empty of life, deserted, and lonely.</p>
<p class='rw-defn idx8' style='display:none'>When someone disinters a dead body, they dig it up; likewise, something disinterred is exposed or revealed to the public after having been hidden for some time.</p>
<p class='rw-defn idx9' style='display:none'>To divulge something is to reveal information that was once private or secret.</p>
<p class='rw-defn idx10' style='display:none'>If you elucidate something, you make it easier to understand by giving more relevant information.</p>
<p class='rw-defn idx11' style='display:none'>Someone or something that is enigmatic is mysterious and difficult to understand.</p>
<p class='rw-defn idx12' style='display:none'>Something esoteric is known about or understood by very few people.</p>
<p class='rw-defn idx13' style='display:none'>If you evince particular feelings, qualities, or attitudes, you show them, often clearly.</p>
<p class='rw-defn idx14' style='display:none'>If you exude a quality or feeling, people easily notice that you have a large quantity of it because it flows from you; if a smell or liquid exudes from something, it flows steadily and slowly from it.</p>
<p class='rw-defn idx15' style='display:none'>If you gloss a difficult word, phrase, or other text, you provide an explanation for it in the form of a note.</p>
<p class='rw-defn idx16' style='display:none'>An impenetrable barrier cannot be gotten through by any means; this word can refer to parts of a building such as walls and doors—or to a problem of some kind that cannot be solved.</p>
<p class='rw-defn idx17' style='display:none'>If you are impervious to things, such as someone&#8217;s actions or words, you are not affected by them or do not notice them.</p>
<p class='rw-defn idx18' style='display:none'>If you are incommunicado, you are out of touch, unable to be communicated with, or in an isolated situation.</p>
<p class='rw-defn idx19' style='display:none'>When a spy infiltrates enemy lines, they creep in or penetrate them so as to gather information.</p>
<p class='rw-defn idx20' style='display:none'>Something that is latent, such as an undiscovered talent or undiagnosed disease, exists but is not active or has not developed at the moment—it may develop or become active in the future.</p>
<p class='rw-defn idx21' style='display:none'>A metropolitan area contains a very large city.</p>
<p class='rw-defn idx22' style='display:none'>An overt act is not hidden or secret but is done in an open and public way.</p>
<p class='rw-defn idx23' style='display:none'>When a substance permeates something, it enters into all parts of it.</p>
<p class='rw-defn idx24' style='display:none'>A porous substance has many holes through which liquid can be absorbed, such as a sponge.</p>
<p class='rw-defn idx25' style='display:none'>A recluse is someone who chooses to live alone and deliberately avoids other people.</p>
<p class='rw-defn idx26' style='display:none'>Recondite areas of knowledge are those that are very difficult to understand and/or are not known by many people.</p>
<p class='rw-defn idx27' style='display:none'>Retention is the act or condition of keeping or holding on to something, including the ability to remember things.</p>
<p class='rw-defn idx28' style='display:none'>A surreptitious deed is done secretly to avoid bringing any attention to it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>hermetic</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='hermetic#' id='pronounce-sound' path='audio/words/amy-hermetic'></a>
hur-MET-ik
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='hermetic#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='hermetic#' id='context-sound' path='audio/wordcontexts/brian-hermetic'></a>
Sheryl says that in college she had an airtight, <em>hermetic</em> seal on her heart: she wouldn&#8217;t let anyone in.  Sheryl created an entirely <em>hermetic</em> world into which very few people were allowed access of any kind.  The closed-off, <em>hermetic</em> confines of her life meant that she was alone much of the time, for she did not invite advice or intimacy from even her closest friends.  She seems to feel that her isolated, <em>hermetic</em> existence kept her free from other people&#8217;s control, influence, and interference.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What might someone who is seeking a <em>hermetic</em> lifestyle do?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Avoid processed foods and eat mostly fruits and vegetables.
</li>
<li class='choice answer '>
<span class='result'></span>
Move out into the wilderness far away from others.
</li>
<li class='choice '>
<span class='result'></span>
Dedicate their life to helping others.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='hermetic#' id='definition-sound' path='audio/wordmeanings/amy-hermetic'></a>
The adjective <em>hermetic</em> describes something that is set apart, isolated, or separate from the influence or interference of society at large.
</li>
<li class='def-text'>
A container that is <em>hermetically</em> sealed allows no air to pass through.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>sealed off</em>
</span>
</span>
</div>
<a class='quick-help' href='hermetic#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='hermetic#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/hermetic/memory_hooks/5749.json'></span>
<p>
<span class="emp0"><span>S<span class="emp3"><span>tic</span></span>k of <span class="emp1"><span>Herme</span></span>s</span></span> The Greek messenger god <span class="emp1"><span>Herme</span></span>s carried a magical wand named the caduceus; a <span class="emp1"><span>herme</span></span><span class="emp3"><span>tic</span></span> society is rumored today to possesses that wondrous s<span class="emp3"><span>tic</span></span>k of <span class="emp1"><span>Herme</span></span>s, but both the caduceus and the society are kept in <span class="emp1"><span>herme</span></span><span class="emp3"><span>tic</span></span>, secretive, and closely-guarded confines.
</p>
</div>

<div id='memhook-button-bar'>
<a href="hermetic#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/hermetic/memory_hooks">Use other hook</a>
<a href="hermetic#" id="memhook-use-own" url="https://membean.com/mywords/hermetic/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='hermetic#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The revelation of this retrospective: the more <b>hermetic</b> the piece, the more marooned the character, the more hypnotic the theatrical experience.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
"There's the whole process of Riyaaz, where a great Indian classical musician will go into a <b>hermetic</b> state and will be just on their own for many years, practicing, which is something that he did," Sawhney says. "That whole idea of meditation and isolation and also being creative and constructive with that time is very, very important for all of us and we can all hear the fruition of all of that in every piece of music he ever created."
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/hermetic/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='hermetic#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>hermetic</td>
<td>
&rarr;
</td>
<td class='meaning'>alchemical</td>
</tr>
</table>
<p><em>Hermetic</em> is derived from &#8220;Hermes,&#8221; the Greek messenger and science god, and author of secret and mysterious texts on alchemy (the science of converting lead into gold).  Hermes was believed to have possessed the ability to seal treasure chests so that nothing could access them. In the 17th century English writers began using <em>hermetic</em> to mean anything that was &#8220;sealed&#8221; or &#8220;secret.&#8221;  Subsequently <em>hermetic</em> has been used to refer to something sealed so tightly that it is &#8220;airtight.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='hermetic#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Hermetic" src="https://cdn2.membean.com/public/images/wordimages/cons2/hermetic.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='hermetic#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abstruse</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>arcane</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>cloister</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>conundrum</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>covert</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>desolate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>enigmatic</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>esoteric</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>impenetrable</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>impervious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>incommunicado</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>latent</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>recluse</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>recondite</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>retention</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>surreptitious</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>apprise</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>broach</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>disinter</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>divulge</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>elucidate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>evince</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>exude</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>gloss</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>infiltrate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>metropolitan</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>overt</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>permeate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>porous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="hermetic" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>hermetic</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="hermetic#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

