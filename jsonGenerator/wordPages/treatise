
<!DOCTYPE html>
<html>
<head>
<title>Word: treatise | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Treatise-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/treatise-large.jpg?qdep8" />
</div>
<a href="treatise#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>Acumen is the ability to make quick decisions and keen, precise judgments.</p>
<p class='rw-defn idx1' style='display:none'>If you describe someone as articulate, you mean that they are able to express their thoughts, arguments, and ideas clearly and effectively.</p>
<p class='rw-defn idx2' style='display:none'>An assiduous person works hard to ensure that something is done properly and completely.</p>
<p class='rw-defn idx3' style='display:none'>A chronicle is a record of historical events that arranges those events in the correct order in which they happened.</p>
<p class='rw-defn idx4' style='display:none'>A compendium is a detailed collection of information on a particular or specific subject, usually in a book.</p>
<p class='rw-defn idx5' style='display:none'>A cursory examination or reading of something is very quick, incomplete, and does not pay close attention to detail.</p>
<p class='rw-defn idx6' style='display:none'>Something that is desultory is done in a way that is unplanned, disorganized, and without direction.</p>
<p class='rw-defn idx7' style='display:none'>A dilettante is someone who frequently pursues an interest in a subject; nevertheless, they never spend the time and effort to study it thoroughly enough to master it.</p>
<p class='rw-defn idx8' style='display:none'>When you discern something, you notice, detect, or understand it, often after thinking about it carefully or studying it for some time.  </p>
<p class='rw-defn idx9' style='display:none'>Someone who is erudite is steeped in knowledge because they have read and studied extensively.</p>
<p class='rw-defn idx10' style='display:none'>When a person is being flippant, they are not taking something as seriously as they should be; therefore, they tend to dismiss things they should respectfully pay attention to.</p>
<p class='rw-defn idx11' style='display:none'>The gravity of a situation or event is its seriousness or importance.</p>
<p class='rw-defn idx12' style='display:none'>A homily is a speech or piece of writing that advises people about how they ought to behave from a moral perspective.</p>
<p class='rw-defn idx13' style='display:none'>Someone is considered meticulous when they act with careful attention to detail.</p>
<p class='rw-defn idx14' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx15' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx16' style='display:none'>An omnibus is a book containing a collection of stories or articles that have previously been printed separately.</p>
<p class='rw-defn idx17' style='display:none'>An opus is an important piece of artistic work by a writer, painter, musician, etc.; an opus can also be one in a series of numbered musical works by the same composer.</p>
<p class='rw-defn idx18' style='display:none'>A perfunctory action is done with little care or interest; consequently, it is only completed because it is expected of someone to do so.</p>
<p class='rw-defn idx19' style='display:none'>If you peruse some written text, you read it over carefully.</p>
<p class='rw-defn idx20' style='display:none'>When someone exhibits profundity, they display great intellectual depth and understanding; profundity can also be the depth or complexity of something.</p>
<p class='rw-defn idx21' style='display:none'>To be punctilious is to pay precise attention to detail.</p>
<p class='rw-defn idx22' style='display:none'>A sagacious person is wise, intelligent, and has the ability to make good practical decisions.</p>
<p class='rw-defn idx23' style='display:none'>Someone who is sedulous works hard and performs tasks very carefully and thoroughly, not stopping their work until it is completely accomplished.</p>
<p class='rw-defn idx24' style='display:none'>If you transcribe something, such as a speech or other text, you write it or type it in full.</p>
<p class='rw-defn idx25' style='display:none'>If you refer to something as a trifle, you mean that it is of little or no importance or value.</p>
<p class='rw-defn idx26' style='display:none'>A trite remark or idea is so overused that it is no longer interesting or novel.</p>
<p class='rw-defn idx27' style='display:none'>If someone is unlettered, they are illiterate, unable to read and write.</p>
<p class='rw-defn idx28' style='display:none'>Verbiage is an excessive use of words to convey something that could be expressed using fewer words; it can also be the manner or style in which someone uses words.</p>
<p class='rw-defn idx29' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>
<p class='rw-defn idx30' style='display:none'>A writ is an official document that orders a person to do something.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>treatise</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='treatise#' id='pronounce-sound' path='audio/words/amy-treatise'></a>
TREE-tis
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='treatise#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='treatise#' id='context-sound' path='audio/wordcontexts/brian-treatise'></a>
Tom&#8217;s extensive academic <em>treatise</em> on the Roman military proved to be a highly important text in his chosen field.  His <em>treatise</em> or written study had a great deal of new information that he had acquired over many years of intensive research and great effort.  There was little wonder why his detailed and informative <em>treatise</em> earned him a great deal of respect amongst his peers.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Who might create a <em>treatise</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A famous actress who wants to tell the public about the events of her life.
</li>
<li class='choice '>
<span class='result'></span>
A government leader who wants to establish peace with a neighboring country.
</li>
<li class='choice answer '>
<span class='result'></span>
A professor who wants to write a formal paper on a serious topic.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='treatise#' id='definition-sound' path='audio/wordmeanings/amy-treatise'></a>
A <em>treatise</em> is a formal written work that considers and examines a particular subject in a very systematic and extensive way.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>written study</em>
</span>
</span>
</div>
<a class='quick-help' href='treatise#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='treatise#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/treatise/memory_hooks/4092.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Treat</span></span> Disgu<span class="emp1"><span>ise</span></span></span></span> The master assassin had written a <span class="emp3"><span>treat</span></span><span class="emp1"><span>ise</span></span> on how to disgu<span class="emp1"><span>ise</span></span> edible <span class="emp3"><span>treat</span></span>s that victims would readily <span class="emp3"><span>eat</span></span> and not even consider that the disgu<span class="emp1"><span>ise</span></span>d <span class="emp3"><span>treat</span></span> would lead to their fatal dem<span class="emp1"><span>ise</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="treatise#" id="add-public-hook" style="" url="https://membean.com/mywords/treatise/memory_hooks">Use other public hook</a>
<a href="treatise#" id="memhook-use-own" url="https://membean.com/mywords/treatise/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='treatise#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Scholars believe the <b>treatise</b> was copied by a scribe in the 10th century from Archimedes' original Greek scrolls, written in the third century B.C..
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
Frye’s witty <b>treatise</b> traces "the modern science of cereal studies" from the publication of Linnaeus’s De Cerialibus in 1764 to the 20th century.
<cite class='attribution'>
&mdash;
Epicurious
</cite>
</li>
<li>
If there’s a sonnet or a patent or an economic <b>treatise</b> that has been produced by one of these media hounds, it has not yet surfaced.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/treatise/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='treatise#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='treat_drawn' data-tree-url='//cdn1.membean.com/public/data/treexml' href='treatise#'>
<span class=''></span>
treat
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>drawn</td>
</tr>
</table>
<p>A <em>treatise</em> is a written account of a subject that is carefully &#8220;drawn&#8221; up.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='treatise#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: The School of Life</strong><span> John Locke's two treatises of government.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/treatise.jpg' video_url='examplevids/treatise' video_width='350'></span>
<div id='wt-container'>
<img alt="Treatise" height="288" src="https://cdn1.membean.com/video/examplevids/treatise.jpg" width="350" />
<div class='center'>
<a href="treatise#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='treatise#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Treatise" src="https://cdn2.membean.com/public/images/wordimages/cons2/treatise.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='treatise#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>acumen</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>articulate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>assiduous</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>chronicle</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>compendium</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>discern</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>erudite</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>gravity</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>homily</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>meticulous</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>omnibus</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>opus</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>peruse</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>profundity</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>punctilious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>sagacious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>sedulous</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>transcribe</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>verbiage</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>writ</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='5' class = 'rw-wordform notlearned'><span>cursory</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>desultory</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>dilettante</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>flippant</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>perfunctory</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>trifle</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>trite</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>unlettered</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="treatise" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>treatise</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="treatise#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

