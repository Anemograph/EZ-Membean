
<!DOCTYPE html>
<html>
<head>
<title>Word: nettle | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>Someone who has an abrasive manner is unkind and rude, wearing away at you in an irritating fashion.</p>
<p class='rw-defn idx2' style='display:none'>An affliction is something that causes pain and mental suffering, especially a medical condition.</p>
<p class='rw-defn idx3' style='display:none'>When you ameliorate a bad condition or situation, you make it better in some way.</p>
<p class='rw-defn idx4' style='display:none'>Approbation is official praise or approval of something.</p>
<p class='rw-defn idx5' style='display:none'>When something bedevils you, it causes you a lot of problems and difficulties over a period of time.</p>
<p class='rw-defn idx6' style='display:none'>If you are beset by something, you are experiencing serious problems or dangers because of it.</p>
<p class='rw-defn idx7' style='display:none'>Someone who has a bilious personality is highly irritable and bad-tempered.</p>
<p class='rw-defn idx8' style='display:none'>If you describe a person&#8217;s behavior as brazen, you mean that they are not embarrassed by anything they do—they simply don&#8217;t care what other people think about them.</p>
<p class='rw-defn idx9' style='display:none'>If you condole with someone, you express sympathy or sorrow, usually on the death of someone dear.</p>
<p class='rw-defn idx10' style='display:none'>If someone will countenance something, they will approve, tolerate, or support it.</p>
<p class='rw-defn idx11' style='display:none'>If something discomfits you, it makes you feel embarrassed, confused, uncomfortable, or frustrated.</p>
<p class='rw-defn idx12' style='display:none'>When someone feels disquiet about a situation, they feel very worried or nervous.</p>
<p class='rw-defn idx13' style='display:none'>An emollient substance soothes your skin and provides moisture.</p>
<p class='rw-defn idx14' style='display:none'>If something encumbers you, it makes it difficult for you to move freely or do what you want.</p>
<p class='rw-defn idx15' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx16' style='display:none'>When you exasperate another person, you annoy or anger them a great deal because you keep on doing something that is highly irritating.</p>
<p class='rw-defn idx17' style='display:none'>To foment is to encourage people to protest, fight, or cause trouble and violent opposition to something that is viewed by some as undesirable.</p>
<p class='rw-defn idx18' style='display:none'>If someone is imperturbable, they are always calm and not easily upset or disturbed by any situation, even dangerous ones.</p>
<p class='rw-defn idx19' style='display:none'>When you are indignant about something, you are angry or really annoyed about it.</p>
<p class='rw-defn idx20' style='display:none'>An irascible person becomes angry very easily.</p>
<p class='rw-defn idx21' style='display:none'>Malaise is a feeling of discontent, general unease, or even illness in a person or group for which there does not seem to be a quick and easy solution because the cause of it is not clear.</p>
<p class='rw-defn idx22' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx23' style='display:none'>If you mollify someone, you say or do something to make that person feel less angry or upset.</p>
<p class='rw-defn idx24' style='display:none'>A task or responsibility is onerous if you dislike having to do it because it is difficult or tiring.</p>
<p class='rw-defn idx25' style='display:none'>When you are piqued by something, either your curiosity is aroused by it or you feel resentment or anger towards it.</p>
<p class='rw-defn idx26' style='display:none'>A person who is solicitous behaves in a way that shows great concern about someone&#8217;s health, feelings, safety, etc.</p>
<p class='rw-defn idx27' style='display:none'>When you take umbrage, you take offense at what another has done.</p>
<p class='rw-defn idx28' style='display:none'>An untoward situation is something that is unfavorable, unfortunate, inappropriate, or troublesome.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>nettle</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='nettle#' id='pronounce-sound' path='audio/words/amy-nettle'></a>
NET-l
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='nettle#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='nettle#' id='context-sound' path='audio/wordcontexts/brian-nettle'></a>
I am going to have to find a new roommate since everything my current one does has begun to <em>nettle</em> or annoy me.  She is nice, but her upsetting habits have started to put me on edge and <em>nettle</em> me to the breaking point.  The thing that irritates or <em>nettles</em> me the most is when she hums the same song, over and over again, in an annoying, irritating way.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How would someone react if you <em>nettled</em> them?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They would probably thank you for helping them with a problem.
</li>
<li class='choice answer '>
<span class='result'></span>
They would likely tell you to stop bothering them so much.
</li>
<li class='choice '>
<span class='result'></span>
They would likely be in pain and need to be taken to a hospital. 
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='nettle#' id='definition-sound' path='audio/wordmeanings/amy-nettle'></a>
If you <em>nettle</em> someone, you irritate or annoy them.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>annoy</em>
</span>
</span>
</div>
<a class='quick-help' href='nettle#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='nettle#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/nettle/memory_hooks/4099.json'></span>
<p>
<span class="emp0"><span>K<span class="emp3"><span>ettle</span></span> of Fish</span></span> I couldn't believe it when my mother boiled a k<span class="emp3"><span>ettle</span></span> of fish for my birthday; that k<span class="emp3"><span>ettle</span></span> of fish n<span class="emp3"><span>ettle</span></span>d me to no end because she took the fish from my aquarium!
</p>
</div>

<div id='memhook-button-bar'>
<a href="nettle#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/nettle/memory_hooks">Use other hook</a>
<a href="nettle#" id="memhook-use-own" url="https://membean.com/mywords/nettle/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/nettle/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='nettle#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>From the plant, the &#8220;nettle,&#8221; which has stingers on it that irritate the skin.</p>
<a href="nettle#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> Old English</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='nettle#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>D.O.A. (1950)</strong><span> The questioner is beginning to nettle him.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/nettle.jpg' video_url='examplevids/nettle' video_width='350'></span>
<div id='wt-container'>
<img alt="Nettle" height="288" src="https://cdn1.membean.com/video/examplevids/nettle.jpg" width="350" />
<div class='center'>
<a href="nettle#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='nettle#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Nettle" src="https://cdn1.membean.com/public/images/wordimages/cons2/nettle.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='nettle#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>abrasive</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>affliction</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>bedevil</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>beset</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>bilious</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>discomfit</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>disquiet</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>encumber</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>exasperate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>foment</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>indignant</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>irascible</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>malaise</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>onerous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>pique</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>solicitous</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>umbrage</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>untoward</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>ameliorate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>approbation</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>brazen</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>condole</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>countenance</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>emollient</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>imperturbable</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>mollify</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='nettle#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
nettlesome
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>annoying</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="nettle" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>nettle</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="nettle#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

