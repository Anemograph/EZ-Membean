
<!DOCTYPE html>
<html>
<head>
<title>Word: facile | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you are adept at something, you are very good, skillful, or talented at it.</p>
<p class='rw-defn idx1' style='display:none'>Someone who is adroit is skillful and clever, especially in thought, behavior, or action.</p>
<p class='rw-defn idx2' style='display:none'>If you describe someone as articulate, you mean that they are able to express their thoughts, arguments, and ideas clearly and effectively.</p>
<p class='rw-defn idx3' style='display:none'>If something moves or grows with celerity, it does so rapidly.</p>
<p class='rw-defn idx4' style='display:none'>A cursory examination or reading of something is very quick, incomplete, and does not pay close attention to detail.</p>
<p class='rw-defn idx5' style='display:none'>A deft movement or action is made quickly and with skill.</p>
<p class='rw-defn idx6' style='display:none'>If something encumbers you, it makes it difficult for you to move freely or do what you want.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is exacting expects others to work very hard and carefully.</p>
<p class='rw-defn idx8' style='display:none'>When you expedite an action or process, you make it happen more quickly.</p>
<p class='rw-defn idx9' style='display:none'>When a person is being flippant, they are not taking something as seriously as they should be; therefore, they tend to dismiss things they should respectfully pay attention to.</p>
<p class='rw-defn idx10' style='display:none'>If someone is being glib, they make something sound simple, easy, and problem-free— when it isn&#8217;t at all.</p>
<p class='rw-defn idx11' style='display:none'>If an idea or thought is incisive, it is expressed in a penetrating and knowledgeable manner that is clear and brief; additionally, it can demonstrate impressive understanding of related ideas or thoughts.</p>
<p class='rw-defn idx12' style='display:none'>A laborious job or process takes a long time, requires a lot of effort, and is often boring.</p>
<p class='rw-defn idx13' style='display:none'>If you describe a situation or process as labyrinthine, you mean that it is very complicated, involved, and difficult to understand.</p>
<p class='rw-defn idx14' style='display:none'>Someone who is malleable is easily influenced or controlled by other people.</p>
<p class='rw-defn idx15' style='display:none'>Someone who is obdurate is stubborn, unreasonable, or uncontrollable; hence, they simply refuse to change their behavior, actions, or beliefs to fit any situation whatsoever.</p>
<p class='rw-defn idx16' style='display:none'>Something is opaque if it is either difficult to understand or is not transparent.</p>
<p class='rw-defn idx17' style='display:none'>A perfunctory action is done with little care or interest; consequently, it is only completed because it is expected of someone to do so.</p>
<p class='rw-defn idx18' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx19' style='display:none'>A person who is plastic can be easily influenced and molded by others.</p>
<p class='rw-defn idx20' style='display:none'>A recalcitrant animal or person is difficult to control and refuses to obey orders—even after stiff punishment.</p>
<p class='rw-defn idx21' style='display:none'>Refractory people deliberately don&#8217;t obey someone in authority and so are difficult to deal with or control.</p>
<p class='rw-defn idx22' style='display:none'>Something that is specious seems to be good, sound, or correct but is actually wrong or fake.</p>
<p class='rw-defn idx23' style='display:none'>A subtle point is so clever or small that it is hard to notice or understand; it can also be very wise or deep in meaning.</p>
<p class='rw-defn idx24' style='display:none'>A person or subject that is superficial is shallow, without depth, obvious, and concerned only with surface matters.</p>
<p class='rw-defn idx25' style='display:none'>A veneer is a thin layer, such as a thin sheet of expensive wood over a cheaper type of wood that gives a false appearance of higher quality; a person can also put forth a false front or veneer.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>facile</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='facile#' id='pronounce-sound' path='audio/words/amy-facile'></a>
FAS-il
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='facile#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='facile#' id='context-sound' path='audio/wordcontexts/brian-facile'></a>
Dan found the economist&#8217;s interpretation of the complex situation on Wall Street to be <em>facile</em> or too simplistic.  The downward trend on the stock market was not just a temporary slump that would repair itself; such a <em>facile</em>, thoughtless remark proved the writer&#8217;s lack of informed commentary.  Dan believed that the economic situation was much more complex and driven by many difficult things, rather than something that could be explained with <em>facile</em>, surface, and quickly-formed arguments.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If your teacher said your science project was <em>facile</em>, what did they mean?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It was brilliant and well done.
</li>
<li class='choice answer '>
<span class='result'></span>
It lacked depth and effort.
</li>
<li class='choice '>
<span class='result'></span>
It was nowhere to be found.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='facile#' id='definition-sound' path='audio/wordmeanings/amy-facile'></a>
If you criticize someone&#8217;s arguments as being <em>facile</em>, you consider their ideas simplistic and not well thought through.
</li>
<li class='def-text'>
<em>Facile</em> can also simply mean &#8220;easy.&#8221;
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>simplistic</em>
</span>
</span>
</div>
<a class='quick-help' href='facile#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='facile#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/facile/memory_hooks/5075.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Fac</span></span>ts on F<span class="emp3"><span>ile</span></span></span></span> Reading through the entire <span class="emp2"><span>Fac</span></span>ts on F<span class="emp3"><span>ile</span></span> database is a <span class="emp2"><span>fac</span></span><span class="emp3"><span>ile</span></span> solution to world hunger, Jeremy, but it may be a good start.
</p>
</div>

<div id='memhook-button-bar'>
<a href="facile#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/facile/memory_hooks">Use other hook</a>
<a href="facile#" id="memhook-use-own" url="https://membean.com/mywords/facile/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='facile#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
"Humble sincerity is needed in order not to deny the sins of the past, and at the same time not to indulge in <b>facile</b> accusations in absence of real evidence, or without regard for the different preconceptions of the time."
<cite class='attribution'>
&mdash;
CBS News
</cite>
</li>
<li>
A <b>facile</b> conclusion might be that real elections should be conducted by phone, text message and other new media, like television voting.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Comparing the frustrations of Charleston, who never had the chance to play in the major leagues, to the success of Cobb is a clumsy device even in the hands of a <b>facile</b> playwright.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Sometimes positive behaviors were so obvious, I couldn’t not acknowledge them—<b>facile</b> use of technology to enhance a project, composing a music score as part of an in-class assignment, embracing their character in a role play.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/facile/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='facile#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='facil_easy' data-tree-url='//cdn1.membean.com/public/data/treexml' href='facile#'>
<span class=''></span>
facil
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>easy, easily accomplished</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='facile#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>A <em>facile</em> solution has been too &#8220;easily accomplished.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='facile#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Shameless</strong><span> This student earned a D because his paper was facile.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/facile.jpg' video_url='examplevids/facile' video_width='350'></span>
<div id='wt-container'>
<img alt="Facile" height="288" src="https://cdn1.membean.com/video/examplevids/facile.jpg" width="350" />
<div class='center'>
<a href="facile#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='facile#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Facile" src="https://cdn3.membean.com/public/images/wordimages/cons2/facile.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='facile#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adept</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>adroit</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>articulate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>celerity</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>cursory</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>deft</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>expedite</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>flippant</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>glib</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>malleable</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>perfunctory</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>plastic</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>specious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>superficial</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>veneer</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='6' class = 'rw-wordform notlearned'><span>encumber</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>exacting</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>incisive</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>laborious</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>labyrinthine</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>obdurate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>opaque</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>recalcitrant</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>refractory</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>subtle</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="facile" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>facile</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="facile#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

