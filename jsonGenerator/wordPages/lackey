
<!DOCTYPE html>
<html>
<head>
<title>Word: lackey | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abase yourself, people respect you less because you act in a way that is beneath you; due to this behavior, you are lowered or reduced in rank, esteem, or reputation. </p>
<p class='rw-defn idx1' style='display:none'>The word abject emphasizes a very bad situation or quality, thereby making it even worse.</p>
<p class='rw-defn idx2' style='display:none'>An acolyte is someone who serves a leader as a devoted assistant or believes in the leader&#8217;s ideas.</p>
<p class='rw-defn idx3' style='display:none'>An autonomous person makes their own decisions without being influenced by anyone else; an autonomous country or organization is independent and has the power to govern itself.</p>
<p class='rw-defn idx4' style='display:none'>Someone who is contumacious is purposely stubborn, contrary, or disobedient.</p>
<p class='rw-defn idx5' style='display:none'>If you behave with deference towards someone, you show them respect and accept their opinions or decisions, especially because they have an important position.</p>
<p class='rw-defn idx6' style='display:none'>If you say a person&#8217;s actions are despicable, you think they are extremely unpleasant or nasty.</p>
<p class='rw-defn idx7' style='display:none'>A dissident is someone who disagrees publicly with a government, especially in a country where this is not allowed.</p>
<p class='rw-defn idx8' style='display:none'>If someone is fractious, they are easily upset or annoyed over unimportant things.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is incorrigible has bad habits or does bad things and is unlikely to ever change; this word is often used in a humorous way.</p>
<p class='rw-defn idx10' style='display:none'>A minion is an unimportant person who merely obeys the orders of someone else; they  usually perform boring tasks that require little skill.</p>
<p class='rw-defn idx11' style='display:none'>Obeisance is respect and obedience shown to someone or something, expressed by bowing or some other humble gesture.</p>
<p class='rw-defn idx12' style='display:none'>If someone is being obsequious, they are trying so hard to please someone that they lack sincerity in their actions towards that person.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is obstreperous is noisy, unruly, and difficult to control.</p>
<p class='rw-defn idx14' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx15' style='display:none'>A recalcitrant animal or person is difficult to control and refuses to obey orders—even after stiff punishment.</p>
<p class='rw-defn idx16' style='display:none'>Refractory people deliberately don&#8217;t obey someone in authority and so are difficult to deal with or control.</p>
<p class='rw-defn idx17' style='display:none'>If you say that someone is servile, you don&#8217;t respect them because they are too obedient, too agreeable, and too willing to do anything for another person.</p>
<p class='rw-defn idx18' style='display:none'>If you are subservient, you are too eager and willing to do what other people want and often put your own wishes aside.</p>
<p class='rw-defn idx19' style='display:none'>If you are supine, you are lying on your back with your face upward.</p>
<p class='rw-defn idx20' style='display:none'>Sycophants praise people in authority or those who have considerable influence in order to seek some favor from them in return.</p>
<p class='rw-defn idx21' style='display:none'>An unctuous person acts in an overly deceptive manner that is obviously insincere because they want to convince you of something.</p>
<p class='rw-defn idx22' style='display:none'>In the Middle Ages, a vassal was a worker who served a lord in exchange for land to live upon; today you are a vassal if you serve another and are largely controlled by them.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>lackey</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='lackey#' id='pronounce-sound' path='audio/words/amy-lackey'></a>
LAK-ee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='lackey#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='lackey#' id='context-sound' path='audio/wordcontexts/brian-lackey'></a>
Trevor was more like Mr. Corrigan&#8217;s <em>lackey</em> or obedient servant than a simple employee.  He followed behind Mr. Corrigan, took detailed notes, and obeyed commands exactly, behaving more like a <em>lackey</em> or too-willing attendant than a business assistant.  Others working at Corrigan&#8217;s found the <em>lackey</em>&#8217;s excessively flattering conduct and thoughtless actions to be insulting to the professionalism of the company.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>lackey</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Someone who is motivated to work for the public.
</li>
<li class='choice '>
<span class='result'></span>
Someone who wants to work hard to impress their boss.
</li>
<li class='choice answer '>
<span class='result'></span>
Someone who will do anything their boss demands.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='lackey#' id='definition-sound' path='audio/wordmeanings/amy-lackey'></a>
A <em>lackey</em> is a person who follows their superior&#8217;s orders so completely that they never openly question those commands.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>unquestioning servant</em>
</span>
</span>
</div>
<a class='quick-help' href='lackey#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='lackey#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/lackey/memory_hooks/4194.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Lac</span></span>king the <span class="emp2"><span>Key</span></span></span></span> Don't people who try to be <span class="emp3"><span>lac</span></span><span class="emp2"><span>key</span></span>s realize that people can see right through them?  <span class="emp3"><span>Lac</span></span><span class="emp2"><span>key</span></span>s clearly <span class="emp3"><span>lac</span></span>k the <span class="emp2"><span>key</span></span> for success, for no one respects someone you can walk all over.
</p>
</div>

<div id='memhook-button-bar'>
<a href="lackey#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/lackey/memory_hooks">Use other hook</a>
<a href="lackey#" id="memhook-use-own" url="https://membean.com/mywords/lackey/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='lackey#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
While Pakistan’s new leadership will likely share America’s desire to rein in extremists, experts say, they will want to distance themselves from the perception that they are Washington’s <b>lackey</b>, which is the general view of Mr. Musharraf here.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
Shame on Amazon UK and their new Alexa commercial that asks Alexa to stop playing Mozart and start playing “something more fun.” . . . I get it. Amazon is portraying the every-man as a monarch in charge of his domestic castle. The average man can tell his <b>lackey</b> (Alexa) to “play something more fun” and Alexa does his bidding. Why does Mozart have to be dragged through the mud in this case?
<cite class='attribution'>
&mdash;
San Diego Reader
</cite>
</li>
<li>
In its free form, the app lets you view, tag and rate images as they are snapped in-studio. This feature is aimed less at the photographer and more at those people who bother him as he tries to go about his work. . . . [T]hanks to the feature that allows the app to be used from "remote locations," the parasite can sit in the coffee shop across the street and have their <b>lackey</b> call you and direct the shoot.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
Hysterics surrounding officiating predate the digital age, but the immediacy of the present day has created a hyper-charged world in which players, execs and any fan with a wireless signal can rewatch a missed call on loop, stew in their exasperation, text a friend who shares a belief that a specific referee is a <b>lackey</b> for the NBA, fuel that mutual ire . . . then watch it again.
<cite class='attribution'>
&mdash;
ESPN
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/lackey/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='lackey#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p><em>Lackey</em> comes from a root word meaning &#8220;servant.&#8221;</p>
<a href="lackey#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> Arabic</strong>
<span id='wordorigin-map'></span>
<span>When the Spanish conquered the Moors, whose military leaders were called <em>al-kaid</em>, or &#8220;chief, leader,&#8221; they made them into slaves, and corrupted their names to <em>alcayo</em>, from which we get our word &#8220;lackey.&#8221;</span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='lackey#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Lackey" src="https://cdn1.membean.com/public/images/wordimages/cons2/lackey.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='lackey#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abase</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abject</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>acolyte</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>deference</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>despicable</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>minion</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>obeisance</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>obsequious</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>servile</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>subservient</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>supine</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>sycophant</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>unctuous</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>vassal</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>autonomous</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>contumacious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>dissident</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>fractious</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>incorrigible</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>obstreperous</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>recalcitrant</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>refractory</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="lackey" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>lackey</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="lackey#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

