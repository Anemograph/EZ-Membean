
<!DOCTYPE html>
<html>
<head>
<title>Word: dissolution | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>The process of ablation is the removal of diseased organs or harmful substances from the body, often through surgical procedure.</p>
<p class='rw-defn idx1' style='display:none'>If you abridge a book or play, you make it shorter by making cuts to the original.</p>
<p class='rw-defn idx2' style='display:none'>Accretion is the slow, gradual process by which new things are added and something gets bigger.</p>
<p class='rw-defn idx3' style='display:none'>An aggregate is the final or sum total after many different amounts or scores have been added together.</p>
<p class='rw-defn idx4' style='display:none'>When two or more things, such as organizations, amalgamate, they combine to become one large thing.</p>
<p class='rw-defn idx5' style='display:none'>A state of anarchy occurs when there is no organization or order in a nation or country, especially when no effective government exists.</p>
<p class='rw-defn idx6' style='display:none'>Attrition is the process of gradually decreasing the strength of something (such as an enemy force) by continually attacking it.</p>
<p class='rw-defn idx7' style='display:none'>If two or more things coalesce, they come together to form a single larger unit.</p>
<p class='rw-defn idx8' style='display:none'>A coalition is a temporary union of different political or social groups that agrees to work together to achieve a shared aim.</p>
<p class='rw-defn idx9' style='display:none'>A demise can be the death of someone or the slow end of something.</p>
<p class='rw-defn idx10' style='display:none'>When something is diffuse, it is widely spread out and scattered.</p>
<p class='rw-defn idx11' style='display:none'>When you are in a state of disarray, you are disorganized, disordered, and in a state of confusion.</p>
<p class='rw-defn idx12' style='display:none'>When you dismantle something, you take it apart or destroy it piece by piece. </p>
<p class='rw-defn idx13' style='display:none'>When you dispel a thought from your mind, you cause it to go away or disappear; when you do the same to a crowd, you cause it to scatter into different directions.</p>
<p class='rw-defn idx14' style='display:none'>Entropy is the lack of organization or measure of disorder currently in a system.</p>
<p class='rw-defn idx15' style='display:none'>Something ephemeral, such as some insects or a sunset, lasts for only a short time or has a very short lifespan.</p>
<p class='rw-defn idx16' style='display:none'>Something that is evanescent lasts for only a short time before disappearing from sight or memory.</p>
<p class='rw-defn idx17' style='display:none'>An inaugural event celebrates the beginning of something, such as the term of a president or the beginning of a series of meetings.</p>
<p class='rw-defn idx18' style='display:none'>An inception of something is its beginning or start.</p>
<p class='rw-defn idx19' style='display:none'>If an idea, plan, or attitude is inchoate, it is vague and imperfectly formed because it is just starting to develop.</p>
<p class='rw-defn idx20' style='display:none'>An incipient situation or quality is just beginning to appear or develop.</p>
<p class='rw-defn idx21' style='display:none'>To initiate something is to begin or start it.</p>
<p class='rw-defn idx22' style='display:none'>If you describe something as moribund, you imply that it is no longer effective; it may also be coming to the end of its useful existence.</p>
<p class='rw-defn idx23' style='display:none'>Something that is nascent is just starting to develop and is expected to become stronger and bigger in time.</p>
<p class='rw-defn idx24' style='display:none'>A nexus is a connection or a series of connections between a number of people, things, or ideas that often form the center of a system or situation.</p>
<p class='rw-defn idx25' style='display:none'>If something is obsolescent, it is slowly becoming no longer needed because something newer or more effective has been invented.</p>
<p class='rw-defn idx26' style='display:none'>Something that is pristine has not lost any of its original perfection or purity.</p>
<p class='rw-defn idx27' style='display:none'>A seminal article, book, piece of music or other work has many new ideas; additionally, it has great influence on generating ideas for future work.</p>
<p class='rw-defn idx28' style='display:none'>When you succumb to something, you give in to, yield to, or die from it.</p>
<p class='rw-defn idx29' style='display:none'>If something, such as clothing, is in vogue, it is currently in fashion or is one of the hottest new trends; hence, it is very popular.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>dissolution</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='dissolution#' id='pronounce-sound' path='audio/words/amy-dissolution'></a>
dis-uh-LOO-shuhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='dissolution#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='dissolution#' id='context-sound' path='audio/wordcontexts/brian-dissolution'></a>
When he turned thirteen, Finnegan ordered a formal ending or <em>dissolution</em> of his club &#8220;The Boys&#8217; Haven.&#8221;  When questioned by his friends, Finnegan admitted that the sudden <em>dissolution</em> or split-up of their society was due to a pretty girl named Elise whom he had just met, and who wanted to form another club with him.  While the club&#8217;s members were shocked at this new development, Finnegan merely grinned, shrugged, and with great haste signed the official papers that marked the club&#8217;s <em>dissolution</em> or official end.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of <em>dissolution</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A medical treatment that doesn&#8217;t work.
</li>
<li class='choice '>
<span class='result'></span>
A call for a leader to step down.
</li>
<li class='choice answer '>
<span class='result'></span>
A rock band that breaks up.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='dissolution#' id='definition-sound' path='audio/wordmeanings/amy-dissolution'></a>
<em>Dissolution</em> is the breaking up or official end of a group, such as a couple or institution; it can also be the act of separating something into smaller components.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>breaking up</em>
</span>
</span>
</div>
<a class='quick-help' href='dissolution#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='dissolution#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/dissolution/memory_hooks/5815.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Dis</span></span>h <span class="emp3"><span>Solution</span></span></span></span> I hate washing <span class="emp2"><span>dis</span></span>hes, so I've decided to start a campaign to call for the <span class="emp2"><span>dis</span></span><span class="emp3"><span>solution</span></span> of all <span class="emp2"><span>dis</span></span>h makers in the world; if no more <span class="emp2"><span>dis</span></span>hes are made, we won't have to wash any more when our current ones eventually break, a truly fabulous <span class="emp3"><span>solution</span></span> to a huge waste of time.
</p>
</div>

<div id='memhook-button-bar'>
<a href="dissolution#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/dissolution/memory_hooks">Use other hook</a>
<a href="dissolution#" id="memhook-use-own" url="https://membean.com/mywords/dissolution/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='dissolution#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
V6, a male idol supergroup associated with the once all-powerful Johnny & Associates agency, has announced its break-up, effective Nov. 1. . . . Following the group’s <b>dissolution</b>, the five members remaining with the agency will pursue solo careers.
<cite class='attribution'>
&mdash;
Variety
</cite>
</li>
<li>
But the large-scale <b>dissolution</b> of the gig economy for artists during lockdown has meant a radical sea change. Like so many other creators, Potter has had to reimagine his life, his career and his art.
<cite class='attribution'>
&mdash;
ABC News
</cite>
</li>
<li>
It is a mistake to regard age as a downhill grade toward <b>dissolution</b>. The reverse is true. As one grows older, one climbs with surprising strides.
<cite class='attribution'>
&mdash;
Amantine Lucile Aurore Dupin, know by the pen name George Sand, Nineteenth century French novelist
</cite>
</li>
<li>
As so many of us know, the <b>dissolution</b> of any marriage is a sad and painful process. . . . I am now filing for divorce. This came after many unsuccessful efforts at reconciliation, yet I am still dedicated to keeping the process that lies ahead peaceful for our family.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/dissolution/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='dissolution#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='di_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='dissolution#'>
<span class=''></span>
di-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='solut_loosened' data-tree-url='//cdn1.membean.com/public/data/treexml' href='dissolution#'>
<span class=''></span>
solut
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>loosened, untied</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ion_act' data-tree-url='//cdn1.membean.com/public/data/treexml' href='dissolution#'>
<span class=''></span>
-ion
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>act, state, or result of doing something</td>
</tr>
</table>
<p>An organization or substance that undergoes <em>dissolution</em> becomes &#8220;thoroughly untied or loosened.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='dissolution#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Infolaw</strong><span> Discussing the dissolution of a business.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/dissolution.jpg' video_url='examplevids/dissolution' video_width='350'></span>
<div id='wt-container'>
<img alt="Dissolution" height="288" src="https://cdn1.membean.com/video/examplevids/dissolution.jpg" width="350" />
<div class='center'>
<a href="dissolution#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='dissolution#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Dissolution" src="https://cdn0.membean.com/public/images/wordimages/cons2/dissolution.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='dissolution#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>ablation</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abridge</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>anarchy</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>attrition</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>demise</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>diffuse</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>disarray</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>dismantle</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>dispel</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>entropy</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>ephemeral</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>evanescent</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>moribund</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>obsolescent</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>succumb</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>accretion</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>aggregate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>amalgamate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>coalesce</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>coalition</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>inaugural</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>inception</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>inchoate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>incipient</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>initiate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>nascent</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>nexus</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>pristine</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>seminal</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>vogue</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='dissolution#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
dissolute
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>excessively participating in immoral activities</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="dissolution" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>dissolution</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="dissolution#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

