
<!DOCTYPE html>
<html>
<head>
<title>Word: unsullied | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you adulterate something, you lessen its quality by adding inferior ingredients or elements to it.</p>
<p class='rw-defn idx1' style='display:none'>Something brackish is distasteful and unpleasant usually because something else is mixed in with it; brackish water, for instance, is a mixture of fresh and salt water and cannot be drunk.</p>
<p class='rw-defn idx2' style='display:none'>A captious person has a fondness for catching others at fault; hence, they are overly critical and raise unwarranted objections too often.</p>
<p class='rw-defn idx3' style='display:none'>To cavil is to make unnecessary complaints about things that are unimportant.</p>
<p class='rw-defn idx4' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx5' style='display:none'>When you debunk someone&#8217;s statement, you show that it is false, thereby exposing the truth of the matter.</p>
<p class='rw-defn idx6' style='display:none'>To defile something holy or important is to do something to it or say something about it that is highly offensive to others.</p>
<p class='rw-defn idx7' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx8' style='display:none'>If you disparage someone or something, you say unpleasant words that show you have no respect for that person or thing.</p>
<p class='rw-defn idx9' style='display:none'>A fallacy is an idea or belief that is false.</p>
<p class='rw-defn idx10' style='display:none'>Something that is immaculate is very clean, pure, or completely free from error.</p>
<p class='rw-defn idx11' style='display:none'>If you describe someone&#8217;s appearance or behavior as impeccable, you mean that it is perfect and therefore impossible to criticize.</p>
<p class='rw-defn idx12' style='display:none'>Something that has inestimable value or benefit has so much of it that it cannot be calculated.</p>
<p class='rw-defn idx13' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx14' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx15' style='display:none'>If you malign someone, you say unpleasant things about them to damage their reputation.</p>
<p class='rw-defn idx16' style='display:none'>If someone is <em>pilloried</em>, they are publicly criticized or ridiculed by a lot of people, especially in the media.</p>
<p class='rw-defn idx17' style='display:none'>Something that is pristine has not lost any of its original perfection or purity.</p>
<p class='rw-defn idx18' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>
<p class='rw-defn idx19' style='display:none'>People or things that are resplendent are beautiful, bright, and impressive in appearance.</p>
<p class='rw-defn idx20' style='display:none'>If something is reviled, it is intensely hated and criticized.</p>
<p class='rw-defn idx21' style='display:none'>A sacrilegious act is one of deep disrespect that violates something that is sacred or holy.</p>
<p class='rw-defn idx22' style='display:none'>A scintillating conversation, speech, or performance is brilliantly clever, interesting, and lively.</p>
<p class='rw-defn idx23' style='display:none'>An environment or character can be sordid—the former dirty, the latter low or base in an immoral or greedy sort of way.</p>
<p class='rw-defn idx24' style='display:none'>To taint is to give an undesirable quality that damages a person&#8217;s reputation or otherwise spoils something.</p>
<p class='rw-defn idx25' style='display:none'>If you vilify people, you write or say bad things about them in an attempt to make them unpopular.</p>
<p class='rw-defn idx26' style='display:none'>If someone vitiates something, they make it less effective, weaker, or reduced in quality.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>unsullied</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='unsullied#' id='pronounce-sound' path='audio/words/amy-unsullied'></a>
un-SUHL-eed
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='unsullied#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='unsullied#' id='context-sound' path='audio/wordcontexts/brian-unsullied'></a>
The <em>unsullied</em> tablecloth was clean and crisp until I accidentally spilled the red wine.  The waiter moved quickly, grabbed another one from the drawer, and replaced the stained tablecloth with a spotless, <em>unsullied</em> one.  The waiter&#8217;s <em>unsullied</em>, spotless reputation for being quick to make customers happy was once again illustrated to perfection.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is something <em>unsullied</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Quick to react to a difficult situation.
</li>
<li class='choice answer '>
<span class='result'></span>
Without dirt or impurity.
</li>
<li class='choice '>
<span class='result'></span>
Carefully washed each time it gets dirty.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='unsullied#' id='definition-sound' path='audio/wordmeanings/amy-unsullied'></a>
Something that is <em>unsullied</em> is unstained and clean.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>clean</em>
</span>
</span>
</div>
<a class='quick-help' href='unsullied#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='unsullied#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/unsullied/memory_hooks/4487.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Sul</span></span>ly <span class="emp1"><span>Lied</span></span></span></span>  <span class="emp2"><span>Sul</span></span>ly, whom we all thought to be so honest, <span class="emp1"><span>lied</span></span> to us, so her reputation is no longer un<span class="emp2"><span>sul</span></span><span class="emp1"><span>lied</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="unsullied#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/unsullied/memory_hooks">Use other hook</a>
<a href="unsullied#" id="memhook-use-own" url="https://membean.com/mywords/unsullied/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='unsullied#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
In order to hold your faith intact, be sure it's kept <b>unsullied</b> by fact.
<span class='attribution'>&mdash; Donald Westlake, American writer</span>
<img alt="Donald westlake, american writer" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Donald Westlake, American writer.jpg?qdep8" width="80" />
</li>
<li>
By day’s end we were nicely tanned, our catch was cleaned and put into a deep-frozen hereafter and our hands remained as clean and <b>unsullied</b> as when we had started.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Some prefer the components of a meal served separate and <b>unsullied</b>, with nothing touching; others can fully enjoy them only when the flavors mingle in a pot.
<cite class='attribution'>
&mdash;
Discover Magazine
</cite>
</li>
<li>
The glacier is roughly 20 per cent shorter than it used to be, having receded about 3km since the 1800s when German geologist Julius von Haast named it after the Hapsburg royal. "The white <b>unsullied</b> face of the ice was before us, broken up into a thousand turrets, needles and other fantastic forms," he wrote.
<cite class='attribution'>
&mdash;
New Zealand Herald
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/unsullied/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='unsullied#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='un_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='unsullied#'>
<span class='common'></span>
un-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not, opposite of</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='sull_soil' data-tree-url='//cdn1.membean.com/public/data/treexml' href='unsullied#'>
<span class=''></span>
sull
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>soil</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ed_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='unsullied#'>
<span class=''></span>
-ed
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>having a particular state</td>
</tr>
</table>
<p>Anything <em>unsullied</em> is &#8220;not soiled.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='unsullied#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Unsullied" src="https://cdn3.membean.com/public/images/wordimages/cons2/unsullied.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='unsullied#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='4' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>immaculate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>impeccable</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>inestimable</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>pristine</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>resplendent</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>scintillating</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>adulterate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>brackish</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>captious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>cavil</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>debunk</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>defile</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>disparage</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>fallacy</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>malign</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>pillory</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>revile</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>sacrilegious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>sordid</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>taint</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>vilify</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>vitiate</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='unsullied#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
sully
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to soil or dirty</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="unsullied" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>unsullied</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="unsullied#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

