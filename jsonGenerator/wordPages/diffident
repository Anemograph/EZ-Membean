
<!DOCTYPE html>
<html>
<head>
<title>Word: diffident | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you deal with a difficult situation with aplomb, you deal with it in a confident and skillful way.</p>
<p class='rw-defn idx1' style='display:none'>An audacious person acts with great daring and sometimes reckless bravery—despite risks and warnings from other people—in order to achieve something.</p>
<p class='rw-defn idx2' style='display:none'>If you describe a person&#8217;s behavior as brazen, you mean that they are not embarrassed by anything they do—they simply don&#8217;t care what other people think about them.</p>
<p class='rw-defn idx3' style='display:none'>Bumptious people are annoying because they are too proud of their abilities or opinions—they are full of themselves.</p>
<p class='rw-defn idx4' style='display:none'>Someone who is craven is very cowardly.</p>
<p class='rw-defn idx5' style='display:none'>If something daunts you, it makes you worried about dealing with it because you think it will be very difficult or dangerous.</p>
<p class='rw-defn idx6' style='display:none'>If you describe someone, usually a young woman, as demure, you mean that she is quiet, shy, and always behaves modestly.</p>
<p class='rw-defn idx7' style='display:none'>If you think someone is showing hubris, you think that they are demonstrating excessive pride and vanity.</p>
<p class='rw-defn idx8' style='display:none'>Something that inhibits you from doing something restricts or keeps you from doing it.</p>
<p class='rw-defn idx9' style='display:none'>An intrepid person is willing to do dangerous things or go to dangerous places because they are brave.</p>
<p class='rw-defn idx10' style='display:none'>A person&#8217;s morale is their current state of self-confidence, how they feel emotionally, and how motivated they are to complete tasks.</p>
<p class='rw-defn idx11' style='display:none'>When you are presumptuous, you act improperly, rudely, or without respect, especially while attempting to do something that is not socially acceptable or that you are not qualified to do.</p>
<p class='rw-defn idx12' style='display:none'>If you are pretentious, you think you are really great in some way and let everyone know about it, despite the fact that it&#8217;s not the case at all.</p>
<p class='rw-defn idx13' style='display:none'>Skittish persons or animals are made easily nervous or alarmed; they are likely to change behavior quickly and unpredictably.</p>
<p class='rw-defn idx14' style='display:none'>When you strut, you move as though you own the world by walking in a confident and showy fashion.</p>
<p class='rw-defn idx15' style='display:none'>To be tentative is to be hesitant or uncertain about something; an agreement or decision of this kind is likely to have changes before it reaches its final form.</p>
<p class='rw-defn idx16' style='display:none'>To be timorous is to be fearful.</p>
<p class='rw-defn idx17' style='display:none'>Someone is tremulous when they are shaking slightly from nervousness or fear; they may also simply be afraid of something.</p>
<p class='rw-defn idx18' style='display:none'>When you are unflappable, you remain calm, cool, and collected in even the most trying of situations.</p>
<p class='rw-defn idx19' style='display:none'>If something unnerves you, it makes you upset or nervous; it can also make you lose your courage because it frightens you so much.</p>
<p class='rw-defn idx20' style='display:none'>If you are vainglorious, you are very proud of yourself and let other people know about it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>diffident</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='diffident#' id='pronounce-sound' path='audio/words/amy-diffident'></a>
DIF-i-duhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='diffident#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='diffident#' id='context-sound' path='audio/wordcontexts/brian-diffident'></a>
Even though he had won an Academy Award for directing, Jonathan was <em>diffident</em> and shy about mentioning his artistic accomplishments.  At social gatherings, he hovered at the edge of the group and <em>diffidently</em> asked about other people&#8217;s lives, lacking the self-confidence to talk about his own.  His wife Juno proudly pointed out Jonathan&#8217;s award-winning works, but he just humbly grinned, meekly blushed, and <em>diffidently</em> lacked the courage to add to his wife&#8217;s comments.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is something a <em>diffident</em> person would likely avoid?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Impulsively buying something that cost a lot of money.
</li>
<li class='choice '>
<span class='result'></span>
Accepting a job that required them to work overtime.
</li>
<li class='choice answer '>
<span class='result'></span>
Loudly telling jokes to get attention at a party.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='diffident#' id='definition-sound' path='audio/wordmeanings/amy-diffident'></a>
Someone who is <em>diffident</em> is shy, does not want to draw notice to themselves, and is lacking in self-confidence.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>timid</em>
</span>
</span>
</div>
<a class='quick-help' href='diffident#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='diffident#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/diffident/memory_hooks/3779.json'></span>
<p>
<img alt="Diffident" src="https://cdn0.membean.com/public/images/wordimages/hook/diffident.jpg?qdep8" />
<span class="emp0"><span><span class="emp1"><span>Diff</span></span>erent <span class="emp2"><span>Ident</span></span>ity</span></span> The author was so <span class="emp1"><span>diff</span></span><span class="emp2"><span>ident</span></span> about her first novel that she decided to use a <span class="emp1"><span>diff</span></span>erent <span class="emp2"><span>ident</span></span>ity. She changed it from Daphne Dent to <span class="emp1"><span>Diffie</span></span> <span class="emp2"><span>Dent</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="diffident#" id="add-public-hook" style="" url="https://membean.com/mywords/diffident/memory_hooks">Use other public hook</a>
<a href="diffident#" id="memhook-use-own" url="https://membean.com/mywords/diffident/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='diffident#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The <b>diffident</b> youngster of early idealistic years, in course of time, is transformed into an arrogant senior fond of throwing his weight around; he becomes a conceited prig.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Is she up to the task? Party members naturally say yes, but base their optimism on fairly thin evidence. "Even her mother-in-law, Indira Gandhi, was an inexperienced, sheltered, <b>diffident</b> woman when she took the reins of the party in the mid-’60s," says a top Congress leader. "And we all know what a great leader she turned out to be."
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
The Canadian-born Gehry, who lives in L.A., is <b>diffident</b> about the acclaim, recalling that in the initial competition, he wasn’t even considered a serious contender.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/diffident/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='diffident#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='dif_apart' data-tree-url='//cdn1.membean.com/public/data/treexml' href='diffident#'>
<span class=''></span>
dif-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>apart, not, away from</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='fid_trust' data-tree-url='//cdn1.membean.com/public/data/treexml' href='diffident#'>
<span class=''></span>
fid
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>trust, faith</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ent_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='diffident#'>
<span class=''></span>
-ent
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p>If one is <em>diffident</em>, one is &#8220;not trusting&#8221; in one&#8217;s own abilities.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='diffident#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Rocky</strong><span> Adrian is pretty diffident.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/diffident.jpg' video_url='examplevids/diffident' video_width='350'></span>
<div id='wt-container'>
<img alt="Diffident" height="288" src="https://cdn1.membean.com/video/examplevids/diffident.jpg" width="350" />
<div class='center'>
<a href="diffident#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='diffident#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Diffident" src="https://cdn2.membean.com/public/images/wordimages/cons2/diffident.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='diffident#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='4' class = 'rw-wordform notlearned'><span>craven</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>daunt</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>demure</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>inhibit</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>skittish</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>tentative</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>timorous</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>tremulous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>unnerve</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>aplomb</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>audacious</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>brazen</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bumptious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>hubris</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>intrepid</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>morale</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>presumptuous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>pretentious</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>strut</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>unflappable</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>vainglorious</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="diffident" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>diffident</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="diffident#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

