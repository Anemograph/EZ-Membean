
<!DOCTYPE html>
<html>
<head>
<title>Word: propagate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>If someone in authority abolishes a law or practice, they formally put an end to it.</p>
<p class='rw-defn idx2' style='display:none'>To augment something is to increase its value or effectiveness by adding something to it.</p>
<p class='rw-defn idx3' style='display:none'>When you beget something, you cause it to happen or create it.</p>
<p class='rw-defn idx4' style='display:none'>If something burgeons, it grows or develops quickly.</p>
<p class='rw-defn idx5' style='display:none'>A copious amount of something is a very large amount of it.</p>
<p class='rw-defn idx6' style='display:none'>If something, such as a sound, is undergoing a crescendo, it is getting louder and louder or increasing in intensity and is about to reach its peak strength.</p>
<p class='rw-defn idx7' style='display:none'>When there is a dearth of something, there is a scarcity or lack of it.</p>
<p class='rw-defn idx8' style='display:none'>Someone defoliates a tree or plant by removing its leaves, usually by applying a chemical agent.</p>
<p class='rw-defn idx9' style='display:none'>To denude an area is to remove the plants and trees that cover it; it can also mean to make something bare.</p>
<p class='rw-defn idx10' style='display:none'>A derivative is something borrowed from something else, such as an English word that comes from another language.</p>
<p class='rw-defn idx11' style='display:none'>When an area is devoid of life, it is empty or completely lacking in it.</p>
<p class='rw-defn idx12' style='display:none'>When you dispel a thought from your mind, you cause it to go away or disappear; when you do the same to a crowd, you cause it to scatter into different directions.</p>
<p class='rw-defn idx13' style='display:none'>To disseminate something, such as knowledge or information, is to distribute it so that it reaches a lot of people.</p>
<p class='rw-defn idx14' style='display:none'>When something dissipates, it either disappears because it is driven away, or it is completely used up in some way—sometimes wastefully.</p>
<p class='rw-defn idx15' style='display:none'>If you divest someone of power, rights, or authority, you take those things away from them.</p>
<p class='rw-defn idx16' style='display:none'>When something is dormant, it is in a state of sleep or temporarily not active.</p>
<p class='rw-defn idx17' style='display:none'>When an amount of something dwindles, it becomes less and less over time.</p>
<p class='rw-defn idx18' style='display:none'>To efface something is to erase or remove it completely from recognition or memory.</p>
<p class='rw-defn idx19' style='display:none'>When you elicit a response from someone, you draw it forth or cause it to happen by something you say or do.</p>
<p class='rw-defn idx20' style='display:none'>If something engenders a particular feeling or attitude, it causes that condition.</p>
<p class='rw-defn idx21' style='display:none'>When you eradicate something, you tear it up by the roots or remove it completely.</p>
<p class='rw-defn idx22' style='display:none'>When you excise something, you remove it by cutting it out.</p>
<p class='rw-defn idx23' style='display:none'>An exiguous amount of something is meager, small, or limited in nature.</p>
<p class='rw-defn idx24' style='display:none'>If you expunge something, you completely remove it or do away with it.</p>
<p class='rw-defn idx25' style='display:none'>If you extirpate something, you completely destroy it because it is unpleasant or unwanted.</p>
<p class='rw-defn idx26' style='display:none'>If someone, such as a writer, is described as having fecundity, they have the ability to produce many original and different ideas.</p>
<p class='rw-defn idx27' style='display:none'>A fruitless effort at doing something does not bring about a successful result.</p>
<p class='rw-defn idx28' style='display:none'>Gestation is the process by which a new idea,  plan, or piece of work develops in your mind.</p>
<p class='rw-defn idx29' style='display:none'>To induce a state is to bring it about; to induce someone to do something is to persuade them to do it.</p>
<p class='rw-defn idx30' style='display:none'>Something luxuriant, such as plants or hair, is growing well and is very healthy.</p>
<p class='rw-defn idx31' style='display:none'>To nullify something is to cancel or negate it.</p>
<p class='rw-defn idx32' style='display:none'>When you obliterate something, you destroy it to such an extent that there is nothing or very little of it left.</p>
<p class='rw-defn idx33' style='display:none'>A paucity of something is not enough of it.</p>
<p class='rw-defn idx34' style='display:none'>Your posterity are your children; they are also your future generations.</p>
<p class='rw-defn idx35' style='display:none'>A profusion of something is a very large quantity or variety of it.</p>
<p class='rw-defn idx36' style='display:none'>A progenitor is someone&#8217;s ancestor; they can also be the originator of something.</p>
<p class='rw-defn idx37' style='display:none'>Progeny are children or descendants.</p>
<p class='rw-defn idx38' style='display:none'>Something or someone that is prolific is highly fruitful and so produces a lot of something.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>propagate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='propagate#' id='pronounce-sound' path='audio/words/amy-propagate'></a>
PROP-uh-gayt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='propagate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='propagate#' id='context-sound' path='audio/wordcontexts/brian-propagate'></a>
In the film Jurassic Park, dinosaurs were <em>propagated</em> or reproduced from ancient <span class="caps">DNA</span> found in amber.  As the dinosaurs <em>propagated</em> or multiplied, they began to test the island&#8217;s fences that were meant to keep them in.  The dinosaurs <em>propagated</em> or produced so many offspring themselves that soon they were spread about the entire island, which became too small for them.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is happening if something is <em>propagating</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
It is increasing in number.
</li>
<li class='choice '>
<span class='result'></span>
It is hiding itself from the rest of the world.
</li>
<li class='choice '>
<span class='result'></span>
It is losing moisture and drying up.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='propagate#' id='definition-sound' path='audio/wordmeanings/amy-propagate'></a>
When things <em>propagate</em>, such as plants, animals, or ideas, they reproduce or generate greater numbers, causing them to spread.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>reproduce</em>
</span>
</span>
</div>
<a class='quick-help' href='propagate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='propagate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/propagate/memory_hooks/4196.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Pro</span></span>cre<span class="emp2"><span>ate</span></span> <span class="emp3"><span>Pag</span></span>es</span></span> That author <span class="emp2"><span>pro</span></span><span class="emp3"><span>pag</span></span><span class="emp2"><span>ate</span></span>s so many <span class="emp3"><span>pag</span></span>es of writing that he seems to simply <span class="emp2"><span>pro</span></span>cre<span class="emp2"><span>ate</span></span> <span class="emp3"><span>pag</span></span>es!
</p>
</div>

<div id='memhook-button-bar'>
<a href="propagate#" id="add-public-hook" style="" url="https://membean.com/mywords/propagate/memory_hooks">Use other public hook</a>
<a href="propagate#" id="memhook-use-own" url="https://membean.com/mywords/propagate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='propagate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Silence <b>propagates</b> itself, and the longer talk has been suspended, the more difficult it is to find anything to say.
<span class='attribution'>&mdash; Samuel Johnson, eighteenth century English writer</span>
<img alt="Samuel johnson, eighteenth century english writer" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Samuel Johnson, eighteenth century English writer.jpg?qdep8" width="80" />
</li>
<li>
Though “live virus” sounds alarming, the fact is that the viruses are weakened to such an extent that they cannot grow or <b>propagate</b> at normal body temperature.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
At one extreme it was suggested that some of the young cranes be captured, possibly two pairs, and attempts be made to <b>propagate</b> them in captivity.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
For 5,000 years or more, humans have mixed flour and water, waited for the mixture to ferment, and when it was good and sour and full of gas, used it as leavening to make dough rise. They found that they could <b>propagate</b> their leavening by saving a bit of unused dough to sow the seeds of foment in the next batch.
<cite class='attribution'>
&mdash;
Discover Magazine
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/propagate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='propagate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pro_forward' data-tree-url='//cdn1.membean.com/public/data/treexml' href='propagate#'>
<span class='common'></span>
pro-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>forward, forth</td>
</tr>
<tr>
<td class='partform'>pag</td>
<td>
&rarr;
</td>
<td class='meaning'>fix</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ate_quality' data-tree-url='//cdn1.membean.com/public/data/treexml' href='propagate#'>
<span class=''></span>
-ate
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make something have a certain quality</td>
</tr>
</table>
<p>The idea behind <em>propagate</em> is a &#8220;fixing&#8221; of a &#8220;slip&#8221; or part of a mature plant for transplanting &#8220;forth&#8221; in the ground which causes it to &#8220;reproduce;&#8221; &#8220;slips&#8221; are often taken from larger plants to create or <em>propagate</em> more.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='propagate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Propagate" src="https://cdn2.membean.com/public/images/wordimages/cons2/propagate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='propagate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>augment</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>beget</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>burgeon</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>copious</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>crescendo</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>derivative</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>disseminate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>elicit</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>engender</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>fecundity</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>gestation</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>induce</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>luxuriant</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>posterity</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>profusion</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>progenitor</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>progeny</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>prolific</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abolish</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>dearth</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>defoliate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>denude</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>devoid</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>dispel</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>dissipate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>divest</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>dormant</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>dwindle</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>efface</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>eradicate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>excise</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>exiguous</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>expunge</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>extirpate</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>fruitless</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>nullify</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>obliterate</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>paucity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="propagate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>propagate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="propagate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

