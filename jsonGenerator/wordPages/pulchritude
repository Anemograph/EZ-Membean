
<!DOCTYPE html>
<html>
<head>
<title>Word: pulchritude | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Pulchritude-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/pulchritude-large.jpg?qdep8" />
</div>
<a href="pulchritude#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>If you abhor something, you dislike it very much, usually because you think it&#8217;s immoral.</p>
<p class='rw-defn idx1' style='display:none'>To accentuate something is to emphasize it or make it more noticeable.</p>
<p class='rw-defn idx2' style='display:none'>Something austere is simple and plain in its style; an austere person is strict and severe with themselves.</p>
<p class='rw-defn idx3' style='display:none'>Someone who has a bilious personality is highly irritable and bad-tempered.</p>
<p class='rw-defn idx4' style='display:none'>When you are captivated by someone or something, you are enchanted, fascinated, or delighted by them or it.</p>
<p class='rw-defn idx5' style='display:none'>A person who is comely is attractive; this adjective is usually used with females, but not always.</p>
<p class='rw-defn idx6' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx7' style='display:none'>If you say a person&#8217;s actions are despicable, you think they are extremely unpleasant or nasty.</p>
<p class='rw-defn idx8' style='display:none'>If something enthralls you, it makes you so interested and excited that you give it all your attention.</p>
<p class='rw-defn idx9' style='display:none'>Something grotesque is so distorted or misshapen that it is disturbing, bizarre, gross, or very ugly.</p>
<p class='rw-defn idx10' style='display:none'>If you describe something as heinous, you mean that is extremely evil, shocking, and bad.</p>
<p class='rw-defn idx11' style='display:none'>Something loathsome is offensive, disgusting, and brings about intense dislike.</p>
<p class='rw-defn idx12' style='display:none'>When something is luminous, it is bright and glowing.</p>
<p class='rw-defn idx13' style='display:none'>Something macabre is so gruesome or frightening that it causes great horror in those who see it.</p>
<p class='rw-defn idx14' style='display:none'>If something mesmerizes you, it attracts or holds your interest so much that you do not pay attention to anything else.</p>
<p class='rw-defn idx15' style='display:none'>Animals preen when they smooth out their fur or their feathers; humans preen by making themselves beautiful in front of a mirror.</p>
<p class='rw-defn idx16' style='display:none'>Repugnance is a strong feeling of dislike for something or someone you find horrible and offensive.</p>
<p class='rw-defn idx17' style='display:none'>Something that is repulsive is offensive, highly unpleasant, or just plain disgusting.</p>
<p class='rw-defn idx18' style='display:none'>People or things that are resplendent are beautiful, bright, and impressive in appearance.</p>
<p class='rw-defn idx19' style='display:none'>When you experience revulsion, you feel a great deal of disgust or extreme dislike for something.</p>
<p class='rw-defn idx20' style='display:none'>A scintillating conversation, speech, or performance is brilliantly clever, interesting, and lively.</p>
<p class='rw-defn idx21' style='display:none'>Something that is tantalizing is desirable and exciting; nevertheless, it may be out of reach, perhaps due to being too expensive.</p>
<p class='rw-defn idx22' style='display:none'>Something is toothsome when it is tasty, attractive, or pleasing in some way; this adjective can apply to food, people, or objects.</p>
<p class='rw-defn idx23' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx24' style='display:none'>An unsightly person presents an ugly, unattractive, or disagreeable face to the world.</p>
<p class='rw-defn idx25' style='display:none'>Something vile is evil, very unpleasant, horrible, or extremely bad.</p>
<p class='rw-defn idx26' style='display:none'>Someone who is winsome is attractive and charming.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>pulchritude</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='pulchritude#' id='pronounce-sound' path='audio/words/amy-pulchritude'></a>
PUHL-kri-tood
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='pulchritude#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='pulchritude#' id='context-sound' path='audio/wordcontexts/brian-pulchritude'></a>
The <em>pulchritude</em> of the flowers was amazing: never had I seen such beautiful blossoms.  The fields were full of pretty and colorful blooms, all of which were at the height of their <em>pulchritude</em>.  There were no blemishes or any sign of decay whatsoever.  All looked fresh and ripe in their <em>pulchritude</em>, a true testament to the stunning beauty of nature.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is <em>pulchritude</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Physical attractiveness.
</li>
<li class='choice '>
<span class='result'></span>
An amazing quality of something.
</li>
<li class='choice '>
<span class='result'></span>
When something is just beginning to blossom.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='pulchritude#' id='definition-sound' path='audio/wordmeanings/amy-pulchritude'></a>
When you possess <em>pulchritude</em>, you are beautiful.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>beauty</em>
</span>
</span>
</div>
<a class='quick-help' href='pulchritude#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='pulchritude#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/pulchritude/memory_hooks/4258.json'></span>
<p>
<span class="emp0"><span>M<span class="emp2"><span>ulch</span></span> D<span class="emp3"><span>ude</span></span></span></span> The garden M<span class="emp2"><span>ulch</span></span> D<span class="emp3"><span>ude</span></span> practically gets covered in m<span class="emp2"><span>ulch</span></span> since he shovels it all day, so I wouldn't exactly say that the dirty M<span class="emp2"><span>ulch</span></span> D<span class="emp3"><span>ude</span></span> has much p<span class="emp2"><span>ulch</span></span>rit<span class="emp3"><span>ude</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="pulchritude#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/pulchritude/memory_hooks">Use other hook</a>
<a href="pulchritude#" id="memhook-use-own" url="https://membean.com/mywords/pulchritude/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='pulchritude#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The practiced reader of Powell will wince in particular at the unkind emphasis of "lovely blue eyes"; she is a writer, like George Eliot, in whose novels feminine <b>pulchritude</b> gets what it deserves.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
Paying for <b>pulchritude</b> isn't limited to Wall Street. Research shows that attractive people are widely perceived to be more competent leaders, harder negotiators, and smarter workers.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/pulchritude/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='pulchritude#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>pulchr</td>
<td>
&rarr;
</td>
<td class='meaning'>beautiful</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='itude_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='pulchritude#'>
<span class=''></span>
-itude
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state or quality of</td>
</tr>
</table>
<p><em>Pulchritude</em> is the &#8220;state or quality of being beautiful.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='pulchritude#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Pulchritude" src="https://cdn3.membean.com/public/images/wordimages/cons2/pulchritude.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='pulchritude#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>accentuate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>captivate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>comely</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>enthrall</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>luminous</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>mesmerize</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>preen</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>resplendent</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>scintillating</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>tantalize</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>toothsome</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>winsome</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abhor</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>austere</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bilious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>despicable</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>grotesque</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>heinous</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>loathsome</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>macabre</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>repugnance</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>repulsive</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>revulsion</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>unsightly</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>vile</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="pulchritude" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>pulchritude</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="pulchritude#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

