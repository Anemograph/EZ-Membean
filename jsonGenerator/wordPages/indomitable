
<!DOCTYPE html>
<html>
<head>
<title>Word: indomitable | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Indomitable-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/indomitable-large.jpg?qdep8" />
</div>
<a href="indomitable#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>An assiduous person works hard to ensure that something is done properly and completely.</p>
<p class='rw-defn idx1' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx2' style='display:none'>A bastion is considered an important and effective defense of a way of life or principle; a bastion can also refer to a heavily fortified place, such as a castle.</p>
<p class='rw-defn idx3' style='display:none'>When you buttress an argument, idea, or even a building, you make it stronger by providing support.</p>
<p class='rw-defn idx4' style='display:none'>If something daunts you, it makes you worried about dealing with it because you think it will be very difficult or dangerous.</p>
<p class='rw-defn idx5' style='display:none'>Debility is a state of being physically or mentally weak, usually due to illness.</p>
<p class='rw-defn idx6' style='display:none'>A decadent person has low moral standards and is more interested in pleasure than serious matters.</p>
<p class='rw-defn idx7' style='display:none'>Decrepitude is the state of being very old, worn out, or very ill; therefore, something or someone is no longer in good physical condition or good health.</p>
<p class='rw-defn idx8' style='display:none'>Someone who is diffident is shy, does not want to draw notice to themselves, and is lacking in self-confidence.</p>
<p class='rw-defn idx9' style='display:none'>A dilapidated building, vehicle, etc. is old, broken-down, and in very bad condition.</p>
<p class='rw-defn idx10' style='display:none'>When one person eclipses another&#8217;s achievements, they surpass or outshine that person in that endeavor.</p>
<p class='rw-defn idx11' style='display:none'>If you describe a person, group, or civilization as effete, you mean it is weak, exhausted,  powerless, unproductive, and/or corrupt.</p>
<p class='rw-defn idx12' style='display:none'>Efficacy is the ability or power to produce an expected effect or result.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx14' style='display:none'>If something is formidable, it is impressive in size, power, or skill; therefore, it can make you feel frightened, alarmed, or in awe because it is so powerful or difficult to deal with.</p>
<p class='rw-defn idx15' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx16' style='display:none'>An impregnable fortress or castle is very difficult to defeat or overcome; an opinion or argument of that same quality is almost impossible to successfully change or challenge.</p>
<p class='rw-defn idx17' style='display:none'>Someone, such as a performer or athlete, is inimitable when they are so good or unique in their talent that it is unlikely anyone else can be their equal.</p>
<p class='rw-defn idx18' style='display:none'>Someone or something that is invulnerable cannot be harmed in any way; hence, they or it is completely safe or secure.</p>
<p class='rw-defn idx19' style='display:none'>When you are in jeopardy, you are in danger or trouble of some kind.</p>
<p class='rw-defn idx20' style='display:none'>A juggernaut is a very powerful force, organization, or group whose influence cannot be stopped; its strength and power can overwhelm or crush any competition or anything else that stands in its way.</p>
<p class='rw-defn idx21' style='display:none'>A leviathan is something that is very large, powerful, difficult to control, and rather frightening.</p>
<p class='rw-defn idx22' style='display:none'>If you have a nemesis, it is an opponent or rival whom you cannot beat or is a source of ruin that causes your downfall.</p>
<p class='rw-defn idx23' style='display:none'>Someone who is obdurate is stubborn, unreasonable, or uncontrollable; hence, they simply refuse to change their behavior, actions, or beliefs to fit any situation whatsoever.</p>
<p class='rw-defn idx24' style='display:none'>An omnipotent being or entity is unlimited in its power.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is pertinacious is determined to continue doing something rather than giving up—even when it gets very difficult.</p>
<p class='rw-defn idx26' style='display:none'>If something plummets, it falls down from a high position very quickly; for example, a piano can plummet from a window and a stock value can plummet during a market crash.</p>
<p class='rw-defn idx27' style='display:none'>Something&#8217;s potency is how powerful or effective it is.</p>
<p class='rw-defn idx28' style='display:none'>A precarious situation or state can very quickly become dangerous without warning.</p>
<p class='rw-defn idx29' style='display:none'>A recalcitrant animal or person is difficult to control and refuses to obey orders—even after stiff punishment.</p>
<p class='rw-defn idx30' style='display:none'>If you describe someone as redoubtable, you have great respect for their power and strength; you may be afraid of them as well.</p>
<p class='rw-defn idx31' style='display:none'>If you are sanguine about a situation, especially a difficult one, you are confident and cheerful that everything will work out the way you want it to.</p>
<p class='rw-defn idx32' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx33' style='display:none'>When you subdue something, such as an enemy or emotions, you defeat or bring them under control.</p>
<p class='rw-defn idx34' style='display:none'>If you surmount a problem or difficulty, you get the better of it by conquering or overcoming it.</p>
<p class='rw-defn idx35' style='display:none'>To be tentative is to be hesitant or uncertain about something; an agreement or decision of this kind is likely to have changes before it reaches its final form.</p>
<p class='rw-defn idx36' style='display:none'>If you are unsurpassed in what you do, you are the best—period.</p>
<p class='rw-defn idx37' style='display:none'>The zenith of something is its highest, most powerful, or most successful point.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>indomitable</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='indomitable#' id='pronounce-sound' path='audio/words/amy-indomitable'></a>
in-DOM-i-tuh-buhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='indomitable#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='indomitable#' id='context-sound' path='audio/wordcontexts/brian-indomitable'></a>
Caesar&#8217;s legions were <em>indomitable</em> or unbeatable.  Led by Julius Caesar, who himself was <em>indomitable</em> in his unyielding drive to be the absolute best, the legions had little trouble knocking off their enemies one after the other.  It didn&#8217;t hurt that Caesar had an unbelievable ability to determine how to attack and take any fortress, which turned previously <em>indomitable</em> or invincible enemies over to the might of Rome.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of an <em>indomitable</em> contestant?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A person who auditions for a singing competition even though they don&#8217;t sing very well.
</li>
<li class='choice answer '>
<span class='result'></span>
A person who goes on a television game show and beats everyone they compete against.
</li>
<li class='choice '>
<span class='result'></span>
A person who enters a baking contest with a pie that they didn&#8217;t actually bake.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='indomitable#' id='definition-sound' path='audio/wordmeanings/amy-indomitable'></a>
An <em>indomitable</em> foe cannot be beaten.
</li>
<li class='def-text'>
Someone who has an <em>indomitable</em> drive refuses to give up under any circumstances.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>unconquerable</em>
</span>
</span>
</div>
<a class='quick-help' href='indomitable#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='indomitable#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/indomitable/memory_hooks/5583.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>N</span></span>ot <span class="emp2"><span>Able</span></span> to Be <span class="emp3"><span>Domi</span></span>na<span class="emp3"><span>t</span></span>ed</span></span> An i<span class="emp1"><span>n</span></span><span class="emp3"><span>domit</span></span><span class="emp2"><span>able</span></span> person or castle is not able to be <span class="emp3"><span>domi</span></span>na<span class="emp3"><span>t</span></span>ed.
</p>
</div>

<div id='memhook-button-bar'>
<a href="indomitable#" id="add-public-hook" style="" url="https://membean.com/mywords/indomitable/memory_hooks">Use other public hook</a>
<a href="indomitable#" id="memhook-use-own" url="https://membean.com/mywords/indomitable/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='indomitable#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Harley-Davidson used to be one of those brands, signifying a sort of <b>indomitable</b> endurance that, you'd hope, would apply to its motorcycles as well as their riders.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
Mr. Howard, who is clever, street-wise and a ruthless debater, now frequently draws blood when he squares up to the previously <b>indomitable</b> Gordon Brown.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/indomitable/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='indomitable#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='in_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='indomitable#'>
<span class='common'></span>
in-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not</td>
</tr>
<tr>
<td class='partform'>domit</td>
<td>
&rarr;
</td>
<td class='meaning'>tame</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='able_handy' data-tree-url='//cdn1.membean.com/public/data/treexml' href='indomitable#'>
<span class=''></span>
-able
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>handy, capable of</td>
</tr>
</table>
<p>An <em>indomitable</em> person is &#8220;not capable of being tamed.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='indomitable#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Tennis Stuff: Unstoppable: Venus Williams vs. Belinda Becic US Open 2015</strong><span> The indomitable Venus Williams.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/indomitable.jpg' video_url='examplevids/indomitable' video_width='350'></span>
<div id='wt-container'>
<img alt="Indomitable" height="288" src="https://cdn1.membean.com/video/examplevids/indomitable.jpg" width="350" />
<div class='center'>
<a href="indomitable#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='indomitable#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Indomitable" src="https://cdn2.membean.com/public/images/wordimages/cons2/indomitable.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='indomitable#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>assiduous</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bastion</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>buttress</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>daunt</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>eclipse</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>efficacy</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>formidable</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>impregnable</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>inimitable</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>invulnerable</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>juggernaut</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>leviathan</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>nemesis</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>obdurate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>omnipotent</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>pertinacious</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>potency</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>recalcitrant</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>redoubtable</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>sanguine</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>surmount</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>unsurpassed</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>zenith</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>debility</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>decadent</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>decrepitude</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>diffident</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>dilapidated</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>effete</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>jeopardy</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>plummet</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>precarious</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>subdue</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>tentative</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="indomitable" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>indomitable</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="indomitable#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

