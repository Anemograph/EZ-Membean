
<!DOCTYPE html>
<html>
<head>
<title>Word: remnant | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Something that happens unexpectedly and by chance that is not inherent or natural to a given item or situation is adventitious, such as a root appearing in an unusual place on a plant.</p>
<p class='rw-defn idx1' style='display:none'>Something that is apposite is relevant or suitable to what is happening or being discussed.</p>
<p class='rw-defn idx2' style='display:none'>A bagatelle is something that is not important or of very little value or significance.</p>
<p class='rw-defn idx3' style='display:none'>A cardinal rule or quality is considered to be the most important or basic in a set of rules or qualities.</p>
<p class='rw-defn idx4' style='display:none'>Chaff is the husks of grain separated from the edible plant; it can also be useless matter in general that is cast aside.</p>
<p class='rw-defn idx5' style='display:none'>The crux of a problem or argument is the most important or difficult part of it that affects everything else.</p>
<p class='rw-defn idx6' style='display:none'>A cynosure is an object that serves as the center of attention.</p>
<p class='rw-defn idx7' style='display:none'>Detritus is the small pieces of waste that remain after something has been destroyed or used.</p>
<p class='rw-defn idx8' style='display:none'>Something described as dross is of very poor quality and has little or no value.</p>
<p class='rw-defn idx9' style='display:none'>Something that is epochal is highly significant or important; this adjective often refers to the bringing about or marking of the beginning of a new era.</p>
<p class='rw-defn idx10' style='display:none'>If you think something, such as a statement or idea, is fatuous, you consider it stupid or extremely silly; a fatuous hope is unrealistic.</p>
<p class='rw-defn idx11' style='display:none'>If you describe someone&#8217;s behavior as inane, you think it is completely stupid or without much meaning.</p>
<p class='rw-defn idx12' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx13' style='display:none'>Something that is pivotal is more important than anything else in a situation or a system; consequently, it is the key to its success.</p>
<p class='rw-defn idx14' style='display:none'>Something that is residual is the part that still stays or remains after the main part is taken away.</p>
<p class='rw-defn idx15' style='display:none'>Rubble is the debris, remains, or wreckage of something that has been ruined, such as a building or other structure.</p>
<p class='rw-defn idx16' style='display:none'>A rubric is a set of instructions at the beginning of a document, such as an examination or term paper, that is usually printed in a different style so as to highlight its importance.</p>
<p class='rw-defn idx17' style='display:none'>Shards are sharp pieces of broken glass, pottery, metal, or other hard substances.</p>
<p class='rw-defn idx18' style='display:none'>Something that is superfluous is unnecessary; it is more than what is wanted or needed at the current time.</p>
<p class='rw-defn idx19' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx20' style='display:none'>If you refer to something as a trifle, you mean that it is of little or no importance or value.</p>
<p class='rw-defn idx21' style='display:none'>A trite remark or idea is so overused that it is no longer interesting or novel.</p>
<p class='rw-defn idx22' style='display:none'>A vestige of something is a very small part that remains from a once larger whole.</p>
<p class='rw-defn idx23' style='display:none'>If you winnow a list or group, you reduce its size by getting rid of the things that are no longer useful or by keeping only the desirable things.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>remnant</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='remnant#' id='pronounce-sound' path='audio/words/amy-remnant'></a>
REM-nuhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='remnant#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='remnant#' id='context-sound' path='audio/wordcontexts/brian-remnant'></a>
The crumbs, candle stubs, leftover icing, and other <em>remnants</em> of the birthday cake were scattered all over the tray.  There was barely anything left after the kids had eaten, but Sheila nevertheless gathered the sweet <em>remnants</em> or traces of cake that remained and enjoyed them herself.  The <em>remnants</em> or remains of the energetic party for her young son would take much longer to clean up!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>remnant</em> of something?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A small piece of paper from a destroyed book.
</li>
<li class='choice '>
<span class='result'></span>
An older and out-of-date version of a book.
</li>
<li class='choice '>
<span class='result'></span>
A writing style used in all an author&#8217;s books.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='remnant#' id='definition-sound' path='audio/wordmeanings/amy-remnant'></a>
The <em>remnants</em> of something are the small parts of it that are left after the main part has been used or destroyed.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>parts remaining</em>
</span>
</span>
</div>
<a class='quick-help' href='remnant#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='remnant#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/remnant/memory_hooks/5958.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Rema</span></span>i<span class="emp3"><span>n</span></span>s</span></span> A <span class="emp3"><span>remna</span></span>nt is that small part which <span class="emp3"><span>rema</span></span>i<span class="emp3"><span>n</span></span>s after the rest is gone.
</p>
</div>

<div id='memhook-button-bar'>
<a href="remnant#" id="add-public-hook" style="" url="https://membean.com/mywords/remnant/memory_hooks">Use other public hook</a>
<a href="remnant#" id="memhook-use-own" url="https://membean.com/mywords/remnant/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='remnant#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Astronomers believe the <b>remnant</b> was produced by the explosion of a massive star weighing 10 to 30 times the mass of our sun.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
Each of its eight artists has taken an existing object—in some cases a work of art—or elements from it to produce quite another work that at the same time is a <b>remnant</b> of things past.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
The school's method is based on the belief that the totally deaf person is almost nonexistent; even those who seem totally deaf to others usually have some slight <b>remnant</b> of hearing.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/remnant/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='remnant#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='man_stay' data-tree-url='//cdn1.membean.com/public/data/treexml' href='remnant#'>
<span class=''></span>
man
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>stay, remain</td>
</tr>
</table>
<p>The <em>remnant</em> of something is the part that &#8220;stays or remains&#8221; after the rest has been used.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='remnant#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Alf</strong><span> Remnants of a vanished civilization.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/remnant.jpg' video_url='examplevids/remnant' video_width='350'></span>
<div id='wt-container'>
<img alt="Remnant" height="288" src="https://cdn1.membean.com/video/examplevids/remnant.jpg" width="350" />
<div class='center'>
<a href="remnant#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='remnant#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Remnant" src="https://cdn1.membean.com/public/images/wordimages/cons2/remnant.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='remnant#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adventitious</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bagatelle</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>chaff</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>detritus</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>dross</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>fatuous</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>inane</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>residual</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>rubble</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>shard</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>superfluous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>trifle</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>trite</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>vestige</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>winnow</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>apposite</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>cardinal</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>crux</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>cynosure</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>epochal</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>pivotal</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>rubric</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="remnant" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>remnant</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="remnant#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

