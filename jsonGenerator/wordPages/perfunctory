
<!DOCTYPE html>
<html>
<head>
<title>Word: perfunctory | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Acumen is the ability to make quick decisions and keen, precise judgments.</p>
<p class='rw-defn idx1' style='display:none'>Apathy is a lack of interest or unenthusiastic involvement in an activity; there is no effort to change or improve it at all.</p>
<p class='rw-defn idx2' style='display:none'>When you have ardor for something, you have an intense feeling of love, excitement, and admiration for it.</p>
<p class='rw-defn idx3' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx4' style='display:none'>A cursory examination or reading of something is very quick, incomplete, and does not pay close attention to detail.</p>
<p class='rw-defn idx5' style='display:none'>When you discern something, you notice, detect, or understand it, often after thinking about it carefully or studying it for some time.  </p>
<p class='rw-defn idx6' style='display:none'>Someone does something in a disinterested way when they have no personal involvement or attachment to the action.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx8' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx9' style='display:none'>Ennui is the feeling of being bored, tired, and dissatisfied with life.</p>
<p class='rw-defn idx10' style='display:none'>Someone who is erudite is steeped in knowledge because they have read and studied extensively.</p>
<p class='rw-defn idx11' style='display:none'>An injury that is excruciating is extremely painful.</p>
<p class='rw-defn idx12' style='display:none'>When a person is being flippant, they are not taking something as seriously as they should be; therefore, they tend to dismiss things they should respectfully pay attention to.</p>
<p class='rw-defn idx13' style='display:none'>To handle an object gingerly is to be careful and cautious with it.</p>
<p class='rw-defn idx14' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx15' style='display:none'>If an idea or thought is incisive, it is expressed in a penetrating and knowledgeable manner that is clear and brief; additionally, it can demonstrate impressive understanding of related ideas or thoughts.</p>
<p class='rw-defn idx16' style='display:none'>If you are indifferent about something, you are uninterested or neutral about it, not caring either in a positive or negative way.</p>
<p class='rw-defn idx17' style='display:none'>Insouciance is a lack of concern or worry for something that should be shown more careful attention or consideration.</p>
<p class='rw-defn idx18' style='display:none'>If you do something in a lackadaisical way, you do it carelessly and without putting much effort into it—thereby showing that you are not really interested in what you&#8217;re doing.</p>
<p class='rw-defn idx19' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx20' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx21' style='display:none'>If you peruse some written text, you read it over carefully.</p>
<p class='rw-defn idx22' style='display:none'>When someone exhibits profundity, they display great intellectual depth and understanding; profundity can also be the depth or complexity of something.</p>
<p class='rw-defn idx23' style='display:none'>A sagacious person is wise, intelligent, and has the ability to make good practical decisions.</p>
<p class='rw-defn idx24' style='display:none'>A slovenly person is untidy or messy.</p>
<p class='rw-defn idx25' style='display:none'>If you are suffering from tedium, you are bored.</p>
<p class='rw-defn idx26' style='display:none'>A tepid reaction to something is mild, unemotional, and lacking in enthusiasm; tepid water is likewise lukewarm—not too hot and not too cold.</p>
<p class='rw-defn idx27' style='display:none'>Someone who is zealous spends a lot of time, energy, and effort to support something—  notably that of a political or religious nature—because they believe in it very strongly.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>perfunctory</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='perfunctory#' id='pronounce-sound' path='audio/words/amy-perfunctory'></a>
per-FUHNGK-tuh-ree
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='perfunctory#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='perfunctory#' id='context-sound' path='audio/wordcontexts/brian-perfunctory'></a>
The <em>perfunctory</em> performance of the band members left us feeling less than enthusiastic as they seemed to be uninterested in what they were doing.  The lead singer didn&#8217;t even look out into the crowd as he <em><em>perfunctorily</em></em> went through what appeared to be a tired old routine.  The band went through the motions in a <em>perfunctory</em>, unthinking manner, like they were unconcerned about both their music and the audience.  The band broke up after this <em>perfunctory</em> and wooden performance, citing boredom as the main cause.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>perfunctory</em> act?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
One which is performed with funk music.
</li>
<li class='choice '>
<span class='result'></span>
One which is performed on stage in front of many people.
</li>
<li class='choice answer '>
<span class='result'></span>
One which lacks spirit and is done carelessly.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='perfunctory#' id='definition-sound' path='audio/wordmeanings/amy-perfunctory'></a>
A <em>perfunctory</em> action is done with little care or interest; consequently, it is only completed because it is expected of someone to do so.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>careless</em>
</span>
</span>
</div>
<a class='quick-help' href='perfunctory#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='perfunctory#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/perfunctory/memory_hooks/5637.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Per</span></span>cy's <span class="emp2"><span>Fun</span></span> Fa<span class="emp3"><span>ctory</span></span></span></span> <span class="emp1"><span>Per</span></span>cy's <span class="emp2"><span>fun</span></span> fa<span class="emp3"><span>ctory</span></span> used to be a lot of <span class="emp2"><span>fun</span></span>, but now that we've gone through <span class="emp1"><span>Per</span></span>cy's <span class="emp2"><span>fun</span></span> fa<span class="emp3"><span>ctory</span></span> 500 times, our enjoyment is nothing but <span class="emp1"><span>per</span></span><span class="emp2"><span>fun</span></span><span class="emp3"><span>ctory</span></span>, despite the fact that <span class="emp1"><span>Per</span></span>cy is our father and he gives us extra cotton candy each time we go through.
</p>
</div>

<div id='memhook-button-bar'>
<a href="perfunctory#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/perfunctory/memory_hooks">Use other hook</a>
<a href="perfunctory#" id="memhook-use-own" url="https://membean.com/mywords/perfunctory/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='perfunctory#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
At one end of the [fantasy cinema] spectrum are sword-and-sorcery sagas set in exotic places. At the other end are science-fiction stories, which are more grounded in reality, but use their futuristic technologies as <b>perfunctory</b> backdrops for magical adventures.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
Every four years, the House and Senate come together to formally tabulate the electoral votes and raise any final concerns about the results. Normally, it is a <b>perfunctory</b> confirmation of the Electoral College vote.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/perfunctory/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='perfunctory#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='per_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='perfunctory#'>
<span class='common'></span>
per-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='funct_perform' data-tree-url='//cdn1.membean.com/public/data/treexml' href='perfunctory#'>
<span class=''></span>
funct
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>perform, complete</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ory_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='perfunctory#'>
<span class=''></span>
-ory
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>To perform a task in a <em>perfunctory</em> fashion is to &#8220;thoroughly perform or complete&#8221; it in the sense of getting it over with as quickly as possible.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='perfunctory#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Perfunctory" src="https://cdn2.membean.com/public/images/wordimages/cons2/perfunctory.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='perfunctory#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>apathy</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>cursory</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>disinterested</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>ennui</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>flippant</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>indifferent</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>insouciance</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>lackadaisical</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>slovenly</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>tedium</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>tepid</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>acumen</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>ardor</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>discern</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>erudite</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>excruciating</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>gingerly</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>incisive</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>peruse</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>profundity</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>sagacious</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>zealous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="perfunctory" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>perfunctory</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="perfunctory#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

