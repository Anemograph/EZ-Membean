
<!DOCTYPE html>
<html>
<head>
<title>Word: espouse | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you abet someone, you help them in a criminal or illegal act.</p>
<p class='rw-defn idx1' style='display:none'>If you abjure a belief or a way of behaving, you state publicly that you will give it up or reject it.</p>
<p class='rw-defn idx2' style='display:none'>When you accede to a demand or proposal, you agree to it, especially after first disagreeing with it.</p>
<p class='rw-defn idx3' style='display:none'>When you advocate a plan or action, you publicly push for implementing it.</p>
<p class='rw-defn idx4' style='display:none'>If you have an affiliation with a group or another person, you are officially involved or connected with them.</p>
<p class='rw-defn idx5' style='display:none'>Apathy is a lack of interest or unenthusiastic involvement in an activity; there is no effort to change or improve it at all.</p>
<p class='rw-defn idx6' style='display:none'>When you assimilate an idea, you completely understand it; likewise, when someone is assimilated into a new place, they become part of it by understanding what it is all about and by being accepted by others who live there.</p>
<p class='rw-defn idx7' style='display:none'>When you bolster something or someone, you offer support in a moment of need.</p>
<p class='rw-defn idx8' style='display:none'>When you buttress an argument, idea, or even a building, you make it stronger by providing support.</p>
<p class='rw-defn idx9' style='display:none'>A coalition is a temporary union of different political or social groups that agrees to work together to achieve a shared aim.</p>
<p class='rw-defn idx10' style='display:none'>A confluence is a situation where two or more things meet or flow together at a single point or area; a confluence usually refers to two streams joining together.</p>
<p class='rw-defn idx11' style='display:none'>The adjective conjugal refers to marriage or the relationship between two married people.</p>
<p class='rw-defn idx12' style='display:none'>To contravene a law, rule, or agreement is to do something that is not allowed or is forbidden by that law, rule, or agreement.</p>
<p class='rw-defn idx13' style='display:none'>To decry something is to speak against it and find fault with it.</p>
<p class='rw-defn idx14' style='display:none'>To defile something holy or important is to do something to it or say something about it that is highly offensive to others.</p>
<p class='rw-defn idx15' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx16' style='display:none'>If you deprecate something, you disapprove of it strongly.</p>
<p class='rw-defn idx17' style='display:none'>If you treat someone with derision, you mock or make fun of them because you think they are stupid, unimportant, or useless.</p>
<p class='rw-defn idx18' style='display:none'>If you disparage someone or something, you say unpleasant words that show you have no respect for that person or thing.</p>
<p class='rw-defn idx19' style='display:none'>Dissolution is the breaking up or official end of a group, such as a couple or institution; it can also be the act of separating something into smaller components.</p>
<p class='rw-defn idx20' style='display:none'>An emissary is someone who acts as a representative from one government or leader to another.</p>
<p class='rw-defn idx21' style='display:none'>If you exhort someone to do something, you try very hard to persuade them to do it.</p>
<p class='rw-defn idx22' style='display:none'>An exponent of a particular idea or cause is someone who supports or defends it.</p>
<p class='rw-defn idx23' style='display:none'>If you gainsay something, you say that it is not true and therefore reject it.</p>
<p class='rw-defn idx24' style='display:none'>If you impugn someone&#8217;s motives or integrity, you say that they do not deserve to be trusted.</p>
<p class='rw-defn idx25' style='display:none'>If someone is <em>pilloried</em>, they are publicly criticized or ridiculed by a lot of people, especially in the media.</p>
<p class='rw-defn idx26' style='display:none'>A proponent is a supporter or backer of something, such as a cause or other endeavor.</p>
<p class='rw-defn idx27' style='display:none'>To remonstrate with someone is to tell that person that you strongly disapprove of something they have said or done.</p>
<p class='rw-defn idx28' style='display:none'>To renounce something, such as a position or practice, is to let go of it or reject it.</p>
<p class='rw-defn idx29' style='display:none'>If you repudiate something, you state that you do not accept or agree with it and no longer want to be connected with it in any way.</p>
<p class='rw-defn idx30' style='display:none'>To undermine a plan or endeavor is to weaken it or make it unstable in a gradual but purposeful fashion.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>espouse</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='espouse#' id='pronounce-sound' path='audio/words/amy-espouse'></a>
i-SPOUZ
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='espouse#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='espouse#' id='context-sound' path='audio/wordcontexts/brian-espouse'></a>
Nadia <em>espoused</em> or strongly believed in the idea of equal rights for all people.  As a human rights lawyer she believed that no government had the right to detain its citizens without cause, and she fiercely fought for and <em>espoused</em> policies that supported individual freedom.  Nadia embraced and <em>espoused</em> the concept of citizens&#8217; rights so much that she organized and led a protest on the steps of the capital against a bill that could limit those rights.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How would you <em>espouse</em> certain political beliefs?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
You would embrace and defend them.
</li>
<li class='choice '>
<span class='result'></span>
You would question if they are practical.
</li>
<li class='choice '>
<span class='result'></span>
You would urge others to reject them. 
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='espouse#' id='definition-sound' path='audio/wordmeanings/amy-espouse'></a>
If you <em>espouse</em> an idea, principle, or belief, you support it wholeheartedly.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>stand up for</em>
</span>
</span>
</div>
<a class='quick-help' href='espouse#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='espouse#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/espouse/memory_hooks/3951.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Spouse</span></span> E<span class="emp3"><span>spous</span></span>al</span></span> My <span class="emp3"><span>spouse</span></span> has decided to attend graduate school, and I completely e<span class="emp3"><span>spouse</span></span> her decision to do so--I try to stand up for my <span class="emp3"><span>spouse</span></span> on general principle.
</p>
</div>

<div id='memhook-button-bar'>
<a href="espouse#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/espouse/memory_hooks">Use other hook</a>
<a href="espouse#" id="memhook-use-own" url="https://membean.com/mywords/espouse/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='espouse#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
You can disagree with a certain policy without demonizing the person who <b>espouses</b> it. You can question somebody’s views and their judgment without questioning their motives or their patriotism.
<cite class='attribution'>
&mdash;
Barack Obama, Forty-fourth president of the United States
</cite>
</li>
<li>
[Lupita Nyong'o shared] the story of her grandfather in Kenya who believed girls and boys deserved an equal education and raised his children to believe the same. "I'm here because my grandpa and my grandma raised my father to <b>espouse</b> the belief in the value of women," she said through tears.
<cite class='attribution'>
&mdash;
Chicago Tribune
</cite>
</li>
<li>
For a long time, Whitfield said, “corporate entities and clubs” have sought to use “their messaging to pacify player voices.” There is a sense that has changed now, not only because clubs are prepared to “back up what they <b>espouse”</b> through action, but also because there are those, like Sanneh and Steffen and others, who have sufficient clout to “put their thumb on the scale.”
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Joey Cheek was shocked yet no longer surprised that a number of countries have tried to stifle what their athletes at this summer’s Beijing Olympics say about China. Cheek has learned quickly that it is one thing for Olympic officials to <b>espouse</b> the humanitarian ideals expressed in the Olympic Charter and another to insist those officials stand behind the ideals to help alleviate a humanitarian crisis.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/espouse/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='espouse#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='espouse#'>
<span class=''></span>
e-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='spous_pledged' data-tree-url='//cdn1.membean.com/public/data/treexml' href='espouse#'>
<span class=''></span>
spous
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>pledged, promised</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='espouse#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>To <em>espouse</em> an idea is to &#8220;thoroughly pledge&#8221; oneself to it, or &#8220;thoroughly promise&#8221; to follow it.  In a similar way, one&#8217;s &#8220;spouse&#8221; is the person to whom one has &#8220;pledged&#8221; or &#8220;promised&#8221; oneself.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='espouse#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Espouse" src="https://cdn1.membean.com/public/images/wordimages/cons2/espouse.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='espouse#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abet</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>accede</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>advocate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>affiliation</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>assimilate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>bolster</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>buttress</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>coalition</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>confluence</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>conjugal</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>emissary</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>exhort</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>exponent</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>proponent</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>abjure</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>apathy</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>contravene</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>decry</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>defile</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>deprecate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>derision</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>disparage</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>dissolution</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>gainsay</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>impugn</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>pillory</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>remonstrate</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>renounce</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>repudiate</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>undermine</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="espouse" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>espouse</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="espouse#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

