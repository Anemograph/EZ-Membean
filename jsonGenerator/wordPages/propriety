
<!DOCTYPE html>
<html>
<head>
<title>Word: propriety | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx1' style='display:none'>If something is an anomaly, it is different from what is usual or expected; hence, it is highly noticeable.</p>
<p class='rw-defn idx2' style='display:none'>Something that is apposite is relevant or suitable to what is happening or being discussed.</p>
<p class='rw-defn idx3' style='display:none'>A condign reward or punishment is deserved by and appropriate or fitting for the person who receives it.</p>
<p class='rw-defn idx4' style='display:none'>A man who is debonair wears fashionable clothes and is sophisticated, charming, friendly, and confident.</p>
<p class='rw-defn idx5' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx6' style='display:none'>A degenerate person is immoral, wicked, or corrupt.</p>
<p class='rw-defn idx7' style='display:none'>Something, such as a building, is derelict if it is empty, not used, and in bad condition or disrepair.</p>
<p class='rw-defn idx8' style='display:none'>When someone&#8217;s behavior is deviating, they do things differently by departing or straying from their usual way of acting.</p>
<p class='rw-defn idx9' style='display:none'>If you refer to the enormity of a situation, problem, or event, you mean that it is very evil or morally offensive; this word has nothing to do with physical size.</p>
<p class='rw-defn idx10' style='display:none'>Errant things or people behave in a way that is unacceptable or wrong, such as a missile that travels in the wrong direction or a student who does not follow classroom rules.</p>
<p class='rw-defn idx11' style='display:none'>When you act in an imprudent fashion, you do something that is unwise, is lacking in good judgment, or has no forethought.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is indiscreet shows lack of judgment, especially because they say or do things in public that should only be said or done privately—if at all.</p>
<p class='rw-defn idx13' style='display:none'>To interject is to insert a comment during a conversation that interrupts its flow.</p>
<p class='rw-defn idx14' style='display:none'>When you interpose, you interrupt or interfere in some fashion.</p>
<p class='rw-defn idx15' style='display:none'>Kitsch is cheap and showy art that sometimes amuses people because of its bad taste; it is often an inferior imitation of existing sophisticated art styles that have true value.</p>
<p class='rw-defn idx16' style='display:none'>A malapropism is an unintentional and usually humorous mistake you make when you use a word that sounds similar to the word you actually intended to use but means something completely different.</p>
<p class='rw-defn idx17' style='display:none'>Malfeasance is an unlawful act, especially one committed by a trusted public official.</p>
<p class='rw-defn idx18' style='display:none'>A nonconformist is unwilling to believe in the same things other people do or act in a fashion that society sets as a standard.</p>
<p class='rw-defn idx19' style='display:none'>A peccadillo is a slight or minor offense that can be overlooked.</p>
<p class='rw-defn idx20' style='display:none'>To be politic in a decision is to be socially wise and diplomatic.</p>
<p class='rw-defn idx21' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx22' style='display:none'>Someone who is profligate is lacking in restraint, which can manifest in carelessly and wastefully using resources, such as energy or money; this kind of person can also act in an immoral way.</p>
<p class='rw-defn idx23' style='display:none'>Protocol is the rules of conduct or proper behavior that a social situation demands.</p>
<p class='rw-defn idx24' style='display:none'>Rambunctious conduct is wild, unruly, very active, and sometimes hard to control.</p>
<p class='rw-defn idx25' style='display:none'>A rapscallion is a young rascal, naughty child, or an older person who is dishonest and a scoundrel.</p>
<p class='rw-defn idx26' style='display:none'>You show rectitude if you behave or conduct yourself in an honest and morally correct manner.</p>
<p class='rw-defn idx27' style='display:none'>A reprobate has a bad character and behaves in an immoral or improper way.</p>
<p class='rw-defn idx28' style='display:none'>If you commit a solecism, you make an error of some kind, such as one of a grammatical or social nature.</p>
<p class='rw-defn idx29' style='display:none'>An unorthodox opinion is unusual, not customary, and goes against established ways of thinking.</p>
<p class='rw-defn idx30' style='display:none'>A vicious person is corrupt, violent, aggressive, and/or highly immoral.</p>
<p class='rw-defn idx31' style='display:none'>Something vile is evil, very unpleasant, horrible, or extremely bad.</p>
<p class='rw-defn idx32' style='display:none'>If you act in a wayward fashion, you are very headstrong, independent, disobedient, unpredictable, and practically unmanageable.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>propriety</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='propriety#' id='pronounce-sound' path='audio/words/amy-propriety'></a>
pruh-PRAHY-i-tee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='propriety#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='propriety#' id='context-sound' path='audio/wordcontexts/brian-propriety'></a>
Annabelle acted with perfect <em>propriety</em> in all her dealings with clients; in fact, she had the best manners of anyone on the real estate staff.  Annabelle had a real taste for correctness in social behavior, and felt obligated to act with <em>propriety</em> even when customers were rude and inconsiderate.  Annabelle&#8217;s <em>propriety</em> and suitableness in her dealings with clients were good for business, as the customers always left with a good opinion of her and her company.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What would someone who cares about <em>propriety</em> do?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
They would speak and act in a way that is socially correct.
</li>
<li class='choice '>
<span class='result'></span>
They would only accept a job if it interested them.
</li>
<li class='choice '>
<span class='result'></span>
They would invest all of their money in real estate.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='propriety#' id='definition-sound' path='audio/wordmeanings/amy-propriety'></a>
<em>Propriety</em> is behaving in a socially acceptable and appropriate way.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>accepted conduct</em>
</span>
</span>
</div>
<a class='quick-help' href='propriety#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='propriety#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/propriety/memory_hooks/4126.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Proper</span></span> Soc<span class="emp2"><span>iety</span></span></span></span> Behaving with <span class="emp1"><span>propr</span></span><span class="emp2"><span>iety</span></span> is acting like "<span class="emp1"><span>proper</span></span> soc<span class="emp2"><span>iety</span></span>".
</p>
</div>

<div id='memhook-button-bar'>
<a href="propriety#" id="add-public-hook" style="" url="https://membean.com/mywords/propriety/memory_hooks">Use other public hook</a>
<a href="propriety#" id="memhook-use-own" url="https://membean.com/mywords/propriety/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='propriety#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
For much of the past century, stern looks and curt shushes from other patrons greeted the person who dared clap at the "wrong" time at a classical concert. Conductors, too, sometimes got carried up in the idea, acting like policemen of <b>propriety</b>.
<cite class='attribution'>
&mdash;
Pittsburg Post-Gazette
</cite>
</li>
<li>
Royal guidelines state that gifts offered from businesses in the UK "should normally be declined," unless they're a souvenir from an official visit or in celebration for a royal marriage or other personal occasion. . . . And when it comes to gifts from individuals royals don't know personally, the offering "should be refused where there are concerns about the <b>propriety</b> or motives of the donor or the gift itself."
<cite class='attribution'>
&mdash;
Harper's Bazaar
</cite>
</li>
<li>
At this point, his father, a Houston native, steps in and explains that yes, Patrick has been talked to about what was wrong about the Astros’ actions, and the right things to do in these situations and that Patrick knows far more about <b>propriety</b> than he’s letting on.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
The project of holding a National Defense Day on Sept. 12 has met considerable opposition from pacifist bodies. President Coolidge under-took last week to uphold the <b>propriety</b> of the proposed "Day" in a letter to the National Council for the Prevention of War.
<cite class='attribution'>
&mdash;
TIME, from 1924
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/propriety/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='propriety#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='prop_proper' data-tree-url='//cdn1.membean.com/public/data/treexml' href='propriety#'>
<span class=''></span>
prop
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>proper, fitting</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ty_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='propriety#'>
<span class=''></span>
-ty
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state, quality</td>
</tr>
</table>
<p><em>Propriety</em> is the &#8220;state of proper&#8221; social behavior, or the &#8220;quality of fitting&#8221; conduct in public.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='propriety#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: The Tom Lehrer Wisdom Channel: Poisoning Pigeons in the Park</strong><span> Publicly killing pigeons shows a lack of propriety.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/propriety.jpg' video_url='examplevids/propriety' video_width='350'></span>
<div id='wt-container'>
<img alt="Propriety" height="288" src="https://cdn1.membean.com/video/examplevids/propriety.jpg" width="350" />
<div class='center'>
<a href="propriety#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='propriety#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Propriety" src="https://cdn3.membean.com/public/images/wordimages/cons2/propriety.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='propriety#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>apposite</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>condign</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>debonair</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>politic</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>protocol</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>rectitude</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>anomaly</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>degenerate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>derelict</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>deviate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>enormity</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>errant</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>imprudent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>indiscreet</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>interject</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>interpose</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>kitsch</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>malapropism</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>malfeasance</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>nonconformist</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>peccadillo</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>profligate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>rambunctious</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>rapscallion</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>reprobate</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>solecism</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>unorthodox</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>vicious</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>vile</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>wayward</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='propriety#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
impropriety
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>inappropriateness of behavior</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="propriety" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>propriety</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="propriety#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

