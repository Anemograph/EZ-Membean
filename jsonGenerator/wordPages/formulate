
<!DOCTYPE html>
<html>
<head>
<title>Word: formulate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Formulate-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/formulate-large.jpg?qdep8" />
</div>
<a href="formulate#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>If you adjudicate a competition or dispute, you officially decide who is right or what should be done concerning any difficulties that may arise.</p>
<p class='rw-defn idx1' style='display:none'>If you describe a decision, rule, or plan as arbitrary, you think that it was decided without any thought, standard, or system to guide it; therefore, it can seem unfair.</p>
<p class='rw-defn idx2' style='display:none'>If you describe someone as articulate, you mean that they are able to express their thoughts, arguments, and ideas clearly and effectively.</p>
<p class='rw-defn idx3' style='display:none'>Someone who is capricious changes their mind repeatedly or behaves in unexpected ways; a capricious series of events follows no predictable pattern.</p>
<p class='rw-defn idx4' style='display:none'>A chimerical idea is unrealistic, imaginary, or unlikely to be fulfilled.</p>
<p class='rw-defn idx5' style='display:none'>Cogitating about something is thinking deeply about it for a long time.</p>
<p class='rw-defn idx6' style='display:none'>A conjecture is a theory or guess that is based on information that is not certain or complete.</p>
<p class='rw-defn idx7' style='display:none'>When you contemplate something, you either think about it deeply or gaze at it intently.</p>
<p class='rw-defn idx8' style='display:none'>When something is contrived, it is obviously thought about or planned beforehand, although it tries to be passed off as not being so.</p>
<p class='rw-defn idx9' style='display:none'>A cursory examination or reading of something is very quick, incomplete, and does not pay close attention to detail.</p>
<p class='rw-defn idx10' style='display:none'>If you deliberate, you think about or discuss something very carefully, especially before making an important decision.</p>
<p class='rw-defn idx11' style='display:none'>If you delineate something, such as an idea or situation or border, you describe it in great detail.</p>
<p class='rw-defn idx12' style='display:none'>Empirical evidence or study is based on real experience or scientific experiments rather than on unproven theories.</p>
<p class='rw-defn idx13' style='display:none'>When you envisage something, you imagine or consider its future possibility.</p>
<p class='rw-defn idx14' style='display:none'>To explicate an idea or plan is to make it clear by explaining it.</p>
<p class='rw-defn idx15' style='display:none'>An exposition is a detailed explanation or setting forth of an idea, theory, or problem, either in a written or spoken format.</p>
<p class='rw-defn idx16' style='display:none'>Something that is extemporaneous, such as an action, speech, or performance, is done without any preparation or practice beforehand.</p>
<p class='rw-defn idx17' style='display:none'>Someone who is fastidious cares excessively about small details and wants to keep everything correct, tidy, very clean, and in perfect order.</p>
<p class='rw-defn idx18' style='display:none'>If you fathom something complicated or mysterious, you are able to understand it.</p>
<p class='rw-defn idx19' style='display:none'>If you think something, such as a statement or idea, is fatuous, you consider it stupid or extremely silly; a fatuous hope is unrealistic.</p>
<p class='rw-defn idx20' style='display:none'>A heuristic method of teaching encourages learning based on students&#8217; discovering and experiencing things for themselves.</p>
<p class='rw-defn idx21' style='display:none'>Something that is hypothetical is based on possible situations or events rather than actual ones.</p>
<p class='rw-defn idx22' style='display:none'>An impromptu speech is unplanned or spontaneous—it has not been practiced in any way beforehand.</p>
<p class='rw-defn idx23' style='display:none'>When someone improvises, they make something up at once because an unexpected situation has arisen.</p>
<p class='rw-defn idx24' style='display:none'>When you act in an imprudent fashion, you do something that is unwise, is lacking in good judgment, or has no forethought.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is impulsive tends to do things without thinking about them ahead of time; therefore, their actions can be unpredictable.</p>
<p class='rw-defn idx26' style='display:none'>An inadvertent action is not done intentionally; rather, it is an accident that happens because someone is not being attentive to their surroundings.</p>
<p class='rw-defn idx27' style='display:none'>A judicious person shows good or sound judgment because they are wise and careful in making decisions.</p>
<p class='rw-defn idx28' style='display:none'>Someone is considered meticulous when they act with careful attention to detail.</p>
<p class='rw-defn idx29' style='display:none'>If you describe something as nebulous, you mean that it is unclear, vague, and not clearly defined; a shape that is nebulous has no clear boundaries.</p>
<p class='rw-defn idx30' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx31' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx32' style='display:none'>A perfunctory action is done with little care or interest; consequently, it is only completed because it is expected of someone to do so.</p>
<p class='rw-defn idx33' style='display:none'>To be punctilious is to pay precise attention to detail.</p>
<p class='rw-defn idx34' style='display:none'>Quixotic plans or ideas are not very practical or realistic; they are often based on unreasonable hopes for improving the world.</p>
<p class='rw-defn idx35' style='display:none'>A rubric is a set of instructions at the beginning of a document, such as an examination or term paper, that is usually printed in a different style so as to highlight its importance.</p>
<p class='rw-defn idx36' style='display:none'>If you ruminate on something, you think carefully and deeply about it over a long period of time.</p>
<p class='rw-defn idx37' style='display:none'>A scrupulous person takes great care to do everything in an extremely honest, conscientious, and fair manner.</p>
<p class='rw-defn idx38' style='display:none'>Spontaneity is freedom to act when and how you want to, often in an unpredictable or unplanned way.</p>
<p class='rw-defn idx39' style='display:none'>To be tentative is to be hesitant or uncertain about something; an agreement or decision of this kind is likely to have changes before it reaches its final form.</p>
<p class='rw-defn idx40' style='display:none'>If you do something in an unwitting fashion, you didn&#8217;t know that you were doing it; therefore, it was unintentional on your part.</p>
<p class='rw-defn idx41' style='display:none'>A utilitarian object is useful, serviceable, and practical—rather than fancy or unnecessary.</p>
<p class='rw-defn idx42' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Verb</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>formulate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='formulate#' id='pronounce-sound' path='audio/words/amy-formulate'></a>
FAWR-myuh-layt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='formulate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='formulate#' id='context-sound' path='audio/wordcontexts/brian-formulate'></a>
The thieves <em>formulated</em> or carefully planned out the perfect way to steal the Mona Lisa, the most precious painting in the world.  As they <em>formulated</em> or systematically considered their complicated theft, they took into account absolutely every detail and trap they could think of.  Although they <em>formulated</em> or drew up the plan to perfection, they did not consider to whom to sell the priceless painting once they had it, for no one wanted to take the risk to buy it!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What happens when you <em>formulate</em> something?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
You invite other people to join you in fighting against it.
</li>
<li class='choice answer '>
<span class='result'></span>
You plan it carefully by thinking about it ahead of time.
</li>
<li class='choice '>
<span class='result'></span>
You use what you know to make a reasonable guess about it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='formulate#' id='definition-sound' path='audio/wordmeanings/amy-formulate'></a>
When you <em>formulate</em> a plan of action, you carefully work it out or design it in great detail ahead of time.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>plan</em>
</span>
</span>
</div>
<a class='quick-help' href='formulate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='formulate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/formulate/memory_hooks/3418.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Form</span></span> and Reg<span class="emp3"><span>ulate</span></span></span></span> When you <span class="emp1"><span>form</span></span><span class="emp3"><span>ulate</span></span> a hypothesis, you carefully <span class="emp1"><span>form</span></span> and reg<span class="emp3"><span>ulate</span></span> it in a detailed way to make sure that it can be proven.
</p>
</div>

<div id='memhook-button-bar'>
<a href="formulate#" id="add-public-hook" style="" url="https://membean.com/mywords/formulate/memory_hooks">Use other public hook</a>
<a href="formulate#" id="memhook-use-own" url="https://membean.com/mywords/formulate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='formulate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The site is now a UNESCO World Heritage Monument, and it took 18 months for Athanasoulis and his colleagues to <b>formulate</b> a plan for installing the sculptures in a way that would not cause damage to the archaeology.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
Two new tropical storms have emerged in the eastern Atlantic Ocean and one could threaten some of the Caribbean Islands this week. . . . Now is the time for people on the islands to review or <b>formulate</b> a plan of action.
<cite class='attribution'>
&mdash;
Yahoo News
</cite>
</li>
<li>
The first step towards savings is to <b>formulate</b> a budget plan. A budget plan serves as a blueprint for tracking expenses and plug gaps that result in unnecessary spending.
<cite class='attribution'>
&mdash;
Forbes
</cite>
</li>
<li>
"The same people who helped me <b>formulate</b> my health care plan are the people who helped <b>formulate</b> the Clinton budget in the 1990s," Kerry said.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/formulate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='formulate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='form_shape' data-tree-url='//cdn1.membean.com/public/data/treexml' href='formulate#'>
<span class=''></span>
form
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>shape</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ul_little' data-tree-url='//cdn1.membean.com/public/data/treexml' href='formulate#'>
<span class=''></span>
-ul-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>little</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ate_quality' data-tree-url='//cdn1.membean.com/public/data/treexml' href='formulate#'>
<span class=''></span>
-ate
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make something have a certain quality</td>
</tr>
</table>
<p>To <em>formulate</em> something, such as a plan or action, is to &#8220;make it have a shape&#8221; by carefully planning or working it out.  Note that a <em>formula</em> in mathematics is an equation which gives a pattern or &#8220;small form or shape&#8221; to a rule or fact, which mathematicians <em>formulate</em>.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='formulate#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Toy Story</strong><span> The toys are forced to formulate a new plan.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/formulate.jpg' video_url='examplevids/formulate' video_width='350'></span>
<div id='wt-container'>
<img alt="Formulate" height="288" src="https://cdn1.membean.com/video/examplevids/formulate.jpg" width="350" />
<div class='center'>
<a href="formulate#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='formulate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Formulate" src="https://cdn3.membean.com/public/images/wordimages/cons2/formulate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='formulate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adjudicate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>articulate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>cogitate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>conjecture</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>contemplate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>contrived</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>deliberate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>delineate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>empirical</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>envisage</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>explicate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>exposition</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>fastidious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>fathom</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>heuristic</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>hypothetical</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>judicious</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>meticulous</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>punctilious</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>rubric</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>ruminate</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>scrupulous</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>utilitarian</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>arbitrary</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>capricious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>chimerical</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>cursory</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>extemporaneous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>fatuous</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>impromptu</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>improvise</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>imprudent</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>impulsive</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>inadvertent</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>nebulous</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>perfunctory</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>quixotic</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>spontaneity</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>tentative</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>unwitting</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="formulate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>formulate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="formulate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

