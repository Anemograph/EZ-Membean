
<!DOCTYPE html>
<html>
<head>
<title>Word: opus | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An artifact is a weapon, tool, or piece of art created by human beings that is historically and culturally interesting or valuable.</p>
<p class='rw-defn idx1' style='display:none'>When you claim that something is banal, you do not like it because you think it is ordinary, dull, commonplace, and boring.</p>
<p class='rw-defn idx2' style='display:none'>A bravura performance, such as of a highly technical work for the violin, is done with consummate skill.</p>
<p class='rw-defn idx3' style='display:none'>A compendium is a detailed collection of information on a particular or specific subject, usually in a book.</p>
<p class='rw-defn idx4' style='display:none'>A connoisseur is an appreciative, informed, and knowledgeable judge of the fine arts; they can also just have excellent or perceptive taste in the fine arts without being an expert.</p>
<p class='rw-defn idx5' style='display:none'>Efficacy is the ability or power to produce an expected effect or result.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx7' style='display:none'>If you are lethargic, you are tired, lack energy, and are unwilling to exert effort.</p>
<p class='rw-defn idx8' style='display:none'>Something mediocre is average or ordinary in quality; it’s just OK.</p>
<p class='rw-defn idx9' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx10' style='display:none'>Something prosaic is dull, boring, and ordinary.</p>
<p class='rw-defn idx11' style='display:none'>To be punctilious is to pay precise attention to detail.</p>
<p class='rw-defn idx12' style='display:none'>If your body is affected by torpor, you are severely lacking in energy; therefore, you are idle—and can even be numb.</p>
<p class='rw-defn idx13' style='display:none'>If you transcribe something, such as a speech or other text, you write it or type it in full.</p>
<p class='rw-defn idx14' style='display:none'>A trite remark or idea is so overused that it is no longer interesting or novel.</p>
<p class='rw-defn idx15' style='display:none'>Verbiage is an excessive use of words to convey something that could be expressed using fewer words; it can also be the manner or style in which someone uses words.</p>
<p class='rw-defn idx16' style='display:none'>A virtuoso is someone who is very skillful at something, especially playing a musical instrument.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>opus</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='opus#' id='pronounce-sound' path='audio/words/amy-opus'></a>
OH-puhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='opus#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='opus#' id='context-sound' path='audio/wordcontexts/brian-opus'></a>
Miriam&#8217;s master <em>opus</em> or most important work was an opera score with full orchestral arrangement.  To her professional delight, this <em>opus</em> or artistic achievement was performed at the Metropolitan Opera in New York City.  Her crowning <em>opus</em> or creation was well received by listeners and critics alike.  The cast and orchestra were honored with a standing ovation for Miriam&#8217;s <em>opus</em> or musical composition, during which Miriam took a bow as well.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What can be considered an <em>opus</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
An elegant mathematical proof by Euler.
</li>
<li class='choice answer '>
<span class='result'></span>
A piano concerto by Mozart.
</li>
<li class='choice '>
<span class='result'></span>
The discovery of the structure of <span class="caps">DNA</span> by Watson and Crick.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='opus#' id='definition-sound' path='audio/wordmeanings/amy-opus'></a>
An <em>opus</em> is an important piece of artistic work by a writer, painter, musician, etc.; an <em>opus</em> can also be one in a series of numbered musical works by the same composer.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>artistic work</em>
</span>
</span>
</div>
<a class='quick-help' href='opus#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='opus#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/opus/memory_hooks/3404.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>O</span></span> <span class="emp3"><span>Pus</span></span>sy! <span class="emp1"><span>O</span></span> <span class="emp3"><span>Pus</span></span>sy!</span></span> <span class="emp1"><span>O</span></span> <span class="emp3"><span>pus</span></span>sy cat!  <span class="emp1"><span>O</span></span> <span class="emp3"><span>pus</span></span>sy cat!  What did you do with my great <span class="emp1"><span>o</span></span><span class="emp3"><span>pus</span></span> of which I only have one c<span class="emp1"><span>o</span></span><span class="emp3"><span>p</span></span>y?  That master concerto that took me five years to write?  <span class="emp1"><span>O</span></span> <span class="emp3"><span>pus</span></span>sy cat!  <span class="emp1"><span>O</span></span> <span class="emp3"><span>pus</span></span>sy?!
</p>
</div>

<div id='memhook-button-bar'>
<a href="opus#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/opus/memory_hooks">Use other hook</a>
<a href="opus#" id="memhook-use-own" url="https://membean.com/mywords/opus/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='opus#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The book is the second of three volumes in The Baroque Cycle, a nearly 3,000-page <b>opus</b> that fictionalizes the exploits of Newton and the Royal Society of scientists to which he belonged.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
This has to be one of the most original <b>opus</b> No. 1s in the history of music, because it fuses the two warring schools of modern music—those of Stravinsky and Schoenberg.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/opus/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='opus#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='oper_work' data-tree-url='//cdn1.membean.com/public/data/treexml' href='opus#'>
<span class=''></span>
oper
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>work</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='us_noun' data-tree-url='//cdn1.membean.com/public/data/treexml' href='opus#'>
<span class=''></span>
-us
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>forms a Latin noun</td>
</tr>
</table>
<p>An <em>opus</em> is an artistic &#8220;work.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='opus#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Opus" src="https://cdn1.membean.com/public/images/wordimages/cons2/opus.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='opus#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>artifact</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bravura</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>compendium</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>connoisseur</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>efficacy</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>punctilious</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>transcribe</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>verbiage</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>virtuoso</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>banal</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>lethargic</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>mediocre</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>prosaic</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>torpor</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>trite</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="opus" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>opus</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="opus#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

