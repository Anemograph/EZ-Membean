
<!DOCTYPE html>
<html>
<head>
<title>Word: medley | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Medley-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/medley-large.jpg?qdep8" />
</div>
<a href="medley#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>To agglomerate a group of things is to gather them together without any noticeable order.</p>
<p class='rw-defn idx1' style='display:none'>An autonomous person makes their own decisions without being influenced by anyone else; an autonomous country or organization is independent and has the power to govern itself.</p>
<p class='rw-defn idx2' style='display:none'>If two or more things coalesce, they come together to form a single larger unit.</p>
<p class='rw-defn idx3' style='display:none'>A coalition is a temporary union of different political or social groups that agrees to work together to achieve a shared aim.</p>
<p class='rw-defn idx4' style='display:none'>If you concatenate two or more things, you join them together by linking them one after the other.</p>
<p class='rw-defn idx5' style='display:none'>Things that are disparate are clearly different from each other and belong to different groups or classes.</p>
<p class='rw-defn idx6' style='display:none'>An eclectic assortment of things or people comes from many varied sources; additionally, it usually includes the best of those sources.</p>
<p class='rw-defn idx7' style='display:none'>Ecumenical activities and ideas encourage different religions or congregations to work and worship together in order to unite them in friendship.</p>
<p class='rw-defn idx8' style='display:none'>A farrago is a confused collection or mixture of different types of things.</p>
<p class='rw-defn idx9' style='display:none'>A heterogeneous grouping is made up of many differing or unlike parts.</p>
<p class='rw-defn idx10' style='display:none'>The word homogeneous is used to describe a group that has members or parts which are similar or are all of the same type.</p>
<p class='rw-defn idx11' style='display:none'>A melange is a collection of different kinds of things.</p>
<p class='rw-defn idx12' style='display:none'>A collection of things is motley if its parts are so different that they do not seem to belong in the same space; groups of widely variegated people can also be described as motley.</p>
<p class='rw-defn idx13' style='display:none'>Something that is multifarious is made up of many kinds of different things.</p>
<p class='rw-defn idx14' style='display:none'>A pastiche is a piece of writing, music, or film, etc. that is deliberately made in the style of other works; it can also use a variety of such copied material in order to laugh in a gentle way at those works.</p>
<p class='rw-defn idx15' style='display:none'>A smorgasbord is a wide variety of things, generally but not always pertaining to a sizable buffet.</p>
<p class='rw-defn idx16' style='display:none'>Sundry people or things are all different from each other and cannot form a single group; nevertheless, they can be thought of as being together for the sake of ease, such as the various items in a garage.</p>
<p class='rw-defn idx17' style='display:none'>Something that is variegated has various tones or colors; it can also mean filled with variety.</p>
<p class='rw-defn idx18' style='display:none'>A welter of something is a large, overwhelming, and confusing amount of it; this word can also refer to a state of confusion, disorder, or turmoil.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>medley</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='medley#' id='pronounce-sound' path='audio/words/amy-medley'></a>
MED-lee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='medley#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='medley#' id='context-sound' path='audio/wordcontexts/brian-medley'></a>
The musical <em>medley</em> or varied selection of songs played pleasantly over the loudspeaker at Fred&#8217;s Costume Shop.  In honor of the theater, Fred played music from many Broadway shows to accompany his <em>medley</em> or assorted collection of odd and brightly designed costumes.  That evening, an excited bunch of young customers came to buy a <em>medley</em> or random variety of props and paints for their Halloween party.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>medley</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A mix of different songs that are combined into one song.
</li>
<li class='choice '>
<span class='result'></span>
A list of tasks that must be done in preparation for a big event.
</li>
<li class='choice '>
<span class='result'></span>
A large number of people running for the same political office.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='medley#' id='definition-sound' path='audio/wordmeanings/amy-medley'></a>
A <em>medley</em> is a collection of various things, such as different tunes in a song or different types of food in a meal.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>varying mixture</em>
</span>
</span>
</div>
<a class='quick-help' href='medley#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='medley#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/medley/memory_hooks/3390.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Mel</span></span>o<span class="emp3"><span>dy</span></span> <span class="emp3"><span>Medl</span></span>e<span class="emp3"><span>y</span></span></span></span> I have a favorite <span class="emp3"><span>medl</span></span>e<span class="emp3"><span>y</span></span> of <span class="emp3"><span>mel</span></span>o<span class="emp3"><span>d</span></span>ies that I like to play whenever I work out in the gym--my <span class="emp3"><span>mel</span></span>o<span class="emp3"><span>dy</span></span> <span class="emp3"><span>medl</span></span>e<span class="emp3"><span>y</span></span> keeps me really pumped.
</p>
</div>

<div id='memhook-button-bar'>
<a href="medley#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/medley/memory_hooks">Use other hook</a>
<a href="medley#" id="memhook-use-own" url="https://membean.com/mywords/medley/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='medley#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The mornings are crisper now in Portland, Ore. The sun rises later and sets earlier, and the local farmers' markets are running out of their <b>medley</b> of green, yellow, and orange heirloom tomatoes.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
[Carrie] Underwood marked the 95th anniversary of the iconic Grand Ole Opry by singing a <b>medley</b> of songs by fellow Opry members Patsy Cline, Loretta Lynn, Barbara Mandrell, Martina McBride, Reba McEntire and Dolly Parton.
<cite class='attribution'>
&mdash;
People Magazine
</cite>
</li>
<li>
What happens when you combine two passionate chefs, a celebration of Central American food, and a dash of spice? Masa Taqueria! Chefs Chris Caldaróne and Jonathon Kirk started doling out their mouth-watering <b>medley</b> of flavors last winter . . . .
<cite class='attribution'>
&mdash;
Providence Monthly
</cite>
</li>
<li>
The [Mississippi Mudbug Festival] lineup includes a <b>medley</b> of country, southern rock and Zydeco music.
<cite class='attribution'>
&mdash;
The Associated Press
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/medley/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='medley#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='medl_blend' data-tree-url='//cdn1.membean.com/public/data/treexml' href='medley#'>
<span class=''></span>
medl
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>blend, mingle, mix</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='y_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='medley#'>
<span class=''></span>
-y
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>body, group, collection</td>
</tr>
</table>
<p>A <em>medley</em> of something is a &#8220;body, group, or collection&#8221; that is &#8220;blended, mingled, or mixed.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='medley#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Moosebutter</strong><span> A Star Wars musical medley.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/medley.jpg' video_url='examplevids/medley' video_width='350'></span>
<div id='wt-container'>
<img alt="Medley" height="288" src="https://cdn1.membean.com/video/examplevids/medley.jpg" width="350" />
<div class='center'>
<a href="medley#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='medley#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Medley" src="https://cdn3.membean.com/public/images/wordimages/cons2/medley.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='medley#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>agglomerate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>coalesce</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>coalition</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>concatenate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>disparate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>eclectic</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>ecumenical</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>farrago</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>heterogeneous</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>melange</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>motley</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>multifarious</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>pastiche</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>smorgasbord</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>sundry</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>variegated</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>welter</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>autonomous</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>homogeneous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="medley" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>medley</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="medley#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

