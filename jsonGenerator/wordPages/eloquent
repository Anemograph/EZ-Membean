
<!DOCTYPE html>
<html>
<head>
<title>Word: eloquent | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you describe someone as articulate, you mean that they are able to express their thoughts, arguments, and ideas clearly and effectively.</p>
<p class='rw-defn idx1' style='display:none'>A clarion call is a stirring, emotional, and strong appeal to people to take action on something.</p>
<p class='rw-defn idx2' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx3' style='display:none'>Something convoluted, such as a difficult concept or procedure, is complex and takes many twists and turns.</p>
<p class='rw-defn idx4' style='display:none'>A deft movement or action is made quickly and with skill.</p>
<p class='rw-defn idx5' style='display:none'>Someone&#8217;s elocution is their artistic manner of speaking in public, including both the delivery of their voice and gestures.</p>
<p class='rw-defn idx6' style='display:none'>If you elucidate something, you make it easier to understand by giving more relevant information.</p>
<p class='rw-defn idx7' style='display:none'>When you enunciate, you speak or state something clearly so that you can be easily understood.</p>
<p class='rw-defn idx8' style='display:none'>To explicate an idea or plan is to make it clear by explaining it.</p>
<p class='rw-defn idx9' style='display:none'>When someone gesticulates, they make movements with their hands and arms when talking, usually because they want to emphasize something or are having difficulty in expressing an idea using words alone.</p>
<p class='rw-defn idx10' style='display:none'>If someone is being glib, they make something sound simple, easy, and problem-free— when it isn&#8217;t at all.</p>
<p class='rw-defn idx11' style='display:none'>Grandiloquent speech is highly formal, exaggerated, and often seems both silly and hollow because it is expressly used to appear impressive and important.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is speaking in an incoherent fashion cannot be understood or is very hard to understand.</p>
<p class='rw-defn idx13' style='display:none'>A person who is being laconic uses very few words to say something.</p>
<p class='rw-defn idx14' style='display:none'>Limpid speech or prose is clear, simple, and easily understood; things, such as pools of water or eyes, are limpid if they are clear or transparent.</p>
<p class='rw-defn idx15' style='display:none'>Someone who is loquacious talks too much.</p>
<p class='rw-defn idx16' style='display:none'>If someone is lucid, they are able to understand things and think clearly; this adjective also describes writing or speech that is crystal clear and easy to understand.</p>
<p class='rw-defn idx17' style='display:none'>If you misconstrue something that has been said or something that happens, you understand or interpret it incorrectly.</p>
<p class='rw-defn idx18' style='display:none'>If you describe something as nebulous, you mean that it is unclear, vague, and not clearly defined; a shape that is nebulous has no clear boundaries.</p>
<p class='rw-defn idx19' style='display:none'>To obfuscate something is to make it deliberately unclear, confusing, and difficult to understand.</p>
<p class='rw-defn idx20' style='display:none'>Something is opaque if it is either difficult to understand or is not transparent.</p>
<p class='rw-defn idx21' style='display:none'>Something that is pellucid is either extremely clear because it is transparent to the eye or it is very easy for the mind to understand.</p>
<p class='rw-defn idx22' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx23' style='display:none'>Something that is prolix, such as a lecture or speech, is excessively wordy; consequently, it can be tiresome to read or listen to.</p>
<p class='rw-defn idx24' style='display:none'>To ramble is to wander about leisurely, with no specific destination in mind; to ramble while speaking is to talk with no particular aim or point intended.</p>
<p class='rw-defn idx25' style='display:none'>To rant is to go on an angry verbal attack.</p>
<p class='rw-defn idx26' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx27' style='display:none'>Rhetoric is the skill or art of using language to persuade or influence people, especially language that sounds impressive but may not be sincere or honest.</p>
<p class='rw-defn idx28' style='display:none'>If someone is sententious, they are terse or brief in writing and speech; they also often use proverbs to appear morally upright and wise.</p>
<p class='rw-defn idx29' style='display:none'>A stentorian voice is extremely loud and strong.</p>
<p class='rw-defn idx30' style='display:none'>Something that is succinct is clearly and briefly explained without using any unnecessary words.</p>
<p class='rw-defn idx31' style='display:none'>A tacit agreement between two people is understood without having to use words to express it.</p>
<p class='rw-defn idx32' style='display:none'>A taciturn person is quiet or silent by nature.</p>
<p class='rw-defn idx33' style='display:none'>A truculent person is bad-tempered, easily annoyed, and prone to arguing excessively.</p>
<p class='rw-defn idx34' style='display:none'>If someone is unlettered, they are illiterate, unable to read and write.</p>
<p class='rw-defn idx35' style='display:none'>Something that is verbose, such as a speech or article, contains too many words.</p>
<p class='rw-defn idx36' style='display:none'>A voluble person can speak easily with speech that naturally flows; voluble people also tend to be quite chatty.</p>
<p class='rw-defn idx37' style='display:none'>A yokel is uneducated, naive, and unsophisticated; they do not know much about modern life or ideas because they sequester themselves in a rural setting.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>eloquent</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='eloquent#' id='pronounce-sound' path='audio/words/amy-eloquent'></a>
EL-uh-kwint
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='eloquent#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='eloquent#' id='context-sound' path='audio/wordcontexts/brian-eloquent'></a>
The <em>eloquent</em> or highly persuasive speaker is so talented at presenting and beautifully using words.  He spoke so <em>eloquently</em>, clearly, and persuasively that we could have listened to him for hours.  As a representative of his company, he brings in business with his <em>eloquent</em>, stirring, and moving silver tongue.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How does someone speak if they are <em>eloquent</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They speak for a long time and often bore their listeners.
</li>
<li class='choice answer '>
<span class='result'></span>
They use expressive language that persuades others to act.
</li>
<li class='choice '>
<span class='result'></span>
They use as few words as possible to say what they mean.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='eloquent#' id='definition-sound' path='audio/wordmeanings/amy-eloquent'></a>
When you speak in an <em>eloquent</em> fashion, you speak beautifully in an expressive way that is convincing to an audience.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>well-spoken</em>
</span>
</span>
</div>
<a class='quick-help' href='eloquent#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='eloquent#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/eloquent/memory_hooks/4945.json'></span>
<p>
<span class="emp0"><span>Fl<span class="emp3"><span>uent</span></span> in H<span class="emp1"><span>ello</span></span>s</span></span> My friend Mabel is highly <span class="emp1"><span>elo</span></span>q<span class="emp3"><span>uent</span></span> when it comes to saying the word "h<span class="emp1"><span>ello</span></span>;" she is fl<span class="emp3"><span>uent</span></span> in saying "h<span class="emp1"><span>ello</span></span>" in 63 different languages!
</p>
</div>

<div id='memhook-button-bar'>
<a href="eloquent#" id="add-public-hook" style="" url="https://membean.com/mywords/eloquent/memory_hooks">Use other public hook</a>
<a href="eloquent#" id="memhook-use-own" url="https://membean.com/mywords/eloquent/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='eloquent#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
It was actress Michelle Williams who raised the flag for equal pay, with an <b>eloquent</b> speech that was one of the most effective of the night.
<cite class='attribution'>
&mdash;
Austin American-Statesman
</cite>
</li>
<li>
Even though we have no audio record of Lincoln’s words, he still speaks to us through his expressive letters and his <b>eloquent</b> speeches.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
[Holland Taylor] speaks in a brisk yet <b>eloquent</b> manner—she will not waste a word, nor stumble nervously over an “umm” or a misplaced “like.” It is startlingly authoritative.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
“I have a Dream.” These four iconic words are a stark reminder that the man who spoke them in a powerful and <b>eloquent</b> speech in 1963 did not—as he predicted—live to see that dream.
<cite class='attribution'>
&mdash;
The Atlanta Voice
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/eloquent/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='eloquent#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_out' data-tree-url='//cdn1.membean.com/public/data/treexml' href='eloquent#'>
<span class='common'></span>
e-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>out</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='loqu_talk' data-tree-url='//cdn1.membean.com/public/data/treexml' href='eloquent#'>
<span class=''></span>
loqu
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>talk, speak</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ent_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='eloquent#'>
<span class=''></span>
-ent
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p>One who is <em>eloquent</em> &#8220;speaks out&#8221; well.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='eloquent#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Parks and Recreation</strong><span> An eloquent speech or just more of Leslie's empty words?</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/eloquent.jpg' video_url='examplevids/eloquent' video_width='350'></span>
<div id='wt-container'>
<img alt="Eloquent" height="288" src="https://cdn1.membean.com/video/examplevids/eloquent.jpg" width="350" />
<div class='center'>
<a href="eloquent#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='eloquent#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Eloquent" src="https://cdn3.membean.com/public/images/wordimages/cons2/eloquent.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='eloquent#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>articulate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>clarion</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>deft</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>elocution</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>elucidate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>enunciate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>explicate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>gesticulate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>glib</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>grandiloquent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>limpid</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>loquacious</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>lucid</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>pellucid</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>prolix</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>rhetoric</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>sententious</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>stentorian</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>succinct</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>verbose</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>voluble</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>convoluted</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>incoherent</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>laconic</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>misconstrue</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>nebulous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>obfuscate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>opaque</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>ramble</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>rant</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>tacit</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>taciturn</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>truculent</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>unlettered</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>yokel</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='eloquent#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
elocution
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>an artistic manner of public speaking</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="eloquent" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>eloquent</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="eloquent#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

