
<!DOCTYPE html>
<html>
<head>
<title>Word: inalienable | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you adulterate something, you lessen its quality by adding inferior ingredients or elements to it.</p>
<p class='rw-defn idx1' style='display:none'>Something that happens unexpectedly and by chance that is not inherent or natural to a given item or situation is adventitious, such as a root appearing in an unusual place on a plant.</p>
<p class='rw-defn idx2' style='display:none'>If you describe a decision, rule, or plan as arbitrary, you think that it was decided without any thought, standard, or system to guide it; therefore, it can seem unfair.</p>
<p class='rw-defn idx3' style='display:none'>A chronic illness or pain is serious and lasts for a long time; a chronic problem is always happening or returning and is very difficult to solve.</p>
<p class='rw-defn idx4' style='display:none'>A congenital condition is something someone is born with, such as a character trait or physical state.</p>
<p class='rw-defn idx5' style='display:none'>If something is contingent upon something else, the first thing depends on the second in order to happen or exist.</p>
<p class='rw-defn idx6' style='display:none'>The crux of a problem or argument is the most important or difficult part of it that affects everything else.</p>
<p class='rw-defn idx7' style='display:none'>Something ephemeral, such as some insects or a sunset, lasts for only a short time or has a very short lifespan.</p>
<p class='rw-defn idx8' style='display:none'>Something that is evanescent lasts for only a short time before disappearing from sight or memory.</p>
<p class='rw-defn idx9' style='display:none'>If something is extant, it is still in existence despite being very old.</p>
<p class='rw-defn idx10' style='display:none'>Something factitious is not genuine or natural; it is made to happen in a forced, artificial way.</p>
<p class='rw-defn idx11' style='display:none'>Things that fluctuate vary or change often, rising or falling seemingly at random.</p>
<p class='rw-defn idx12' style='display:none'>An immanent quality is present within something and is so much a part of it that the object cannot be imagined without that quality.</p>
<p class='rw-defn idx13' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx14' style='display:none'>When something is implicit, it is understood without having to say it.</p>
<p class='rw-defn idx15' style='display:none'>Incontrovertible facts are certain, unquestionably true, and impossible to doubt however hard you might try to convince yourself otherwise.</p>
<p class='rw-defn idx16' style='display:none'>If someone leaves an indelible impression on you, it will not be forgotten; an indelible mark is permanent or impossible to erase or remove.</p>
<p class='rw-defn idx17' style='display:none'>An inherent characteristic is one that exists in a person at birth or in a thing naturally.</p>
<p class='rw-defn idx18' style='display:none'>An innate quality of someone is present since birth or is a quality of something that is essential to it.</p>
<p class='rw-defn idx19' style='display:none'>Something that is integral to something else is an essential or necessary part of it.</p>
<p class='rw-defn idx20' style='display:none'>An intrinsic characteristic of something is the basic and essential feature that makes it what it is.</p>
<p class='rw-defn idx21' style='display:none'>An inveterate person is always doing a particular thing, especially something questionable—and they are not likely to stop doing it.</p>
<p class='rw-defn idx22' style='display:none'>Irrelevant information is unrelated or unconnected to the situation at hand.</p>
<p class='rw-defn idx23' style='display:none'>An irrevocable action or decision is impossible to change, reverse, or stop.</p>
<p class='rw-defn idx24' style='display:none'>When someone or something undergoes the process of metamorphosis, there is a change in appearance, character, or even physical shape.</p>
<p class='rw-defn idx25' style='display:none'>Something that is perennial lasts a very long time and is enduring.</p>
<p class='rw-defn idx26' style='display:none'>The periphery of a place is its boundary or outer edge.</p>
<p class='rw-defn idx27' style='display:none'>Your prerogative is your right or privilege to do something.</p>
<p class='rw-defn idx28' style='display:none'>Something or someone that is protean is adaptable, variable, or changeable in nature.</p>
<p class='rw-defn idx29' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>
<p class='rw-defn idx30' style='display:none'>Something that is sacrosanct is considered to be so important, special, or holy that no one is allowed to criticize, tamper with, or change it in any way.</p>
<p class='rw-defn idx31' style='display:none'>Something that is specious seems to be good, sound, or correct but is actually wrong or fake.</p>
<p class='rw-defn idx32' style='display:none'>A spurious statement is false because it is not based on sound thinking; therefore, it is not what it appears to be.</p>
<p class='rw-defn idx33' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx34' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx35' style='display:none'>Suffrage is the right of people to vote in public elections.</p>
<p class='rw-defn idx36' style='display:none'>Something that is superfluous is unnecessary; it is more than what is wanted or needed at the current time.</p>
<p class='rw-defn idx37' style='display:none'>A tangential point in an argument merely touches upon something that is not really relevant to the conversation at hand; rather, it is a minor or unimportant point.</p>
<p class='rw-defn idx38' style='display:none'>Something that is temporal deals with the present and somewhat brief time of this world.</p>
<p class='rw-defn idx39' style='display:none'>Something or someone that is tertiary is third in rank.</p>
<p class='rw-defn idx40' style='display:none'>Something that has the quality of transience lasts for only a short time or is constantly changing.</p>
<p class='rw-defn idx41' style='display:none'>If you are unflagging while doing a task, you are untiring when working upon it and do not stop until it is finished.</p>
<p class='rw-defn idx42' style='display:none'>When someone vacillates, they go back and forth about a choice or opinion, unable to make a firm decision.</p>
<p class='rw-defn idx43' style='display:none'>If someone vitiates something, they make it less effective, weaker, or reduced in quality.</p>
<p class='rw-defn idx44' style='display:none'>Something that is volatile can change easily and vary widely.</p>
<p class='rw-defn idx45' style='display:none'>If you waver, you cannot decide between two things because you have serious doubts about which choice is better.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>inalienable</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='inalienable#' id='pronounce-sound' path='audio/words/amy-inalienable'></a>
in-AY-lee-uhn-uh-buhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='inalienable#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='inalienable#' id='context-sound' path='audio/wordcontexts/brian-inalienable'></a>
Humans have <em>inalienable</em> or absolute rights that cannot be taken away.  The United Nations has outlined these <em>inalienable</em> or undeniable rights for children.  Some include the <em>inalienable</em> or basic right to be treated with love and respect, and the right to be clothed and fed.  There are supportive measures in place to assure these natural, <em>inalienable</em> needs are met.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean if your family owns <em>inalienable</em> rights to a property?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is land that your family pays to live on but that its members don&#8217;t actually own.
</li>
<li class='choice '>
<span class='result'></span>
It is protected land that the law says cannot be developed.
</li>
<li class='choice answer '>
<span class='result'></span>
That land cannot be taken away from your family for any reason.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='inalienable#' id='definition-sound' path='audio/wordmeanings/amy-inalienable'></a>
An <em>inalienable</em> right is a privilege that cannot be taken away.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>undeniable</em>
</span>
</span>
</div>
<a class='quick-help' href='inalienable#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='inalienable#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/inalienable/memory_hooks/3082.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Un</span></span>d<span class="emp1"><span>eniable</span></span></span></span> An <span class="emp1"><span>in</span></span>al<span class="emp1"><span>ienable</span></span> right is <span class="emp1"><span>un</span></span>d<span class="emp1"><span>eniable</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="inalienable#" id="add-public-hook" style="" url="https://membean.com/mywords/inalienable/memory_hooks">Use other public hook</a>
<a href="inalienable#" id="memhook-use-own" url="https://membean.com/mywords/inalienable/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='inalienable#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
And when he appealed for agreement on "a world beyond terror," he reached for a landmark U.N. document: the Universal Declaration of Human Rights. "This document declares that the "equal and <b>inalienable</b> rights of all members of the human family is the foundation of freedom and justice and peace in the world," Bush said.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
"All men and women are created equal, they have certain <b>inalienable</b> rights, and one of those is to practice their religion freely," Obama said. "You can build a church on a site, you can build a synagogue on a site, if you could build a Hindu temple on a site, you should be able to build a mosque."
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
Even political rights, like the right to vote, and nearly all other rights enumerated in the Constitution, are secondary to the <b>inalienable</b> human rights to “life, liberty and the pursuit of happiness” proclaimed in the Declaration of Independence; and to this category the right to home and marriage unquestionably belongs.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/inalienable/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='inalienable#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='in_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='inalienable#'>
<span class='common'></span>
in-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not</td>
</tr>
<tr>
<td class='partform'>alien</td>
<td>
&rarr;
</td>
<td class='meaning'>other</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='able_handy' data-tree-url='//cdn1.membean.com/public/data/treexml' href='inalienable#'>
<span class=''></span>
-able
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>handy, capable of</td>
</tr>
</table>
<p>An <em>inalienable</em> right is &#8220;not capable of other&#8221; status, that is, not becoming a right.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='inalienable#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Anthony Ibbott</strong><span> This living creature has inalienable rules by which it must live or suffer the fatal consequences.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/inalienable.jpg' video_url='examplevids/inalienable' video_width='350'></span>
<div id='wt-container'>
<img alt="Inalienable" height="288" src="https://cdn1.membean.com/video/examplevids/inalienable.jpg" width="350" />
<div class='center'>
<a href="inalienable#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='inalienable#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Inalienable" src="https://cdn2.membean.com/public/images/wordimages/cons2/inalienable.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='inalienable#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>chronic</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>congenital</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>crux</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>extant</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>immanent</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>implicit</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>incontrovertible</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>indelible</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>inherent</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>innate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>integral</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>intrinsic</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>inveterate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>irrevocable</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>perennial</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>prerogative</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>sacrosanct</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>suffrage</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>unflagging</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>adulterate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>adventitious</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>arbitrary</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>contingent</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>ephemeral</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>evanescent</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>factitious</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>fluctuate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>irrelevant</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>metamorphosis</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>periphery</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>protean</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>specious</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>spurious</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>superfluous</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>tangential</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>temporal</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>tertiary</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>transience</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>vacillate</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>vitiate</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>volatile</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>waver</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="inalienable" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>inalienable</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="inalienable#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

