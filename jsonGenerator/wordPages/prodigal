
<!DOCTYPE html>
<html>
<head>
<title>Word: prodigal | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Prodigal-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/prodigal-large.jpg?qdep8" />
</div>
<a href="prodigal#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>Someone who is abstemious avoids doing too much of something enjoyable, such as eating or drinking; rather, they consume in a moderate fashion.</p>
<p class='rw-defn idx1' style='display:none'>Abstinence is the practice of keeping away from or avoiding something you enjoy—such as the physical pleasures of excessive food and drink—usually for health or religious reasons.</p>
<p class='rw-defn idx2' style='display:none'>Something austere is simple and plain in its style; an austere person is strict and severe with themselves.</p>
<p class='rw-defn idx3' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx4' style='display:none'>A copious amount of something is a very large amount of it.</p>
<p class='rw-defn idx5' style='display:none'>A decadent person has low moral standards and is more interested in pleasure than serious matters.</p>
<p class='rw-defn idx6' style='display:none'>A deficit occurs when a person or government spends more money than has been received.</p>
<p class='rw-defn idx7' style='display:none'>If you deliberate, you think about or discuss something very carefully, especially before making an important decision.</p>
<p class='rw-defn idx8' style='display:none'>Something, such as a building, is derelict if it is empty, not used, and in bad condition or disrepair.</p>
<p class='rw-defn idx9' style='display:none'>When something dissipates, it either disappears because it is driven away, or it is completely used up in some way—sometimes wastefully.</p>
<p class='rw-defn idx10' style='display:none'>Dissolution is the breaking up or official end of a group, such as a couple or institution; it can also be the act of separating something into smaller components.</p>
<p class='rw-defn idx11' style='display:none'>If something is done for someone&#8217;s edification, it is done to benefit that person by teaching them something that improves or enlightens their knowledge or character.</p>
<p class='rw-defn idx12' style='display:none'>An exorbitant price or fee is much higher than what it should be or what is considered reasonable.</p>
<p class='rw-defn idx13' style='display:none'>An extravaganza is an elaborate production or spectacular display that is meant to entertain, often in an excessive fashion.</p>
<p class='rw-defn idx14' style='display:none'>Someone who is frugal spends very little money—and even then only on things that are absolutely necessary.</p>
<p class='rw-defn idx15' style='display:none'>If something is inordinate, it is much larger in amount or degree than is normally expected.</p>
<p class='rw-defn idx16' style='display:none'>When you behave with moderation, you live in a balanced and measured way; you do nothing to excess.</p>
<p class='rw-defn idx17' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx18' style='display:none'>Something that is opulent is very impressive, grand, and is made from expensive materials.</p>
<p class='rw-defn idx19' style='display:none'>A parsimonious person is not willing to give or spend money.</p>
<p class='rw-defn idx20' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx21' style='display:none'>Someone who is profligate is lacking in restraint, which can manifest in carelessly and wastefully using resources, such as energy or money; this kind of person can also act in an immoral way.</p>
<p class='rw-defn idx22' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx23' style='display:none'>If something bad, such as crime or disease, is rampant, there is a lot of it—and it is increasing in a way that is difficult to control.</p>
<p class='rw-defn idx24' style='display:none'>You show rectitude if you behave or conduct yourself in an honest and morally correct manner.</p>
<p class='rw-defn idx25' style='display:none'>A reprobate has a bad character and behaves in an immoral or improper way.</p>
<p class='rw-defn idx26' style='display:none'>Someone who is sedulous works hard and performs tasks very carefully and thoroughly, not stopping their work until it is completely accomplished.</p>
<p class='rw-defn idx27' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx28' style='display:none'>Something that is sumptuous is impressive, grand, and very expensive.</p>
<p class='rw-defn idx29' style='display:none'>A sybarite is very fond of expensive and luxurious things; therefore, they are devoted to a life of pleasure.</p>
<p class='rw-defn idx30' style='display:none'>If you show temperance, you limit yourself so that you don’t do too much of something; you act in a controlled and well-balanced way.</p>
<p class='rw-defn idx31' style='display:none'>A feeling that is unbridled is enthusiastic and unlimited in its expression.</p>
<p class='rw-defn idx32' style='display:none'>Someone who is unrestrained is free to do as they please; they are not controlled by anyone but themselves, which can lead to excessive behavior.</p>
<p class='rw-defn idx33' style='display:none'>A wanton action deliberately harms someone or damages something for no apparent or good reason.</p>
<p class='rw-defn idx34' style='display:none'>A wastrel is a lazy person who wastes time and money.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
Middle/High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>prodigal</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='prodigal#' id='pronounce-sound' path='audio/words/amy-prodigal'></a>
PROD-i-guhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='prodigal#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='prodigal#' id='context-sound' path='audio/wordcontexts/brian-prodigal'></a>
The miserable millionaire&#8217;s wife often went on <em>prodigal</em>, reckless, and expensive shopping trips when her impulse buying knew no bounds.  In a <em>prodigal</em> act of even further wastefulness, she would only wear her fancy new clothes once, and then throw them out.  Soon the couple went bankrupt because of her <em>prodigal</em> and excessive lifestyle, and they were forced to each get a job.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What would a <em>prodigal</em> person likely do?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Boss other people around in a way that is really irritating.
</li>
<li class='choice '>
<span class='result'></span>
Help keep their house neatly organized without being told to do so.
</li>
<li class='choice answer '>
<span class='result'></span>
Spend all of their money as soon as they get some with no forethought.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='prodigal#' id='definition-sound' path='audio/wordmeanings/amy-prodigal'></a>
Someone who behaves in a <em>prodigal</em> way spends a lot of money and/or time carelessly and wastefully with no concern for the future.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>wasteful</em>
</span>
</span>
</div>
<a class='quick-help' href='prodigal#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='prodigal#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/prodigal/memory_hooks/6142.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Prod</span></span> That <span class="emp3"><span>Gal</span></span></span></span> "It would be rather <span class="emp1"><span>prod</span></span>i<span class="emp3"><span>gal</span></span> of you to <span class="emp1"><span>prod</span></span> that <span class="emp3"><span>gal</span></span>, don't you think?"
</p>
</div>

<div id='memhook-button-bar'>
<a href="prodigal#" id="add-public-hook" style="" url="https://membean.com/mywords/prodigal/memory_hooks">Use other public hook</a>
<a href="prodigal#" id="memhook-use-own" url="https://membean.com/mywords/prodigal/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='prodigal#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
But [the novel] _Jack_ focuses on, as its title would suggest, the character who has eluded, bedeviled, and grieved all the people who have ever loved him: the <b>prodigal</b> son.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
Like the <b>prodigal</b> son, [Jeff] Francoeur rejoined his hometown team, the team where it all started, shortly before the start of spring training.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
As Dilly lays dying in a Dublin hospital bed, she remembers an earlier life in New York, a lost love and an awkward marriage, and she calls her <b>prodigal</b> daughter home to the family fold.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/prodigal/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='prodigal#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pro_forward' data-tree-url='//cdn1.membean.com/public/data/treexml' href='prodigal#'>
<span class='common'></span>
pro-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>forward, forth</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ig_do' data-tree-url='//cdn1.membean.com/public/data/treexml' href='prodigal#'>
<span class='common'></span>
ig
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>drive, do, act</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='al_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='prodigal#'>
<span class=''></span>
-al
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>When one is <em>prodigal</em>, one &#8220;drives forward&#8221; or &#8220;acts forth&#8221; uncontrollably until one has done too much of something.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='prodigal#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>A Hard Day's Night</strong><span> They are both acting in a prodigal fashion.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/prodigal.jpg' video_url='examplevids/prodigal' video_width='350'></span>
<div id='wt-container'>
<img alt="Prodigal" height="198" src="https://cdn1.membean.com/video/examplevids/prodigal.jpg" width="350" />
<div class='center'>
<a href="prodigal#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='prodigal#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Prodigal" src="https://cdn1.membean.com/public/images/wordimages/cons2/prodigal.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='prodigal#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>copious</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>decadent</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>deficit</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>derelict</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>dissipate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>dissolution</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>exorbitant</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>extravaganza</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>inordinate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>opulent</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>profligate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>rampant</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>reprobate</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>sumptuous</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>sybarite</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>unbridled</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>unrestrained</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>wanton</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>wastrel</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abstemious</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abstinence</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>austere</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>deliberate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>edification</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>frugal</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>moderation</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>parsimonious</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>rectitude</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>sedulous</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>temperance</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="prodigal" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>prodigal</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="prodigal#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

