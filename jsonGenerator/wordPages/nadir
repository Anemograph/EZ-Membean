
<!DOCTYPE html>
<html>
<head>
<title>Word: nadir | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>The word abject emphasizes a very bad situation or quality, thereby making it even worse.</p>
<p class='rw-defn idx1' style='display:none'>A situation or condition that is abysmal is extremely bad or of wretched quality.</p>
<p class='rw-defn idx2' style='display:none'>The acme of something is its highest point of achievement or excellence.</p>
<p class='rw-defn idx3' style='display:none'>The apex of anything, such as an organization or system, is its highest part or most important position.</p>
<p class='rw-defn idx4' style='display:none'>The apogee of something is its highest or greatest point, especially in reference to a culture or career.</p>
<p class='rw-defn idx5' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx6' style='display:none'>When something culminates, it reaches its highest point or climax.</p>
<p class='rw-defn idx7' style='display:none'>Depravity is behavior that is immoral, corrupt, or evil.</p>
<p class='rw-defn idx8' style='display:none'>If you say a person&#8217;s actions are despicable, you think they are extremely unpleasant or nasty.</p>
<p class='rw-defn idx9' style='display:none'>A hapless person is unlucky, so much so that a stream of unfortunate things has happened and continues to happen to them.</p>
<p class='rw-defn idx10' style='display:none'>Someone who has had an illustrious professional career is celebrated and outstanding in their given field of expertise.</p>
<p class='rw-defn idx11' style='display:none'>When you personify something, such as a quality, you are the perfect example of it.</p>
<p class='rw-defn idx12' style='display:none'>If someone reaches a pinnacle of something—such as a career or mountain—they have arrived at the highest point of it.</p>
<p class='rw-defn idx13' style='display:none'>If something plummets, it falls down from a high position very quickly; for example, a piano can plummet from a window and a stock value can plummet during a market crash.</p>
<p class='rw-defn idx14' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>
<p class='rw-defn idx15' style='display:none'>A superlative deed or act is excellent, outstanding, or the best of its kind.</p>
<p class='rw-defn idx16' style='display:none'>An unparalleled accomplishment has not been equaled by anyone or cannot be compared to anything that anyone else has ever done.</p>
<p class='rw-defn idx17' style='display:none'>If you are unsurpassed in what you do, you are the best—period.</p>
<p class='rw-defn idx18' style='display:none'>Something in the vanguard is in the leading spot of something, such as an army or institution.</p>
<p class='rw-defn idx19' style='display:none'>Something vintage is the best of its kind and of excellent quality; it is often of a past time and is considered a classic example of its type.</p>
<p class='rw-defn idx20' style='display:none'>The zenith of something is its highest, most powerful, or most successful point.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>nadir</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='nadir#' id='pronounce-sound' path='audio/words/amy-nadir'></a>
NAY-dir
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='nadir#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='nadir#' id='context-sound' path='audio/wordcontexts/brian-nadir'></a>
My family&#8217;s finances reached their <em>nadir</em> or lowest point when we were forced to sell off all our possessions to pay the rent.  Despite the healthy economy, the bottom or <em>nadir</em> of our family fortune could be traced to bad investments and poor planning.  During this time, my father&#8217;s relationship with creditors reached an all-time low or grim <em>nadir</em>, with one of them even threatening to assault him if he didn&#8217;t pay his debt.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>nadir</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A sudden financial gain.
</li>
<li class='choice '>
<span class='result'></span>
A cause for disagreement.
</li>
<li class='choice answer '>
<span class='result'></span>
The rock bottom of a situation.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='nadir#' id='definition-sound' path='audio/wordmeanings/amy-nadir'></a>
The <em>nadir</em> of a situation is its lowest point.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>lowest point</em>
</span>
</span>
</div>
<a class='quick-help' href='nadir#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='nadir#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/nadir/memory_hooks/5668.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Nader</span></span>'s <span class="emp3"><span>Nadir</span></span></span></span>  When Ralph <span class="emp3"><span>Nader</span></span> ran for office for the fourth time and still had no chance, he had hit his political <span class="emp3"><span>nadir</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="nadir#" id="add-public-hook" style="" url="https://membean.com/mywords/nadir/memory_hooks">Use other public hook</a>
<a href="nadir#" id="memhook-use-own" url="https://membean.com/mywords/nadir/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='nadir#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Military exchanges have been slowly reviving since their <b>nadir</b> of April 2001, when a Chinese fighter jet hit an American spy plane close to China.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
India’s outlook toward foreign firms hit a <b>nadir</b> in 1984 when a Union Carbide plant in Bhopal leaked toxic gas into the city, immediately killing between 3,500 and 8,000 people.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
If that’s even close to accurate, it would represent a historic <b>nadir</b>: The next lowest approval rating on record, 22%, was scored by Harry Truman during the Korean War.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
The <b>nadir</b> for movies came in 1972, when box office admissions fell well below one billion the worst year in the industry’s history.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/nadir/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='nadir#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>From a root word meaning &#8220;opposite the zenith.&#8221;  Since the &#8220;zenith&#8221; is the &#8220;topmost point,&#8221; the <em>nadir</em> would be the &#8220;lowest point.&#8221;</p>
<a href="nadir#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> Arabic</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='nadir#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Nadir" src="https://cdn1.membean.com/public/images/wordimages/cons2/nadir.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='nadir#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abject</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abysmal</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>depravity</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>despicable</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>hapless</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>plummet</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>acme</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>apex</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>apogee</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>culminate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>illustrious</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>personify</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>pinnacle</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>superlative</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>unparalleled</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>unsurpassed</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>vanguard</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>vintage</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>zenith</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="nadir" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>nadir</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="nadir#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

