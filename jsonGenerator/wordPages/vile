
<!DOCTYPE html>
<html>
<head>
<title>Word: vile | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx1' style='display:none'>If you abhor something, you dislike it very much, usually because you think it&#8217;s immoral.</p>
<p class='rw-defn idx2' style='display:none'>If you abominate something, you hate it because you think it is extremely wrong and unacceptable.</p>
<p class='rw-defn idx3' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx4' style='display:none'>If your behavior or manner is altruistic, you show you care more about other people and their interests than you care about yourself.</p>
<p class='rw-defn idx5' style='display:none'>An atrocious deed is outrageously bad, extremely evil, or shocking in its wrongness.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is benevolent wishes others well, often by being kind, filled with goodwill, and charitable towards them.</p>
<p class='rw-defn idx7' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx8' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx9' style='display:none'>A person who is comely is attractive; this adjective is usually used with females, but not always.</p>
<p class='rw-defn idx10' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx11' style='display:none'>A contemptible act is shameful, disgraceful, and worthy of scorn.</p>
<p class='rw-defn idx12' style='display:none'>A decadent person has low moral standards and is more interested in pleasure than serious matters.</p>
<p class='rw-defn idx13' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx14' style='display:none'>A degenerate person is immoral, wicked, or corrupt.</p>
<p class='rw-defn idx15' style='display:none'>Depravity is behavior that is immoral, corrupt, or evil.</p>
<p class='rw-defn idx16' style='display:none'>If you say a person&#8217;s actions are despicable, you think they are extremely unpleasant or nasty.</p>
<p class='rw-defn idx17' style='display:none'>If something is done for someone&#8217;s edification, it is done to benefit that person by teaching them something that improves or enlightens their knowledge or character.</p>
<p class='rw-defn idx18' style='display:none'>If you refer to the enormity of a situation, problem, or event, you mean that it is very evil or morally offensive; this word has nothing to do with physical size.</p>
<p class='rw-defn idx19' style='display:none'>An exemplary person is one that sets an ideal or praiseworthy example for others to follow.</p>
<p class='rw-defn idx20' style='display:none'>An action that is flagrant shows that someone does not care if they obviously break the rules or highly offend people.</p>
<p class='rw-defn idx21' style='display:none'>Something grotesque is so distorted or misshapen that it is disturbing, bizarre, gross, or very ugly.</p>
<p class='rw-defn idx22' style='display:none'>If you describe something as heinous, you mean that is extremely evil, shocking, and bad.</p>
<p class='rw-defn idx23' style='display:none'>An infamous person has a very bad reputation because they have done disgraceful or dishonorable things.</p>
<p class='rw-defn idx24' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx25' style='display:none'>Something loathsome is offensive, disgusting, and brings about intense dislike.</p>
<p class='rw-defn idx26' style='display:none'>Something macabre is so gruesome or frightening that it causes great horror in those who see it.</p>
<p class='rw-defn idx27' style='display:none'>If someone exhibits magnanimity towards another, they show them kindness and noble generosity, especially after defeating them in battle or after having been treated badly by them.</p>
<p class='rw-defn idx28' style='display:none'>A malevolent person or thing is evil due to deliberate attempts to cause harm.</p>
<p class='rw-defn idx29' style='display:none'>Someone who is meritorious is worthy of receiving recognition or is praiseworthy because of what they have accomplished.</p>
<p class='rw-defn idx30' style='display:none'>A miasma is a harmful influence or evil feeling that seems to surround a person or place.</p>
<p class='rw-defn idx31' style='display:none'>A nefarious activity is considered evil and highly dishonest.</p>
<p class='rw-defn idx32' style='display:none'>If something is noisome, it is extremely unpleasant, especially because it is very dirty or has an awful smell.</p>
<p class='rw-defn idx33' style='display:none'>Something that is noxious is harmful, extremely unpleasant, and often poisonous.</p>
<p class='rw-defn idx34' style='display:none'>If you describe people or things as odious, you think that they are extremely unpleasant.</p>
<p class='rw-defn idx35' style='display:none'>If you describe something as palatable, such as an idea or suggestion, you believe that people will accept it; palatable food or drink is agreeable to the taste.</p>
<p class='rw-defn idx36' style='display:none'>Something that is pernicious is very harmful or evil, often in a way that is hidden or not quickly noticed.</p>
<p class='rw-defn idx37' style='display:none'>Philanthropy is unselfish support in the form of donating money, work, or gifts to positive social purposes; philanthropy is also overall love for humans in general.</p>
<p class='rw-defn idx38' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx39' style='display:none'>Someone who is profligate is lacking in restraint, which can manifest in carelessly and wastefully using resources, such as energy or money; this kind of person can also act in an immoral way.</p>
<p class='rw-defn idx40' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx41' style='display:none'>When you possess pulchritude, you are beautiful.</p>
<p class='rw-defn idx42' style='display:none'>You show rectitude if you behave or conduct yourself in an honest and morally correct manner.</p>
<p class='rw-defn idx43' style='display:none'>If you think a type of behavior or idea is reprehensible, you think that it is very bad, morally wrong, and deserves to be strongly criticized.</p>
<p class='rw-defn idx44' style='display:none'>A reprobate has a bad character and behaves in an immoral or improper way.</p>
<p class='rw-defn idx45' style='display:none'>Repugnance is a strong feeling of dislike for something or someone you find horrible and offensive.</p>
<p class='rw-defn idx46' style='display:none'>Something that is repulsive is offensive, highly unpleasant, or just plain disgusting.</p>
<p class='rw-defn idx47' style='display:none'>People or things that are resplendent are beautiful, bright, and impressive in appearance.</p>
<p class='rw-defn idx48' style='display:none'>When you experience revulsion, you feel a great deal of disgust or extreme dislike for something.</p>
<p class='rw-defn idx49' style='display:none'>A scrupulous person takes great care to do everything in an extremely honest, conscientious, and fair manner.</p>
<p class='rw-defn idx50' style='display:none'>An environment or character can be sordid—the former dirty, the latter low or base in an immoral or greedy sort of way.</p>
<p class='rw-defn idx51' style='display:none'>Turpitude is the state of engaging in immoral or evil activities.</p>
<p class='rw-defn idx52' style='display:none'>An action or deed is unconscionable if it is excessively shameful, unfair, or unjust and its effects are more severe than is reasonable or acceptable.</p>
<p class='rw-defn idx53' style='display:none'>If you describe something as unsavory, you mean that it is unpleasant or morally unacceptable.</p>
<p class='rw-defn idx54' style='display:none'>An unsightly person presents an ugly, unattractive, or disagreeable face to the world.</p>
<p class='rw-defn idx55' style='display:none'>A vicious person is corrupt, violent, aggressive, and/or highly immoral.</p>
<p class='rw-defn idx56' style='display:none'>A wanton action deliberately harms someone or damages something for no apparent or good reason.</p>
<p class='rw-defn idx57' style='display:none'>Someone who is winsome is attractive and charming.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>vile</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='vile#' id='pronounce-sound' path='audio/words/amy-vile'></a>
vahyl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='vile#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='vile#' id='context-sound' path='audio/wordcontexts/brian-vile'></a>
We have to get back at Rupert in as <em>vile</em>, disgusting, and nasty a way as possible.  What if we pour some <em>vile</em>, evil-smelling liquid on his hair that cannot be washed out?  Or how about spreading a rumor, beginning with his boss, that is a <em>vile</em> and horrible attempt to ruin his character?  I still can&#8217;t believe that he <em>vilely</em> or wickedly poured gorilla glue all over my iPod, iPad, and computer keyboard!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is <em>vile</em> behavior?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
It is so bad that people find it evil and offensive.
</li>
<li class='choice '>
<span class='result'></span>
It is so unusual that it makes people stare.
</li>
<li class='choice '>
<span class='result'></span>
It is so awkward that it makes people uncomfortable.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='vile#' id='definition-sound' path='audio/wordmeanings/amy-vile'></a>
Something <em>vile</em> is evil, very unpleasant, horrible, or extremely bad.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>evil</em>
</span>
</span>
</div>
<a class='quick-help' href='vile#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='vile#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/vile/memory_hooks/5302.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>E</span></span> to the Front</span></span>  Move the "<span class="emp1"><span>e</span></span>" of "<span class="emp1"><span>vile</span></span>" to the front and you get its synonym, "<span class="emp1"><span>evil</span></span>."
</p>
</div>

<div id='memhook-button-bar'>
<a href="vile#" id="add-public-hook" style="" url="https://membean.com/mywords/vile/memory_hooks">Use other public hook</a>
<a href="vile#" id="memhook-use-own" url="https://membean.com/mywords/vile/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='vile#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
She makes friends with the Young Avengers, particularly Eli Bradley, the Patriot, who's also connected to the Captain America mythos. To spend time with this universe’s version of her brother, Rikki goes back to high school and gets involved in stopping a super-villain trying to manipulate the students as part of some <b>vile</b> experiment.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
I recommend the glass rather than plastic bottles, which store the seltzer better and can go into the dishwasher, as the plastic can't. (Ignore the <b>vile</b>-colored, <b>vile</b>-seeming flavor cartridges that come with the starter pack—they may be more "natural" than other sodas, but seltzer’s what you want.)
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
"The American Legion expects whoever is responsible for this <b>vile</b> act to be brought to justice," said Clarence Hill, the group’s national commander. "While the memorial has been attacked, the fight will continue to ensure that veterans memorials will remain sacrosanct."
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/vile/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='vile#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='vil_mean' data-tree-url='//cdn1.membean.com/public/data/treexml' href='vile#'>
<span class=''></span>
vil
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>mean, base</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='vile#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>A <em>vile</em> person is &#8220;mean&#8221; or &#8220;base&#8221; in his conduct.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='vile#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Muffin Songs</strong><span> Now that would be simply vile to eat!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/vile.jpg' video_url='examplevids/vile' video_width='350'></span>
<div id='wt-container'>
<img alt="Vile" height="288" src="https://cdn1.membean.com/video/examplevids/vile.jpg" width="350" />
<div class='center'>
<a href="vile#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='vile#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Vile" src="https://cdn0.membean.com/public/images/wordimages/cons2/vile.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='vile#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abhor</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abominate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>atrocious</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>contemptible</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>decadent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>degenerate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>depravity</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>despicable</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>enormity</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>flagrant</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>grotesque</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>heinous</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>infamous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>loathsome</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>macabre</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>malevolent</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>miasma</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>nefarious</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>noisome</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>noxious</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>odious</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>pernicious</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>profligate</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>reprehensible</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>reprobate</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>repugnance</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>repulsive</span> &middot;</li><li data-idx='48' class = 'rw-wordform notlearned'><span>revulsion</span> &middot;</li><li data-idx='50' class = 'rw-wordform notlearned'><span>sordid</span> &middot;</li><li data-idx='51' class = 'rw-wordform notlearned'><span>turpitude</span> &middot;</li><li data-idx='52' class = 'rw-wordform notlearned'><span>unconscionable</span> &middot;</li><li data-idx='53' class = 'rw-wordform notlearned'><span>unsavory</span> &middot;</li><li data-idx='54' class = 'rw-wordform notlearned'><span>unsightly</span> &middot;</li><li data-idx='55' class = 'rw-wordform notlearned'><span>vicious</span> &middot;</li><li data-idx='56' class = 'rw-wordform notlearned'><span>wanton</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>altruistic</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>benevolent</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>comely</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>edification</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>exemplary</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>magnanimity</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>meritorious</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>palatable</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>philanthropy</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>pulchritude</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>rectitude</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>resplendent</span> &middot;</li><li data-idx='49' class = 'rw-wordform notlearned'><span>scrupulous</span> &middot;</li><li data-idx='57' class = 'rw-wordform notlearned'><span>winsome</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="vile" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>vile</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="vile#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

