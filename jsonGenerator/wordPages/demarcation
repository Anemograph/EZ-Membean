
<!DOCTYPE html>
<html>
<head>
<title>Word: demarcation | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Demarcation-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/demarcation-large.jpg?qdep8" />
</div>
<a href="demarcation#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When something abuts something else, it is next to it or forms a border with it.</p>
<p class='rw-defn idx1' style='display:none'>If you describe something as ambiguous, you mean that it is unclear or confusing because it can be understood in multiple ways.</p>
<p class='rw-defn idx2' style='display:none'>If you describe someone as articulate, you mean that they are able to express their thoughts, arguments, and ideas clearly and effectively.</p>
<p class='rw-defn idx3' style='display:none'>If one&#8217;s powers or rights are circumscribed, they are limited or restricted.</p>
<p class='rw-defn idx4' style='display:none'>Contiguous things are in contact with or near each other; contiguous events happen one right after the other without a break.</p>
<p class='rw-defn idx5' style='display:none'>If you delineate something, such as an idea or situation or border, you describe it in great detail.</p>
<p class='rw-defn idx6' style='display:none'>If you elucidate something, you make it easier to understand by giving more relevant information.</p>
<p class='rw-defn idx7' style='display:none'>An enclave is a small area within a larger area of a city or country in which people of a different nationality or culture live.</p>
<p class='rw-defn idx8' style='display:none'>When something encompasses something else, it includes all aspects of it or completely surrounds it.</p>
<p class='rw-defn idx9' style='display:none'>To gerrymander a voting district is to change its physical boundaries in order to include more people who vote in a particular way.</p>
<p class='rw-defn idx10' style='display:none'>The juxtaposition of two objects is the act of positioning them side by side so that the differences between them are more readily visible.</p>
<p class='rw-defn idx11' style='display:none'>If you describe something as nebulous, you mean that it is unclear, vague, and not clearly defined; a shape that is nebulous has no clear boundaries.</p>
<p class='rw-defn idx12' style='display:none'>The periphery of a place is its boundary or outer edge.</p>
<p class='rw-defn idx13' style='display:none'>The propinquity of a thing is its nearness in location, relationship, or similarity to another thing.</p>
<p class='rw-defn idx14' style='display:none'>When people or animals are quarantined, they are put in isolation so that they will not spread disease.</p>
<p class='rw-defn idx15' style='display:none'>A rubric is a set of instructions at the beginning of a document, such as an examination or term paper, that is usually printed in a different style so as to highlight its importance.</p>
<p class='rw-defn idx16' style='display:none'>Something statutory, such as the power given to a governor or president, is created, established, and controlled by rules and laws; hence, it has the full force of the law behind it and must be followed.</p>
<p class='rw-defn idx17' style='display:none'>A stricture is a rule or condition that imposes restrictions or limits on what you can do.</p>
<p class='rw-defn idx18' style='display:none'>When you trammel something or someone, you bring about limits to freedom or hinder movements or activities in some fashion.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>demarcation</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='demarcation#' id='pronounce-sound' path='audio/words/amy-demarcation'></a>
dee-mar-KAY-shun
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='demarcation#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='demarcation#' id='context-sound' path='audio/wordcontexts/brian-demarcation'></a>
The <em>demarcations</em> or boundary markings along the trail helped us stay on the path&#8212;finding these trail signs eased our worry about getting lost.  We knew we were on the right trail because the <em>demarcation</em> or separation between being on the trail and not was clearly indicated.  We also felt a distinct <em>demarcation</em> or split between those living in comfort in the city and we who were roughing it in the back country!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>demarcation</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is an argument between two people that is not resolved.
</li>
<li class='choice '>
<span class='result'></span>
It is a marked path that allows a traveler to stay on course.
</li>
<li class='choice answer '>
<span class='result'></span>
It is a clear limit between one thing and another.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='demarcation#' id='definition-sound' path='audio/wordmeanings/amy-demarcation'></a>
<em>Demarcation</em> is the process of setting boundaries or limits; it is also a line that provides a distinct separation between two things.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>boundary</em>
</span>
</span>
</div>
<a class='quick-help' href='demarcation#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='demarcation#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/demarcation/memory_hooks/3393.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Mark</span></span>s a N<span class="emp3"><span>ation</span></span></span></span> De<span class="emp1"><span>marc</span></span><span class="emp3"><span>ation</span></span>s on a map clearly <span class="emp1"><span>mar</span></span>k the boundaries of a n<span class="emp3"><span>ation</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="demarcation#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/demarcation/memory_hooks">Use other hook</a>
<a href="demarcation#" id="memhook-use-own" url="https://membean.com/mywords/demarcation/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='demarcation#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Himalayan Kashmir has long been disputed, with both India and Pakistan claiming the Muslim-majority region. Following multiple wars over Kashmir, an uneasy truce prevails along an unofficial <b>demarcation</b> known as the Line of Control.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
And while working from home might have some benefits, the lack of any clear <b>demarcation</b> between work life and home life can also present challenges . . . “What ends up happening is that your brain has a harder time of understanding where’s the boundary—are we working or are we home? So that burdens us with that extra layer where we need to do more work to define that more clearly,” said Pederson.
<cite class='attribution'>
&mdash;
WTTW News
</cite>
</li>
<li>
Critics say that the fence now being built does not follow the precise line of <b>demarcation</b> which this or that party favors as the border between Israel and Palestine.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
The leaders of Malaysia and Brunei have urged the Joint Land Boundary Technical Committee to expedite works on the <b>demarcation</b> and survey of the land boundary between both countries.
<cite class='attribution'>
&mdash;
New Straits Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/demarcation/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='demarcation#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='de_off' data-tree-url='//cdn1.membean.com/public/data/treexml' href='demarcation#'>
<span class='common'></span>
de-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>off</td>
</tr>
<tr>
<td class='partform'>marc</td>
<td>
&rarr;
</td>
<td class='meaning'>mark</td>
</tr>
<tr>
<td class='partform'>-ation</td>
<td>
&rarr;
</td>
<td class='meaning'>act of doing something</td>
</tr>
</table>
<p><em>Demarcation</em> is the &#8220;act of marking off&#8221; in order to set boundaries or limits.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='demarcation#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>WKRP in Cincinnati</strong><span> A clear office demarcation.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/demarcation.jpg' video_url='examplevids/demarcation' video_width='350'></span>
<div id='wt-container'>
<img alt="Demarcation" height="288" src="https://cdn1.membean.com/video/examplevids/demarcation.jpg" width="350" />
<div class='center'>
<a href="demarcation#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='demarcation#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Demarcation" src="https://cdn2.membean.com/public/images/wordimages/cons2/demarcation.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='demarcation#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abut</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>articulate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>circumscribe</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>contiguous</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>delineate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>elucidate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>enclave</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>encompass</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>gerrymander</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>juxtaposition</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>nebulous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>periphery</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>propinquity</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>quarantine</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>rubric</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>statutory</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>stricture</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>trammel</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>ambiguous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="demarcation" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>demarcation</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="demarcation#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

