
<!DOCTYPE html>
<html>
<head>
<title>Word: decry | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you absolve someone, you publicly and formally say that they are not guilty or responsible for any wrongdoing.</p>
<p class='rw-defn idx1' style='display:none'>If something is anathema to you, such as a cursed object or idea, you very strongly dislike it or even hate it.</p>
<p class='rw-defn idx2' style='display:none'>Approbation is official praise or approval of something.</p>
<p class='rw-defn idx3' style='display:none'>When you arraign someone, you make them come to court and answer criminal charges made against them.</p>
<p class='rw-defn idx4' style='display:none'>Censure is written or verbal disapproval and harsh criticism of something that someone has done.</p>
<p class='rw-defn idx5' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx6' style='display:none'>If you denounce people or actions, you criticize them severely in public because you feel strongly that they are wrong or evil.</p>
<p class='rw-defn idx7' style='display:none'>If you deprecate something, you disapprove of it strongly.</p>
<p class='rw-defn idx8' style='display:none'>If you disparage someone or something, you say unpleasant words that show you have no respect for that person or thing.</p>
<p class='rw-defn idx9' style='display:none'>An encomium strongly praises someone or something via oral or written communication.</p>
<p class='rw-defn idx10' style='display:none'>A eulogy is a speech or other piece of writing, often part of a funeral, in which someone or something is highly praised.</p>
<p class='rw-defn idx11' style='display:none'>When you exalt another person, you either praise them highly or promote them to a higher position in an organization.</p>
<p class='rw-defn idx12' style='display:none'>If you exculpate someone, you prove that they are not guilty of a crime.</p>
<p class='rw-defn idx13' style='display:none'>An exemplary person is one that sets an ideal or praiseworthy example for others to follow.</p>
<p class='rw-defn idx14' style='display:none'>When someone is exonerated from guilt, a task, or a responsibility, they are set free or excused from it.</p>
<p class='rw-defn idx15' style='display:none'>If you extol something or someone, you praise it or them very enthusiastically.</p>
<p class='rw-defn idx16' style='display:none'>If you impugn someone&#8217;s motives or integrity, you say that they do not deserve to be trusted.</p>
<p class='rw-defn idx17' style='display:none'>A laudatory article or speech expresses praise or admiration for someone or something.</p>
<p class='rw-defn idx18' style='display:none'>When someone is lionized, they are treated as being very important or famous—although they may not deserve to be.</p>
<p class='rw-defn idx19' style='display:none'>If you malign someone, you say unpleasant things about them to damage their reputation.</p>
<p class='rw-defn idx20' style='display:none'>If you receive a plaudit, you receive admiration, praise, and approval from someone.</p>
<p class='rw-defn idx21' style='display:none'>A proponent is a supporter or backer of something, such as a cause or other endeavor.</p>
<p class='rw-defn idx22' style='display:none'>If you traduce someone, you deliberately say hurtful and untrue things to damage their reputation.</p>
<p class='rw-defn idx23' style='display:none'>If you vilify people, you write or say bad things about them in an attempt to make them unpopular.</p>
<p class='rw-defn idx24' style='display:none'>If a person is vindicated, their ideas, decisions, or actions—once considered wrong—are proved correct or not to be blamed.</p>
<p class='rw-defn idx25' style='display:none'>Vituperative remarks are full of hate, anger, and cruel criticism.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>decry</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='decry#' id='pronounce-sound' path='audio/words/amy-decry'></a>
di-KRAHY
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='decry#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='decry#' id='context-sound' path='audio/wordcontexts/brian-decry'></a>
Simon condemned, criticized, and <em><em>decried</em></em> the lack of support he experienced at work.  He complained loudly to his fellow employees, <em><em>decrying</em></em> how the directors of their media company did not notice how much effort they all lent to each creative film project.  Simon blamed the directors for their neglect and greatly <em><em>decried</em></em> the executives&#8217; ignorance about the team of artists who worked for them.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When might you <em>decry</em> something?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When you are concerned that it is being misunderstood.
</li>
<li class='choice '>
<span class='result'></span>
When you feel overwhelmed by it and need help doing it.
</li>
<li class='choice answer '>
<span class='result'></span>
When you strongly disapprove and are critical of it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='decry#' id='definition-sound' path='audio/wordmeanings/amy-decry'></a>
To <em>decry</em> something is to speak against it and find fault with it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>criticize</em>
</span>
</span>
</div>
<a class='quick-help' href='decry#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='decry#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/decry/memory_hooks/4675.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>De</span></span>lay? <span class="emp3"><span>Cry</span></span>, <span class="emp3"><span>Cry</span></span>, <span class="emp3"><span>Cry</span></span>!</span></span> When we <span class="emp2"><span>de</span></span><span class="emp3"><span>cry</span></span> against unjust behavior, we must, <span class="emp3"><span>cry</span></span>, <span class="emp3"><span>cry</span></span>, and <span class="emp3"><span>cry</span></span> some more until our voices are heard, or our <span class="emp2"><span>de</span></span><span class="emp3"><span>cry</span></span>ing will just be <span class="emp2"><span>de</span></span>layed by those against whom we <span class="emp2"><span>de</span></span><span class="emp3"><span>cry</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="decry#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/decry/memory_hooks">Use other hook</a>
<a href="decry#" id="memhook-use-own" url="https://membean.com/mywords/decry/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='decry#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Invoking his personal history of experiencing racism while reflecting on the death of George Floyd, Clippers coach Doc Rivers <b>decried</b> the “senseless acts of racial injustice that continue to plague our country” in a statement issued Sunday by the team.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
Speaking to USA Today, Shurtleff <b>decried</b> "serious antitrust violations that are harming taxpayer-funded institutions to the tune of hundreds of millions of dollars."
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
The University of Pittsburgh is moving away from investing in fossil fuels with the goal of phasing such holdings out completely by 2035. . . . Still, some student climate activists are unhappy with the school’s slow pace of divestment, with one of the larger groups, Fossil Free Pitt, <b>decrying</b> the report as “feeble” and hanging a banner proclaiming, “The climate crisis won’t wait for 2035.”
<cite class='attribution'>
&mdash;
Pittsburg Post-Gazette
</cite>
</li>
<li>
It is forbidden to <b>decry</b> other sects; the true believer gives honor to whatever in them is worthy of honor.
<cite class='attribution'>
&mdash;
Ashoka, Indian emperor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/decry/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='decry#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='de_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='decry#'>
<span class='common'></span>
de-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='cry_complain' data-tree-url='//cdn1.membean.com/public/data/treexml' href='decry#'>
<span class=''></span>
cry
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>shout out in complaint</td>
</tr>
</table>
<p>To <em>decry</em> is to &#8220;thoroughly shout out in complaint&#8221; against something or someone.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='decry#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Network</strong><span> This man is decrying the watching of television.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/decry.jpg' video_url='examplevids/decry' video_width='350'></span>
<div id='wt-container'>
<img alt="Decry" height="288" src="https://cdn1.membean.com/video/examplevids/decry.jpg" width="350" />
<div class='center'>
<a href="decry#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='decry#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Decry" src="https://cdn3.membean.com/public/images/wordimages/cons2/decry.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='decry#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>anathema</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>arraign</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>censure</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>denounce</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>deprecate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>disparage</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>impugn</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>malign</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>traduce</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>vilify</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>vituperative</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>absolve</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>approbation</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>encomium</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>eulogy</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>exalt</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>exculpate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>exemplary</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>exonerate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>extol</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>laudatory</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>lionize</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>plaudit</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>proponent</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>vindicate</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="decry" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>decry</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="decry#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

