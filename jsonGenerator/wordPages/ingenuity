
<!DOCTYPE html>
<html>
<head>
<title>Word: ingenuity | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Acuity is sharpness or clearness of vision, hearing, or thought.</p>
<p class='rw-defn idx1' style='display:none'>Acumen is the ability to make quick decisions and keen, precise judgments.</p>
<p class='rw-defn idx2' style='display:none'>When you are adept at something, you are very good, skillful, or talented at it.</p>
<p class='rw-defn idx3' style='display:none'>Someone who is adroit is skillful and clever, especially in thought, behavior, or action.</p>
<p class='rw-defn idx4' style='display:none'>If you do something with alacrity, you do it eagerly and quickly.</p>
<p class='rw-defn idx5' style='display:none'>When you describe someone as astute, you think they quickly understand situations or behavior and use it to their advantage.</p>
<p class='rw-defn idx6' style='display:none'>If someone is competent in a job, they are able and skilled enough to do it well.</p>
<p class='rw-defn idx7' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx8' style='display:none'>A deft movement or action is made quickly and with skill.</p>
<p class='rw-defn idx9' style='display:none'>When you discern something, you notice, detect, or understand it, often after thinking about it carefully or studying it for some time.  </p>
<p class='rw-defn idx10' style='display:none'>Efficacy is the ability or power to produce an expected effect or result.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx12' style='display:none'>If someone exhibits finesse in something, they do it with great skill and care; this most often refers to handling difficult situations that might easily offend people.</p>
<p class='rw-defn idx13' style='display:none'>A fruitless effort at doing something does not bring about a successful result.</p>
<p class='rw-defn idx14' style='display:none'>A futile attempt at doing something is hopeless and pointless because it simply cannot be accomplished.</p>
<p class='rw-defn idx15' style='display:none'>A gauche person behaves in an awkward and unsuitable way in public because they lack social skills.</p>
<p class='rw-defn idx16' style='display:none'>If an idea or thought is incisive, it is expressed in a penetrating and knowledgeable manner that is clear and brief; additionally, it can demonstrate impressive understanding of related ideas or thoughts.</p>
<p class='rw-defn idx17' style='display:none'>Something that is inconceivable cannot be imagined or thought of; that is, it is beyond reason or unbelievable.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is ineffectual at a task is useless or ineffective when attempting to do it.</p>
<p class='rw-defn idx19' style='display:none'>Someone who is inept is unable or unsuitable to do a job; hence, they are unfit for it.</p>
<p class='rw-defn idx20' style='display:none'>A myopic person is unable to visualize the long-term negative consequences of their current actions; rather, they are narrowly focused upon short-term results.</p>
<p class='rw-defn idx21' style='display:none'>Someone is obtuse when they are slow to understand things; someone can choose to be obtuse when they deliberately do not try to figure things out.</p>
<p class='rw-defn idx22' style='display:none'>If you do something with panache, you do it in a way that shows great skill, style, and confidence.</p>
<p class='rw-defn idx23' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx24' style='display:none'>To be politic in a decision is to be socially wise and diplomatic.</p>
<p class='rw-defn idx25' style='display:none'>A precocious child shows advanced intelligence or skill at an unusually young age.</p>
<p class='rw-defn idx26' style='display:none'>When you resolve a problem, you solve it or come to a decision about it.</p>
<p class='rw-defn idx27' style='display:none'>A sagacious person is wise, intelligent, and has the ability to make good practical decisions.</p>
<p class='rw-defn idx28' style='display:none'>If you behave in an urbane way, you are behaving in a polite, refined, and civilized fashion in social situations.</p>
<p class='rw-defn idx29' style='display:none'>Something that is vacuous is empty or blank, such as a mind or stare.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>ingenuity</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='ingenuity#' id='pronounce-sound' path='audio/words/amy-ingenuity'></a>
in-juh-NOO-i-tee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='ingenuity#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='ingenuity#' id='context-sound' path='audio/wordcontexts/brian-ingenuity'></a>
The Olympian&#8217;s <em>ingenuity</em> or creativity made her bobsled the best in the competition.  By using her <em>ingenuity</em> or clever intelligence, she had found a way to mold the material of the runners to be more streamlined.  Her <em>ingenuity</em> or engineering talent also enabled her to create a specialized wax that decreased resistance by 9%.  Her intellectual <em>ingenuity</em> combined with her superior athleticism made her an unbeatable foe, and won her the gold medal.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What would someone with <em>ingenuity</em> do?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Fix something using intelligence and imagination.
</li>
<li class='choice '>
<span class='result'></span>
Win a game by breaking the rules and cheating.
</li>
<li class='choice '>
<span class='result'></span>
Make a decision without thinking it through first.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='ingenuity#' id='definition-sound' path='audio/wordmeanings/amy-ingenuity'></a>
<em>Ingenuity</em> in solving a problem uses creativity, intelligence, and cleverness.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>creativity</em>
</span>
</span>
</div>
<a class='quick-help' href='ingenuity#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='ingenuity#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/ingenuity/memory_hooks/5375.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Geniu</span></span>s Abil<span class="emp3"><span>ity</span></span> <span class="emp1"><span>in</span></span> Solving</span></span> Someone's <span class="emp1"><span>in</span></span><span class="emp2"><span>genu</span></span><span class="emp3"><span>ity</span></span> shows forth when he shows his <span class="emp2"><span>geniu</span></span>s or creative abil<span class="emp3"><span>ity</span></span> <span class="emp1"><span>in</span></span> solving a difficult problem.
</p>
</div>

<div id='memhook-button-bar'>
<a href="ingenuity#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/ingenuity/memory_hooks">Use other hook</a>
<a href="ingenuity#" id="memhook-use-own" url="https://membean.com/mywords/ingenuity/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='ingenuity#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Never tell people how to do things. Tell them what to do and they will surprise you with their <b>ingenuity</b>.
<span class='attribution'>&mdash; General George S. Patton, U.S. Army General</span>
<img alt="General george s" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/General George S. Patton, U.S. Army General.jpg?qdep8" width="80" />
</li>
<li>
The mind is intricate and complex, human <b>ingenuity</b> is astonishing, but the creation itself is of course simple. The charm of machines is precisely the fact that every intelligent, dextrous man can create a machine.
<cite class='attribution'>
&mdash;
The New Yorker
</cite>
</li>
<li>
The goal of cap and trade, therefore—"to align the incentives better, so human <b>ingenuity</b> can be harnessed to fight global warming"—makes sense.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/ingenuity/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='ingenuity#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ingenu_honest' data-tree-url='//cdn1.membean.com/public/data/treexml' href='ingenuity#'>
<span class=''></span>
ingenu
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>honest, inherent talent, inventive skill</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ity_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='ingenuity#'>
<span class=''></span>
-ity
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state or quality</td>
</tr>
</table>
<p>A person who possesses <em>ingenuity</em> has the &#8220;state or quality of inherent talent or inventive skill;&#8221; this word has been heavily influenced by &#8220;ingenious.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='ingenuity#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>The Martian</strong><span> It would take a lot of ingenuity to grow food on Mars all by oneself.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/ingenuity.jpg' video_url='examplevids/ingenuity' video_width='350'></span>
<div id='wt-container'>
<img alt="Ingenuity" height="288" src="https://cdn1.membean.com/video/examplevids/ingenuity.jpg" width="350" />
<div class='center'>
<a href="ingenuity#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='ingenuity#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Ingenuity" src="https://cdn1.membean.com/public/images/wordimages/cons2/ingenuity.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='ingenuity#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>acuity</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>acumen</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>adept</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>adroit</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>alacrity</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>astute</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>competent</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>deft</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>discern</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>efficacy</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>finesse</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>incisive</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>panache</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>politic</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>precocious</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>resolve</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>sagacious</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>urbane</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='11' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>fruitless</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>futile</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>gauche</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>inconceivable</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>ineffectual</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>inept</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>myopic</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>obtuse</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>vacuous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='ingenuity#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
ingenious
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>clever; original in thought</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="ingenuity" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>ingenuity</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="ingenuity#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

