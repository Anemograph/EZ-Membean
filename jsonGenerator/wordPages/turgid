
<!DOCTYPE html>
<html>
<head>
<title>Word: turgid | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abridge a book or play, you make it shorter by making cuts to the original.</p>
<p class='rw-defn idx1' style='display:none'>Something austere is simple and plain in its style; an austere person is strict and severe with themselves.</p>
<p class='rw-defn idx2' style='display:none'>When a person is speaking or writing in a bombastic fashion, they are very self-centered, extremely showy, and excessively proud.</p>
<p class='rw-defn idx3' style='display:none'>If a part of your body distends, it becomes swollen and unnaturally large.</p>
<p class='rw-defn idx4' style='display:none'>A person or animal that is emaciated is extremely thin because of a serious illness or lack of food.</p>
<p class='rw-defn idx5' style='display:none'>If someone or something is flamboyant, the former is trying to show off in a way that deliberately attracts attention, and the latter is brightly colored and highly decorated.</p>
<p class='rw-defn idx6' style='display:none'>Something florid has too much decoration or is too elaborate.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is frugal spends very little money—and even then only on things that are absolutely necessary.</p>
<p class='rw-defn idx8' style='display:none'>Praise, an apology, or gratitude is fulsome if it is so exaggerated and elaborate that it does not seem sincere.</p>
<p class='rw-defn idx9' style='display:none'>A gaunt person is extremely thin, often due to an extended illness or a lack of food over a long period of time.</p>
<p class='rw-defn idx10' style='display:none'>Grandiloquent speech is highly formal, exaggerated, and often seems both silly and hollow because it is expressly used to appear impressive and important.</p>
<p class='rw-defn idx11' style='display:none'>If something is inordinate, it is much larger in amount or degree than is normally expected.</p>
<p class='rw-defn idx12' style='display:none'>If you describe a situation or process as labyrinthine, you mean that it is very complicated, involved, and difficult to understand.</p>
<p class='rw-defn idx13' style='display:none'>A person who is being laconic uses very few words to say something.</p>
<p class='rw-defn idx14' style='display:none'>An ornate object is heavily or excessively decorated with complicated shapes and patterns.</p>
<p class='rw-defn idx15' style='display:none'>If you describe an action as ostentatious, you think it is an extreme and exaggerated way of impressing people.</p>
<p class='rw-defn idx16' style='display:none'>To pare something down is to reduce or lessen it.</p>
<p class='rw-defn idx17' style='display:none'>A parsimonious person is not willing to give or spend money.</p>
<p class='rw-defn idx18' style='display:none'>Ponderous writing or speech is boring, highly serious, and seems very long and wordy; it definitely lacks both grace and style.</p>
<p class='rw-defn idx19' style='display:none'>When someone pontificates, they give their opinions in a heavy-handed way that shows they think they are always right.</p>
<p class='rw-defn idx20' style='display:none'>Prattle is mindless chatter that seems like it will never stop.</p>
<p class='rw-defn idx21' style='display:none'>Someone who behaves in a prodigal way spends a lot of money and/or time carelessly and wastefully with no concern for the future.</p>
<p class='rw-defn idx22' style='display:none'>To ramble is to wander about leisurely, with no specific destination in mind; to ramble while speaking is to talk with no particular aim or point intended.</p>
<p class='rw-defn idx23' style='display:none'>To rant is to go on an angry verbal attack.</p>
<p class='rw-defn idx24' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx25' style='display:none'>Something that is succinct is clearly and briefly explained without using any unnecessary words.</p>
<p class='rw-defn idx26' style='display:none'>To be terse in speech is to be short and to the point, often in an abrupt manner that may seem unfriendly.</p>
<p class='rw-defn idx27' style='display:none'>If you truncate something, you make it shorter or quicker by removing or cutting off part of it.</p>
<p class='rw-defn idx28' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx29' style='display:none'>A feeling that is unbridled is enthusiastic and unlimited in its expression.</p>
<p class='rw-defn idx30' style='display:none'>Something that is vaunted, such as someone&#8217;s ability, is too highly praised and boasted about.</p>
<p class='rw-defn idx31' style='display:none'>Something that is verbose, such as a speech or article, contains too many words.</p>
<p class='rw-defn idx32' style='display:none'>A voluble person can speak easily with speech that naturally flows; voluble people also tend to be quite chatty.</p>
<p class='rw-defn idx33' style='display:none'>A wizened person is very old, shrunken with age, and has a lot of wrinkles on their skin.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>turgid</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='turgid#' id='pronounce-sound' path='audio/words/amy-turgid'></a>
TUR-jid
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='turgid#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='turgid#' id='context-sound' path='audio/wordcontexts/brian-turgid'></a>
Professor Will Rambles gave some of the most confusing, overly complicated, and <em>turgid</em> lectures ever heard at the university, which bored some students to tears.  The professor&#8217;s writing style was no different: his excessive use of difficult words made his prose needlessly <em>turgid</em>, puzzling generations of students.  Clarifying the meaning beneath the <em>turgid</em>, showy, complex prose became a rite of passage for young, earnest scholars.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When are one&#8217;s words considered <em>turgid</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When they have existed in spoken or written form for a long time.
</li>
<li class='choice '>
<span class='result'></span>
When they insult others to an excessive and angry degree.
</li>
<li class='choice answer '>
<span class='result'></span>
When they are unduly hard to comprehend and therefore tedious.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='turgid#' id='definition-sound' path='audio/wordmeanings/amy-turgid'></a>
<em>Turgid</em> writing or speech is excessively complicated, being filled with too many needlessly difficult words; consequently, such verbiage is boring and difficult to understand.
</li>
<li class='def-text'>
Something <em>turgid</em> can also be swollen or overflowing, such as a <em>turgid</em> stream.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>overcomplicated</em>
</span>
</span>
</div>
<a class='quick-help' href='turgid#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='turgid#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/turgid/memory_hooks/4218.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Too</span></span> <span class="emp2"><span>Rigid</span></span></span></span> <span class="emp1"><span>Tu</span></span><span class="emp2"><span>rgid</span></span> writing is <span class="emp1"><span>too</span></span> <span class="emp2"><span>rigid</span></span>, bursting with complexity and too many words.  Authors should loosen up and use a more relaxed style for more to enjoy.
</p>
</div>

<div id='memhook-button-bar'>
<a href="turgid#" id="add-public-hook" style="" url="https://membean.com/mywords/turgid/memory_hooks">Use other public hook</a>
<a href="turgid#" id="memhook-use-own" url="https://membean.com/mywords/turgid/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='turgid#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
"Read me what you've done," I said. "He read, and it was wondrous bad, and he paused at all the specially <b>turgid</b> sentences, expecting a little approval; for he was proud of those sentences, as I knew he would be. "It needs compression," I suggested, cautiously.
<span class='attribution'>&mdash; Rudyard Kipling, English writer, born in India</span>
<img alt="Rudyard kipling, english writer, born in india" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Rudyard Kipling, English writer, born in India.jpg?qdep8" width="80" />
</li>
<li>
This <b>turgid</b> style, intended to augment the reader’s concern and sympathy, is so excessive that after 458 pages it has done the opposite.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
_Eating the Sun_ is surely not a <b>turgid</b> scientific textbook; parts of it have the feel of an adventure story.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
_The Artist_ is solidly middlebrow—entertaining, yes; well-made, certainly; but not the equal of the films it imitates. On the other hand, it’s not a <b>turgid</b> “masterpiece,” not an endless, portentous epic about the plight of mankind. It’s approachable, fun, undemanding, like a lot of mainstream movies from the silent era.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/turgid/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='turgid#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='turg_swollen' data-tree-url='//cdn1.membean.com/public/data/treexml' href='turgid#'>
<span class=''></span>
turg
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>be swollen</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='id_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='turgid#'>
<span class=''></span>
-id
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>Something <em>turgid</em> &#8220;is swollen&#8221; because it has too much of something in it, such as prose with too many words, or a river with too much water.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='turgid#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Yes Minister</strong><span> A turgid but nevertheless correct description of what the Greek preposition and English prefix "meta-" means.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/turgid.jpg' video_url='examplevids/turgid' video_width='350'></span>
<div id='wt-container'>
<img alt="Turgid" height="288" src="https://cdn1.membean.com/video/examplevids/turgid.jpg" width="350" />
<div class='center'>
<a href="turgid#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='turgid#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Turgid" src="https://cdn2.membean.com/public/images/wordimages/cons2/turgid.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='turgid#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>bombastic</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>distend</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>flamboyant</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>florid</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>fulsome</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>grandiloquent</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>inordinate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>labyrinthine</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>ornate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>ostentatious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>ponderous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>pontificate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>prattle</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>prodigal</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>ramble</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>rant</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>unbridled</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>vaunted</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>verbose</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>voluble</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abridge</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>austere</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>emaciated</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>frugal</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>gaunt</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>laconic</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>pare</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>parsimonious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>succinct</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>terse</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>truncate</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>wizened</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="turgid" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>turgid</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="turgid#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

