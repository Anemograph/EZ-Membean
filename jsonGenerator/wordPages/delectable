
<!DOCTYPE html>
<html>
<head>
<title>Word: delectable | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you take an acerbic tone with someone, you are criticizing them in a clever but critical and mean way.</p>
<p class='rw-defn idx1' style='display:none'>An acrid smell or taste is strong, unpleasant, and stings your nose or throat.</p>
<p class='rw-defn idx2' style='display:none'>Something that is apposite is relevant or suitable to what is happening or being discussed.</p>
<p class='rw-defn idx3' style='display:none'>When something is apropos, it is fitting to the moment or occasion.</p>
<p class='rw-defn idx4' style='display:none'>Something brackish is distasteful and unpleasant usually because something else is mixed in with it; brackish water, for instance, is a mixture of fresh and salt water and cannot be drunk.</p>
<p class='rw-defn idx5' style='display:none'>A comestible is something that can be eaten.</p>
<p class='rw-defn idx6' style='display:none'>A commodious room or house is large and roomy, which makes it convenient and highly suitable for living.</p>
<p class='rw-defn idx7' style='display:none'>A condign reward or punishment is deserved by and appropriate or fitting for the person who receives it.</p>
<p class='rw-defn idx8' style='display:none'>A dulcet sound is soft and pleasant in a gentle way.</p>
<p class='rw-defn idx9' style='display:none'>Something insipid is dull, boring, and has no interesting features; for example, insipid food has no taste or little flavor.</p>
<p class='rw-defn idx10' style='display:none'>If you describe ideas as jejune, you are saying they are childish and uninteresting; the adjective jejune also describes those having little experience, maturity, or knowledge.</p>
<p class='rw-defn idx11' style='display:none'>If something is noisome, it is extremely unpleasant, especially because it is very dirty or has an awful smell.</p>
<p class='rw-defn idx12' style='display:none'>Something that is noxious is harmful, extremely unpleasant, and often poisonous.</p>
<p class='rw-defn idx13' style='display:none'>If you describe people or things as odious, you think that they are extremely unpleasant.</p>
<p class='rw-defn idx14' style='display:none'>If you describe something as palatable, such as an idea or suggestion, you believe that people will accept it; palatable food or drink is agreeable to the taste.</p>
<p class='rw-defn idx15' style='display:none'>Something that is piquant is interesting and exciting; food that is piquant is pleasantly spicy.</p>
<p class='rw-defn idx16' style='display:none'>Potable water is clean and safe to drink.</p>
<p class='rw-defn idx17' style='display:none'>Something pungent, such as a spice, aroma, or speech, is sharp and penetrating.</p>
<p class='rw-defn idx18' style='display:none'>A putrid substance is decaying or rotting; therefore, it is also foul and stinking.</p>
<p class='rw-defn idx19' style='display:none'>If something is redolent of something else, it has features that make you think of it; this word also refers to a particular odor or scent that can be pleasantly strong.</p>
<p class='rw-defn idx20' style='display:none'>Repugnance is a strong feeling of dislike for something or someone you find horrible and offensive.</p>
<p class='rw-defn idx21' style='display:none'>A salubrious place or area is pleasant, clean, healthy, and comfortable to live in.</p>
<p class='rw-defn idx22' style='display:none'>A salutary experience is beneficial to you since it strengthens you in some way, although it may be unpleasant as you undergo it; this word also refers to promoting good health.</p>
<p class='rw-defn idx23' style='display:none'>A succulent food, such as sweet fruit or a good tomato, is juicy and tasty.</p>
<p class='rw-defn idx24' style='display:none'>Something that is tantalizing is desirable and exciting; nevertheless, it may be out of reach, perhaps due to being too expensive.</p>
<p class='rw-defn idx25' style='display:none'>Something is toothsome when it is tasty, attractive, or pleasing in some way; this adjective can apply to food, people, or objects.</p>
<p class='rw-defn idx26' style='display:none'>If you describe something as unsavory, you mean that it is unpleasant or morally unacceptable.</p>
<p class='rw-defn idx27' style='display:none'>Something vapid is dull, boring, and/or tiresome.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>delectable</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='delectable#' id='pronounce-sound' path='audio/words/amy-delectable'></a>
di-LEK-tuh-buhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='delectable#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='delectable#' id='context-sound' path='audio/wordcontexts/brian-delectable'></a>
As the mother of the bride entered the reception hall, she was delighted by the <em>delectable</em> and pleasing wedding cake awaiting the guests.  She gazed in awe and appreciation at the inviting design and <em>delectable</em> decorations along its edges&#8212;every sugary rose and luscious lily were perfectly in place!  When the bride and groom cut the cake, all agreed that it was as enjoyably <em>delectable</em> to the tongue as it had appeared to the eye!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is something that can be described as <em>delectable</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A poem that has multiple layers of meaning.
</li>
<li class='choice '>
<span class='result'></span>
A work of art that is worth a great deal of money.
</li>
<li class='choice answer '>
<span class='result'></span>
Food or drink that is tasty and appealing.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='delectable#' id='definition-sound' path='audio/wordmeanings/amy-delectable'></a>
If you describe something, especially food and drink, as <em>delectable</em>, you mean that it is very pleasant, tasty, or attractive.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>delicious</em>
</span>
</span>
</div>
<a class='quick-help' href='delectable#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='delectable#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/delectable/memory_hooks/5809.json'></span>
<p>
<img alt="Delectable" src="https://cdn3.membean.com/public/images/wordimages/hook/delectable.jpg?qdep8" />
<span class="emp0"><span><span class="emp2"><span>Del</span></span>ightful <span class="emp3"><span>Table</span></span></span></span> All the food is so <span class="emp2"><span>del</span></span>ightful on this <span class="emp2"><span>del</span></span>ec<span class="emp3"><span>table</span></span> <span class="emp3"><span>table</span></span>!
</p>
</div>

<div id='memhook-button-bar'>
<a href="delectable#" id="add-public-hook" style="" url="https://membean.com/mywords/delectable/memory_hooks">Use other public hook</a>
<a href="delectable#" id="memhook-use-own" url="https://membean.com/mywords/delectable/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='delectable#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Lovingly written by Tartine chef/owners Elisabeth M. Prueitt and Chad Robertson, the new cookbook shares the secrets of many of their most <b>delectable</b> treats. Just mention the name to anyone who’s been there, and each is sure to have their absolute favorite.
<cite class='attribution'>
&mdash;
East Bay Times
</cite>
</li>
<li>
In honor of the sandwich and its universal appeal, I submit to you a <b>delectable</b> recipe for banh mi . . . The appeal of banh mi lies in a perfect balance of spicy, salty, sweet and piquant flavors matched by a satisfying blend of textures—crusty tender bread, sprigs of leafy herbs, sharp pickles and a creamy chile-spiked mayo sauce.
<cite class='attribution'>
&mdash;
Chicago Sun-Times
</cite>
</li>
<li>
Holi is celebrated each year with zeal and enthusiasm in the month of March by followers of the Hindu religion. Those who celebrate this festival, wait for it every year eagerly to play with colors and have <b>delectable</b> dishes.
<cite class='attribution'>
&mdash;
India Today
</cite>
</li>
<li>
When it comes to unique and <b>delectable</b> comfort food, look no further than House of Chimney Cakes. . . . The cone-shaped sweet bread is filled with ice cream and elaborately decorated with toppings such as Oreo, Fruity Pebbles, fresh fruits and other deliciousness.
<cite class='attribution'>
&mdash;
WABC-TV, Anaheim
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/delectable/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='delectable#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>delect</td>
<td>
&rarr;
</td>
<td class='meaning'>please, delight</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='able_handy' data-tree-url='//cdn1.membean.com/public/data/treexml' href='delectable#'>
<span class=''></span>
-able
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>handy, capable of</td>
</tr>
</table>
<p>Food that is <em>delectable</em> is &#8220;capable of pleasing or delighting&#8221; those who eat it.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='delectable#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Hidden Valley Ranch</strong><span> How to make even vegetables delectable.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/delectable.jpg' video_url='examplevids/delectable' video_width='350'></span>
<div id='wt-container'>
<img alt="Delectable" height="198" src="https://cdn1.membean.com/video/examplevids/delectable.jpg" width="350" />
<div class='center'>
<a href="delectable#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='delectable#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Delectable" src="https://cdn1.membean.com/public/images/wordimages/cons2/delectable.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='delectable#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>apposite</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>apropos</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>comestible</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>commodious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>condign</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>dulcet</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>palatable</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>piquant</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>potable</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>pungent</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>redolent</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>salubrious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>salutary</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>succulent</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>tantalize</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>toothsome</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>acerbic</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>acrid</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>brackish</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>insipid</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>jejune</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>noisome</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>noxious</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>odious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>putrid</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>repugnance</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>unsavory</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>vapid</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="delectable" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>delectable</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="delectable#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

