
<!DOCTYPE html>
<html>
<head>
<title>Word: vacuous | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>You can describe something as abstruse if you find it highly complicated and difficult to understand.</p>
<p class='rw-defn idx1' style='display:none'>Acumen is the ability to make quick decisions and keen, precise judgments.</p>
<p class='rw-defn idx2' style='display:none'>Something that is amorphous has no clear shape, boundaries, or structure.</p>
<p class='rw-defn idx3' style='display:none'>Something that is arcane is mysterious and secret, known and understood by only a few people.</p>
<p class='rw-defn idx4' style='display:none'>Someone who is callow is young, immature, and inexperienced; consequently, they possess little knowledge of the world.</p>
<p class='rw-defn idx5' style='display:none'>A connoisseur is an appreciative, informed, and knowledgeable judge of the fine arts; they can also just have excellent or perceptive taste in the fine arts without being an expert.</p>
<p class='rw-defn idx6' style='display:none'>The crux of a problem or argument is the most important or difficult part of it that affects everything else.</p>
<p class='rw-defn idx7' style='display:none'>When there is a dearth of something, there is a scarcity or lack of it.</p>
<p class='rw-defn idx8' style='display:none'>A demented person is not in their right mind; hence, they are crazy, insane, and highly irrational.</p>
<p class='rw-defn idx9' style='display:none'>To denude an area is to remove the plants and trees that cover it; it can also mean to make something bare.</p>
<p class='rw-defn idx10' style='display:none'>When you discern something, you notice, detect, or understand it, often after thinking about it carefully or studying it for some time.  </p>
<p class='rw-defn idx11' style='display:none'>Someone who is erudite is steeped in knowledge because they have read and studied extensively.</p>
<p class='rw-defn idx12' style='display:none'>If you fathom something complicated or mysterious, you are able to understand it.</p>
<p class='rw-defn idx13' style='display:none'>If you think something, such as a statement or idea, is fatuous, you consider it stupid or extremely silly; a fatuous hope is unrealistic.</p>
<p class='rw-defn idx14' style='display:none'>If you describe someone&#8217;s behavior as inane, you think it is completely stupid or without much meaning.</p>
<p class='rw-defn idx15' style='display:none'>If an idea or thought is incisive, it is expressed in a penetrating and knowledgeable manner that is clear and brief; additionally, it can demonstrate impressive understanding of related ideas or thoughts.</p>
<p class='rw-defn idx16' style='display:none'>Something insipid is dull, boring, and has no interesting features; for example, insipid food has no taste or little flavor.</p>
<p class='rw-defn idx17' style='display:none'>The intelligentsia of a society are those individuals who are the most highly educated.</p>
<p class='rw-defn idx18' style='display:none'>If you describe ideas as jejune, you are saying they are childish and uninteresting; the adjective jejune also describes those having little experience, maturity, or knowledge.</p>
<p class='rw-defn idx19' style='display:none'>A lacuna is an empty space or gap where something is missing; if there is a lacuna in a person&#8217;s argument, for example, part of that argument is lacking.</p>
<p class='rw-defn idx20' style='display:none'>If you describe something as nebulous, you mean that it is unclear, vague, and not clearly defined; a shape that is nebulous has no clear boundaries.</p>
<p class='rw-defn idx21' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx22' style='display:none'>Someone is obtuse when they are slow to understand things; someone can choose to be obtuse when they deliberately do not try to figure things out.</p>
<p class='rw-defn idx23' style='display:none'>Something is opaque if it is either difficult to understand or is not transparent.</p>
<p class='rw-defn idx24' style='display:none'>A paucity of something is not enough of it.</p>
<p class='rw-defn idx25' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx26' style='display:none'>A precocious child shows advanced intelligence or skill at an unusually young age.</p>
<p class='rw-defn idx27' style='display:none'>When someone exhibits profundity, they display great intellectual depth and understanding; profundity can also be the depth or complexity of something.</p>
<p class='rw-defn idx28' style='display:none'>Recondite areas of knowledge are those that are very difficult to understand and/or are not known by many people.</p>
<p class='rw-defn idx29' style='display:none'>A sagacious person is wise, intelligent, and has the ability to make good practical decisions.</p>
<p class='rw-defn idx30' style='display:none'>If you are sapient, you are wise or very learned.</p>
<p class='rw-defn idx31' style='display:none'>A savant is a person who knows a lot about a subject, either via a lifetime of learning or by considerable natural ability.</p>
<p class='rw-defn idx32' style='display:none'>To be tentative is to be hesitant or uncertain about something; an agreement or decision of this kind is likely to have changes before it reaches its final form.</p>
<p class='rw-defn idx33' style='display:none'>Something vapid is dull, boring, and/or tiresome.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>vacuous</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='vacuous#' id='pronounce-sound' path='audio/words/amy-vacuous'></a>
VAK-yoo-uhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='vacuous#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='vacuous#' id='context-sound' path='audio/wordcontexts/brian-vacuous'></a>
Myron&#8217;s blank and <em>vacuous</em> stare made me wonder if he had understood a word I had said.  When I spoke again, Myron&#8217;s <em>vacuous</em> or inattentive behavior was even more apparent, and I realized the topic of basket weaving was either completely boring or way over his head.  Perhaps my enthusiasm for the subject made him wonder if I had a <em>vacuous</em> or empty mind, as he was more interested in highly complex topics, like robotics and nanotechnology.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>vacuous</em> smile?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
One that appears meaningless and empty.
</li>
<li class='choice '>
<span class='result'></span>
One that appears worried and anxious.
</li>
<li class='choice '>
<span class='result'></span>
One that appears evil and threatening.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='vacuous#' id='definition-sound' path='audio/wordmeanings/amy-vacuous'></a>
Something that is <em>vacuous</em> is empty or blank, such as a mind or stare.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>empty</em>
</span>
</span>
</div>
<a class='quick-help' href='vacuous#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='vacuous#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/vacuous/memory_hooks/5461.json'></span>
<p>
<span class="emp0"><span>Brain <span class="emp2"><span>Vac</span></span> Finds M<span class="emp3"><span>u</span></span>c<span class="emp3"><span>ous</span></span></span></span>  A long time ago Mama Cowey was concerned about Baby Cowey's long and <span class="emp2"><span>vac</span></span><span class="emp3"><span>uous</span></span> stares, so she took a brain <span class="emp2"><span>vac</span></span> and sucked m<span class="emp3"><span>u</span></span>c<span class="emp3"><span>ous</span></span> out of his brain, which must have worked because that baby grew up to be a brain surgeon!
</p>
</div>

<div id='memhook-button-bar'>
<a href="vacuous#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/vacuous/memory_hooks">Use other hook</a>
<a href="vacuous#" id="memhook-use-own" url="https://membean.com/mywords/vacuous/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='vacuous#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
I hate the <b>vacuous</b>, false, banal conversation that goes on among people.
<span class='attribution'>&mdash; Robert Crumb, American cartoonist and musician</span>
<img alt="Robert crumb, american cartoonist and musician" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Robert Crumb, American cartoonist and musician.jpg?qdep8" width="80" />
</li>
<li>
These are not entirely <b>vacuous</b> arguments either—that sense of destiny does filter into players' minds, reinforcing belief, confidence and conviction.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
The same mass-produced, <b>vacuous</b> music and movies are being eaten up with the same, if not greater, enthusiasm as in the late '90s.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
. . . I had got quite far from Heiligendamm, I hadn't even noticed how far, and seemed to be on higher ground, too, because somewhere below, on the line dividing land and water, the twinkling of a few tiny stars suggested the presence of houses, and I would have been at least as ashamed of taking flight as I was disturbed by the <b>vacuous</b> stare of the marsh at my back.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/vacuous/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='vacuous#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='vac_empty' data-tree-url='//cdn1.membean.com/public/data/treexml' href='vacuous#'>
<span class=''></span>
vac
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>be empty</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='uous_nature' data-tree-url='//cdn1.membean.com/public/data/treexml' href='vacuous#'>
<span class=''></span>
-uous
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of the nature of</td>
</tr>
</table>
<p>If one&#8217;s mind is <em>vacuous</em>, it has the &#8220;nature of being empty,&#8221; hence &#8220;unintelligent.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='vacuous#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Vacuous" src="https://cdn3.membean.com/public/images/wordimages/cons2/vacuous.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='vacuous#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>amorphous</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>callow</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>dearth</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>demented</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>denude</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>fatuous</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>inane</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>insipid</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>jejune</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>lacuna</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>nebulous</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>obtuse</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>opaque</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>paucity</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>tentative</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>vapid</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abstruse</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>acumen</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>arcane</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>connoisseur</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>crux</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>discern</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>erudite</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>fathom</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>incisive</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>intelligentsia</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>precocious</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>profundity</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>recondite</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>sagacious</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>sapient</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>savant</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='vacuous#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
vacuity
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>an emptiness</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="vacuous" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>vacuous</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="vacuous#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

