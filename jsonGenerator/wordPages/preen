
<!DOCTYPE html>
<html>
<head>
<title>Word: preen | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Preen-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/preen-large.jpg?qdep8" />
</div>
<a href="preen#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>To accentuate something is to emphasize it or make it more noticeable.</p>
<p class='rw-defn idx1' style='display:none'>Something austere is simple and plain in its style; an austere person is strict and severe with themselves.</p>
<p class='rw-defn idx2' style='display:none'>When you bedeck something, you decorate it in a showy way.</p>
<p class='rw-defn idx3' style='display:none'>Braggadocio is an excessively proud way of talking about your achievements or possessions that can annoy others.</p>
<p class='rw-defn idx4' style='display:none'>To burnish something is to work hard to improve it, such as polishing a metal until it shines.</p>
<p class='rw-defn idx5' style='display:none'>To denude an area is to remove the plants and trees that cover it; it can also mean to make something bare.</p>
<p class='rw-defn idx6' style='display:none'>If you embellish something, you make it more beautiful by decorating it.</p>
<p class='rw-defn idx7' style='display:none'>Something florid has too much decoration or is too elaborate.</p>
<p class='rw-defn idx8' style='display:none'>If something is gilded, it has been deceptively given a more attractive appearance than it normally possesses; a very thin layer of gold often covers something gilded.</p>
<p class='rw-defn idx9' style='display:none'>Something loathsome is offensive, disgusting, and brings about intense dislike.</p>
<p class='rw-defn idx10' style='display:none'>Something macabre is so gruesome or frightening that it causes great horror in those who see it.</p>
<p class='rw-defn idx11' style='display:none'>An ornate object is heavily or excessively decorated with complicated shapes and patterns.</p>
<p class='rw-defn idx12' style='display:none'>If you describe an action as ostentatious, you think it is an extreme and exaggerated way of impressing people.</p>
<p class='rw-defn idx13' style='display:none'>When you refurbish something, you repair it to improve its appearance or ability to function.</p>
<p class='rw-defn idx14' style='display:none'>Something that is repulsive is offensive, highly unpleasant, or just plain disgusting.</p>
<p class='rw-defn idx15' style='display:none'>People or things that are resplendent are beautiful, bright, and impressive in appearance.</p>
<p class='rw-defn idx16' style='display:none'>A scintillating conversation, speech, or performance is brilliantly clever, interesting, and lively.</p>
<p class='rw-defn idx17' style='display:none'>Something that is specious seems to be good, sound, or correct but is actually wrong or fake.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx19' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Verb</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>preen</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='preen#' id='pronounce-sound' path='audio/words/amy-preen'></a>
preen
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='preen#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='preen#' id='context-sound' path='audio/wordcontexts/brian-preen'></a>
Watching the birds <em>preen</em> and clean themselves in the birdbath, I was reminded of my Aunt Mary as she beautified herself before she went out for the day.  Like the birds in my view, Aunt Mary would <em>preen</em> and groom herself by brushing her hair, cleaning her face, and putting on her make-up.  As the birds cleaned their feathers during their careful <em>preening</em>, I wondered whether Aunt Mary had ever noticed the similarities between the birds and herself.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Which is an example of <em>preening</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Styling your hair to make it look presentable to others.
</li>
<li class='choice '>
<span class='result'></span>
Plucking feathers from a bird to prepare it for cooking.
</li>
<li class='choice '>
<span class='result'></span>
Using computer technology to make your work easier.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='preen#' id='definition-sound' path='audio/wordmeanings/amy-preen'></a>
Animals <em>preen</em> when they smooth out their fur or their feathers; humans <em>preen</em> by making themselves beautiful in front of a mirror.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>beautify</em>
</span>
</span>
</div>
<a class='quick-help' href='preen#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='preen#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/preen/memory_hooks/3577.json'></span>
<p>
<span class="emp0"><span>Cl<span class="emp3"><span>ean</span></span>, Then S<span class="emp3"><span>een</span></span></span></span> J<span class="emp3"><span>ean</span></span>nette's rule before she went out with her boyfriend was to pr<span class="emp3"><span>een</span></span> and cl<span class="emp3"><span>ean</span></span> herself well before being s<span class="emp3"><span>een</span></span>; she did, after all, have an image she wanted to present!
</p>
</div>

<div id='memhook-button-bar'>
<a href="preen#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/preen/memory_hooks">Use other hook</a>
<a href="preen#" id="memhook-use-own" url="https://membean.com/mywords/preen/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='preen#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Shanahan had hoped to tug on his winged-wheel sweater in private and take a good long look in the mirror, but he had no time to pose or <b>preen</b>.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Humans use mirrors so reflexively that we’ll often use shop windows or phone screens to <b>preen</b> ourselves without a second thought.
<cite class='attribution'>
&mdash;
Popular Science
</cite>
</li>
<li>
When a bird grasps a feather with its bill and slides to the feather end, the bird is, simply put, zipping itself up. The bird, often after a vigorous shake, pulls itself together, and is a smooth beauty once again. Birds also <b>preen</b> to distribute oil to their feathers from what is called the <b>preen</b> gland, located at the base of the tail. <b>Preening</b> can also include what we might call sweeping and dusting, and pest control, picking out lice and mites.
<cite class='attribution'>
&mdash;
Minneapolis Star Tribune
</cite>
</li>
<li>
What do cats mean now, in the heyday of cat videos, as demands for animal rights grow? The feline-centric exhibition “A Cat May Look” examines that question at Wellesley College’s Jewett Art Gallery. . . . Good portraits convey the tension between posing and revealing in a subject. Cats may <b>preen</b>, but they don’t fuss about what others will see in pictures of them.
<cite class='attribution'>
&mdash;
The Boston Globe
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/preen/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='preen#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep4" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>A combination of root words meaning &#8220;to smear with oil before&#8221; and &#8220;to prune.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='preen#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep4" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Preen" src="https://cdn2.membean.com/public/images/wordimages/cons2/preen.png?qdep4" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='preen#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>accentuate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bedeck</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>braggadocio</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>burnish</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>embellish</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>florid</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>gilded</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>ornate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>ostentatious</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>refurbish</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>resplendent</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>scintillating</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>specious</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>austere</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>denude</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>loathsome</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>macabre</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>repulsive</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="preen" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>preen</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="preen#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

