
<!DOCTYPE html>
<html>
<head>
<title>Word: gregarious | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Gregarious-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/gregarious-large.jpg?qdep8" />
</div>
<a href="gregarious#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx1' style='display:none'>Amity is a peaceful friendship between two parties, particularly between two countries.</p>
<p class='rw-defn idx2' style='display:none'>Someone who has a bilious personality is highly irritable and bad-tempered.</p>
<p class='rw-defn idx3' style='display:none'>If you describe a person&#8217;s behavior or speech as brusque, you mean that they say or do things abruptly or too quickly in a somewhat rude and impolite way.</p>
<p class='rw-defn idx4' style='display:none'>A clique is a group of people that spends a lot of time together and tends to be unfriendly towards people who are not part of the group.</p>
<p class='rw-defn idx5' style='display:none'>To cloister someone is to remove and isolate them from the rest of the world.</p>
<p class='rw-defn idx6' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx7' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx8' style='display:none'>People convene when they gather together or are called together by someone for a meeting.</p>
<p class='rw-defn idx9' style='display:none'>A convivial atmosphere or occasion is friendly, pleasant, cheerful, and relaxed.</p>
<p class='rw-defn idx10' style='display:none'>A dissident is someone who disagrees publicly with a government, especially in a country where this is not allowed.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is dour is serious, stubborn, and unfriendly; they can also be gloomy.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is effusive expresses happiness, pleasure, admiration, praise, etc., in an extremely enthusiastic way.</p>
<p class='rw-defn idx13' style='display:none'>An enclave is a small area within a larger area of a city or country in which people of a different nationality or culture live.</p>
<p class='rw-defn idx14' style='display:none'>A forlorn person is lonely because they have been abandoned; a forlorn home has been deserted.</p>
<p class='rw-defn idx15' style='display:none'>A garrulous person talks a lot, especially about unimportant things.</p>
<p class='rw-defn idx16' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx17' style='display:none'>If you are incommunicado, you are out of touch, unable to be communicated with, or in an isolated situation.</p>
<p class='rw-defn idx18' style='display:none'>An introvert is someone who primarily prefers being by themselves instead of hanging out with others socially; nevertheless, they still enjoy spending time with friends.</p>
<p class='rw-defn idx19' style='display:none'>An irascible person becomes angry very easily.</p>
<p class='rw-defn idx20' style='display:none'>A person who is being laconic uses very few words to say something.</p>
<p class='rw-defn idx21' style='display:none'>Someone who is loquacious talks too much.</p>
<p class='rw-defn idx22' style='display:none'>Someone who is morose is unhappy, bad-tempered, and unwilling to talk very much.</p>
<p class='rw-defn idx23' style='display:none'>A recluse is someone who chooses to live alone and deliberately avoids other people.</p>
<p class='rw-defn idx24' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is saturnine is looking miserable and sad, sometimes in a threatening or unfriendly way.</p>
<p class='rw-defn idx26' style='display:none'>A taciturn person is quiet or silent by nature.</p>
<p class='rw-defn idx27' style='display:none'>A troglodyte is reclusive, severely lacking in social skills, and is out of step with current times.</p>
<p class='rw-defn idx28' style='display:none'>A voluble person can speak easily with speech that naturally flows; voluble people also tend to be quite chatty.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>gregarious</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='gregarious#' id='pronounce-sound' path='audio/words/amy-gregarious'></a>
gri-GAIR-ee-uhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='gregarious#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='gregarious#' id='context-sound' path='audio/wordcontexts/brian-gregarious'></a>
The <em>gregarious</em>, outgoing musician loved to talk to his audience after a performance.  He shook hands, laughed, met old friends, and generally behaved in a friendly, welcoming, and <em>gregarious</em> way.  His social or <em>gregarious</em> nature was well-known and drew more people to hear the music.  Music lovers admired the skill of his performance on stage, but they also enjoyed his kind, sociable, and <em>gregarious</em> welcome face to face.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How might someone who is <em>gregarious</em> act at school?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They would try to correctly answer every question in class.
</li>
<li class='choice '>
<span class='result'></span>
They would have trouble staying on task and get in trouble often.
</li>
<li class='choice answer '>
<span class='result'></span>
They would sit with and talk to a large group of friends at lunch.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='gregarious#' id='definition-sound' path='audio/wordmeanings/amy-gregarious'></a>
A <em>gregarious</em> person is friendly, highly social, and prefers being with people rather than being alone.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>very social</em>
</span>
</span>
</div>
<a class='quick-help' href='gregarious#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='gregarious#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/gregarious/memory_hooks/9518.json'></span>
<p>
<img alt="Gregarious" src="https://cdn2.membean.com/public/images/wordimages/hook/gregarious.jpg?qdep8" />
<span class="emp0"><span><span class="emp1"><span>Greg</span></span>, <span class="emp2"><span>Gary</span></span>, & <span class="emp3"><span>Us</span></span></span></span> We are such a <span class="emp1"><span>greg</span></span><span class="emp2"><span>ari</span></span>o<span class="emp3"><span>us</span></span> group, <span class="emp1"><span>Greg</span></span>, <span class="emp2"><span>Gary</span></span>, and <span class="emp3"><span>us</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="gregarious#" id="add-public-hook" style="" url="https://membean.com/mywords/gregarious/memory_hooks">Use other public hook</a>
<a href="gregarious#" id="memhook-use-own" url="https://membean.com/mywords/gregarious/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='gregarious#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
He may be a blunt-spoken jurist who enjoys mingling with the Washington establishment, but Scalia likes the sheltered life of the court. A <b>gregarious</b> academic before he ascended to the bench, he believes deeply in the British common-law tradition that judges keep a low public profile.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
People who love sales often are <b>gregarious</b> and garrulous, but that gets in the way when they like to hear the sound of their own voice more than that of their customers'.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/gregarious/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='gregarious#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='greg_flock' data-tree-url='//cdn1.membean.com/public/data/treexml' href='gregarious#'>
<span class=''></span>
greg
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>flock</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ar_pertaining' data-tree-url='//cdn1.membean.com/public/data/treexml' href='gregarious#'>
<span class=''></span>
-ar
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>someone who does something, pertaining to</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ious_nature' data-tree-url='//cdn1.membean.com/public/data/treexml' href='gregarious#'>
<span class=''></span>
-ious
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of the nature of</td>
</tr>
</table>
<p>A <em>gregarious</em> person is highly social because he &#8220;has the nature&#8221; of &#8220;flocking&#8221; with others.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='gregarious#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Finding Vivian Maier</strong><span> Street photographers need to be gregarious to find the best shots.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/gregarious.jpg' video_url='examplevids/gregarious' video_width='350'></span>
<div id='wt-container'>
<img alt="Gregarious" height="288" src="https://cdn1.membean.com/video/examplevids/gregarious.jpg" width="350" />
<div class='center'>
<a href="gregarious#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='gregarious#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Gregarious" src="https://cdn1.membean.com/public/images/wordimages/cons2/gregarious.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='gregarious#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>amity</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>clique</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>convene</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>convivial</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>effusive</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>garrulous</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>loquacious</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>voluble</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>bilious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>brusque</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>cloister</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>dissident</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>dour</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>enclave</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>forlorn</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>incommunicado</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>introvert</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>irascible</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>laconic</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>morose</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>recluse</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>saturnine</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>taciturn</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>troglodyte</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="gregarious" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>gregarious</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="gregarious#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

