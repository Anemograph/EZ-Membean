
<!DOCTYPE html>
<html>
<head>
<title>Word: promulgate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you adjure someone to do something, you persuade, eagerly appeal, or solemnly order them to do it.</p>
<p class='rw-defn idx1' style='display:none'>Covert activities or situations are secret and hidden.</p>
<p class='rw-defn idx2' style='display:none'>When something is diffuse, it is widely spread out and scattered.</p>
<p class='rw-defn idx3' style='display:none'>When you are given a directive, you are given an instruction or order that directs you to do something.</p>
<p class='rw-defn idx4' style='display:none'>A dispersal of something is its scattering or distribution over a wide area.</p>
<p class='rw-defn idx5' style='display:none'>To disseminate something, such as knowledge or information, is to distribute it so that it reaches a lot of people.</p>
<p class='rw-defn idx6' style='display:none'>To divulge something is to reveal information that was once private or secret.</p>
<p class='rw-defn idx7' style='display:none'>An edict is an official order or command given by a government or someone in authority.</p>
<p class='rw-defn idx8' style='display:none'>If something fetters you, it restricts you from doing something you want to do.</p>
<p class='rw-defn idx9' style='display:none'>A fiat is an official order from a person or group in authority.</p>
<p class='rw-defn idx10' style='display:none'>If someone behaves in a furtive way, they do things sneakily and secretly in order to avoid being noticed.</p>
<p class='rw-defn idx11' style='display:none'>When it is imperative that something be done, it is absolutely necessary or very important that it be accomplished.</p>
<p class='rw-defn idx12' style='display:none'>An injunction is a court order that prevents someone from doing something.</p>
<p class='rw-defn idx13' style='display:none'>An interdict is an official order that prevents someone from doing something.</p>
<p class='rw-defn idx14' style='display:none'>When you litigate, you go before a court in order to bring forth a lawsuit or voice another serious concern you have.</p>
<p class='rw-defn idx15' style='display:none'>A mandate is an official order or command issued by a government or other authorized body.</p>
<p class='rw-defn idx16' style='display:none'>An overt act is not hidden or secret but is done in an open and public way.</p>
<p class='rw-defn idx17' style='display:none'>When a substance permeates something, it enters into all parts of it.</p>
<p class='rw-defn idx18' style='display:none'>If something is pervasive, it appears to be everywhere.</p>
<p class='rw-defn idx19' style='display:none'>To be politic in a decision is to be socially wise and diplomatic.</p>
<p class='rw-defn idx20' style='display:none'>When someone proscribes an activity, they prohibit it by establishing a rule against it.</p>
<p class='rw-defn idx21' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx22' style='display:none'>Something statutory, such as the power given to a governor or president, is created, established, and controlled by rules and laws; hence, it has the full force of the law behind it and must be followed.</p>
<p class='rw-defn idx23' style='display:none'>A stentorian voice is extremely loud and strong.</p>
<p class='rw-defn idx24' style='display:none'>A stricture is a rule or condition that imposes restrictions or limits on what you can do.</p>
<p class='rw-defn idx25' style='display:none'>If something, such as warmth, color, or liquid, suffuses a material, it gradually spreads through or covers it; if you are suffused with a feeling, you are full of that feeling.</p>
<p class='rw-defn idx26' style='display:none'>A surreptitious deed is done secretly to avoid bringing any attention to it.</p>
<p class='rw-defn idx27' style='display:none'>When something is transfused to another thing, it is given, put, or imparted to it; for example, you can transfuse blood or a love of reading from one person to another.</p>
<p class='rw-defn idx28' style='display:none'>A writ is an official document that orders a person to do something.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>promulgate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='promulgate#' id='pronounce-sound' path='audio/words/amy-promulgate'></a>
PROM-uhl-gayt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='promulgate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='promulgate#' id='context-sound' path='audio/wordcontexts/brian-promulgate'></a>
After many miners had died in a mismanaged and unsafe mine, the coal company <em>promulgated</em> or made widely known to the public new safety measures that were being put into effect immediately.  The primary reason that the company <em>promulgated</em> or officially announced these safety procedures was to calm the public and protect its rapidly worsening image.  In addition, lawmakers in the state in which the mines were located <em>promulgated</em> or broadcasted  new laws that had been put into effect in order to further make sure that the public knew the mines were safe places in which to work.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If someone is in the process of <em>promulgating</em> something, what are they doing?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They are continuing to ignore a problem that is gradually worsening.
</li>
<li class='choice '>
<span class='result'></span>
They are sharing secret information with a few close friends.
</li>
<li class='choice answer '>
<span class='result'></span>
They are making an official declaration to the public.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='promulgate#' id='definition-sound' path='audio/wordmeanings/amy-promulgate'></a>
To <em>promulgate</em> something is to officially announce it in order to make it widely known or more specifically to let the public know that a new law has been put into effect.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>declare</em>
</span>
</span>
</div>
<a class='quick-help' href='promulgate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='promulgate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/promulgate/memory_hooks/5829.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Romul</span></span>an <span class="emp1"><span>Gate</span></span></span></span> As soon as members of the Federation found out about the new <span class="emp3"><span>Romul</span></span>an exploding space <span class="emp1"><span>gate</span></span>, they p<span class="emp3"><span>romul</span></span><span class="emp1"><span>gate</span></span>d its dangers to all of the fleet of starships to make them aware of it.
</p>
</div>

<div id='memhook-button-bar'>
<a href="promulgate#" id="add-public-hook" style="" url="https://membean.com/mywords/promulgate/memory_hooks">Use other public hook</a>
<a href="promulgate#" id="memhook-use-own" url="https://membean.com/mywords/promulgate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='promulgate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
U.S. officials also want to avoid the soapbox nature of Zacarias Moussaoui’s trial in Washington, which gives terrorists an opportunity to <b>promulgate</b> their world view publicly.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
Mehta observed that the agency has had more than 20 years—far in excess of any “unreasonable delay”—to <b>promulgate</b> a final regulation and ordered the board to come up with a regulation within 12 months. “The court will not grant it two full years to do what it should have done long ago,” Mehta wrote.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/promulgate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='promulgate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pro_forward' data-tree-url='//cdn1.membean.com/public/data/treexml' href='promulgate#'>
<span class='common'></span>
pro-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>forward, forth</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='vulg_make' data-tree-url='//cdn1.membean.com/public/data/treexml' href='promulgate#'>
<span class=''></span>
vulg
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make common, publish</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ate_quality' data-tree-url='//cdn1.membean.com/public/data/treexml' href='promulgate#'>
<span class=''></span>
-ate
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make something have a certain quality</td>
</tr>
</table>
<p>When one <em>promulgates</em> something, one makes it widely known by &#8220;publishing it forth&#8221; among the people, thereby &#8220;making it common.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='promulgate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Promulgate" src="https://cdn1.membean.com/public/images/wordimages/cons2/promulgate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='promulgate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adjure</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>diffuse</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>directive</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>dispersal</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>disseminate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>divulge</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>edict</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>fiat</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>imperative</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>injunction</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>interdict</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>litigate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>mandate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>overt</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>permeate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>pervasive</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>politic</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>proscribe</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>statutory</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>stentorian</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>stricture</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>suffuse</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>transfuse</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>writ</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>covert</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>fetter</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>furtive</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>surreptitious</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="promulgate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>promulgate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="promulgate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

