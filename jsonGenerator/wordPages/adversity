
<!DOCTYPE html>
<html>
<head>
<title>Word: adversity | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An affliction is something that causes pain and mental suffering, especially a medical condition.</p>
<p class='rw-defn idx1' style='display:none'>Something that is arduous is extremely difficult; hence, it involves a lot of effort.</p>
<p class='rw-defn idx2' style='display:none'>The adjective auspicious describes a positive beginning of something, such as a new business, or a certain time that looks to have a good chance of success or prosperity.</p>
<p class='rw-defn idx3' style='display:none'>Bedlam is a situation with a great deal of noise and confusion.</p>
<p class='rw-defn idx4' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx5' style='display:none'>Something is a blight if it spoils or damages things severely; blight is often used to describe something that ruins or kills crops.</p>
<p class='rw-defn idx6' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx7' style='display:none'>The adjective bucolic is used to describe things that are related to or characteristic of rural or country life.</p>
<p class='rw-defn idx8' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx9' style='display:none'>A cataclysm is a violent, sudden event that causes great change and/or harm.</p>
<p class='rw-defn idx10' style='display:none'>When you cavort, you jump and dance around in a playful, excited, or physically lively way.</p>
<p class='rw-defn idx11' style='display:none'>A chaotic state of affairs is in a state of confusion and complete disorder.</p>
<p class='rw-defn idx12' style='display:none'>If something encumbers you, it makes it difficult for you to move freely or do what you want.</p>
<p class='rw-defn idx13' style='display:none'>Ennui is the feeling of being bored, tired, and dissatisfied with life.</p>
<p class='rw-defn idx14' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx15' style='display:none'>When someone is exonerated from guilt, a task, or a responsibility, they are set free or excused from it.</p>
<p class='rw-defn idx16' style='display:none'>If you criticize someone&#8217;s arguments as being facile, you consider their ideas simplistic and not well thought through.</p>
<p class='rw-defn idx17' style='display:none'>A fete is a celebration or festival in honor of a special occasion.</p>
<p class='rw-defn idx18' style='display:none'>If something is formidable, it is impressive in size, power, or skill; therefore, it can make you feel frightened, alarmed, or in awe because it is so powerful or difficult to deal with.</p>
<p class='rw-defn idx19' style='display:none'>If someone gambols, they run, jump, and play like a young child or animal.</p>
<p class='rw-defn idx20' style='display:none'>A halcyon time is calm, peaceful, and undisturbed.</p>
<p class='rw-defn idx21' style='display:none'>An idyll is a place or situation that is extremely pleasant, peaceful, and has no problems.</p>
<p class='rw-defn idx22' style='display:none'>If someone is indefatigable, they never shows signs of getting tired.</p>
<p class='rw-defn idx23' style='display:none'>A laborious job or process takes a long time, requires a lot of effort, and is often boring.</p>
<p class='rw-defn idx24' style='display:none'>If you nettle someone, you irritate or annoy them.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is obdurate is stubborn, unreasonable, or uncontrollable; hence, they simply refuse to change their behavior, actions, or beliefs to fit any situation whatsoever.</p>
<p class='rw-defn idx26' style='display:none'>An obstinate person refuses to change their mind, even when other people think they are being highly unreasonable.</p>
<p class='rw-defn idx27' style='display:none'>A task or responsibility is onerous if you dislike having to do it because it is difficult or tiring.</p>
<p class='rw-defn idx28' style='display:none'>Pandemonium is a very noisy and uncontrolled situation, especially one that is caused by a lot of angry or excited people.</p>
<p class='rw-defn idx29' style='display:none'>If someone regales you, they tell you stories and jokes to entertain you— and they could also serve you a wonderful feast.</p>
<p class='rw-defn idx30' style='display:none'>Retribution is severe punishment that someone deserves because they have done something very wrong; it especially refers to punishment or revenge that is carried out by someone other than official authorities.</p>
<p class='rw-defn idx31' style='display:none'>A salubrious place or area is pleasant, clean, healthy, and comfortable to live in.</p>
<p class='rw-defn idx32' style='display:none'>A salutary experience is beneficial to you since it strengthens you in some way, although it may be unpleasant as you undergo it; this word also refers to promoting good health.</p>
<p class='rw-defn idx33' style='display:none'>A serene place or situation is peaceful and calm.</p>
<p class='rw-defn idx34' style='display:none'>If something is tranquil, it is peaceful, calm, and quiet.</p>
<p class='rw-defn idx35' style='display:none'>Severe problems, suffering, or difficulties experienced in a particular situation are all examples of tribulations.</p>
<p class='rw-defn idx36' style='display:none'>If you are unflagging while doing a task, you are untiring when working upon it and do not stop until it is finished.</p>
<p class='rw-defn idx37' style='display:none'>A vagary is an unpredictable or unexpected change in action or behavior.</p>
<p class='rw-defn idx38' style='display:none'>Vicissitudes can be unexpected or simply normal changes and variations that happen during the course of people&#8217;s lives.</p>
<p class='rw-defn idx39' style='display:none'>Someone who is woebegone is very sad and filled with grief.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>adversity</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='adversity#' id='pronounce-sound' path='audio/words/amy-adversity'></a>
ad-VUR-si-tee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='adversity#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='adversity#' id='context-sound' path='audio/wordcontexts/brian-adversity'></a>
After facing much <em>adversity</em>, such as a knee injury, the untimely death of her mother, and political unrest, the Olympic athlete nevertheless made it to the finals of the mile.  The <em>adversity</em> or hardship that preceded her gold-medal run seemed nonexistent as she ran with total focus and dedication.  When she won, the sports announcer asked her how she managed to leave all that <em>adversity</em> and misfortune behind her.  She said that she had dedicated her run to her mother, and so was able to overcome any <em>adversity</em> or trouble she otherwise might have encountered.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is <em>adversity</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is an event you are excited about.
</li>
<li class='choice answer '>
<span class='result'></span>
It is a hardship in your life.
</li>
<li class='choice '>
<span class='result'></span>
It is an unforgettable moment in your day.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='adversity#' id='definition-sound' path='audio/wordmeanings/amy-adversity'></a>
If you undergo <em>adversity</em> in life, you struggle with trouble, bad luck, and difficult times.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>difficult times</em>
</span>
</span>
</div>
<a class='quick-help' href='adversity#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='adversity#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/adversity/memory_hooks/3167.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>A</span></span> "<span class="emp2"><span>D</span></span>" at the Uni<span class="emp3"><span>versity</span></span></span></span> I faced a great deal of academic <span class="emp1"><span>a</span></span><span class="emp2"><span>d</span></span><span class="emp3"><span>versity</span></span> my first year in college, especially when I received <span class="emp1"><span>a</span></span> "<span class="emp2"><span>D</span></span>" on my uni<span class="emp3"><span>versity</span></span> report card.
</p>
</div>

<div id='memhook-button-bar'>
<a href="adversity#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/adversity/memory_hooks">Use other hook</a>
<a href="adversity#" id="memhook-use-own" url="https://membean.com/mywords/adversity/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='adversity#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Nearly all men can stand <b>adversity</b>, but if you want to test a man's character, give him power.
<span class='attribution'>&mdash; Robert G. Ingersoll, 19th century American writer</span>
<img alt="Robert g" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Robert G. Ingersoll, 19th century American writer.jpg?qdep8" width="80" />
</li>
<li>
Here are some of Siebert’s marks of emotional resilience and excellent mental health: When hit by <b>adversity</b>, you have a learning/coping reaction, rather than a blaming/victim reaction.
<cite class='attribution'>
&mdash;
WebMD
</cite>
</li>
<li>
At their best, superhero origin stories inspire us and provide models of coping with <b>adversity</b>, finding meaning in loss and trauma, discovering our strengths and using them for good purpose. (Wearing a cape or tights is optional.)
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
"Children and their parents can come out of it all with a sense having been able to triumph over <b>adversity</b>," she says.
<cite class='attribution'>
&mdash;
WebMD
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/adversity/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='adversity#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ad_towards' data-tree-url='//cdn1.membean.com/public/data/treexml' href='adversity#'>
<span class='common'></span>
ad-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to, towards</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='vers_turned' data-tree-url='//cdn1.membean.com/public/data/treexml' href='adversity#'>
<span class='common'></span>
vers
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>turned</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ity_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='adversity#'>
<span class=''></span>
-ity
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state or quality</td>
</tr>
</table>
<p>When you suffer from the unfortunate &#8220;state of (something being) turned towards&#8221; you, you experience <em>adversity.</em></p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='adversity#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>ABC News: Overcomers</strong><span> People who overcome adversity.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/adversity.jpg' video_url='examplevids/adversity' video_width='350'></span>
<div id='wt-container'>
<img alt="Adversity" height="288" src="https://cdn1.membean.com/video/examplevids/adversity.jpg" width="350" />
<div class='center'>
<a href="adversity#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='adversity#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Adversity" src="https://cdn0.membean.com/public/images/wordimages/cons2/adversity.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='adversity#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>affliction</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>arduous</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bedlam</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>blight</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>cataclysm</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>chaotic</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>encumber</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>ennui</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>formidable</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>laborious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>nettle</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>onerous</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>pandemonium</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>retribution</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>tribulation</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>vagary</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>vicissitude</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>woebegone</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>auspicious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>bucolic</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>cavort</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>exonerate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>facile</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>fete</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>gambol</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>halcyon</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>idyll</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>indefatigable</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>obdurate</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>obstinate</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>regale</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>salubrious</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>salutary</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>serene</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>tranquil</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>unflagging</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='adversity#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
adverse
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>opposed, harmful</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="adversity" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>adversity</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="adversity#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

