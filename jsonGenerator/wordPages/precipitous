
<!DOCTYPE html>
<html>
<head>
<title>Word: precipitous | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>The acme of something is its highest point of achievement or excellence.</p>
<p class='rw-defn idx1' style='display:none'>The apex of anything, such as an organization or system, is its highest part or most important position.</p>
<p class='rw-defn idx2' style='display:none'>Something that is arduous is extremely difficult; hence, it involves a lot of effort.</p>
<p class='rw-defn idx3' style='display:none'>An embankment is a built-up bank or ridge made of earth or stone that supports a road or holds back water.</p>
<p class='rw-defn idx4' style='display:none'>An escarpment is a wide, steep slope or ridge, formed by erosion, that rises suddenly from level land.</p>
<p class='rw-defn idx5' style='display:none'>A gradient is an upward or downward slanting slope, the steepness of that slope, or the rate of change of a measured quantity over a given distance.</p>
<p class='rw-defn idx6' style='display:none'>A laborious job or process takes a long time, requires a lot of effort, and is often boring.</p>
<p class='rw-defn idx7' style='display:none'>The nadir of a situation is its lowest point.</p>
<p class='rw-defn idx8' style='display:none'>If something obtrudes, it becomes noticeable or attracts attention in a way that is not pleasant or welcome.</p>
<p class='rw-defn idx9' style='display:none'>A perilous situation is highly dangerous or extremely risky.</p>
<p class='rw-defn idx10' style='display:none'>A precarious situation or state can very quickly become dangerous without warning.</p>
<p class='rw-defn idx11' style='display:none'>A promontory is a high feature of the landscape that juts out over a body of water.</p>
<p class='rw-defn idx12' style='display:none'>Something that protrudes is sticking or pushing outward from something else.</p>
<p class='rw-defn idx13' style='display:none'>A recumbent figure or person is lying down.</p>
<p class='rw-defn idx14' style='display:none'>If you are supine, you are lying on your back with your face upward.</p>
<p class='rw-defn idx15' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>precipitous</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='precipitous#' id='pronounce-sound' path='audio/words/amy-precipitous'></a>
pre-SIP-i-tuhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='precipitous#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='precipitous#' id='context-sound' path='audio/wordcontexts/brian-precipitous'></a>
On the day that the stock market dropped so <em>precipitously</em> or steeply I lost one million dollars. I felt a <em>precipitous</em> or sharp lurch in my stomach when I found out the bad news. The graph that showed the sharp drop resembled a <em>precipitous</em> or sheer cliff; luckily, money isn&#8217;t everything in the world.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of something that is <em>precipitous</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A room so dark that you cannot see anything at all.
</li>
<li class='choice answer '>
<span class='result'></span>
A ski slope that goes almost straight down a mountainside.
</li>
<li class='choice '>
<span class='result'></span>
A rope bridge that feels like it is not securely attached at each end.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='precipitous#' id='definition-sound' path='audio/wordmeanings/amy-precipitous'></a>
A <em>precipitous</em> cliff or drop is very steep or falls sharply.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>steep</em>
</span>
</span>
</div>
<a class='quick-help' href='precipitous#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='precipitous#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/precipitous/memory_hooks/2806.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Recipe</span></span> for Disaster</span></span> A p<span class="emp1"><span>recip</span></span>ic<span class="emp1"><span>e</span></span> is a <span class="emp1"><span>recipe</span></span> for disaster, so don't go near it!
</p>
</div>

<div id='memhook-button-bar'>
<a href="precipitous#" id="add-public-hook" style="" url="https://membean.com/mywords/precipitous/memory_hooks">Use other public hook</a>
<a href="precipitous#" id="memhook-use-own" url="https://membean.com/mywords/precipitous/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='precipitous#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Whenever the 54-year-old schoolteacher happened upon a run that was <b>precipitous</b>, icy or decked with large moguls, "I'd just become this frumpy lady, this timid soul," she says. "And I'm not that way at all.”
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
The story detailed the previously unexpected consequences of nuclear war: prolonged dust and smoke, a <b>precipitous</b> drop in Earth's temperatures and widespread failure of crops, leading to deadly famine.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
The researchers also modeled what will happen if [White-Nose Syndrome] mortality declines. "You get a longer time frame to extinction, and that's good. The crash isn't so <b>precipitous</b>, so rapid," said Frick. "But you still have to get the declines to less than five percent per year" before eastern little brown bats last beyond the century.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
All of the materials, including the logs and dirt for the walls, had to be found elsewhere and carried up from the valley floor. It took 12 years to build. Just as I began to walk back to the visitor center, down the narrow and <b>precipitous</b> stone path that the Acoma used before the road was built in 1929 by a film company, our guide pointed out a small, opaque mica window in one of the homes.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/precipitous/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='precipitous#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pre_before' data-tree-url='//cdn1.membean.com/public/data/treexml' href='precipitous#'>
<span class='common'></span>
pre-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>before, in front</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='capit_head' data-tree-url='//cdn1.membean.com/public/data/treexml' href='precipitous#'>
<span class=''></span>
capit
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>head</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ous_possessing' data-tree-url='//cdn1.membean.com/public/data/treexml' href='precipitous#'>
<span class=''></span>
-ous
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>possessing the nature of</td>
</tr>
</table>
<p>A <em>precipitous</em> fall is &#8220;headlong,&#8221; that is, one&#8217;s &#8220;head is in front&#8221; as one falls.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='precipitous#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Mount Huashan</strong><span> Even with a chain and support a precipitous drop can be terrifying and worthy of respect.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/precipitous.jpg' video_url='examplevids/precipitous' video_width='350'></span>
<div id='wt-container'>
<img alt="Precipitous" height="288" src="https://cdn1.membean.com/video/examplevids/precipitous.jpg" width="350" />
<div class='center'>
<a href="precipitous#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='precipitous#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Precipitous" src="https://cdn2.membean.com/public/images/wordimages/cons2/precipitous.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='precipitous#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>acme</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>apex</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>arduous</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>embankment</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>escarpment</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>gradient</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>laborious</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>obtrude</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>perilous</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>precarious</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>promontory</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>protrude</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='7' class = 'rw-wordform notlearned'><span>nadir</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>recumbent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>supine</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='precipitous#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
precipice
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>steep cliff</td>
</tr>
<tr>
<td class='wordform'>
<span>
precipitate
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>hasty, sudden, rash</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="precipitous" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>precipitous</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="precipitous#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

