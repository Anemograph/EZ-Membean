
<!DOCTYPE html>
<html>
<head>
<title>Word: allude | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An allegorical poem or story employs allegory, that is, a literary device that uses literal events and characters to represent abstract ideas or deeper meanings.</p>
<p class='rw-defn idx1' style='display:none'>If one thing is analogous to another, a comparison can be made between the two because they are similar in some way.</p>
<p class='rw-defn idx2' style='display:none'>If a word or behavior connotes something, it suggests an additional idea or emotion that is not part of its original literal meaning.</p>
<p class='rw-defn idx3' style='display:none'>Someone who is erudite is steeped in knowledge because they have read and studied extensively.</p>
<p class='rw-defn idx4' style='display:none'>Something esoteric is known about or understood by very few people.</p>
<p class='rw-defn idx5' style='display:none'>If you use words in a figurative way, they have an abstract or symbolic meaning beyond their literal interpretation.</p>
<p class='rw-defn idx6' style='display:none'>Hackneyed words, images, ideas or sayings have been used so often that they no longer seem interesting or amusing.</p>
<p class='rw-defn idx7' style='display:none'>The adjective hermetic describes something that is set apart, isolated, or separate from the influence or interference of society at large.</p>
<p class='rw-defn idx8' style='display:none'>A metaphor is a word or phrase that means one thing but is used to describe something else in order to highlight similar qualities; for example, if someone is very brave, you might say that they have the heart of a lion.</p>
<p class='rw-defn idx9' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx10' style='display:none'>A nuance is a small difference in something that may be difficult to notice but is fairly important.</p>
<p class='rw-defn idx11' style='display:none'>A platitude is an unoriginal statement that has been used so many times that it is considered almost meaningless and pointless, even though it is presented as important.</p>
<p class='rw-defn idx12' style='display:none'>Something prosaic is dull, boring, and ordinary.</p>
<p class='rw-defn idx13' style='display:none'>If you refer to something as a trifle, you mean that it is of little or no importance or value.</p>
<p class='rw-defn idx14' style='display:none'>A trite remark or idea is so overused that it is no longer interesting or novel.</p>
<p class='rw-defn idx15' style='display:none'>Something vapid is dull, boring, and/or tiresome.</p>
<p class='rw-defn idx16' style='display:none'>If you repeat something verbatim, you use the same words that were spoken or written.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>allude</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='allude#' id='pronounce-sound' path='audio/words/amy-allude'></a>
uh-LOOD
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='allude#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='allude#' id='context-sound' path='audio/wordcontexts/brian-allude'></a>
Leonard Bernstein&#8217;s Broadway musical <em>West Side Story</em> <em>alludes</em> to Shakespeare&#8217;s play <em>Romeo and Juliet</em> by indirectly referring to it.  In creating such <em>allusions</em>, the artist echoes another story while associating his or her own to it.  This method of <em><em>alluding</em></em> or referring to other works of art without giving an exact identification of them enriches what the author is trying to say by drawing from a deep tradition.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When might you <em>allude</em> to a problem?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When you want to publicly discuss solutions to it through chatting.
</li>
<li class='choice '>
<span class='result'></span>
When you need other people to help you solve it.
</li>
<li class='choice answer '>
<span class='result'></span>
When you don&#8217;t want to talk about it by referring to it directly.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='allude#' id='definition-sound' path='audio/wordmeanings/amy-allude'></a>
When you <em>allude</em> to something or someone, often events or characters from literature or history, you refer to them in an indirect way.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>refer indirectly to</em>
</span>
</span>
</div>
<a class='quick-help' href='allude#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='allude#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/allude/memory_hooks/3625.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>All</span></span> N<span class="emp3"><span>ude</span></span></span></span> Would it be smart to <span class="emp1"><span>all</span></span><span class="emp3"><span>ude</span></span> to an <span class="emp1"><span>all</span></span> n<span class="emp3"><span>ude</span></span> adventure one has had during an important business meeting?
</p>
</div>

<div id='memhook-button-bar'>
<a href="allude#" id="add-public-hook" style="" url="https://membean.com/mywords/allude/memory_hooks">Use other public hook</a>
<a href="allude#" id="memhook-use-own" url="https://membean.com/mywords/allude/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='allude#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The significance of the topographic scenes is heightened by the fact that they <b>allude</b> to views of Constantinople, visual depictions of which are extremely rare in Byzantine art.
<cite class='attribution'>
&mdash;
The Art Bulletin
</cite>
</li>
<li>
Until the industry and public policy-makers can come up with a legal framework within which both the customers and suppliers can work securely with personal data, the disquiet to which you <b>allude</b> will only grow.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Both make reference to consumption-waste cycles, break down art-life distinctions, and <b>allude</b> to the magical powers of the artist, most emphatically in Klein’s filling a vacant gallery with intangible "pictorial sensibility."
<cite class='attribution'>
&mdash;
Art Journal
</cite>
</li>
<li>
Wal-Mart would not directly comment on tensions with the labels, but Gary Severson, Wal-Mart’s senior vice president and general merchandise manager in charge of the chain’s entertainment section, did <b>allude</b> to the dispute about music prices.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/allude/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='allude#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='al_to' data-tree-url='//cdn1.membean.com/public/data/treexml' href='allude#'>
<span class=''></span>
al-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to, towards, near</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='lud_play' data-tree-url='//cdn1.membean.com/public/data/treexml' href='allude#'>
<span class=''></span>
lud
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>play</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='allude#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>To <em>allude</em> to or associate one work of literature with another is to play towards or near it by mentioning it indirectly.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='allude#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Dilbert</strong><span> Asok came to the US because of the wide-open spaces alluded to in travel brochures.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/allude.jpg' video_url='examplevids/allude' video_width='350'></span>
<div id='wt-container'>
<img alt="Allude" height="288" src="https://cdn1.membean.com/video/examplevids/allude.jpg" width="350" />
<div class='center'>
<a href="allude#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='allude#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Allude" src="https://cdn2.membean.com/public/images/wordimages/cons2/allude.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='allude#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>allegorical</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>analogous</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>connote</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>erudite</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>esoteric</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>figurative</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>hermetic</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>metaphor</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>nuance</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='6' class = 'rw-wordform notlearned'><span>hackneyed</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>platitude</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>prosaic</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>trifle</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>trite</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>vapid</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>verbatim</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='allude#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
allusion
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>indirect reference</td>
</tr>
<tr>
<td class='wordform'>
<span>
allusive
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>consisting of indirect references</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="allude" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>allude</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="allude#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

