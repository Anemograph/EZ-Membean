
<!DOCTYPE html>
<html>
<head>
<title>Word: idiosyncratic | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Idiosyncratic-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/idiosyncratic-large.jpg?qdep8" />
</div>
<a href="idiosyncratic#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx1' style='display:none'>If something is an anomaly, it is different from what is usual or expected; hence, it is highly noticeable.</p>
<p class='rw-defn idx2' style='display:none'>A chimerical idea is unrealistic, imaginary, or unlikely to be fulfilled.</p>
<p class='rw-defn idx3' style='display:none'>A clique is a group of people that spends a lot of time together and tends to be unfriendly towards people who are not part of the group.</p>
<p class='rw-defn idx4' style='display:none'>If someone is eccentric, they behave in a strange and unusual way that is different from most people.</p>
<p class='rw-defn idx5' style='display:none'>A foible is a small weakness or character flaw in a person that is considered somewhat unusual; that said, it is also viewed as unimportant and harmless.</p>
<p class='rw-defn idx6' style='display:none'>Hackneyed words, images, ideas or sayings have been used so often that they no longer seem interesting or amusing.</p>
<p class='rw-defn idx7' style='display:none'>Heterodox beliefs, ideas, or practices are different from accepted or official ones.</p>
<p class='rw-defn idx8' style='display:none'>The word homogeneous is used to describe a group that has members or parts which are similar or are all of the same type.</p>
<p class='rw-defn idx9' style='display:none'>An iconoclast is someone who often attacks beliefs, ideas, or customs that are generally accepted by society.</p>
<p class='rw-defn idx10' style='display:none'>An intrinsic characteristic of something is the basic and essential feature that makes it what it is.</p>
<p class='rw-defn idx11' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx12' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx13' style='display:none'>A platitude is an unoriginal statement that has been used so many times that it is considered almost meaningless and pointless, even though it is presented as important.</p>
<p class='rw-defn idx14' style='display:none'>If you have a predilection for something, you have a preference for it.</p>
<p class='rw-defn idx15' style='display:none'>A proclivity is the tendency to behave in a particular way or to like a particular thing.</p>
<p class='rw-defn idx16' style='display:none'>A propensity is a natural tendency towards a particular behavior.</p>
<p class='rw-defn idx17' style='display:none'>Something prosaic is dull, boring, and ordinary.</p>
<p class='rw-defn idx18' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>idiosyncratic</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='idiosyncratic#' id='pronounce-sound' path='audio/words/amy-idiosyncratic'></a>
ID-ee-oh-sin-KRAT-it
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='idiosyncratic#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='idiosyncratic#' id='context-sound' path='audio/wordcontexts/brian-idiosyncratic'></a>
Ivonne was a genius with an amazing IQ, but her strange and <em>idiosyncratic</em> behavior puzzled some people.  Ivonne would <em>idiosyncratically</em> count each step on every staircase, and she also had the unusual habit of wearing bright orange socks every day.  These <em>idiosyncratic</em> tendencies may have appeared out of place or odd, but they merely reflected Ivonne&#8217;s unique and precise mind.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of <em>idiosyncratic</em> behavior?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Picking up one bad habit from your roommate after living with them for a few months. 
</li>
<li class='choice '>
<span class='result'></span>
Riding a bike to work to avoid negatively impacting the environment.
</li>
<li class='choice answer '>
<span class='result'></span>
Singing to your houseplants to help them grow.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='idiosyncratic#' id='definition-sound' path='audio/wordmeanings/amy-idiosyncratic'></a>
<em>Idiosyncratic</em> tendencies, behavior, or habits are unusual and strange.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>peculiar</em>
</span>
</span>
</div>
<a class='quick-help' href='idiosyncratic#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='idiosyncratic#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/idiosyncratic/memory_hooks/5530.json'></span>
<p>
<span class="emp0"><span>Demo<span class="emp3"><span>cratic</span></span> <span class="emp2"><span>Idio</span></span>t <span class="emp1"><span>Syn</span></span>thesis</span></span> Jan believed that if we took all the world's <span class="emp2"><span>idio</span></span>ts and formed a demo<span class="emp3"><span>cratic</span></span> state with them using a forced form of <span class="emp1"><span>syn</span></span>thesis, they would end up ruling the world; Jan tried to convince people everyday that her <span class="emp2"><span>idio</span></span><span class="emp1"><span>syn</span></span><span class="emp3"><span>cratic</span></span> view of demo<span class="emp3"><span>cratic</span></span> <span class="emp2"><span>idio</span></span>t <span class="emp1"><span>syn</span></span>thesis really would solve all the world's problems.
</p>
</div>

<div id='memhook-button-bar'>
<a href="idiosyncratic#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/idiosyncratic/memory_hooks">Use other hook</a>
<a href="idiosyncratic#" id="memhook-use-own" url="https://membean.com/mywords/idiosyncratic/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='idiosyncratic#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The leanness of _Rising_ is a major improvement on the sugar-shock overproduction of Ono’s mid -'80s solo albums and shows Lennon’s confidence in the <b>idiosyncratic</b> vitality of his mother’s voice.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
<li>
Eastern Europe’s largest democracy seemed to have been captured by a vengeful populist clique, with ideas about the outside world that ranged from the <b>idiosyncratic</b> to the unpleasant.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Then a paradoxical thing happened: Those small, scattered designs, by their very <b>idiosyncratic</b> strength, wound up becoming symbolic of the city and even Southern California as a whole.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
The country preferred <b>idiosyncratic</b>, homegrown games—baseball, basketball and American football—and, without the least embarrassment, pronounced its winners "world champions."
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/idiosyncratic/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='idiosyncratic#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='idio_own' data-tree-url='//cdn1.membean.com/public/data/treexml' href='idiosyncratic#'>
<span class=''></span>
idio-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>own, personal, private</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='syn_together' data-tree-url='//cdn1.membean.com/public/data/treexml' href='idiosyncratic#'>
<span class=''></span>
syn-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>together, with</td>
</tr>
<tr>
<td class='partform'>crat</td>
<td>
&rarr;
</td>
<td class='meaning'>a mixing</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ic_like' data-tree-url='//cdn1.membean.com/public/data/treexml' href='idiosyncratic#'>
<span class=''></span>
-ic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>like</td>
</tr>
</table>
<p><em>Idiosyncratic</em> ideas are one&#8217;s &#8220;own, personal, or private&#8221; views &#8220;mixed with&#8221; the general belief system; in other words, they are one&#8217;s &#8220;own&#8221; peculiar &#8220;mix&#8221; of traits.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='idiosyncratic#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>A Christmas Story</strong><span> This boy acts in a rather idiosyncratic fashion, which seems to delight his mother.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/idiosyncratic.jpg' video_url='examplevids/idiosyncratic' video_width='350'></span>
<div id='wt-container'>
<img alt="Idiosyncratic" height="288" src="https://cdn1.membean.com/video/examplevids/idiosyncratic.jpg" width="350" />
<div class='center'>
<a href="idiosyncratic#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='idiosyncratic#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Idiosyncratic" src="https://cdn1.membean.com/public/images/wordimages/cons2/idiosyncratic.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='idiosyncratic#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>anomaly</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>chimerical</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>eccentric</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>foible</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>heterodox</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>iconoclast</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>intrinsic</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>predilection</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>proclivity</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>propensity</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>clique</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>hackneyed</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>homogeneous</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>platitude</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>prosaic</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='idiosyncratic#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
idiosyncrasy
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>peculiarity; unusual characteristic</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="idiosyncratic" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>idiosyncratic</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="idiosyncratic#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

