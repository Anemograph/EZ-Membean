
<!DOCTYPE html>
<html>
<head>
<title>Word: surrogate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abjure a belief or a way of behaving, you state publicly that you will give it up or reject it.</p>
<p class='rw-defn idx1' style='display:none'>If someone in authority abolishes a law or practice, they formally put an end to it.</p>
<p class='rw-defn idx2' style='display:none'>To abrogate an act is to end it by official and sometimes legal means.</p>
<p class='rw-defn idx3' style='display:none'>An adjunct is something that is added to or joined to something else that is larger or more important.</p>
<p class='rw-defn idx4' style='display:none'>When you advocate a plan or action, you publicly push for implementing it.</p>
<p class='rw-defn idx5' style='display:none'>If one thing is analogous to another, a comparison can be made between the two because they are similar in some way.</p>
<p class='rw-defn idx6' style='display:none'>When you consign someone into another&#8217;s care, you sign them over or entrust them to that person&#8217;s protection.</p>
<p class='rw-defn idx7' style='display:none'>An emissary is someone who acts as a representative from one government or leader to another.</p>
<p class='rw-defn idx8' style='display:none'>If you describe something as ersatz, you dislike it because it is artificial or fake—and is used in place of a higher quality item.</p>
<p class='rw-defn idx9' style='display:none'>When you expedite an action or process, you make it happen more quickly.</p>
<p class='rw-defn idx10' style='display:none'>An exponent of a particular idea or cause is someone who supports or defends it.</p>
<p class='rw-defn idx11' style='display:none'>If you expunge something, you completely remove it or do away with it.</p>
<p class='rw-defn idx12' style='display:none'>If you extirpate something, you completely destroy it because it is unpleasant or unwanted.</p>
<p class='rw-defn idx13' style='display:none'>Someone, such as a performer or athlete, is inimitable when they are so good or unique in their talent that it is unlikely anyone else can be their equal.</p>
<p class='rw-defn idx14' style='display:none'>An interim position at a school or business is only temporary; it lasts until the position can be filled permanently.</p>
<p class='rw-defn idx15' style='display:none'>An irrevocable action or decision is impossible to change, reverse, or stop.</p>
<p class='rw-defn idx16' style='display:none'>The juxtaposition of two objects is the act of positioning them side by side so that the differences between them are more readily visible.</p>
<p class='rw-defn idx17' style='display:none'>A plenipotentiary is someone who has full power to take action or make decisions on behalf of their government in a foreign country.</p>
<p class='rw-defn idx18' style='display:none'>A provisional measure is temporary or conditional until more permanent action is taken.</p>
<p class='rw-defn idx19' style='display:none'>If you recant, you publicly announce that your once firmly held beliefs or statements were wrong and that you no longer agree with them.</p>
<p class='rw-defn idx20' style='display:none'>If you renege on a deal, agreement, or promise, you do not do what you promised or agreed to do.</p>
<p class='rw-defn idx21' style='display:none'>When someone in power rescinds a law or agreement, they officially end it and state that it is no longer valid.</p>
<p class='rw-defn idx22' style='display:none'>A vicarious pleasure or feeling is experienced by watching or reading about another person&#8217;s doing something rather than by doing it yourself.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>surrogate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='surrogate#' id='pronounce-sound' path='audio/words/amy-surrogate'></a>
SUR-uh-git
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='surrogate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='surrogate#' id='context-sound' path='audio/wordcontexts/brian-surrogate'></a>
When the sheriff in the isolated town died, the owner of the general store was appointed as his <em>surrogate</em> to temporarily replace him.  It was well known that Arthur could maintain the peace if needed, and the townspeople claimed that he would be the best man to serve as an alternate, <em>surrogate</em> sheriff until the new man arrived.  Arthur was reluctant to be that law-enforcing <em>surrogate</em> or substitute&#8212;he preferred groceries to guns.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When might someone need a <em>surrogate</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
When they are too sick to attend an important meeting and need someone else to go.
</li>
<li class='choice '>
<span class='result'></span>
When they are overwhelmed with work and need an assistant to help them. 
</li>
<li class='choice '>
<span class='result'></span>
When they need extra sleep because work has been difficult as of late.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='surrogate#' id='definition-sound' path='audio/wordmeanings/amy-surrogate'></a>
A <em>surrogate</em> is someone who temporarily takes the place of another person, usually acting as a representative because that person is not available to or cannot carry out a task.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>substitute</em>
</span>
</span>
</div>
<a class='quick-help' href='surrogate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='surrogate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/surrogate/memory_hooks/3832.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Sure</span></span>ly <span class="emp2"><span>Roga</span></span>ine</span></span> Since I have lost most of my hair, I now need <span class="emp1"><span>sur</span></span><span class="emp2"><span>roga</span></span>t<span class="emp1"><span>e</span></span> hair--<span class="emp1"><span>sure</span></span>ly <span class="emp2"><span>Roga</span></span>ine will give that <span class="emp1"><span>sur</span></span><span class="emp2"><span>roga</span></span>t<span class="emp1"><span>e</span></span> hair to me?
</p>
</div>

<div id='memhook-button-bar'>
<a href="surrogate#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/surrogate/memory_hooks">Use other hook</a>
<a href="surrogate#" id="memhook-use-own" url="https://membean.com/mywords/surrogate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='surrogate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Hewlett-Packard doesn’t consider it a robot, but a "<b>surrogate</b>," since it would have the face and the voice of the person who remotely controls it.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
While the prospect of a basketball game being played with only the sounds of the ball hitting the hardwood [and] sneakers squeaking is interesting in theory, it isn’t very exciting over the course of an entire game, let alone multiple games. Why not have one lone fan act as the <b>surrogate</b> for the entire fanbase?
<cite class='attribution'>
&mdash;
NBC Sports
</cite>
</li>
<li>
Over the years Patuxent scientists have studied and bred captive bald eagles, masked bobwhite quail and Andean condors (a <b>surrogate</b> for their endangered Californian relative).
<cite class='attribution'>
&mdash;
Capital Gazette
</cite>
</li>
<li>
[Jeb Bush Jr.] serves as a frequent travel companion and active campaign <b>surrogate</b> for his father, with a focus on building support among Hispanic and millennial voters. It is a role once played by Jeb Bush himself for his father, George H.W. Bush, and for his brother, George W. Bush.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/surrogate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='surrogate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='sur_under' data-tree-url='//cdn1.membean.com/public/data/treexml' href='surrogate#'>
<span class=''></span>
sur-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>under, instead of</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='rog_ask' data-tree-url='//cdn1.membean.com/public/data/treexml' href='surrogate#'>
<span class='common'></span>
rog
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>ask, request</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='surrogate#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>The idea behind <em>surrogate</em> is that one person is &#8220;asked&#8221; or &#8220;requested instead of&#8221; another person to do something, and so works &#8220;under&#8221; his or her power as a &#8220;substitute.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='surrogate#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Perfect Life</strong><span> Celebrities pair with their surrogates.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/surrogate.jpg' video_url='examplevids/surrogate' video_width='350'></span>
<div id='wt-container'>
<img alt="Surrogate" height="198" src="https://cdn1.membean.com/video/examplevids/surrogate.jpg" width="350" />
<div class='center'>
<a href="surrogate#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='surrogate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Surrogate" src="https://cdn3.membean.com/public/images/wordimages/cons2/surrogate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='surrogate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>adjunct</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>advocate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>analogous</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>consign</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>emissary</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>ersatz</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>expedite</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>exponent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>interim</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>juxtaposition</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>plenipotentiary</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>provisional</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>vicarious</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abjure</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abolish</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abrogate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>expunge</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>extirpate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>inimitable</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>irrevocable</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>recant</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>renege</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>rescind</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='surrogate#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
surrogate
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>substitute</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="surrogate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>surrogate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="surrogate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

