
<!DOCTYPE html>
<html>
<head>
<title>Word: abysmal | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>The word abject emphasizes a very bad situation or quality, thereby making it even worse.</p>
<p class='rw-defn idx1' style='display:none'>If you abominate something, you hate it because you think it is extremely wrong and unacceptable.</p>
<p class='rw-defn idx2' style='display:none'>When you ameliorate a bad condition or situation, you make it better in some way.</p>
<p class='rw-defn idx3' style='display:none'>Something that is apposite is relevant or suitable to what is happening or being discussed.</p>
<p class='rw-defn idx4' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx5' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx6' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx7' style='display:none'>A condign reward or punishment is deserved by and appropriate or fitting for the person who receives it.</p>
<p class='rw-defn idx8' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx9' style='display:none'>Depravity is behavior that is immoral, corrupt, or evil.</p>
<p class='rw-defn idx10' style='display:none'>If you deprecate something, you disapprove of it strongly.</p>
<p class='rw-defn idx11' style='display:none'>If you desecrate something that is considered holy or very special, you deliberately spoil or damage it.</p>
<p class='rw-defn idx12' style='display:none'>If you say a person&#8217;s actions are despicable, you think they are extremely unpleasant or nasty.</p>
<p class='rw-defn idx13' style='display:none'>If you are despondent, you are extremely unhappy because you are in an unpleasant situation that you do not think will improve.</p>
<p class='rw-defn idx14' style='display:none'>If you refer to the enormity of a situation, problem, or event, you mean that it is very evil or morally offensive; this word has nothing to do with physical size.</p>
<p class='rw-defn idx15' style='display:none'>If you extol something or someone, you praise it or them very enthusiastically.</p>
<p class='rw-defn idx16' style='display:none'>Something gargantuan is extremely large.</p>
<p class='rw-defn idx17' style='display:none'>A hapless person is unlucky, so much so that a stream of unfortunate things has happened and continues to happen to them.</p>
<p class='rw-defn idx18' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx19' style='display:none'>When someone is lionized, they are treated as being very important or famous—although they may not deserve to be.</p>
<p class='rw-defn idx20' style='display:none'>A malevolent person or thing is evil due to deliberate attempts to cause harm.</p>
<p class='rw-defn idx21' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx22' style='display:none'>If you mollify someone, you say or do something to make that person feel less angry or upset.</p>
<p class='rw-defn idx23' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx24' style='display:none'>If you describe people or things as odious, you think that they are extremely unpleasant.</p>
<p class='rw-defn idx25' style='display:none'>If something plummets, it falls down from a high position very quickly; for example, a piano can plummet from a window and a stock value can plummet during a market crash.</p>
<p class='rw-defn idx26' style='display:none'>Something that is pristine has not lost any of its original perfection or purity.</p>
<p class='rw-defn idx27' style='display:none'>Something that is prodigious is very large, impressive, or great.</p>
<p class='rw-defn idx28' style='display:none'>If something is requisite for a purpose, it is needed, appropriate, or necessary for that specific purpose.</p>
<p class='rw-defn idx29' style='display:none'>The word squalor describes very dirty and unpleasant conditions that people live or work in, usually due to poverty or neglect.</p>
<p class='rw-defn idx30' style='display:none'>When you are stultified by something, you lose interest in it because it is dull, boring, or time-consuming.</p>
<p class='rw-defn idx31' style='display:none'>A stupendous event or accomplishment is amazing, wonderful, or spectacular.</p>
<p class='rw-defn idx32' style='display:none'>To taint is to give an undesirable quality that damages a person&#8217;s reputation or otherwise spoils something.</p>
<p class='rw-defn idx33' style='display:none'>Something that is unsullied is unstained and clean.</p>
<p class='rw-defn idx34' style='display:none'>Venerable people command respect because they are old and wise.</p>
<p class='rw-defn idx35' style='display:none'>If someone vitiates something, they make it less effective, weaker, or reduced in quality.</p>
<p class='rw-defn idx36' style='display:none'>Vitriolic words are bitter, unkind, and mean-spirited.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>abysmal</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='abysmal#' id='pronounce-sound' path='audio/words/amy-abysmal'></a>
uh-BIZ-muhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='abysmal#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='abysmal#' id='context-sound' path='audio/wordcontexts/brian-abysmal'></a>
Having a miserable bout of the flu on her birthday was an <em>abysmal</em> occurrence for Olivia.  Not only did Olivia feel truly ill, but she also felt <em>abysmally</em> and completely sad since she knew her friends were planning a surprise party.  Sitting on her couch with a cup of herbal tea and a box of tissues, she considered the deep, dark <em>abyss</em> or deep pit of her foul mood.  There seemed no way to brighten her <em>abysmal</em> and extremely poor outlook on this disappointing birthday.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an <em>abysmal</em> play performance?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
One that is horribly awful.
</li>
<li class='choice '>
<span class='result'></span>
One that is relatively unknown.
</li>
<li class='choice '>
<span class='result'></span>
One that is cancelled suddenly.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='abysmal#' id='definition-sound' path='audio/wordmeanings/amy-abysmal'></a>
A situation or condition that is <em>abysmal</em> is extremely bad or of wretched quality.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>awful</em>
</span>
</span>
</div>
<a class='quick-help' href='abysmal#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='abysmal#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/abysmal/memory_hooks/5950.json'></span>
<p>
<img alt="Abysmal" src="https://cdn3.membean.com/public/images/wordimages/hook/abysmal.jpg?qdep8" />
<span class="emp0"><span>Ab<span class="emp3"><span>ysmal</span></span> and D<span class="emp3"><span>ismal</span></span></span></span>  Both ab<span class="emp3"><span>ysmal</span></span> and d<span class="emp3"><span>ismal</span></span>--what could be worse than that?
</p>
</div>

<div id='memhook-button-bar'>
<a href="abysmal#" id="add-public-hook" style="" url="https://membean.com/mywords/abysmal/memory_hooks">Use other public hook</a>
<a href="abysmal#" id="memhook-use-own" url="https://membean.com/mywords/abysmal/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='abysmal#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
America’s <b>abysmal</b> failure in Iraq has brought about the collapse of a stunningly near-sighted movement, a movement which had the arrogance to assert that the Middle East would welcome American-style democracy with open arms.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
In spring training, the Mariners had two catchers trying to bounce back from the worst seasons of their careers, but with the odd juxtaposition that one was brought into replace the other. It was a calculated move by general manager Jerry Dipoto to address the Mariners’ <b>abysmal</b> situation at the position.
<cite class='attribution'>
&mdash;
The Seattle Times
</cite>
</li>
<li>
“The alert system failed, period—no ifs, ands or buts,” said Gary Kozel, who lives in the small town of Kenwood, which was devastated by the Nuns Fire. “I’m really hopeful the county will take the report seriously and will take the measures necessary to remedy what was an <b>abysmal</b> situation.”
<cite class='attribution'>
&mdash;
San Francisco Chronicle
</cite>
</li>
<li>
Human Rights Watch/Africa—hardly a water carrier for U.S. policy—had recently labeled Khartoum’s human rights record as "<b>abysmal</b>," and reported that "all forms of political opposition remain banned both legally and through systematic terror."
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/abysmal/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='abysmal#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='a_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='abysmal#'>
<span class=''></span>
a-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not, without</td>
</tr>
<tr>
<td class='partform'>bysm</td>
<td>
&rarr;
</td>
<td class='meaning'>bottom</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='al_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='abysmal#'>
<span class=''></span>
-al
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>An <em>abysmal</em> mood is so dark and depressing that it seems &#8220;without a bottom.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='abysmal#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Scrubs</strong><span> This doctor's explanation of how to recognize a heart murmur was apparently abysmal.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/abysmal.jpg' video_url='examplevids/abysmal' video_width='350'></span>
<div id='wt-container'>
<img alt="Abysmal" height="288" src="https://cdn1.membean.com/video/examplevids/abysmal.jpg" width="350" />
<div class='center'>
<a href="abysmal#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='abysmal#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Abysmal" src="https://cdn1.membean.com/public/images/wordimages/cons2/abysmal.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='abysmal#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abject</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abominate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>depravity</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>deprecate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>desecrate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>despicable</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>despondent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>enormity</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>gargantuan</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>hapless</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>malevolent</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>odious</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>plummet</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>prodigious</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>squalor</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>stultify</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>taint</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>vitiate</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>vitriolic</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>ameliorate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>apposite</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>condign</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>extol</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>lionize</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>mollify</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>pristine</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>requisite</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>stupendous</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>unsullied</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>venerable</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='abysmal#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
abyss
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>a seemingly bottomless pit</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="abysmal" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>abysmal</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="abysmal#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

