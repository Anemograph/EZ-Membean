
<!DOCTYPE html>
<html>
<head>
<title>Word: unsurpassed | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>The acme of something is its highest point of achievement or excellence.</p>
<p class='rw-defn idx1' style='display:none'>When you are adept at something, you are very good, skillful, or talented at it.</p>
<p class='rw-defn idx2' style='display:none'>Someone who is adroit is skillful and clever, especially in thought, behavior, or action.</p>
<p class='rw-defn idx3' style='display:none'>The apex of anything, such as an organization or system, is its highest part or most important position.</p>
<p class='rw-defn idx4' style='display:none'>The apogee of something is its highest or greatest point, especially in reference to a culture or career.</p>
<p class='rw-defn idx5' style='display:none'>The best or perfect example of something is its apotheosis; likewise, this word can be used to indicate the best or highest point in someone&#8217;s life or job.</p>
<p class='rw-defn idx6' style='display:none'>An atrocious deed is outrageously bad, extremely evil, or shocking in its wrongness.</p>
<p class='rw-defn idx7' style='display:none'>A bravura performance, such as of a highly technical work for the violin, is done with consummate skill.</p>
<p class='rw-defn idx8' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx9' style='display:none'>A definitive opinion on an issue cannot be challenged; therefore, it is the final word or the most authoritative pronouncement on that issue.</p>
<p class='rw-defn idx10' style='display:none'>A deft movement or action is made quickly and with skill.</p>
<p class='rw-defn idx11' style='display:none'>If you say a person&#8217;s actions are despicable, you think they are extremely unpleasant or nasty.</p>
<p class='rw-defn idx12' style='display:none'>A dilettante is someone who frequently pursues an interest in a subject; nevertheless, they never spend the time and effort to study it thoroughly enough to master it.</p>
<p class='rw-defn idx13' style='display:none'>When one person eclipses another&#8217;s achievements, they surpass or outshine that person in that endeavor.</p>
<p class='rw-defn idx14' style='display:none'>If you describe a person, group, or civilization as effete, you mean it is weak, exhausted,  powerless, unproductive, and/or corrupt.</p>
<p class='rw-defn idx15' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx16' style='display:none'>If you describe someone&#8217;s appearance or behavior as impeccable, you mean that it is perfect and therefore impossible to criticize.</p>
<p class='rw-defn idx17' style='display:none'>An indomitable foe cannot be beaten.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is ineffectual at a task is useless or ineffective when attempting to do it.</p>
<p class='rw-defn idx19' style='display:none'>Someone who is inept is unable or unsuitable to do a job; hence, they are unfit for it.</p>
<p class='rw-defn idx20' style='display:none'>Something that has inestimable value or benefit has so much of it that it cannot be calculated.</p>
<p class='rw-defn idx21' style='display:none'>If something is infallible, it is never wrong and so is incapable of making mistakes.</p>
<p class='rw-defn idx22' style='display:none'>Someone, such as a performer or athlete, is inimitable when they are so good or unique in their talent that it is unlikely anyone else can be their equal.</p>
<p class='rw-defn idx23' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx24' style='display:none'>A laudatory article or speech expresses praise or admiration for someone or something.</p>
<p class='rw-defn idx25' style='display:none'>Something mediocre is average or ordinary in quality; it’s just OK.</p>
<p class='rw-defn idx26' style='display:none'>Someone who is meritorious is worthy of receiving recognition or is praiseworthy because of what they have accomplished.</p>
<p class='rw-defn idx27' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx28' style='display:none'>The nadir of a situation is its lowest point.</p>
<p class='rw-defn idx29' style='display:none'>Something nonpareil has no equal because it is much better than all others of its kind or type.</p>
<p class='rw-defn idx30' style='display:none'>An omnipotent being or entity is unlimited in its power.</p>
<p class='rw-defn idx31' style='display:none'>A paragon is an example of a thing or person at its or their very best.</p>
<p class='rw-defn idx32' style='display:none'>If someone reaches a pinnacle of something—such as a career or mountain—they have arrived at the highest point of it.</p>
<p class='rw-defn idx33' style='display:none'>Something that is prodigious is very large, impressive, or great.</p>
<p class='rw-defn idx34' style='display:none'>Something prosaic is dull, boring, and ordinary.</p>
<p class='rw-defn idx35' style='display:none'>If you have prowess, you possess considerable skill or ability in something.</p>
<p class='rw-defn idx36' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>
<p class='rw-defn idx37' style='display:none'>A superlative deed or act is excellent, outstanding, or the best of its kind.</p>
<p class='rw-defn idx38' style='display:none'>An unparalleled accomplishment has not been equaled by anyone or cannot be compared to anything that anyone else has ever done.</p>
<p class='rw-defn idx39' style='display:none'>Something unprecedented has never occurred; therefore, it is unusual, original, or new.</p>
<p class='rw-defn idx40' style='display:none'>Something in the vanguard is in the leading spot of something, such as an army or institution.</p>
<p class='rw-defn idx41' style='display:none'>Venerable people command respect because they are old and wise.</p>
<p class='rw-defn idx42' style='display:none'>Something vintage is the best of its kind and of excellent quality; it is often of a past time and is considered a classic example of its type.</p>
<p class='rw-defn idx43' style='display:none'>A virtuoso is someone who is very skillful at something, especially playing a musical instrument.</p>
<p class='rw-defn idx44' style='display:none'>The zenith of something is its highest, most powerful, or most successful point.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>unsurpassed</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='unsurpassed#' id='pronounce-sound' path='audio/words/amy-unsurpassed'></a>
uhn-ser-PASD
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='unsurpassed#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='unsurpassed#' id='context-sound' path='audio/wordcontexts/brian-unsurpassed'></a>
The <em>unsurpassed</em> film won every award, for nobody could beat this matchless movie in any Oscar category.  The scenery was <em>unsurpassed</em>; nothing could touch its quality or breathtaking views.  The film also had an <em>unsurpassed</em> budget as no movie had ever cost that much to make before.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If something has <em>unsurpassed</em> quality, what does it have?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It has very low quality because it is not made well.
</li>
<li class='choice answer '>
<span class='result'></span>
It has the highest quality known.
</li>
<li class='choice '>
<span class='result'></span>
It has excellent quality.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='unsurpassed#' id='definition-sound' path='audio/wordmeanings/amy-unsurpassed'></a>
If you are <em>unsurpassed</em> in what you do, you are the best—period.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>best</em>
</span>
</span>
</div>
<a class='quick-help' href='unsurpassed#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='unsurpassed#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/unsurpassed/memory_hooks/6300.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Sur</span></span>e to <span class="emp3"><span>Pass</span></span></span></span> I am <span class="emp1"><span>sur</span></span>e to <span class="emp3"><span>pass</span></span> you in the marathon because I'm un<span class="emp1"><span>sur</span></span><span class="emp3"><span>pass</span></span>ed in my ability to run fast and far.
</p>
</div>

<div id='memhook-button-bar'>
<a href="unsurpassed#" id="add-public-hook" style="" url="https://membean.com/mywords/unsurpassed/memory_hooks">Use other public hook</a>
<a href="unsurpassed#" id="memhook-use-own" url="https://membean.com/mywords/unsurpassed/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='unsurpassed#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Surely his record for versatility, studied characterization—ranging from modern colloquial to the classics—and artistic integrity is <b>unsurpassed</b>.
<span class='attribution'>&mdash; Douglas Fairbanks, Jr., American actor and producer</span>
<img alt="Douglas fairbanks, jr" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Douglas Fairbanks, Jr., American actor and producer.jpg?qdep8" width="80" />
</li>
<li>
Stevie’s Sixties hits are amazing—joyful music that still sounds great—but then, starting in the Seventies, he hit a run of albums that’s <b>unsurpassed</b> in music history, from Talking Book to Songs in the Key of Life.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
<li>
The United States, with its monopoly of the atom bomb and its <b>unsurpassed</b> industrial establishments, was at the zenith of its military power.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
The brilliant red of currant jelly is <b>unsurpassed</b> as a complement to most meats, and its tartness is a refreshing change from other fruits.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/unsurpassed/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='unsurpassed#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='un_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='unsurpassed#'>
<span class='common'></span>
un-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not, opposite of</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='sur_over' data-tree-url='//cdn1.membean.com/public/data/treexml' href='unsurpassed#'>
<span class=''></span>
sur-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>over, above</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pass_step' data-tree-url='//cdn1.membean.com/public/data/treexml' href='unsurpassed#'>
<span class='common'></span>
pass
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>step, pace</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ed_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='unsurpassed#'>
<span class=''></span>
-ed
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>having a particular state</td>
</tr>
</table>
<p>An <em>unsurpassed</em> record has &#8220;not been stepped over&#8221; by anyone else.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='unsurpassed#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Breakfast of Champions</strong><span> They are selling a car that gives the owner unsurpassed driving excitement.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/unsurpassed.jpg' video_url='examplevids/unsurpassed' video_width='350'></span>
<div id='wt-container'>
<img alt="Unsurpassed" height="288" src="https://cdn1.membean.com/video/examplevids/unsurpassed.jpg" width="350" />
<div class='center'>
<a href="unsurpassed#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='unsurpassed#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Unsurpassed" src="https://cdn0.membean.com/public/images/wordimages/cons2/unsurpassed.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='unsurpassed#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>acme</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>adept</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>adroit</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>apex</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>apogee</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>apotheosis</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>bravura</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>definitive</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>deft</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>eclipse</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>impeccable</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>indomitable</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>inestimable</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>infallible</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>inimitable</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>laudatory</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>meritorious</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>nonpareil</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>omnipotent</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>paragon</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>pinnacle</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>prodigious</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>prowess</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>superlative</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>unparalleled</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>unprecedented</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>vanguard</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>venerable</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>vintage</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>virtuoso</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>zenith</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='6' class = 'rw-wordform notlearned'><span>atrocious</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>despicable</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>dilettante</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>effete</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>ineffectual</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>inept</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>mediocre</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>nadir</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>prosaic</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="unsurpassed" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>unsurpassed</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="unsurpassed#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

