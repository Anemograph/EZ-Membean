
<!DOCTYPE html>
<html>
<head>
<title>Word: extirpate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If someone in authority abolishes a law or practice, they formally put an end to it.</p>
<p class='rw-defn idx1' style='display:none'>An addendum is an additional section added to the end of a book, document, or speech that gives more information.</p>
<p class='rw-defn idx2' style='display:none'>An adjunct is something that is added to or joined to something else that is larger or more important.</p>
<p class='rw-defn idx3' style='display:none'>Attrition is the process of gradually decreasing the strength of something (such as an enemy force) by continually attacking it.</p>
<p class='rw-defn idx4' style='display:none'>To augment something is to increase its value or effectiveness by adding something to it.</p>
<p class='rw-defn idx5' style='display:none'>When you beget something, you cause it to happen or create it.</p>
<p class='rw-defn idx6' style='display:none'>To bowdlerize a book, play, or other literary work is to remove parts of it that are considered indecent or unsuitable for family reading.</p>
<p class='rw-defn idx7' style='display:none'>Censure is written or verbal disapproval and harsh criticism of something that someone has done.</p>
<p class='rw-defn idx8' style='display:none'>If you cull items or information, you gather them from a number of different places in a selective manner.</p>
<p class='rw-defn idx9' style='display:none'>If you curtail something, you reduce or limit it.</p>
<p class='rw-defn idx10' style='display:none'>If you decimate something, you destroy a large part of it, reducing its size and effectiveness greatly.</p>
<p class='rw-defn idx11' style='display:none'>Someone defoliates a tree or plant by removing its leaves, usually by applying a chemical agent.</p>
<p class='rw-defn idx12' style='display:none'>To denude an area is to remove the plants and trees that cover it; it can also mean to make something bare.</p>
<p class='rw-defn idx13' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx14' style='display:none'>When you dismantle something, you take it apart or destroy it piece by piece. </p>
<p class='rw-defn idx15' style='display:none'>To efface something is to erase or remove it completely from recognition or memory.</p>
<p class='rw-defn idx16' style='display:none'>If something engenders a particular feeling or attitude, it causes that condition.</p>
<p class='rw-defn idx17' style='display:none'>If you ensconce yourself somewhere, you put yourself into a comfortable place or safe position and have no intention of moving or leaving for quite some time.</p>
<p class='rw-defn idx18' style='display:none'>When you eradicate something, you tear it up by the roots or remove it completely.</p>
<p class='rw-defn idx19' style='display:none'>When you excise something, you remove it by cutting it out.</p>
<p class='rw-defn idx20' style='display:none'>To expurgate part of a book, play, or other text is to remove parts of it before publishing because they are considered objectionable, harmful, or offensive.</p>
<p class='rw-defn idx21' style='display:none'>When you extricate yourself from a difficult or unpleasant situation, you manage to escape it.</p>
<p class='rw-defn idx22' style='display:none'>Gestation is the process by which a new idea,  plan, or piece of work develops in your mind.</p>
<p class='rw-defn idx23' style='display:none'>Something that is incessant continues on for a long time without stopping.</p>
<p class='rw-defn idx24' style='display:none'>To induce a state is to bring it about; to induce someone to do something is to persuade them to do it.</p>
<p class='rw-defn idx25' style='display:none'>An infusion is the pouring in or the introduction of something into something else so as to fill it up.</p>
<p class='rw-defn idx26' style='display:none'>Something that has been ingrained in your mind has been fixed or rooted there permanently.</p>
<p class='rw-defn idx27' style='display:none'>An inherent characteristic is one that exists in a person at birth or in a thing naturally.</p>
<p class='rw-defn idx28' style='display:none'>An innate quality of someone is present since birth or is a quality of something that is essential to it.</p>
<p class='rw-defn idx29' style='display:none'>Something that is interminable continues for a very long time in a boring or annoying way.</p>
<p class='rw-defn idx30' style='display:none'>To liquidate a business or company is to close it down and sell the things that belong to it in order to pay off its debts.</p>
<p class='rw-defn idx31' style='display:none'>Something luxuriant, such as plants or hair, is growing well and is very healthy.</p>
<p class='rw-defn idx32' style='display:none'>To nullify something is to cancel or negate it.</p>
<p class='rw-defn idx33' style='display:none'>When you obliterate something, you destroy it to such an extent that there is nothing or very little of it left.</p>
<p class='rw-defn idx34' style='display:none'>To pare something down is to reduce or lessen it.</p>
<p class='rw-defn idx35' style='display:none'>When you perpetuate something, you keep it going or continue it indefinitely.</p>
<p class='rw-defn idx36' style='display:none'>To purge something is to get rid of or remove it.</p>
<p class='rw-defn idx37' style='display:none'>When you ravage something, you completely destroy, wreck, or damage it.</p>
<p class='rw-defn idx38' style='display:none'>When someone in power rescinds a law or agreement, they officially end it and state that it is no longer valid.</p>
<p class='rw-defn idx39' style='display:none'>If you truncate something, you make it shorter or quicker by removing or cutting off part of it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>extirpate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='extirpate#' id='pronounce-sound' path='audio/words/amy-extirpate'></a>
EK-ster-payt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='extirpate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='extirpate#' id='context-sound' path='audio/wordcontexts/brian-extirpate'></a>
Before the realtor could sell the house, she had to make certain that all signs of mice were completely <em>extirpated</em> or destroyed.  She removed the mousetraps from the corners of each room, made sure the space was clean of mouse droppings, and <em>extirpated</em> or eliminated the empty nest beneath the kitchen cupboard.  It was more difficult, however, to completely get rid of or <em>extirpate</em> the unpleasant smell that the rodents had left behind.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of <em>extirpating</em> something?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Selling a painting that has increased in value since you bought it.
</li>
<li class='choice answer '>
<span class='result'></span>
Spraying a yard for weeds to get rid of them.
</li>
<li class='choice '>
<span class='result'></span>
Repairing a broken heel on a shoe so you can keep wearing it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='extirpate#' id='definition-sound' path='audio/wordmeanings/amy-extirpate'></a>
If you <em>extirpate</em> something, you completely destroy it because it is unpleasant or unwanted.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>destroy</em>
</span>
</span>
</div>
<a class='quick-help' href='extirpate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='extirpate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/extirpate/memory_hooks/4730.json'></span>
<p>
<span class="emp0"><span>S<span class="emp2"><span>tir</span></span> <span class="emp3"><span>Pat</span></span> in V<span class="emp3"><span>at</span></span></span></span> The psycho<span class="emp3"><span>pat</span></span>hic killer ex<span class="emp2"><span>tir</span></span><span class="emp3"><span>pat</span></span>ed <span class="emp3"><span>Pat</span></span> by s<span class="emp2"><span>tir</span></span>ring <span class="emp3"><span>Pat</span></span> into a big v<span class="emp3"><span>at</span></span> of chocolate fudge that chopped <span class="emp3"><span>Pat</span></span> up into little tiny bits to be later consumed by unwary fudge lovers.
</p>
</div>

<div id='memhook-button-bar'>
<a href="extirpate#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/extirpate/memory_hooks">Use other hook</a>
<a href="extirpate#" id="memhook-use-own" url="https://membean.com/mywords/extirpate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='extirpate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
I wish you could simply <b>extirpate</b> violence and war from the world, abolish all the armed forces, and destroy all the bombs. But this is probably not very realistic. Ultimately, everyone has to start with themselves.
<span class='attribution'>&mdash; Jón Gnarr, former mayor of Reykjavík, Iceland</span>
<img alt="Jón gnarr, former mayor of reykjavík, iceland" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Jón Gnarr, former mayor of Reykjavík, Iceland.jpg?qdep8" width="80" />
</li>
<li>
It is not possible to <b>extirpate</b> terrorism from the face of the globe, but it should be possible to reduce the incidence and effectiveness of terrorism, through coordinated international action.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
Someone’s always in the kitchen at the last minute, punishing the spuds with an electric mixer, trying to <b>extirpate</b> the dreaded lumps, so it resembles flavorless Cool Whip. Heck, you can put some sugar in the potatoes and use them for pie topping; who’d know?
<cite class='attribution'>
&mdash;
Minneapolis Star Tribune
</cite>
</li>
<li>
[Our] actions right now during fall cleanup can have lasting impacts on the wildlife in our communities. As we neatly clean our landscape for winter, we are unknowingly removing highly valuable habitat for overwintering insects. . . . A number of moths and butterflies overwinter in a chrysalis buried in leaves. In fact, some chrysalises mimic the appearance of leaves, making them almost indistinguishable. So, leaf removal or burning can directly <b>extirpate</b> these species from your yard.
<cite class='attribution'>
&mdash;
Champagne Urbana News-Gazette
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/extirpate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='extirpate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ex_out' data-tree-url='//cdn1.membean.com/public/data/treexml' href='extirpate#'>
<span class='common'></span>
ex-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>out of, from, off</td>
</tr>
<tr>
<td class='partform'>tirp</td>
<td>
&rarr;
</td>
<td class='meaning'>stem, stock, root</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ate_quality' data-tree-url='//cdn1.membean.com/public/data/treexml' href='extirpate#'>
<span class=''></span>
-ate
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make something have a certain quality</td>
</tr>
</table>
<p>To <em>extirpate</em> something is to pull it &#8220;from the stem,&#8221; or &#8220;off (by) the root&#8221; in order to fully get rid of it.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='extirpate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Extirpate" src="https://cdn0.membean.com/public/images/wordimages/cons2/extirpate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='extirpate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abolish</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>attrition</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>bowdlerize</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>censure</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>cull</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>curtail</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>decimate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>defoliate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>denude</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>dismantle</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>efface</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>eradicate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>excise</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>expurgate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>extricate</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>liquidate</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>nullify</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>obliterate</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>pare</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>purge</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>ravage</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>rescind</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>truncate</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>addendum</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>adjunct</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>augment</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>beget</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>engender</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>ensconce</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>gestation</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>incessant</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>induce</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>infusion</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>ingrained</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>inherent</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>innate</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>interminable</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>luxuriant</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>perpetuate</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="extirpate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>extirpate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="extirpate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

