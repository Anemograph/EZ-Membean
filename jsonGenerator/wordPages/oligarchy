
<!DOCTYPE html>
<html>
<head>
<title>Word: oligarchy | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>A cavalier person does not seem to care about rules, principles, or other people&#8217;s feelings, no matter how dire or serious a situation may be.</p>
<p class='rw-defn idx1' style='display:none'>A despot is a leader who has a lot of power and uses it in a cruel and unreasonable way.</p>
<p class='rw-defn idx2' style='display:none'>An echelon is one level of status or rank in an organization.</p>
<p class='rw-defn idx3' style='display:none'>A small group or issue that is factious is controversial and promotes strong disagreement or dissatisfaction within a larger group.</p>
<p class='rw-defn idx4' style='display:none'>A fiat is an official order from a person or group in authority.</p>
<p class='rw-defn idx5' style='display:none'>If something is formidable, it is impressive in size, power, or skill; therefore, it can make you feel frightened, alarmed, or in awe because it is so powerful or difficult to deal with.</p>
<p class='rw-defn idx6' style='display:none'>A hierarchy is a system of organization in a society, company, or other group in which people are divided into different ranks or levels of importance.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is imperious behaves in a proud, overbearing, and highly confident manner that shows they expect to be obeyed without question.</p>
<p class='rw-defn idx8' style='display:none'>A martinet is a very strict person who demands that people obey rules exactly.</p>
<p class='rw-defn idx9' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx10' style='display:none'>If you are oppressed by someone or something, you are beaten down, troubled, or burdened by them or it.</p>
<p class='rw-defn idx11' style='display:none'>Someone is overweening when they are not modest; rather, they think way too much of themselves and let everyone know about it.</p>
<p class='rw-defn idx12' style='display:none'>A potentate is a ruler who has great power over people.</p>
<p class='rw-defn idx13' style='display:none'>A regime is the system of government currently in power in a country; it can also be the controlling group or management of an organization.</p>
<p class='rw-defn idx14' style='display:none'>If you behave in a supercilious way, you act as if you were more important or better than everyone else.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 6
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>oligarchy</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='oligarchy#' id='pronounce-sound' path='audio/words/amy-oligarchy'></a>
OL-i-gahr-kee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='oligarchy#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='oligarchy#' id='context-sound' path='audio/wordcontexts/brian-oligarchy'></a>
We are all so tired of the <em>oligarchy</em> or small group of leaders who run our country.  This <em>oligarchy</em>, made up of just ten of the most powerful people in our land, always makes laws that solely benefit themselves, not considering the people they govern at all.  We have tried to overthrow this <em>oligarchy</em> or rule by the privileged few a number of times, but they just have too much wealth and influence for us to get any traction or success in that endeavor.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an oligarchy?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is the people who are ruled by a relatively small group of powerful men.
</li>
<li class='choice answer '>
<span class='result'></span>
It is a government which is run by a small number of strong and controlling people.
</li>
<li class='choice '>
<span class='result'></span>
It is a system of government that runs a small country.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='oligarchy#' id='definition-sound' path='audio/wordmeanings/amy-oligarchy'></a>
An <em>oligarchy</em> is a government that is run by a small group of highly influential and powerful people.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>ruling group</em>
</span>
</span>
</div>
<a class='quick-help' href='oligarchy#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='oligarchy#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/oligarchy/memory_hooks/116628.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Ol</span></span>e, <span class="emp2"><span>Ig</span></span>gy, and <span class="emp3"><span>Archi</span></span>e</span></span> Yes siree Bob, our country is run by the elite <span class="emp1"><span>ol</span></span><span class="emp2"><span>ig</span></span><span class="emp3"><span>archy</span></span> of <span class="emp1"><span>Ol</span></span>e, <span class="emp2"><span>Ig</span></span>gy, and <span class="emp3"><span>Archi</span></span>e.
</p>
</div>

<div id='memhook-button-bar'>
<a href="oligarchy#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/oligarchy/memory_hooks">Use other hook</a>
<a href="oligarchy#" id="memhook-use-own" url="https://membean.com/mywords/oligarchy/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='oligarchy#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
The tyranny of a prince in an <b>oligarchy</b> is not so dangerous to the public welfare as the apathy of a citizen in a democracy.
<span class='attribution'>&mdash; Charles de Montesquieu, Eighteenth century French philosopher</span>
<img alt="Charles de montesquieu, eighteenth century french philosopher" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Charles de Montesquieu, Eighteenth century French philosopher.jpg?qdep8" width="80" />
</li>
<li>
Bruce Springsteen's 17th album is rock's most pointed response to the Great Recession: a song suite explicitly for the 99 percent, as largehearted, and as righteously wrathful, as any album he's made. _Wrecking Ball_ rages at corporate <b>oligarchy</b> and economic injustice.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
<li>
What was once universally recognized as a nation of shared prosperity has virtually overnight become an <b>oligarchy</b>.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/oligarchy/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='oligarchy#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>olig-</td>
<td>
&rarr;
</td>
<td class='meaning'>few</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='arch_rule' data-tree-url='//cdn1.membean.com/public/data/treexml' href='oligarchy#'>
<span class=''></span>
arch
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>rule</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='y_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='oligarchy#'>
<span class=''></span>
-y
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>body, group, collection</td>
</tr>
</table>
<p>An <em>oligarchy</em> is a &#8220;group of few rulers&#8221; that govern a country.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='oligarchy#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Oligarchy" src="https://cdn3.membean.com/public/images/wordimages/cons2/oligarchy.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='oligarchy#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>cavalier</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>despot</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>echelon</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>factious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>fiat</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>formidable</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>hierarchy</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>imperious</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>martinet</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>oppress</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>overweening</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>potentate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>regime</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>supercilious</span> &middot;</li></ul>
<ul class='related-ants'></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="oligarchy" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>oligarchy</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="oligarchy#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

