
<!DOCTYPE html>
<html>
<head>
<title>Word: squalor | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Squalor-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/squalor-large.jpg?qdep8" />
</div>
<a href="squalor#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>The word abject emphasizes a very bad situation or quality, thereby making it even worse.</p>
<p class='rw-defn idx1' style='display:none'>When you perform your ablutions, you wash yourself; this can be part of a religious ceremony as well.</p>
<p class='rw-defn idx2' style='display:none'>Affluence is the state of having a lot of money and many possessions, granting one a high economic standard of living.</p>
<p class='rw-defn idx3' style='display:none'>When you bedeck something, you decorate it in a showy way.</p>
<p class='rw-defn idx4' style='display:none'>Something is a blight if it spoils or damages things severely; blight is often used to describe something that ruins or kills crops.</p>
<p class='rw-defn idx5' style='display:none'>A man who is dapper has a very neat and clean appearance; he is both fashionable and stylish.</p>
<p class='rw-defn idx6' style='display:none'>When there is a dearth of something, there is a scarcity or lack of it.</p>
<p class='rw-defn idx7' style='display:none'>A man who is debonair wears fashionable clothes and is sophisticated, charming, friendly, and confident.</p>
<p class='rw-defn idx8' style='display:none'>Decrepitude is the state of being very old, worn out, or very ill; therefore, something or someone is no longer in good physical condition or good health.</p>
<p class='rw-defn idx9' style='display:none'>Something, such as a building, is derelict if it is empty, not used, and in bad condition or disrepair.</p>
<p class='rw-defn idx10' style='display:none'>If you say a person&#8217;s actions are despicable, you think they are extremely unpleasant or nasty.</p>
<p class='rw-defn idx11' style='display:none'>A dilapidated building, vehicle, etc. is old, broken-down, and in very bad condition.</p>
<p class='rw-defn idx12' style='display:none'>If you refer to the enormity of a situation, problem, or event, you mean that it is very evil or morally offensive; this word has nothing to do with physical size.</p>
<p class='rw-defn idx13' style='display:none'>Fetid water or air has a bad odor, usually caused by decay.</p>
<p class='rw-defn idx14' style='display:none'>Something that is immaculate is very clean, pure, or completely free from error.</p>
<p class='rw-defn idx15' style='display:none'>Someone who is impecunious has very little money, especially over a long period of time.</p>
<p class='rw-defn idx16' style='display:none'>An impoverished person or nation is very poor and stricken by poverty.</p>
<p class='rw-defn idx17' style='display:none'>An indigent person is very poor.</p>
<p class='rw-defn idx18' style='display:none'>Something that has inestimable value or benefit has so much of it that it cannot be calculated.</p>
<p class='rw-defn idx19' style='display:none'>Someone, such as a performer or athlete, is inimitable when they are so good or unique in their talent that it is unlikely anyone else can be their equal.</p>
<p class='rw-defn idx20' style='display:none'>Something luxuriant, such as plants or hair, is growing well and is very healthy.</p>
<p class='rw-defn idx21' style='display:none'>If something is noisome, it is extremely unpleasant, especially because it is very dirty or has an awful smell.</p>
<p class='rw-defn idx22' style='display:none'>Something that is noxious is harmful, extremely unpleasant, and often poisonous.</p>
<p class='rw-defn idx23' style='display:none'>If you describe people or things as odious, you think that they are extremely unpleasant.</p>
<p class='rw-defn idx24' style='display:none'>Something that is opulent is very impressive, grand, and is made from expensive materials.</p>
<p class='rw-defn idx25' style='display:none'>A paucity of something is not enough of it.</p>
<p class='rw-defn idx26' style='display:none'>Pecuniary means relating to or concerning money.</p>
<p class='rw-defn idx27' style='display:none'>Penury is the state of being extremely poor.</p>
<p class='rw-defn idx28' style='display:none'>Something that is pristine has not lost any of its original perfection or purity.</p>
<p class='rw-defn idx29' style='display:none'>If you suffer privation, you live without many of the basic things required for a comfortable life.</p>
<p class='rw-defn idx30' style='display:none'>To purge something is to get rid of or remove it.</p>
<p class='rw-defn idx31' style='display:none'>If you think a type of behavior or idea is reprehensible, you think that it is very bad, morally wrong, and deserves to be strongly criticized.</p>
<p class='rw-defn idx32' style='display:none'>People or things that are resplendent are beautiful, bright, and impressive in appearance.</p>
<p class='rw-defn idx33' style='display:none'>A salubrious place or area is pleasant, clean, healthy, and comfortable to live in.</p>
<p class='rw-defn idx34' style='display:none'>A salutary experience is beneficial to you since it strengthens you in some way, although it may be unpleasant as you undergo it; this word also refers to promoting good health.</p>
<p class='rw-defn idx35' style='display:none'>A slovenly person is untidy or messy.</p>
<p class='rw-defn idx36' style='display:none'>An environment or character can be sordid—the former dirty, the latter low or base in an immoral or greedy sort of way.</p>
<p class='rw-defn idx37' style='display:none'>To act in an uncouth manner is to be awkward and unmannerly.</p>
<p class='rw-defn idx38' style='display:none'>An unkempt person or thing is untidy and has not been kept neat.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>squalor</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='squalor#' id='pronounce-sound' path='audio/words/amy-squalor'></a>
SKWOL-er
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='squalor#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='squalor#' id='context-sound' path='audio/wordcontexts/brian-squalor'></a>
Mill workers of 19th-century England often lived in harsh, unhealthy situations, full of <em>squalor</em> or dirtiness.  Crowded lodgings held large, barely-nourished families whose lives included disease, hunger, and filthy surroundings&#8212;all the conditions that go with <em>squalor</em>.  Some mill owners addressed the <em>squalor</em> or unhealthy living conditions of these workers by paying livable wages, while others avoided the desperation around them by merely hiring new workers when other wretched employees died.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Which of the following quotes describes someone&#8217;s living in <em>squalor</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
“When I was poor living in a garage in Kansas I began to draw the mice that scampered over my desk.&quot;—Walt  Disney
</li>
<li class='choice '>
<span class='result'></span>
&#8220;We must learn to live together as brothers or perish together as fools.&#8221;—Martin  Luther King, Jr.
</li>
<li class='choice '>
<span class='result'></span>
“I live in that solitude which is painful in youth, but delicious in the years of maturity.”—Albert Einstein
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='squalor#' id='definition-sound' path='audio/wordmeanings/amy-squalor'></a>
The word <em>squalor</em> describes very dirty and unpleasant conditions that people live or work in, usually due to poverty or neglect.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>dirtiness</em>
</span>
</span>
</div>
<a class='quick-help' href='squalor#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='squalor#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/squalor/memory_hooks/4618.json'></span>
<p>
<span class="emp0"><span>Po<span class="emp3"><span>or</span></span> <span class="emp1"><span>Qual</span></span>ity</span></span> The <span class="emp1"><span>qual</span></span>ity of life for those who live in s<span class="emp1"><span>qual</span></span><span class="emp3"><span>or</span></span> is very po<span class="emp3"><span>or</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="squalor#" id="add-public-hook" style="" url="https://membean.com/mywords/squalor/memory_hooks">Use other public hook</a>
<a href="squalor#" id="memhook-use-own" url="https://membean.com/mywords/squalor/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='squalor#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Thousands have spent four nights sleeping in the open under improvised shelters of reed stalks, blankets and salvaged tents. The Moria camp was built to house around 2,750 but overcrowding led to more than 12,500 people living in <b>squalor</b>, and had been held up by critics as a symbol of the European Union’s migration policy failings.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
"Grey Gardens" the documentary was a study in eccentricity and <b>squalor</b> on Long Island. Its subjects, faded socialites Edith Beale and her daughter, also named Edith Beale, once faced eviction by the county health department. Their 28-room house was full of trash and infested with vermin, with no running water—very out of place in the Hamptons.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/squalor/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='squalor#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>squal</td>
<td>
&rarr;
</td>
<td class='meaning'>be filthy or unkempt</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='or_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='squalor#'>
<span class=''></span>
-or
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state</td>
</tr>
</table>
<p><em>Squalor</em> is the &#8220;state or being filthy or dirty.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='squalor#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Kosovo</strong><span> The Roma community living in squalor.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/squalor.jpg' video_url='examplevids/squalor' video_width='350'></span>
<div id='wt-container'>
<img alt="Squalor" height="198" src="https://cdn1.membean.com/video/examplevids/squalor.jpg" width="350" />
<div class='center'>
<a href="squalor#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='squalor#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Squalor" src="https://cdn0.membean.com/public/images/wordimages/cons2/squalor.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='squalor#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abject</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>blight</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>dearth</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>decrepitude</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>derelict</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>despicable</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>dilapidated</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>enormity</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>fetid</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>impecunious</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>impoverished</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>indigent</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>noisome</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>noxious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>odious</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>paucity</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>penury</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>privation</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>reprehensible</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>slovenly</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>sordid</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>uncouth</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>unkempt</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>ablution</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>affluence</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bedeck</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>dapper</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>debonair</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>immaculate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>inestimable</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>inimitable</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>luxuriant</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>opulent</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>pecuniary</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>pristine</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>purge</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>resplendent</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>salubrious</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>salutary</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='squalor#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
squalid
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>dirty</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="squalor" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>squalor</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="squalor#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

