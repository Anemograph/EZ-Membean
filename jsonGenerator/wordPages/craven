
<!DOCTYPE html>
<html>
<head>
<title>Word: craven | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you abash someone, you make them feel uncomfortable, ashamed, embarrassed, or inferior.</p>
<p class='rw-defn idx1' style='display:none'>An audacious person acts with great daring and sometimes reckless bravery—despite risks and warnings from other people—in order to achieve something.</p>
<p class='rw-defn idx2' style='display:none'>Bravado is behavior that is deliberately intended to show courage or confidence, sometimes actual but usually pretended, in order to impress other people.</p>
<p class='rw-defn idx3' style='display:none'>If you describe a person&#8217;s behavior as brazen, you mean that they are not embarrassed by anything they do—they simply don&#8217;t care what other people think about them.</p>
<p class='rw-defn idx4' style='display:none'>If you are chary of doing something, you are cautious or timid about doing it because you are afraid that something bad will happen.</p>
<p class='rw-defn idx5' style='display:none'>If you are ___, you are cautious; you think carefully about something before saying or doing it.</p>
<p class='rw-defn idx6' style='display:none'>Consternation is the feeling of anxiety or fear, sometimes paralyzing in its effect, and often caused by something unexpected that has happened.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is contumacious is purposely stubborn, contrary, or disobedient.</p>
<p class='rw-defn idx8' style='display:none'>If something daunts you, it makes you worried about dealing with it because you think it will be very difficult or dangerous.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is diffident is shy, does not want to draw notice to themselves, and is lacking in self-confidence.</p>
<p class='rw-defn idx10' style='display:none'>Effrontery is very rude behavior that shows a great lack of respect and is often insulting.</p>
<p class='rw-defn idx11' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx12' style='display:none'>Fortitude is the determination or lasting courage to endure hardship or difficulty over an extended period of time.</p>
<p class='rw-defn idx13' style='display:none'>If someone is fractious, they are easily upset or annoyed over unimportant things.</p>
<p class='rw-defn idx14' style='display:none'>If you think someone is showing hubris, you think that they are demonstrating excessive pride and vanity.</p>
<p class='rw-defn idx15' style='display:none'>If someone behaves in an impertinent way, they behave rudely and disrespectfully.</p>
<p class='rw-defn idx16' style='display:none'>If someone demonstrates impudence, they behave in a very rude or disrespectful way.</p>
<p class='rw-defn idx17' style='display:none'>An intrepid person is willing to do dangerous things or go to dangerous places because they are brave.</p>
<p class='rw-defn idx18' style='display:none'>Someone&#8217;s mettle is their courageous determination and spirited ability to deal with problems or difficult situations.</p>
<p class='rw-defn idx19' style='display:none'>Someone who is obstreperous is noisy, unruly, and difficult to control.</p>
<p class='rw-defn idx20' style='display:none'>Someone is overweening when they are not modest; rather, they think way too much of themselves and let everyone know about it.</p>
<p class='rw-defn idx21' style='display:none'>When you are presumptuous, you act improperly, rudely, or without respect, especially while attempting to do something that is not socially acceptable or that you are not qualified to do.</p>
<p class='rw-defn idx22' style='display:none'>To be pusillanimous is to be cowardly.</p>
<p class='rw-defn idx23' style='display:none'>To quaver is to shake or tremble, especially when speaking.</p>
<p class='rw-defn idx24' style='display:none'>A raucous sound is unpleasantly loud, harsh, and noisy.</p>
<p class='rw-defn idx25' style='display:none'>Skittish persons or animals are made easily nervous or alarmed; they are likely to change behavior quickly and unpredictably.</p>
<p class='rw-defn idx26' style='display:none'>To act with temerity is to act in a carelessly and irresponsibly bold way.</p>
<p class='rw-defn idx27' style='display:none'>To be timorous is to be fearful.</p>
<p class='rw-defn idx28' style='display:none'>Someone is tremulous when they are shaking slightly from nervousness or fear; they may also simply be afraid of something.</p>
<p class='rw-defn idx29' style='display:none'>If you waver, you cannot decide between two things because you have serious doubts about which choice is better.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>craven</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='craven#' id='pronounce-sound' path='audio/words/amy-craven'></a>
KRAY-vuhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='craven#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='craven#' id='context-sound' path='audio/wordcontexts/brian-craven'></a>
Even though he was supposed to be leading his regiment into battle, the <em>craven</em> and fearful commander left his post on the first charge and ran away from the field.  Reginald&#8217;s  terror of large animals, loud noises, and risk of any sort gave him a <em>craven</em>, cowardly reputation.  One wondered how such a frightened, <em>craven</em> fellow had been promoted to the rank of officer.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What would be considered a <em>craven</em> act?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Stealing a twenty-dollar bill from your brother&#8217;s wallet.
</li>
<li class='choice answer '>
<span class='result'></span>
Running away instead of helping a friend who is being bullied.
</li>
<li class='choice '>
<span class='result'></span>
Ordering several pies that you plan to eat yourself in one day.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='craven#' id='definition-sound' path='audio/wordmeanings/amy-craven'></a>
Someone who is <em>craven</em> is very cowardly.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>cowardly</em>
</span>
</span>
</div>
<a class='quick-help' href='craven#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='craven#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/craven/memory_hooks/5713.json'></span>
<p>
<img alt="Wescraven" src="https://cdn1.membean.com/public/images/wordimages/hook/wescraven.jpg?qdep8" />
<span class="emp0"><span>Wes <span class="emp1"><span>Craven</span></span></span></span> The horror writer Wes <span class="emp1"><span>Craven</span></span> probably chose his last name to suggest that his readers will become <span class="emp1"><span>craven</span></span> by reading his works because they are so scary!
</p>
</div>

<div id='memhook-button-bar'>
<a href="craven#" id="add-public-hook" style="" url="https://membean.com/mywords/craven/memory_hooks">Use other public hook</a>
<a href="craven#" id="memhook-use-own" url="https://membean.com/mywords/craven/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='craven#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
When it comes to trade policy, where governments are reliably at their <b>craven</b>, cynical and incompetent worst, one must be grateful for all mercies, however small.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
More than a century ago, Joseph Conrad's novel "Lord Jim" told of a <b>craven</b> sea captain who abandoned ship, leaving his many passengers to fend for themselves.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/craven/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='craven#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>crav</td>
<td>
&rarr;
</td>
<td class='meaning'>burst, crack</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='en_pertaining' data-tree-url='//cdn1.membean.com/public/data/treexml' href='craven#'>
<span class=''></span>
-en
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>made of, pertaining to</td>
</tr>
</table>
<p>One&#8217;s courage is &#8220;cracked or broken&#8221; if one is <em>craven</em>.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='craven#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Monty Python and the Holy Grail</strong><span> Not brave, but craven Sir Robin!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/craven.jpg' video_url='examplevids/craven' video_width='350'></span>
<div id='wt-container'>
<img alt="Craven" height="198" src="https://cdn1.membean.com/video/examplevids/craven.jpg" width="350" />
<div class='center'>
<a href="craven#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='craven#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Craven" src="https://cdn3.membean.com/public/images/wordimages/cons2/craven.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='craven#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abash</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>chary</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>circumspect</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>consternation</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>daunt</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>diffident</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>pusillanimous</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>quaver</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>skittish</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>timorous</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>tremulous</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>waver</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>audacious</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bravado</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>brazen</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>contumacious</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>effrontery</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>fortitude</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>fractious</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>hubris</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>impertinent</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>impudence</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>intrepid</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>mettle</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>obstreperous</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>overweening</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>presumptuous</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>raucous</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>temerity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="craven" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>craven</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="craven#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

