
<!DOCTYPE html>
<html>
<head>
<title>Word: utilitarian | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>The word aesthetic is used to talk about art, beauty, the study of beauty, and the appreciation of beautiful things.</p>
<p class='rw-defn idx1' style='display:none'>Something that is apposite is relevant or suitable to what is happening or being discussed.</p>
<p class='rw-defn idx2' style='display:none'>A chimerical idea is unrealistic, imaginary, or unlikely to be fulfilled.</p>
<p class='rw-defn idx3' style='display:none'>A commodious room or house is large and roomy, which makes it convenient and highly suitable for living.</p>
<p class='rw-defn idx4' style='display:none'>Something that is delusive deceives you by giving a false belief about yourself or the situation you are in.</p>
<p class='rw-defn idx5' style='display:none'>Efficacy is the ability or power to produce an expected effect or result.</p>
<p class='rw-defn idx6' style='display:none'>Empirical evidence or study is based on real experience or scientific experiments rather than on unproven theories.</p>
<p class='rw-defn idx7' style='display:none'>A thing or action that is expedient is useful, advantageous, or appropriate to get something accomplished.</p>
<p class='rw-defn idx8' style='display:none'>If you think something, such as a statement or idea, is fatuous, you consider it stupid or extremely silly; a fatuous hope is unrealistic.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx10' style='display:none'>Two items are fungible if one can be exchanged for the other with no loss in inherent value; for example, one $10 bill and two $5 bills are fungible.</p>
<p class='rw-defn idx11' style='display:none'>A futile attempt at doing something is hopeless and pointless because it simply cannot be accomplished.</p>
<p class='rw-defn idx12' style='display:none'>Something that is hypothetical is based on possible situations or events rather than actual ones.</p>
<p class='rw-defn idx13' style='display:none'>Something illusory appears real or possible; in fact, it is neither.</p>
<p class='rw-defn idx14' style='display:none'>If you describe someone&#8217;s behavior as inane, you think it is completely stupid or without much meaning.</p>
<p class='rw-defn idx15' style='display:none'>Someone who is ineffectual at a task is useless or ineffective when attempting to do it.</p>
<p class='rw-defn idx16' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx17' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx18' style='display:none'>If you handle something in a pragmatic way, you deal with it in a realistic and practical manner rather than just following unproven theories or ideas.</p>
<p class='rw-defn idx19' style='display:none'>Quixotic plans or ideas are not very practical or realistic; they are often based on unreasonable hopes for improving the world.</p>
<p class='rw-defn idx20' style='display:none'>A rationale is a reason or explanation for doing something.</p>
<p class='rw-defn idx21' style='display:none'>When you have been remiss, you have been careless because you did not do something that you should have done.</p>
<p class='rw-defn idx22' style='display:none'>Something that is specious seems to be good, sound, or correct but is actually wrong or fake.</p>
<p class='rw-defn idx23' style='display:none'>A spurious statement is false because it is not based on sound thinking; therefore, it is not what it appears to be.</p>
<p class='rw-defn idx24' style='display:none'>To be tentative is to be hesitant or uncertain about something; an agreement or decision of this kind is likely to have changes before it reaches its final form.</p>
<p class='rw-defn idx25' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx26' style='display:none'>If a person holds a titular position, they have a title but no real power.</p>
<p class='rw-defn idx27' style='display:none'>Something unfeasible cannot be made or achieved.</p>
<p class='rw-defn idx28' style='display:none'>Something that is unwieldy is hard or awkward to handle because of the way that it is shaped.</p>
<p class='rw-defn idx29' style='display:none'>A viable project is practical or can be accomplished; therefore, it is worth doing.</p>
<p class='rw-defn idx30' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>utilitarian</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='utilitarian#' id='pronounce-sound' path='audio/words/amy-utilitarian'></a>
yoo-til-i-TAIR-ee-uhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='utilitarian#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='utilitarian#' id='context-sound' path='audio/wordcontexts/brian-utilitarian'></a>
The Swiss Army Knife is so <em>utilitarian</em> because of its many tools that are useful in nature.  First, of course, there is the <em>utilitarian</em> and practical jackknife, which can be used as an all-purpose cutting tool.  The <em>utilitarian</em> items of the scissors, compass, awl, file, and magnifier can come in handy when outside in nature.  The shape and size of the entire tool contributes to its <em>utilitarian</em> nature because it does not take up much room at all.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Why would someone want a <em>utilitarian</em> item?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
It is handy and useful to complete a task.
</li>
<li class='choice '>
<span class='result'></span>
It is small and can be transported easily.
</li>
<li class='choice '>
<span class='result'></span>
It is fun to play with and has many features.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='utilitarian#' id='definition-sound' path='audio/wordmeanings/amy-utilitarian'></a>
A <em>utilitarian</em> object is useful, serviceable, and practical—rather than fancy or unnecessary.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>practical</em>
</span>
</span>
</div>
<a class='quick-help' href='utilitarian#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='utilitarian#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/utilitarian/memory_hooks/5079.json'></span>
<p>
<span class="emp0"><span>Useful <span class="emp2"><span>Utilit</span></span>ies</span></span> Water, electric, natural gas, and sewage are the <span class="emp2"><span>utilit</span></span>ies in one's home, meant to be <span class="emp2"><span>utilit</span></span>arian or highly useful for the consumer.
</p>
</div>

<div id='memhook-button-bar'>
<a href="utilitarian#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/utilitarian/memory_hooks">Use other hook</a>
<a href="utilitarian#" id="memhook-use-own" url="https://membean.com/mywords/utilitarian/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='utilitarian#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Electric and water meters must remain accessible to the utility companies, as well as any cables, transformers, junction boxes, air conditioners and other <b>utilitarian</b> features.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
The conservatives' rooms were not only tidy and orderly, they were full of <b>utilitarian</b> stuff like cleaning supplies, calendars and postage stamps. The liberals' rooms were painted in bold colors and cluttered with books and art and travel brochures.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
The hippie-chick styles don’t stray too far from Tatami’s everyday <b>utilitarian</b> aesthetic, and are similarly priced—but details are distinctively Lim.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
Offering opportunities for young, mostly American singers and conductors to be heard in the standard repertory is one of them; making opera accessible by making it affordable is another. The company’s production of Verdi’s _Traviata,_ which had its first performance of the season on Saturday evening, is a reminder of the City Opera’s more <b>utilitarian</b> side.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/utilitarian/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='utilitarian#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='util_useful' data-tree-url='//cdn1.membean.com/public/data/treexml' href='utilitarian#'>
<span class=''></span>
util
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>useful, practical, convenient</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='arian_pertaining' data-tree-url='//cdn1.membean.com/public/data/treexml' href='utilitarian#'>
<span class=''></span>
-arian
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>pertaining to</td>
</tr>
</table>
<p>Anything described as <em>utilitarian</em> &#8220;pertains to&#8221; the fact that is it &#8220;useful, practical, or convenient.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='utilitarian#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Victorinox Swiss Army Knife Commercial</strong><span> One of the most utilitarian tools there is.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/utilitarian.jpg' video_url='examplevids/utilitarian' video_width='350'></span>
<div id='wt-container'>
<img alt="Utilitarian" height="288" src="https://cdn1.membean.com/video/examplevids/utilitarian.jpg" width="350" />
<div class='center'>
<a href="utilitarian#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='utilitarian#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Utilitarian" src="https://cdn3.membean.com/public/images/wordimages/cons2/utilitarian.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='utilitarian#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>apposite</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>commodious</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>efficacy</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>empirical</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>expedient</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>fungible</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>pragmatic</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>rationale</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>viable</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>aesthetic</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>chimerical</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>delusive</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>fatuous</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>futile</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>hypothetical</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>illusory</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>inane</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>ineffectual</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>quixotic</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>remiss</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>specious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>spurious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>tentative</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>titular</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>unfeasible</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>unwieldy</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='utilitarian#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
utility
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>usefulness</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="utilitarian" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>utilitarian</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="utilitarian#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

