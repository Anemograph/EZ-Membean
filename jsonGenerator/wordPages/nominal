
<!DOCTYPE html>
<html>
<head>
<title>Word: nominal | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An autocratic person rules with complete power; consequently, they make decisions and give orders to people without asking them for their opinion or advice.</p>
<p class='rw-defn idx1' style='display:none'>A bagatelle is something that is not important or of very little value or significance.</p>
<p class='rw-defn idx2' style='display:none'>A cardinal rule or quality is considered to be the most important or basic in a set of rules or qualities.</p>
<p class='rw-defn idx3' style='display:none'>When you commemorate a person, you honor them or cause them to be remembered in some way.</p>
<p class='rw-defn idx4' style='display:none'>To corroborate something that has been said or reported means to give information or evidence that further supports it.</p>
<p class='rw-defn idx5' style='display:none'>The crux of a problem or argument is the most important or difficult part of it that affects everything else.</p>
<p class='rw-defn idx6' style='display:none'>A cynosure is an object that serves as the center of attention.</p>
<p class='rw-defn idx7' style='display:none'>If someone is deified, they have been either made into a god or are adored like one.</p>
<p class='rw-defn idx8' style='display:none'>A despot is a leader who has a lot of power and uses it in a cruel and unreasonable way.</p>
<p class='rw-defn idx9' style='display:none'>An echelon is one level of status or rank in an organization.</p>
<p class='rw-defn idx10' style='display:none'>Empirical evidence or study is based on real experience or scientific experiments rather than on unproven theories.</p>
<p class='rw-defn idx11' style='display:none'>Something that is epochal is highly significant or important; this adjective often refers to the bringing about or marking of the beginning of a new era.</p>
<p class='rw-defn idx12' style='display:none'>An exiguous amount of something is meager, small, or limited in nature.</p>
<p class='rw-defn idx13' style='display:none'>A figurehead in an organization is the apparent authority or head in name only—the real power lies somewhere else.</p>
<p class='rw-defn idx14' style='display:none'>If something is formidable, it is impressive in size, power, or skill; therefore, it can make you feel frightened, alarmed, or in awe because it is so powerful or difficult to deal with.</p>
<p class='rw-defn idx15' style='display:none'>The gravity of a situation or event is its seriousness or importance.</p>
<p class='rw-defn idx16' style='display:none'>A hierarchy is a system of organization in a society, company, or other group in which people are divided into different ranks or levels of importance.</p>
<p class='rw-defn idx17' style='display:none'>Inconsequential matters are unimportant or are of little to no significance.</p>
<p class='rw-defn idx18' style='display:none'>An indispensable item is absolutely necessary or essential—it cannot be done without.</p>
<p class='rw-defn idx19' style='display:none'>An intrinsic characteristic of something is the basic and essential feature that makes it what it is.</p>
<p class='rw-defn idx20' style='display:none'>Irrelevant information is unrelated or unconnected to the situation at hand.</p>
<p class='rw-defn idx21' style='display:none'>A juggernaut is a very powerful force, organization, or group whose influence cannot be stopped; its strength and power can overwhelm or crush any competition or anything else that stands in its way.</p>
<p class='rw-defn idx22' style='display:none'>When someone is lionized, they are treated as being very important or famous—although they may not deserve to be.</p>
<p class='rw-defn idx23' style='display:none'>Something minuscule is extremely small in size or amount.</p>
<p class='rw-defn idx24' style='display:none'>A momentous occurrence is very important, significant, or vital in some way.</p>
<p class='rw-defn idx25' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx26' style='display:none'>A monumental event is very great, impressive, or extremely important in some way.</p>
<p class='rw-defn idx27' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx28' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx29' style='display:none'>Something that is paltry is practically worthless or insignificant.</p>
<p class='rw-defn idx30' style='display:none'>Anything picayune is unimportant, insignificant, or minor.</p>
<p class='rw-defn idx31' style='display:none'>Something that is pivotal is more important than anything else in a situation or a system; consequently, it is the key to its success.</p>
<p class='rw-defn idx32' style='display:none'>A plenipotentiary is someone who has full power to take action or make decisions on behalf of their government in a foreign country.</p>
<p class='rw-defn idx33' style='display:none'>A potentate is a ruler who has great power over people.</p>
<p class='rw-defn idx34' style='display:none'>Something that is prodigious is very large, impressive, or great.</p>
<p class='rw-defn idx35' style='display:none'>Something putative is supposed to be real; for example, a putative leader is one who everyone assumes is the leader—even though they may not be in reality.</p>
<p class='rw-defn idx36' style='display:none'>Substantive issues are the most important, serious, and real issues of a subject.</p>
<p class='rw-defn idx37' style='display:none'>A person or subject that is superficial is shallow, without depth, obvious, and concerned only with surface matters.</p>
<p class='rw-defn idx38' style='display:none'>If a person holds a titular position, they have a title but no real power.</p>
<p class='rw-defn idx39' style='display:none'>If you refer to something as a trifle, you mean that it is of little or no importance or value.</p>
<p class='rw-defn idx40' style='display:none'>Something trivial is of little value or is not important.</p>
<p class='rw-defn idx41' style='display:none'>Something that is vaporous is not completely formed but foggy and misty; in the same vein, a vaporous idea is insubstantial and vague.</p>
<p class='rw-defn idx42' style='display:none'>Venerable people command respect because they are old and wise.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>nominal</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='nominal#' id='pronounce-sound' path='audio/words/amy-nominal'></a>
NOM-uh-nl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='nominal#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='nominal#' id='context-sound' path='audio/wordcontexts/brian-nominal'></a>
At the farmers&#8217; market the vendors paid a <em>nominal</em> or small fee of a dollar to the Market Manager.  The purpose of this <em>nominal</em> or insignificant charge was to pay for little things needed at the market, a charge which gave great value compared to the slight cost.  Each year the farmers who sold at the market elected a Market Manager to handle this fee, a truly <em>nominal</em> position because he really didn&#8217;t do much of anything at all, but rather turned the money over to the Farmers&#8217; Market Council which decided what it would be used for.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If you have a <em>nominal</em> amount of time to study for a test, what is your situation?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
You have a great deal of time to study because you&#8217;ve put aside time to do so.
</li>
<li class='choice '>
<span class='result'></span>
You don&#8217;t yet know how much time you&#8217;ll have to study.
</li>
<li class='choice answer '>
<span class='result'></span>
You don&#8217;t have much time to study at all. 
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='nominal#' id='definition-sound' path='audio/wordmeanings/amy-nominal'></a>
<em>Nominal</em> can refer to someone who is in charge in name only, or it can refer to a very small amount of something; both are related by their relative insignificance.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>insignificant</em>
</span>
</span>
</div>
<a class='quick-help' href='nominal#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='nominal#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/nominal/memory_hooks/5189.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Nam</span></span>e F<span class="emp1"><span>inal</span></span></span></span> A <span class="emp2"><span>nom</span></span><span class="emp1"><span>inal</span></span> position in a company has no power, so the <span class="emp2"><span>nam</span></span>e is the f<span class="emp1"><span>inal</span></span> aspect of it.
</p>
</div>

<div id='memhook-button-bar'>
<a href="nominal#" id="add-public-hook" style="" url="https://membean.com/mywords/nominal/memory_hooks">Use other public hook</a>
<a href="nominal#" id="memhook-use-own" url="https://membean.com/mywords/nominal/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='nominal#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
She was so touched by how much they did medically for Wednesday while charging only a <b>nominal</b> adoption fee that, once retired, she volunteered for three months to walk ARF's dogs.
<cite class='attribution'>
&mdash;
The East Hampton Star
</cite>
</li>
<li>
"Because Alex is on our staff, he has to earn a salary," said Tyurkin, who intimated it was a <b>nominal</b> salary.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
We do not own any of the intellectual property, we just provide a small service to them for a <b>nominal</b> fixed fee.
<cite class='attribution'>
&mdash;
Forbes
</cite>
</li>
<li>
Kim, reportedly holed up in his coastal resort in Wonsan, is the <b>nominal</b> head of Office 39, which is housed in a faceless government building in the capital.
<cite class='attribution'>
&mdash;
New York Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/nominal/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='nominal#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='nom_name' data-tree-url='//cdn1.membean.com/public/data/treexml' href='nominal#'>
<span class=''></span>
nom
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>name</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='al_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='nominal#'>
<span class=''></span>
-al
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>Something <em>nominal</em> is &#8220;in name only.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='nominal#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>The Two Towers</strong><span> Theoden King is but a nominal king at this point.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/nominal.jpg' video_url='examplevids/nominal' video_width='350'></span>
<div id='wt-container'>
<img alt="Nominal" height="288" src="https://cdn1.membean.com/video/examplevids/nominal.jpg" width="350" />
<div class='center'>
<a href="nominal#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='nominal#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Nominal" src="https://cdn3.membean.com/public/images/wordimages/cons2/nominal.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='nominal#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>bagatelle</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>exiguous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>figurehead</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>inconsequential</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>irrelevant</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>minuscule</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>paltry</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>picayune</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>putative</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>superficial</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>titular</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>trifle</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>trivial</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>vaporous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>autocratic</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>cardinal</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>commemorate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>corroborate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>crux</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>cynosure</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>deify</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>despot</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>echelon</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>empirical</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>epochal</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>formidable</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>gravity</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>hierarchy</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>indispensable</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>intrinsic</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>juggernaut</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>lionize</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>momentous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>monumental</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>pivotal</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>plenipotentiary</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>potentate</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>prodigious</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>substantive</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>venerable</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="nominal" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>nominal</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="nominal#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

