
<!DOCTYPE html>
<html>
<head>
<title>Word: inconspicuous | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Inconspicuous-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/inconspicuous-large.jpg?qdep8" />
</div>
<a href="inconspicuous#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>To accentuate something is to emphasize it or make it more noticeable.</p>
<p class='rw-defn idx1' style='display:none'>You accost a stranger when you move towards them and speak in an unpleasant or threatening way.</p>
<p class='rw-defn idx2' style='display:none'>You affront someone by openly and intentionally offending or insulting him.</p>
<p class='rw-defn idx3' style='display:none'>If you describe a person&#8217;s behavior as brazen, you mean that they are not embarrassed by anything they do—they simply don&#8217;t care what other people think about them.</p>
<p class='rw-defn idx4' style='display:none'>Something that is clandestine is hidden or kept a secret, often because it is illegal.</p>
<p class='rw-defn idx5' style='display:none'>Covert activities or situations are secret and hidden.</p>
<p class='rw-defn idx6' style='display:none'>If you describe someone, usually a young woman, as demure, you mean that she is quiet, shy, and always behaves modestly.</p>
<p class='rw-defn idx7' style='display:none'>Effrontery is very rude behavior that shows a great lack of respect and is often insulting.</p>
<p class='rw-defn idx8' style='display:none'>Someone who is effusive expresses happiness, pleasure, admiration, praise, etc., in an extremely enthusiastic way.</p>
<p class='rw-defn idx9' style='display:none'>An egregious mistake, failure, or problem is an extremely bad and very noticeable one.</p>
<p class='rw-defn idx10' style='display:none'>When you are guilty of encroachment, you intrude upon or invade another person&#8217;s private space.</p>
<p class='rw-defn idx11' style='display:none'>An extravaganza is an elaborate production or spectacular display that is meant to entertain, often in an excessive fashion.</p>
<p class='rw-defn idx12' style='display:none'>An action that is flagrant shows that someone does not care if they obviously break the rules or highly offend people.</p>
<p class='rw-defn idx13' style='display:none'>When someone flaunts their good looks, they show them off or boast about them in a very proud and shameless way.</p>
<p class='rw-defn idx14' style='display:none'>If something is formidable, it is impressive in size, power, or skill; therefore, it can make you feel frightened, alarmed, or in awe because it is so powerful or difficult to deal with.</p>
<p class='rw-defn idx15' style='display:none'>Praise, an apology, or gratitude is fulsome if it is so exaggerated and elaborate that it does not seem sincere.</p>
<p class='rw-defn idx16' style='display:none'>If someone behaves in a furtive way, they do things sneakily and secretly in order to avoid being noticed.</p>
<p class='rw-defn idx17' style='display:none'>Something gargantuan is extremely large.</p>
<p class='rw-defn idx18' style='display:none'>Something that is imperceptible is either impossible to be perceived by the mind or is very difficult to perceive.</p>
<p class='rw-defn idx19' style='display:none'>If someone behaves in an impertinent way, they behave rudely and disrespectfully.</p>
<p class='rw-defn idx20' style='display:none'>An imposition is giving someone an additional duty or extra work that is not welcomed by that person.</p>
<p class='rw-defn idx21' style='display:none'>Someone who is indiscreet shows lack of judgment, especially because they say or do things in public that should only be said or done privately—if at all.</p>
<p class='rw-defn idx22' style='display:none'>Something infinitesimal is so extremely small or minute that it is very hard to measure it.</p>
<p class='rw-defn idx23' style='display:none'>To infringe on another person&#8217;s rights is to violate or intrude upon those rights.</p>
<p class='rw-defn idx24' style='display:none'>An interloper is someone who barges into a place where they are not welcome and interferes with what is going on, often for personal gain.</p>
<p class='rw-defn idx25' style='display:none'>An intrusive person intrudes, butts in, or interferes where they are not welcome.</p>
<p class='rw-defn idx26' style='display:none'>Something that is latent, such as an undiscovered talent or undiagnosed disease, exists but is not active or has not developed at the moment—it may develop or become active in the future.</p>
<p class='rw-defn idx27' style='display:none'>A leviathan is something that is very large, powerful, difficult to control, and rather frightening.</p>
<p class='rw-defn idx28' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx29' style='display:none'>A notorious person is well-known by the public at large; they are usually famous for doing something bad.</p>
<p class='rw-defn idx30' style='display:none'>An officious person acts in a self-important manner; therefore, they are very eager to offer unwanted advice or services—which makes them annoying.</p>
<p class='rw-defn idx31' style='display:none'>Something is opaque if it is either difficult to understand or is not transparent.</p>
<p class='rw-defn idx32' style='display:none'>If you describe an action as ostentatious, you think it is an extreme and exaggerated way of impressing people.</p>
<p class='rw-defn idx33' style='display:none'>An overt act is not hidden or secret but is done in an open and public way.</p>
<p class='rw-defn idx34' style='display:none'>Someone is overweening when they are not modest; rather, they think way too much of themselves and let everyone know about it.</p>
<p class='rw-defn idx35' style='display:none'>If a mood or feeling is palpable, it is so strong and intense that it is easily noticed and is almost able to be physically felt.</p>
<p class='rw-defn idx36' style='display:none'>When you are presumptuous, you act improperly, rudely, or without respect, especially while attempting to do something that is not socially acceptable or that you are not qualified to do.</p>
<p class='rw-defn idx37' style='display:none'>Something prosaic is dull, boring, and ordinary.</p>
<p class='rw-defn idx38' style='display:none'>The salient qualities of an issue or feature are those that are most important and noticeable.</p>
<p class='rw-defn idx39' style='display:none'>When you strut, you move as though you own the world by walking in a confident and showy fashion.</p>
<p class='rw-defn idx40' style='display:none'>A surreptitious deed is done secretly to avoid bringing any attention to it.</p>
<p class='rw-defn idx41' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx42' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>inconspicuous</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='inconspicuous#' id='pronounce-sound' path='audio/words/amy-inconspicuous'></a>
in-kuhn-SPIK-yoo-uhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='inconspicuous#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='inconspicuous#' id='context-sound' path='audio/wordcontexts/brian-inconspicuous'></a>
No one would notice the plain, ordinary, <em>inconspicuous</em> Belle in a crowd.  Her appearance was simple and unremarkable, so nothing drew attention to her <em>inconspicuous</em>, almost invisible presence.  Those who got to know Belle, however, realized that her <em>inconspicuous</em>, hidden exterior housed a truly beautiful and amazing spirit.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of something that is <em>inconspicuous</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A large ketchup stain on your shirt that doesn&#8217;t come out when it&#8217;s washed.
</li>
<li class='choice answer '>
<span class='result'></span>
A tiny crack in your mom&#8217;s favorite vase that she thankfully doesn&#8217;t notice.
</li>
<li class='choice '>
<span class='result'></span>
A dog that refuses to sit on command despite months of training.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='inconspicuous#' id='definition-sound' path='audio/wordmeanings/amy-inconspicuous'></a>
Something that is <em>inconspicuous</em> does not attract attention and is not easily seen or noticed because it is small or ordinary.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>unnoticeable</em>
</span>
</span>
</div>
<a class='quick-help' href='inconspicuous#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='inconspicuous#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/inconspicuous/memory_hooks/2966.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Spic</span></span>e, <span class="emp2"><span>Incon</span></span>stancy, and Tumult<span class="emp3"><span>uous</span></span> Weather</span></span> Events that add <span class="emp1"><span>spic</span></span>e to life, such as <span class="emp2"><span>incons</span></span>tancy in relationships or tumult<span class="emp3"><span>uous</span></span> and stormy weather, are all things that are definitely <em>NOT</em> <span class="emp2"><span>incon</span></span><span class="emp1"><span>spic</span></span><span class="emp3"><span>uous</span></span> because people pay attention to them.
</p>
</div>

<div id='memhook-button-bar'>
<a href="inconspicuous#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/inconspicuous/memory_hooks">Use other hook</a>
<a href="inconspicuous#" id="memhook-use-own" url="https://membean.com/mywords/inconspicuous/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='inconspicuous#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Studies have shown that in a single day, hundreds of thousands of cars will pass stopped school buses illegally, and that’s not because a stopped bus is <b>inconspicuous</b>.
<cite class='attribution'>
&mdash;
WebMD
</cite>
</li>
<li>
“I have been, for so long, a quiet, <b>inconspicuous</b> person in my neighborhood, and now there are signs up telling me that they love me,” [Dr. Anthony] Fauci marveled on Monday.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
The pheasantshell is a freshwater mussel, a less-edible version of its saltwater cousin that spends most of its <b>inconspicuous</b> life part-buried in riverbeds, blending in with the rocks and filtering the water around them.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
[Earbuds'] <b>inconspicuous</b> design, while convenient, can be easy to miss, and their users try to remedy that.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/inconspicuous/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='inconspicuous#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='in_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='inconspicuous#'>
<span class='common'></span>
in-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='con_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='inconspicuous#'>
<span class='common'></span>
con-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='spic_see' data-tree-url='//cdn1.membean.com/public/data/treexml' href='inconspicuous#'>
<span class=''></span>
spic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>see, observe, look, watch over</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='uous_nature' data-tree-url='//cdn1.membean.com/public/data/treexml' href='inconspicuous#'>
<span class=''></span>
-uous
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of the nature of</td>
</tr>
</table>
<p>If something remains <em>inconspicuous</em>, it is &#8220;not thoroughly seen or watched&#8221; because it is relatively unnoticeable.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='inconspicuous#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Skyfall</strong><span> A car like that is certainly not inconspicuous.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/inconspicuous.jpg' video_url='examplevids/inconspicuous' video_width='350'></span>
<div id='wt-container'>
<img alt="Inconspicuous" height="288" src="https://cdn1.membean.com/video/examplevids/inconspicuous.jpg" width="350" />
<div class='center'>
<a href="inconspicuous#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='inconspicuous#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Inconspicuous" src="https://cdn1.membean.com/public/images/wordimages/cons2/inconspicuous.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='inconspicuous#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='4' class = 'rw-wordform notlearned'><span>clandestine</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>covert</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>demure</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>furtive</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>imperceptible</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>infinitesimal</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>latent</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>opaque</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>prosaic</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>surreptitious</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>accentuate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>accost</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>affront</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>brazen</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>effrontery</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>effusive</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>egregious</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>encroachment</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>extravaganza</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>flagrant</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>flaunt</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>formidable</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>fulsome</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>gargantuan</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>impertinent</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>imposition</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>indiscreet</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>infringe</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>interloper</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>intrusive</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>leviathan</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>notorious</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>officious</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>ostentatious</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>overt</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>overweening</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>palpable</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>presumptuous</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>salient</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>strut</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='inconspicuous#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
conspicuous
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>easy to catch sight of</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="inconspicuous" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>inconspicuous</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="inconspicuous#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

