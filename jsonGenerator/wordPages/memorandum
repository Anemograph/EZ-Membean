
<!DOCTYPE html>
<html>
<head>
<title>Word: memorandum | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>A colloquy is a formal conversation.</p>
<p class='rw-defn idx1' style='display:none'>A piece of writing is discursive if it includes a lot of information that is not relevant to the main subject.</p>
<p class='rw-defn idx2' style='display:none'>A dispersal of something is its scattering or distribution over a wide area.</p>
<p class='rw-defn idx3' style='display:none'>To disseminate something, such as knowledge or information, is to distribute it so that it reaches a lot of people.</p>
<p class='rw-defn idx4' style='display:none'>If you elucidate something, you make it easier to understand by giving more relevant information.</p>
<p class='rw-defn idx5' style='display:none'>An evocation of something creates or summons a clear mental image or impression of it through words, pictures, or music.</p>
<p class='rw-defn idx6' style='display:none'>An interlocutor is the person with whom you are having a (usually formal) conversation or discussion.</p>
<p class='rw-defn idx7' style='display:none'>A missive is a written letter, especially a formal, legal, or important one.</p>
<p class='rw-defn idx8' style='display:none'>If you peruse some written text, you read it over carefully.</p>
<p class='rw-defn idx9' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx10' style='display:none'>A tacit agreement between two people is understood without having to use words to express it.</p>
<p class='rw-defn idx11' style='display:none'>If someone is unlettered, they are illiterate, unable to read and write.</p>
<p class='rw-defn idx12' style='display:none'>Verbiage is an excessive use of words to convey something that could be expressed using fewer words; it can also be the manner or style in which someone uses words.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>memorandum</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='memorandum#' id='pronounce-sound' path='audio/words/amy-memorandum'></a>
mem-uh-RAN-duhm
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='memorandum#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='memorandum#' id='context-sound' path='audio/wordcontexts/brian-memorandum'></a>
I wrote myself a <em>memorandum</em> or reminder the other day that I had to remember to pick Marguerite up from the university.  Luckily I had written that <em>memorandum</em> or short note to myself, otherwise I would have forgotten.  It was funny that the same day I read my personal message, I received an interoffice <em>memorandum</em> or announcement that the <span class="caps">CEO</span> had been fired, and that a search would begin immediately to replace her.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
For what might a <em>memorandum</em> be used?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
To gather top-secret clues to an international problem.
</li>
<li class='choice '>
<span class='result'></span>
To ask what kind of foods you like better than others.
</li>
<li class='choice answer '>
<span class='result'></span>
To remind you of an important due date for a project.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='memorandum#' id='definition-sound' path='audio/wordmeanings/amy-memorandum'></a>
A <em>memorandum</em> is a short note that helps someone remember what they&#8217;ve done or have to do; it can also be a form of communication that gives out information.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>message</em>
</span>
</span>
</div>
<a class='quick-help' href='memorandum#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='memorandum#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/memorandum/memory_hooks/3822.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Memo</span></span></span></span> The word "<span class="emp3"><span>memo</span></span>" is short for "<span class="emp3"><span>memo</span></span>randum."
</p>
</div>

<div id='memhook-button-bar'>
<a href="memorandum#" id="add-public-hook" style="" url="https://membean.com/mywords/memorandum/memory_hooks">Use other public hook</a>
<a href="memorandum#" id="memhook-use-own" url="https://membean.com/mywords/memorandum/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='memorandum#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
According to an interoffice <b>memorandum</b> sent on behalf of the lead physicist of the Manhattan Project, [Dr. Robert] Oppenheimer asked for a coat rack to be installed in his office. Shortly after the first request, Oppenheimer’s office sent another memo to this effect: “Thank you for the coat rack you installed this morning, but I still need a nail for my hat.”
<cite class='attribution'>
&mdash;
Huffington Post
</cite>
</li>
<li>
Although the new <b>memorandum</b> is not significantly different from the older versions, it contains a few additions, such as requiring the group to submit quarterly reports to the Commerce Department.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
President Obama signed a <b>memorandum</b> Wednesday designed to save the federal government $40 billion a year by overhauling what Obama calls the "broken system of government contracting."
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
First Transit Inc., the contract operator of the bus system, announced Price’s new job in a short <b>memorandum</b> signed by Larry Morris, region vice president.
<cite class='attribution'>
&mdash;
The Roanoke Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/memorandum/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='memorandum#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='memor_remembering' data-tree-url='//cdn1.membean.com/public/data/treexml' href='memorandum#'>
<span class=''></span>
memor
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>remembering, mindful</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='and_to_be' data-tree-url='//cdn1.membean.com/public/data/treexml' href='memorandum#'>
<span class=''></span>
-and
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>that which is to be ...ed</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='um_noun' data-tree-url='//cdn1.membean.com/public/data/treexml' href='memorandum#'>
<span class=''></span>
-um
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>forms a singular Latin noun</td>
</tr>
</table>
<p>A <em>memorandum</em> is &#8220;that which is to be remembered or kept in mind.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='memorandum#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Cheaper by the Dozen</strong><span> He even made a memorandum to help him remember that he wanted to have six girls and six boys.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/memorandum.jpg' video_url='examplevids/memorandum' video_width='350'></span>
<div id='wt-container'>
<img alt="Memorandum" height="288" src="https://cdn1.membean.com/video/examplevids/memorandum.jpg" width="350" />
<div class='center'>
<a href="memorandum#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='memorandum#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Memorandum" src="https://cdn1.membean.com/public/images/wordimages/cons2/memorandum.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='memorandum#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>colloquy</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>discursive</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>dispersal</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>disseminate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>elucidate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>evocation</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>interlocutor</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>missive</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>peruse</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>verbiage</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='9' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>tacit</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>unlettered</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='memorandum#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
memoranda
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>plural form of "memorandum"</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="memorandum" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>memorandum</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="memorandum#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

