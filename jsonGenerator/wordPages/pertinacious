
<!DOCTYPE html>
<html>
<head>
<title>Word: pertinacious | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Pertinacious-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/pertinacious-large.jpg?qdep8" />
</div>
<a href="pertinacious#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>An assiduous person works hard to ensure that something is done properly and completely.</p>
<p class='rw-defn idx1' style='display:none'>Someone who is capricious changes their mind repeatedly or behaves in unexpected ways; a capricious series of events follows no predictable pattern.</p>
<p class='rw-defn idx2' style='display:none'>When someone is conscientious in performing a task, they do it in a thorough and careful fashion to make sure that it is completely done.</p>
<p class='rw-defn idx3' style='display:none'>Something that is desultory is done in a way that is unplanned, disorganized, and without direction.</p>
<p class='rw-defn idx4' style='display:none'>When you create a diversion, you cause someone to turn aside momentarily from what they are doing by distracting them.</p>
<p class='rw-defn idx5' style='display:none'>Efficacy is the ability or power to produce an expected effect or result.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is exacting expects others to work very hard and carefully.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx8' style='display:none'>Fortitude is the determination or lasting courage to endure hardship or difficulty over an extended period of time.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is impetuous does things quickly and rashly without thinking carefully first.</p>
<p class='rw-defn idx10' style='display:none'>An impromptu speech is unplanned or spontaneous—it has not been practiced in any way beforehand.</p>
<p class='rw-defn idx11' style='display:none'>When someone improvises, they make something up at once because an unexpected situation has arisen.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is impulsive tends to do things without thinking about them ahead of time; therefore, their actions can be unpredictable.</p>
<p class='rw-defn idx13' style='display:none'>Something that is incessant continues on for a long time without stopping.</p>
<p class='rw-defn idx14' style='display:none'>If someone is indefatigable, they never shows signs of getting tired.</p>
<p class='rw-defn idx15' style='display:none'>Something that happens on an intermittent basis happens in irregular intervals, stopping and starting at unpredictable times.</p>
<p class='rw-defn idx16' style='display:none'>Intractable problems, situations, or people are very difficult or impossible to deal with.</p>
<p class='rw-defn idx17' style='display:none'>An inveterate person is always doing a particular thing, especially something questionable—and they are not likely to stop doing it.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is irresolute is unable to decide what to do.</p>
<p class='rw-defn idx19' style='display:none'>Someone is considered meticulous when they act with careful attention to detail.</p>
<p class='rw-defn idx20' style='display:none'>An obstinate person refuses to change their mind, even when other people think they are being highly unreasonable.</p>
<p class='rw-defn idx21' style='display:none'>When you procrastinate, you put off or delay doing something—usually because it is something unpleasant that you&#8217;d rather not do.</p>
<p class='rw-defn idx22' style='display:none'>If someone watches or listens to something with rapt attention, they are so involved with it that they do not notice anything else.</p>
<p class='rw-defn idx23' style='display:none'>Refractory people deliberately don&#8217;t obey someone in authority and so are difficult to deal with or control.</p>
<p class='rw-defn idx24' style='display:none'>Someone who is sedulous works hard and performs tasks very carefully and thoroughly, not stopping their work until it is completely accomplished.</p>
<p class='rw-defn idx25' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx26' style='display:none'>A tenacious person does not quit until they finish what they&#8217;ve started.</p>
<p class='rw-defn idx27' style='display:none'>If you are unflagging while doing a task, you are untiring when working upon it and do not stop until it is finished.</p>
<p class='rw-defn idx28' style='display:none'>If you are unrelenting in your desire to do something, you stop at nothing until you&#8217;ve done it.</p>
<p class='rw-defn idx29' style='display:none'>When someone vacillates, they go back and forth about a choice or opinion, unable to make a firm decision.</p>
<p class='rw-defn idx30' style='display:none'>If you waver, you cannot decide between two things because you have serious doubts about which choice is better.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>pertinacious</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='pertinacious#' id='pronounce-sound' path='audio/words/amy-pertinacious'></a>
pur-tn-AY-shuhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='pertinacious#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='pertinacious#' id='context-sound' path='audio/wordcontexts/brian-pertinacious'></a>
Lindsey Vonn&#8217;s <em>pertinacious</em> attitude towards bringing home the gold in skiing made her a model for extreme determination at the last Olympics.  She had practiced for many years with a <em>pertinacious</em> work ethic which was insistent and unyielding.  Her <em>pertinacious</em>, firm spirit paid off as she took the gold medal in the downhill, despite having to stubbornly work through an injury that would have left others calling it quits.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>pertinacious</em> attitude?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
One that is too ambitious.
</li>
<li class='choice answer '>
<span class='result'></span>
Persistent and unshakable.
</li>
<li class='choice '>
<span class='result'></span>
One that tries to win at all costs, even by cheating.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='pertinacious#' id='definition-sound' path='audio/wordmeanings/amy-pertinacious'></a>
Someone who is <em>pertinacious</em> is determined to continue doing something rather than giving up—even when it gets very difficult.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>determined</em>
</span>
</span>
</div>
<a class='quick-help' href='pertinacious#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='pertinacious#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/pertinacious/memory_hooks/5210.json'></span>
<p>
<span class="emp0"><span>Pre<span class="emp2"><span>cious</span></span> <span class="emp3"><span>Pertain</span></span>ing</span></span> Anything <span class="emp3"><span>pertain</span></span>ing to winning the Latin competition was very pre<span class="emp2"><span>cious</span></span> to Hannah; she would study and study, being <span class="emp3"><span>pertina</span></span><span class="emp2"><span>cious</span></span> in her goal for winning it, so anything <span class="emp3"><span>pertain</span></span>ing to Latin became pre<span class="emp2"><span>cious</span></span> to her.
</p>
</div>

<div id='memhook-button-bar'>
<a href="pertinacious#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/pertinacious/memory_hooks">Use other hook</a>
<a href="pertinacious#" id="memhook-use-own" url="https://membean.com/mywords/pertinacious/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='pertinacious#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
[Alexander Hamilton's] mind was energetic and <b>pertinacious</b>. He thought little of sitting over a paper till the dawn dimmed his candles.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
Predictably, [Michael] Jordan, <b>pertinacious</b> even [while] clad in a pair of _Tennis, anyone?_ white shorts, takes on all questions in these two hours and swats them away with the same ferocity that he reject[ed] a Patrick Ewing shot during the heated 1993 Eastern Conference Finals against the Knicks.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/pertinacious/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='pertinacious#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep5" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='per_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='pertinacious#'>
<span class='common'></span>
per-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='tin_hold' data-tree-url='//cdn1.membean.com/public/data/treexml' href='pertinacious#'>
<span class='common'></span>
tin
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>hold</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='acious_inclined' data-tree-url='//cdn1.membean.com/public/data/treexml' href='pertinacious#'>
<span class=''></span>
-acious
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>inclined to, abounding in</td>
</tr>
</table>
<p>When one is <em>pertinacious</em> while performing a task, one is &#8220;inclined to thoroughly hold&#8221; onto it until it is completely done.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='pertinacious#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep5" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Microcosmos</strong><span> This little fellow pertinaciously pushes his ball, no matter what gets in the way.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/pertinacious.jpg' video_url='examplevids/pertinacious' video_width='350'></span>
<div id='wt-container'>
<img alt="Pertinacious" height="198" src="https://cdn1.membean.com/video/examplevids/pertinacious.jpg" width="350" />
<div class='center'>
<a href="pertinacious#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='pertinacious#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep5" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Pertinacious" src="https://cdn0.membean.com/public/images/wordimages/cons2/pertinacious.png?qdep5" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='pertinacious#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>assiduous</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>conscientious</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>efficacy</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>exacting</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>fortitude</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>incessant</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>indefatigable</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>intractable</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>inveterate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>meticulous</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>obstinate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>rapt</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>sedulous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>tenacious</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>unflagging</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>unrelenting</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>capricious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>desultory</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>diversion</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>impetuous</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>impromptu</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>improvise</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>impulsive</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>intermittent</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>irresolute</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>procrastinate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>refractory</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>vacillate</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>waver</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='pertinacious#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep5" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
pertinacity
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>very persistent or determined</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="pertinacious" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>pertinacious</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="pertinacious#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

