
<!DOCTYPE html>
<html>
<head>
<title>Word: omnipresent | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx2' style='display:none'>A copious amount of something is a very large amount of it.</p>
<p class='rw-defn idx3' style='display:none'>If you cull items or information, you gather them from a number of different places in a selective manner.</p>
<p class='rw-defn idx4' style='display:none'>When there is a dearth of something, there is a scarcity or lack of it.</p>
<p class='rw-defn idx5' style='display:none'>When you deplete a supply of something, you use it up or lessen its amount over a given period of time.</p>
<p class='rw-defn idx6' style='display:none'>When an area is devoid of life, it is empty or completely lacking in it.</p>
<p class='rw-defn idx7' style='display:none'>When something is diffuse, it is widely spread out and scattered.</p>
<p class='rw-defn idx8' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx9' style='display:none'>An exiguous amount of something is meager, small, or limited in nature.</p>
<p class='rw-defn idx10' style='display:none'>A gamut is a complete range of things of a particular type or the full extent of possibilities of some experience or action.</p>
<p class='rw-defn idx11' style='display:none'>Something that is imperceptible is either impossible to be perceived by the mind or is very difficult to perceive.</p>
<p class='rw-defn idx12' style='display:none'>Something infinitesimal is so extremely small or minute that it is very hard to measure it.</p>
<p class='rw-defn idx13' style='display:none'>Something luxuriant, such as plants or hair, is growing well and is very healthy.</p>
<p class='rw-defn idx14' style='display:none'>A macrocosm is a large, complex, and organized system or structure that is made of many small parts that form one unit, such as a society or the universe.</p>
<p class='rw-defn idx15' style='display:none'>A pandemic disease is a far-reaching epidemic that affects people in a very wide geographic area.</p>
<p class='rw-defn idx16' style='display:none'>A panoply is a large and impressive collection of people or things.</p>
<p class='rw-defn idx17' style='display:none'>A paucity of something is not enough of it.</p>
<p class='rw-defn idx18' style='display:none'>When a substance permeates something, it enters into all parts of it.</p>
<p class='rw-defn idx19' style='display:none'>If something is pervasive, it appears to be everywhere.</p>
<p class='rw-defn idx20' style='display:none'>A plethora of something is too much of it.</p>
<p class='rw-defn idx21' style='display:none'>A preponderance of things of a particular type in a group means that there are more of that type than of any other.</p>
<p class='rw-defn idx22' style='display:none'>A profusion of something is a very large quantity or variety of it.</p>
<p class='rw-defn idx23' style='display:none'>Something or someone that is prolific is highly fruitful and so produces a lot of something.</p>
<p class='rw-defn idx24' style='display:none'>If something bad, such as crime or disease, is rampant, there is a lot of it—and it is increasing in a way that is difficult to control.</p>
<p class='rw-defn idx25' style='display:none'>To rarefy something is to make it less dense, as in oxygen at high altitudes; this word also refers to purifying or refining something, thereby making it less ordinary or commonplace.</p>
<p class='rw-defn idx26' style='display:none'>If you say that something bad or unpleasant is rife somewhere, you mean that it is very common.</p>
<p class='rw-defn idx27' style='display:none'>A scant amount of something is a very limited or slight amount of it; hence, it is inadequate or insufficient in size or quantity.</p>
<p class='rw-defn idx28' style='display:none'>If something, such as warmth, color, or liquid, suffuses a material, it gradually spreads through or covers it; if you are suffused with a feeling, you are full of that feeling.</p>
<p class='rw-defn idx29' style='display:none'>Something that is superfluous is unnecessary; it is more than what is wanted or needed at the current time.</p>
<p class='rw-defn idx30' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx31' style='display:none'>If something is ubiquitous, it seems to be everywhere.</p>
<p class='rw-defn idx32' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>
<p class='rw-defn idx33' style='display:none'>If you winnow a list or group, you reduce its size by getting rid of the things that are no longer useful or by keeping only the desirable things.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>omnipresent</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='omnipresent#' id='pronounce-sound' path='audio/words/amy-omnipresent'></a>
OM-nuh-prez-uhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='omnipresent#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='omnipresent#' id='context-sound' path='audio/wordcontexts/brian-omnipresent'></a>
Energy is <em>omnipresent</em> in preschools&#8212;it&#8217;s everywhere!  Preschool teachers feel they must be <em>omnipresent</em> or present in all places at all times so that they can really watch their students.  Young children want their teacher to be <em>omnipresent</em> or all-present because they look to them for security, and want to be able to see them whenever they look for them.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When is something <em>omnipresent</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
When it seems to exist all around you and everyone else.
</li>
<li class='choice '>
<span class='result'></span>
When it feels threatening and makes you and others afraid.
</li>
<li class='choice '>
<span class='result'></span>
When it is extremely important to your success.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='omnipresent#' id='definition-sound' path='audio/wordmeanings/amy-omnipresent'></a>
Something that is <em>omnipresent</em> appears to be everywhere at the same time or is ever-present.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>very widespread</em>
</span>
</span>
</div>
<a class='quick-help' href='omnipresent#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='omnipresent#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/omnipresent/memory_hooks/5456.json'></span>
<p>
<span class="emp0"><span>All or Ever-<span class="emp3"><span>Present</span></span></span></span> That which is omni<span class="emp3"><span>present</span></span> is all-<span class="emp3"><span>present</span></span> or ever-<span class="emp3"><span>present</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="omnipresent#" id="add-public-hook" style="" url="https://membean.com/mywords/omnipresent/memory_hooks">Use other public hook</a>
<a href="omnipresent#" id="memhook-use-own" url="https://membean.com/mywords/omnipresent/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='omnipresent#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The strong national pride was <b>omnipresent</b>: from the red, white and blue Chilean flags that permeated the rescue site to the chants that rang out as the miners rose to the surface.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
[Michael Jordan] rolled into the mid-1990s as perhaps the most recognizable face on the planet . . . He was <b>omnipresent</b>, pitching products on TV and billboards and magazines.
<cite class='attribution'>
&mdash;
NBC Sports
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/omnipresent/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='omnipresent#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='omni_all' data-tree-url='//cdn1.membean.com/public/data/treexml' href='omnipresent#'>
<span class=''></span>
omni-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>all</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pre_before' data-tree-url='//cdn1.membean.com/public/data/treexml' href='omnipresent#'>
<span class='common'></span>
pre-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>before, in front</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='sent_be' data-tree-url='//cdn1.membean.com/public/data/treexml' href='omnipresent#'>
<span class=''></span>
sent
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>be, exist, be real</td>
</tr>
</table>
<p>When one is <em>omnipresent</em>, one &#8220;exists before all,&#8221; that is, &#8220;is in front of all&#8221; in &#8220;all&#8221; places.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='omnipresent#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Vintage Tootsie Roll Advertisement</strong><span> Tootsie Rolls are omnipresent--hooray!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/omnipresent.jpg' video_url='examplevids/omnipresent' video_width='350'></span>
<div id='wt-container'>
<img alt="Omnipresent" height="288" src="https://cdn1.membean.com/video/examplevids/omnipresent.jpg" width="350" />
<div class='center'>
<a href="omnipresent#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='omnipresent#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Omnipresent" src="https://cdn2.membean.com/public/images/wordimages/cons2/omnipresent.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='omnipresent#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>copious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>diffuse</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>gamut</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>luxuriant</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>macrocosm</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>pandemic</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>panoply</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>permeate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>pervasive</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>plethora</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>preponderance</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>profusion</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>prolific</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>rampant</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>rife</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>suffuse</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>superfluous</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>ubiquitous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>cull</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>dearth</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>deplete</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>devoid</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>exiguous</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>imperceptible</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>infinitesimal</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>paucity</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>rarefy</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>scant</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>winnow</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="omnipresent" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>omnipresent</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="omnipresent#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

