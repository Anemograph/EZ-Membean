
<!DOCTYPE html>
<html>
<head>
<title>Word: writ | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you act with abandon, you give in to the impulse of the moment and behave in a wild, uncontrolled way.</p>
<p class='rw-defn idx1' style='display:none'>An abortive attempt or action is cut short before it is finished; hence, it is unsuccessful.</p>
<p class='rw-defn idx2' style='display:none'>When you adjure someone to do something, you persuade, eagerly appeal, or solemnly order them to do it.</p>
<p class='rw-defn idx3' style='display:none'>When you admonish someone, you tell them gently but with seriousness that they have done something wrong; you usually caution and advise them not to do it again.</p>
<p class='rw-defn idx4' style='display:none'>If you describe a decision, rule, or plan as arbitrary, you think that it was decided without any thought, standard, or system to guide it; therefore, it can seem unfair.</p>
<p class='rw-defn idx5' style='display:none'>An axiom is a wise saying or widely recognized general truth that is often self-evident; it can also be an established rule or law.</p>
<p class='rw-defn idx6' style='display:none'>If something is done at someone&#8217;s behest, it is done because they urgently ask for it or authoritatively order it to happen.</p>
<p class='rw-defn idx7' style='display:none'>A cardinal rule or quality is considered to be the most important or basic in a set of rules or qualities.</p>
<p class='rw-defn idx8' style='display:none'>Someone who is contumacious is purposely stubborn, contrary, or disobedient.</p>
<p class='rw-defn idx9' style='display:none'>A dictum is a saying that people often repeat because it says something interesting or wise about a subject.</p>
<p class='rw-defn idx10' style='display:none'>When you are given a directive, you are given an instruction or order that directs you to do something.</p>
<p class='rw-defn idx11' style='display:none'>A disaffected member of a group or organization is not satisfied with it; consequently, they feel little loyalty towards it.</p>
<p class='rw-defn idx12' style='display:none'>An edict is an official order or command given by a government or someone in authority.</p>
<p class='rw-defn idx13' style='display:none'>If you eschew something, you deliberately avoid doing it, especially for moral reasons.</p>
<p class='rw-defn idx14' style='display:none'>If you exhort someone to do something, you try very hard to persuade them to do it.</p>
<p class='rw-defn idx15' style='display:none'>A small group or issue that is factious is controversial and promotes strong disagreement or dissatisfaction within a larger group.</p>
<p class='rw-defn idx16' style='display:none'>A fiat is an official order from a person or group in authority.</p>
<p class='rw-defn idx17' style='display:none'>If someone is fractious, they are easily upset or annoyed over unimportant things.</p>
<p class='rw-defn idx18' style='display:none'>When it is imperative that something be done, it is absolutely necessary or very important that it be accomplished.</p>
<p class='rw-defn idx19' style='display:none'>An injunction is a court order that prevents someone from doing something.</p>
<p class='rw-defn idx20' style='display:none'>An interdict is an official order that prevents someone from doing something.</p>
<p class='rw-defn idx21' style='display:none'>A mandate is an official order or command issued by a government or other authorized body.</p>
<p class='rw-defn idx22' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx23' style='display:none'>A peremptory action, such as a decree or demand, is authoritative and absolute; therefore, it is not open to debate but must be carried out.</p>
<p class='rw-defn idx24' style='display:none'>A precept is a rule or principle that teaches correct behavior.</p>
<p class='rw-defn idx25' style='display:none'>When someone proscribes an activity, they prohibit it by establishing a rule against it.</p>
<p class='rw-defn idx26' style='display:none'>A recalcitrant animal or person is difficult to control and refuses to obey orders—even after stiff punishment.</p>
<p class='rw-defn idx27' style='display:none'>Refractory people deliberately don&#8217;t obey someone in authority and so are difficult to deal with or control.</p>
<p class='rw-defn idx28' style='display:none'>If you refrain from doing something, you do not do it—even though you want to.</p>
<p class='rw-defn idx29' style='display:none'>If something is requisite for a purpose, it is needed, appropriate, or necessary for that specific purpose.</p>
<p class='rw-defn idx30' style='display:none'>Sedition is the act of encouraging people to disobey and oppose the government currently in power.</p>
<p class='rw-defn idx31' style='display:none'>Something statutory, such as the power given to a governor or president, is created, established, and controlled by rules and laws; hence, it has the full force of the law behind it and must be followed.</p>
<p class='rw-defn idx32' style='display:none'>Something that is superfluous is unnecessary; it is more than what is wanted or needed at the current time.</p>
<p class='rw-defn idx33' style='display:none'>A tenet is a belief held by a group, organization, or person.</p>
<p class='rw-defn idx34' style='display:none'>A touchstone is the standard or best example by which the value or quality of something else is judged.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>writ</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='writ#' id='pronounce-sound' path='audio/words/amy-writ'></a>
rit
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='writ#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='writ#' id='context-sound' path='audio/wordcontexts/brian-writ'></a>
After being sealed with the wax impression of the queen&#8217;s seal, the <em>writ</em> declaring increased taxes became law.  Once the <em>writ</em> had passed her majesty&#8217;s approval and had been formalized in ink and parchment, no act of Parliament could undo the deed.  Subjects of that monarchy were thereby obligated to obey the <em>writ</em> as law, whether they knew of its existence or not.  They were to follow its command as if it were a sacred text, or holy <em>writ</em>.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When might you receive a <em>writ</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
If you&#8217;ve registered to run for public office.
</li>
<li class='choice '>
<span class='result'></span>
If you&#8217;ve just moved to a new address.
</li>
<li class='choice answer '>
<span class='result'></span>
If you&#8217;ve been ordered to appear in court.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='writ#' id='definition-sound' path='audio/wordmeanings/amy-writ'></a>
A <em>writ</em> is an official document that orders a person to do something.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>court order</em>
</span>
</span>
</div>
<a class='quick-help' href='writ#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='writ#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/writ/memory_hooks/6162.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Writ</span></span> <span class="emp1"><span>Writ</span></span>ten</span></span> "Holy smokes!  That new <span class="emp1"><span>writ</span></span>, just <span class="emp1"><span>writ</span></span>ten yesterday, declares that all <span class="emp1"><span>writ</span></span>ers must show their <span class="emp1"><span>writ</span></span>ing to the government first.  What an unjust, repressive <span class="emp1"><span>writ</span></span>--it should have been covered in <span class="emp1"><span>red</span></span> ink!"
</p>
</div>

<div id='memhook-button-bar'>
<a href="writ#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/writ/memory_hooks">Use other hook</a>
<a href="writ#" id="memhook-use-own" url="https://membean.com/mywords/writ/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='writ#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
[President Elpidio] Quirino’s government thereupon announced that the <b>writ</b> would be operative in 15 of the republic’s 51 provinces, where "the condition of peace and order is relatively normal."
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
<li>
A medieval innkeeper, for example, often offered the only lodging in town; a boatman could cross only with the king’s <b>writ</b>.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/writ/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='writ#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='writ_write' data-tree-url='//cdn1.membean.com/public/data/treexml' href='writ#'>
<span class=''></span>
writ
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>write</td>
</tr>
</table>
<p>A <em>writ</em> is an order &#8220;written&#8221; by a court or other legal authority.</p>
<a href="writ#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> Old English</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='writ#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Writ" src="https://cdn1.membean.com/public/images/wordimages/cons2/writ.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='writ#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>adjure</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>admonish</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>axiom</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>behest</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>cardinal</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>dictum</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>directive</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>edict</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>exhort</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>fiat</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>imperative</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>injunction</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>interdict</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>mandate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>peremptory</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>precept</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>proscribe</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>requisite</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>statutory</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>tenet</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>touchstone</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abandon</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abortive</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>arbitrary</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>contumacious</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>disaffected</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>eschew</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>factious</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>fractious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>recalcitrant</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>refractory</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>refrain</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>sedition</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>superfluous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="writ" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>writ</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="writ#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

