
<!DOCTYPE html>
<html>
<head>
<title>Word: stagnant | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Stagnant-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/stagnant-large.jpg?qdep8" />
</div>
<a href="stagnant#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>If you do something with alacrity, you do it eagerly and quickly.</p>
<p class='rw-defn idx1' style='display:none'>Apathy is a lack of interest or unenthusiastic involvement in an activity; there is no effort to change or improve it at all.</p>
<p class='rw-defn idx2' style='display:none'>Something that is arduous is extremely difficult; hence, it involves a lot of effort.</p>
<p class='rw-defn idx3' style='display:none'>Something brackish is distasteful and unpleasant usually because something else is mixed in with it; brackish water, for instance, is a mixture of fresh and salt water and cannot be drunk.</p>
<p class='rw-defn idx4' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx5' style='display:none'>A catalyst is an agent that enacts change, such as speeding up a chemical reaction or causing an event to occur.</p>
<p class='rw-defn idx6' style='display:none'>A catatonic person is in a state of suspended action; therefore, they are rigid, immobile, and unresponsive.</p>
<p class='rw-defn idx7' style='display:none'>When you cavort, you jump and dance around in a playful, excited, or physically lively way.</p>
<p class='rw-defn idx8' style='display:none'>If something moves or grows with celerity, it does so rapidly.</p>
<p class='rw-defn idx9' style='display:none'>When something is dormant, it is in a state of sleep or temporarily not active.</p>
<p class='rw-defn idx10' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx11' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is effusive expresses happiness, pleasure, admiration, praise, etc., in an extremely enthusiastic way.</p>
<p class='rw-defn idx13' style='display:none'>If something enervates you, it makes you very tired and weak—almost to the point of collapse.</p>
<p class='rw-defn idx14' style='display:none'>Ennui is the feeling of being bored, tired, and dissatisfied with life.</p>
<p class='rw-defn idx15' style='display:none'>If you show exuberance, you display great excitement, energy, and enthusiasm.</p>
<p class='rw-defn idx16' style='display:none'>If someone gambols, they run, jump, and play like a young child or animal.</p>
<p class='rw-defn idx17' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx18' style='display:none'>Something inanimate is dead, motionless, or not living; therefore, it does not display the same traits as an active and alive organism.</p>
<p class='rw-defn idx19' style='display:none'>Something that is incessant continues on for a long time without stopping.</p>
<p class='rw-defn idx20' style='display:none'>If someone is indefatigable, they never shows signs of getting tired.</p>
<p class='rw-defn idx21' style='display:none'>An indolent person is lazy.</p>
<p class='rw-defn idx22' style='display:none'>When something is inert, it has no power of movement or is inactive.</p>
<p class='rw-defn idx23' style='display:none'>When someone is described as itinerant, they are characterized by traveling or moving about from place to place.</p>
<p class='rw-defn idx24' style='display:none'>Something kinetic is moving, active, and using energy.</p>
<p class='rw-defn idx25' style='display:none'>If you do something in a lackadaisical way, you do it carelessly and without putting much effort into it—thereby showing that you are not really interested in what you&#8217;re doing.</p>
<p class='rw-defn idx26' style='display:none'>A languid person is slow, relaxed, and shows little energy or interest in doing anything.</p>
<p class='rw-defn idx27' style='display:none'>Lassitude is a state of tiredness, lack of energy, and having little interest in what&#8217;s going on around you.</p>
<p class='rw-defn idx28' style='display:none'>If you are lethargic, you are tired, lack energy, and are unwilling to exert effort.</p>
<p class='rw-defn idx29' style='display:none'>When you are listless, you lack energy and interest and are unwilling to exert any effort.</p>
<p class='rw-defn idx30' style='display:none'>When you meander, you either wander about with no particular goal or follow a path that twists and turns a great deal.</p>
<p class='rw-defn idx31' style='display:none'>If you describe something as moribund, you imply that it is no longer effective; it may also be coming to the end of its useful existence.</p>
<p class='rw-defn idx32' style='display:none'>A peregrination is a long journey or act of traveling from place to place, especially by foot.</p>
<p class='rw-defn idx33' style='display:none'>If someone leads a peripatetic life, they travel from place to place, living and working only for a short time in each place before moving on.</p>
<p class='rw-defn idx34' style='display:none'>Someone who is phlegmatic stays calm and unemotional even in dangerous or exciting situations.</p>
<p class='rw-defn idx35' style='display:none'>A putrid substance is decaying or rotting; therefore, it is also foul and stinking.</p>
<p class='rw-defn idx36' style='display:none'>A state of quiescence is one of quiet and restful inaction.</p>
<p class='rw-defn idx37' style='display:none'>Someone who has a sedentary habit, job, or lifestyle spends a lot of time sitting down without moving or exercising often.</p>
<p class='rw-defn idx38' style='display:none'>If you are somnolent, you are sleepy.</p>
<p class='rw-defn idx39' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx40' style='display:none'>Stupor is a state in which someone&#8217;s mind and senses are dulled; consequently, they are unable to think clearly or act normally, usually due to the use of alcohol or drugs.</p>
<p class='rw-defn idx41' style='display:none'>If your body is affected by torpor, you are severely lacking in energy; therefore, you are idle—and can even be numb.</p>
<p class='rw-defn idx42' style='display:none'>A vibrant person is lively and full of energy in a way that is exciting and attractive.</p>
<p class='rw-defn idx43' style='display:none'>If someone is described as vivacious, they are lively and have a happy, lighthearted manner.</p>
<p class='rw-defn idx44' style='display:none'>Someone who is zealous spends a lot of time, energy, and effort to support something—  notably that of a political or religious nature—because they believe in it very strongly.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>stagnant</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='stagnant#' id='pronounce-sound' path='audio/words/amy-stagnant'></a>
STAYG-nuhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='stagnant#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='stagnant#' id='context-sound' path='audio/wordcontexts/brian-stagnant'></a>
Myron&#8217;s mind was made <em>stagnant</em> or inactive from watching too many television shows.  Over the years his once active and quick mind became <em>stagnant</em> and still; no original thoughts arose at all, and he was unable to think for himself.  His mind reminded me of a <em>stagnant</em> swamp in which the motionless water is choked with weeds and has a foul odor arising from it.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If someone&#8217;s career is <em>stagnant</em>, what is it?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
It is not presenting opportunities for growth and change.
</li>
<li class='choice '>
<span class='result'></span>
It is not viewed by others as a realistic way to make money.
</li>
<li class='choice '>
<span class='result'></span>
It is marked by repeated success and advancement.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='stagnant#' id='definition-sound' path='audio/wordmeanings/amy-stagnant'></a>
Something that is <em>stagnant</em> is not moving; therefore, it is not growing, progressing, or acting as it should.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>still</em>
</span>
</span>
</div>
<a class='quick-help' href='stagnant#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='stagnant#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/stagnant/memory_hooks/5690.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Sta</span></span>le <span class="emp2"><span>Gnat</span></span></span></span> Gary the <span class="emp2"><span>gnat</span></span> wasn't progressing in his life at all, but was one <span class="emp3"><span>sta</span></span><span class="emp2"><span>gnant</span></span>, <span class="emp3"><span>sta</span></span>le <span class="emp2"><span>gnat</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="stagnant#" id="add-public-hook" style="" url="https://membean.com/mywords/stagnant/memory_hooks">Use other public hook</a>
<a href="stagnant#" id="memhook-use-own" url="https://membean.com/mywords/stagnant/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='stagnant#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Iron rusts from disuse; water loses its purity from stagnation . . . even so does inaction sap the vigor of the mind.
<span class='attribution'>&mdash; Leonardo da Vinci, Italian painter, sculptor, architect, and engineer</span>
<img alt="Leonardo da vinci, italian painter, sculptor, architect, and engineer" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Leonardo da Vinci, Italian painter, sculptor, architect, and engineer.jpg?qdep8" width="80" />
</li>
<li>
The Wolves' offense grew <b>stagnant</b> in the fourth quarter as the Spurs made their run, and you could feel them tighten up as the score got closer and closer.
<cite class='attribution'>
&mdash;
ESPN
</cite>
</li>
<li>
The success of _Songs in A Minor_ has been refreshing in an otherwise <b>stagnant</b> chart, as music sales continue to sputter through a cruel, cruel summer.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
<li>
The workers quoted in the report complained about inhumane schedules requiring regular overtime work, <b>stagnant</b> wages and inflating dorm fees for factory beds, according to [_GlobalPost_], who has been investigating Asian supply chains.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/stagnant/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='stagnant#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>stagn</td>
<td>
&rarr;
</td>
<td class='meaning'>pool of standing water</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ant_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='stagnant#'>
<span class=''></span>
-ant
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p>Something <em>stagnant</em> is in a &#8220;condition (like) a pool of standing water.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='stagnant#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Gary, Indiana</strong><span> Gary, Indiana now has a stagnant economy.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/stagnant.jpg' video_url='examplevids/stagnant' video_width='350'></span>
<div id='wt-container'>
<img alt="Stagnant" height="288" src="https://cdn1.membean.com/video/examplevids/stagnant.jpg" width="350" />
<div class='center'>
<a href="stagnant#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='stagnant#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Stagnant" src="https://cdn2.membean.com/public/images/wordimages/cons2/stagnant.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='stagnant#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>apathy</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>brackish</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>catatonic</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>dormant</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>enervate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>ennui</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>inanimate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>indolent</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>inert</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>lackadaisical</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>languid</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>lassitude</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>lethargic</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>listless</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>moribund</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>phlegmatic</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>putrid</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>quiescence</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>sedentary</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>somnolent</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>stupor</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>torpor</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>alacrity</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>arduous</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>catalyst</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>cavort</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>celerity</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>effusive</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>exuberance</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>gambol</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>incessant</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>indefatigable</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>itinerant</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>kinetic</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>meander</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>peregrination</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>peripatetic</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>vibrant</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>vivacious</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>zealous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="stagnant" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>stagnant</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="stagnant#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

