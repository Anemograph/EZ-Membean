
<!DOCTYPE html>
<html>
<head>
<title>Word: irrefutable | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you describe something as ambiguous, you mean that it is unclear or confusing because it can be understood in multiple ways.</p>
<p class='rw-defn idx1' style='display:none'>A bastion is considered an important and effective defense of a way of life or principle; a bastion can also refer to a heavily fortified place, such as a castle.</p>
<p class='rw-defn idx2' style='display:none'>When you bolster something or someone, you offer support in a moment of need.</p>
<p class='rw-defn idx3' style='display:none'>When you buttress an argument, idea, or even a building, you make it stronger by providing support.</p>
<p class='rw-defn idx4' style='display:none'>If something confounds you, it makes you feel surprised and confused, often because it does not meet your predefined expectations.</p>
<p class='rw-defn idx5' style='display:none'>To confute an argument is to prove it to be thoroughly false; to confute a person is to prove them to be wrong.</p>
<p class='rw-defn idx6' style='display:none'>A conjecture is a theory or guess that is based on information that is not certain or complete.</p>
<p class='rw-defn idx7' style='display:none'>To contravene a law, rule, or agreement is to do something that is not allowed or is forbidden by that law, rule, or agreement.</p>
<p class='rw-defn idx8' style='display:none'>To corroborate something that has been said or reported means to give information or evidence that further supports it.</p>
<p class='rw-defn idx9' style='display:none'>A definitive opinion on an issue cannot be challenged; therefore, it is the final word or the most authoritative pronouncement on that issue.</p>
<p class='rw-defn idx10' style='display:none'>If someone makes a dubious claim, there is a great deal of disbelief or doubt about it.</p>
<p class='rw-defn idx11' style='display:none'>Empirical evidence or study is based on real experience or scientific experiments rather than on unproven theories.</p>
<p class='rw-defn idx12' style='display:none'>When people equivocate, they avoid making a clear statement; they are deliberately vague in order to avoid giving a direct answer to a question.</p>
<p class='rw-defn idx13' style='display:none'>If you espouse an idea, principle, or belief, you support it wholeheartedly.</p>
<p class='rw-defn idx14' style='display:none'>A fallacy is an idea or belief that is false.</p>
<p class='rw-defn idx15' style='display:none'>If you gainsay something, you say that it is not true and therefore reject it.</p>
<p class='rw-defn idx16' style='display:none'>Something that is implausible is unlikely to be true or hard to believe that it&#8217;s true.</p>
<p class='rw-defn idx17' style='display:none'>Something that is inconceivable cannot be imagined or thought of; that is, it is beyond reason or unbelievable.</p>
<p class='rw-defn idx18' style='display:none'>If the results of something are inconclusive, they are uncertain or provide no final answers.</p>
<p class='rw-defn idx19' style='display:none'>Incontrovertible facts are certain, unquestionably true, and impossible to doubt however hard you might try to convince yourself otherwise.</p>
<p class='rw-defn idx20' style='display:none'>Something that is ineluctable is impossible to avoid or escape, however much you try.</p>
<p class='rw-defn idx21' style='display:none'>An inference is a conclusion that is reached using available data.</p>
<p class='rw-defn idx22' style='display:none'>An irrevocable action or decision is impossible to change, reverse, or stop.</p>
<p class='rw-defn idx23' style='display:none'>A mendacious person does not tell the truth.</p>
<p class='rw-defn idx24' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx25' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx26' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx27' style='display:none'>To parry is to ward something off or deflect it.</p>
<p class='rw-defn idx28' style='display:none'>If you prevaricate, you avoid giving a direct or honest answer, usually because you want to hide the truth or want to delay or avoid making a hard decision.</p>
<p class='rw-defn idx29' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx30' style='display:none'>If you repudiate something, you state that you do not accept or agree with it and no longer want to be connected with it in any way.</p>
<p class='rw-defn idx31' style='display:none'>When you give a retort to what someone has said, you reply in a quick and witty fashion.</p>
<p class='rw-defn idx32' style='display:none'>A riposte is a quick and clever reply that is often made in answer to criticism of some kind.</p>
<p class='rw-defn idx33' style='display:none'>A subjective opinion is not based upon facts or hard evidence; rather, it rests upon someone&#8217;s personal feelings or beliefs about a matter or concern.</p>
<p class='rw-defn idx34' style='display:none'>Substantive issues are the most important, serious, and real issues of a subject.</p>
<p class='rw-defn idx35' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx36' style='display:none'>Something that is veritable is authentic, actual, genuine, or without doubt.</p>
<p class='rw-defn idx37' style='display:none'>The verity of something is the truth or reality of it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>irrefutable</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='irrefutable#' id='pronounce-sound' path='audio/words/amy-irrefutable'></a>
ir-i-FYOO-tuh-buhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='irrefutable#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='irrefutable#' id='context-sound' path='audio/wordcontexts/brian-irrefutable'></a>
Even before the mathematical proof of Sir Isaac Newton, gravity has been an <em>irrefutable</em>, unquestionable force on the Earth.  Gravity&#8217;s influence cannot be proven nonexistent and so remains one of the certain, <em>irrefutable</em> forces within the study of physics.  Gravity becomes especially undeniable or <em>irrefutable</em> when one takes a big fall on hard concrete.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What would likely be considered <em>irrefutable</em> evidence?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A clear picture that shows someone stealing the mayor&#8217;s bike outside city hall.
</li>
<li class='choice '>
<span class='result'></span>
Statements from three people who somewhat disagree about who stole the mayor&#8217;s bike.
</li>
<li class='choice '>
<span class='result'></span>
A long angry letter from the mayor accusing a specific person of stealing his bike.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='irrefutable#' id='definition-sound' path='audio/wordmeanings/amy-irrefutable'></a>
An <em>irrefutable</em> argument or statement cannot be proven wrong; therefore, it must be accepted because it is certain.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>beyond dispute</em>
</span>
</span>
</div>
<a class='quick-help' href='irrefutable#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='irrefutable#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/irrefutable/memory_hooks/4581.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Rea</span></span>l <span class="emp2"><span>Futb</span></span>o<span class="emp2"><span>l</span></span> Des<span class="emp1"><span>ire</span></span></span></span> It is an <span class="emp1"><span>ir</span></span><span class="emp3"><span>r</span></span><span class="emp1"><span>e</span></span><span class="emp2"><span>fut</span></span><span class="emp3"><span>a</span></span><span class="emp2"><span>bl</span></span><span class="emp3"><span>e</span></span> fact that I have a <span class="emp3"><span>rea</span></span>l des<span class="emp1"><span>ire</span></span> to play <span class="emp2"><span>futb</span></span>o<span class="emp2"><span>l</span></span>; ever since I was young I wanted to do nothing else, and I still play <span class="emp2"><span>futb</span></span>o<span class="emp2"><span>l</span></span> with r<span class="emp3"><span>eal</span></span> des<span class="emp1"><span>ire</span></span> today.  Note: <em><span class="emp2"><span>futb</span></span>o<span class="emp2"><span>l</span></span></em> is the Spanish word for European <span class="emp2"><span>f</span></span>oo<span class="emp2"><span>tb</span></span>a<span class="emp2"><span>l</span></span>l, or American soccer.
</p>
</div>

<div id='memhook-button-bar'>
<a href="irrefutable#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/irrefutable/memory_hooks">Use other hook</a>
<a href="irrefutable#" id="memhook-use-own" url="https://membean.com/mywords/irrefutable/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='irrefutable#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
To the young chemist and neurologist-to-be, this grand display was an <b>irrefutable</b> confirmation that there was order underlying the apparent chaos of the universe, and that the human mind had been keen enough to perceive it.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
The Russian Army provides only 15 standard sizes, and Germany has only a third as many as the U.S. The U.S. theory is logically <b>irrefutable</b>: by providing a multiplicity of [shoe] sizes, more pairs of feet can be drafted.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/irrefutable/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='irrefutable#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ir_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='irrefutable#'>
<span class=''></span>
ir-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='re_back' data-tree-url='//cdn1.membean.com/public/data/treexml' href='irrefutable#'>
<span class='common'></span>
re-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>back, again</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='fut_check' data-tree-url='//cdn1.membean.com/public/data/treexml' href='irrefutable#'>
<span class=''></span>
fut
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>check, restrain</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='able_handy' data-tree-url='//cdn1.membean.com/public/data/treexml' href='irrefutable#'>
<span class=''></span>
-able
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>handy, capable of</td>
</tr>
</table>
<p><em>Irrefutable</em> evidence is &#8220;not capable of being checked or restrained again&#8221; because there is no possibility of proof against it.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='irrefutable#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Fringe</strong><span> The doctor's claim is irrefutable.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/irrefutable.jpg' video_url='examplevids/irrefutable' video_width='350'></span>
<div id='wt-container'>
<img alt="Irrefutable" height="288" src="https://cdn1.membean.com/video/examplevids/irrefutable.jpg" width="350" />
<div class='center'>
<a href="irrefutable#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='irrefutable#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Irrefutable" src="https://cdn0.membean.com/public/images/wordimages/cons2/irrefutable.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='irrefutable#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>bastion</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bolster</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>buttress</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>corroborate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>definitive</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>empirical</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>espouse</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>incontrovertible</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>ineluctable</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>inference</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>irrevocable</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>substantive</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>veritable</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>ambiguous</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>confound</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>confute</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>conjecture</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>contravene</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>dubious</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>equivocate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>fallacy</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>gainsay</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>implausible</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>inconceivable</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>inconclusive</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>mendacious</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>parry</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>prevaricate</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>repudiate</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>retort</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>riposte</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>subjective</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='irrefutable#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
refute
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>disprove</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="irrefutable" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>irrefutable</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="irrefutable#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

