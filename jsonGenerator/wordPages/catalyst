
<!DOCTYPE html>
<html>
<head>
<title>Word: catalyst | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>When you abet someone, you help them in a criminal or illegal act.</p>
<p class='rw-defn idx2' style='display:none'>If someone in authority abolishes a law or practice, they formally put an end to it.</p>
<p class='rw-defn idx3' style='display:none'>An abortive attempt or action is cut short before it is finished; hence, it is unsuccessful.</p>
<p class='rw-defn idx4' style='display:none'>You allay someone&#8217;s fear or concern by making them feel less afraid or worried.</p>
<p class='rw-defn idx5' style='display:none'>If you alleviate pain or suffering, you make it less intense or severe.</p>
<p class='rw-defn idx6' style='display:none'>Your antagonist is an opponent in a competition or battle.</p>
<p class='rw-defn idx7' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx8' style='display:none'>To augment something is to increase its value or effectiveness by adding something to it.</p>
<p class='rw-defn idx9' style='display:none'>If you circumvent something, such as a rule or restriction, you try to get around it in a clever and perhaps dishonest way.</p>
<p class='rw-defn idx10' style='display:none'>If something, such as a sound, is undergoing a crescendo, it is getting louder and louder or increasing in intensity and is about to reach its peak strength.</p>
<p class='rw-defn idx11' style='display:none'>Debility is a state of being physically or mentally weak, usually due to illness.</p>
<p class='rw-defn idx12' style='display:none'>A deterrent keeps someone from doing something against you.</p>
<p class='rw-defn idx13' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx14' style='display:none'>A disincentive to do something does not encourage you to do that thing; rather, it restrains and hinders you from doing it.</p>
<p class='rw-defn idx15' style='display:none'>Dissension is a disagreement or difference of opinion among a group of people that can cause conflict.</p>
<p class='rw-defn idx16' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx17' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx18' style='display:none'>Efficacy is the ability or power to produce an expected effect or result.</p>
<p class='rw-defn idx19' style='display:none'>A small group or issue that is factious is controversial and promotes strong disagreement or dissatisfaction within a larger group.</p>
<p class='rw-defn idx20' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx21' style='display:none'>When you galvanize someone, you cause them to improve a situation, solve a problem, or take some other action by making them feel excited, afraid, or angry.</p>
<p class='rw-defn idx22' style='display:none'>A hindrance is an obstacle or obstruction that gets in your way and prevents you from doing something.</p>
<p class='rw-defn idx23' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx24' style='display:none'>An impediment is something that blocks or obstructs progress; it can also be a weakness or disorder, such as having difficulty with speaking.</p>
<p class='rw-defn idx25' style='display:none'>An incendiary device causes objects to catch on fire; incendiary comments can cause riots to flare up.</p>
<p class='rw-defn idx26' style='display:none'>To induce a state is to bring it about; to induce someone to do something is to persuade them to do it.</p>
<p class='rw-defn idx27' style='display:none'>An inflammable substance or person&#8217;s temper is easily set on fire.</p>
<p class='rw-defn idx28' style='display:none'>Something that inhibits you from doing something restricts or keeps you from doing it.</p>
<p class='rw-defn idx29' style='display:none'>When you instigate something, you start it or stir it up, usually for the purpose of causing trouble of some kind.</p>
<p class='rw-defn idx30' style='display:none'>An insurrection is a rebellion or open uprising against an established form of government.</p>
<p class='rw-defn idx31' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx32' style='display:none'>If you mollify someone, you say or do something to make that person feel less angry or upset.</p>
<p class='rw-defn idx33' style='display:none'>If you describe something as moribund, you imply that it is no longer effective; it may also be coming to the end of its useful existence.</p>
<p class='rw-defn idx34' style='display:none'>If something is obsolescent, it is slowly becoming no longer needed because something newer or more effective has been invented.</p>
<p class='rw-defn idx35' style='display:none'>If something, such as a road, is occluded, it has been closed off or blocked; therefore, cars are prevented from continuing on their way until the road is opened once again.</p>
<p class='rw-defn idx36' style='display:none'>If you perpetrate something, you commit a crime or do some other bad thing for which you are responsible.</p>
<p class='rw-defn idx37' style='display:none'>When you preclude something from happening, you prevent it from doing so.</p>
<p class='rw-defn idx38' style='display:none'>To quell something is to stamp out, quiet, or overcome it.</p>
<p class='rw-defn idx39' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx40' style='display:none'>If you recant, you publicly announce that your once firmly held beliefs or statements were wrong and that you no longer agree with them.</p>
<p class='rw-defn idx41' style='display:none'>If you refrain from doing something, you do not do it—even though you want to.</p>
<p class='rw-defn idx42' style='display:none'>Sedition is the act of encouraging people to disobey and oppose the government currently in power.</p>
<p class='rw-defn idx43' style='display:none'>Something that is stagnant is not moving; therefore, it is not growing, progressing, or acting as it should.</p>
<p class='rw-defn idx44' style='display:none'>If you are staid, you are set in your ways; consequently, you are settled, serious, law-abiding, conservative, and traditional—and perhaps even a tad dull.</p>
<p class='rw-defn idx45' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx46' style='display:none'>Something that stymies you presents an obstacle that prevents you from doing what you need or want to do.</p>
<p class='rw-defn idx47' style='display:none'>Synergy is the extra energy or additional effectiveness gained when two groups or organizations combine their efforts instead of working separately.</p>
<p class='rw-defn idx48' style='display:none'>When you thwart someone&#8217;s ambitions, you obstruct or stop them from happening.</p>
<p class='rw-defn idx49' style='display:none'>If your body is affected by torpor, you are severely lacking in energy; therefore, you are idle—and can even be numb.</p>
<p class='rw-defn idx50' style='display:none'>A tumultuous event or period of time is filled with great excitement, confusion, or violence; a tumultuous reaction to something is likewise very loud and noisy because people are happy and excited.</p>
<p class='rw-defn idx51' style='display:none'>If you feel unrequited love for another, you love that person, but they don&#8217;t love you in return.</p>
<p class='rw-defn idx52' style='display:none'>When something whets your appetite, it increases your desire for it, especially by giving you a little idea or pleasing hint of what it is like.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>catalyst</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='catalyst#' id='pronounce-sound' path='audio/words/amy-catalyst'></a>
KAT-l-ist
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='catalyst#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='catalyst#' id='context-sound' path='audio/wordcontexts/brian-catalyst'></a>
Natural disasters can be a <em>catalyst</em> or stimulus for change; seeing the power of nature can encourage communities to improve their emergency systems.  In Scottsville, a terrible flood was the <em>catalyst</em> or impulse that encouraged the town to finally put in a levee to better protect the town against flooding.  Disasters can also be a <em>catalyst</em> in bringing about community members to help each other out and work together for the good of everyone.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>catalyst</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
It is something that causes change to occur.
</li>
<li class='choice '>
<span class='result'></span>
It is a conclusion drawn from a large number of data.
</li>
<li class='choice '>
<span class='result'></span>
It is a message sent through a series of hidden channels.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='catalyst#' id='definition-sound' path='audio/wordmeanings/amy-catalyst'></a>
A <em>catalyst</em> is an agent that enacts change, such as speeding up a chemical reaction or causing an event to occur.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>stimulus</em>
</span>
</span>
</div>
<a class='quick-help' href='catalyst#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='catalyst#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/catalyst/memory_hooks/5921.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Cat</span></span> <span class="emp2"><span>List</span></span></span></span> The <span class="emp3"><span>cat</span></span>a<span class="emp2"><span>lyst</span></span> for making a detailed <span class="emp3"><span>cat</span></span> <span class="emp2"><span>list</span></span> came from the director of the animal shelter who could not believe we didn't know how many <span class="emp3"><span>cat</span></span>s we had!
</p>
</div>

<div id='memhook-button-bar'>
<a href="catalyst#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/catalyst/memory_hooks">Use other hook</a>
<a href="catalyst#" id="memhook-use-own" url="https://membean.com/mywords/catalyst/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='catalyst#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
[T]he company emphasized it was not blaming anyone but itself. "The Microsoft Update patches were merely a <b>catalyst—a</b> trigger—for a series of events that led to the disruption of Skype, not the root cause of it."
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
"Shrimp shell is an excellent raw material for the preparation of [a fuel] <b>catalyst</b>, due to its wide source, low price, favorable biodegradability, and environment-friendly property," the study authors wrote.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
In many biological and industrial processes, reactions must proceed rapidly or they are useless. That is why <b>catalysts</b> are so important. These agents hasten reactions, without themselves being consumed by them. This neat trick [using enzymes] also makes them very cost-effective.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Bear markets usually need three things to turn round. Valuations need to be depressed (arguably true of equities and corporate bonds). Forced sellers need to have been dumping assets on the market (hedge funds and banks are two examples). These first two are necessary but not sufficient conditions. The third is a <b>catalyst</b>. Any one of this year’s rescues, from Bear Stearns on, might have been that <b>catalyst—but</b> not one of them has yet done the trick.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/catalyst/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='catalyst#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='cata_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='catalyst#'>
<span class=''></span>
cata-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ly_untie' data-tree-url='//cdn1.membean.com/public/data/treexml' href='catalyst#'>
<span class=''></span>
ly
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>loosen, untie</td>
</tr>
</table>
<p>A <em>catalyst</em> &#8220;thoroughly loosens&#8221; a situation; it also &#8220;loosens&#8221; up chemicals so as to get a reaction moving.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='catalyst#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Stranger than Fiction</strong><span> Harold's conservative ways strangely enough act as a catalyst.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/catalyst.jpg' video_url='examplevids/catalyst' video_width='350'></span>
<div id='wt-container'>
<img alt="Catalyst" height="288" src="https://cdn1.membean.com/video/examplevids/catalyst.jpg" width="350" />
<div class='center'>
<a href="catalyst#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='catalyst#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Catalyst" src="https://cdn3.membean.com/public/images/wordimages/cons2/catalyst.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='catalyst#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>abet</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>antagonist</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>augment</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>crescendo</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>dissension</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>efficacy</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>factious</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>galvanize</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>incendiary</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>induce</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>inflammable</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>instigate</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>insurrection</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>perpetrate</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>sedition</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>synergy</span> &middot;</li><li data-idx='50' class = 'rw-wordform notlearned'><span>tumultuous</span> &middot;</li><li data-idx='52' class = 'rw-wordform notlearned'><span>whet</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abolish</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>abortive</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>allay</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>alleviate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>circumvent</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>debility</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>deterrent</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>disincentive</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>hindrance</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>impediment</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>inhibit</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>mollify</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>moribund</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>obsolescent</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>occlude</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>preclude</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>quell</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>recant</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>refrain</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>stagnant</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>staid</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>stymie</span> &middot;</li><li data-idx='48' class = 'rw-wordform notlearned'><span>thwart</span> &middot;</li><li data-idx='49' class = 'rw-wordform notlearned'><span>torpor</span> &middot;</li><li data-idx='51' class = 'rw-wordform notlearned'><span>unrequited</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="catalyst" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>catalyst</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="catalyst#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

