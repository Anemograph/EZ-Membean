
<!DOCTYPE html>
<html>
<head>
<title>Word: lissome | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you are adept at something, you are very good, skillful, or talented at it.</p>
<p class='rw-defn idx1' style='display:none'>Someone who is adroit is skillful and clever, especially in thought, behavior, or action.</p>
<p class='rw-defn idx2' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx3' style='display:none'>The choreography of a work of dance is either the art of arranging the steps or the actual dance composition itself.</p>
<p class='rw-defn idx4' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx5' style='display:none'>The word corporeal refers to the physical or material world rather than the spiritual; it also means characteristic of the body rather than the mind or feelings.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is corpulent is extremely fat.</p>
<p class='rw-defn idx7' style='display:none'>A deft movement or action is made quickly and with skill.</p>
<p class='rw-defn idx8' style='display:none'>Efficacy is the ability or power to produce an expected effect or result.</p>
<p class='rw-defn idx9' style='display:none'>A person or animal that is emaciated is extremely thin because of a serious illness or lack of food.</p>
<p class='rw-defn idx10' style='display:none'>Something ethereal has a delicate beauty that makes it seem not part of the real world.</p>
<p class='rw-defn idx11' style='display:none'>If someone exhibits finesse in something, they do it with great skill and care; this most often refers to handling difficult situations that might easily offend people.</p>
<p class='rw-defn idx12' style='display:none'>A gauche person behaves in an awkward and unsuitable way in public because they lack social skills.</p>
<p class='rw-defn idx13' style='display:none'>A hapless person is unlucky, so much so that a stream of unfortunate things has happened and continues to happen to them.</p>
<p class='rw-defn idx14' style='display:none'>Someone who is limber can stretch, move, or bend themselves in many different directions or ways.</p>
<p class='rw-defn idx15' style='display:none'>Someone with a lithe body can move easily and gracefully.</p>
<p class='rw-defn idx16' style='display:none'>Someone who is malleable is easily influenced or controlled by other people.</p>
<p class='rw-defn idx17' style='display:none'>A person who is plastic can be easily influenced and molded by others.</p>
<p class='rw-defn idx18' style='display:none'>Ponderous writing or speech is boring, highly serious, and seems very long and wordy; it definitely lacks both grace and style.</p>
<p class='rw-defn idx19' style='display:none'>When you have been remiss, you have been careless because you did not do something that you should have done.</p>
<p class='rw-defn idx20' style='display:none'>Something or someone that shows resilience is able to recover quickly and easily from unpleasant, difficult, and damaging situations or events.</p>
<p class='rw-defn idx21' style='display:none'>If someone is rotund, they have a round and fat body.</p>
<p class='rw-defn idx22' style='display:none'>If you say that something is somatic, you mean that it relates to or affects the body and not the mind.</p>
<p class='rw-defn idx23' style='display:none'>A svelte person, most often a female, describes one who is attractive, slender, and possesses a graceful figure.</p>
<p class='rw-defn idx24' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx25' style='display:none'>Something unfeasible cannot be made or achieved.</p>
<p class='rw-defn idx26' style='display:none'>If you say someone&#8217;s behavior is unseemly, you disapprove of it because it is not in good taste or not suitable for a particular situation.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>lissome</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='lissome#' id='pronounce-sound' path='audio/words/amy-lissome'></a>
LIS-uhm
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='lissome#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='lissome#' id='context-sound' path='audio/wordcontexts/brian-lissome'></a>
At the circus last night I was captivated by the <em>lissome</em>, or flexible and graceful trapeze artist.  In <em>lissome</em>, supple movements, she swirled effortlessly above our heads.  Beautiful, thin, and <em>lissome</em>, she spun so fast that I was dizzy just watching her athletic moving shape.  This <em>lissome</em> performer finished her act with a triple somersault, landing with gentle grace onto the mat.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean when someone is <em>lissome</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
They are supple and fluid in motion.
</li>
<li class='choice '>
<span class='result'></span>
They defy gravity and perform impossibilities.
</li>
<li class='choice '>
<span class='result'></span>
They have a great deal of stage presence.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='lissome#' id='definition-sound' path='audio/wordmeanings/amy-lissome'></a>
If a dancer is <em>lissome</em>, she moves gracefully and is very flexible.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>gracefully flexible</em>
</span>
</span>
</div>
<a class='quick-help' href='lissome#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='lissome#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/lissome/memory_hooks/4810.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Lis</span></span>a is <span class="emp2"><span>Some</span></span>thing Else!</span></span> <span class="emp3"><span>Lis</span></span>a's gymnastic ability is made greater by her <span class="emp3"><span>lis</span></span><span class="emp2"><span>some</span></span> body, making <span class="emp3"><span>Lis</span></span>a <span class="emp2"><span>some</span></span>thing else when she performs!
</p>
</div>

<div id='memhook-button-bar'>
<a href="lissome#" id="add-public-hook" style="" url="https://membean.com/mywords/lissome/memory_hooks">Use other public hook</a>
<a href="lissome#" id="memhook-use-own" url="https://membean.com/mywords/lissome/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='lissome#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
On other nights, when the stage was ranked with silk tights and tutus (tarlatan ballet skirts), pretty, plump-cheeked Irina Baronova and dark, <b>lissome</b> Tamara Toumanova took the spotlight for effortless spins, whirls, leaps.
<cite class='attribution'>
&mdash;
Time
</cite>
</li>
<li>
Pingping, though thirty-three, looked almost ten years younger than her age, with large vivid eyes, a straight nose, a delicate chin, and a <b>lissome</b> figure.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/lissome/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='lissome#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='some_having' data-tree-url='//cdn1.membean.com/public/data/treexml' href='lissome#'>
<span class=''></span>
-some
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>having a certain quality</td>
</tr>
</table>
<p>The &#8220;lis&#8221; of &#8220;lissome&#8221; is an alteration of the English word &#8220;lithe,&#8221; which means &#8220;graceful, supple.&#8221;</p>
<a href="lissome#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> Old English</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='lissome#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Tchaikovsky's Swan Lake</strong><span> A lissome Diana Vishneva performs Act II of Swan Lake.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/lissome.jpg' video_url='examplevids/lissome' video_width='350'></span>
<div id='wt-container'>
<img alt="Lissome" height="288" src="https://cdn1.membean.com/video/examplevids/lissome.jpg" width="350" />
<div class='center'>
<a href="lissome#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='lissome#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Lissome" src="https://cdn2.membean.com/public/images/wordimages/cons2/lissome.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='lissome#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adept</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>adroit</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>choreography</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>deft</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>efficacy</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>emaciated</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>ethereal</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>finesse</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>limber</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>lithe</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>malleable</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>plastic</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>resilience</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>somatic</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>svelte</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='5' class = 'rw-wordform notlearned'><span>corporeal</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>corpulent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>gauche</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>hapless</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>ponderous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>remiss</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>rotund</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>unfeasible</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>unseemly</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="lissome" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>lissome</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="lissome#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

