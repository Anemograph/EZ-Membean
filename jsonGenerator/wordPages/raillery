
<!DOCTYPE html>
<html>
<head>
<title>Word: raillery | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx1' style='display:none'>When you bandy about ideas with someone, you casually discuss them without caring if the discussion is informed or effective in any way.</p>
<p class='rw-defn idx2' style='display:none'>Blandishments are words or actions that are pleasant and complimentary, intended to persuade someone to do something via a use of flattery.</p>
<p class='rw-defn idx3' style='display:none'>Camaraderie is a feeling of friendship and trust among a group of people who have usually known each over a long period of time.</p>
<p class='rw-defn idx4' style='display:none'>When someone disports, they entertain or divert themselves for the sake of amusement.</p>
<p class='rw-defn idx5' style='display:none'>When something is droll, it is humorous in an odd way.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is being facetious says things they intend to be funny but are nevertheless out of place.</p>
<p class='rw-defn idx7' style='display:none'>When a person is being flippant, they are not taking something as seriously as they should be; therefore, they tend to dismiss things they should respectfully pay attention to.</p>
<p class='rw-defn idx8' style='display:none'>Someone who is jocose is given to joking around or being playful, merry, and humorous.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is jocular is cheerful and often makes jokes or tries to make people laugh.</p>
<p class='rw-defn idx10' style='display:none'>Levity is an amusing way of speaking or behaving during a serious situation; it can lighten the moment but can also be considered inappropriate.</p>
<p class='rw-defn idx11' style='display:none'>If you describe something as ludicrous, you mean that it is extremely silly, stupid, or just plain ridiculous.</p>
<p class='rw-defn idx12' style='display:none'>When someone natters, they are chattering or talking a great deal, usually about insignificant things.</p>
<p class='rw-defn idx13' style='display:none'>Satire is a way of criticizing people or ideas in a humorous way by going beyond the truth; it is often used to make people see their faults.</p>
<p class='rw-defn idx14' style='display:none'>A scintillating conversation, speech, or performance is brilliantly clever, interesting, and lively.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 6
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>raillery</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='raillery#' id='pronounce-sound' path='audio/words/amy-raillery'></a>
RAY-luh-ree
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='raillery#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='raillery#' id='context-sound' path='audio/wordcontexts/brian-raillery'></a>
One of the things I loved best about my college experience was the <em>raillery</em> or banter my friends and I would constantly engage in.  The <em>raillery</em> or light teasing would focus on all of our shortcomings, which were not in short supply.  Despite the fact that we ridiculed each other, our <em>raillery</em> or friendly joking around was never taken seriously, for we all were best at laughing at our own shortcomings!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is <em>raillery</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is the special language of a specific group.
</li>
<li class='choice '>
<span class='result'></span>
It is harsh but truthful criticism of someone.
</li>
<li class='choice answer '>
<span class='result'></span>
It is good-natured teasing among friends.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='raillery#' id='definition-sound' path='audio/wordmeanings/amy-raillery'></a>
<em>Raillery</em> is good-natured, playful, and friendly teasing and joking around; it can also consist of poking fun of or ridiculing someone or something that is not meant to be taken seriously.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>friendly teasing</em>
</span>
</span>
</div>
<a class='quick-help' href='raillery#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='raillery#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/raillery/memory_hooks/116662.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Rail</span></span> <span class="emp2"><span>Leery</span></span></span></span> It wouldn't be <span class="emp1"><span>rail</span></span><span class="emp2"><span>lery</span></span> to suggest that someone lie down on the <span class="emp1"><span>rail</span></span>s of train tracks and wait until the train runs him over; one ought to be <span class="emp1"><span>rail</span></span> <span class="emp2"><span>leery</span></span> about that kind of supposed <span class="emp1"><span>rail</span></span><span class="emp2"><span>lery</span></span>!
</p>
</div>

<div id='memhook-button-bar'>
<a href="raillery#" id="add-public-hook" style="" url="https://membean.com/mywords/raillery/memory_hooks">Use other public hook</a>
<a href="raillery#" id="memhook-use-own" url="https://membean.com/mywords/raillery/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='raillery#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Humor is the only test of gravity, and gravity of humor; for a subject which will not bear <b>raillery</b> is suspicious, and a jest which will not bear serious examination is false wit.
<span class='attribution'>&mdash; Aristotle, Greek philosopher</span>
<img alt="Aristotle, greek philosopher" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Aristotle, Greek philosopher.jpg?qdep8" width="80" />
</li>
<li>
[Jennie] Finch traveled to Arizona to face [Barry] Bonds in spring training, and after he watched several of her pitches fly by, the <b>raillery</b> stopped. He insisted that the cameras not film him batting against her.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Decades ago, Alfred Hitchcock said actors were cattle. Today celebrities are meat: junk food for tabloid headlines, canapes for cocktail-party surmise, fodder for Leno and Letterman <b>raillery</b>.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/raillery/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='raillery#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>The word <em>raillery</em> comes from a root word meaning &#8220;tease.&#8221;</p>
<a href="raillery#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> French</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='raillery#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Raillery" src="https://cdn2.membean.com/public/images/wordimages/cons2/raillery.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='raillery#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>bandy</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>blandishment</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>camaraderie</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>disport</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>droll</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>facetious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>flippant</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>jocose</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>jocular</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>levity</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>ludicrous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>natter</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>satire</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>scintillating</span> &middot;</li></ul>
<ul class='related-ants'></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="raillery" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>raillery</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="raillery#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

