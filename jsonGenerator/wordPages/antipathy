
<!DOCTYPE html>
<html>
<head>
<title>Word: antipathy | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abhor something, you dislike it very much, usually because you think it&#8217;s immoral.</p>
<p class='rw-defn idx1' style='display:none'>If you abominate something, you hate it because you think it is extremely wrong and unacceptable.</p>
<p class='rw-defn idx2' style='display:none'>If you acquiesce to something, you allow it to happen by doing what someone wants without putting up a struggle or voicing your own concerns.</p>
<p class='rw-defn idx3' style='display:none'>If something is anathema to you, such as a cursed object or idea, you very strongly dislike it or even hate it.</p>
<p class='rw-defn idx4' style='display:none'>If you have animus against someone, you have a strong feeling of dislike or hatred towards them, often without a good reason or based upon your personal prejudices.</p>
<p class='rw-defn idx5' style='display:none'>Your antagonist is an opponent in a competition or battle.</p>
<p class='rw-defn idx6' style='display:none'>An aversion to something is a strong dislike for or forceful opposition to it.</p>
<p class='rw-defn idx7' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx8' style='display:none'>Bigotry is the expression of strong and unreasonable opinions without accepting or tolerating opposing views.</p>
<p class='rw-defn idx9' style='display:none'>If someone is granted clemency, they are punished less severely than they could have been—they have been shown mercy.</p>
<p class='rw-defn idx10' style='display:none'>If you commiserate with someone, you show them pity or sympathy because something bad or unpleasant has happened to them.</p>
<p class='rw-defn idx11' style='display:none'>Conciliation is a process intended to end an argument between two groups of people.</p>
<p class='rw-defn idx12' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx13' style='display:none'>When someone is contentious, they are argumentative and likely to provoke a fight.</p>
<p class='rw-defn idx14' style='display:none'>A situation or thing that is discordant does not fit in with other things; therefore, it is disagreeable, strange, or unpleasant.</p>
<p class='rw-defn idx15' style='display:none'>When you are disposed towards a particular action or thing, you are inclined or partial towards it.</p>
<p class='rw-defn idx16' style='display:none'>A dissident is someone who disagrees publicly with a government, especially in a country where this is not allowed.</p>
<p class='rw-defn idx17' style='display:none'>Dissonance is an unpleasant situation of opposition in which ideas or actions are not in agreement or harmony; dissonance also refers to a harsh combination of sounds.</p>
<p class='rw-defn idx18' style='display:none'>When you execrate someone, you curse them to show your intense dislike or hatred of them.</p>
<p class='rw-defn idx19' style='display:none'>If someone is fractious, they are easily upset or annoyed over unimportant things.</p>
<p class='rw-defn idx20' style='display:none'>When two people are in a harmonious state, they are in agreement with each other; when a sound is harmonious, it is pleasant or agreeable to the ear.</p>
<p class='rw-defn idx21' style='display:none'>If two people are incompatible, they do not get along, tend to disagree, and are unable to cooperate with one another.</p>
<p class='rw-defn idx22' style='display:none'>If you are indifferent about something, you are uninterested or neutral about it, not caring either in a positive or negative way.</p>
<p class='rw-defn idx23' style='display:none'>If someone is inimical, they are unfriendly and will likely cause you harm.</p>
<p class='rw-defn idx24' style='display:none'>Something loathsome is offensive, disgusting, and brings about intense dislike.</p>
<p class='rw-defn idx25' style='display:none'>A malevolent person or thing is evil due to deliberate attempts to cause harm.</p>
<p class='rw-defn idx26' style='display:none'>Malice is a type of hatred that causes a strong desire to harm others both physically and emotionally.</p>
<p class='rw-defn idx27' style='display:none'>If you describe people or things as odious, you think that they are extremely unpleasant.</p>
<p class='rw-defn idx28' style='display:none'>If you have a penchant for some activity, you have a strong fondness for it and thus find it enjoyable.</p>
<p class='rw-defn idx29' style='display:none'>If you have a predilection for something, you have a preference for it.</p>
<p class='rw-defn idx30' style='display:none'>If someone is predisposed to something, they are made favorable or inclined to it in advance, or they are made susceptible to something, such as a disease.</p>
<p class='rw-defn idx31' style='display:none'>A proclivity is the tendency to behave in a particular way or to like a particular thing.</p>
<p class='rw-defn idx32' style='display:none'>If you are prone to doing something, you are likely or inclined to do it.</p>
<p class='rw-defn idx33' style='display:none'>A propensity is a natural tendency towards a particular behavior.</p>
<p class='rw-defn idx34' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx35' style='display:none'>Refractory people deliberately don&#8217;t obey someone in authority and so are difficult to deal with or control.</p>
<p class='rw-defn idx36' style='display:none'>If you repudiate something, you state that you do not accept or agree with it and no longer want to be connected with it in any way.</p>
<p class='rw-defn idx37' style='display:none'>Repugnance is a strong feeling of dislike for something or someone you find horrible and offensive.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
Middle/High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>antipathy</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='antipathy#' id='pronounce-sound' path='audio/words/amy-antipathy'></a>
an-TIP-uh-thee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='antipathy#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='antipathy#' id='context-sound' path='audio/wordcontexts/brian-antipathy'></a>
My feelings about seafood range from mild dislike to disgusted <em>antipathy</em>.  I don&#8217;t like the smell or texture of fish, and when it comes to shellfish, I feel nothing but distaste and <em>antipathy</em>.  Eating slimy creatures that live in crusty shells? No thanks! Nothing arouses <em>antipathy</em> or opposition in me more quickly than the suggestion that we go out for sushi, although most of my friends absolutely love raw fish.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If you have an <em>antipathy</em> toward cats, how do you feel about them?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
You have an intense dislike of them.
</li>
<li class='choice '>
<span class='result'></span>
You don&#8217;t love them, but you don&#8217;t hate them either.
</li>
<li class='choice '>
<span class='result'></span>
You have no interest in them whatsoever.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='antipathy#' id='definition-sound' path='audio/wordmeanings/amy-antipathy'></a>
<em>Antipathy</em> is a strong feeling of dislike or hostility towards someone or something.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>intense dislike</em>
</span>
</span>
</div>
<a class='quick-help' href='antipathy#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='antipathy#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/antipathy/memory_hooks/5536.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Ant</span></span> on <span class="emp3"><span>Path</span></span> to Picnic</span></span>  I feel a great deal of <span class="emp1"><span>ant</span></span>i<span class="emp3"><span>path</span></span>y towards an <span class="emp1"><span>ant</span></span> on a <span class="emp3"><span>path</span></span> to my picnic because it might eat my yummy tomato sandwiches!
</p>
</div>

<div id='memhook-button-bar'>
<a href="antipathy#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/antipathy/memory_hooks">Use other hook</a>
<a href="antipathy#" id="memhook-use-own" url="https://membean.com/mywords/antipathy/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='antipathy#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
But while [policy] disagreements are fierce, they are not as strong as their feelings of distrust, dislike and disdain for people who belong to the opposing political party. In fact, Democrats and Republicans have developed such harsh <b>antipathy</b> for their political opposites that it exceeds the level of camaraderie they feel for those who vote the same way.
<cite class='attribution'>
&mdash;
Time
</cite>
</li>
<li>
[Thomas] Jefferson and [Alexander] Hamilton hated each other, and their <b>antipathy</b> has carried through the ages. As America matured, the battle lines became more complicated.
<cite class='attribution'>
&mdash;
Politico
</cite>
</li>
<li>
The Silicon Valley mythos of freewheeling, entrepreneurial cowboys allows little room for big-government involvement, and <b>antipathy</b> toward bureaucracies and politics-as-usual has been a hallmark of tech culture for a while.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
[T]heir own internal records dating back to the 1970s show that industry officials long knew that recycling plastic on a large scale was unlikely to ever be economically viable. . . . Despite this, three former top officials, who have never spoken publicly before, said the industry promoted recycling as a way to beat back a growing tide of <b>antipathy</b> toward plastic in the 1980s and '90s.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/antipathy/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='antipathy#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='anti_opposite' data-tree-url='//cdn1.membean.com/public/data/treexml' href='antipathy#'>
<span class=''></span>
anti-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>opposite, against</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='path_feeling' data-tree-url='//cdn1.membean.com/public/data/treexml' href='antipathy#'>
<span class='common'></span>
path
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>feeling</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='y_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='antipathy#'>
<span class=''></span>
-y
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state or condition</td>
</tr>
</table>
<p><em>Antipathy</em> is a &#8220;feeling against or opposed to&#8221; something or someone.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='antipathy#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Looney Tunes</strong><span> Daffy Duck expresses antipathy towards sweet Bugs.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/antipathy.jpg' video_url='examplevids/antipathy' video_width='350'></span>
<div id='wt-container'>
<img alt="Antipathy" height="288" src="https://cdn1.membean.com/video/examplevids/antipathy.jpg" width="350" />
<div class='center'>
<a href="antipathy#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='antipathy#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Antipathy" src="https://cdn0.membean.com/public/images/wordimages/cons2/antipathy.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='antipathy#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abhor</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abominate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>anathema</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>animus</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>antagonist</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>aversion</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>bigotry</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>contentious</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>discordant</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>dissident</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>dissonance</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>execrate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>fractious</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>incompatible</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>inimical</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>loathsome</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>malevolent</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>malice</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>odious</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>refractory</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>repudiate</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>repugnance</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>acquiesce</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>clemency</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>commiserate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>conciliation</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>disposed</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>harmonious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>indifferent</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>penchant</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>predilection</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>predispose</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>proclivity</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>prone</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>propensity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='antipathy#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
antipathetic
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>possessing a strong feeling of dislike towards something</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="antipathy" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>antipathy</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="antipathy#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

