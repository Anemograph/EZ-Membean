
<!DOCTYPE html>
<html>
<head>
<title>Word: polarization | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you accede to a demand or proposal, you agree to it, especially after first disagreeing with it.</p>
<p class='rw-defn idx1' style='display:none'>When you are in accord with someone, you are in agreement or harmony with them.</p>
<p class='rw-defn idx2' style='display:none'>If you acquiesce to something, you allow it to happen by doing what someone wants without putting up a struggle or voicing your own concerns.</p>
<p class='rw-defn idx3' style='display:none'>Amity is a peaceful friendship between two parties, particularly between two countries.</p>
<p class='rw-defn idx4' style='display:none'>Your antagonist is an opponent in a competition or battle.</p>
<p class='rw-defn idx5' style='display:none'>An apostate has abandoned their religious faith, political party, or devotional cause.</p>
<p class='rw-defn idx6' style='display:none'>When a group of people reaches a consensus, it has reached a general agreement about something.</p>
<p class='rw-defn idx7' style='display:none'>When someone is contentious, they are argumentative and likely to provoke a fight.</p>
<p class='rw-defn idx8' style='display:none'>A disaffected member of a group or organization is not satisfied with it; consequently, they feel little loyalty towards it.</p>
<p class='rw-defn idx9' style='display:none'>A situation or thing that is discordant does not fit in with other things; therefore, it is disagreeable, strange, or unpleasant.</p>
<p class='rw-defn idx10' style='display:none'>Dissension is a disagreement or difference of opinion among a group of people that can cause conflict.</p>
<p class='rw-defn idx11' style='display:none'>A dissident is someone who disagrees publicly with a government, especially in a country where this is not allowed.</p>
<p class='rw-defn idx12' style='display:none'>Dissonance is an unpleasant situation of opposition in which ideas or actions are not in agreement or harmony; dissonance also refers to a harsh combination of sounds.</p>
<p class='rw-defn idx13' style='display:none'>A small group or issue that is factious is controversial and promotes strong disagreement or dissatisfaction within a larger group.</p>
<p class='rw-defn idx14' style='display:none'>When two people are in a harmonious state, they are in agreement with each other; when a sound is harmonious, it is pleasant or agreeable to the ear.</p>
<p class='rw-defn idx15' style='display:none'>A heretic is someone who doubts or acts in opposition to commonly or generally accepted beliefs.</p>
<p class='rw-defn idx16' style='display:none'>If two people are incompatible, they do not get along, tend to disagree, and are unable to cooperate with one another.</p>
<p class='rw-defn idx17' style='display:none'>An insurrection is a rebellion or open uprising against an established form of government.</p>
<p class='rw-defn idx18' style='display:none'>A nonconformist is unwilling to believe in the same things other people do or act in a fashion that society sets as a standard.</p>
<p class='rw-defn idx19' style='display:none'>The propinquity of a thing is its nearness in location, relationship, or similarity to another thing.</p>
<p class='rw-defn idx20' style='display:none'>Rapprochement is the development of greater understanding and friendliness between two countries or groups of people after a period of unfriendly relations.</p>
<p class='rw-defn idx21' style='display:none'>Refractory people deliberately don&#8217;t obey someone in authority and so are difficult to deal with or control.</p>
<p class='rw-defn idx22' style='display:none'>A schism causes a group or organization to divide into two groups as a result of differences in their aims and beliefs.</p>
<p class='rw-defn idx23' style='display:none'>Sedition is the act of encouraging people to disobey and oppose the government currently in power.</p>
<p class='rw-defn idx24' style='display:none'>If you say that someone is servile, you don&#8217;t respect them because they are too obedient, too agreeable, and too willing to do anything for another person.</p>
<p class='rw-defn idx25' style='display:none'>If you are subservient, you are too eager and willing to do what other people want and often put your own wishes aside.</p>
<p class='rw-defn idx26' style='display:none'>All people involved in a unanimous decision agree or are united in their opinion about something.</p>
<p class='rw-defn idx27' style='display:none'>Doing something in unison is doing it all together at one time.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>polarization</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='polarization#' id='pronounce-sound' path='audio/words/amy-polarization'></a>
poh-ler-uh-ZAY-shuhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='polarization#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='polarization#' id='context-sound' path='audio/wordcontexts/brian-polarization'></a>
The <em>polarization</em> or division into two opposing groups within the same country can result, in its most extreme form, in civil war.  The United States experienced this <em>polarization</em> or separation into groups with conflicting views over the issue of slavery.  The North was against it but the South supported it; this <em>polarization</em> into Union and Confederate states led to a brutal, bloody war.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When does <em>polarization</em> happen?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
When two groups of people have very different ideas about something.
</li>
<li class='choice '>
<span class='result'></span>
When two colors or patterns clash with each other in a piece of art.
</li>
<li class='choice '>
<span class='result'></span>
When two friends suddenly realize they don&#8217;t have much in common.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='polarization#' id='definition-sound' path='audio/wordmeanings/amy-polarization'></a>
<em>Polarization</em> between two groups is a division or separation caused by a difference in opinion or conflicting views.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>separation</em>
</span>
</span>
</div>
<a class='quick-help' href='polarization#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='polarization#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/polarization/memory_hooks/3790.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Polar</span></span> Civil<span class="emp1"><span>ization</span></span></span></span> If citizens of a planet were to undergo severe <span class="emp3"><span>polar</span></span><span class="emp1"><span>ization</span></span>, they might so disagree with one another that their civil<span class="emp1"><span>ization</span></span> would split into two groups, and live at the North and South <span class="emp3"><span>Pol</span></span>es.
</p>
</div>

<div id='memhook-button-bar'>
<a href="polarization#" id="add-public-hook" style="" url="https://membean.com/mywords/polarization/memory_hooks">Use other public hook</a>
<a href="polarization#" id="memhook-use-own" url="https://membean.com/mywords/polarization/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='polarization#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
The degree of <b>polarization</b> that currently exists in Washington is such where I think it's fair to say if I presented a cure for cancer, getting legislation passed to move that forward would be a nail-biter.
<span class='attribution'>&mdash; Barack Obama, Forty-fourth president of the United States</span>
<img alt="Barack obama, forty-fourth president of the united states" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Barack Obama, Forty-fourth president of the United States.jpg?qdep8" width="80" />
</li>
<li>
"When all the owners are in one room, it leads to <b>polarization</b> and makes agreement almost impossible," says Tex Schramm, the president of the Dallas Cowboys.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Amid the nation’s political <b>polarization</b> and widening cultural divides are millions of Americans who have lost sight of each other, caught in reflexive rituals and simplistic clichés that dismiss, demonize, or otherwise delegitimize perceived enemies.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
“The point is that these kinds of shock, like the current pandemic, is not a guarantee of any type of change, but it is oftentimes a necessary condition to change deeply entrenched patterns like political <b>polarization</b> around the world,” Coleman told me.
<cite class='attribution'>
&mdash;
Miami Herald
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/polarization/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='polarization#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pol_axis' data-tree-url='//cdn1.membean.com/public/data/treexml' href='polarization#'>
<span class=''></span>
pol
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>axis of a sphere</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ar_pertaining' data-tree-url='//cdn1.membean.com/public/data/treexml' href='polarization#'>
<span class=''></span>
-ar
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>pertaining to or resembling</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ize_make' data-tree-url='//cdn1.membean.com/public/data/treexml' href='polarization#'>
<span class=''></span>
-ize
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to make</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ation_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='polarization#'>
<span class=''></span>
-ation
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state or quality of</td>
</tr>
</table>
<p><em>Polarization</em> is the &#8220;state of making&#8221; two groups go towards the opposite sides of an &#8220;axis,&#8221; thus <em>polarizing</em> themselves into two opposed camps.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='polarization#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Polarization" src="https://cdn3.membean.com/public/images/wordimages/cons2/polarization.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='polarization#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='4' class = 'rw-wordform notlearned'><span>antagonist</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>apostate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>contentious</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>disaffected</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>discordant</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>dissension</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>dissident</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>dissonance</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>factious</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>heretic</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>incompatible</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>insurrection</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>nonconformist</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>refractory</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>schism</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>sedition</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>accede</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>accord</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>acquiesce</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>amity</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>consensus</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>harmonious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>propinquity</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>rapprochement</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>servile</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>subservient</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>unanimous</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>unison</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='polarization#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
polarizing
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>causing to divide or separate</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="polarization" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>polarization</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="polarization#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

