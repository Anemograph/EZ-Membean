
<!DOCTYPE html>
<html>
<head>
<title>Word: gossamer | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Gossamer-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/gossamer-large.jpg?qdep8" />
</div>
<a href="gossamer#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>Something that is amorphous has no clear shape, boundaries, or structure.</p>
<p class='rw-defn idx1' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx2' style='display:none'>The word corporeal refers to the physical or material world rather than the spiritual; it also means characteristic of the body rather than the mind or feelings.</p>
<p class='rw-defn idx3' style='display:none'>Someone who is corpulent is extremely fat.</p>
<p class='rw-defn idx4' style='display:none'>A diaphanous cloth is thin enough to see through.</p>
<p class='rw-defn idx5' style='display:none'>Something ethereal has a delicate beauty that makes it seem not part of the real world.</p>
<p class='rw-defn idx6' style='display:none'>Limpid speech or prose is clear, simple, and easily understood; things, such as pools of water or eyes, are limpid if they are clear or transparent.</p>
<p class='rw-defn idx7' style='display:none'>If someone is lucid, they are able to understand things and think clearly; this adjective also describes writing or speech that is crystal clear and easy to understand.</p>
<p class='rw-defn idx8' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx9' style='display:none'>If you describe something as nebulous, you mean that it is unclear, vague, and not clearly defined; a shape that is nebulous has no clear boundaries.</p>
<p class='rw-defn idx10' style='display:none'>Something is opaque if it is either difficult to understand or is not transparent.</p>
<p class='rw-defn idx11' style='display:none'>Something that is pellucid is either extremely clear because it is transparent to the eye or it is very easy for the mind to understand.</p>
<p class='rw-defn idx12' style='display:none'>When a substance permeates something, it enters into all parts of it.</p>
<p class='rw-defn idx13' style='display:none'>To rarefy something is to make it less dense, as in oxygen at high altitudes; this word also refers to purifying or refining something, thereby making it less ordinary or commonplace.</p>
<p class='rw-defn idx14' style='display:none'>If you say that something is somatic, you mean that it relates to or affects the body and not the mind.</p>
<p class='rw-defn idx15' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx16' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx17' style='display:none'>A svelte person, most often a female, describes one who is attractive, slender, and possesses a graceful figure.</p>
<p class='rw-defn idx18' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx19' style='display:none'>A translucent object allows some light to pass through it.</p>
<p class='rw-defn idx20' style='display:none'>Something that is vaporous is not completely formed but foggy and misty; in the same vein, a vaporous idea is insubstantial and vague.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>gossamer</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='gossamer#' id='pronounce-sound' path='audio/words/amy-gossamer'></a>
GOS-uh-mer
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='gossamer#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='gossamer#' id='context-sound' path='audio/wordcontexts/brian-gossamer'></a>
As I was hiking through the forest, I kept on seeing thin, <em>gossamer</em> threads of spiderwebs above me&#8212;and to the right and left. These <em>gossamer</em> threads seemed so insubstantial, so not solid, that it was hard for me to believe they could support the weight of even something so light as a spider. Even the air wafting through these <em>gossamer</em>, flimsy strands seemed of the same slight consistency as these spiderwebs. Imagine my delight when some light hit the dew hanging on the <em>gossamer</em> threads, creating a beautiful luminous spectacle; I watched them, spellbound&#8212;until I saw thousands of spiders emerge!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does a poet mean if they describe a dragonfly&#8217;s wings as <em>gossamer</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They are fluttering too fast to be seen by the human eye.
</li>
<li class='choice '>
<span class='result'></span>
They are disproportionately large compared to the body of the dragonfly.
</li>
<li class='choice answer '>
<span class='result'></span>
They are rather insubstantial, almost transparent, and airy in nature.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='gossamer#' id='definition-sound' path='audio/wordmeanings/amy-gossamer'></a>
A <em>gossamer</em> material is very thin, light, and delicate.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>insubstantial</em>
</span>
</span>
</div>
<a class='quick-help' href='gossamer#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='gossamer#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/gossamer/memory_hooks/5622.json'></span>
<p>
<span class="emp0"><span>Bec<span class="emp2"><span>ame</span></span> a <span class="emp3"><span>G</span></span>h<span class="emp3"><span>os</span></span>t</span></span> When I bec<span class="emp2"><span>ame</span></span> a <span class="emp3"><span>g</span></span>h<span class="emp3"><span>os</span></span>t, I gained a <span class="emp3"><span>gos</span></span>s<span class="emp2"><span>ame</span></span>r body through which all could see.
</p>
</div>

<div id='memhook-button-bar'>
<a href="gossamer#" id="add-public-hook" style="" url="https://membean.com/mywords/gossamer/memory_hooks">Use other public hook</a>
<a href="gossamer#" id="memhook-use-own" url="https://membean.com/mywords/gossamer/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='gossamer#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The <b>gossamer</b>-thin strands of ultra-pure glass delivering voice, video and data at the speed of light have replaced copper as the backbone of America’s telephone and cable television networks and enabled the phenomenal growth of the Internet.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
Large and bold, and crafted in a flowing style reminiscent of the Belle Epoque, the brooch is mounted in titanium, and is so light you could wear it on the thinnest <b>gossamer</b> silk.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
The National Aeronautics and Space Administration said its Stardust spacecraft could pass within 186 miles of the comet Wild 2 on Friday while flying through the <b>gossamer</b> cloud that envelops the dirty ball of ice and rock.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
"This is my dress!" I announced. The clerk draped it over her arm and headed toward the cash register. Driving home I imagined myself gliding across the polished dance floor in high-heeled silken shoes, the <b>gossamer</b> layers of chiffon rustling from side to side.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/gossamer/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='gossamer#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>Perhaps from root words meaning &#8220;goose&#8221; and &#8220;summer.&#8221;  During the season of &#8220;summer,&#8221; fine, filmy cobweb-like strands, or <em>gossamer</em>, are often found in the grass in the early morning dew which resemble fine &#8220;goose&#8221; down.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='gossamer#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Gossamer" src="https://cdn2.membean.com/public/images/wordimages/cons2/gossamer.png?qdep6" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='gossamer#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>amorphous</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>diaphanous</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>ethereal</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>limpid</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>lucid</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>pellucid</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>permeate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>rarefy</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>svelte</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>translucent</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>vaporous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>corporeal</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>corpulent</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>nebulous</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>opaque</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>somatic</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="gossamer" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>gossamer</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="gossamer#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

