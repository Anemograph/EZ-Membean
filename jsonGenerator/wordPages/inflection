
<!DOCTYPE html>
<html>
<head>
<title>Word: inflection | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>To accentuate something is to emphasize it or make it more noticeable.</p>
<p class='rw-defn idx1' style='display:none'>If you describe someone as articulate, you mean that they are able to express their thoughts, arguments, and ideas clearly and effectively.</p>
<p class='rw-defn idx2' style='display:none'>When a person is speaking or writing in a bombastic fashion, they are very self-centered, extremely showy, and excessively proud.</p>
<p class='rw-defn idx3' style='display:none'>A cadence is a repeated pattern of movement or sound, such as the way someone&#8217;s voice rises and falls when reading something out loud.</p>
<p class='rw-defn idx4' style='display:none'>Someone&#8217;s elocution is their artistic manner of speaking in public, including both the delivery of their voice and gestures.</p>
<p class='rw-defn idx5' style='display:none'>When you speak in an eloquent fashion, you speak beautifully in an expressive way that is convincing to an audience.</p>
<p class='rw-defn idx6' style='display:none'>If someone or something is flamboyant, the former is trying to show off in a way that deliberately attracts attention, and the latter is brightly colored and highly decorated.</p>
<p class='rw-defn idx7' style='display:none'>Things that fluctuate vary or change often, rising or falling seemingly at random.</p>
<p class='rw-defn idx8' style='display:none'>When someone gesticulates, they make movements with their hands and arms when talking, usually because they want to emphasize something or are having difficulty in expressing an idea using words alone.</p>
<p class='rw-defn idx9' style='display:none'>Grandiloquent speech is highly formal, exaggerated, and often seems both silly and hollow because it is expressly used to appear impressive and important.</p>
<p class='rw-defn idx10' style='display:none'>The gravity of a situation or event is its seriousness or importance.</p>
<p class='rw-defn idx11' style='display:none'>A guttural sound is deep, harsh, and made at the back of someone&#8217;s throat.</p>
<p class='rw-defn idx12' style='display:none'>The word homogeneous is used to describe a group that has members or parts which are similar or are all of the same type.</p>
<p class='rw-defn idx13' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx14' style='display:none'>Something that is imperceptible is either impossible to be perceived by the mind or is very difficult to perceive.</p>
<p class='rw-defn idx15' style='display:none'>If someone is intoning something, they are saying it in a slow and serious way without making their voice rise or fall.</p>
<p class='rw-defn idx16' style='display:none'>When someone or something undergoes the process of metamorphosis, there is a change in appearance, character, or even physical shape.</p>
<p class='rw-defn idx17' style='display:none'>A monotonous activity is so repetitious that it quickly becomes boring and dull.</p>
<p class='rw-defn idx18' style='display:none'>If you describe an action as ostentatious, you think it is an extreme and exaggerated way of impressing people.</p>
<p class='rw-defn idx19' style='display:none'>If an activity is punctuated by something, it is interrupted or emphasized by it at intervals.</p>
<p class='rw-defn idx20' style='display:none'>To quaver is to shake or tremble, especially when speaking.</p>
<p class='rw-defn idx21' style='display:none'>Rhetoric is the skill or art of using language to persuade or influence people, especially language that sounds impressive but may not be sincere or honest.</p>
<p class='rw-defn idx22' style='display:none'>A scintillating conversation, speech, or performance is brilliantly clever, interesting, and lively.</p>
<p class='rw-defn idx23' style='display:none'>If someone is unlettered, they are illiterate, unable to read and write.</p>
<p class='rw-defn idx24' style='display:none'>When someone vacillates, they go back and forth about a choice or opinion, unable to make a firm decision.</p>
<p class='rw-defn idx25' style='display:none'>Something that is variegated has various tones or colors; it can also mean filled with variety.</p>
<p class='rw-defn idx26' style='display:none'>Verbiage is an excessive use of words to convey something that could be expressed using fewer words; it can also be the manner or style in which someone uses words.</p>
<p class='rw-defn idx27' style='display:none'>If you waver, you cannot decide between two things because you have serious doubts about which choice is better.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>inflection</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='inflection#' id='pronounce-sound' path='audio/words/amy-inflection'></a>
in-FLEK-shuhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='inflection#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='inflection#' id='context-sound' path='audio/wordcontexts/brian-inflection'></a>
The actor was highly talented at changing the <em>inflection</em> or tone of his voice to indicate any change in emotion.  The <em>inflections</em> or variations in the pitch of his voice conveyed a range of feelings, which, coupled with his facial expressions, made him very believable indeed.  When he acted nervous, for example, the <em>inflection</em> or accent of anxiety and panic was so effective that he seemed like a different person onstage.  This tactic was particularly effective when he would use different <em>inflections</em> or forms of nouns and verbs to truly and faithfully portray his character.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When might you speak with <em>inflection</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When you want to pause to indicate a new topic.
</li>
<li class='choice '>
<span class='result'></span>
When you want to use words that exaggerate to make something more entertaining.
</li>
<li class='choice answer '>
<span class='result'></span>
When you want to change your tone to communicate emotion.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='inflection#' id='definition-sound' path='audio/wordmeanings/amy-inflection'></a>
An <em>inflection</em> is a change or variation, such as in a person&#8217;s voice or in the form a noun or verb takes.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>variation</em>
</span>
</span>
</div>
<a class='quick-help' href='inflection#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='inflection#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/inflection/memory_hooks/2884.json'></span>
<p>
<span class="emp0"><span>Re<span class="emp3"><span>flections</span></span> <span class="emp1"><span>in</span></span> Mirrors</span></span> Six different mirrors would give six different re<span class="emp3"><span>flect</span></span>ive images or <span class="emp1"><span>in</span></span><span class="emp3"><span>flection</span></span>s of the same person.
</p>
</div>

<div id='memhook-button-bar'>
<a href="inflection#" id="add-public-hook" style="" url="https://membean.com/mywords/inflection/memory_hooks">Use other public hook</a>
<a href="inflection#" id="memhook-use-own" url="https://membean.com/mywords/inflection/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='inflection#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
In conversation you can use timing, a look, an <b>inflection</b>. But on the page all you have is commas, dashes, the amount of syllables in a word. When I write, I read everything out loud to get the right rhythm.
<span class='attribution'>&mdash; Fran Lebowitz, American author</span>
<img alt="Fran lebowitz, american author" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Fran Lebowitz, American author.jpg?qdep8" width="80" />
</li>
<li>
The farther from Bridgend he went, however, the more he feared that some expression on his face or <b>inflection</b> in his voice would betray him.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Yet these markets will eventually pass an "<b>inflection</b> point" where data services outstrip other revenue growth, predicts Sachin Gupta of Nomura Securities, an investment bank.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
For example, if you go into a convenience store to buy some cat food and you can't find it on the shelves, and you ask the salesperson if they have any cat food, he or she will reply, cheerful as can be, "We sure don't!" The last word is spoken with a rising <b>inflection</b>, as if the expression were a positive one ending with the word "do."
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/inflection/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='inflection#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='in_in' data-tree-url='//cdn1.membean.com/public/data/treexml' href='inflection#'>
<span class='common'></span>
in-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>in, inwards</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='flect_bent' data-tree-url='//cdn1.membean.com/public/data/treexml' href='inflection#'>
<span class=''></span>
flect
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>bent</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ion_act' data-tree-url='//cdn1.membean.com/public/data/treexml' href='inflection#'>
<span class=''></span>
-ion
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>act, state, or result of doing something</td>
</tr>
</table>
<p>An <em>inflection</em> is &#8220;bent inwards&#8221; from the standard.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='inflection#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Empowered Sales Training</strong><span> The power of inflection.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/inflection.jpg' video_url='examplevids/inflection' video_width='350'></span>
<div id='wt-container'>
<img alt="Inflection" height="288" src="https://cdn1.membean.com/video/examplevids/inflection.jpg" width="350" />
<div class='center'>
<a href="inflection#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='inflection#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Inflection" src="https://cdn2.membean.com/public/images/wordimages/cons2/inflection.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='inflection#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>accentuate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>articulate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bombastic</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>cadence</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>elocution</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>eloquent</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>flamboyant</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>fluctuate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>gesticulate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>grandiloquent</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>gravity</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>guttural</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>metamorphosis</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>ostentatious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>punctuate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>quaver</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>rhetoric</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>scintillating</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>vacillate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>variegated</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>verbiage</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>waver</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='12' class = 'rw-wordform notlearned'><span>homogeneous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>imperceptible</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>intone</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>monotonous</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>unlettered</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="inflection" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>inflection</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="inflection#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

