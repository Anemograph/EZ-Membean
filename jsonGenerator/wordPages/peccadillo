
<!DOCTYPE html>
<html>
<head>
<title>Word: peccadillo | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx1' style='display:none'>If you abominate something, you hate it because you think it is extremely wrong and unacceptable.</p>
<p class='rw-defn idx2' style='display:none'>When you absolve someone, you publicly and formally say that they are not guilty or responsible for any wrongdoing.</p>
<p class='rw-defn idx3' style='display:none'>A cardinal rule or quality is considered to be the most important or basic in a set of rules or qualities.</p>
<p class='rw-defn idx4' style='display:none'>Censure is written or verbal disapproval and harsh criticism of something that someone has done.</p>
<p class='rw-defn idx5' style='display:none'>If someone is granted clemency, they are punished less severely than they could have been—they have been shown mercy.</p>
<p class='rw-defn idx6' style='display:none'>If you are culpable for an action, you are held responsible for something wrong or bad that has happened.</p>
<p class='rw-defn idx7' style='display:none'>A man who is debonair wears fashionable clothes and is sophisticated, charming, friendly, and confident.</p>
<p class='rw-defn idx8' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx9' style='display:none'>Something, such as a building, is derelict if it is empty, not used, and in bad condition or disrepair.</p>
<p class='rw-defn idx10' style='display:none'>If you refer to the enormity of a situation, problem, or event, you mean that it is very evil or morally offensive; this word has nothing to do with physical size.</p>
<p class='rw-defn idx11' style='display:none'>Errant things or people behave in a way that is unacceptable or wrong, such as a missile that travels in the wrong direction or a student who does not follow classroom rules.</p>
<p class='rw-defn idx12' style='display:none'>If you exculpate someone, you prove that they are not guilty of a crime.</p>
<p class='rw-defn idx13' style='display:none'>When someone is exonerated from guilt, a task, or a responsibility, they are set free or excused from it.</p>
<p class='rw-defn idx14' style='display:none'>If you expiate your crimes, guilty feelings, or bad behavior in general, you show you are sorry by doing something good to make up for the bad things you did.</p>
<p class='rw-defn idx15' style='display:none'>If you describe something as heinous, you mean that is extremely evil, shocking, and bad.</p>
<p class='rw-defn idx16' style='display:none'>Someone who is indiscreet shows lack of judgment, especially because they say or do things in public that should only be said or done privately—if at all.</p>
<p class='rw-defn idx17' style='display:none'>Malfeasance is an unlawful act, especially one committed by a trusted public official.</p>
<p class='rw-defn idx18' style='display:none'>To be politic in a decision is to be socially wise and diplomatic.</p>
<p class='rw-defn idx19' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx20' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx21' style='display:none'>You show rectitude if you behave or conduct yourself in an honest and morally correct manner.</p>
<p class='rw-defn idx22' style='display:none'>To taint is to give an undesirable quality that damages a person&#8217;s reputation or otherwise spoils something.</p>
<p class='rw-defn idx23' style='display:none'>If you refer to something as a trifle, you mean that it is of little or no importance or value.</p>
<p class='rw-defn idx24' style='display:none'>If you behave in an urbane way, you are behaving in a polite, refined, and civilized fashion in social situations.</p>
<p class='rw-defn idx25' style='display:none'>A venial fault or mistake is not very serious; therefore, it can be forgiven or excused.</p>
<p class='rw-defn idx26' style='display:none'>If a person is vindicated, their ideas, decisions, or actions—once considered wrong—are proved correct or not to be blamed.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>peccadillo</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='peccadillo#' id='pronounce-sound' path='audio/words/amy-peccadillo'></a>
pek-uh-DIL-oh
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='peccadillo#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='peccadillo#' id='context-sound' path='audio/wordcontexts/brian-peccadillo'></a>
As children grow, they learn from each and every <em>peccadillo</em>, or minor mistake, about how they should actually behave.  For example, when a young child chews loudly with his mouth open, his parents let him know that this is a <em>peccadillo</em> or slight fault that needs to be corrected.  Pointing at others is another <em>peccadillo</em> or minor social error that parents correct.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>peccadillo</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A small offense.
</li>
<li class='choice '>
<span class='result'></span>
A method of learning about how to act in the world.
</li>
<li class='choice '>
<span class='result'></span>
A way of behaving.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='peccadillo#' id='definition-sound' path='audio/wordmeanings/amy-peccadillo'></a>
A <em>peccadillo</em> is a slight or minor offense that can be overlooked.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>minor offense</em>
</span>
</span>
</div>
<a class='quick-help' href='peccadillo#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='peccadillo#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/peccadillo/memory_hooks/4943.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Peck</span></span>ing Arm<span class="emp3"><span>adillo</span></span></span></span> My pet arm<span class="emp3"><span>adillo</span></span> loves to <span class="emp1"><span>peck</span></span> at our wood table, putting deep marks in it ... but my arm<span class="emp3"><span>adillo</span></span>'s <span class="emp1"><span>peck</span></span>ing is its only <span class="emp1"><span>pecc</span></span><span class="emp3"><span>adillo</span></span>!
</p>
</div>

<div id='memhook-button-bar'>
<a href="peccadillo#" id="add-public-hook" style="" url="https://membean.com/mywords/peccadillo/memory_hooks">Use other public hook</a>
<a href="peccadillo#" id="memhook-use-own" url="https://membean.com/mywords/peccadillo/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='peccadillo#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
The way in which sports focuses more on the <b>peccadilloes</b>, lives and putative personalities of athletes and less on the finer points of playing games, I've become less interested in it. I don't want to write sports profiles.
<span class='attribution'>&mdash; Richard Ford, American writer</span>
<img alt="Richard ford, american writer" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Richard Ford, American writer.jpg?qdep8" width="80" />
</li>
<li>
The flat shape of the cheese enchiladas ($9.95) proves the corn tortillas have been properly softened, though the gravy is a little darker and sweeter than I like. That <b>peccadillo</b> aside, Cactus Cantina is my clear favorite Tex-Mex restaurant in Washington—it makes me feel at home.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
One <b>peccadillo</b>: to my ear, this performance does not quite honor the composer’s direction to change tempo suddenly three times in six bars, by 22 metronomic points at each turn (about 20 percent faster at each new tempo).
<cite class='attribution'>
&mdash;
Oregon ArtsWatch
</cite>
</li>
<li>
Another regulatory <b>peccadillo</b> may be the 2017 mandate to equip trucks with electronic logging devices, or ELDs, which record driving data. “Most of the fleets were moving toward this anyway,” Wiesen explains, “but when it becomes a regulation, you’re forced to upgrade your fleet, whether the truck needs to be upgraded or not. That puts economic pressure on a business.
<cite class='attribution'>
&mdash;
Westchester Magazine
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/peccadillo/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='peccadillo#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pecc_stumble' data-tree-url='//cdn1.membean.com/public/data/treexml' href='peccadillo#'>
<span class=''></span>
pecc
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>stumble, sin</td>
</tr>
<tr>
<td class='partform'>-illo</td>
<td>
&rarr;
</td>
<td class='meaning'>little</td>
</tr>
</table>
<p>A <em>peccadillo</em> is simply a &#8220;little sin.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='peccadillo#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Miami Vice</strong><span> A heated discussion ending with the threat of letting the mob's peccadillos go unchecked.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/peccadillo.jpg' video_url='examplevids/peccadillo' video_width='350'></span>
<div id='wt-container'>
<img alt="Peccadillo" height="288" src="https://cdn1.membean.com/video/examplevids/peccadillo.jpg" width="350" />
<div class='center'>
<a href="peccadillo#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='peccadillo#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Peccadillo" src="https://cdn3.membean.com/public/images/wordimages/cons2/peccadillo.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='peccadillo#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>absolve</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>censure</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>clemency</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>culpable</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>derelict</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>errant</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>exculpate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>exonerate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>expiate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>indiscreet</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>malfeasance</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>taint</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>trifle</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>venial</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>vindicate</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>abominate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>cardinal</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>debonair</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>enormity</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>heinous</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>politic</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>rectitude</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>urbane</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="peccadillo" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>peccadillo</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="peccadillo#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

