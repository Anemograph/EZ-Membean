
<!DOCTYPE html>
<html>
<head>
<title>Word: calumny | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An aspersion is an unkind remark or unfair judgment attacking someone&#8217;s character or reputation.</p>
<p class='rw-defn idx1' style='display:none'>If you besmirch someone, you spoil their good reputation by saying bad things about them, usually things that you know are not true.</p>
<p class='rw-defn idx2' style='display:none'>A canard is a piece of news or information that is false; it is deliberately spread either to harm someone or as a hoax.</p>
<p class='rw-defn idx3' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx4' style='display:none'>A captious person has a fondness for catching others at fault; hence, they are overly critical and raise unwarranted objections too often.</p>
<p class='rw-defn idx5' style='display:none'>Something that is delusive deceives you by giving a false belief about yourself or the situation you are in.</p>
<p class='rw-defn idx6' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is disingenuous is not straightforward or is dishonest in what they say or do.</p>
<p class='rw-defn idx8' style='display:none'>When you exalt another person, you either praise them highly or promote them to a higher position in an organization.</p>
<p class='rw-defn idx9' style='display:none'>A feint is the act of pretending to make a movement in one direction while actually moving in the other, especially to trick an opponent; a feint can also be a deceptive act meant to turn attention away from one&#8217;s true purpose.</p>
<p class='rw-defn idx10' style='display:none'>People who are ingenuous are excessively trusting and believe almost everything that people tell them, especially because they have not had much life experience.</p>
<p class='rw-defn idx11' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx12' style='display:none'>If you inveigh against something, you criticize it very strongly.</p>
<p class='rw-defn idx13' style='display:none'>Malfeasance is an unlawful act, especially one committed by a trusted public official.</p>
<p class='rw-defn idx14' style='display:none'>If you malign someone, you say unpleasant things about them to damage their reputation.</p>
<p class='rw-defn idx15' style='display:none'>A mendacious person does not tell the truth.</p>
<p class='rw-defn idx16' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx17' style='display:none'>Opprobrium is strong criticism of something that someone has done, especially when expressed publicly; it is also the shame that arises from such an activity.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is perfidious is not loyal and cannot be trusted.</p>
<p class='rw-defn idx19' style='display:none'>If you prevaricate, you avoid giving a direct or honest answer, usually because you want to hide the truth or want to delay or avoid making a hard decision.</p>
<p class='rw-defn idx20' style='display:none'>If you repudiate something, you state that you do not accept or agree with it and no longer want to be connected with it in any way.</p>
<p class='rw-defn idx21' style='display:none'>If something is reviled, it is intensely hated and criticized.</p>
<p class='rw-defn idx22' style='display:none'>A salubrious place or area is pleasant, clean, healthy, and comfortable to live in.</p>
<p class='rw-defn idx23' style='display:none'>A salutary experience is beneficial to you since it strengthens you in some way, although it may be unpleasant as you undergo it; this word also refers to promoting good health.</p>
<p class='rw-defn idx24' style='display:none'>A scrupulous person takes great care to do everything in an extremely honest, conscientious, and fair manner.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx26' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx27' style='display:none'>Verisimilitude is something&#8217;s authenticity or appearance of being real or true.</p>
<p class='rw-defn idx28' style='display:none'>The verity of something is the truth or reality of it.</p>
<p class='rw-defn idx29' style='display:none'>If you vilify people, you write or say bad things about them in an attempt to make them unpopular.</p>
<p class='rw-defn idx30' style='display:none'>Vituperative remarks are full of hate, anger, and cruel criticism.</p>
<p class='rw-defn idx31' style='display:none'>Wiles are clever tricks or cunning schemes that are used to persuade someone to do what you want.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>calumny</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='calumny#' id='pronounce-sound' path='audio/words/amy-calumny'></a>
KAL-uhm-nee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='calumny#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='calumny#' id='context-sound' path='audio/wordcontexts/brian-calumny'></a>
The newspaper columnist tried to damage the council member&#8217;s career by releasing false statements or <em>calumny</em> about her last vote.  His harsh words and <em>calumny</em> attacked the councilwoman for actions she hadn&#8217;t done.  Could her reputation survive this untruthful and damaging <em>calumny</em>, this unfair, deceitful criticism with its many invented claims?
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an act of <em>calumny</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Posting a false claim on social media to damage someone&#8217;s integrity.
</li>
<li class='choice '>
<span class='result'></span>
Publicly blaming someone for something that they did do.
</li>
<li class='choice '>
<span class='result'></span>
Offering constructive feedback to a leader to help them improve.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='calumny#' id='definition-sound' path='audio/wordmeanings/amy-calumny'></a>
<em>Calumny</em> consists of untrue or unfair statements about someone expressly made to hurt their reputation.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>defamation</em>
</span>
</span>
</div>
<a class='quick-help' href='calumny#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='calumny#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/calumny/memory_hooks/3387.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Alum</span></span>inum Not Steel</span></span> "What c<span class="emp3"><span>alum</span></span>ny! My enemy said that my heart was made of <span class="emp3"><span>alum</span></span>inum, not steel!"
</p>
</div>

<div id='memhook-button-bar'>
<a href="calumny#" id="add-public-hook" style="" url="https://membean.com/mywords/calumny/memory_hooks">Use other public hook</a>
<a href="calumny#" id="memhook-use-own" url="https://membean.com/mywords/calumny/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='calumny#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The prime minister told reporters: "As far as I was concerned, I was very happy to be able to provide my testimony in this affair where for many months I have been the victim of <b>calumny</b> and lies."
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Merely being wrong—as even his partisans admit he probably was about a lot of things—seems inadequate to explain the <b>calumny</b> he has engendered, so Freudians invoke a Freudian explanation.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
Be thou as chaste as ice, as pure as snow, thou shalt not escape <b>calumny</b>. Get thee to a nunnery, go.
<cite class='attribution'>
&mdash;
William Shakespeare in Hamlet
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/calumny/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='calumny#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>calumn</td>
<td>
&rarr;
</td>
<td class='meaning'>contrive false accusations, find fault</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='y_activity' data-tree-url='//cdn1.membean.com/public/data/treexml' href='calumny#'>
<span class=''></span>
-y
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>activity</td>
</tr>
</table>
<p>Speaking <em>calumny</em> against another person is the &#8220;activity of finding fault&#8221; in her character or &#8220;contriving false accusations&#8221; against her.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='calumny#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Calumny" src="https://cdn0.membean.com/public/images/wordimages/cons2/calumny.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='calumny#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>aspersion</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>besmirch</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>canard</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>captious</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>delusive</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>disingenuous</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>feint</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>inveigh</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>malfeasance</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>malign</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>mendacious</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>opprobrium</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>perfidious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>prevaricate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>repudiate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>revile</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>vilify</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>vituperative</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>wile</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>exalt</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>ingenuous</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>salubrious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>salutary</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>scrupulous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>verisimilitude</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='calumny#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
calumniate
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to knowingly lie about someone to hurt his or her reputation</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="calumny" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>calumny</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="calumny#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

