
<!DOCTYPE html>
<html>
<head>
<title>Word: retribution | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>If someone in authority abolishes a law or practice, they formally put an end to it.</p>
<p class='rw-defn idx2' style='display:none'>To abrogate an act is to end it by official and sometimes legal means.</p>
<p class='rw-defn idx3' style='display:none'>An affliction is something that causes pain and mental suffering, especially a medical condition.</p>
<p class='rw-defn idx4' style='display:none'>If someone is granted clemency, they are punished less severely than they could have been—they have been shown mercy.</p>
<p class='rw-defn idx5' style='display:none'>If you expiate your crimes, guilty feelings, or bad behavior in general, you show you are sorry by doing something good to make up for the bad things you did.</p>
<p class='rw-defn idx6' style='display:none'>If you indemnify someone against something bad happening, you promise to protect them from financial loss or legal responsibility if it happens.</p>
<p class='rw-defn idx7' style='display:none'>The adjective inexorable describes a process that is impossible to stop once it has begun.</p>
<p class='rw-defn idx8' style='display:none'>An irrevocable action or decision is impossible to change, reverse, or stop.</p>
<p class='rw-defn idx9' style='display:none'>To propitiate another is to calm or soothe them; it is often giving someone what they want so that everyone is happy.</p>
<p class='rw-defn idx10' style='display:none'>A punitive action is intended to punish someone.</p>
<p class='rw-defn idx11' style='display:none'>If you reciprocate, you give something to someone because they have given something similar to you.</p>
<p class='rw-defn idx12' style='display:none'>When you offer recompense to someone, you give them something, usually money, for the trouble or loss that you have caused them or as payment for their help.</p>
<p class='rw-defn idx13' style='display:none'>To recoup is to get back an amount of money you have lost or spent.</p>
<p class='rw-defn idx14' style='display:none'>If you redress a complaint or a bad situation, you correct or improve it for the person who has been wronged, usually by paying them money or offering an apology.</p>
<p class='rw-defn idx15' style='display:none'>If you renege on a deal, agreement, or promise, you do not do what you promised or agreed to do.</p>
<p class='rw-defn idx16' style='display:none'>A reprisal is something violent or harmful that you do to punish someone for something bad or unpleasant that they did to you.</p>
<p class='rw-defn idx17' style='display:none'>When someone in power rescinds a law or agreement, they officially end it and state that it is no longer valid.</p>
<p class='rw-defn idx18' style='display:none'>Restitution is the formal act of giving something that was taken away back to the rightful owner—or paying them money for the loss of the object.</p>
<p class='rw-defn idx19' style='display:none'>When you retaliate, you get back at or get even with someone for something that they did to you.</p>
<p class='rw-defn idx20' style='display:none'>Severe problems, suffering, or difficulties experienced in a particular situation are all examples of tribulations.</p>
<p class='rw-defn idx21' style='display:none'>If you feel unrequited love for another, you love that person, but they don&#8217;t love you in return.</p>
<p class='rw-defn idx22' style='display:none'>A vendetta is a prolonged situation in which one person or group tries to harm another person or group—and vice versa.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>retribution</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='retribution#' id='pronounce-sound' path='audio/words/amy-retribution'></a>
re-truh-BYOO-shuhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='retribution#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='retribution#' id='context-sound' path='audio/wordcontexts/brian-retribution'></a>
After being wrongly imprisoned for fifty years, Monty sought <em>retribution</em> for his pain by severely punishing his enemies.  He thoroughly blamed those who were responsible for his misery behind bars, and his revenge or <em>retribution</em> took the form of political shame and financial ruin.  Monty performed that <em>retribution</em> or payback in secret, taking the law into his own hands.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When might you seek <em>retribution</em> against someone else?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
You want to forgive them for something bad that they did in the past.
</li>
<li class='choice answer '>
<span class='result'></span>
You want to punish them for doing something that harmed you.
</li>
<li class='choice '>
<span class='result'></span>
You want to warn them about something bad that is going to happen.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='retribution#' id='definition-sound' path='audio/wordmeanings/amy-retribution'></a>
<em>Retribution</em> is severe punishment that someone deserves because they have done something very wrong; it especially refers to punishment or revenge that is carried out by someone other than official authorities.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>significant payback</em>
</span>
</span>
</div>
<a class='quick-help' href='retribution#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='retribution#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/retribution/memory_hooks/4967.json'></span>
<p>
<span class="emp0"><span>Re<span class="emp3"><span>tribution</span></span> Con<span class="emp3"><span>tribution</span></span></span></span> Each of the people wronged by the criminals con<span class="emp3"><span>tribut</span></span>ed something different to their just re<span class="emp3"><span>tribution</span></span>: one con<span class="emp3"><span>tribut</span></span>ed death, another con<span class="emp3"><span>tribut</span></span>ed destruction, and yet another con<span class="emp3"><span>tribut</span></span>ed an incurable disease to the group's most satisfying and successful re<span class="emp3"><span>tribution</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="retribution#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/retribution/memory_hooks">Use other hook</a>
<a href="retribution#" id="memhook-use-own" url="https://membean.com/mywords/retribution/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='retribution#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Following Kyle McLaren's vicious hit on Richard Zednik late in Game 4, Therrien vowed <b>retribution</b>. But Saturday's contest was a relatively tame affair. Only five minor penalties were called, none in the third period.
<cite class='attribution'>
&mdash;
UPI
</cite>
</li>
<li>
[Yuanmingyuan] was originally meant to be a more than 864-acre private pleasure garden for Chinese emperors, but it was destroyed in the 1860s by British and French forces who plundered and then burned the garden in <b>retribution</b> for prisoner deaths.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
“Because of technology, it’s become very easy for people to be pushed to do their jobs 24 hours a day,” Espinal said in an interview with _USA Today_, “and employees should have the right without fear of <b>retribution</b> to draw a clear line as to whether they want to work during their personal time.”
<cite class='attribution'>
&mdash;
Slate
</cite>
</li>
<li>
<b>Retribution</b> often means that we eventually do to ourselves what we have done unto others.
<cite class='attribution'>
&mdash;
Eric Hoffer, American philosopher
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/retribution/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='retribution#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='re_back' data-tree-url='//cdn1.membean.com/public/data/treexml' href='retribution#'>
<span class='common'></span>
re-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>back, again</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='tribut_divided' data-tree-url='//cdn1.membean.com/public/data/treexml' href='retribution#'>
<span class=''></span>
tribut
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>divided, granted, given credit</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ion_act' data-tree-url='//cdn1.membean.com/public/data/treexml' href='retribution#'>
<span class=''></span>
-ion
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>act, state, or result of doing something</td>
</tr>
</table>
<p><em>Retribution</em> is the &#8220;act of giving credit back&#8221; to another who has wronged you in order to settle the score.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='retribution#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>The Princess Bride</strong><span> Inigo Montoya seeks retribution for the death of his father at the hands of the six-fingered man.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/retribution.jpg' video_url='examplevids/retribution' video_width='350'></span>
<div id='wt-container'>
<img alt="Retribution" height="288" src="https://cdn1.membean.com/video/examplevids/retribution.jpg" width="350" />
<div class='center'>
<a href="retribution#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='retribution#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Retribution" src="https://cdn3.membean.com/public/images/wordimages/cons2/retribution.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='retribution#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>affliction</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>expiate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>inexorable</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>propitiate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>punitive</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>reciprocate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>recompense</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>recoup</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>redress</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>reprisal</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>restitution</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>retaliate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>tribulation</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>vendetta</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abolish</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abrogate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>clemency</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>indemnify</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>irrevocable</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>renege</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>rescind</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>unrequited</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="retribution" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>retribution</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="retribution#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

