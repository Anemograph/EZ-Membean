
<!DOCTYPE html>
<html>
<head>
<title>Word: adjunct | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Accretion is the slow, gradual process by which new things are added and something gets bigger.</p>
<p class='rw-defn idx1' style='display:none'>An addendum is an additional section added to the end of a book, document, or speech that gives more information.</p>
<p class='rw-defn idx2' style='display:none'>An adherent is a supporter or follower of a leader or a cause.</p>
<p class='rw-defn idx3' style='display:none'>If you have an affiliation with a group or another person, you are officially involved or connected with them.</p>
<p class='rw-defn idx4' style='display:none'>When something is ancillary to something else, such as a workbook to a textbook, it supports it but is less important than that which it supports.</p>
<p class='rw-defn idx5' style='display:none'>An appurtenance is a supporting feature, form of equipment, or item associated with a particular activity.</p>
<p class='rw-defn idx6' style='display:none'>An autonomous person makes their own decisions without being influenced by anyone else; an autonomous country or organization is independent and has the power to govern itself.</p>
<p class='rw-defn idx7' style='display:none'>An auxiliary device is one that acts in support of a main one, such as a backup generator that replaces power in case the main source of that power goes down.</p>
<p class='rw-defn idx8' style='display:none'>A coalition is a temporary union of different political or social groups that agrees to work together to achieve a shared aim.</p>
<p class='rw-defn idx9' style='display:none'>A codicil is a supplement, usually to a will, that is added after the main part has been written.</p>
<p class='rw-defn idx10' style='display:none'>When people collaborate, they work together, usually to solve some problem or issue.</p>
<p class='rw-defn idx11' style='display:none'>A complement to something else finishes it or brings it into a fuller state.</p>
<p class='rw-defn idx12' style='display:none'>Something is concomitant when it happens at the same time as something else and is connected with it in some fashion.</p>
<p class='rw-defn idx13' style='display:none'>Contiguous things are in contact with or near each other; contiguous events happen one right after the other without a break.</p>
<p class='rw-defn idx14' style='display:none'>A coterie is a small group of people who are close friends; therefore, when its members do things together, they do not want other people to join them.</p>
<p class='rw-defn idx15' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx16' style='display:none'>Dissension is a disagreement or difference of opinion among a group of people that can cause conflict.</p>
<p class='rw-defn idx17' style='display:none'>A dissident is someone who disagrees publicly with a government, especially in a country where this is not allowed.</p>
<p class='rw-defn idx18' style='display:none'>The juxtaposition of two objects is the act of positioning them side by side so that the differences between them are more readily visible.</p>
<p class='rw-defn idx19' style='display:none'>Liaison is the cooperation and exchange of information between people or organizations in order to facilitate their joint efforts; this word also refers to a person who coordinates a connection between people or organizations.</p>
<p class='rw-defn idx20' style='display:none'>A pariah is a social outcast.</p>
<p class='rw-defn idx21' style='display:none'>The propinquity of a thing is its nearness in location, relationship, or similarity to another thing.</p>
<p class='rw-defn idx22' style='display:none'>A schism causes a group or organization to divide into two groups as a result of differences in their aims and beliefs.</p>
<p class='rw-defn idx23' style='display:none'>When you supplement, you add on some extra to something to make up for a lack in it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>adjunct</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='adjunct#' id='pronounce-sound' path='audio/words/amy-adjunct'></a>
AJ-uhngkt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='adjunct#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='adjunct#' id='context-sound' path='audio/wordcontexts/brian-adjunct'></a>
Although Edward has not yet been offered a full professorship of English, he is still happy being an <em>adjunct</em> or addition to the English department.  Although someday he would like to become a full professor, now his helpful role as an <em>adjunct</em> or associate suits him just fine.  Edward has a personality that does not do well being in leadership roles, so an <em>adjunct</em> or secondary position is perfect for him at this point in his life.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean when something is an <em>adjunct</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is an essential part of something else.
</li>
<li class='choice answer '>
<span class='result'></span>
It is added on to something else.
</li>
<li class='choice '>
<span class='result'></span>
It is a cheap imitation of something valuable.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='adjunct#' id='definition-sound' path='audio/wordmeanings/amy-adjunct'></a>
An <em>adjunct</em> is something that is added to or joined to something else that is larger or more important.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>addition</em>
</span>
</span>
</div>
<a class='quick-help' href='adjunct#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='adjunct#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/adjunct/memory_hooks/3728.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Ad</span></span>ding Fuzzy <span class="emp1"><span>Junk</span></span></span></span> "More fuzzy <span class="emp1"><span>junk</span></span> to <span class="emp2"><span>ad</span></span>d to our already huge pile?" yelled Suzy as she gazed in dismay at yet another <span class="emp2"><span>ad</span></span><span class="emp1"><span>junc</span></span>t beanie baby her husband purchased to add to their monstrous collection of over 1000 of the little stuffed animals.
</p>
</div>

<div id='memhook-button-bar'>
<a href="adjunct#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/adjunct/memory_hooks">Use other hook</a>
<a href="adjunct#" id="memhook-use-own" url="https://membean.com/mywords/adjunct/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='adjunct#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Culture Minister Lisa Beare and the museum’s chairman of the board of directors, Daniel Muzyka, made the announcement Friday, saying more than 900 jobs would be created during construction of the 150,000-square-foot building on a three-hectare site that will serve as an <b>adjunct</b> to the current Downtown Victoria museum.
<cite class='attribution'>
&mdash;
The Vancouver Sun
</cite>
</li>
<li>
The students love the flexibility. And frankly, studies like that show us that the online modality can be a terrific <b>adjunct</b> to the in-person modality on campus. My belief is that the future is going to be blended [learning].
<cite class='attribution'>
&mdash;
Boston Magazine
</cite>
</li>
<li>
A quick—and free—fix is to turn on the closed captioning, which can be done in the TV's setup menus or the menus for your cable or satellite box. . . . They often prove to be a useful <b>adjunct</b> to the sound because they can help you make out words that you cannot clearly understand.
<cite class='attribution'>
&mdash;
Minneapolis Star Tribune
</cite>
</li>
<li>
The firm slices off discrete pieces of work—often research or proposal writing—for its <b>adjuncts</b>, who aren’t on the payroll but have kept their Booz Allene-mail addresses.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/adjunct/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='adjunct#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ad_towards' data-tree-url='//cdn1.membean.com/public/data/treexml' href='adjunct#'>
<span class='common'></span>
ad-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to, towards</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='junct_joined' data-tree-url='//cdn1.membean.com/public/data/treexml' href='adjunct#'>
<span class=''></span>
junct
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>joined, attached</td>
</tr>
</table>
<p>An <em>adjunct</em> is &#8220;joined to&#8221; or &#8220;attached towards&#8221; a larger whole.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='adjunct#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Tolarian Community College</strong><span> Adjunct professors fill in for classes that full-time tenured professors do not teach.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/adjunct.jpg' video_url='examplevids/adjunct' video_width='350'></span>
<div id='wt-container'>
<img alt="Adjunct" height="288" src="https://cdn1.membean.com/video/examplevids/adjunct.jpg" width="350" />
<div class='center'>
<a href="adjunct#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='adjunct#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Adjunct" src="https://cdn0.membean.com/public/images/wordimages/cons2/adjunct.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='adjunct#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>accretion</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>addendum</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>adherent</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>affiliation</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>ancillary</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>appurtenance</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>auxiliary</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>coalition</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>codicil</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>collaborate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>complement</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>concomitant</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>contiguous</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>coterie</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>juxtaposition</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>liaison</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>propinquity</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>supplement</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='6' class = 'rw-wordform notlearned'><span>autonomous</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>dissension</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>dissident</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>pariah</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>schism</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='adjunct#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
adjunct
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>additional</td>
</tr>
<tr>
<td class='wordform'>
<span>
adjoin
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to add to</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="adjunct" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>adjunct</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="adjunct#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

