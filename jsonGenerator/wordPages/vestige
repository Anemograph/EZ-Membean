
<!DOCTYPE html>
<html>
<head>
<title>Word: vestige | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>The process of ablation is the removal of diseased organs or harmful substances from the body, often through surgical procedure.</p>
<p class='rw-defn idx1' style='display:none'>Accretion is the slow, gradual process by which new things are added and something gets bigger.</p>
<p class='rw-defn idx2' style='display:none'>To agglomerate a group of things is to gather them together without any noticeable order.</p>
<p class='rw-defn idx3' style='display:none'>An aggregate is the final or sum total after many different amounts or scores have been added together.</p>
<p class='rw-defn idx4' style='display:none'>When two or more things, such as organizations, amalgamate, they combine to become one large thing.</p>
<p class='rw-defn idx5' style='display:none'>An artifact is a weapon, tool, or piece of art created by human beings that is historically and culturally interesting or valuable.</p>
<p class='rw-defn idx6' style='display:none'>A cardinal rule or quality is considered to be the most important or basic in a set of rules or qualities.</p>
<p class='rw-defn idx7' style='display:none'>Chaff is the husks of grain separated from the edible plant; it can also be useless matter in general that is cast aside.</p>
<p class='rw-defn idx8' style='display:none'>If liquid coagulates, it becomes thick and solid.</p>
<p class='rw-defn idx9' style='display:none'>If two or more things coalesce, they come together to form a single larger unit.</p>
<p class='rw-defn idx10' style='display:none'>When you collate pieces of information, you gather them all together and arrange them in some sensible order so that you can examine and compare those data efficiently.</p>
<p class='rw-defn idx11' style='display:none'>When a liquid congeals, it becomes very thick and sticky, almost like a solid.</p>
<p class='rw-defn idx12' style='display:none'>The crux of a problem or argument is the most important or difficult part of it that affects everything else.</p>
<p class='rw-defn idx13' style='display:none'>If you cull items or information, you gather them from a number of different places in a selective manner.</p>
<p class='rw-defn idx14' style='display:none'>Detritus is the small pieces of waste that remain after something has been destroyed or used.</p>
<p class='rw-defn idx15' style='display:none'>To disseminate something, such as knowledge or information, is to distribute it so that it reaches a lot of people.</p>
<p class='rw-defn idx16' style='display:none'>Dissolution is the breaking up or official end of a group, such as a couple or institution; it can also be the act of separating something into smaller components.</p>
<p class='rw-defn idx17' style='display:none'>Something described as dross is of very poor quality and has little or no value.</p>
<p class='rw-defn idx18' style='display:none'>When you excise something, you remove it by cutting it out.</p>
<p class='rw-defn idx19' style='display:none'>Something that is pivotal is more important than anything else in a situation or a system; consequently, it is the key to its success.</p>
<p class='rw-defn idx20' style='display:none'>The remnants of something are the small parts of it that are left after the main part has been used or destroyed.</p>
<p class='rw-defn idx21' style='display:none'>A seminal article, book, piece of music or other work has many new ideas; additionally, it has great influence on generating ideas for future work.</p>
<p class='rw-defn idx22' style='display:none'>Shards are sharp pieces of broken glass, pottery, metal, or other hard substances.</p>
<p class='rw-defn idx23' style='display:none'>Sporadic occurrences happen from time to time but not at constant or regular intervals.</p>
<p class='rw-defn idx24' style='display:none'>Something that is superfluous is unnecessary; it is more than what is wanted or needed at the current time.</p>
<p class='rw-defn idx25' style='display:none'>If you refer to something as a trifle, you mean that it is of little or no importance or value.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
Middle/High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>vestige</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='vestige#' id='pronounce-sound' path='audio/words/amy-vestige'></a>
VES-tij
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='vestige#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='vestige#' id='context-sound' path='audio/wordcontexts/brian-vestige'></a>
The teddy bear was the last <em>vestige</em> or trace of Keegan&#8217;s youth&#8212;all his other toys had been given away or stored in the attic.  Any <em>vestiges</em> or signs of childhood were carefully hidden away, except for the bear who was placed upon a shelf of honor amongst Keegan&#8217;s computer, iPod, and school books.  Keegan would have been embarrassed if any of his friends came over to view too many <em>vestiges</em> or remains of his early childhood years lying around!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>vestige</em> of something?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A costume that is designed to make a person look just like a fire fighter.
</li>
<li class='choice '>
<span class='result'></span>
A small model that shows what a building will be like once it is built. 
</li>
<li class='choice answer '>
<span class='result'></span>
A brick chimney that&#8217;s left standing after a house has burned to the ground.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='vestige#' id='definition-sound' path='audio/wordmeanings/amy-vestige'></a>
A <em>vestige</em> of something is a very small part that remains from a once larger whole.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>trace</em>
</span>
</span>
</div>
<a class='quick-help' href='vestige#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='vestige#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/vestige/memory_hooks/3304.json'></span>
<p>
<span class="emp0"><span>No <span class="emp3"><span>Vest</span></span>iges of the <span class="emp3"><span>Vest</span></span>al V<span class="emp1"><span>i</span></span>r<span class="emp1"><span>g</span></span>in<span class="emp1"><span>s</span></span></span></span> Although <span class="emp3"><span>vest</span></span><span class="emp1"><span>ig</span></span>e<span class="emp1"><span>s</span></span> of the Temple of <span class="emp3"><span>Vest</span></span>a remain, no remains or <span class="emp3"><span>vest</span></span><span class="emp1"><span>ig</span></span>e<span class="emp1"><span>s</span></span> of the <span class="emp3"><span>Vest</span></span>al V<span class="emp1"><span>i</span></span>r<span class="emp1"><span>g</span></span>in<span class="emp1"><span>s</span></span> are in existence.
</p>
</div>

<div id='memhook-button-bar'>
<a href="vestige#" id="add-public-hook" style="" url="https://membean.com/mywords/vestige/memory_hooks">Use other public hook</a>
<a href="vestige#" id="memhook-use-own" url="https://membean.com/mywords/vestige/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='vestige#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
There’s no staircase, either, so Veevers-Carter shimmies down a ladder to the basement and leads me to a small patch of wall that still retains a bit of leaf-green paint. Like Indiana Jones discovering the lost ark, he thinks he’s unearthed the last <b>vestige</b> of the old kitchen.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Of traveling—and eating—around the Mediterranean, Jenkins remembers "an abundance of very fresh food, uncomplicated cooking—and lots of olive oil," she told Renee Montagne. Another <b>vestige</b> of those days is a deep love for figs, which can be so good fresh, Jenkins says, that it’s hard for her to buy them in a store.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Tension, exhaustion, and the last <b>vestige</b> of my finely honed American work ethic melt away under her expert fingers, carrying me into another world.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
Relics of a time before the dinosaurs, these aquatic giants hail from a period when amphibians dominated the landscape. . . . "In a sense, [the giant salamander] is like the last <b>vestige</b> of this previous freshwater megafauna," says Samuel Turvey. . . . "If you lose any of those species you're losing part of a very long branch of evolutionary history, rather than just a twig."
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/vestige/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='vestige#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='vestig_footprint' data-tree-url='//cdn1.membean.com/public/data/treexml' href='vestige#'>
<span class=''></span>
vestig
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>footprint</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='vestige#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>A <em>vestige</em> is but a &#8220;footprint&#8221; left behind as evidence that indicates a larger whole, such as the &#8220;footprint&#8221; a person leaves in wet sand which suggests the whole of the body that made the &#8220;footprint.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='vestige#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>The Picture of Dorian Gray</strong><span> He wants to make sure that not a vestige remains of the person upstairs.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/vestige.jpg' video_url='examplevids/vestige' video_width='350'></span>
<div id='wt-container'>
<img alt="Vestige" height="288" src="https://cdn1.membean.com/video/examplevids/vestige.jpg" width="350" />
<div class='center'>
<a href="vestige#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='vestige#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Vestige" src="https://cdn2.membean.com/public/images/wordimages/cons2/vestige.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='vestige#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>ablation</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>artifact</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>chaff</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>cull</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>detritus</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>disseminate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>dissolution</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>dross</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>excise</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>remnant</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>shard</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>sporadic</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>superfluous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>trifle</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>accretion</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>agglomerate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>aggregate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>amalgamate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>cardinal</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>coagulate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>coalesce</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>collate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>congeal</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>crux</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>pivotal</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>seminal</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='vestige#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
vestigial
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or pertaining to a trace of something larger</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="vestige" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>vestige</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="vestige#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

