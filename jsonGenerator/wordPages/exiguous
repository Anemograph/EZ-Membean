
<!DOCTYPE html>
<html>
<head>
<title>Word: exiguous | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Exiguous-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/exiguous-large.jpg?qdep8" />
</div>
<a href="exiguous#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>Affluence is the state of having a lot of money and many possessions, granting one a high economic standard of living.</p>
<p class='rw-defn idx2' style='display:none'>A copious amount of something is a very large amount of it.</p>
<p class='rw-defn idx3' style='display:none'>A cornucopia is a large quantity and variety of something good and nourishing.</p>
<p class='rw-defn idx4' style='display:none'>When there is a dearth of something, there is a scarcity or lack of it.</p>
<p class='rw-defn idx5' style='display:none'>When you deplete a supply of something, you use it up or lessen its amount over a given period of time.</p>
<p class='rw-defn idx6' style='display:none'>When an area is devoid of life, it is empty or completely lacking in it.</p>
<p class='rw-defn idx7' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx8' style='display:none'>Something expansive has a wide scope or is large in area.</p>
<p class='rw-defn idx9' style='display:none'>If someone, such as a writer, is described as having fecundity, they have the ability to produce many original and different ideas.</p>
<p class='rw-defn idx10' style='display:none'>A granule is a small particle or tiny grain of something.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is impecunious has very little money, especially over a long period of time.</p>
<p class='rw-defn idx12' style='display:none'>Something infinitesimal is so extremely small or minute that it is very hard to measure it.</p>
<p class='rw-defn idx13' style='display:none'>Lavish praise, giving, or a meal is rich, plentiful, or very generous; it can sometimes border on being too much.</p>
<p class='rw-defn idx14' style='display:none'>Something luxuriant, such as plants or hair, is growing well and is very healthy.</p>
<p class='rw-defn idx15' style='display:none'>Something minuscule is extremely small in size or amount.</p>
<p class='rw-defn idx16' style='display:none'>Nominal can refer to someone who is in charge in name only, or it can refer to a very small amount of something; both are related by their relative insignificance.</p>
<p class='rw-defn idx17' style='display:none'>Something that is omnipresent appears to be everywhere at the same time or is ever-present.</p>
<p class='rw-defn idx18' style='display:none'>Something that is opulent is very impressive, grand, and is made from expensive materials.</p>
<p class='rw-defn idx19' style='display:none'>Something that is paltry is practically worthless or insignificant.</p>
<p class='rw-defn idx20' style='display:none'>A pandemic disease is a far-reaching epidemic that affects people in a very wide geographic area.</p>
<p class='rw-defn idx21' style='display:none'>A panoply is a large and impressive collection of people or things.</p>
<p class='rw-defn idx22' style='display:none'>A parsimonious person is not willing to give or spend money.</p>
<p class='rw-defn idx23' style='display:none'>A paucity of something is not enough of it.</p>
<p class='rw-defn idx24' style='display:none'>Penury is the state of being extremely poor.</p>
<p class='rw-defn idx25' style='display:none'>If you say that you&#8217;ve received a pittance, you mean that you received a small amount of something—and you know that you deserved more.</p>
<p class='rw-defn idx26' style='display:none'>A plethora of something is too much of it.</p>
<p class='rw-defn idx27' style='display:none'>A preponderance of things of a particular type in a group means that there are more of that type than of any other.</p>
<p class='rw-defn idx28' style='display:none'>If you suffer privation, you live without many of the basic things required for a comfortable life.</p>
<p class='rw-defn idx29' style='display:none'>Something that is prodigious is very large, impressive, or great.</p>
<p class='rw-defn idx30' style='display:none'>A profusion of something is a very large quantity or variety of it.</p>
<p class='rw-defn idx31' style='display:none'>Something or someone that is prolific is highly fruitful and so produces a lot of something.</p>
<p class='rw-defn idx32' style='display:none'>If something bad, such as crime or disease, is rampant, there is a lot of it—and it is increasing in a way that is difficult to control.</p>
<p class='rw-defn idx33' style='display:none'>When you replenish something, you refill or restock it.</p>
<p class='rw-defn idx34' style='display:none'>Stores that are replete with goods to sell are well-stocked and completely supplied to satisfy many customers.</p>
<p class='rw-defn idx35' style='display:none'>If you say that something bad or unpleasant is rife somewhere, you mean that it is very common.</p>
<p class='rw-defn idx36' style='display:none'>A scant amount of something is a very limited or slight amount of it; hence, it is inadequate or insufficient in size or quantity.</p>
<p class='rw-defn idx37' style='display:none'>A spate of things is a sudden series, significant quantity, or outpouring of them.</p>
<p class='rw-defn idx38' style='display:none'>Something that is superfluous is unnecessary; it is more than what is wanted or needed at the current time.</p>
<p class='rw-defn idx39' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx40' style='display:none'>A torrential downpour of rain is very heavy and intense.</p>
<p class='rw-defn idx41' style='display:none'>Something that is voluminous is long, large, or vast in size.</p>
<p class='rw-defn idx42' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>exiguous</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='exiguous#' id='pronounce-sound' path='audio/words/amy-exiguous'></a>
ig-ZIG-yoo-uhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='exiguous#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='exiguous#' id='context-sound' path='audio/wordcontexts/brian-exiguous'></a>
Although the man used to have a full head of hair, the hairs on his scalp are now more <em>exiguous</em>, or scanty, than they had been in his youth.  With his current <em>exiguous</em> or limited amount of hair, he is a bit self-conscious, but nobody really notices it as much as he does.  In fact, other men who sport similarly <em>exiguous</em> or slight amounts of hair don&#8217;t notice his at all, since they are focused on their own rapidly thinning locks.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an <em>exiguous</em> retirement savings?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
One that has experienced sudden growth.
</li>
<li class='choice answer '>
<span class='result'></span>
One that is likely not enough on which to live.
</li>
<li class='choice '>
<span class='result'></span>
One that is provided by an employer.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='exiguous#' id='definition-sound' path='audio/wordmeanings/amy-exiguous'></a>
An <em>exiguous</em> amount of something is meager, small, or limited in nature.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>small</em>
</span>
</span>
</div>
<a class='quick-help' href='exiguous#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='exiguous#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/exiguous/memory_hooks/4534.json'></span>
<p>
<span class="emp0"><span>P<span class="emp1"><span>ig</span></span> H<span class="emp3"><span>ex</span></span></span></span> That old witch put a killing h<span class="emp3"><span>ex</span></span> on my p<span class="emp1"><span>ig</span></span>s, so I now have only an <span class="emp1"><span>ex</span></span><span class="emp3"><span>ig</span></span>uous number of them!
</p>
</div>

<div id='memhook-button-bar'>
<a href="exiguous#" id="add-public-hook" style="" url="https://membean.com/mywords/exiguous/memory_hooks">Use other public hook</a>
<a href="exiguous#" id="memhook-use-own" url="https://membean.com/mywords/exiguous/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='exiguous#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
From the very beginning, Buckley acknowledged, his ratings had been “<b>exiguous</b>,” to use one of his distinctive vocabulary words—that is to say, meager. Buckley was pleased with his low ratings, because he knew that his erudite conversations would inherently appeal only to a narrow audience of political junkies.
<cite class='attribution'>
&mdash;
Politico
</cite>
</li>
<li>
You can look at Phoenix as a pretty good example of what the new urbanism is up against. . . . It has a downtown so <b>exiguous</b> that a pedestrian outside its biggest office building at 9 on a weekday morning is a phenomenon as singular as a cow in Times Square.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
For all of this extravagance, Fedorov fits into a long Russian tradition of extreme humility and selflessness. . . . He gave away his <b>exiguous</b> salary as a Moscow librarian, did not buy clothes, never married, and hardly ate.
<cite class='attribution'>
&mdash;
Harper’s Magazine
</cite>
</li>
<li>
So orange-fragrant John came home each day / To a mortgaged house, <b>exiguous</b> but clean, / Out of the noisy town, near Crosby way
<cite class='attribution'>
&mdash;
Anthony Burgess, English writer and composer in _Byrne_
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/exiguous/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='exiguous#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ex_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='exiguous#'>
<span class=''></span>
ex-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ig_do' data-tree-url='//cdn1.membean.com/public/data/treexml' href='exiguous#'>
<span class='common'></span>
ig
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>act, do, drive</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='uous_nature' data-tree-url='//cdn1.membean.com/public/data/treexml' href='exiguous#'>
<span class=''></span>
-uous
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of the nature of</td>
</tr>
</table>
<p>The idea is that when one &#8220;thoroughly weighs&#8221; something, it is being &#8220;weighed strictly&#8221; because there is not much of it.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='exiguous#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Exiguous" src="https://cdn2.membean.com/public/images/wordimages/cons2/exiguous.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='exiguous#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>dearth</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>deplete</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>devoid</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>granule</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>impecunious</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>infinitesimal</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>minuscule</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>nominal</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>paltry</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>parsimonious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>paucity</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>penury</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>pittance</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>privation</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>scant</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>affluence</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>copious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>cornucopia</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>expansive</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>fecundity</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>lavish</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>luxuriant</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>omnipresent</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>opulent</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>pandemic</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>panoply</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>plethora</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>preponderance</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>prodigious</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>profusion</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>prolific</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>rampant</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>replenish</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>replete</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>rife</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>spate</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>superfluous</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>torrential</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>voluminous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='exiguous#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
exiguity
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>the condition of having a small or scanty amount of something</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="exiguous" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>exiguous</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="exiguous#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

