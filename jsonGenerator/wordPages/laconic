
<!DOCTYPE html>
<html>
<head>
<title>Word: laconic | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abridge a book or play, you make it shorter by making cuts to the original.</p>
<p class='rw-defn idx1' style='display:none'>When a person is speaking or writing in a bombastic fashion, they are very self-centered, extremely showy, and excessively proud.</p>
<p class='rw-defn idx2' style='display:none'>Brevity is communicating by using just a few words or by taking very little time to do so.</p>
<p class='rw-defn idx3' style='display:none'>If you describe a person&#8217;s behavior or speech as brusque, you mean that they say or do things abruptly or too quickly in a somewhat rude and impolite way.</p>
<p class='rw-defn idx4' style='display:none'>When there is a dearth of something, there is a scarcity or lack of it.</p>
<p class='rw-defn idx5' style='display:none'>When something is diffuse, it is widely spread out and scattered.</p>
<p class='rw-defn idx6' style='display:none'>A piece of writing is discursive if it includes a lot of information that is not relevant to the main subject.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is effusive expresses happiness, pleasure, admiration, praise, etc., in an extremely enthusiastic way.</p>
<p class='rw-defn idx8' style='display:none'>To expatiate upon a subject is to speak or write in detail and at length about it.</p>
<p class='rw-defn idx9' style='display:none'>A garrulous person talks a lot, especially about unimportant things.</p>
<p class='rw-defn idx10' style='display:none'>If someone is being glib, they make something sound simple, easy, and problem-free— when it isn&#8217;t at all.</p>
<p class='rw-defn idx11' style='display:none'>Grandiloquent speech is highly formal, exaggerated, and often seems both silly and hollow because it is expressly used to appear impressive and important.</p>
<p class='rw-defn idx12' style='display:none'>If something is gratuitous, it is freely given; nevertheless, it is usually unnecessary and can be harmful or upsetting.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is loquacious talks too much.</p>
<p class='rw-defn idx14' style='display:none'>A paucity of something is not enough of it.</p>
<p class='rw-defn idx15' style='display:none'>A peroration is a long speech that sounds impressive but does not have much substance.</p>
<p class='rw-defn idx16' style='display:none'>A pithy statement or piece of writing is brief but intelligent; it is also precise and to the point.</p>
<p class='rw-defn idx17' style='display:none'>Prattle is mindless chatter that seems like it will never stop.</p>
<p class='rw-defn idx18' style='display:none'>A profusion of something is a very large quantity or variety of it.</p>
<p class='rw-defn idx19' style='display:none'>Something that is prolix, such as a lecture or speech, is excessively wordy; consequently, it can be tiresome to read or listen to.</p>
<p class='rw-defn idx20' style='display:none'>If something is redundant, it exceeds what is necessary or is needlessly wordy or repetitive.</p>
<p class='rw-defn idx21' style='display:none'>If someone is sententious, they are terse or brief in writing and speech; they also often use proverbs to appear morally upright and wise.</p>
<p class='rw-defn idx22' style='display:none'>Something that is succinct is clearly and briefly explained without using any unnecessary words.</p>
<p class='rw-defn idx23' style='display:none'>Something that is superfluous is unnecessary; it is more than what is wanted or needed at the current time.</p>
<p class='rw-defn idx24' style='display:none'>A tacit agreement between two people is understood without having to use words to express it.</p>
<p class='rw-defn idx25' style='display:none'>A taciturn person is quiet or silent by nature.</p>
<p class='rw-defn idx26' style='display:none'>To be terse in speech is to be short and to the point, often in an abrupt manner that may seem unfriendly.</p>
<p class='rw-defn idx27' style='display:none'>If you truncate something, you make it shorter or quicker by removing or cutting off part of it.</p>
<p class='rw-defn idx28' style='display:none'>Something that is verbose, such as a speech or article, contains too many words.</p>
<p class='rw-defn idx29' style='display:none'>A voluble person can speak easily with speech that naturally flows; voluble people also tend to be quite chatty.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>laconic</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='laconic#' id='pronounce-sound' path='audio/words/amy-laconic'></a>
luh-KON-ik
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='laconic#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='laconic#' id='context-sound' path='audio/wordcontexts/brian-laconic'></a>
The train conductor gave Allison directions from the station in clipped, brief, <em>laconic</em> phrases.  He sounded unkind, even though he was supposed to be helpful, and his <em>laconic</em>, short answers made the travel-worn Allison feel somewhat slighted.  In reality, the <em>laconic</em> conductor was merely trying to be clear and save time with his use of few words.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Which of the following is the best example in history of a <em>laconic</em> message?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
When Philip II threatened to destroy Sparta in 346 <span class="caps">BCE</span> with his armies, Spartan leaders supposedly responded with the single word “If.”
</li>
<li class='choice '>
<span class='result'></span>
In 1897, English composer Edward Elgar wrote an eighty-seven-character thank-you note to a family he&#8217;d visited that was entirely in unreadable code.
</li>
<li class='choice '>
<span class='result'></span>
Sir Ivan Lawrence, a member of the British Parliament, spoke for four hours and twenty-three minutes in 1985 during a debate about water.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='laconic#' id='definition-sound' path='audio/wordmeanings/amy-laconic'></a>
A person who is being <em>laconic</em> uses very few words to say something.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>to the point</em>
</span>
</span>
</div>
<a class='quick-help' href='laconic#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='laconic#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/laconic/memory_hooks/5701.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>La</span></span>cey's <span class="emp3"><span>Ic</span></span>e Cream <span class="emp2"><span>Con</span></span>e</span></span> <span class="emp1"><span>La</span></span>cey cried and complained and whined until her mother finally gave her the <span class="emp3"><span>ic</span></span>e cream <span class="emp2"><span>con</span></span>e she had so been wanting, at which point <span class="emp1"><span>La</span></span>cey murmured a <span class="emp1"><span>la</span></span><span class="emp2"><span>con</span></span><span class="emp3"><span>ic</span></span> "thanks" and didn't say another word.
</p>
</div>

<div id='memhook-button-bar'>
<a href="laconic#" id="add-public-hook" style="" url="https://membean.com/mywords/laconic/memory_hooks">Use other public hook</a>
<a href="laconic#" id="memhook-use-own" url="https://membean.com/mywords/laconic/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='laconic#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Spartans were notably pithy and sharp-tongued—the term "<b>laconic</b>" is derived from the Spartan word for "Spartan"—and left a legacy of one-liners all pretty much celebrating their own tough hides. "With your shield, or on it," said loving Spartan mothers to their sons, meaning return from battle victorious, or dead.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Harry Bosch, in Amazon’s Los Angeles cop procedural, and Doron Kavillio, in Netflix’s Israeli counterterrorism thriller, would have a lot to talk about if they weren’t so <b>laconic</b>.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Colleagues found all three Apollo 11 crew mates <b>laconic—</b>"the quietest crew in manned space flight history," one said at the time.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Iowans saw a subdued, <b>laconic</b> candidate who spoke in a soft monotone, . . . Mr. Thompson told few jokes and, while an easygoing presence, did not appear to have much interest in the small talk that is a staple of retail campaigning.
<cite class='attribution'>
&mdash;
NBC News
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/laconic/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='laconic#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>lacon</td>
<td>
&rarr;
</td>
<td class='meaning'>a Spartan</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ic_like' data-tree-url='//cdn1.membean.com/public/data/treexml' href='laconic#'>
<span class=''></span>
-ic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>like</td>
</tr>
</table>
<p>Someone who speaks in a <em>laconic</em> fashion speaks &#8220;like&#8221; the ancient &#8220;Spartans,&#8221; who were famous for their use of very few words when they spoke.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='laconic#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Laconic" src="https://cdn0.membean.com/public/images/wordimages/cons2/laconic.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='laconic#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abridge</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>brevity</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>brusque</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>dearth</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>paucity</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>pithy</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>succinct</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>tacit</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>taciturn</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>terse</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>truncate</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>bombastic</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>diffuse</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>discursive</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>effusive</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>expatiate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>garrulous</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>glib</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>grandiloquent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>gratuitous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>loquacious</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>peroration</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>prattle</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>profusion</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>prolix</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>redundant</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>sententious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>superfluous</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>verbose</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>voluble</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="laconic" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>laconic</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="laconic#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

