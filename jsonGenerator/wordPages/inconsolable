
<!DOCTYPE html>
<html>
<head>
<title>Word: inconsolable | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>The word abject emphasizes a very bad situation or quality, thereby making it even worse.</p>
<p class='rw-defn idx1' style='display:none'>If you alleviate pain or suffering, you make it less intense or severe.</p>
<p class='rw-defn idx2' style='display:none'>When you ameliorate a bad condition or situation, you make it better in some way.</p>
<p class='rw-defn idx3' style='display:none'>A bereaved person is someone whose close friend or relative has recently died.</p>
<p class='rw-defn idx4' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx5' style='display:none'>When you cavort, you jump and dance around in a playful, excited, or physically lively way.</p>
<p class='rw-defn idx6' style='display:none'>If you condole with someone, you express sympathy or sorrow, usually on the death of someone dear.</p>
<p class='rw-defn idx7' style='display:none'>A convalescent person spends time resting to gain health and strength after a serious illness or operation.</p>
<p class='rw-defn idx8' style='display:none'>A convivial atmosphere or occasion is friendly, pleasant, cheerful, and relaxed.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is crestfallen is severely disappointed, sad, or depressed.</p>
<p class='rw-defn idx10' style='display:none'>If you are despondent, you are extremely unhappy because you are in an unpleasant situation that you do not think will improve.</p>
<p class='rw-defn idx11' style='display:none'>A dirge is a slow and sad piece of music often performed at funerals.</p>
<p class='rw-defn idx12' style='display:none'>If something discomfits you, it makes you feel embarrassed, confused, uncomfortable, or frustrated.</p>
<p class='rw-defn idx13' style='display:none'>If you are disconsolate, you are very unhappy or so sad that nothing will make you feel better.</p>
<p class='rw-defn idx14' style='display:none'>When you disillusion someone, you show that a belief they hold dear is untrue—thereby disheartening and disappointing them.</p>
<p class='rw-defn idx15' style='display:none'>When someone feels disquiet about a situation, they feel very worried or nervous.</p>
<p class='rw-defn idx16' style='display:none'>Something that is dolorous, such as music or news, causes mental pain and sorrow because it itself is full of grief and sorrow.</p>
<p class='rw-defn idx17' style='display:none'>Weather that is dreary tends to be depressing and gloomy; a situation or person that is dreary tends to be boring or uninteresting.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx19' style='display:none'>When you are ecstatic about something, you are overjoyed or extremely happy about it.</p>
<p class='rw-defn idx20' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx21' style='display:none'>A state of euphoria is one of extreme happiness or overwhelming joy.</p>
<p class='rw-defn idx22' style='display:none'>If you show exuberance, you display great excitement, energy, and enthusiasm.</p>
<p class='rw-defn idx23' style='display:none'>If you exult, you show great pleasure and excitement, especially about something you have achieved.</p>
<p class='rw-defn idx24' style='display:none'>A forlorn person is lonely because they have been abandoned; a forlorn home has been deserted.</p>
<p class='rw-defn idx25' style='display:none'>If someone gambols, they run, jump, and play like a young child or animal.</p>
<p class='rw-defn idx26' style='display:none'>When you have a feeling of gratification, you experience satisfaction or pleasure about a job well done.</p>
<p class='rw-defn idx27' style='display:none'>Someone who is jovial is in a good humor, lighthearted, and jolly.</p>
<p class='rw-defn idx28' style='display:none'>If someone is lachrymose, they tend to cry often; if something, such as a song, is lachrymose, it tends to cause tears because it is so sad.</p>
<p class='rw-defn idx29' style='display:none'>A lamentable state of affairs is miserable and just plain awful.</p>
<p class='rw-defn idx30' style='display:none'>Levity is an amusing way of speaking or behaving during a serious situation; it can lighten the moment but can also be considered inappropriate.</p>
<p class='rw-defn idx31' style='display:none'>If someone is lugubrious, they are looking very sad or gloomy.</p>
<p class='rw-defn idx32' style='display:none'>If you are melancholy, you look and feel sad.</p>
<p class='rw-defn idx33' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx34' style='display:none'>If you mollify someone, you say or do something to make that person feel less angry or upset.</p>
<p class='rw-defn idx35' style='display:none'>Someone who is morose is unhappy, bad-tempered, and unwilling to talk very much.</p>
<p class='rw-defn idx36' style='display:none'>If you are pensive, you are deeply thoughtful, often in a sad and/or serious way.</p>
<p class='rw-defn idx37' style='display:none'>A plaintive sound or voice expresses sadness.</p>
<p class='rw-defn idx38' style='display:none'>When someone is rehabilitated, they are restored to a more normal way of life, such as returning to good health or a crime-free life.</p>
<p class='rw-defn idx39' style='display:none'>Something or someone that shows resilience is able to recover quickly and easily from unpleasant, difficult, and damaging situations or events.</p>
<p class='rw-defn idx40' style='display:none'>Revelry is a festive celebration that includes wild, noisy, and happy dancing, eating, and drinking.</p>
<p class='rw-defn idx41' style='display:none'>If you are sanguine about a situation, especially a difficult one, you are confident and cheerful that everything will work out the way you want it to.</p>
<p class='rw-defn idx42' style='display:none'>Someone who is saturnine is looking miserable and sad, sometimes in a threatening or unfriendly way.</p>
<p class='rw-defn idx43' style='display:none'>If someone is described as vivacious, they are lively and have a happy, lighthearted manner.</p>
<p class='rw-defn idx44' style='display:none'>Someone who is woebegone is very sad and filled with grief.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>inconsolable</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='inconsolable#' id='pronounce-sound' path='audio/words/amy-inconsolable'></a>
in-kuhn-SOH-luh-bulh
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='inconsolable#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='inconsolable#' id='context-sound' path='audio/wordcontexts/brian-inconsolable'></a>
Jill was simply <em>inconsolable</em> or unable to be comforted when both of her children left for college to a state far away.  She moped about in her <em>inconsolable</em> or brokenhearted state for many months, feeling very depressed and sad.  Then the holidays came, and she went from being <em>inconsolable</em> and distressed to happy and smiling once again when her children came home to visit.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Why might someone be <em>inconsolable</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Their cell phone is not working, and no one can get in touch with them. 
</li>
<li class='choice answer '>
<span class='result'></span>
Their house was destroyed in a fire, and they are terribly upset about it.
</li>
<li class='choice '>
<span class='result'></span>
They are on a crowded train and refuse to give up their seat to an older woman. 
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='inconsolable#' id='definition-sound' path='audio/wordmeanings/amy-inconsolable'></a>
Someone who is <em>inconsolable</em> has been so devastated by a terrible event that no one can help them feel better about it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>wretched and miserable</em>
</span>
</span>
</div>
<a class='quick-help' href='inconsolable#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='inconsolable#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/inconsolable/memory_hooks/5837.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>N</span></span>ot <span class="emp2"><span>Able</span></span> to be <span class="emp3"><span>Consol</span></span>ed</span></span> Someone who is i<span class="emp1"><span>n</span></span><span class="emp3"><span>consol</span></span><span class="emp2"><span>able</span></span> is <span class="emp1"><span>n</span></span>ot <span class="emp2"><span>able</span></span> to be <span class="emp3"><span>consol</span></span>ed.
</p>
</div>

<div id='memhook-button-bar'>
<a href="inconsolable#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/inconsolable/memory_hooks">Use other hook</a>
<a href="inconsolable#" id="memhook-use-own" url="https://membean.com/mywords/inconsolable/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='inconsolable#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Regret for the things we did can be tempered by time; it is regret for the things we did not do that is <b>inconsolable</b>.
<span class='attribution'>&mdash; Sidney J. Harris, 20th century American journalist</span>
<img alt="Sidney j" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Sidney J. Harris, 20th century American journalist.jpg?qdep8" width="80" />
</li>
<li>
"This has not been an easy decision," Egidio Torre said after his party officially registered him with the state electoral commission. "The most powerful reason that has brought me here is the loss of my brother, that vital loss, the <b>inconsolable</b> absence of my brother Rodolfo."
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
The first letter is from a Hartford lady, whose little daughter was <b>inconsolable</b> on election night at discovering that all her interest in Gen. Garfield was unknown to the latter.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/inconsolable/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='inconsolable#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='in_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='inconsolable#'>
<span class='common'></span>
in-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='con_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='inconsolable#'>
<span class='common'></span>
con-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='sol_comfort' data-tree-url='//cdn1.membean.com/public/data/treexml' href='inconsolable#'>
<span class=''></span>
sol
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>comfort, relieve</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='able_handy' data-tree-url='//cdn1.membean.com/public/data/treexml' href='inconsolable#'>
<span class=''></span>
-able
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>handy, capable of</td>
</tr>
</table>
<p>Someone who is <em>inconsolable</em> is &#8220;not capable of being thoroughly comforted or relieved&#8221; of something bothering her.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='inconsolable#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Friends</strong><span> The baby is inconsolable.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/inconsolable.jpg' video_url='examplevids/inconsolable' video_width='350'></span>
<div id='wt-container'>
<img alt="Inconsolable" height="288" src="https://cdn1.membean.com/video/examplevids/inconsolable.jpg" width="350" />
<div class='center'>
<a href="inconsolable#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='inconsolable#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Inconsolable" src="https://cdn3.membean.com/public/images/wordimages/cons2/inconsolable.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='inconsolable#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abject</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bereaved</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>crestfallen</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>despondent</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>dirge</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>discomfit</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>disconsolate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>disillusion</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>disquiet</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>dolorous</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>dreary</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>forlorn</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>lachrymose</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>lamentable</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>lugubrious</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>melancholy</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>morose</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>pensive</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>plaintive</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>saturnine</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>woebegone</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>alleviate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>ameliorate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>cavort</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>condole</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>convalescent</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>convivial</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>ecstatic</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>euphoria</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>exuberance</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>exult</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>gambol</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>gratification</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>jovial</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>levity</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>mollify</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>rehabilitate</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>resilience</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>revelry</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>sanguine</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>vivacious</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='inconsolable#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
console
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to comfort</td>
</tr>
<tr>
<td class='wordform'>
<span>
solace
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>comfort, relief</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="inconsolable" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>inconsolable</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="inconsolable#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

