
<!DOCTYPE html>
<html>
<head>
<title>Word: nebulous | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Something that is amorphous has no clear shape, boundaries, or structure.</p>
<p class='rw-defn idx1' style='display:none'>A state of anarchy occurs when there is no organization or order in a nation or country, especially when no effective government exists.</p>
<p class='rw-defn idx2' style='display:none'>If something is an anomaly, it is different from what is usual or expected; hence, it is highly noticeable.</p>
<p class='rw-defn idx3' style='display:none'>When you describe someone as astute, you think they quickly understand situations or behavior and use it to their advantage.</p>
<p class='rw-defn idx4' style='display:none'>If one&#8217;s powers or rights are circumscribed, they are limited or restricted.</p>
<p class='rw-defn idx5' style='display:none'>A clarion call is a stirring, emotional, and strong appeal to people to take action on something.</p>
<p class='rw-defn idx6' style='display:none'>The word corporeal refers to the physical or material world rather than the spiritual; it also means characteristic of the body rather than the mind or feelings.</p>
<p class='rw-defn idx7' style='display:none'>If you delineate something, such as an idea or situation or border, you describe it in great detail.</p>
<p class='rw-defn idx8' style='display:none'>A diaphanous cloth is thin enough to see through.</p>
<p class='rw-defn idx9' style='display:none'>If you elucidate something, you make it easier to understand by giving more relevant information.</p>
<p class='rw-defn idx10' style='display:none'>Exegesis is a detailed explanation or interpretation of a piece of writing, especially a religious one.</p>
<p class='rw-defn idx11' style='display:none'>A generic description or attribute is not specific to any one thing but applies to all members of an entire class or group.</p>
<p class='rw-defn idx12' style='display:none'>A gossamer material is very thin, light, and delicate.</p>
<p class='rw-defn idx13' style='display:none'>If an idea, plan, or attitude is inchoate, it is vague and imperfectly formed because it is just starting to develop.</p>
<p class='rw-defn idx14' style='display:none'>Someone who is speaking in an incoherent fashion cannot be understood or is very hard to understand.</p>
<p class='rw-defn idx15' style='display:none'>If the results of something are inconclusive, they are uncertain or provide no final answers.</p>
<p class='rw-defn idx16' style='display:none'>An inference is a conclusion that is reached using available data.</p>
<p class='rw-defn idx17' style='display:none'>Someone who is irresolute is unable to decide what to do.</p>
<p class='rw-defn idx18' style='display:none'>If someone is lucid, they are able to understand things and think clearly; this adjective also describes writing or speech that is crystal clear and easy to understand.</p>
<p class='rw-defn idx19' style='display:none'>A mercurial person&#8217;s mind or mood changes often, quickly, and unpredictably.</p>
<p class='rw-defn idx20' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx21' style='display:none'>Something is nondescript when its appearance is ordinary, dull, and not at all interesting or attractive.</p>
<p class='rw-defn idx22' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx23' style='display:none'>An oblique statement is not straightforward or direct; rather, it is purposely roundabout, vague, or misleading.</p>
<p class='rw-defn idx24' style='display:none'>Something is opaque if it is either difficult to understand or is not transparent.</p>
<p class='rw-defn idx25' style='display:none'>Something that is pellucid is either extremely clear because it is transparent to the eye or it is very easy for the mind to understand.</p>
<p class='rw-defn idx26' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx27' style='display:none'>A rubric is a set of instructions at the beginning of a document, such as an examination or term paper, that is usually printed in a different style so as to highlight its importance.</p>
<p class='rw-defn idx28' style='display:none'>A sagacious person is wise, intelligent, and has the ability to make good practical decisions.</p>
<p class='rw-defn idx29' style='display:none'>Sporadic occurrences happen from time to time but not at constant or regular intervals.</p>
<p class='rw-defn idx30' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx31' style='display:none'>Something that is vacuous is empty or blank, such as a mind or stare.</p>
<p class='rw-defn idx32' style='display:none'>Something that is vaporous is not completely formed but foggy and misty; in the same vein, a vaporous idea is insubstantial and vague.</p>
<p class='rw-defn idx33' style='display:none'>Something that is volatile can change easily and vary widely.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>nebulous</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='nebulous#' id='pronounce-sound' path='audio/words/amy-nebulous'></a>
NEB-yuh-luhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='nebulous#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='nebulous#' id='context-sound' path='audio/wordcontexts/brian-nebulous'></a>
We have a new boss, whose unclear, <em>nebulous</em> title of &#8220;Vice President of Management Affairs&#8221; tells little about what he actually does.  One theory is that he is the nephew of our <span class="caps">CEO</span>, and was given this job with scattered, unlisted, and <em>nebulous</em> duties so that he can sound important while not doing anything demanding.  When he asks what we&#8217;re doing at the office, we give him vague, indefinite, <em>nebulous</em> answers designed to stop further questions.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If your teacher provided a <em>nebulous</em> response to your question, how would you feel?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Confused because they did not give you a clear answer.
</li>
<li class='choice '>
<span class='result'></span>
Insulted because they gave such a simple and basic answer.
</li>
<li class='choice '>
<span class='result'></span>
Interested because they provided new and fascinating information.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='nebulous#' id='definition-sound' path='audio/wordmeanings/amy-nebulous'></a>
If you describe something as <em>nebulous</em>, you mean that it is unclear, vague, and not clearly defined; a shape that is <em>nebulous</em> has no clear boundaries.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>formless</em>
</span>
</span>
</div>
<a class='quick-help' href='nebulous#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='nebulous#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/nebulous/memory_hooks/3553.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Bolus</span></span> Know-How</span></span> So just how am I supposed to get that <span class="emp3"><span>bolus</span></span> down that fire-breathing bull's throat again?  I think that you are being a bit too ne<span class="emp3"><span>bulous</span></span> about the details of the procedure--why don't you do it yourself?     Note: a "<span class="emp3"><span>bolus</span></span>" is a large pill.
</p>
</div>

<div id='memhook-button-bar'>
<a href="nebulous#" id="add-public-hook" style="" url="https://membean.com/mywords/nebulous/memory_hooks">Use other public hook</a>
<a href="nebulous#" id="memhook-use-own" url="https://membean.com/mywords/nebulous/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='nebulous#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Responsible development is critical, but ignoring the need for this technology in the face of <b>nebulous</b> and poorly defined fears would be truly hazardous.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
When set against the grave threat posed by climate change, the green policies favoured by economists can seem convoluted. Carbon prices, beloved of wonks, require governments to estimate the social cost of carbon emissions, a <b>nebulous</b> concept.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Given that “levelling up” remains more of a <b>nebulous</b> concept than a coherent project, calculating a budget for it is more or less impossible.
<cite class='attribution'>
&mdash;
The Guardian
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/nebulous/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='nebulous#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>nebul</td>
<td>
&rarr;
</td>
<td class='meaning'>mist, cloud</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ous_possessing' data-tree-url='//cdn1.membean.com/public/data/treexml' href='nebulous#'>
<span class=''></span>
-ous
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>possessing the nature of</td>
</tr>
</table>
<p>Something <em>nebulous</em> &#8220;possesses the nature of mist or a cloud.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='nebulous#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Deconstructing Henry</strong><span> Henry's physical appearance is becoming somewhat nebulous.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/nebulous.jpg' video_url='examplevids/nebulous' video_width='350'></span>
<div id='wt-container'>
<img alt="Nebulous" height="288" src="https://cdn1.membean.com/video/examplevids/nebulous.jpg" width="350" />
<div class='center'>
<a href="nebulous#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='nebulous#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Nebulous" src="https://cdn3.membean.com/public/images/wordimages/cons2/nebulous.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='nebulous#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>amorphous</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>anarchy</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>anomaly</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>generic</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>inchoate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>incoherent</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>inconclusive</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>irresolute</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>mercurial</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>nondescript</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>oblique</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>opaque</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>sporadic</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>vacuous</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>vaporous</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>volatile</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>astute</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>circumscribe</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>clarion</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>corporeal</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>delineate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>diaphanous</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>elucidate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>exegesis</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>gossamer</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>inference</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>lucid</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>pellucid</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>rubric</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>sagacious</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="nebulous" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>nebulous</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="nebulous#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

