
<!DOCTYPE html>
<html>
<head>
<title>Word: choleric | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Someone who has an abrasive manner is unkind and rude, wearing away at you in an irritating fashion.</p>
<p class='rw-defn idx1' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx2' style='display:none'>Someone who has a bilious personality is highly irritable and bad-tempered.</p>
<p class='rw-defn idx3' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx4' style='display:none'>Someone who has a bristling personality is easily offended, annoyed, or angered.</p>
<p class='rw-defn idx5' style='display:none'>Someone who is cantankerous is bad-tempered and always finds things to complain about.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is churlish is impolite and unfriendly, especially towards another who has done nothing to deserve ill-treatment.</p>
<p class='rw-defn idx7' style='display:none'>Complacent persons are too confident and relaxed because they think that they can deal with a situation easily; however, in many circumstances, this is not the case.</p>
<p class='rw-defn idx8' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx9' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx10' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx11' style='display:none'>If you show exuberance, you display great excitement, energy, and enthusiasm.</p>
<p class='rw-defn idx12' style='display:none'>If you exult, you show great pleasure and excitement, especially about something you have achieved.</p>
<p class='rw-defn idx13' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx14' style='display:none'>If someone demonstrates impudence, they behave in a very rude or disrespectful way.</p>
<p class='rw-defn idx15' style='display:none'>When you are indignant about something, you are angry or really annoyed about it.</p>
<p class='rw-defn idx16' style='display:none'>An inflammable substance or person&#8217;s temper is easily set on fire.</p>
<p class='rw-defn idx17' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx18' style='display:none'>An irascible person becomes angry very easily.</p>
<p class='rw-defn idx19' style='display:none'>If you are lethargic, you are tired, lack energy, and are unwilling to exert effort.</p>
<p class='rw-defn idx20' style='display:none'>When you are listless, you lack energy and interest and are unwilling to exert any effort.</p>
<p class='rw-defn idx21' style='display:none'>If you nettle someone, you irritate or annoy them.</p>
<p class='rw-defn idx22' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx23' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx24' style='display:none'>A petulant person behaves in an unreasonable and childish way, especially because they cannot get their own way or what they want.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is phlegmatic stays calm and unemotional even in dangerous or exciting situations.</p>
<p class='rw-defn idx26' style='display:none'>When you are piqued by something, either your curiosity is aroused by it or you feel resentment or anger towards it.</p>
<p class='rw-defn idx27' style='display:none'>A puerile person is childish, immature, and foolish.</p>
<p class='rw-defn idx28' style='display:none'>Someone who is querulous often complains about things in an annoying way.</p>
<p class='rw-defn idx29' style='display:none'>If you are sanguine about a situation, especially a difficult one, you are confident and cheerful that everything will work out the way you want it to.</p>
<p class='rw-defn idx30' style='display:none'>If you are sedate, you are calm, unhurried, and unlikely to be disturbed by anything.</p>
<p class='rw-defn idx31' style='display:none'>If you are stolid, you have or show little emotion about anything at all.</p>
<p class='rw-defn idx32' style='display:none'>If you are temperamental, you tend to become easily upset and experience unpredictable mood swings.</p>
<p class='rw-defn idx33' style='display:none'>If your body is affected by torpor, you are severely lacking in energy; therefore, you are idle—and can even be numb.</p>
<p class='rw-defn idx34' style='display:none'>A troglodyte is reclusive, severely lacking in social skills, and is out of step with current times.</p>
<p class='rw-defn idx35' style='display:none'>When you take umbrage, you take offense at what another has done.</p>
<p class='rw-defn idx36' style='display:none'>A virulent disease is very dangerous and spreads very quickly.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>choleric</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='choleric#' id='pronounce-sound' path='audio/words/amy-choleric'></a>
KOL-er-ik
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='choleric#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='choleric#' id='context-sound' path='audio/wordcontexts/brian-choleric'></a>
In a quick-tempered, <em>choleric</em> fit of anger, the young man threw the book down on the table and stomped out of the room.  His classmates remained sitting, shocked and stunned by his irritable and <em>choleric</em> display.  His teacher followed the touchy, <em>choleric</em> fellow and asked him why he had been so easily annoyed.  The student could not explain his sudden offended and <em>choleric</em> outburst but did offer an apology for his inexcusable behavior.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Who would likely be considered <em>choleric</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A co-worker who is frequently sick.
</li>
<li class='choice '>
<span class='result'></span>
A child who frequently disobeys their parents.
</li>
<li class='choice answer '>
<span class='result'></span>
A colleague who has frequent angry outbursts.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='choleric#' id='definition-sound' path='audio/wordmeanings/amy-choleric'></a>
A <em>choleric</em> person becomes angry very easily.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>irritable</em>
</span>
</span>
</div>
<a class='quick-help' href='choleric#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='choleric#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/choleric/memory_hooks/5657.json'></span>
<p>
<span class="emp0"><span>C<span class="emp3"><span>hol</span></span><span class="emp2"><span>eric</span></span> <span class="emp2"><span>Eric</span></span> Falls Down <span class="emp3"><span>Hol</span></span>e</span></span>  "There!  Now you really have something to be c<span class="emp3"><span>hol</span></span><span class="emp2"><span>eric</span></span> about, irritable <span class="emp2"><span>Eric</span></span>!  Maybe that snake-infested <span class="emp3"><span>hol</span></span>e into which you have fallen will teach you what really to be c<span class="emp3"><span>hol</span></span><span class="emp2"><span>eric</span></span> about!"
</p>
</div>

<div id='memhook-button-bar'>
<a href="choleric#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/choleric/memory_hooks">Use other hook</a>
<a href="choleric#" id="memhook-use-own" url="https://membean.com/mywords/choleric/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='choleric#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
As a political figure, Hamilton was volatile, mercurial, <b>choleric</b>, vindictive, conniving, disloyal, and incontinent; those personal flaws eventually led to his death in a duel with Aaron Burr.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
Before blossoming into perhaps the best passing quarterback ever to play for Schembechler, Harbaugh was briefly dismissed from the team by his famously <b>choleric</b> coach.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
When she was eight, her father, the profligate and <b>choleric</b> squire of Blickling Hall in Norfolk, challenged a neighbor to a duel for impugning his valor.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Four centuries later, the Greek physician Galen posited that our personalities are an expression of the mix of the four humors, with an excess of any one making us sanguine, phlegmatic, melancholic, or <b>choleric</b>.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/choleric/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='choleric#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='chol_bile' data-tree-url='//cdn1.membean.com/public/data/treexml' href='choleric#'>
<span class=''></span>
chol
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>bile, gall</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ic_like' data-tree-url='//cdn1.membean.com/public/data/treexml' href='choleric#'>
<span class=''></span>
-ic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>nature of, like</td>
</tr>
</table>
<p>According to the medieval theory of the humors, an excess of <em>choler</em>, or &#8220;yellow bile,&#8221; made one &#8220;angry&#8221; or &#8220;irritable.&#8221; Medieval medicine taught that the body possessed four fluids or humors: black bile, yellow bile (choler), blood, and phlegm; the relative concentrations of these four humors, different for each person, determined their mood, health, and general disposition.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='choleric#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>The Avengers</strong><span> Dr. Banner's choleric personality does have its advantages when danger is near.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/choleric.jpg' video_url='examplevids/choleric' video_width='350'></span>
<div id='wt-container'>
<img alt="Choleric" height="288" src="https://cdn1.membean.com/video/examplevids/choleric.jpg" width="350" />
<div class='center'>
<a href="choleric#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='choleric#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Choleric" src="https://cdn3.membean.com/public/images/wordimages/cons2/choleric.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='choleric#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abrasive</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bilious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>bristling</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>cantankerous</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>churlish</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>impudence</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>indignant</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>inflammable</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>irascible</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>nettle</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>petulant</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>pique</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>puerile</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>querulous</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>temperamental</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>troglodyte</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>umbrage</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>virulent</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>complacent</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>exuberance</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>exult</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>lethargic</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>listless</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>phlegmatic</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>sanguine</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>sedate</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>stolid</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>torpor</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="choleric" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>choleric</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="choleric#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

