
<!DOCTYPE html>
<html>
<head>
<title>Word: camaraderie | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Camaraderie-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/camaraderie-large.jpg?qdep8" />
</div>
<a href="camaraderie#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx1' style='display:none'>Amity is a peaceful friendship between two parties, particularly between two countries.</p>
<p class='rw-defn idx2' style='display:none'>Badinage is lighthearted conversation that involves teasing, jokes, and humor.</p>
<p class='rw-defn idx3' style='display:none'>When you bandy about ideas with someone, you casually discuss them without caring if the discussion is informed or effective in any way.</p>
<p class='rw-defn idx4' style='display:none'>Banter is friendly conversation during which people make friendly jokes and laugh at—and with—one another.</p>
<p class='rw-defn idx5' style='display:none'>Bigotry is the expression of strong and unreasonable opinions without accepting or tolerating opposing views.</p>
<p class='rw-defn idx6' style='display:none'>Someone who has a bilious personality is highly irritable and bad-tempered.</p>
<p class='rw-defn idx7' style='display:none'>A bohemian is someone who lives outside mainstream society, often embracing an unconventional lifestyle of self-expression, creativity, and rebellion against conventional rules and ways of society.</p>
<p class='rw-defn idx8' style='display:none'>If you describe a person&#8217;s behavior or speech as brusque, you mean that they say or do things abruptly or too quickly in a somewhat rude and impolite way.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is cantankerous is bad-tempered and always finds things to complain about.</p>
<p class='rw-defn idx10' style='display:none'>A cavalier person does not seem to care about rules, principles, or other people&#8217;s feelings, no matter how dire or serious a situation may be.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is churlish is impolite and unfriendly, especially towards another who has done nothing to deserve ill-treatment.</p>
<p class='rw-defn idx12' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx13' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx14' style='display:none'>A convivial atmosphere or occasion is friendly, pleasant, cheerful, and relaxed.</p>
<p class='rw-defn idx15' style='display:none'>A man who is debonair wears fashionable clothes and is sophisticated, charming, friendly, and confident.</p>
<p class='rw-defn idx16' style='display:none'>If you disparage someone or something, you say unpleasant words that show you have no respect for that person or thing.</p>
<p class='rw-defn idx17' style='display:none'>Someone who is dour is serious, stubborn, and unfriendly; they can also be gloomy.</p>
<p class='rw-defn idx18' style='display:none'>If someone is eccentric, they behave in a strange and unusual way that is different from most people.</p>
<p class='rw-defn idx19' style='display:none'>Exclusion is the act of shutting someone out of a group or an activity.</p>
<p class='rw-defn idx20' style='display:none'>A forlorn person is lonely because they have been abandoned; a forlorn home has been deserted.</p>
<p class='rw-defn idx21' style='display:none'>A gregarious person is friendly, highly social, and prefers being with people rather than being alone.</p>
<p class='rw-defn idx22' style='display:none'>If someone is inimical, they are unfriendly and will likely cause you harm.</p>
<p class='rw-defn idx23' style='display:none'>A malevolent person or thing is evil due to deliberate attempts to cause harm.</p>
<p class='rw-defn idx24' style='display:none'>Malice is a type of hatred that causes a strong desire to harm others both physically and emotionally.</p>
<p class='rw-defn idx25' style='display:none'>A misanthrope is someone who hates and mistrusts people.</p>
<p class='rw-defn idx26' style='display:none'>If two people have established a good rapport, their connection is such that they have a good understanding of and can communicate well with one other.</p>
<p class='rw-defn idx27' style='display:none'>A recluse is someone who chooses to live alone and deliberately avoids other people.</p>
<p class='rw-defn idx28' style='display:none'>Strife is struggle or conflict that sometimes turns violent.</p>
<p class='rw-defn idx29' style='display:none'>A troglodyte is reclusive, severely lacking in social skills, and is out of step with current times.</p>
<p class='rw-defn idx30' style='display:none'>A truculent person is bad-tempered, easily annoyed, and prone to arguing excessively.</p>
<p class='rw-defn idx31' style='display:none'>To act in an uncouth manner is to be awkward and unmannerly.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>camaraderie</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='camaraderie#' id='pronounce-sound' path='audio/words/amy-camaraderie'></a>
kah-muh-RAH-duh-ree
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='camaraderie#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='camaraderie#' id='context-sound' path='audio/wordcontexts/brian-camaraderie'></a>
The group of scientists had worked together at the lab for years, and through their collaboration a feeling of friendship, respect, and casual <em>camaraderie</em> had grown between them.  They often played harmless jokes on one another; their <em>camaraderie</em> and trustful fellowship was clear to everyone from the sound of laughter pouring out of the conference room.  Such kind <em>camaraderie</em> aided the companions&#8217; ability to work together cheerfully and sociably.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How can someone establish <em>camaraderie</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
By ensuring that everyone in a group knows who is in charge.
</li>
<li class='choice '>
<span class='result'></span>
By creating chaos and uncertainty within a group of people.
</li>
<li class='choice answer '>
<span class='result'></span>
By building meaningful relationships within a group of people.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='camaraderie#' id='definition-sound' path='audio/wordmeanings/amy-camaraderie'></a>
<em>Camaraderie</em> is a feeling of friendship and trust among a group of people who have usually known each over a long period of time.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>friendship</em>
</span>
</span>
</div>
<a class='quick-help' href='camaraderie#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='camaraderie#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/camaraderie/memory_hooks/3909.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Dearie</span></span>, May I Borrow the <span class="emp3"><span>Camera</span></span>?</span></span>  The sense of <span class="emp3"><span>camara</span></span><span class="emp1"><span>derie</span></span> in the group of old women was so great that they only needed one <span class="emp3"><span>camera</span></span> amongst them all;  you would often hear one asking with great <span class="emp3"><span>camara</span></span><span class="emp1"><span>derie</span></span>, "<span class="emp1"><span>Dearie</span></span>, may I use the <span class="emp3"><span>camera</span></span> when you're finished with it?"
</p>
</div>

<div id='memhook-button-bar'>
<a href="camaraderie#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/camaraderie/memory_hooks">Use other hook</a>
<a href="camaraderie#" id="memhook-use-own" url="https://membean.com/mywords/camaraderie/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='camaraderie#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Because people tended to cluster on the sun deck in good weather and in the lounge at night, there were natural opportunities for passengers to mingle, resulting in a pleasant <b>camaraderie</b>.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Writers tend to write in isolation, and the sense of community and <b>camaraderie</b> that this [Writers Guild award ceremony] has produced marks a new period for the guild.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/camaraderie/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='camaraderie#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>camer</td>
<td>
&rarr;
</td>
<td class='meaning'>room, chamber</td>
</tr>
</table>
<p>The idea behind <em>camaraderie</em> is that of friends living or spending a great deal of time together in the same &#8220;room or chamber.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='camaraderie#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Carrier: Squared Away</strong><span> Camaraderie between military shipmates is inevitable.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/camaraderie.jpg' video_url='examplevids/camaraderie' video_width='350'></span>
<div id='wt-container'>
<img alt="Camaraderie" height="288" src="https://cdn1.membean.com/video/examplevids/camaraderie.jpg" width="350" />
<div class='center'>
<a href="camaraderie#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='camaraderie#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Camaraderie" src="https://cdn0.membean.com/public/images/wordimages/cons2/camaraderie.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='camaraderie#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>amity</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>badinage</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bandy</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>banter</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>convivial</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>debonair</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>gregarious</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>rapport</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='5' class = 'rw-wordform notlearned'><span>bigotry</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>bilious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>bohemian</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>brusque</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>cantankerous</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>cavalier</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>churlish</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>disparage</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>dour</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>eccentric</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>exclusion</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>forlorn</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>inimical</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>malevolent</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>malice</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>misanthrope</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>recluse</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>strife</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>troglodyte</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>truculent</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>uncouth</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="camaraderie" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>camaraderie</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="camaraderie#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

