
<!DOCTYPE html>
<html>
<head>
<title>Word: placid | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Placid-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/placid-large.jpg?qdep8" />
</div>
<a href="placid#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>A state of anarchy occurs when there is no organization or order in a nation or country, especially when no effective government exists.</p>
<p class='rw-defn idx1' style='display:none'>Bedlam is a situation with a great deal of noise and confusion.</p>
<p class='rw-defn idx2' style='display:none'>Someone who has a bilious personality is highly irritable and bad-tempered.</p>
<p class='rw-defn idx3' style='display:none'>Someone who is boisterous is noisy, excitable, and full of boundless energy; therefore, they show a lack of disciplined restraint at times.</p>
<p class='rw-defn idx4' style='display:none'>Someone who has a bristling personality is easily offended, annoyed, or angered.</p>
<p class='rw-defn idx5' style='display:none'>Mass carnage is the massacre or slaughter of many people at one time, usually in battle or from an unusually intense natural disaster.</p>
<p class='rw-defn idx6' style='display:none'>A cataclysm is a violent, sudden event that causes great change and/or harm.</p>
<p class='rw-defn idx7' style='display:none'>A chaotic state of affairs is in a state of confusion and complete disorder.</p>
<p class='rw-defn idx8' style='display:none'>Consternation is the feeling of anxiety or fear, sometimes paralyzing in its effect, and often caused by something unexpected that has happened.</p>
<p class='rw-defn idx9' style='display:none'>Depredation can be the act of destroying or robbing a village—or the overall damage that time can do to things held dear.</p>
<p class='rw-defn idx10' style='display:none'>When you are in a state of disarray, you are disorganized, disordered, and in a state of confusion.</p>
<p class='rw-defn idx11' style='display:none'>When you are discombobulated, you are confused and upset because you have been thrown into a situation that you cannot temporarily handle.</p>
<p class='rw-defn idx12' style='display:none'>When something is dormant, it is in a state of sleep or temporarily not active.</p>
<p class='rw-defn idx13' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx14' style='display:none'>To foment is to encourage people to protest, fight, or cause trouble and violent opposition to something that is viewed by some as undesirable.</p>
<p class='rw-defn idx15' style='display:none'>A fracas is a rough and noisy fight or loud argument that can involve multiple people.</p>
<p class='rw-defn idx16' style='display:none'>Frenetic activity is done quickly with lots of energy but is also uncontrolled and disorganized; someone who is in a huge hurry often displays this type of behavior.</p>
<p class='rw-defn idx17' style='display:none'>A halcyon time is calm, peaceful, and undisturbed.</p>
<p class='rw-defn idx18' style='display:none'>When two people are in a harmonious state, they are in agreement with each other; when a sound is harmonious, it is pleasant or agreeable to the ear.</p>
<p class='rw-defn idx19' style='display:none'>A harrowing experience is highly distressing, terrifying, or very disturbing.</p>
<p class='rw-defn idx20' style='display:none'>When there is havoc, there is great disorder, widespread destruction, and much confusion.</p>
<p class='rw-defn idx21' style='display:none'>An impending event is approaching fast or is about to occur; this word usually has a negative implication, referring to something threatening or harmful coming.</p>
<p class='rw-defn idx22' style='display:none'>If someone is imperturbable, they are always calm and not easily upset or disturbed by any situation, even dangerous ones.</p>
<p class='rw-defn idx23' style='display:none'>An incendiary device causes objects to catch on fire; incendiary comments can cause riots to flare up.</p>
<p class='rw-defn idx24' style='display:none'>An incursion is an unpleasant intrusion, such as a sudden hostile attack or a land being flooded.</p>
<p class='rw-defn idx25' style='display:none'>An insurrection is a rebellion or open uprising against an established form of government.</p>
<p class='rw-defn idx26' style='display:none'>An irascible person becomes angry very easily.</p>
<p class='rw-defn idx27' style='display:none'>A maelstrom is either a large whirlpool in the sea or a violent or agitated state of affairs.</p>
<p class='rw-defn idx28' style='display:none'>A melee is a noisy, confusing, hand-to-hand fight involving a group of people.</p>
<p class='rw-defn idx29' style='display:none'>Pandemonium is a very noisy and uncontrolled situation, especially one that is caused by a lot of angry or excited people.</p>
<p class='rw-defn idx30' style='display:none'>A paroxysm is a sudden uncontrolled expression of emotion or a short attack of pain, coughing, or shaking.</p>
<p class='rw-defn idx31' style='display:none'>If soldiers pillage a place, such as a town or museum, they steal a lot of things and damage it using violent methods.</p>
<p class='rw-defn idx32' style='display:none'>When you are piqued by something, either your curiosity is aroused by it or you feel resentment or anger towards it.</p>
<p class='rw-defn idx33' style='display:none'>When you plunder, you forcefully take or rob others of their possessions, usually during times of war, natural disaster, or other unrest.</p>
<p class='rw-defn idx34' style='display:none'>A state of quiescence is one of quiet and restful inaction.</p>
<p class='rw-defn idx35' style='display:none'>To ransack a place is to thoroughly loot or rob items from it; it can also mean to look through something thoroughly by examining every bit of it.</p>
<p class='rw-defn idx36' style='display:none'>When you ravage something, you completely destroy, wreck, or damage it.</p>
<p class='rw-defn idx37' style='display:none'>If you are in a state of repose, your mind is at peace or your body is at rest.</p>
<p class='rw-defn idx38' style='display:none'>If you are sedate, you are calm, unhurried, and unlikely to be disturbed by anything.</p>
<p class='rw-defn idx39' style='display:none'>Sedition is the act of encouraging people to disobey and oppose the government currently in power.</p>
<p class='rw-defn idx40' style='display:none'>A serene place or situation is peaceful and calm.</p>
<p class='rw-defn idx41' style='display:none'>If someone is squeamish, they are easily nauseated or shocked by things that are tolerated by most people; they can also be oversensitive.</p>
<p class='rw-defn idx42' style='display:none'>If you are stolid, you have or show little emotion about anything at all.</p>
<p class='rw-defn idx43' style='display:none'>If you are temperamental, you tend to become easily upset and experience unpredictable mood swings.</p>
<p class='rw-defn idx44' style='display:none'>A tempestuous storm, temper, or crowd is violent, wild, and in an uproar.</p>
<p class='rw-defn idx45' style='display:none'>If something is tranquil, it is peaceful, calm, and quiet.</p>
<p class='rw-defn idx46' style='display:none'>Trepidation is fear or uneasiness about something that is going to happen.</p>
<p class='rw-defn idx47' style='display:none'>A tumultuous event or period of time is filled with great excitement, confusion, or violence; a tumultuous reaction to something is likewise very loud and noisy because people are happy and excited.</p>
<p class='rw-defn idx48' style='display:none'>When you experience turmoil, there is great confusion, disturbance, instability, and disorder in your life.</p>
<p class='rw-defn idx49' style='display:none'>When you are unflappable, you remain calm, cool, and collected in even the most trying of situations.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>placid</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='placid#' id='pronounce-sound' path='audio/words/amy-placid'></a>
PLAS-id
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='placid#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='placid#' id='context-sound' path='audio/wordcontexts/brian-placid'></a>
I couldn&#8217;t believe it one day when my 8th-grade students were so <em>placid</em> or calm&#8212;nothing was able to bother them at all.  They were <em>placid</em> and undisturbed by any other social concerns, which allowed them to truly focus on the material at hand.  Due to their <em>placid</em> or quiet mental state, we were able to learn twenty <span class="caps">SAT</span> words perfectly by the end of the class period!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How might a <em>placid</em> person act in an emergency?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They&#8217;d panic and try to quickly get away from the situation.
</li>
<li class='choice '>
<span class='result'></span>
They&#8217;d yell orders at everyone—despite having little experience.
</li>
<li class='choice answer '>
<span class='result'></span>
They&#8217;d stay calm and quietly try to help fix the situation. 
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='placid#' id='definition-sound' path='audio/wordmeanings/amy-placid'></a>
A <em>placid</em> scene or person is calm, quiet, and undisturbed.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>quiet</em>
</span>
</span>
</div>
<a class='quick-help' href='placid#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='placid#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/placid/memory_hooks/6247.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Pl</span></span>ace an Ant<span class="emp3"><span>acid</span></span></span></span> Your tummy hurts?  Well, then, <span class="emp1"><span>pl</span></span>ace an ant<span class="emp3"><span>acid</span></span> like Tums in it, and that ant<span class="emp3"><span>acid</span></span> will make your tummy <span class="emp1"><span>pl</span></span><span class="emp3"><span>acid</span></span>, calming it right down.
</p>
</div>

<div id='memhook-button-bar'>
<a href="placid#" id="add-public-hook" style="" url="https://membean.com/mywords/placid/memory_hooks">Use other public hook</a>
<a href="placid#" id="memhook-use-own" url="https://membean.com/mywords/placid/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='placid#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
If American politics were as <b>placid</b> as the Rio Grande at sunset, he'd have a much harder time finding things to skewer on his show.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
But beneath its <b>placid</b> surface are the craggy scars of middle age, when holding onto and cherishing love is a lot more difficult than finding it.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
<li>
Christian Bale has been effective in some films, but he’s a <b>placid</b> Bruce Wayne, a swank gent in Armani suits, with every hair in place.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
The moderator made good-natured quips about the possibility of having to restrain these two <b>placid</b> looking men, each tucked respectably into a suit.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/placid/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='placid#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='plac_please' data-tree-url='//cdn1.membean.com/public/data/treexml' href='placid#'>
<span class=''></span>
plac
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>please, calm, soothe</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='id_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='placid#'>
<span class=''></span>
-id
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>A <em>placid</em> scene &#8220;pleases&#8221; one by being &#8220;soothing or calming.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='placid#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: No Reaction Man</strong><span> Now this guy is placid when it really counts!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/placid.jpg' video_url='examplevids/placid' video_width='350'></span>
<div id='wt-container'>
<img alt="Placid" height="288" src="https://cdn1.membean.com/video/examplevids/placid.jpg" width="350" />
<div class='center'>
<a href="placid#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='placid#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Placid" src="https://cdn1.membean.com/public/images/wordimages/cons2/placid.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='placid#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='12' class = 'rw-wordform notlearned'><span>dormant</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>halcyon</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>harmonious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>imperturbable</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>quiescence</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>repose</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>sedate</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>serene</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>stolid</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>tranquil</span> &middot;</li><li data-idx='49' class = 'rw-wordform notlearned'><span>unflappable</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>anarchy</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>bedlam</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bilious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>boisterous</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>bristling</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>carnage</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>cataclysm</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>chaotic</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>consternation</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>depredation</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>disarray</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>discombobulated</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>foment</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>fracas</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>frenetic</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>harrowing</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>havoc</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>impending</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>incendiary</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>incursion</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>insurrection</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>irascible</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>maelstrom</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>melee</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>pandemonium</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>paroxysm</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>pillage</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>pique</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>plunder</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>ransack</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>ravage</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>sedition</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>squeamish</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>temperamental</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>tempestuous</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>trepidation</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>tumultuous</span> &middot;</li><li data-idx='48' class = 'rw-wordform notlearned'><span>turmoil</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="placid" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>placid</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="placid#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

