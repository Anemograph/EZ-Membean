
<!DOCTYPE html>
<html>
<head>
<title>Word: detritus | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Detritus-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/detritus-large.jpg?qdep8" />
</div>
<a href="detritus#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>The process of ablation is the removal of diseased organs or harmful substances from the body, often through surgical procedure.</p>
<p class='rw-defn idx1' style='display:none'>Something that happens unexpectedly and by chance that is not inherent or natural to a given item or situation is adventitious, such as a root appearing in an unusual place on a plant.</p>
<p class='rw-defn idx2' style='display:none'>A cardinal rule or quality is considered to be the most important or basic in a set of rules or qualities.</p>
<p class='rw-defn idx3' style='display:none'>Chaff is the husks of grain separated from the edible plant; it can also be useless matter in general that is cast aside.</p>
<p class='rw-defn idx4' style='display:none'>The crux of a problem or argument is the most important or difficult part of it that affects everything else.</p>
<p class='rw-defn idx5' style='display:none'>Something described as dross is of very poor quality and has little or no value.</p>
<p class='rw-defn idx6' style='display:none'>An exigent situation is urgent and demands a lot of attention, often in ways that are expensive in time, cost, and effort.</p>
<p class='rw-defn idx7' style='display:none'>Something that is pivotal is more important than anything else in a situation or a system; consequently, it is the key to its success.</p>
<p class='rw-defn idx8' style='display:none'>The remnants of something are the small parts of it that are left after the main part has been used or destroyed.</p>
<p class='rw-defn idx9' style='display:none'>Something that is residual is the part that still stays or remains after the main part is taken away.</p>
<p class='rw-defn idx10' style='display:none'>Rubble is the debris, remains, or wreckage of something that has been ruined, such as a building or other structure.</p>
<p class='rw-defn idx11' style='display:none'>A seminal article, book, piece of music or other work has many new ideas; additionally, it has great influence on generating ideas for future work.</p>
<p class='rw-defn idx12' style='display:none'>Shards are sharp pieces of broken glass, pottery, metal, or other hard substances.</p>
<p class='rw-defn idx13' style='display:none'>Something that is superfluous is unnecessary; it is more than what is wanted or needed at the current time.</p>
<p class='rw-defn idx14' style='display:none'>If you refer to something as a trifle, you mean that it is of little or no importance or value.</p>
<p class='rw-defn idx15' style='display:none'>A vestige of something is a very small part that remains from a once larger whole.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>detritus</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='detritus#' id='pronounce-sound' path='audio/words/amy-detritus'></a>
di-TRAHY-tuhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='detritus#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='detritus#' id='context-sound' path='audio/wordcontexts/brian-detritus'></a>
After the devastating storm, Lenard&#8217;s Joke Shop was completely wiped away, leaving only small broken pieces or <em>detritus</em> of what it had once been.  As Lenard looked at the <em>detritus</em>, including scraps from the roof and a random, plastic clown nose, he grieved for the loss of his business.  Friends arrived to help Lenard sort through the rubble or <em>detritus</em> for anything that might be saved.  Unfortunately, there was nothing that could be kept for a new store from the surviving fragments or <em>detritus</em>.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is <em>detritus</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A collection of broken merchandise that you take to the store to be repaired.
</li>
<li class='choice answer '>
<span class='result'></span>
Remaining bits of a structure after its destruction.
</li>
<li class='choice '>
<span class='result'></span>
A feeling of sadness after losing property to sudden disaster.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='detritus#' id='definition-sound' path='audio/wordmeanings/amy-detritus'></a>
<em>Detritus</em> is the small pieces of waste that remain after something has been destroyed or used.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>debris</em>
</span>
</span>
</div>
<a class='quick-help' href='detritus#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='detritus#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/detritus/memory_hooks/4982.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Ri</span></span>gh<span class="emp1"><span>t</span></span> for <span class="emp3"><span>Us</span></span> To Collect the <span class="emp2"><span>De</span></span>b<span class="emp2"><span>t</span></span>?</span></span> The last <span class="emp2"><span>de</span></span>b<span class="emp2"><span>t</span></span>s of the poor are nothing but leavings and <span class="emp2"><span>det</span></span><span class="emp1"><span>rit</span></span><span class="emp3"><span>us</span></span> after all the money we've made from the rich, so is it <span class="emp1"><span>ri</span></span>gh<span class="emp1"><span>t</span></span> for <span class="emp3"><span>us</span></span> to collect that <span class="emp2"><span>de</span></span>b<span class="emp2"><span>t</span></span> since we're so wealthy anyway?
</p>
</div>

<div id='memhook-button-bar'>
<a href="detritus#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/detritus/memory_hooks">Use other hook</a>
<a href="detritus#" id="memhook-use-own" url="https://membean.com/mywords/detritus/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='detritus#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The roar of cars was dampened by a delicious rush of running water carried by a _joob_, one of the deep gutters that line every Tehran street. _Joobs_ cleanse the city with melt water from Mount Tochal and carry the <b>detritus</b> of the wealthier northern suburbs to the poorer districts at the bottom of the hill.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Marine scientists have long known about the ''rain of <b>detritus</b>,'' or ''marine snow,'' but have thought that it was composed only of dying organisms, their wastes and bacteria responsible for decomposition.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
The room went pitch-black and then arose deafening sounds of power drills and heavy trucks, followed by a photo montage of dust storms over Beijing, factory <b>detritus</b> in the Huai River and other environmental filth.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/detritus/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='detritus#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='de_off' data-tree-url='//cdn1.membean.com/public/data/treexml' href='detritus#'>
<span class='common'></span>
de-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>down, off, from</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='trit_rubbed' data-tree-url='//cdn1.membean.com/public/data/treexml' href='detritus#'>
<span class=''></span>
trit
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>rubbed, worn away</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='us_noun' data-tree-url='//cdn1.membean.com/public/data/treexml' href='detritus#'>
<span class=''></span>
-us
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>forms a Latin noun</td>
</tr>
</table>
<p><em>Detritus</em> is that which has been &#8220;worn away&#8221; or &#8220;rubbed off&#8221; from something else and then discarded or abandoned.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='detritus#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Detritus" src="https://cdn3.membean.com/public/images/wordimages/cons2/detritus.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='detritus#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>ablation</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>adventitious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>chaff</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>dross</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>remnant</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>residual</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>rubble</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>shard</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>superfluous</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>trifle</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>vestige</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>cardinal</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>crux</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>exigent</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>pivotal</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>seminal</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="detritus" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>detritus</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="detritus#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

