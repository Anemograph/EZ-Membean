
<!DOCTYPE html>
<html>
<head>
<title>Word: reverent | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abominate something, you hate it because you think it is extremely wrong and unacceptable.</p>
<p class='rw-defn idx1' style='display:none'>An apostate has abandoned their religious faith, political party, or devotional cause.</p>
<p class='rw-defn idx2' style='display:none'>If you besmirch someone, you spoil their good reputation by saying bad things about them, usually things that you know are not true.</p>
<p class='rw-defn idx3' style='display:none'>When the Catholic Church canonizes someone, they are officially declared a saint; this word also refers to placing a work of art or literature in an accepted group of the best of its kind.</p>
<p class='rw-defn idx4' style='display:none'>If you behave with deference towards someone, you show them respect and accept their opinions or decisions, especially because they have an important position.</p>
<p class='rw-defn idx5' style='display:none'>To defile something holy or important is to do something to it or say something about it that is highly offensive to others.</p>
<p class='rw-defn idx6' style='display:none'>If someone is deified, they have been either made into a god or are adored like one.</p>
<p class='rw-defn idx7' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx8' style='display:none'>If you denounce people or actions, you criticize them severely in public because you feel strongly that they are wrong or evil.</p>
<p class='rw-defn idx9' style='display:none'>If you desecrate something that is considered holy or very special, you deliberately spoil or damage it.</p>
<p class='rw-defn idx10' style='display:none'>The adjective ecclesiastical refers to a priest or member of the clergy in the Christian Church; this word also refers to something associated with a church as an organized institution.</p>
<p class='rw-defn idx11' style='display:none'>When you execrate someone, you curse them to show your intense dislike or hatred of them.</p>
<p class='rw-defn idx12' style='display:none'>When you genuflect, you show a lot more respect to something or someone than is usual or sometimes necessary.</p>
<p class='rw-defn idx13' style='display:none'>Something that is hallowed is respected and admired, usually because it is holy or important in some way.</p>
<p class='rw-defn idx14' style='display:none'>When you pay homage to another person, you show them great admiration or respect; you might even worship them.</p>
<p class='rw-defn idx15' style='display:none'>If you believe someone&#8217;s behavior exhibits idolatry, you think they blindly admire someone or something too much.</p>
<p class='rw-defn idx16' style='display:none'>If you hurl invective at another person, you are verbally abusing or highly criticizing them.</p>
<p class='rw-defn idx17' style='display:none'>A liturgy is a set of rules or formal procedures used during a church service.</p>
<p class='rw-defn idx18' style='display:none'>Obeisance is respect and obedience shown to someone or something, expressed by bowing or some other humble gesture.</p>
<p class='rw-defn idx19' style='display:none'>Piety is being devoted or showing loyalty towards something, especially a religion.</p>
<p class='rw-defn idx20' style='display:none'>If soldiers pillage a place, such as a town or museum, they steal a lot of things and damage it using violent methods.</p>
<p class='rw-defn idx21' style='display:none'>When you plunder, you forcefully take or rob others of their possessions, usually during times of war, natural disaster, or other unrest.</p>
<p class='rw-defn idx22' style='display:none'>When you ravage something, you completely destroy, wreck, or damage it.</p>
<p class='rw-defn idx23' style='display:none'>If something is reviled, it is intensely hated and criticized.</p>
<p class='rw-defn idx24' style='display:none'>A sacrilegious act is one of deep disrespect that violates something that is sacred or holy.</p>
<p class='rw-defn idx25' style='display:none'>Something that is sacrosanct is considered to be so important, special, or holy that no one is allowed to criticize, tamper with, or change it in any way.</p>
<p class='rw-defn idx26' style='display:none'>Venerable people command respect because they are old and wise.</p>
<p class='rw-defn idx27' style='display:none'>If you vilify people, you write or say bad things about them in an attempt to make them unpopular.</p>
<p class='rw-defn idx28' style='display:none'>Vituperative remarks are full of hate, anger, and cruel criticism.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>reverent</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='reverent#' id='pronounce-sound' path='audio/words/amy-reverent'></a>
REV-er-uhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='reverent#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='reverent#' id='context-sound' path='audio/wordcontexts/brian-reverent'></a>
When I walked into the old Gothic cathedral, I gazed up in <em>reverent</em> admiration or awe at the beauty of the ceiling&#8217;s architecture and stained-glass windows.  As I continued to look around with a <em>reverent</em> gaze or respectful attitude, the silence of that holy and wondrous place began to fill my soul.  When I stood <em>reverently</em> or humbly in front of the container that held the holy relic around which the massive church had been built, I mouthed words of worship that had never come from my mouth before.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When would someone speak in a <em>reverent</em> tone of voice?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
When they feel great respect for what they are talking about.
</li>
<li class='choice '>
<span class='result'></span>
When they are confident about the subject of which they are speaking.
</li>
<li class='choice '>
<span class='result'></span>
When they fear that they might disturb others by talking loudly.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='reverent#' id='definition-sound' path='audio/wordmeanings/amy-reverent'></a>
When you are <em>reverent</em>, you show a great deal of respect, admiration, or even awe for someone or something.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>respectful</em>
</span>
</span>
</div>
<a class='quick-help' href='reverent#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='reverent#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/reverent/memory_hooks/4444.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Ver</span></span>y Sil<span class="emp3"><span>ent</span></span></span></span> The crowd was re<span class="emp1"><span>ver</span></span><span class="emp3"><span>ent</span></span> or <span class="emp1"><span>ver</span></span>y sil<span class="emp3"><span>ent</span></span> as the image of the god passed by, many bowing their heads and praying.
</p>
</div>

<div id='memhook-button-bar'>
<a href="reverent#" id="add-public-hook" style="" url="https://membean.com/mywords/reverent/memory_hooks">Use other public hook</a>
<a href="reverent#" id="memhook-use-own" url="https://membean.com/mywords/reverent/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='reverent#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
There is a remarkable respect shown to the poets, however, and the din abates to a <b>reverent</b> silence as the readings begin—unless, as is frequently the case, the subject matter encourages otherwise.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
I can do no other than be <b>reverent</b> before everything that is called life. I can do no other than to have compassion for all that is called life. That is the beginning and the foundation of all ethics.
<cite class='attribution'>
&mdash;
Albert Schweitzer, Alsatian-German philosopher, theologian, and winner of the Nobel laureate
</cite>
</li>
<li>
The faint vibrations of a violin stirred the <b>reverent</b> hush of the landscape in the blended light of the setting sun and the "hunter’s moon."
<cite class='attribution'>
&mdash;
Mary Noailles Murfree, American writer, from _The Christmas Miracle_
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/reverent/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='reverent#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='re_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='reverent#'>
<span class=''></span>
re-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ver_fear' data-tree-url='//cdn1.membean.com/public/data/treexml' href='reverent#'>
<span class=''></span>
ver
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>fear, feel awe</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ent_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='reverent#'>
<span class=''></span>
-ent
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p>A <em>reverent</em> person &#8220;thoroughly fears or feels awe&#8221; towards someone who is highly respected or has great social stature.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='reverent#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Wayne's World</strong><span> Wayne and Garth feel and show a great deal of reverence for the rock band Aerosmith.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/reverent.jpg' video_url='examplevids/reverent' video_width='350'></span>
<div id='wt-container'>
<img alt="Reverent" height="288" src="https://cdn1.membean.com/video/examplevids/reverent.jpg" width="350" />
<div class='center'>
<a href="reverent#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='reverent#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Reverent" src="https://cdn1.membean.com/public/images/wordimages/cons2/reverent.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='reverent#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>canonize</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>deference</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>deify</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>ecclesiastical</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>genuflect</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>hallowed</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>homage</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>idolatry</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>liturgy</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>obeisance</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>piety</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>sacrosanct</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>venerable</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abominate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>apostate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>besmirch</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>defile</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>denounce</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>desecrate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>execrate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>invective</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>pillage</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>plunder</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>ravage</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>revile</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>sacrilegious</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>vilify</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>vituperative</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='reverent#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
revere
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>worship or regard with great respect</td>
</tr>
<tr>
<td class='wordform'>
<span>
irreverent
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>disrespectful</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="reverent" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>reverent</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="reverent#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

