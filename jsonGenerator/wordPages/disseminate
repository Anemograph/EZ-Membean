
<!DOCTYPE html>
<html>
<head>
<title>Word: disseminate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Accretion is the slow, gradual process by which new things are added and something gets bigger.</p>
<p class='rw-defn idx1' style='display:none'>To agglomerate a group of things is to gather them together without any noticeable order.</p>
<p class='rw-defn idx2' style='display:none'>When two or more things, such as organizations, amalgamate, they combine to become one large thing.</p>
<p class='rw-defn idx3' style='display:none'>When you apportion something, such as blame or money, you decide how it should be shared among various people.</p>
<p class='rw-defn idx4' style='display:none'>If one&#8217;s powers or rights are circumscribed, they are limited or restricted.</p>
<p class='rw-defn idx5' style='display:none'>When you collate pieces of information, you gather them all together and arrange them in some sensible order so that you can examine and compare those data efficiently.</p>
<p class='rw-defn idx6' style='display:none'>When a liquid congeals, it becomes very thick and sticky, almost like a solid.</p>
<p class='rw-defn idx7' style='display:none'>To convoke a meeting or assembly is to bring people together for a formal gathering or ceremony of some kind.</p>
<p class='rw-defn idx8' style='display:none'>When something is diffuse, it is widely spread out and scattered.</p>
<p class='rw-defn idx9' style='display:none'>A dispersal of something is its scattering or distribution over a wide area.</p>
<p class='rw-defn idx10' style='display:none'>When something dissipates, it either disappears because it is driven away, or it is completely used up in some way—sometimes wastefully.</p>
<p class='rw-defn idx11' style='display:none'>If something fetters you, it restricts you from doing something you want to do.</p>
<p class='rw-defn idx12' style='display:none'>When you intersperse things, you distribute or scatter them among other things, sometimes at different intervals.</p>
<p class='rw-defn idx13' style='display:none'>When a substance permeates something, it enters into all parts of it.</p>
<p class='rw-defn idx14' style='display:none'>If something is pervasive, it appears to be everywhere.</p>
<p class='rw-defn idx15' style='display:none'>To promulgate something is to officially announce it in order to make it widely known or more specifically to let the public know that a new law has been put into effect.</p>
<p class='rw-defn idx16' style='display:none'>A stricture is a rule or condition that imposes restrictions or limits on what you can do.</p>
<p class='rw-defn idx17' style='display:none'>If something, such as warmth, color, or liquid, suffuses a material, it gradually spreads through or covers it; if you are suffused with a feeling, you are full of that feeling.</p>
<p class='rw-defn idx18' style='display:none'>When you trammel something or someone, you bring about limits to freedom or hinder movements or activities in some fashion.</p>
<p class='rw-defn idx19' style='display:none'>When something is transfused to another thing, it is given, put, or imparted to it; for example, you can transfuse blood or a love of reading from one person to another.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>disseminate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='disseminate#' id='pronounce-sound' path='audio/words/amy-disseminate'></a>
di-SEM-uh-nayt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='disseminate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='disseminate#' id='context-sound' path='audio/wordcontexts/brian-disseminate'></a>
Samantha printed hundreds of posters and signs about the concert because she wanted to <em>disseminate</em> and spread the information as widely as possible.  After she spent the whole morning hanging up signs and <em><em>disseminating</em></em> the concert details throughout the entire town, she decided to use her Facebook page as well.  Sam posted a notice on her page at lunch, and by dinnertime hundreds of people had received the <em>disseminated</em>, distributed information about the event.  The concert was a huge, popular success thanks to Sam&#8217;s clever broadcast and <em><em>dissemination</em></em>.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean to <em>disseminate</em> information?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
You pull out the main points and create a summary of it.
</li>
<li class='choice answer '>
<span class='result'></span>
You spread it around so that many people have access to it.
</li>
<li class='choice '>
<span class='result'></span>
You present it to a small group in a creative and entertaining way.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='disseminate#' id='definition-sound' path='audio/wordmeanings/amy-disseminate'></a>
To <em>disseminate</em> something, such as knowledge or information, is to distribute it so that it reaches a lot of people.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>distribute</em>
</span>
</span>
</div>
<a class='quick-help' href='disseminate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='disseminate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/disseminate/memory_hooks/3096.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Semin</span></span>ar Circul<span class="emp2"><span>ate</span></span>s <span class="emp1"><span>Dis</span></span>ease</span></span> I loved my college <span class="emp3"><span>semin</span></span>ar on short stories where we talked and talked about such interesting things, but one day that <span class="emp3"><span>semin</span></span>ar <span class="emp1"><span>dis</span></span><span class="emp3"><span>semin</span></span><span class="emp2"><span>ate</span></span>d a <span class="emp1"><span>dis</span></span>ease which one of the participants had, circul<span class="emp2"><span>at</span></span>ing it amongst us all ... ever afterwards, we remembered it as the <span class="emp1"><span>dis</span></span>ease circul<span class="emp2"><span>at</span></span>ing <span class="emp3"><span>semin</span></span>ar.
</p>
</div>

<div id='memhook-button-bar'>
<a href="disseminate#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/disseminate/memory_hooks">Use other hook</a>
<a href="disseminate#" id="memhook-use-own" url="https://membean.com/mywords/disseminate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='disseminate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The U.N. agency teamed up with over 40 tech companies to help <b>disseminate</b> facts, minimize the spread of false information and remove misleading posts.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Athletic trainers also have to keep up to date with the latest COVID-19 information and protocols, then condense and <b>disseminate</b> to coaches and athletes.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
Von der Leyen said the EU wants the onus put on the tech giants, with “it clearly laid down that internet companies take responsibility for the manner in which they <b>disseminate</b>, promote and remove content.”
<cite class='attribution'>
&mdash;
ABC News
</cite>
</li>
<li>
In October 2005, Al Qaeda even used one of its Web sites to post a help-wanted ad for a job as a communications specialist. The job vacancy called for someone with exceptional English and Arabic skills able to collect and <b>disseminate</b> news on Iraq, including audio and video clips.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/disseminate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='disseminate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='dis_apart' data-tree-url='//cdn1.membean.com/public/data/treexml' href='disseminate#'>
<span class='common'></span>
dis-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>apart</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='semin_seed' data-tree-url='//cdn1.membean.com/public/data/treexml' href='disseminate#'>
<span class=''></span>
semin
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>seed</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ate_quality' data-tree-url='//cdn1.membean.com/public/data/treexml' href='disseminate#'>
<span class=''></span>
-ate
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make something have a certain quality</td>
</tr>
</table>
<p>When one <em>disseminates</em> information, the &#8220;seeds&#8221; of that information go &#8220;apart&#8221; from their sender far and wide.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='disseminate#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Doctor Who</strong><span> The machine will act as a disseminating agent.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/disseminate.jpg' video_url='examplevids/disseminate' video_width='350'></span>
<div id='wt-container'>
<img alt="Disseminate" height="288" src="https://cdn1.membean.com/video/examplevids/disseminate.jpg" width="350" />
<div class='center'>
<a href="disseminate#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='disseminate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Disseminate" src="https://cdn0.membean.com/public/images/wordimages/cons2/disseminate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='disseminate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>apportion</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>diffuse</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>dispersal</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>dissipate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>intersperse</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>permeate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>pervasive</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>promulgate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>suffuse</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>transfuse</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>accretion</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>agglomerate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>amalgamate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>circumscribe</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>collate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>congeal</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>convoke</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>fetter</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>stricture</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>trammel</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="disseminate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>disseminate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="disseminate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

