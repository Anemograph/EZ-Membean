
<!DOCTYPE html>
<html>
<head>
<title>Word: superimpose | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An adherent is a supporter or follower of a leader or a cause.</p>
<p class='rw-defn idx1' style='display:none'>To agglomerate a group of things is to gather them together without any noticeable order.</p>
<p class='rw-defn idx2' style='display:none'>An aggregate is the final or sum total after many different amounts or scores have been added together.</p>
<p class='rw-defn idx3' style='display:none'>When two or more things, such as organizations, amalgamate, they combine to become one large thing.</p>
<p class='rw-defn idx4' style='display:none'>A confluence is a situation where two or more things meet or flow together at a single point or area; a confluence usually refers to two streams joining together.</p>
<p class='rw-defn idx5' style='display:none'>The adjective conjugal refers to marriage or the relationship between two married people.</p>
<p class='rw-defn idx6' style='display:none'>To denude an area is to remove the plants and trees that cover it; it can also mean to make something bare.</p>
<p class='rw-defn idx7' style='display:none'>When someone disinters a dead body, they dig it up; likewise, something disinterred is exposed or revealed to the public after having been hidden for some time.</p>
<p class='rw-defn idx8' style='display:none'>Dissolution is the breaking up or official end of a group, such as a couple or institution; it can also be the act of separating something into smaller components.</p>
<p class='rw-defn idx9' style='display:none'>If you divest someone of power, rights, or authority, you take those things away from them.</p>
<p class='rw-defn idx10' style='display:none'>To divulge something is to reveal information that was once private or secret.</p>
<p class='rw-defn idx11' style='display:none'>If you ensconce yourself somewhere, you put yourself into a comfortable place or safe position and have no intention of moving or leaving for quite some time.</p>
<p class='rw-defn idx12' style='display:none'>When you excise something, you remove it by cutting it out.</p>
<p class='rw-defn idx13' style='display:none'>If you expunge something, you completely remove it or do away with it.</p>
<p class='rw-defn idx14' style='display:none'>If you extirpate something, you completely destroy it because it is unpleasant or unwanted.</p>
<p class='rw-defn idx15' style='display:none'>The juxtaposition of two objects is the act of positioning them side by side so that the differences between them are more readily visible.</p>
<p class='rw-defn idx16' style='display:none'>Something that is latent, such as an undiscovered talent or undiagnosed disease, exists but is not active or has not developed at the moment—it may develop or become active in the future.</p>
<p class='rw-defn idx17' style='display:none'>A lattice is a pattern or structure that has parallel sets of lines of material crossing each other, often at right angles.</p>
<p class='rw-defn idx18' style='display:none'>Liaison is the cooperation and exchange of information between people or organizations in order to facilitate their joint efforts; this word also refers to a person who coordinates a connection between people or organizations.</p>
<p class='rw-defn idx19' style='display:none'>If something, such as a road, is occluded, it has been closed off or blocked; therefore, cars are prevented from continuing on their way until the road is opened once again.</p>
<p class='rw-defn idx20' style='display:none'>Something is opaque if it is either difficult to understand or is not transparent.</p>
<p class='rw-defn idx21' style='display:none'>A schism causes a group or organization to divide into two groups as a result of differences in their aims and beliefs.</p>
<p class='rw-defn idx22' style='display:none'>If you are supine, you are lying on your back with your face upward.</p>
<p class='rw-defn idx23' style='display:none'>A veneer is a thin layer, such as a thin sheet of expensive wood over a cheaper type of wood that gives a false appearance of higher quality; a person can also put forth a false front or veneer.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>superimpose</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='superimpose#' id='pronounce-sound' path='audio/words/amy-superimpose'></a>
soo-per-im-POHZ
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='superimpose#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='superimpose#' id='context-sound' path='audio/wordcontexts/brian-superimpose'></a>
Clark&#8217;s artwork combined photography and artistic drawing, <em>superimposing</em> a drawing over a photographic image to create a layered landscape.  He mixed mythic figures with city scenes, <em><em>superimposing</em></em> the old over the new by placing ancient characters in a modern day downtown setting.  These creative works overlapped marble statues of the gods with Wall Street, <em><em>superimposing</em></em> the power of ancient Greece with the sharp energy of present day urban life.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean to <em>superimpose</em> an image?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
To take that image and create a matching piece of art from it.
</li>
<li class='choice answer '>
<span class='result'></span>
To lay it on top of another image so they become a single image.
</li>
<li class='choice '>
<span class='result'></span>
To remove all of the color from it so it is in black and white.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='superimpose#' id='definition-sound' path='audio/wordmeanings/amy-superimpose'></a>
If two things are <em>superimposed</em>, one is stacked over the other so that both become one.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>lay over</em>
</span>
</span>
</div>
<a class='quick-help' href='superimpose#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='superimpose#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/superimpose/memory_hooks/4123.json'></span>
<p>
<img alt="Superimpose" src="https://cdn1.membean.com/public/images/wordimages/hook/superimpose.jpg?qdep8" />
<span class="emp0"><span><span class="emp1"><span>Super</span></span>model <span class="emp1"><span>Suprem</span></span>e <span class="emp3"><span>Pose</span></span></span></span> A <span class="emp1"><span>suprem</span></span>e <span class="emp3"><span>pose</span></span> would be to <span class="emp1"><span>super</span></span>i<span class="emp1"><span>m</span></span><span class="emp3"><span>pose</span></span> one <span class="emp1"><span>super</span></span>model over another, taking the best aspects of each to form one <span class="emp1"><span>super</span></span> <span class="emp1"><span>super</span></span>model, not an altogether unpleasant proposition!
</p>
</div>

<div id='memhook-button-bar'>
<a href="superimpose#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/superimpose/memory_hooks">Use other hook</a>
<a href="superimpose#" id="memhook-use-own" url="https://membean.com/mywords/superimpose/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='superimpose#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Alaska is so big that if you <b>superimposed</b> it over the continental United States, one end would cover North Carolina and the other would touch California.
<span class='attribution'>&mdash; James Abel (pseudonym of Bob Reiss, American journalist who writes about the Arctic), from _White Plague_</span>
<img alt="James abel (pseudonym of bob reiss, american journalist who writes about the arctic), from _white plague_" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/James Abel (pseudonym of Bob Reiss, American journalist who writes about the Arctic), from _White Plague_.jpg?qdep8" width="80" />
</li>
<li>
What if Google Maps results automatically linked to all available area photographs, or could instantly <b>superimpose</b> satellite or aerial imagery in a form similar to some Multimap charts? More entertaining perhaps, but not necessarily more useful.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
For example, VR can be used to <b>superimpose</b> important information directly onto the windshield. Without glancing at a handheld device, drivers can see alternate routes, blocked roads and traffic snags.
<cite class='attribution'>
&mdash;
Forbes
</cite>
</li>
<li>
Place names and explanatory text are <b>superimposed</b> over objects seen through the viewer’s screen, and animated graphics show how some structures were built or destroyed.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/superimpose/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='superimpose#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='super_over' data-tree-url='//cdn1.membean.com/public/data/treexml' href='superimpose#'>
<span class='common'></span>
super-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>over, above</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='im_in' data-tree-url='//cdn1.membean.com/public/data/treexml' href='superimpose#'>
<span class='common'></span>
im-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>in, on</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pos_put' data-tree-url='//cdn1.membean.com/public/data/treexml' href='superimpose#'>
<span class='common'></span>
pos
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>put, place</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='superimpose#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>To <em>superimpose</em> one thing on another, one &#8220;places it on and over&#8221; it.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='superimpose#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Confused Travolta Meme</strong><span> A meme of John Travolta superimposed on many different scenes.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/superimpose.jpg' video_url='examplevids/superimpose' video_width='350'></span>
<div id='wt-container'>
<img alt="Superimpose" height="288" src="https://cdn1.membean.com/video/examplevids/superimpose.jpg" width="350" />
<div class='center'>
<a href="superimpose#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='superimpose#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Superimpose" src="https://cdn3.membean.com/public/images/wordimages/cons2/superimpose.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='superimpose#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adherent</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>agglomerate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>aggregate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>amalgamate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>confluence</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>conjugal</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>ensconce</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>juxtaposition</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>latent</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>lattice</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>liaison</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>occlude</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>opaque</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>supine</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>veneer</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='6' class = 'rw-wordform notlearned'><span>denude</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>disinter</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>dissolution</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>divest</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>divulge</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>excise</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>expunge</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>extirpate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>schism</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="superimpose" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>superimpose</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="superimpose#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

