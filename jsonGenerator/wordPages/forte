
<!DOCTYPE html>
<html>
<head>
<title>Word: forte | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Forte-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/forte-large.jpg?qdep8" />
</div>
<a href="forte#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When you are adept at something, you are very good, skillful, or talented at it.</p>
<p class='rw-defn idx1' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx2' style='display:none'>A bailiwick is a person&#8217;s sphere of knowledge or learned skill.</p>
<p class='rw-defn idx3' style='display:none'>A bravura performance, such as of a highly technical work for the violin, is done with consummate skill.</p>
<p class='rw-defn idx4' style='display:none'>The caliber of a person is the level or quality of their ability, intelligence, and other talents.</p>
<p class='rw-defn idx5' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx6' style='display:none'>If something daunts you, it makes you worried about dealing with it because you think it will be very difficult or dangerous.</p>
<p class='rw-defn idx7' style='display:none'>Debility is a state of being physically or mentally weak, usually due to illness.</p>
<p class='rw-defn idx8' style='display:none'>Decrepitude is the state of being very old, worn out, or very ill; therefore, something or someone is no longer in good physical condition or good health.</p>
<p class='rw-defn idx9' style='display:none'>A deft movement or action is made quickly and with skill.</p>
<p class='rw-defn idx10' style='display:none'>If you describe someone, usually a young woman, as demure, you mean that she is quiet, shy, and always behaves modestly.</p>
<p class='rw-defn idx11' style='display:none'>If you describe a person, group, or civilization as effete, you mean it is weak, exhausted,  powerless, unproductive, and/or corrupt.</p>
<p class='rw-defn idx12' style='display:none'>Efficacy is the ability or power to produce an expected effect or result.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx14' style='display:none'>If something is formidable, it is impressive in size, power, or skill; therefore, it can make you feel frightened, alarmed, or in awe because it is so powerful or difficult to deal with.</p>
<p class='rw-defn idx15' style='display:none'>A languid person is slow, relaxed, and shows little energy or interest in doing anything.</p>
<p class='rw-defn idx16' style='display:none'>Lassitude is a state of tiredness, lack of energy, and having little interest in what&#8217;s going on around you.</p>
<p class='rw-defn idx17' style='display:none'>If you are lethargic, you are tired, lack energy, and are unwilling to exert effort.</p>
<p class='rw-defn idx18' style='display:none'>When you are listless, you lack energy and interest and are unwilling to exert any effort.</p>
<p class='rw-defn idx19' style='display:none'>Malaise is a feeling of discontent, general unease, or even illness in a person or group for which there does not seem to be a quick and easy solution because the cause of it is not clear.</p>
<p class='rw-defn idx20' style='display:none'>Someone&#8217;s mettle is their courageous determination and spirited ability to deal with problems or difficult situations.</p>
<p class='rw-defn idx21' style='display:none'>Someone is obtuse when they are slow to understand things; someone can choose to be obtuse when they deliberately do not try to figure things out.</p>
<p class='rw-defn idx22' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>
<p class='rw-defn idx23' style='display:none'>When you have been remiss, you have been careless because you did not do something that you should have done.</p>
<p class='rw-defn idx24' style='display:none'>A sagacious person is wise, intelligent, and has the ability to make good practical decisions.</p>
<p class='rw-defn idx25' style='display:none'>A savant is a person who knows a lot about a subject, either via a lifetime of learning or by considerable natural ability.</p>
<p class='rw-defn idx26' style='display:none'>If your body is affected by torpor, you are severely lacking in energy; therefore, you are idle—and can even be numb.</p>
<p class='rw-defn idx27' style='display:none'>A virtuoso is someone who is very skillful at something, especially playing a musical instrument.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>forte</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='forte#' id='pronounce-sound' path='audio/words/amy-forte'></a>
FAWR-tay
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='forte#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='forte#' id='context-sound' path='audio/wordcontexts/brian-forte'></a>
I own a restaurant and can cook reasonably well, but cooking really isn&#8217;t my true strength or <em>forte</em>.  My real talent or <em>forte</em> lies in managing and organizing others.  Over the years, my gourmet cooking skills have improved, but my <em>forte</em> or personal specialty will always be in motivating chefs and the rest of the staff to create great food in the restaurant.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How do you know if something is your <em>forte</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
You&#8217;re an expert at it.
</li>
<li class='choice '>
<span class='result'></span>
You try to avoid doing it.
</li>
<li class='choice '>
<span class='result'></span>
It has your name on it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='forte#' id='definition-sound' path='audio/wordmeanings/amy-forte'></a>
If something is your <em>forte</em>, you are very good at it or know a lot about it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>specialty</em>
</span>
</span>
</div>
<a class='quick-help' href='forte#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='forte#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/forte/memory_hooks/5094.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Fort</span></span> of Str<span class="emp2"><span>e</span></span>ngth</span></span> Although my husband Guy might not be a tower of str<span class="emp2"><span>e</span></span>ngth in all situations, his <span class="emp1"><span>fort</span></span><span class="emp2"><span>e</span></span> is being my <span class="emp1"><span>fort</span></span> of str<span class="emp2"><span>e</span></span>ngth to which I can withdraw if I've had a bad day.
</p>
</div>

<div id='memhook-button-bar'>
<a href="forte#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/forte/memory_hooks">Use other hook</a>
<a href="forte#" id="memhook-use-own" url="https://membean.com/mywords/forte/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='forte#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
My <b>forte</b> is editing and I am most experienced in that. I love the challenge of playing with material and imagination while editing.
<span class='attribution'>&mdash; Vikramaditya Motwane, Indian filmmaker </span>
<img alt="Vikramaditya motwane, indian filmmaker " height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Vikramaditya Motwane, Indian filmmaker .jpg?qdep8" width="80" />
</li>
<li>
Understanding the issues and problems from the local point of view has never been the <b>forte</b> of Americans, but it is especially difficult in Iraq, where security and the language barrier offer unique challenges.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
The Arizona senator has struggled throughout the campaign to seem more energized and engaged in debate formats, which clearly aren’t his <b>forte</b>.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
“I’d always wanted to write story songs,” [Rosanne] Cash replied. “I wanted to write those Appalachian ballads with four characters and 12 verses, but I’d always felt it wasn’t my <b>forte</b>, that it was beyond me.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/forte/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='forte#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='fort_strong' data-tree-url='//cdn1.membean.com/public/data/treexml' href='forte#'>
<span class=''></span>
fort
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>strong, vigorous, powerful</td>
</tr>
</table>
<p>One&#8217;s <em>forte</em> is what one is &#8220;strong, vigorous, or powerful&#8221; at.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='forte#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>King of Queens</strong><span> He claims that table tennis is his forte.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/forte.jpg' video_url='examplevids/forte' video_width='350'></span>
<div id='wt-container'>
<img alt="Forte" height="288" src="https://cdn1.membean.com/video/examplevids/forte.jpg" width="350" />
<div class='center'>
<a href="forte#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='forte#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Forte" src="https://cdn2.membean.com/public/images/wordimages/cons2/forte.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='forte#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adept</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bailiwick</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bravura</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>caliber</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>deft</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>efficacy</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>formidable</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>mettle</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>sagacious</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>savant</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>virtuoso</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>daunt</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>debility</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>decrepitude</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>demure</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>effete</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>languid</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>lassitude</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>lethargic</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>listless</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>malaise</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>obtuse</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>remiss</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>torpor</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="forte" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>forte</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="forte#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

