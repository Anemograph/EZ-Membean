
<!DOCTYPE html>
<html>
<head>
<title>Word: antiquated | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Antiquated-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/antiquated-large.jpg?qdep8" />
</div>
<a href="antiquated#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>Something antediluvian is so old or old-fashioned that it seems to belong to a much earlier period in history.</p>
<p class='rw-defn idx1' style='display:none'>Something that is archaic is out of date or not currently used anymore because it is no longer considered useful or efficient.</p>
<p class='rw-defn idx2' style='display:none'>A contemporary object exists at the same time as something else or exists at the current time.</p>
<p class='rw-defn idx3' style='display:none'>Decrepitude is the state of being very old, worn out, or very ill; therefore, something or someone is no longer in good physical condition or good health.</p>
<p class='rw-defn idx4' style='display:none'>A soft material that becomes hardened into stone over time has become fossilized.</p>
<p class='rw-defn idx5' style='display:none'>Hackneyed words, images, ideas or sayings have been used so often that they no longer seem interesting or amusing.</p>
<p class='rw-defn idx6' style='display:none'>An inception of something is its beginning or start.</p>
<p class='rw-defn idx7' style='display:none'>To initiate something is to begin or start it.</p>
<p class='rw-defn idx8' style='display:none'>An innovation is something new that is created or is a novel process for doing something.</p>
<p class='rw-defn idx9' style='display:none'>A neophyte is a person who is just beginning to learn a subject or skill—or how to do an activity of some kind.</p>
<p class='rw-defn idx10' style='display:none'>If something is obsolescent, it is slowly becoming no longer needed because something newer or more effective has been invented.</p>
<p class='rw-defn idx11' style='display:none'>A paradigm is a model or example of something that shows how it works or how it can be produced; a paradigm shift is a completely new way of thinking about something.</p>
<p class='rw-defn idx12' style='display:none'>The adjective primordial is used to describe things that existed close to the formation of Earth or close to the origin or development of something.</p>
<p class='rw-defn idx13' style='display:none'>Something that is superannuated is so old and worn out that it is no longer working or useful.</p>
<p class='rw-defn idx14' style='display:none'>The adjective topical describes something that is related to a topic or is of current interest; this word also refers to a treatment that is applied to the surface of the body.</p>
<p class='rw-defn idx15' style='display:none'>A tyro has just begun learning something.</p>
<p class='rw-defn idx16' style='display:none'>If something, such as clothing, is in vogue, it is currently in fashion or is one of the hottest new trends; hence, it is very popular.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>antiquated</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='antiquated#' id='pronounce-sound' path='audio/words/amy-antiquated'></a>
AN-ti-kway-tid
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='antiquated#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='antiquated#' id='context-sound' path='audio/wordcontexts/brian-antiquated'></a>
When Robert visited his uncle in the mountains of Nepal, the <em>antiquated</em> or obsolete plumbing system challenged him.  Having spent most of his life in modern urban settings, Robert felt insecure among <em>antiquated</em> hand tools and outdated facilities.  His adventurous and elderly uncle&#8212;who was rather <em>antiquated</em> himself&#8212;merely grinned at Robert&#8217;s views of travel in Nepal and kept hiking, wooden staff in hand, toward the horizon.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean when something is <em>antiquated</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is considered highly likable and even beloved.
</li>
<li class='choice answer '>
<span class='result'></span>
It is considered too old and useless.
</li>
<li class='choice '>
<span class='result'></span>
It is considered boring and not interesting.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='antiquated#' id='definition-sound' path='audio/wordmeanings/amy-antiquated'></a>
Something <em>antiquated</em> is old-fashioned and not suitable for modern needs or conditions.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>obsolete</em>
</span>
</span>
</div>
<a class='quick-help' href='antiquated#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='antiquated#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/antiquated/memory_hooks/4029.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Anti</span></span>qu<span class="emp3"><span>ate</span></span>d</span></span> My great <span class="emp2"><span>Aunti</span></span>e K<span class="emp3"><span>ate</span></span> is so old-fashioned.
</p>
</div>

<div id='memhook-button-bar'>
<a href="antiquated#" id="add-public-hook" style="" url="https://membean.com/mywords/antiquated/memory_hooks">Use other public hook</a>
<a href="antiquated#" id="memhook-use-own" url="https://membean.com/mywords/antiquated/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='antiquated#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
No idea is so <b>antiquated</b> that it was not once modern; no idea is so modern that it will not someday be <b>antiquated</b>.
<span class='attribution'>&mdash; Ellen Glasgow, American novelist</span>
<img alt="Ellen glasgow, american novelist" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Ellen Glasgow, American novelist.jpg?qdep8" width="80" />
</li>
<li>
Both accidents have revealed inefficiencies and problems at all levels of the aviation industry: insufficient airport capacity, too few air-traffic controllers, <b>antiquated</b> air-traffic equipment, inadequate runways, unclear regulatory authority, etc.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Though McEnroe’s complaints were undeniably exaggerated and his actions inexcusable, at the core of his running battle was a beef cited by many competitors, namely that Wimbledon is overly regimented and haughty, an <b>antiquated</b> relic.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Society tends to treat woman of a certain age like they're irrelevant, even invisible, their ways <b>antiquated</b> and their powers, skills and accomplishments long forgotten.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/antiquated/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='antiquated#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='antiqu_old' data-tree-url='//cdn1.membean.com/public/data/treexml' href='antiquated#'>
<span class=''></span>
antiqu
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>old</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ed_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='antiquated#'>
<span class=''></span>
-ed
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>having a particular state</td>
</tr>
</table>
<p>If something is <em>antiquated</em>, it has been &#8220;made old.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='antiquated#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>AFP News Agency</strong><span> This music store hopes some day to bring back the cassette, an antiquated format for music.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/antiquated.jpg' video_url='examplevids/antiquated' video_width='350'></span>
<div id='wt-container'>
<img alt="Antiquated" height="288" src="https://cdn1.membean.com/video/examplevids/antiquated.jpg" width="350" />
<div class='center'>
<a href="antiquated#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='antiquated#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Antiquated" src="https://cdn0.membean.com/public/images/wordimages/cons2/antiquated.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='antiquated#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>antediluvian</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>archaic</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>decrepitude</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>fossilized</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>hackneyed</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>obsolescent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>primordial</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>superannuated</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>contemporary</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>inception</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>initiate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>innovation</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>neophyte</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>paradigm</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>topical</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>tyro</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>vogue</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="antiquated" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>antiquated</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="antiquated#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

