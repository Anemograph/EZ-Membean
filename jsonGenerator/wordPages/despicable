
<!DOCTYPE html>
<html>
<head>
<title>Word: despicable | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abhor something, you dislike it very much, usually because you think it&#8217;s immoral.</p>
<p class='rw-defn idx1' style='display:none'>The word abject emphasizes a very bad situation or quality, thereby making it even worse.</p>
<p class='rw-defn idx2' style='display:none'>If you abominate something, you hate it because you think it is extremely wrong and unacceptable.</p>
<p class='rw-defn idx3' style='display:none'>A situation or condition that is abysmal is extremely bad or of wretched quality.</p>
<p class='rw-defn idx4' style='display:none'>A benefaction is a charitable contribution of money or assistance that someone gives to a person or organization.</p>
<p class='rw-defn idx5' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx6' style='display:none'>A contemptible act is shameful, disgraceful, and worthy of scorn.</p>
<p class='rw-defn idx7' style='display:none'>Depravity is behavior that is immoral, corrupt, or evil.</p>
<p class='rw-defn idx8' style='display:none'>An egregious mistake, failure, or problem is an extremely bad and very noticeable one.</p>
<p class='rw-defn idx9' style='display:none'>An action that is flagrant shows that someone does not care if they obviously break the rules or highly offend people.</p>
<p class='rw-defn idx10' style='display:none'>If you describe something as heinous, you mean that is extremely evil, shocking, and bad.</p>
<p class='rw-defn idx11' style='display:none'>Ignominy is a dishonorable or shameful situation in which someone feels publicly embarrassed and loses the respect of others.</p>
<p class='rw-defn idx12' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx13' style='display:none'>Something loathsome is offensive, disgusting, and brings about intense dislike.</p>
<p class='rw-defn idx14' style='display:none'>A nefarious activity is considered evil and highly dishonest.</p>
<p class='rw-defn idx15' style='display:none'>If something is noisome, it is extremely unpleasant, especially because it is very dirty or has an awful smell.</p>
<p class='rw-defn idx16' style='display:none'>Something that is noxious is harmful, extremely unpleasant, and often poisonous.</p>
<p class='rw-defn idx17' style='display:none'>If you describe people or things as odious, you think that they are extremely unpleasant.</p>
<p class='rw-defn idx18' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx19' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx20' style='display:none'>If you think a type of behavior or idea is reprehensible, you think that it is very bad, morally wrong, and deserves to be strongly criticized.</p>
<p class='rw-defn idx21' style='display:none'>A reprobate has a bad character and behaves in an immoral or improper way.</p>
<p class='rw-defn idx22' style='display:none'>Turpitude is the state of engaging in immoral or evil activities.</p>
<p class='rw-defn idx23' style='display:none'>A vicious person is corrupt, violent, aggressive, and/or highly immoral.</p>
<p class='rw-defn idx24' style='display:none'>Something vile is evil, very unpleasant, horrible, or extremely bad.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>despicable</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='despicable#' id='pronounce-sound' path='audio/words/amy-despicable'></a>
di-SPIK-uh-buhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='despicable#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='despicable#' id='context-sound' path='audio/wordcontexts/brian-despicable'></a>
The summer-camp counselor found the awful tricks of his young campers <em>despicable</em> and hateful.  After finding sneakers full of honey and blankets tossed in the lake, he scolded the group for their cruel and <em>despicable</em> pranks on each other.  Nevertheless, the <em>despicable</em> and shameful acts continued, including poisonous snakes in one boy&#8217;s bed, a boy&#8217;s hat in the toilet, and a package of cookies from home smashed to bits.  When the counselor woke to find his own face painted purple and glue stuck in his hair, he discovered who was at fault and sent the <em>despicable</em> tricksters home.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When is something <em>despicable</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When it&#8217;s an avoidable mistake that&#8217;s hard to fix.
</li>
<li class='choice answer '>
<span class='result'></span>
When it&#8217;s extremely unpleasant and shameful.
</li>
<li class='choice '>
<span class='result'></span>
When it&#8217;s mildly annoying but also amusing.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='despicable#' id='definition-sound' path='audio/wordmeanings/amy-despicable'></a>
If you say a person&#8217;s actions are <em>despicable</em>, you think they are extremely unpleasant or nasty.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>vile</em>
</span>
</span>
</div>
<a class='quick-help' href='despicable#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='despicable#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/despicable/memory_hooks/3917.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Desp</span></span>erate for <span class="emp1"><span>Cable</span></span></span></span> Jeremy was so <span class="emp2"><span>desp</span></span>erate for <span class="emp1"><span>cable</span></span> that he tapped into his neighbor's <span class="emp1"><span>cable</span></span> line and diverted half the bandwidth to his computer, a <span class="emp2"><span>desp</span></span>i<span class="emp1"><span>cable</span></span> act to be sure.
</p>
</div>

<div id='memhook-button-bar'>
<a href="despicable#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/despicable/memory_hooks">Use other hook</a>
<a href="despicable#" id="memhook-use-own" url="https://membean.com/mywords/despicable/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='despicable#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
It’s up to all of us to do away with any feelings of indifference we may feel toward these <b>despicable</b> acts of race-based discrimination and violence and stand up for our neighbors and what’s right.
<cite class='attribution'>
&mdash;
Kent Reporter
</cite>
</li>
<li>
"I call on all other candidates and their supporters to repudiate these attacks and join me in pledging not to engage in such <b>despicable</b> tactics throughout the balance of this campaign," McCain said. "I am outraged by the cowardly telephone calls that hide behind my name in an effort to disparage one candidate and advance the candidacy of another."
<cite class='attribution'>
&mdash;
Politico
</cite>
</li>
<li>
Gru is <b>despicable—on</b> purpose. He means to be the biggest villain in the world, in fact, but he's not very good at villainy.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Nothing is more <b>despicable</b> than a professional talker who uses his words as a quack uses his remedies.
<cite class='attribution'>
&mdash;
François Fénelon, French archbishop and writer
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/despicable/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='despicable#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='de_off' data-tree-url='//cdn1.membean.com/public/data/treexml' href='despicable#'>
<span class='common'></span>
de-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>down, off, from</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='spic_see' data-tree-url='//cdn1.membean.com/public/data/treexml' href='despicable#'>
<span class=''></span>
spic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>see, observe, look, watch over</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='able_handy' data-tree-url='//cdn1.membean.com/public/data/treexml' href='despicable#'>
<span class=''></span>
-able
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>handy, capable of</td>
</tr>
</table>
<p>Something <em>despicable</em> is that which is only &#8220;capable&#8221; of being done &#8220;from the sight or observation&#8221; of others because it is so bad.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='despicable#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Toy Story</strong><span> Sid is acting in a despicable way.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/despicable.jpg' video_url='examplevids/despicable' video_width='350'></span>
<div id='wt-container'>
<img alt="Despicable" height="198" src="https://cdn1.membean.com/video/examplevids/despicable.jpg" width="350" />
<div class='center'>
<a href="despicable#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='despicable#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Despicable" src="https://cdn2.membean.com/public/images/wordimages/cons2/despicable.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='despicable#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abhor</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abject</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abominate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>abysmal</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>contemptible</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>depravity</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>egregious</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>flagrant</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>heinous</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>ignominy</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>loathsome</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>nefarious</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>noisome</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>noxious</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>odious</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>reprehensible</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>reprobate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>turpitude</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>vicious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>vile</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='4' class = 'rw-wordform notlearned'><span>benefaction</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="despicable" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>despicable</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="despicable#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

