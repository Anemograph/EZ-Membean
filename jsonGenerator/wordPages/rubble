
<!DOCTYPE html>
<html>
<head>
<title>Word: rubble | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Rubble-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/rubble-large.jpg?qdep8" />
</div>
<a href="rubble#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>A state of anarchy occurs when there is no organization or order in a nation or country, especially when no effective government exists.</p>
<p class='rw-defn idx1' style='display:none'>Bedlam is a situation with a great deal of noise and confusion.</p>
<p class='rw-defn idx2' style='display:none'>Chaff is the husks of grain separated from the edible plant; it can also be useless matter in general that is cast aside.</p>
<p class='rw-defn idx3' style='display:none'>A chaotic state of affairs is in a state of confusion and complete disorder.</p>
<p class='rw-defn idx4' style='display:none'>A cohesive argument sticks together, working as a consistent, unified whole.</p>
<p class='rw-defn idx5' style='display:none'>Contiguous things are in contact with or near each other; contiguous events happen one right after the other without a break.</p>
<p class='rw-defn idx6' style='display:none'>Detritus is the small pieces of waste that remain after something has been destroyed or used.</p>
<p class='rw-defn idx7' style='display:none'>A dilapidated building, vehicle, etc. is old, broken-down, and in very bad condition.</p>
<p class='rw-defn idx8' style='display:none'>When you are in a state of disarray, you are disorganized, disordered, and in a state of confusion.</p>
<p class='rw-defn idx9' style='display:none'>When you dismantle something, you take it apart or destroy it piece by piece. </p>
<p class='rw-defn idx10' style='display:none'>Dissolution is the breaking up or official end of a group, such as a couple or institution; it can also be the act of separating something into smaller components.</p>
<p class='rw-defn idx11' style='display:none'>Something described as dross is of very poor quality and has little or no value.</p>
<p class='rw-defn idx12' style='display:none'>Entropy is the lack of organization or measure of disorder currently in a system.</p>
<p class='rw-defn idx13' style='display:none'>If you forage for things, such as keys or coins, you search for them inside a backpack or purse; when animals forage, they search for food over a wide area.</p>
<p class='rw-defn idx14' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx15' style='display:none'>An insurrection is a rebellion or open uprising against an established form of government.</p>
<p class='rw-defn idx16' style='display:none'>Pandemonium is a very noisy and uncontrolled situation, especially one that is caused by a lot of angry or excited people.</p>
<p class='rw-defn idx17' style='display:none'>If soldiers pillage a place, such as a town or museum, they steal a lot of things and damage it using violent methods.</p>
<p class='rw-defn idx18' style='display:none'>When you plunder, you forcefully take or rob others of their possessions, usually during times of war, natural disaster, or other unrest.</p>
<p class='rw-defn idx19' style='display:none'>When you ravage something, you completely destroy, wreck, or damage it.</p>
<p class='rw-defn idx20' style='display:none'>The remnants of something are the small parts of it that are left after the main part has been used or destroyed.</p>
<p class='rw-defn idx21' style='display:none'>Something that is residual is the part that still stays or remains after the main part is taken away.</p>
<p class='rw-defn idx22' style='display:none'>Sedition is the act of encouraging people to disobey and oppose the government currently in power.</p>
<p class='rw-defn idx23' style='display:none'>Shards are sharp pieces of broken glass, pottery, metal, or other hard substances.</p>
<p class='rw-defn idx24' style='display:none'>A vestige of something is a very small part that remains from a once larger whole.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>rubble</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='rubble#' id='pronounce-sound' path='audio/words/amy-rubble'></a>
RUHB-uhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='rubble#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='rubble#' id='context-sound' path='audio/wordcontexts/brian-rubble'></a>
After the hurricane hit we sadly went back to our homes to pick up the <em>rubble</em> or crumbled debris.  As we looked through the <em>rubble</em> or wreckage we found all sorts of broken items.  It was a difficult process, but we were able to pick through the <em>rubble</em> or fragments of our former houses to recover many items, but many were lost forever.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Where might you find <em>rubble</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
At the bottom of a lake or pond.
</li>
<li class='choice answer '>
<span class='result'></span>
At the site of a building that has collapsed.
</li>
<li class='choice '>
<span class='result'></span>
In the warehouse of a large food market.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='rubble#' id='definition-sound' path='audio/wordmeanings/amy-rubble'></a>
<em>Rubble</em> is the debris, remains, or wreckage of something that has been ruined, such as a building or other structure.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>debris</em>
</span>
</span>
</div>
<a class='quick-help' href='rubble#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='rubble#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/rubble/memory_hooks/5588.json'></span>
<p>
<span class="emp0"><span>T<span class="emp2"><span>rouble</span></span>! St<span class="emp2"><span>ubble</span></span> R<span class="emp2"><span>ubble</span></span>!</span></span> I always get in t<span class="emp2"><span>rouble</span></span> when my girlfriend finds my st<span class="emp2"><span>ubble</span></span> in the sink after shaving--she tells me to clean up that st<span class="emp2"><span>ubble</span></span> r<span class="emp2"><span>ubble</span></span> or else!
</p>
</div>

<div id='memhook-button-bar'>
<a href="rubble#" id="add-public-hook" style="" url="https://membean.com/mywords/rubble/memory_hooks">Use other public hook</a>
<a href="rubble#" id="memhook-use-own" url="https://membean.com/mywords/rubble/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='rubble#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Late last week independent engineers were examining the <b>rubble</b> that once was the Coliseum's "delicate roof," a roof that appeared to have floated 85 feet straight down, shattering into a million pieces under the weight of a huge layer of snow and ice.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Two police officers were pulled alive from the <b>rubble</b> late that night, more than 12 hours after the attack, raising a ray of hope for others.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
Although this isn’t a surprise in itself—indeed, many asteroids are believed to be floating "<b>rubble</b> piles"—the rate of spin of the asteroid posed a problem. Itokawa spins rather fast and if only the force of gravity was keeping the lumps of rock together, they would have been flung out into space long ago. In short, the asteroid shouldn’t exist.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/rubble/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='rubble#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p><em>Rubble</em> comes via a root word meaning &#8220;that which is rubbed off.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='rubble#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>CBS News</strong><span> Dog makes it out of the rubble alive.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/rubble.jpg' video_url='examplevids/rubble' video_width='350'></span>
<div id='wt-container'>
<img alt="Rubble" height="288" src="https://cdn1.membean.com/video/examplevids/rubble.jpg" width="350" />
<div class='center'>
<a href="rubble#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='rubble#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Rubble" src="https://cdn0.membean.com/public/images/wordimages/cons2/rubble.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='rubble#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>chaff</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>chaotic</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>detritus</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>dilapidated</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>disarray</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>dismantle</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>dissolution</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>dross</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>entropy</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>forage</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>remnant</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>residual</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>shard</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>vestige</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>anarchy</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>bedlam</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>cohesive</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>contiguous</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>insurrection</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>pandemonium</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>pillage</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>plunder</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>ravage</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>sedition</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="rubble" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>rubble</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="rubble#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

