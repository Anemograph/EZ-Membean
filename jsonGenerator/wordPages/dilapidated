
<!DOCTYPE html>
<html>
<head>
<title>Word: dilapidated | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Dilapidated-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/dilapidated-large.jpg?qdep8" />
</div>
<a href="dilapidated#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>Amenities are things that make a place comfortable or easy to live in, such as hot water, updated bathrooms, and sports facilities.</p>
<p class='rw-defn idx1' style='display:none'>Something antiquated is old-fashioned and not suitable for modern needs or conditions.</p>
<p class='rw-defn idx2' style='display:none'>A commodious room or house is large and roomy, which makes it convenient and highly suitable for living.</p>
<p class='rw-defn idx3' style='display:none'>A man who is dapper has a very neat and clean appearance; he is both fashionable and stylish.</p>
<p class='rw-defn idx4' style='display:none'>Decrepitude is the state of being very old, worn out, or very ill; therefore, something or someone is no longer in good physical condition or good health.</p>
<p class='rw-defn idx5' style='display:none'>Something, such as a building, is derelict if it is empty, not used, and in bad condition or disrepair.</p>
<p class='rw-defn idx6' style='display:none'>A fortification is a structure or building that is used in defense against an invading army.</p>
<p class='rw-defn idx7' style='display:none'>If you describe someone&#8217;s appearance or behavior as impeccable, you mean that it is perfect and therefore impossible to criticize.</p>
<p class='rw-defn idx8' style='display:none'>When someone is rehabilitated, they are restored to a more normal way of life, such as returning to good health or a crime-free life.</p>
<p class='rw-defn idx9' style='display:none'>When you renovate something, such as a home or other building, you modernize or make it like new again.</p>
<p class='rw-defn idx10' style='display:none'>People or things that are resplendent are beautiful, bright, and impressive in appearance.</p>
<p class='rw-defn idx11' style='display:none'>A resurgence is a rising again or comeback of something.</p>
<p class='rw-defn idx12' style='display:none'>A scintillating conversation, speech, or performance is brilliantly clever, interesting, and lively.</p>
<p class='rw-defn idx13' style='display:none'>A slovenly person is untidy or messy.</p>
<p class='rw-defn idx14' style='display:none'>If an organization is in a state of solvency, it has enough money to pay its bills and other debts.</p>
<p class='rw-defn idx15' style='display:none'>An environment or character can be sordid—the former dirty, the latter low or base in an immoral or greedy sort of way.</p>
<p class='rw-defn idx16' style='display:none'>The word squalor describes very dirty and unpleasant conditions that people live or work in, usually due to poverty or neglect.</p>
<p class='rw-defn idx17' style='display:none'>An unkempt person or thing is untidy and has not been kept neat.</p>
<p class='rw-defn idx18' style='display:none'>If you say someone&#8217;s behavior is unseemly, you disapprove of it because it is not in good taste or not suitable for a particular situation.</p>
<p class='rw-defn idx19' style='display:none'>Someone who is woebegone is very sad and filled with grief.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>dilapidated</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='dilapidated#' id='pronounce-sound' path='audio/words/amy-dilapidated'></a>
di-LAP-i-day-tid
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='dilapidated#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='dilapidated#' id='context-sound' path='audio/wordcontexts/brian-dilapidated'></a>
This old house is so <em>dilapidated</em> that I think it may fall down at any moment.  The <em>dilapidated</em> front porch is leaning dangerously to one side, cannot bear any weight, and looks like it might fall apart if I even touch it.  Old houses need to be maintained in order to keep them from becoming <em>dilapidated</em> and falling into disrepair.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean when something is <em>dilapidated</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is more valuable because of its age.
</li>
<li class='choice answer '>
<span class='result'></span>
It is crumbling, old, and beat up.
</li>
<li class='choice '>
<span class='result'></span>
It is different from everything around it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='dilapidated#' id='definition-sound' path='audio/wordmeanings/amy-dilapidated'></a>
A <em>dilapidated</em> building, vehicle, etc. is old, broken-down, and in very bad condition.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>falling apart</em>
</span>
</span>
</div>
<a class='quick-help' href='dilapidated#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='dilapidated#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/dilapidated/memory_hooks/3048.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Dated</span></span> <span class="emp1"><span>Dil</span></span>l <span class="emp2"><span>Pi</span></span>ckles</span></span> The <span class="emp3"><span>dated</span></span> <span class="emp1"><span>dil</span></span>l <span class="emp2"><span>pi</span></span>ckles were so old that even the <span class="emp2"><span>pi</span></span>ckling process could no longer prevent them from becoming <span class="emp1"><span>dil</span></span>a<span class="emp2"><span>pi</span></span><span class="emp3"><span>dated</span></span> and falling apart.
</p>
</div>

<div id='memhook-button-bar'>
<a href="dilapidated#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/dilapidated/memory_hooks">Use other hook</a>
<a href="dilapidated#" id="memhook-use-own" url="https://membean.com/mywords/dilapidated/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='dilapidated#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
More than a quarter of schools have been forced to close <b>dilapidated</b> buildings, and 18.7 per cent have had pupils or staff suffering from illness or injury linked to poor conditions, according to the report by the National Union of Teachers.
<cite class='attribution'>
&mdash;
The Independent
</cite>
</li>
<li>
Public housing in New York City has become synonymous with the <b>dilapidated</b> living conditions many of its more than 400,000 residents have endured in recent years.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Though Brown left Alabama at 18, he frequently returned to visit relatives and was in the process of buying a <b>dilapidated</b> house in Beulah for use as a studio and residence when he died.
<cite class='attribution'>
&mdash;
FOX News
</cite>
</li>
<li>
Workers are cataloging the tens of thousands of abandoned and <b>dilapidated</b> buildings in Detroit.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/dilapidated/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='dilapidated#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='di_apart' data-tree-url='//cdn1.membean.com/public/data/treexml' href='dilapidated#'>
<span class=''></span>
di-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>apart, not, away from</td>
</tr>
<tr>
<td class='partform'>lapid</td>
<td>
&rarr;
</td>
<td class='meaning'>stone</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ed_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='dilapidated#'>
<span class=''></span>
-ed
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>having a particular state</td>
</tr>
</table>
<p>The idea here is twofold: either a <em>dilapidated</em> building looks like someone has thrown &#8220;stones away from&#8221; herself against the building in order to ruin it, or if the &#8220;stones&#8221; from which a building is made come &#8220;apart,&#8221; it becomes <em>dilapidated</em> as well.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='dilapidated#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>The Money Pit</strong><span> This dilapidated home is just full of surprises!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/dilapidated.jpg' video_url='examplevids/dilapidated' video_width='350'></span>
<div id='wt-container'>
<img alt="Dilapidated" height="288" src="https://cdn1.membean.com/video/examplevids/dilapidated.jpg" width="350" />
<div class='center'>
<a href="dilapidated#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='dilapidated#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Dilapidated" src="https://cdn3.membean.com/public/images/wordimages/cons2/dilapidated.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='dilapidated#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>antiquated</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>decrepitude</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>derelict</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>slovenly</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>sordid</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>squalor</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>unkempt</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>unseemly</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>woebegone</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>amenity</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>commodious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>dapper</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>fortification</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>impeccable</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>rehabilitate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>renovate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>resplendent</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>resurgence</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>scintillating</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>solvency</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="dilapidated" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>dilapidated</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="dilapidated#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

