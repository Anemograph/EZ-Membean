
<!DOCTYPE html>
<html>
<head>
<title>Word: denude | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>When you apportion something, such as blame or money, you decide how it should be shared among various people.</p>
<p class='rw-defn idx2' style='display:none'>Atrophy is a process by which parts of the body, such as muscles and organs, lessen in size or weaken in strength due to internal nerve damage, poor nutrition, or aging.</p>
<p class='rw-defn idx3' style='display:none'>Something austere is simple and plain in its style; an austere person is strict and severe with themselves.</p>
<p class='rw-defn idx4' style='display:none'>When you bedeck something, you decorate it in a showy way.</p>
<p class='rw-defn idx5' style='display:none'>Something is a blight if it spoils or damages things severely; blight is often used to describe something that ruins or kills crops.</p>
<p class='rw-defn idx6' style='display:none'>A copious amount of something is a very large amount of it.</p>
<p class='rw-defn idx7' style='display:none'>A cornucopia is a large quantity and variety of something good and nourishing.</p>
<p class='rw-defn idx8' style='display:none'>When there is a dearth of something, there is a scarcity or lack of it.</p>
<p class='rw-defn idx9' style='display:none'>Someone defoliates a tree or plant by removing its leaves, usually by applying a chemical agent.</p>
<p class='rw-defn idx10' style='display:none'>When an area is devoid of life, it is empty or completely lacking in it.</p>
<p class='rw-defn idx11' style='display:none'>When something is diffuse, it is widely spread out and scattered.</p>
<p class='rw-defn idx12' style='display:none'>When you dismantle something, you take it apart or destroy it piece by piece. </p>
<p class='rw-defn idx13' style='display:none'>If you divest someone of power, rights, or authority, you take those things away from them.</p>
<p class='rw-defn idx14' style='display:none'>If you embellish something, you make it more beautiful by decorating it.</p>
<p class='rw-defn idx15' style='display:none'>When you eradicate something, you tear it up by the roots or remove it completely.</p>
<p class='rw-defn idx16' style='display:none'>If someone, such as a writer, is described as having fecundity, they have the ability to produce many original and different ideas.</p>
<p class='rw-defn idx17' style='display:none'>If something, such as an idea or plan, comes to fruition, it produces the result you wanted to achieve from it.</p>
<p class='rw-defn idx18' style='display:none'>To imbue someone with a quality, such as an emotion or idea, is to fill that person completely with it.</p>
<p class='rw-defn idx19' style='display:none'>Something luxuriant, such as plants or hair, is growing well and is very healthy.</p>
<p class='rw-defn idx20' style='display:none'>When you obliterate something, you destroy it to such an extent that there is nothing or very little of it left.</p>
<p class='rw-defn idx21' style='display:none'>Something that is opulent is very impressive, grand, and is made from expensive materials.</p>
<p class='rw-defn idx22' style='display:none'>A paucity of something is not enough of it.</p>
<p class='rw-defn idx23' style='display:none'>If you suffer privation, you live without many of the basic things required for a comfortable life.</p>
<p class='rw-defn idx24' style='display:none'>A profusion of something is a very large quantity or variety of it.</p>
<p class='rw-defn idx25' style='display:none'>Something or someone that is prolific is highly fruitful and so produces a lot of something.</p>
<p class='rw-defn idx26' style='display:none'>If something bad, such as crime or disease, is rampant, there is a lot of it—and it is increasing in a way that is difficult to control.</p>
<p class='rw-defn idx27' style='display:none'>When you ravage something, you completely destroy, wreck, or damage it.</p>
<p class='rw-defn idx28' style='display:none'>When you refurbish something, you repair it to improve its appearance or ability to function.</p>
<p class='rw-defn idx29' style='display:none'>If you say that something bad or unpleasant is rife somewhere, you mean that it is very common.</p>
<p class='rw-defn idx30' style='display:none'>If you have good sartorial taste, you know what to wear to your best advantage, or you have the skills of a tailor.</p>
<p class='rw-defn idx31' style='display:none'>If something, such as warmth, color, or liquid, suffuses a material, it gradually spreads through or covers it; if you are suffused with a feeling, you are full of that feeling.</p>
<p class='rw-defn idx32' style='display:none'>Something that is sumptuous is impressive, grand, and very expensive.</p>
<p class='rw-defn idx33' style='display:none'>Something that is vacuous is empty or blank, such as a mind or stare.</p>
<p class='rw-defn idx34' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>denude</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='denude#' id='pronounce-sound' path='audio/words/amy-denude'></a>
di-NOOD
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='denude#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='denude#' id='context-sound' path='audio/wordcontexts/brian-denude'></a>
The process that <em><em>denudes</em></em> or strips the tropical rain forests of their trees accelerates global warming. This <em><em>denuding</em></em> or removal of trees means that fewer of them are available to consume carbon dioxide, one of the leading gasses that causes Earth to warm at a more rapid pace. The <em><em>denuding</em></em> of these forests by mining and timber companies uncovers the land, making it bare of trees, which concurrently destroys natural habitats. Once these lush forests are laid to waste and <em><em>denuded</em></em>, they may never return to their original abundance, leaving the land permanently uncovered and exposed.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Why might someone <em>denude</em> a hillside?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
So that construction can begin there once all the vegetation is removed.
</li>
<li class='choice '>
<span class='result'></span>
So that the hillside will not wash away in the rain once new trees are planted there.
</li>
<li class='choice '>
<span class='result'></span>
So that no one will be able to develop the area once legal protection is placed on it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='denude#' id='definition-sound' path='audio/wordmeanings/amy-denude'></a>
To <em>denude</em> an area is to remove the plants and trees that cover it; it can also mean to make something bare.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>uncover</em>
</span>
</span>
</div>
<a class='quick-help' href='denude#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='denude#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/denude/memory_hooks/4683.json'></span>
<p>
<img alt="Denude" src="https://cdn3.membean.com/public/images/wordimages/hook/denude.jpg?qdep8" />
<span class="emp0"><span>Streaking D<span class="emp2"><span>ude</span></span> Renewed</span></span> In the 1970s it was a big thing in the United States to go "streaking," that is, to run out in public totally <span class="emp2"><span>nude</span></span>; a d<span class="emp2"><span>ude</span></span> de<span class="emp2"><span>nude</span></span>d himself of clothes in our town just the other day, so the de<span class="emp2"><span>nude</span></span>d bare-naked d<span class="emp2"><span>ude</span></span> was renewed!
</p>
</div>

<div id='memhook-button-bar'>
<a href="denude#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/denude/memory_hooks">Use other hook</a>
<a href="denude#" id="memhook-use-own" url="https://membean.com/mywords/denude/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='denude#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
“Southwestern desert ecosystems did not evolve with large, slow-moving herbivorous grazers like livestock,” said Joe Trudeau, Southwest Advocate at the Center for Biological Diversity, a nonprofit environmental organization. Trudeau said indigenous wildlife, like deer and pronghorn, move more quickly across the landscape and don’t <b>denude</b> it in the way cows do.
<cite class='attribution'>
&mdash;
The Arizona Republic
</cite>
</li>
<li>
Known as Queensland poplar (Homalanthus populneus), it grows into a large shrub or small tree. . . . Emerging foliage is pink to copper and soon takes on a pleasant green color that is somewhere between emerald and lime. Leaf drop occurs throughout the year but only a freeze will completely <b>denude</b> it of foliage.
<cite class='attribution'>
&mdash;
The San Bernardino Sun 
</cite>
</li>
<li>
Kenya is battling its worst desert locust outbreak in 70 years, and the infestation has spread through much of the eastern part of the continent and the Horn of Africa, razing pasture and croplands in Somalia and Ethiopia and sweeping into South Sudan, Djibouti, Uganda and Tanzania. . . . Given how quickly locusts can <b>denude</b> an entire landscape, there’s also fear they pose a serious threat to large herbivores in Kenya.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Al Jacobs, a resident of Cresswind, which is directly across Farrow Boulevard from the ITAP, said he wasn’t concerned about most development there because the area’s proximity to the airport limits the size of buildings. “As far as businesses go over there, it’s far enough away from us . . . that it shouldn’t really have any impact, as long as they don’t <b>denude</b> the area and take all the trees down,” he said.
<cite class='attribution'>
&mdash;
The Sun News
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/denude/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='denude#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='de_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='denude#'>
<span class='common'></span>
de-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td class='partform'>nud</td>
<td>
&rarr;
</td>
<td class='meaning'>unclothed, bare</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='denude#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>If a mannequin is <em>denuded</em>, it becomes &#8220;thoroughly unclothed or bare.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='denude#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Conservation International</strong><span> Aerial surveying of the denuding of Madagascar's forests.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/denude.jpg' video_url='examplevids/denude' video_width='350'></span>
<div id='wt-container'>
<img alt="Denude" height="288" src="https://cdn1.membean.com/video/examplevids/denude.jpg" width="350" />
<div class='center'>
<a href="denude#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='denude#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Denude" src="https://cdn3.membean.com/public/images/wordimages/cons2/denude.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='denude#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>atrophy</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>austere</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>blight</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>dearth</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>defoliate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>devoid</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>dismantle</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>divest</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>eradicate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>obliterate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>paucity</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>privation</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>ravage</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>vacuous</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>apportion</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>bedeck</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>copious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>cornucopia</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>diffuse</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>embellish</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>fecundity</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>fruition</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>imbue</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>luxuriant</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>opulent</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>profusion</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>prolific</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>rampant</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>refurbish</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>rife</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>sartorial</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>suffuse</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>sumptuous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="denude" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>denude</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="denude#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

