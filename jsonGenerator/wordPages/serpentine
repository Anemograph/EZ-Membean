
<!DOCTYPE html>
<html>
<head>
<title>Word: serpentine | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Serpentine-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/serpentine-large.jpg?qdep8" />
</div>
<a href="serpentine#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx1' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx2' style='display:none'>A circuitous route, journey, or piece of writing is long and complicated rather than simple and direct.</p>
<p class='rw-defn idx3' style='display:none'>Something convoluted, such as a difficult concept or procedure, is complex and takes many twists and turns.</p>
<p class='rw-defn idx4' style='display:none'>Something that is desultory is done in a way that is unplanned, disorganized, and without direction.</p>
<p class='rw-defn idx5' style='display:none'>When someone&#8217;s behavior is deviating, they do things differently by departing or straying from their usual way of acting.</p>
<p class='rw-defn idx6' style='display:none'>Errant things or people behave in a way that is unacceptable or wrong, such as a missile that travels in the wrong direction or a student who does not follow classroom rules.</p>
<p class='rw-defn idx7' style='display:none'>The word homogeneous is used to describe a group that has members or parts which are similar or are all of the same type.</p>
<p class='rw-defn idx8' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx9' style='display:none'>If you describe a situation or process as labyrinthine, you mean that it is very complicated, involved, and difficult to understand.</p>
<p class='rw-defn idx10' style='display:none'>If a dancer is lissome, she moves gracefully and is very flexible.</p>
<p class='rw-defn idx11' style='display:none'>Someone with a lithe body can move easily and gracefully.</p>
<p class='rw-defn idx12' style='display:none'>When you meander, you either wander about with no particular goal or follow a path that twists and turns a great deal.</p>
<p class='rw-defn idx13' style='display:none'>A mendacious person does not tell the truth.</p>
<p class='rw-defn idx14' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx15' style='display:none'>If an object oscillates, it moves repeatedly from one point to another and then back again; if you oscillate between two moods or attitudes, you keep changing from one to the other.</p>
<p class='rw-defn idx16' style='display:none'>If something plummets, it falls down from a high position very quickly; for example, a piano can plummet from a window and a stock value can plummet during a market crash.</p>
<p class='rw-defn idx17' style='display:none'>A precipitous cliff or drop is very steep or falls sharply.</p>
<p class='rw-defn idx18' style='display:none'>If you prevaricate, you avoid giving a direct or honest answer, usually because you want to hide the truth or want to delay or avoid making a hard decision.</p>
<p class='rw-defn idx19' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx20' style='display:none'>You show rectitude if you behave or conduct yourself in an honest and morally correct manner.</p>
<p class='rw-defn idx21' style='display:none'>Something that is sinuous is shaped or moves like a snake, having many smooth twists and turns that can often be highly graceful.</p>
<p class='rw-defn idx22' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx23' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx24' style='display:none'>Something that is tortuous, such as a piece of writing, is long and complicated with many twists and turns in direction; a tortuous argument can be deceitful because it twists or turns the truth.</p>
<p class='rw-defn idx25' style='display:none'>Something that undulates moves or is shaped like waves with gentle curves that smoothly rise and fall.</p>
<p class='rw-defn idx26' style='display:none'>If you waver, you cannot decide between two things because you have serious doubts about which choice is better.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
Middle/High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>serpentine</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='serpentine#' id='pronounce-sound' path='audio/words/amy-serpentine'></a>
SUR-puhn-tahyn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='serpentine#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='serpentine#' id='context-sound' path='audio/wordcontexts/brian-serpentine'></a>
The University of Virginia has beautiful <em>serpentine</em> or curved walls.  Thomas Jefferson built these <em>serpentine</em> or winding walls that form a repeated s shape to save on bricks.  The added benefit is that the <em>serpentine</em> or snakelike structure is beautiful and sturdy.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of something with a <em>serpentine</em> form?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
The Amazon rainforest that spreads out over almost three million square miles of South America.
</li>
<li class='choice '>
<span class='result'></span>
The Golden Gate Bridge that connects the San Francisco Bay to the Pacific Ocean.
</li>
<li class='choice answer '>
<span class='result'></span>
The Great Wall of China that winds through the countryside of China.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='serpentine#' id='definition-sound' path='audio/wordmeanings/amy-serpentine'></a>
A <em>serpentine</em> figure has a winding or twisting form, much like that of a slithering snake.
</li>
<li class='def-text'>
A <em>serpentine</em> personality is crafty and sly.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>snakelike</em>
</span>
</span>
</div>
<a class='quick-help' href='serpentine#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='serpentine#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/serpentine/memory_hooks/5197.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Serpent</span></span> L<span class="emp3"><span>ine</span></span></span></span> A <span class="emp1"><span>serpent</span></span><span class="emp3"><span>ine</span></span> figure is drawn with l<span class="emp3"><span>ine</span></span>s like a <span class="emp1"><span>serpent</span></span> crawls.
</p>
</div>

<div id='memhook-button-bar'>
<a href="serpentine#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/serpentine/memory_hooks">Use other hook</a>
<a href="serpentine#" id="memhook-use-own" url="https://membean.com/mywords/serpentine/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='serpentine#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
In this emerald land high above the clouds in the Talysh Mountains, reached by loop after loop of a <b>serpentine</b> road, people seem to have discovered a secret to a long and healthy life.
<cite class='attribution'>
&mdash;
CNN Travel
</cite>
</li>
<li>
An ADA-accessible ramp allows wheelchairs and strollers to meander the <b>serpentine</b> path and enjoy the immersive experience of being among the pine trees.
<cite class='attribution'>
&mdash;
Architect Magazine
</cite>
</li>
<li>
Few details emerged from the secretive talks, held at a remote rural resort in Taiwan, where protesters wearing pointed straw farmer’s hats scuffled with police along a barricaded, wooded <b>serpentine</b> road.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
"Strange and unusual facts," Tom Waits says, flipping through the pages of a crumpled notebook filled with bursts of <b>serpentine</b> scrawl that he has pulled from his back pocket.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/serpentine/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='serpentine#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep5" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>serpent</td>
<td>
&rarr;
</td>
<td class='meaning'>snake, serpent</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ine_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='serpentine#'>
<span class=''></span>
-ine
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p><em>Serpentine</em> motion is &#8220;of or relating to a snake.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='serpentine#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep5" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Trollstigen Norway: Famous Serpetine Mountain Road, Der Trollstigen in Norwegen</strong><span> Imagine having to drive along this serpentine road!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/serpentine.jpg' video_url='examplevids/serpentine' video_width='350'></span>
<div id='wt-container'>
<img alt="Serpentine" height="288" src="https://cdn1.membean.com/video/examplevids/serpentine.jpg" width="350" />
<div class='center'>
<a href="serpentine#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='serpentine#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep5" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Serpentine" src="https://cdn2.membean.com/public/images/wordimages/cons2/serpentine.png?qdep5" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='serpentine#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>circuitous</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>convoluted</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>desultory</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>deviate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>errant</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>labyrinthine</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>lissome</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>lithe</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>meander</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>mendacious</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>oscillate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>prevaricate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>sinuous</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>tortuous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>undulate</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>waver</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>homogeneous</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>plummet</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>precipitous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>rectitude</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="serpentine" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>serpentine</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="serpentine#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

