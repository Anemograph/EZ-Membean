
<!DOCTYPE html>
<html>
<head>
<title>Word: egress | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>To cloister someone is to remove and isolate them from the rest of the world.</p>
<p class='rw-defn idx1' style='display:none'>Both light and heat emanate or come forth from a wood stove.</p>
<p class='rw-defn idx2' style='display:none'>If you ensconce yourself somewhere, you put yourself into a comfortable place or safe position and have no intention of moving or leaving for quite some time.</p>
<p class='rw-defn idx3' style='display:none'>If you exude a quality or feeling, people easily notice that you have a large quantity of it because it flows from you; if a smell or liquid exudes from something, it flows steadily and slowly from it.</p>
<p class='rw-defn idx4' style='display:none'>If you incarcerate someone, you put them in prison or jail.</p>
<p class='rw-defn idx5' style='display:none'>Ingress is the action of entering a place, the entrance itself, or the right or permission to enter a place.</p>
<p class='rw-defn idx6' style='display:none'>When someone is described as itinerant, they are characterized by traveling or moving about from place to place.</p>
<p class='rw-defn idx7' style='display:none'>If someone is ostracized from a group, its members deliberately refuse to talk or listen to them and do not allow them to take part in any of their social activities.</p>
<p class='rw-defn idx8' style='display:none'>A pariah is a social outcast.</p>
<p class='rw-defn idx9' style='display:none'>When people or animals are quarantined, they are put in isolation so that they will not spread disease.</p>
<p class='rw-defn idx10' style='display:none'>Retention is the act or condition of keeping or holding on to something, including the ability to remember things.</p>
<p class='rw-defn idx11' style='display:none'>If you sequester someone, you keep them separate from other people.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>egress</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='egress#' id='pronounce-sound' path='audio/words/amy-egress'></a>
EE-gres
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='egress#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='egress#' id='context-sound' path='audio/wordcontexts/brian-egress'></a>
Large gates at either end of the gardens witness an <em>egress</em> or the going out of satisfied visitors as they leave the gardens at sunset each day.  The designers of the gardens feel that the finer points of departure or <em>egress</em> are often overlooked in landscape design.  They want visitors to feel, as they make their exit or <em>egress</em>, that even their leave taking is part of their overall positive experience.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an <em>egress</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A utilitarian exit.
</li>
<li class='choice '>
<span class='result'></span>
An intricate garden.
</li>
<li class='choice '>
<span class='result'></span>
A beautiful bird.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='egress#' id='definition-sound' path='audio/wordmeanings/amy-egress'></a>
<em>Egress</em> is the act of going out or exiting from an enclosed space; it can also be the exit itself.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>exit</em>
</span>
</span>
</div>
<a class='quick-help' href='egress#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='egress#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/egress/memory_hooks/5777.json'></span>
<p>
<img alt="Egress" src="https://cdn2.membean.com/public/images/wordimages/hook/egress.jpg?qdep8" />
<span class="emp0"><span><span class="emp3"><span>Egre</span></span>t <span class="emp3"><span>Egre</span></span>ss -> Migration Only!</span></span> Let's hope that the snowy <span class="emp3"><span>egre</span></span>t never makes a permanent <span class="emp3"><span>egre</span></span>ss from this planet--and that the <span class="emp3"><span>egre</span></span>t continues its yearly <span class="emp3"><span>egre</span></span>ss as it happily migrates.
</p>
</div>

<div id='memhook-button-bar'>
<a href="egress#" id="add-public-hook" style="" url="https://membean.com/mywords/egress/memory_hooks">Use other public hook</a>
<a href="egress#" id="memhook-use-own" url="https://membean.com/mywords/egress/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='egress#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Fisker says it chose [butterfly doors] to aid ingress and <b>egress</b>, to what’s likely a low-slung car.
<cite class='attribution'>
&mdash;
Christian Science Monitor
</cite>
</li>
<li>
In an interview with "Good Morning America," Dolphins president Tom Garfinkel outlined a number of ways in which attending football games might look different for the time being, including new processes for ingress and <b>egress</b>, cleanliness measures for the building and a system for concessions where food would be ordered from a seat and picked up rather than people waiting in lines.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
This is a shorthand method of evaluating the efficacy of <b>egress</b> routes, according to emergency planning experts. To evaluate exit routes for Californians living in areas at risk of a fire-related evacuation, we combined and analyzed data from the U.S. Census Bureau, Cal Fire and OpenStreetMap.
<cite class='attribution'>
&mdash;
Associated Press
</cite>
</li>
<li>
Who will greet me in the lobby, should I ever gain access—a human whose job is to handle ingress and <b>egress</b>, or is each person expected to greet their own visitors?
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/egress/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='egress#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_out' data-tree-url='//cdn1.membean.com/public/data/treexml' href='egress#'>
<span class='common'></span>
e-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>out of, from</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='gress_step' data-tree-url='//cdn1.membean.com/public/data/treexml' href='egress#'>
<span class='common'></span>
gress
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>having stepped, having moved</td>
</tr>
</table>
<p><em>Egress</em> is &#8220;having stepped or moved out of or from&#8221; a place.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='egress#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Egress" src="https://cdn2.membean.com/public/images/wordimages/cons2/egress.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='egress#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>emanate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>exude</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>itinerant</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>ostracize</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>pariah</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>cloister</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>ensconce</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>incarcerate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>ingress</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>quarantine</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>retention</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>sequester</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="egress" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>egress</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="egress#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

