
<!DOCTYPE html>
<html>
<head>
<title>Word: coagulate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Coagulate-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/coagulate-large.jpg?qdep8" />
</div>
<a href="coagulate#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>The process of ablation is the removal of diseased organs or harmful substances from the body, often through surgical procedure.</p>
<p class='rw-defn idx1' style='display:none'>Accretion is the slow, gradual process by which new things are added and something gets bigger.</p>
<p class='rw-defn idx2' style='display:none'>An adherent is a supporter or follower of a leader or a cause.</p>
<p class='rw-defn idx3' style='display:none'>To agglomerate a group of things is to gather them together without any noticeable order.</p>
<p class='rw-defn idx4' style='display:none'>An aggregate is the final or sum total after many different amounts or scores have been added together.</p>
<p class='rw-defn idx5' style='display:none'>When two or more things, such as organizations, amalgamate, they combine to become one large thing.</p>
<p class='rw-defn idx6' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx7' style='display:none'>If two or more things coalesce, they come together to form a single larger unit.</p>
<p class='rw-defn idx8' style='display:none'>A coalition is a temporary union of different political or social groups that agrees to work together to achieve a shared aim.</p>
<p class='rw-defn idx9' style='display:none'>When a liquid congeals, it becomes very thick and sticky, almost like a solid.</p>
<p class='rw-defn idx10' style='display:none'>When something is diffuse, it is widely spread out and scattered.</p>
<p class='rw-defn idx11' style='display:none'>To disseminate something, such as knowledge or information, is to distribute it so that it reaches a lot of people.</p>
<p class='rw-defn idx12' style='display:none'>The juxtaposition of two objects is the act of positioning them side by side so that the differences between them are more readily visible.</p>
<p class='rw-defn idx13' style='display:none'>Liaison is the cooperation and exchange of information between people or organizations in order to facilitate their joint efforts; this word also refers to a person who coordinates a connection between people or organizations.</p>
<p class='rw-defn idx14' style='display:none'>A medley is a collection of various things, such as different tunes in a song or different types of food in a meal.</p>
<p class='rw-defn idx15' style='display:none'>A schism causes a group or organization to divide into two groups as a result of differences in their aims and beliefs.</p>
<p class='rw-defn idx16' style='display:none'>Sporadic occurrences happen from time to time but not at constant or regular intervals.</p>
<p class='rw-defn idx17' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Verb</li>
<li>
Level 5
<br>
<small>
Middle/High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>coagulate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='coagulate#' id='pronounce-sound' path='audio/words/amy-coagulate'></a>
koh-AG-yuh-layt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='coagulate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='coagulate#' id='context-sound' path='audio/wordcontexts/brian-coagulate'></a>
Gina&#8217;s father taught her how to make jam by cooking fresh berries and sugar slowly on low heat to help them thicken or <em>coagulate</em> into the correct thick consistency.  Adding pectin to the fruit mixture also helped it to <em>coagulate</em> or grow more solid.  After a few hours, Gina could see the syrupy liquid in the pot <em>coagulate</em> into firm, thick, concentrated, and spreadable jam.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What has happened to a liquid that has <em>coagulated</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It has been mixed with another liquid to form a new substance.
</li>
<li class='choice answer '>
<span class='result'></span>
It has thickened so much that it has become more solid than liquid.
</li>
<li class='choice '>
<span class='result'></span>
It has boiled for so long that most of it has evaporated, having become a gas.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='coagulate#' id='definition-sound' path='audio/wordmeanings/amy-coagulate'></a>
If liquid <em>coagulates</em>, it becomes thick and solid.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>thicken</em>
</span>
</span>
</div>
<a class='quick-help' href='coagulate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='coagulate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/coagulate/memory_hooks/3342.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>G</span></span>ho<span class="emp1"><span>ul</span></span> <span class="emp3"><span>Eat</span></span>s <span class="emp2"><span>Co</span></span>mpanion</span></span> As the horrifying <span class="emp1"><span>g</span></span>ho<span class="emp1"><span>ul</span></span> <span class="emp3"><span>ate</span></span> my <span class="emp2"><span>co</span></span>mpanion, my blood <span class="emp2"><span>co</span></span>a<span class="emp1"><span>gul</span></span><span class="emp3"><span>ate</span></span>d in frozen fear, for I knew that the <span class="emp1"><span>g</span></span>ho<span class="emp1"><span>ul</span></span>'s m<span class="emp3"><span>ate</span></span> <span class="emp3"><span>ate</span></span> humans as well.
</p>
</div>

<div id='memhook-button-bar'>
<a href="coagulate#" id="add-public-hook" style="" url="https://membean.com/mywords/coagulate/memory_hooks">Use other public hook</a>
<a href="coagulate#" id="memhook-use-own" url="https://membean.com/mywords/coagulate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='coagulate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The yolk is the part of the egg that's going to <b>coagulate</b> and hold all the custard ingredients together.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
He advised avoiding anything with high-fat gravies and sauces that will likely <b>coagulate</b> as they cool.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/coagulate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='coagulate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='co_with' data-tree-url='//cdn1.membean.com/public/data/treexml' href='coagulate#'>
<span class=''></span>
co-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>with, together</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ag_do' data-tree-url='//cdn1.membean.com/public/data/treexml' href='coagulate#'>
<span class=''></span>
ag
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>do, act, drive</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ate_quality' data-tree-url='//cdn1.membean.com/public/data/treexml' href='coagulate#'>
<span class=''></span>
-ate
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make something have a certain quality</td>
</tr>
</table>
<p>When materials <em>coagulate</em>, they are &#8220;driven together.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='coagulate#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Thrombosis Adviser</strong><span> How certain parts of the blood coagulate to seal a wound.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/coagulate.jpg' video_url='examplevids/coagulate' video_width='350'></span>
<div id='wt-container'>
<img alt="Coagulate" height="288" src="https://cdn1.membean.com/video/examplevids/coagulate.jpg" width="350" />
<div class='center'>
<a href="coagulate#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='coagulate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Coagulate" src="https://cdn1.membean.com/public/images/wordimages/cons2/coagulate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='coagulate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>accretion</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>adherent</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>agglomerate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>aggregate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>amalgamate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>coalesce</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>coalition</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>congeal</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>juxtaposition</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>liaison</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>medley</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>ablation</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>diffuse</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>disseminate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>schism</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>sporadic</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="coagulate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>coagulate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="coagulate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

