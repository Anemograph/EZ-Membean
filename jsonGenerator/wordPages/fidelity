
<!DOCTYPE html>
<html>
<head>
<title>Word: fidelity | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you have an affiliation with a group or another person, you are officially involved or connected with them.</p>
<p class='rw-defn idx1' style='display:none'>When you act in an aloof fashion towards others, you purposely do not participate in activities with them; instead, you remain distant and detached.</p>
<p class='rw-defn idx2' style='display:none'>Antipathy is a strong feeling of dislike or hostility towards someone or something.</p>
<p class='rw-defn idx3' style='display:none'>An apostate has abandoned their religious faith, political party, or devotional cause.</p>
<p class='rw-defn idx4' style='display:none'>When you have ardor for something, you have an intense feeling of love, excitement, and admiration for it.</p>
<p class='rw-defn idx5' style='display:none'>An assiduous person works hard to ensure that something is done properly and completely.</p>
<p class='rw-defn idx6' style='display:none'>A cadre is a group specifically trained to lead, formalize, and accomplish a particular task or to train others as part of a larger organization; it can also be used to describe an elite military force.</p>
<p class='rw-defn idx7' style='display:none'>You describe someone as a charlatan if they pretend to have special knowledge or skill that they don&#8217;t actually possess.</p>
<p class='rw-defn idx8' style='display:none'>If you employ chicanery, you are devising and carrying out clever plans and trickery to cheat and deceive people.</p>
<p class='rw-defn idx9' style='display:none'>A coalition is a temporary union of different political or social groups that agrees to work together to achieve a shared aim.</p>
<p class='rw-defn idx10' style='display:none'>A confidant is a close and trusted friend to whom you can tell secret matters of importance and remain assured that they will be kept safe.</p>
<p class='rw-defn idx11' style='display:none'>The adjective conjugal refers to marriage or the relationship between two married people.</p>
<p class='rw-defn idx12' style='display:none'>When you consign someone into another&#8217;s care, you sign them over or entrust them to that person&#8217;s protection.</p>
<p class='rw-defn idx13' style='display:none'>A coterie is a small group of people who are close friends; therefore, when its members do things together, they do not want other people to join them.</p>
<p class='rw-defn idx14' style='display:none'>Credence is the mental act of believing or accepting that something is true.</p>
<p class='rw-defn idx15' style='display:none'>A disaffected member of a group or organization is not satisfied with it; consequently, they feel little loyalty towards it.</p>
<p class='rw-defn idx16' style='display:none'>Someone who is disingenuous is not straightforward or is dishonest in what they say or do.</p>
<p class='rw-defn idx17' style='display:none'>A dissident is someone who disagrees publicly with a government, especially in a country where this is not allowed.</p>
<p class='rw-defn idx18' style='display:none'>If you accuse someone of duplicity, you think that they are dishonest and are intending to trick you.</p>
<p class='rw-defn idx19' style='display:none'>Erratic behavior is irregular, unpredictable, and unusual.</p>
<p class='rw-defn idx20' style='display:none'>If you espouse an idea, principle, or belief, you support it wholeheartedly.</p>
<p class='rw-defn idx21' style='display:none'>A small group or issue that is factious is controversial and promotes strong disagreement or dissatisfaction within a larger group.</p>
<p class='rw-defn idx22' style='display:none'>A fiduciary relationship arises when one individual has the trust and confidence of another, such as a financial client; subsequently, that trustworthy person is entrusted with the management or protection of the client&#8217;s money.</p>
<p class='rw-defn idx23' style='display:none'>Things that fluctuate vary or change often, rising or falling seemingly at random.</p>
<p class='rw-defn idx24' style='display:none'>To foment is to encourage people to protest, fight, or cause trouble and violent opposition to something that is viewed by some as undesirable.</p>
<p class='rw-defn idx25' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx26' style='display:none'>If someone is imperturbable, they are always calm and not easily upset or disturbed by any situation, even dangerous ones.</p>
<p class='rw-defn idx27' style='display:none'>Someone who is impulsive tends to do things without thinking about them ahead of time; therefore, their actions can be unpredictable.</p>
<p class='rw-defn idx28' style='display:none'>If someone leaves an indelible impression on you, it will not be forgotten; an indelible mark is permanent or impossible to erase or remove.</p>
<p class='rw-defn idx29' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx30' style='display:none'>A mendacious person does not tell the truth.</p>
<p class='rw-defn idx31' style='display:none'>A mountebank is a smooth-talking, dishonest person who tricks and cheats people.</p>
<p class='rw-defn idx32' style='display:none'>If an object oscillates, it moves repeatedly from one point to another and then back again; if you oscillate between two moods or attitudes, you keep changing from one to the other.</p>
<p class='rw-defn idx33' style='display:none'>Someone who is perfidious is not loyal and cannot be trusted.</p>
<p class='rw-defn idx34' style='display:none'>Piety is being devoted or showing loyalty towards something, especially a religion.</p>
<p class='rw-defn idx35' style='display:none'>If you prevaricate, you avoid giving a direct or honest answer, usually because you want to hide the truth or want to delay or avoid making a hard decision.</p>
<p class='rw-defn idx36' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx37' style='display:none'>You show rectitude if you behave or conduct yourself in an honest and morally correct manner.</p>
<p class='rw-defn idx38' style='display:none'>A scrupulous person takes great care to do everything in an extremely honest, conscientious, and fair manner.</p>
<p class='rw-defn idx39' style='display:none'>Sedition is the act of encouraging people to disobey and oppose the government currently in power.</p>
<p class='rw-defn idx40' style='display:none'>If you are stalwart, you are dependable, sturdy, and firm in what you do or promise.</p>
<p class='rw-defn idx41' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx42' style='display:none'>A subversive act destroys or does great harm to a government or other institution.</p>
<p class='rw-defn idx43' style='display:none'>If you are temperamental, you tend to become easily upset and experience unpredictable mood swings.</p>
<p class='rw-defn idx44' style='display:none'>Something that has the quality of transience lasts for only a short time or is constantly changing.</p>
<p class='rw-defn idx45' style='display:none'>To undermine a plan or endeavor is to weaken it or make it unstable in a gradual but purposeful fashion.</p>
<p class='rw-defn idx46' style='display:none'>If you are unflagging while doing a task, you are untiring when working upon it and do not stop until it is finished.</p>
<p class='rw-defn idx47' style='display:none'>Doing something in unison is doing it all together at one time.</p>
<p class='rw-defn idx48' style='display:none'>When someone vacillates, they go back and forth about a choice or opinion, unable to make a firm decision.</p>
<p class='rw-defn idx49' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx50' style='display:none'>The verity of something is the truth or reality of it.</p>
<p class='rw-defn idx51' style='display:none'>When you are vigilant, you are keenly watchful, careful, or attentive to a task or situation.</p>
<p class='rw-defn idx52' style='display:none'>If you waver, you cannot decide between two things because you have serious doubts about which choice is better.</p>
<p class='rw-defn idx53' style='display:none'>Wiles are clever tricks or cunning schemes that are used to persuade someone to do what you want.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>fidelity</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='fidelity#' id='pronounce-sound' path='audio/words/amy-fidelity'></a>
fi-DEL-i-tee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='fidelity#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='fidelity#' id='context-sound' path='audio/wordcontexts/brian-fidelity'></a>
<em>Fidelity</em> or faithfulness to the boss in this organization is an absolute must.  If you do not show <em>fidelity</em> or loyalty towards her and she finds out about it, you will be immediately fired.  She demands this kind of <em>fidelity</em> or devotion because she believes that no one can or should question what comes from the top.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Who is showing <em>fidelity</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Mrs. Jimenez, who is deciding whether to give your class free time today.
</li>
<li class='choice '>
<span class='result'></span>
Mike, who is accusing you of something you didn&#8217;t do.
</li>
<li class='choice answer '>
<span class='result'></span>
Jasmine, who is keeping a promise to you and your company.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='fidelity#' id='definition-sound' path='audio/wordmeanings/amy-fidelity'></a>
<em>Fidelity</em> towards something, such as a marriage or promise, is faithfulness or loyalty towards it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>faithfulness</em>
</span>
</span>
</div>
<a class='quick-help' href='fidelity#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='fidelity#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/fidelity/memory_hooks/3702.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Fidelity</span></span> Investments</span></span> <span class="emp2"><span>Fidelity</span></span> is an investment company possibly so named because it wants its customers to believe that its primary concern is <span class="emp2"><span>fidelity</span></span> or loyalty towards them in providing top-notch investment vehicles that provide an excellent return on capital.
</p>
</div>

<div id='memhook-button-bar'>
<a href="fidelity#" id="add-public-hook" style="" url="https://membean.com/mywords/fidelity/memory_hooks">Use other public hook</a>
<a href="fidelity#" id="memhook-use-own" url="https://membean.com/mywords/fidelity/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='fidelity#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Many persons have a wrong idea of what constitutes true happiness. It is not attained through self-gratification but through <b>fidelity</b> to a worthy purpose.
<span class='attribution'>&mdash; Helen Keller, American author, educator, and social activist</span>
<img alt="Helen keller, american author, educator, and social activist" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Helen Keller, American author, educator, and social activist.jpg?qdep8" width="80" />
</li>
<li>
Critics agree that Gerwig gracefully walks the tightrope between <b>fidelity</b> to Alcott’s novel and making a movie of her own, but how faithful is the new _Little Women_ to Alcott’s title?
<cite class='attribution'>
&mdash;
Slate
</cite>
</li>
<li>
Most songbirds have the same mate every spring, but this has more to do with site <b>fidelity</b> rather than long-term partnership. . . . Twelve species of seabirds breed in our region, and most of them mate for life. But again, it’s <b>fidelity</b> to territory, rather than to individuals.
<cite class='attribution'>
&mdash;
KQED
</cite>
</li>
<li>
"[Fact checkers at _Sports Illustrated_] find two or three errors a week," says Honor [Fitzpatrick, Chief of Reporters and Research], with her customary <b>fidelity</b> to facts, no matter how unwelcome.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/fidelity/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='fidelity#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='fid_trust' data-tree-url='//cdn1.membean.com/public/data/treexml' href='fidelity#'>
<span class=''></span>
fid
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>trust, faith</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ity_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='fidelity#'>
<span class=''></span>
-ity
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state or quality</td>
</tr>
</table>
<p>Having <em>fidelity</em> towards someone or something shows a &#8220;state or quality of trust or faith&#8221; in them.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='fidelity#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Vericast</strong><span> This man believes that there is a lack of fidelity in today's people.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/fidelity.jpg' video_url='examplevids/fidelity' video_width='350'></span>
<div id='wt-container'>
<img alt="Fidelity" height="288" src="https://cdn1.membean.com/video/examplevids/fidelity.jpg" width="350" />
<div class='center'>
<a href="fidelity#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='fidelity#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Fidelity" src="https://cdn3.membean.com/public/images/wordimages/cons2/fidelity.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='fidelity#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>affiliation</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>ardor</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>assiduous</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>cadre</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>coalition</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>confidant</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>conjugal</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>consign</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>coterie</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>credence</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>espouse</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>fiduciary</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>imperturbable</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>indelible</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>piety</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>rectitude</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>scrupulous</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>stalwart</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>unflagging</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>unison</span> &middot;</li><li data-idx='49' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='50' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li><li data-idx='51' class = 'rw-wordform notlearned'><span>vigilant</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>aloof</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>antipathy</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>apostate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>charlatan</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>chicanery</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>disaffected</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>disingenuous</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>dissident</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>duplicity</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>erratic</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>factious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>fluctuate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>foment</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>impulsive</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>mendacious</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>mountebank</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>oscillate</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>perfidious</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>prevaricate</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>sedition</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>subversive</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>temperamental</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>transience</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>undermine</span> &middot;</li><li data-idx='48' class = 'rw-wordform notlearned'><span>vacillate</span> &middot;</li><li data-idx='52' class = 'rw-wordform notlearned'><span>waver</span> &middot;</li><li data-idx='53' class = 'rw-wordform notlearned'><span>wile</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='fidelity#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
infidelity
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>unfaithfulness</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="fidelity" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>fidelity</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="fidelity#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

