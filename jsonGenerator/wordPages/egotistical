
<!DOCTYPE html>
<html>
<head>
<title>Word: egotistical | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If your behavior or manner is altruistic, you show you care more about other people and their interests than you care about yourself.</p>
<p class='rw-defn idx1' style='display:none'>When a person is speaking or writing in a bombastic fashion, they are very self-centered, extremely showy, and excessively proud.</p>
<p class='rw-defn idx2' style='display:none'>Braggadocio is an excessively proud way of talking about your achievements or possessions that can annoy others.</p>
<p class='rw-defn idx3' style='display:none'>Bravado is behavior that is deliberately intended to show courage or confidence, sometimes actual but usually pretended, in order to impress other people.</p>
<p class='rw-defn idx4' style='display:none'>Bumptious people are annoying because they are too proud of their abilities or opinions—they are full of themselves.</p>
<p class='rw-defn idx5' style='display:none'>Camaraderie is a feeling of friendship and trust among a group of people who have usually known each over a long period of time.</p>
<p class='rw-defn idx6' style='display:none'>A cavalier person does not seem to care about rules, principles, or other people&#8217;s feelings, no matter how dire or serious a situation may be.</p>
<p class='rw-defn idx7' style='display:none'>A chivalrous man behaves in a polite, kind, generous, and honorable way, especially towards women.</p>
<p class='rw-defn idx8' style='display:none'>A man who is debonair wears fashionable clothes and is sophisticated, charming, friendly, and confident.</p>
<p class='rw-defn idx9' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx10' style='display:none'>An egalitarian social position states that all people should be equal or treated in the same way when it comes to economic, political, and social rights.</p>
<p class='rw-defn idx11' style='display:none'>When someone flaunts their good looks, they show them off or boast about them in a very proud and shameless way.</p>
<p class='rw-defn idx12' style='display:none'>If you think someone is showing hubris, you think that they are demonstrating excessive pride and vanity.</p>
<p class='rw-defn idx13' style='display:none'>If something is inordinate, it is much larger in amount or degree than is normally expected.</p>
<p class='rw-defn idx14' style='display:none'>If someone exhibits magnanimity towards another, they show them kindness and noble generosity, especially after defeating them in battle or after having been treated badly by them.</p>
<p class='rw-defn idx15' style='display:none'>If you describe an action as ostentatious, you think it is an extreme and exaggerated way of impressing people.</p>
<p class='rw-defn idx16' style='display:none'>Someone is overweening when they are not modest; rather, they think way too much of themselves and let everyone know about it.</p>
<p class='rw-defn idx17' style='display:none'>Philanthropy is unselfish support in the form of donating money, work, or gifts to positive social purposes; philanthropy is also overall love for humans in general.</p>
<p class='rw-defn idx18' style='display:none'>If you are pompous, you think that you are better than other people; therefore, you tend to show off and be highly egocentric.</p>
<p class='rw-defn idx19' style='display:none'>When you are presumptuous, you act improperly, rudely, or without respect, especially while attempting to do something that is not socially acceptable or that you are not qualified to do.</p>
<p class='rw-defn idx20' style='display:none'>If you are pretentious, you think you are really great in some way and let everyone know about it, despite the fact that it&#8217;s not the case at all.</p>
<p class='rw-defn idx21' style='display:none'>Someone&#8217;s sardonic smile, expression, or comment shows a lack of respect for what someone else has said or done; this occurs because the former thinks they are too important to consider or discuss such a supposedly trivial matter of the latter.</p>
<p class='rw-defn idx22' style='display:none'>When you strut, you move as though you own the world by walking in a confident and showy fashion.</p>
<p class='rw-defn idx23' style='display:none'>If you behave in a supercilious way, you act as if you were more important or better than everyone else.</p>
<p class='rw-defn idx24' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx25' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>
<p class='rw-defn idx26' style='display:none'>If you are vainglorious, you are very proud of yourself and let other people know about it.</p>
<p class='rw-defn idx27' style='display:none'>Something that is vaunted, such as someone&#8217;s ability, is too highly praised and boasted about.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>egotistical</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='egotistical#' id='pronounce-sound' path='audio/words/amy-egotistical'></a>
ee-guh-TIS-tik-uhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='egotistical#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='egotistical#' id='context-sound' path='audio/wordcontexts/brian-egotistical'></a>
That woman is so <em>egotistical</em>&#8212;she thinks anything and everything is entirely about her!  Her <em>egotistical</em> or self-centered attitude leads her to believe that she is the most important person there is.  Her <em>egotistical</em> and selfish ways have made her lose many friends, for they all get tired of her always talking and bragging only about herself.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How would you describe someone who is <em>egotistical</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
They only care about themselves.
</li>
<li class='choice '>
<span class='result'></span>
They love to learn more about other people. 
</li>
<li class='choice '>
<span class='result'></span>
They easily confuse fact with fiction.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='egotistical#' id='definition-sound' path='audio/wordmeanings/amy-egotistical'></a>
An <em>egotistical</em> person thinks about or is concerned with no one else other than themselves.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>self-centered</em>
</span>
</span>
</div>
<a class='quick-help' href='egotistical#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='egotistical#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/egotistical/memory_hooks/2891.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Eggo</span></span> Fan<span class="emp3"><span>tastical</span></span></span></span> Only an <span class="emp1"><span>ego</span></span><span class="emp3"><span>tistical</span></span> person would believe that his <span class="emp1"><span>Eggo</span></span> is more fan<span class="emp3"><span>tastical</span></span> than anyone else's!
</p>
</div>

<div id='memhook-button-bar'>
<a href="egotistical#" id="add-public-hook" style="" url="https://membean.com/mywords/egotistical/memory_hooks">Use other public hook</a>
<a href="egotistical#" id="memhook-use-own" url="https://membean.com/mywords/egotistical/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='egotistical#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
I don't like to sound <b>egotistical</b>, but every time I stepped up to the plate with a bat in my hands, I couldn't help but feel sorry for the pitcher.
<span class='attribution'>&mdash; Rogers Hornsby, American major league baseball player in the first half of the 20th century</span>
<img alt="Rogers hornsby, american major league baseball player in the first half of the 20th century" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Rogers Hornsby, American major league baseball player in the first half of the 20th century.jpg?qdep8" width="80" />
</li>
<li>
"The problem is, if I do it this way, some of my awards get in the frame," he said, gesturing to a wall hung with gold and platinum plaques. "And I don't want you to think I'm so . . . <b>egotistical</b> that I have to do Zoom calls with awards in 'em."
<cite class='attribution'>
&mdash;
Chicago Tribune
</cite>
</li>
<li>
Contrary to his <b>egotistical</b> and hard-edge moments, Thompson was not an intimidating man, not deep down. That was his armor.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
"Everyone is trying to make political mileage out of this," says the head of one aid organization, speaking on condition of anonymity. "Each side is trying to bend the ear of the donor community, get their hands on the funds, and raise their own status. There is a major human catastrophe at hand, but the political leaders can't put aside their <b>egotistical</b> interests for even a second."
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/egotistical/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='egotistical#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ego_I' data-tree-url='//cdn1.membean.com/public/data/treexml' href='egotistical#'>
<span class=''></span>
ego
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>I</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ic_like' data-tree-url='//cdn1.membean.com/public/data/treexml' href='egotistical#'>
<span class=''></span>
-ic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>nature of, like</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='al_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='egotistical#'>
<span class=''></span>
-al
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>An <em>egotistical</em> person is only interested in things &#8220;relating to the nature of I,&#8221; that is, &#8220;himself.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='egotistical#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Beauty and the Beast</strong><span> No one compares to the egotistical Gaston.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/egotistical.jpg' video_url='examplevids/egotistical' video_width='350'></span>
<div id='wt-container'>
<img alt="Egotistical" height="288" src="https://cdn1.membean.com/video/examplevids/egotistical.jpg" width="350" />
<div class='center'>
<a href="egotistical#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='egotistical#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Egotistical" src="https://cdn1.membean.com/public/images/wordimages/cons2/egotistical.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='egotistical#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>bombastic</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>braggadocio</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bravado</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>bumptious</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>cavalier</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>flaunt</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>hubris</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>inordinate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>ostentatious</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>overweening</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>pompous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>presumptuous</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>pretentious</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>sardonic</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>strut</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>supercilious</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>vainglorious</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>vaunted</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>altruistic</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>camaraderie</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>chivalrous</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>debonair</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>egalitarian</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>magnanimity</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>philanthropy</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='egotistical#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
egocentric
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>self-centered</td>
</tr>
<tr>
<td class='wordform'>
<span>
egotism
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>the belief that the self is the most important thing there is</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="egotistical" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>egotistical</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="egotistical#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

