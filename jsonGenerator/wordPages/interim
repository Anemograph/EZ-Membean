
<!DOCTYPE html>
<html>
<head>
<title>Word: interim | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>The cessation of a process is a stop or halt to it.</p>
<p class='rw-defn idx1' style='display:none'>A chronic illness or pain is serious and lasts for a long time; a chronic problem is always happening or returning and is very difficult to solve.</p>
<p class='rw-defn idx2' style='display:none'>Something ephemeral, such as some insects or a sunset, lasts for only a short time or has a very short lifespan.</p>
<p class='rw-defn idx3' style='display:none'>Something that is evanescent lasts for only a short time before disappearing from sight or memory.</p>
<p class='rw-defn idx4' style='display:none'>Things that fluctuate vary or change often, rising or falling seemingly at random.</p>
<p class='rw-defn idx5' style='display:none'>A hiatus is a period of time when there is a break or interruption in some activity.</p>
<p class='rw-defn idx6' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx7' style='display:none'>Something that is interminable continues for a very long time in a boring or annoying way.</p>
<p class='rw-defn idx8' style='display:none'>An interregnum is a period of time when there is temporarily no one in charge of a country or large organization.</p>
<p class='rw-defn idx9' style='display:none'>An inveterate person is always doing a particular thing, especially something questionable—and they are not likely to stop doing it.</p>
<p class='rw-defn idx10' style='display:none'>A lacuna is an empty space or gap where something is missing; if there is a lacuna in a person&#8217;s argument, for example, part of that argument is lacking.</p>
<p class='rw-defn idx11' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx12' style='display:none'>A moratorium on a particular activity or process is an official agreement to stop it temporarily.</p>
<p class='rw-defn idx13' style='display:none'>Something that is perennial lasts a very long time and is enduring.</p>
<p class='rw-defn idx14' style='display:none'>A provisional measure is temporary or conditional until more permanent action is taken.</p>
<p class='rw-defn idx15' style='display:none'>A regime is the system of government currently in power in a country; it can also be the controlling group or management of an organization.</p>
<p class='rw-defn idx16' style='display:none'>A respite is a short period of rest from work or something troubling.</p>
<p class='rw-defn idx17' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx18' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx19' style='display:none'>A surrogate is someone who temporarily takes the place of another person, usually acting as a representative because that person is not available to or cannot carry out a task.</p>
<p class='rw-defn idx20' style='display:none'>Something that is temporal deals with the present and somewhat brief time of this world.</p>
<p class='rw-defn idx21' style='display:none'>Something that has the quality of transience lasts for only a short time or is constantly changing.</p>
<p class='rw-defn idx22' style='display:none'>If you are unflagging while doing a task, you are untiring when working upon it and do not stop until it is finished.</p>
<p class='rw-defn idx23' style='display:none'>When someone vacillates, they go back and forth about a choice or opinion, unable to make a firm decision.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>interim</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='interim#' id='pronounce-sound' path='audio/words/amy-interim'></a>
IN-ter-uhm
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='interim#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='interim#' id='context-sound' path='audio/wordcontexts/brian-interim'></a>
Since our school just lost its principal, we need an <em>interim</em> or temporary principal to fill the position until we can hire a new head of school.  We have decided that, in the <em>interim</em>, or the time between now and when a new one comes, Mrs. Fisher will take on the duties of the principal.  As the <em>interim</em> or acting administrator, she will have to teach both her regular classes and serve as principal, which will not be an easy thing to accomplish.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an <em>interim</em> position?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
It is held for a short time until someone else is officially hired.
</li>
<li class='choice '>
<span class='result'></span>
It is low-paying and often requires little to no training.
</li>
<li class='choice '>
<span class='result'></span>
It is high-paying and requires a great deal of training and skill.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='interim#' id='definition-sound' path='audio/wordmeanings/amy-interim'></a>
An <em>interim</em> position at a school or business is only temporary; it lasts until the position can be filled permanently.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>temporary</em>
</span>
</span>
</div>
<a class='quick-help' href='interim#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='interim#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/interim/memory_hooks/4957.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Inter</span></span><span class="emp3"><span>im</span></span> to <span class="emp1"><span>Intern</span></span> H<span class="emp3"><span>im</span></span>?</span></span> It would be a huge mistake to give the <span class="emp1"><span>inter</span></span><span class="emp3"><span>im</span></span> position of Chief of Surgery to an <span class="emp1"><span>intern</span></span>, and certainly not to h<span class="emp3"><span>im</span></span> of all people!
</p>
</div>

<div id='memhook-button-bar'>
<a href="interim#" id="add-public-hook" style="" url="https://membean.com/mywords/interim/memory_hooks">Use other public hook</a>
<a href="interim#" id="memhook-use-own" url="https://membean.com/mywords/interim/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='interim#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Dunlap said that after Arizona officials explained the <b>interim</b> situation to him, he understood this year would be "a chapter in a long book."
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
After months of waiting, Mary Tullius has been notified she will be director of the Division of Parks and Recreation. Tullius, who has been <b>interim</b> director the past 14 months, will become Utah's first woman director of the division and one of only six women nationwide holding that position.
<cite class='attribution'>
&mdash;
Deseret News
</cite>
</li>
<li>
Defensive line coach Jim Tomsula was promoted to <b>interim</b> coach and will run the team in the season finale at home against Arizona.
<cite class='attribution'>
&mdash;
ESPN
</cite>
</li>
<li>
The proposal does not detail who would serve in what job, though delegates meeting for a sixth day were hopeful the <b>interim</b> government could be in place for the New Year.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/interim/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='interim#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='inter_between' data-tree-url='//cdn1.membean.com/public/data/treexml' href='interim#'>
<span class='common'></span>
inter-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>between, within, among</td>
</tr>
</table>
<p>An <em>interim</em> is a time &#8220;between&#8221; two greater periods of time.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='interim#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>FNN Reports</strong><span> Joyce Slocum has stepped in as interim CEO for NPR, and will remain until a successor for Vivian Schiller is found.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/interim.jpg' video_url='examplevids/interim' video_width='350'></span>
<div id='wt-container'>
<img alt="Interim" height="198" src="https://cdn1.membean.com/video/examplevids/interim.jpg" width="350" />
<div class='center'>
<a href="interim#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='interim#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Interim" src="https://cdn1.membean.com/public/images/wordimages/cons2/interim.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='interim#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>cessation</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>ephemeral</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>evanescent</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>fluctuate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>hiatus</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>interregnum</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>lacuna</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>moratorium</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>provisional</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>respite</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>surrogate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>temporal</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>transience</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>vacillate</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>chronic</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>interminable</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>inveterate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>perennial</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>regime</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>unflagging</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='interim#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
interim
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>a space of time between a period or event</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="interim" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>interim</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="interim#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

