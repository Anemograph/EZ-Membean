
<!DOCTYPE html>
<html>
<head>
<title>Word: defile | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you absolve someone, you publicly and formally say that they are not guilty or responsible for any wrongdoing.</p>
<p class='rw-defn idx1' style='display:none'>If you adulterate something, you lessen its quality by adding inferior ingredients or elements to it.</p>
<p class='rw-defn idx2' style='display:none'>If you besmirch someone, you spoil their good reputation by saying bad things about them, usually things that you know are not true.</p>
<p class='rw-defn idx3' style='display:none'>Something is a blight if it spoils or damages things severely; blight is often used to describe something that ruins or kills crops.</p>
<p class='rw-defn idx4' style='display:none'>Something brackish is distasteful and unpleasant usually because something else is mixed in with it; brackish water, for instance, is a mixture of fresh and salt water and cannot be drunk.</p>
<p class='rw-defn idx5' style='display:none'>A contagion is a disease—or the transmission of that disease—that is easily spread from one person to another through touch or the air.</p>
<p class='rw-defn idx6' style='display:none'>Something that is deleterious has a harmful effect.</p>
<p class='rw-defn idx7' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx8' style='display:none'>If you desecrate something that is considered holy or very special, you deliberately spoil or damage it.</p>
<p class='rw-defn idx9' style='display:none'>When you execrate someone, you curse them to show your intense dislike or hatred of them.</p>
<p class='rw-defn idx10' style='display:none'>When you pay homage to another person, you show them great admiration or respect; you might even worship them.</p>
<p class='rw-defn idx11' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx12' style='display:none'>A liturgy is a set of rules or formal procedures used during a church service.</p>
<p class='rw-defn idx13' style='display:none'>A malevolent person or thing is evil due to deliberate attempts to cause harm.</p>
<p class='rw-defn idx14' style='display:none'>Malfeasance is an unlawful act, especially one committed by a trusted public official.</p>
<p class='rw-defn idx15' style='display:none'>If you malign someone, you say unpleasant things about them to damage their reputation.</p>
<p class='rw-defn idx16' style='display:none'>Piety is being devoted or showing loyalty towards something, especially a religion.</p>
<p class='rw-defn idx17' style='display:none'>Potable water is clean and safe to drink.</p>
<p class='rw-defn idx18' style='display:none'>Something that is pristine has not lost any of its original perfection or purity.</p>
<p class='rw-defn idx19' style='display:none'>When you ravage something, you completely destroy, wreck, or damage it.</p>
<p class='rw-defn idx20' style='display:none'>When you are reverent, you show a great deal of respect, admiration, or even awe for someone or something.</p>
<p class='rw-defn idx21' style='display:none'>If something is reviled, it is intensely hated and criticized.</p>
<p class='rw-defn idx22' style='display:none'>A sacrilegious act is one of deep disrespect that violates something that is sacred or holy.</p>
<p class='rw-defn idx23' style='display:none'>To taint is to give an undesirable quality that damages a person&#8217;s reputation or otherwise spoils something.</p>
<p class='rw-defn idx24' style='display:none'>When you trammel something or someone, you bring about limits to freedom or hinder movements or activities in some fashion.</p>
<p class='rw-defn idx25' style='display:none'>To come out of something unscathed is to come out of it uninjured.</p>
<p class='rw-defn idx26' style='display:none'>Something that is unsullied is unstained and clean.</p>
<p class='rw-defn idx27' style='display:none'>Venerable people command respect because they are old and wise.</p>
<p class='rw-defn idx28' style='display:none'>If you vilify people, you write or say bad things about them in an attempt to make them unpopular.</p>
<p class='rw-defn idx29' style='display:none'>If someone vitiates something, they make it less effective, weaker, or reduced in quality.</p>
<p class='rw-defn idx30' style='display:none'>Vituperative remarks are full of hate, anger, and cruel criticism.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>defile</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='defile#' id='pronounce-sound' path='audio/words/amy-defile'></a>
DEE-fahyl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='defile#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='defile#' id='context-sound' path='audio/wordcontexts/brian-defile'></a>
In a fit of anger and desperation, the youth <em>defiled</em> and violated the cemetery by knocking over the stones that marked each grave.  This act of <em><em>defilement</em></em> and disgraceful destruction in a holy space shocked and grieved the townspeople.  By <em><em>defiling</em></em> and abusing the gravesites, the youth had released his anger but had also highly offended his neighbors.  That week he confessed to the deed, saying that he was sorry for <em><em>defiling</em></em> and dishonoring the cemetery.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What happens if someone <em>defiles</em> something sacred?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They destroy it completely so it no longer exists.
</li>
<li class='choice answer '>
<span class='result'></span>
They dishonor it by saying something negative about it.
</li>
<li class='choice '>
<span class='result'></span>
They remove it from public view to keep it from being harmed.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='defile#' id='definition-sound' path='audio/wordmeanings/amy-defile'></a>
To <em>defile</em> something holy or important is to do something to it or say something about it that is highly offensive to others.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>violate</em>
</span>
</span>
</div>
<a class='quick-help' href='defile#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='defile#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/defile/memory_hooks/5878.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>De</span></span>a<span class="emp3"><span>f</span></span> and Frag<span class="emp2"><span>ile</span></span></span></span> Just because someone is <span class="emp3"><span>de</span></span>a<span class="emp3"><span>f</span></span> and frag<span class="emp2"><span>ile</span></span> does not mean that one has a right to <span class="emp3"><span>def</span></span><span class="emp2"><span>ile</span></span> his good name ... physical condition has nothing to do with one's character.
</p>
</div>

<div id='memhook-button-bar'>
<a href="defile#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/defile/memory_hooks">Use other hook</a>
<a href="defile#" id="memhook-use-own" url="https://membean.com/mywords/defile/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='defile#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
A Jackson County family is looking for answers. They want to know who <b>defiled</b> the grave of a loved one and why. . . . Red paint was poured on the tombstone, covering up Tiffany's picture. A ceramic Bible was covered in green paint, and flowers had been knocked over. This grave desecration brought a new wave of grief for Tiffany's mother.
<cite class='attribution'>
&mdash;
WLOX
</cite>
</li>
<li>
As a traditional healer of the Orang Rimba, or forest people, here on the Indonesian island of Sumatra, Temenggung Tarip had long depended on jungle blooms to conjure the divine for his seminomadic indigenous community. . . . But timber, rubber, paper and palm oil plantations have encroached on the forests of Indonesia. . . . With the land <b>defiled</b>, the flowers no longer worked their magic, Mr. Tarip said. The gods did not come. Toothaches remained unhealed.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
A fifth verse was added [to the Star-Spangled Banner] in 1861 by poet Oliver Wendell Holmes. His verse included the words “the traitor that dares to <b>defile</b> the flag of her stars,” and it was added to support the Union cause in the Civil War.
<cite class='attribution'>
&mdash;
South Florida Reporter
</cite>
</li>
<li>
Belarusian President Alexander Lukashenko has had enough with polluters. According to reports, he's now threatening to seize the cars of people who <b>defile</b> the country by dumping their trash anywhere but the garbage.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/defile/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='defile#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>From a root word meaning &#8220;trample upon.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='defile#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Click!</strong><span> Thank goodness she doesn't defile the burger any more than it already has been!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/defile.jpg' video_url='examplevids/defile' video_width='350'></span>
<div id='wt-container'>
<img alt="Defile" height="288" src="https://cdn1.membean.com/video/examplevids/defile.jpg" width="350" />
<div class='center'>
<a href="defile#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='defile#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Defile" src="https://cdn1.membean.com/public/images/wordimages/cons2/defile.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='defile#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>adulterate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>besmirch</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>blight</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>brackish</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>contagion</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>deleterious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>desecrate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>execrate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>malevolent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>malfeasance</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>malign</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>ravage</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>revile</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>sacrilegious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>taint</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>trammel</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>vilify</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>vitiate</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>vituperative</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>absolve</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>homage</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>liturgy</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>piety</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>potable</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>pristine</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>reverent</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>unscathed</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>unsullied</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>venerable</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="defile" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>defile</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="defile#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

