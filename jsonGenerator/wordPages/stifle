
<!DOCTYPE html>
<html>
<head>
<title>Word: stifle | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you act with abandon, you give in to the impulse of the moment and behave in a wild, uncontrolled way.</p>
<p class='rw-defn idx1' style='display:none'>If one&#8217;s powers or rights are circumscribed, they are limited or restricted.</p>
<p class='rw-defn idx2' style='display:none'>If you circumvent something, such as a rule or restriction, you try to get around it in a clever and perhaps dishonest way.</p>
<p class='rw-defn idx3' style='display:none'>You coerce people when you force them to do something that they don&#8217;t want to do.</p>
<p class='rw-defn idx4' style='display:none'>If you feel a compulsion to do something, you feel like you must do it.</p>
<p class='rw-defn idx5' style='display:none'>When you are constrained, you are forced to do something or are kept from doing it.</p>
<p class='rw-defn idx6' style='display:none'>A deterrent keeps someone from doing something against you.</p>
<p class='rw-defn idx7' style='display:none'>When you disentangle a knot or a problem, you untie the knot or get yourself out of the problem.</p>
<p class='rw-defn idx8' style='display:none'>A disincentive to do something does not encourage you to do that thing; rather, it restrains and hinders you from doing it.</p>
<p class='rw-defn idx9' style='display:none'>Duress is the application or threat of force to compel someone to act in a particular way.</p>
<p class='rw-defn idx10' style='display:none'>If a fact or idea eludes you, you cannot remember or understand it; if you elude someone, you manage to escape or hide from them.</p>
<p class='rw-defn idx11' style='display:none'>When someone is exonerated from guilt, a task, or a responsibility, they are set free or excused from it.</p>
<p class='rw-defn idx12' style='display:none'>When you extricate yourself from a difficult or unpleasant situation, you manage to escape it.</p>
<p class='rw-defn idx13' style='display:none'>When you facilitate something, such as an event or project, you make it easier for everyone to get it done by giving assistance.</p>
<p class='rw-defn idx14' style='display:none'>If something fetters you, it restricts you from doing something you want to do.</p>
<p class='rw-defn idx15' style='display:none'>A hindrance is an obstacle or obstruction that gets in your way and prevents you from doing something.</p>
<p class='rw-defn idx16' style='display:none'>An impediment is something that blocks or obstructs progress; it can also be a weakness or disorder, such as having difficulty with speaking.</p>
<p class='rw-defn idx17' style='display:none'>To induce a state is to bring it about; to induce someone to do something is to persuade them to do it.</p>
<p class='rw-defn idx18' style='display:none'>Something that inhibits you from doing something restricts or keeps you from doing it.</p>
<p class='rw-defn idx19' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx20' style='display:none'>If you mollify someone, you say or do something to make that person feel less angry or upset.</p>
<p class='rw-defn idx21' style='display:none'>To obviate a problem is to eliminate it or do something that makes solving the problem unnecessary.</p>
<p class='rw-defn idx22' style='display:none'>If something, such as a road, is occluded, it has been closed off or blocked; therefore, cars are prevented from continuing on their way until the road is opened once again.</p>
<p class='rw-defn idx23' style='display:none'>If you are oppressed by someone or something, you are beaten down, troubled, or burdened by them or it.</p>
<p class='rw-defn idx24' style='display:none'>To parry is to ward something off or deflect it.</p>
<p class='rw-defn idx25' style='display:none'>When you preclude something from happening, you prevent it from doing so.</p>
<p class='rw-defn idx26' style='display:none'>Spontaneity is freedom to act when and how you want to, often in an unpredictable or unplanned way.</p>
<p class='rw-defn idx27' style='display:none'>A stricture is a rule or condition that imposes restrictions or limits on what you can do.</p>
<p class='rw-defn idx28' style='display:none'>Something that stymies you presents an obstacle that prevents you from doing what you need or want to do.</p>
<p class='rw-defn idx29' style='display:none'>If someone subjugates a group of people or country, they conquer and bring it under control by force.</p>
<p class='rw-defn idx30' style='display:none'>A tether is a restraint, such as a leash, rope, or chain, that holds something in place.</p>
<p class='rw-defn idx31' style='display:none'>When you thwart someone&#8217;s ambitions, you obstruct or stop them from happening.</p>
<p class='rw-defn idx32' style='display:none'>When you trammel something or someone, you bring about limits to freedom or hinder movements or activities in some fashion.</p>
<p class='rw-defn idx33' style='display:none'>A feeling that is unbridled is enthusiastic and unlimited in its expression.</p>
<p class='rw-defn idx34' style='display:none'>Someone who is unrestrained is free to do as they please; they are not controlled by anyone but themselves, which can lead to excessive behavior.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>stifle</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='stifle#' id='pronounce-sound' path='audio/words/amy-stifle'></a>
STAHY-fuhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='stifle#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='stifle#' id='context-sound' path='audio/wordcontexts/brian-stifle'></a>
When my wife was a child her interests were often <em>stifled</em> or held in check by her parents who had fixed ideas about what girls should do.  She really wanted to play the trumpet, but that interest was at once <em>stifled</em> or put to a stop by her father, who would not let her play it.  When she was a junior in college she really wanted to travel to France to perfect her ability to speak French, but once again her parents <em>stifled</em> or prevented her from following that love as well.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How might someone <em>stifle</em> you?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
By telling you that everything you do is wonderful and amazing.
</li>
<li class='choice answer '>
<span class='result'></span>
By not allowing you to express yourself as you&#8217;d like to.
</li>
<li class='choice '>
<span class='result'></span>
By helping you cope with a difficult and unexpected loss.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='stifle#' id='definition-sound' path='audio/wordmeanings/amy-stifle'></a>
When you <em>stifle</em> someone&#8217;s creativity or inner drive, you prevent it from being expressed.
</li>
<li class='def-text'>
The word <em>stifle</em> also means to cut off someone&#8217;s air via suffocation.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>prevent</em>
</span>
</span>
</div>
<a class='quick-help' href='stifle#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='stifle#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/stifle/memory_hooks/5463.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>St</span></span>op the R<span class="emp2"><span>ifles</span></span></span></span> Some people would like to <span class="emp1"><span>st</span></span>op people from owning r<span class="emp2"><span>ifles</span></span> altogether because they view them as a safety hazard.
</p>
</div>

<div id='memhook-button-bar'>
<a href="stifle#" id="add-public-hook" style="" url="https://membean.com/mywords/stifle/memory_hooks">Use other public hook</a>
<a href="stifle#" id="memhook-use-own" url="https://membean.com/mywords/stifle/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='stifle#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
A privacy backlash, however, would <b>stifle</b> these potentially revolutionary services before they get off the ground—and leave the computer industry’s biggest plan for growth in tatters. . . . The U.S. Congress is considering four bills that would make it illegal for companies to collect and share information online or through cell phones about people without clearer warning and permission.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
That’s no threat to American pride or the content of movies but it constitutes an indictment of the way Washington <b>stifles</b> competition in the entertainment industry.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Swimmer Michelle Engelsman, 28, a Team Darfur member who hopes to make a second straight Australian Olympic team, expressed similar dismay about attempts to <b>stifle</b> athletes.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/stifle/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='stifle#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>The word <em>stifle</em> comes from a root word meaning &#8220;to choke, drown.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='stifle#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>The Simpsons</strong><span> Bart complains that Homer is stifling his creativity.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/stifle.jpg' video_url='examplevids/stifle' video_width='350'></span>
<div id='wt-container'>
<img alt="Stifle" height="288" src="https://cdn1.membean.com/video/examplevids/stifle.jpg" width="350" />
<div class='center'>
<a href="stifle#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='stifle#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Stifle" src="https://cdn3.membean.com/public/images/wordimages/cons2/stifle.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='stifle#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>circumscribe</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>coerce</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>compulsion</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>constrain</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>deterrent</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>disincentive</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>duress</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>fetter</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>hindrance</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>impediment</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>inhibit</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>occlude</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>oppress</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>preclude</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>stricture</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>stymie</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>subjugate</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>tether</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>thwart</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>trammel</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abandon</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>circumvent</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>disentangle</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>elude</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>exonerate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>extricate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>facilitate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>induce</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>mollify</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>obviate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>parry</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>spontaneity</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>unbridled</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>unrestrained</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="stifle" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>stifle</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="stifle#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

