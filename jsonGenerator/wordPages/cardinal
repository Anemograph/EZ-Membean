
<!DOCTYPE html>
<html>
<head>
<title>Word: cardinal | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you describe a decision, rule, or plan as arbitrary, you think that it was decided without any thought, standard, or system to guide it; therefore, it can seem unfair.</p>
<p class='rw-defn idx1' style='display:none'>An axiom is a wise saying or widely recognized general truth that is often self-evident; it can also be an established rule or law.</p>
<p class='rw-defn idx2' style='display:none'>The caliber of a person is the level or quality of their ability, intelligence, and other talents.</p>
<p class='rw-defn idx3' style='display:none'>Chaff is the husks of grain separated from the edible plant; it can also be useless matter in general that is cast aside.</p>
<p class='rw-defn idx4' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx5' style='display:none'>A cynosure is an object that serves as the center of attention.</p>
<p class='rw-defn idx6' style='display:none'>Detritus is the small pieces of waste that remain after something has been destroyed or used.</p>
<p class='rw-defn idx7' style='display:none'>Efficacy is the ability or power to produce an expected effect or result.</p>
<p class='rw-defn idx8' style='display:none'>If you say that a person or thing is the epitome of something, you mean that they or it is the best possible example of that thing.</p>
<p class='rw-defn idx9' style='display:none'>Something that is epochal is highly significant or important; this adjective often refers to the bringing about or marking of the beginning of a new era.</p>
<p class='rw-defn idx10' style='display:none'>Errant things or people behave in a way that is unacceptable or wrong, such as a missile that travels in the wrong direction or a student who does not follow classroom rules.</p>
<p class='rw-defn idx11' style='display:none'>If you eschew something, you deliberately avoid doing it, especially for moral reasons.</p>
<p class='rw-defn idx12' style='display:none'>Something that is extraneous is not relevant or connected to something else, or it is not essential to a given situation.</p>
<p class='rw-defn idx13' style='display:none'>If you think something, such as a statement or idea, is fatuous, you consider it stupid or extremely silly; a fatuous hope is unrealistic.</p>
<p class='rw-defn idx14' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx15' style='display:none'>Something frivolous is not worth taking seriously or considering because it is silly or childish.</p>
<p class='rw-defn idx16' style='display:none'>A hapless person is unlucky, so much so that a stream of unfortunate things has happened and continues to happen to them.</p>
<p class='rw-defn idx17' style='display:none'>If you describe someone&#8217;s behavior as inane, you think it is completely stupid or without much meaning.</p>
<p class='rw-defn idx18' style='display:none'>Inconsequential matters are unimportant or are of little to no significance.</p>
<p class='rw-defn idx19' style='display:none'>An indispensable item is absolutely necessary or essential—it cannot be done without.</p>
<p class='rw-defn idx20' style='display:none'>Something that has inestimable value or benefit has so much of it that it cannot be calculated.</p>
<p class='rw-defn idx21' style='display:none'>If something is infallible, it is never wrong and so is incapable of making mistakes.</p>
<p class='rw-defn idx22' style='display:none'>Irrelevant information is unrelated or unconnected to the situation at hand.</p>
<p class='rw-defn idx23' style='display:none'>A momentous occurrence is very important, significant, or vital in some way.</p>
<p class='rw-defn idx24' style='display:none'>Nominal can refer to someone who is in charge in name only, or it can refer to a very small amount of something; both are related by their relative insignificance.</p>
<p class='rw-defn idx25' style='display:none'>Something that is paltry is practically worthless or insignificant.</p>
<p class='rw-defn idx26' style='display:none'>A paragon is an example of a thing or person at its or their very best.</p>
<p class='rw-defn idx27' style='display:none'>Something that is of paramount importance or significance is chief or supreme in those things.</p>
<p class='rw-defn idx28' style='display:none'>Anything picayune is unimportant, insignificant, or minor.</p>
<p class='rw-defn idx29' style='display:none'>Something that is pivotal is more important than anything else in a situation or a system; consequently, it is the key to its success.</p>
<p class='rw-defn idx30' style='display:none'>A precept is a rule or principle that teaches correct behavior.</p>
<p class='rw-defn idx31' style='display:none'>Something predominant is the most important or the most common thing in a group.</p>
<p class='rw-defn idx32' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>
<p class='rw-defn idx33' style='display:none'>If you refrain from doing something, you do not do it—even though you want to.</p>
<p class='rw-defn idx34' style='display:none'>If something is requisite for a purpose, it is needed, appropriate, or necessary for that specific purpose.</p>
<p class='rw-defn idx35' style='display:none'>A rubric is a set of instructions at the beginning of a document, such as an examination or term paper, that is usually printed in a different style so as to highlight its importance.</p>
<p class='rw-defn idx36' style='display:none'>A person or subject that is superficial is shallow, without depth, obvious, and concerned only with surface matters.</p>
<p class='rw-defn idx37' style='display:none'>Something that is superfluous is unnecessary; it is more than what is wanted or needed at the current time.</p>
<p class='rw-defn idx38' style='display:none'>A tangential point in an argument merely touches upon something that is not really relevant to the conversation at hand; rather, it is a minor or unimportant point.</p>
<p class='rw-defn idx39' style='display:none'>A tenet is a belief held by a group, organization, or person.</p>
<p class='rw-defn idx40' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx41' style='display:none'>A touchstone is the standard or best example by which the value or quality of something else is judged.</p>
<p class='rw-defn idx42' style='display:none'>If you refer to something as a trifle, you mean that it is of little or no importance or value.</p>
<p class='rw-defn idx43' style='display:none'>A trite remark or idea is so overused that it is no longer interesting or novel.</p>
<p class='rw-defn idx44' style='display:none'>Something trivial is of little value or is not important.</p>
<p class='rw-defn idx45' style='display:none'>If someone vitiates something, they make it less effective, weaker, or reduced in quality.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>cardinal</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='cardinal#' id='pronounce-sound' path='audio/words/amy-cardinal'></a>
KAHR-dn-l
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='cardinal#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='cardinal#' id='context-sound' path='audio/wordcontexts/brian-cardinal'></a>
The Latin phrase <em>carpe diem</em>, or &#8220;seize the day,&#8221; is one of the  <em>cardinal</em> and central guiding principles that shapes my life.  The <em>cardinal</em> and key principles of my religion stress honest labor and the act of sharing my earnings with those in need.  I feel that it is important to have a set of core beliefs or <em>cardinal</em> concepts to help navigate one&#8217;s shifting journey in life.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>cardinal</em> saying in business?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
An essential principle, such as: &#8220;The customer is always right.&#8221;
</li>
<li class='choice '>
<span class='result'></span>
An outdated saying, such as: &#8220;If it isn&#8217;t broken, don&#8217;t fix it.&#8221;
</li>
<li class='choice '>
<span class='result'></span>
A widely rejected opinion, such as: &#8220;What the customer doesn&#8217;t know won&#8217;t hurt them.&#8221;
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='cardinal#' id='definition-sound' path='audio/wordmeanings/amy-cardinal'></a>
A <em>cardinal</em> rule or quality is considered to be the most important or basic in a set of rules or qualities.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>key</em>
</span>
</span>
</div>
<a class='quick-help' href='cardinal#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='cardinal#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/cardinal/memory_hooks/3018.json'></span>
<p>
<span class="emp0"><span>Male <span class="emp1"><span>Cardinal</span></span> Sticks Out</span></span>  Just like the bright red <span class="emp1"><span>cardinal</span></span> is highly visible on a snow-covered branch, so too is a <span class="emp1"><span>cardinal</span></span> rule most noticeable because of its central and dominant importance.
</p>
</div>

<div id='memhook-button-bar'>
<a href="cardinal#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/cardinal/memory_hooks">Use other hook</a>
<a href="cardinal#" id="memhook-use-own" url="https://membean.com/mywords/cardinal/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='cardinal#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
There art two <b>cardinal</b> sins from which all others spring: Impatience and Laziness.
<span class='attribution'>&mdash; Franz Kafka, twentieth century Austrian writer</span>
<img alt="Franz kafka, twentieth century austrian writer" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Franz Kafka, twentieth century Austrian writer.jpg?qdep8" width="80" />
</li>
<li>
"The animating principle of my faith, as taught to me by church and home, was that the <b>cardinal</b> sin was abuse of power," he said in an interview with the Monitor.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/cardinal/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='cardinal#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>cardin</td>
<td>
&rarr;
</td>
<td class='meaning'>hinge</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='al_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='cardinal#'>
<span class=''></span>
-al
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>A <em>cardinal</em> aspect of something has the &#8220;nature of a hinge,&#8221; that is, everything else depends upon it: without a working hinge, a door becomes useless.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='cardinal#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>The Pretender: Wild Child</strong><span> Dr. Bell is breaking the cardinal rule of not getting emotionally attached to patients.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/cardinal.jpg' video_url='examplevids/cardinal' video_width='350'></span>
<div id='wt-container'>
<img alt="Cardinal" height="288" src="https://cdn1.membean.com/video/examplevids/cardinal.jpg" width="350" />
<div class='center'>
<a href="cardinal#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='cardinal#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Cardinal" src="https://cdn1.membean.com/public/images/wordimages/cons2/cardinal.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='cardinal#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>axiom</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>caliber</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>cynosure</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>efficacy</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>epitome</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>epochal</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>indispensable</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>inestimable</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>infallible</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>momentous</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>paragon</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>paramount</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>pivotal</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>precept</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>predominant</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>requisite</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>rubric</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>tenet</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>touchstone</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>arbitrary</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>chaff</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>detritus</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>errant</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>eschew</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>extraneous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>fatuous</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>frivolous</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>hapless</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>inane</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>inconsequential</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>irrelevant</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>nominal</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>paltry</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>picayune</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>refrain</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>superficial</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>superfluous</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>tangential</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>trifle</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>trite</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>trivial</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>vitiate</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="cardinal" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>cardinal</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="cardinal#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

