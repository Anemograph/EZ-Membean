
<!DOCTYPE html>
<html>
<head>
<title>Word: appurtenance | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>If you abridge a book or play, you make it shorter by making cuts to the original.</p>
<p class='rw-defn idx2' style='display:none'>An addendum is an additional section added to the end of a book, document, or speech that gives more information.</p>
<p class='rw-defn idx3' style='display:none'>An adjunct is something that is added to or joined to something else that is larger or more important.</p>
<p class='rw-defn idx4' style='display:none'>Something that happens unexpectedly and by chance that is not inherent or natural to a given item or situation is adventitious, such as a root appearing in an unusual place on a plant.</p>
<p class='rw-defn idx5' style='display:none'>Amenities are things that make a place comfortable or easy to live in, such as hot water, updated bathrooms, and sports facilities.</p>
<p class='rw-defn idx6' style='display:none'>When something is ancillary to something else, such as a workbook to a textbook, it supports it but is less important than that which it supports.</p>
<p class='rw-defn idx7' style='display:none'>Attrition is the process of gradually decreasing the strength of something (such as an enemy force) by continually attacking it.</p>
<p class='rw-defn idx8' style='display:none'>To augment something is to increase its value or effectiveness by adding something to it.</p>
<p class='rw-defn idx9' style='display:none'>Something is concomitant when it happens at the same time as something else and is connected with it in some fashion.</p>
<p class='rw-defn idx10' style='display:none'>If something is contingent upon something else, the first thing depends on the second in order to happen or exist.</p>
<p class='rw-defn idx11' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx12' style='display:none'>Irrelevant information is unrelated or unconnected to the situation at hand.</p>
<p class='rw-defn idx13' style='display:none'>To obviate a problem is to eliminate it or do something that makes solving the problem unnecessary.</p>
<p class='rw-defn idx14' style='display:none'>Paraphernalia is the equipment that one uses to do something or one&#8217;s personal belongings collected over a period of time.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>appurtenance</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='appurtenance#' id='pronounce-sound' path='audio/words/amy-appurtenance'></a>
uh-PUR-tn-uhns
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='appurtenance#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='appurtenance#' id='context-sound' path='audio/wordcontexts/brian-appurtenance'></a>
The accessories or <em>appurtenances</em> of wealth inspire envy in some of us who are less well-to-do.  Everyone seems to want an expensive Fendi purse or a Maserati sports car; such glamorous <em><em>appurtenances</em></em> are useful to broadcast one&#8217;s ability to spend money.  Some believe that the <em><em>appurtenances</em></em> of a celebrity lifestyle seem designed merely to allow their owners a chance to gloat about their riches . . . and perhaps to make others envious.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of an <em>appurtenance</em> for a soccer player?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Assisting the coach during practice.
</li>
<li class='choice answer '>
<span class='result'></span>
A nice pair of cleats or boots.
</li>
<li class='choice '>
<span class='result'></span>
Staying late to practice shooting.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='appurtenance#' id='definition-sound' path='audio/wordmeanings/amy-appurtenance'></a>
An <em>appurtenance</em> is a supporting feature, form of equipment, or item associated with a particular activity.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>accessory</em>
</span>
</span>
</div>
<a class='quick-help' href='appurtenance#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='appurtenance#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/appurtenance/memory_hooks/4649.json'></span>
<p>
<span class="emp0"><span>Magic Curls</span></span> The secret behind <span class="emp3"><span>Nanc</span></span>y's sensational curls was <span class="emp2"><span>Pert</span></span> Plus, an ap<span class="emp2"><span>purt</span></span>e<span class="emp3"><span>nanc</span></span>e well worth the small cost.
</p>
</div>

<div id='memhook-button-bar'>
<a href="appurtenance#" id="add-public-hook" style="" url="https://membean.com/mywords/appurtenance/memory_hooks">Use other public hook</a>
<a href="appurtenance#" id="memhook-use-own" url="https://membean.com/mywords/appurtenance/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='appurtenance#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
He got that new <b>appurtenance</b> of politics, an 800 number, and set up a phone bank on a rented floor in his tower on Merit Drive.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
The Pinkerton [guard] on duty eventually stepped aside, but not until he had summoned a superior who thought the matter over and decided that the enraged coach and his players were a necessary <b>appurtenance</b> to the [football game].
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
This action was the result, the plaintiff claiming that he was now the sole owner of the premises, and that as such, he was entitled to the possession of the [buried] vault as an <b>appurtenance</b>.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/appurtenance/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='appurtenance#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ap_to' data-tree-url='//cdn1.membean.com/public/data/treexml' href='appurtenance#'>
<span class=''></span>
ap-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to, towards</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pur_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='appurtenance#'>
<span class=''></span>
pur-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ten_hold' data-tree-url='//cdn1.membean.com/public/data/treexml' href='appurtenance#'>
<span class='common'></span>
ten
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>have, hold</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ance_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='appurtenance#'>
<span class=''></span>
-ance
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state or condition</td>
</tr>
</table>
<p>An <em>appurtenance</em> is that accessory or extra item that has the &#8220;state or condition&#8221; of being &#8220;thoroughly held towards&#8221; another more important thing.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='appurtenance#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Appurtenance" src="https://cdn1.membean.com/public/images/wordimages/cons2/appurtenance.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='appurtenance#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>addendum</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>adjunct</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>adventitious</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>amenity</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>ancillary</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>augment</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>concomitant</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>contingent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>paraphernalia</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abridge</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>attrition</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>irrelevant</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>obviate</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="appurtenance" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>appurtenance</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="appurtenance#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

