
<!DOCTYPE html>
<html>
<head>
<title>Word: confound | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you abash someone, you make them feel uncomfortable, ashamed, embarrassed, or inferior.</p>
<p class='rw-defn idx1' style='display:none'>If someone is addled by something, they are confused by it and unable to think properly.</p>
<p class='rw-defn idx2' style='display:none'>If something bewilders you, you are very confused or puzzled by it.</p>
<p class='rw-defn idx3' style='display:none'>To confute an argument is to prove it to be thoroughly false; to confute a person is to prove them to be wrong.</p>
<p class='rw-defn idx4' style='display:none'>To contravene a law, rule, or agreement is to do something that is not allowed or is forbidden by that law, rule, or agreement.</p>
<p class='rw-defn idx5' style='display:none'>A conundrum is a problem or puzzle that is difficult or impossible to solve.</p>
<p class='rw-defn idx6' style='display:none'>To corroborate something that has been said or reported means to give information or evidence that further supports it.</p>
<p class='rw-defn idx7' style='display:none'>If someone will countenance something, they will approve, tolerate, or support it.</p>
<p class='rw-defn idx8' style='display:none'>When you decipher a message or piece of writing, you work out what it says, even though it is very difficult to read or understand.</p>
<p class='rw-defn idx9' style='display:none'>When you are discombobulated, you are confused and upset because you have been thrown into a situation that you cannot temporarily handle.</p>
<p class='rw-defn idx10' style='display:none'>If something discomfits you, it makes you feel embarrassed, confused, uncomfortable, or frustrated.</p>
<p class='rw-defn idx11' style='display:none'>If something disconcerts you, it makes you feel anxious, worried, or confused.</p>
<p class='rw-defn idx12' style='display:none'>When someone feels disquiet about a situation, they feel very worried or nervous.</p>
<p class='rw-defn idx13' style='display:none'>If you elucidate something, you make it easier to understand by giving more relevant information.</p>
<p class='rw-defn idx14' style='display:none'>If a fact or idea eludes you, you cannot remember or understand it; if you elude someone, you manage to escape or hide from them.</p>
<p class='rw-defn idx15' style='display:none'>If you fathom something complicated or mysterious, you are able to understand it.</p>
<p class='rw-defn idx16' style='display:none'>If an idea or thought is incisive, it is expressed in a penetrating and knowledgeable manner that is clear and brief; additionally, it can demonstrate impressive understanding of related ideas or thoughts.</p>
<p class='rw-defn idx17' style='display:none'>If someone is lucid, they are able to understand things and think clearly; this adjective also describes writing or speech that is crystal clear and easy to understand.</p>
<p class='rw-defn idx18' style='display:none'>A misapprehension is a misunderstanding or false impression that you are under, especially concerning another person&#8217;s intentions towards you.</p>
<p class='rw-defn idx19' style='display:none'>A parenthetical remark further explains or qualifies information.</p>
<p class='rw-defn idx20' style='display:none'>When you are perplexed about something, you are completely confused or baffled by it.</p>
<p class='rw-defn idx21' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx22' style='display:none'>When you resolve a problem, you solve it or come to a decision about it.</p>
<p class='rw-defn idx23' style='display:none'>An aged person who is senile is less mentally aware than they were in the past; therefore, they tend to be confused and forgetful at times.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>confound</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='confound#' id='pronounce-sound' path='audio/words/amy-confound'></a>
kuhn-FOUND
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='confound#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='confound#' id='context-sound' path='audio/wordcontexts/brian-confound'></a>
The math homework the professor gave us was so complicated that it completely <em><em>confounded</em></em> and confused me.  I was always a whiz at math in high school, but this non-Euclidean geometry completely <em><em>confounds</em></em> and puzzles me.  I am <em><em>confounded</em></em> when I try to picture what I&#8217;m trying to learn but remain unsuccessful.  I don&#8217;t like to admit when things <em>confound</em> me, especially since my roommate just breezed through the homework.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When might you be <em>confounded</em> by something?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
If it is difficult and requires more energy and fitness than you have.
</li>
<li class='choice '>
<span class='result'></span>
If it is enjoyable and something you want to remember.
</li>
<li class='choice answer '>
<span class='result'></span>
If it is different from what you expected and therefore surprising.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='confound#' id='definition-sound' path='audio/wordmeanings/amy-confound'></a>
If something <em>confounds</em> you, it makes you feel surprised and confused, often because it does not meet your predefined expectations.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>confuse</em>
</span>
</span>
</div>
<a class='quick-help' href='confound#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='confound#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/confound/memory_hooks/2799.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Con</span></span>cealed, Not <span class="emp3"><span>Found</span></span></span></span> I <span class="emp3"><span>found</span></span>ered, or sunk, mentally, when the <span class="emp2"><span>con</span></span>cealed answer could not be <span class="emp3"><span>found</span></span>, <span class="emp2"><span>con</span></span><span class="emp3"><span>found</span></span>ing my <span class="emp3"><span>f</span></span>l<span class="emp3"><span>ound</span></span>ering and clumsy brain despite all its <span class="emp2"><span>con</span></span>centration.
</p>
</div>

<div id='memhook-button-bar'>
<a href="confound#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/confound/memory_hooks">Use other hook</a>
<a href="confound#" id="memhook-use-own" url="https://membean.com/mywords/confound/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='confound#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The already confusing patchwork of state regulations was further <b>confounded</b> by guideline changes last week from the U.S. Centers for Disease Control and Prevention.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
Afghanistan’s fledgling Little Leaguers, most of them between the ages of 10 and 16, tend to <b>confound</b> the mechanics of baseball with cricket, which is popular in neighboring Pakistan.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
To understand what's going on, you'll have to learn the special, pre-modern language of "filibuster" and "cloture," baffling math and maddening rules like "Rule XXII" that govern Senate procedure and <b>confound</b> common sense.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
We look for laws of culture, but they aren’t nearly as easy to identify or rely on as the laws of science. . . . So while we may get better at predicting, measuring and potentially counteracting natural events, culture will continue to <b>confound</b> us.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/confound/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='confound#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='con_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='confound#'>
<span class='common'></span>
con-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='found_pour' data-tree-url='//cdn1.membean.com/public/data/treexml' href='confound#'>
<span class=''></span>
found
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>pour, pour out, spread out</td>
</tr>
</table>
<p>If a history student becomes <em>confounded</em> by dates, too many have been mixed or &#8220;poured together&#8221; at one time for their brain to handle, thus &#8220;confusing&#8221; it.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='confound#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>ABC World News</strong><span> Gorske has confounded us all.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/confound.jpg' video_url='examplevids/confound' video_width='350'></span>
<div id='wt-container'>
<img alt="Confound" height="288" src="https://cdn1.membean.com/video/examplevids/confound.jpg" width="350" />
<div class='center'>
<a href="confound#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='confound#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Confound" src="https://cdn1.membean.com/public/images/wordimages/cons2/confound.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='confound#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abash</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>addle</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bewilder</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>confute</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>contravene</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>conundrum</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>discombobulated</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>discomfit</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>disconcert</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>disquiet</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>elude</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>misapprehension</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>perplexed</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>senile</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='6' class = 'rw-wordform notlearned'><span>corroborate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>countenance</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>decipher</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>elucidate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>fathom</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>incisive</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>lucid</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>parenthetical</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>resolve</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="confound" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>confound</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="confound#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

