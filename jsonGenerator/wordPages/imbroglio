
<!DOCTYPE html>
<html>
<head>
<title>Word: imbroglio | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Imbroglio-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/imbroglio-large.jpg?qdep8" />
</div>
<a href="imbroglio#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx1' style='display:none'>An altercation is a noisy disagreement or heated argument.</p>
<p class='rw-defn idx2' style='display:none'>Bedlam is a situation with a great deal of noise and confusion.</p>
<p class='rw-defn idx3' style='display:none'>If you are bellicose, you behave in an aggressive way and are likely to start an argument or fight.</p>
<p class='rw-defn idx4' style='display:none'>A belligerent person or country is aggressive, very unfriendly, and likely to start a fight.</p>
<p class='rw-defn idx5' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx6' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx7' style='display:none'>A chivalrous man behaves in a polite, kind, generous, and honorable way, especially towards women.</p>
<p class='rw-defn idx8' style='display:none'>Complacent persons are too confident and relaxed because they think that they can deal with a situation easily; however, in many circumstances, this is not the case.</p>
<p class='rw-defn idx9' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx10' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx11' style='display:none'>When someone is contentious, they are argumentative and likely to provoke a fight.</p>
<p class='rw-defn idx12' style='display:none'>A convivial atmosphere or occasion is friendly, pleasant, cheerful, and relaxed.</p>
<p class='rw-defn idx13' style='display:none'>If someone will countenance something, they will approve, tolerate, or support it.</p>
<p class='rw-defn idx14' style='display:none'>A situation or thing that is discordant does not fit in with other things; therefore, it is disagreeable, strange, or unpleasant.</p>
<p class='rw-defn idx15' style='display:none'>When you disentangle a knot or a problem, you untie the knot or get yourself out of the problem.</p>
<p class='rw-defn idx16' style='display:none'>Euphony is a pleasing sound in speech or music.</p>
<p class='rw-defn idx17' style='display:none'>A fracas is a rough and noisy fight or loud argument that can involve multiple people.</p>
<p class='rw-defn idx18' style='display:none'>An impasse is a difficult situation in which progress is not possible, usually because none of the people involved are willing to agree.</p>
<p class='rw-defn idx19' style='display:none'>If you describe a situation or process as labyrinthine, you mean that it is very complicated, involved, and difficult to understand.</p>
<p class='rw-defn idx20' style='display:none'>A melee is a noisy, confusing, hand-to-hand fight involving a group of people.</p>
<p class='rw-defn idx21' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx22' style='display:none'>Pandemonium is a very noisy and uncontrolled situation, especially one that is caused by a lot of angry or excited people.</p>
<p class='rw-defn idx23' style='display:none'>If you are in a plight, you are in trouble of some kind or in a state of unfortunate circumstances.</p>
<p class='rw-defn idx24' style='display:none'>A polemic is a strong written or spoken statement that usually attacks or less often defends a particular idea, opinion, or belief.</p>
<p class='rw-defn idx25' style='display:none'>If you are in a predicament, you are in a difficult situation or unpleasant mess that is hard to get out of.</p>
<p class='rw-defn idx26' style='display:none'>A quagmire is a difficult and complicated situation that is not easy to avoid or get out of; this word can also refer to a swamp that is hard to travel through.</p>
<p class='rw-defn idx27' style='display:none'>A tumultuous event or period of time is filled with great excitement, confusion, or violence; a tumultuous reaction to something is likewise very loud and noisy because people are happy and excited.</p>
<p class='rw-defn idx28' style='display:none'>A vendetta is a prolonged situation in which one person or group tries to harm another person or group—and vice versa.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>imbroglio</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='imbroglio#' id='pronounce-sound' path='audio/words/amy-imbroglio'></a>
im-BROHL-yoh
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='imbroglio#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='imbroglio#' id='context-sound' path='audio/wordcontexts/brian-imbroglio'></a>
What started as a celebration of my grandmother&#8217;s ninetieth birthday soon became a difficult <em>imbroglio</em> of complicated family tensions.  My father could not decide who was in charge of the event, and when Uncle Roy called Dad a demanding jerk, the heated disagreement or <em>imbroglio</em> really got started.  Luckily, I took my grandmother out to lunch while they were planning the party, so she didn&#8217;t have to hear the competitive <em>imbroglio</em> or complex struggle between her grown children.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a well-known <em>imbroglio</em> in literature?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Newland Archer&#8217;s falling in love with his wife-to-be, May Welland, and hoping that their marriage doesn&#8217;t turn to dullness like his friend&#8217;s.
</li>
<li class='choice '>
<span class='result'></span>
The realization by Babbitt that he has never done one thing that he has wanted to during his entire life.
</li>
<li class='choice answer '>
<span class='result'></span>
The complex plot of Romeo and Juliet in which a family feud prevents the two young lovers from being together.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='imbroglio#' id='definition-sound' path='audio/wordmeanings/amy-imbroglio'></a>
An <em>imbroglio</em> is a very difficult, confusing, and complicated situation.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>convoluted situation</em>
</span>
</span>
</div>
<a class='quick-help' href='imbroglio#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='imbroglio#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/imbroglio/memory_hooks/3628.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>I'm</span></span> <span class="emp2"><span>Broil</span></span>ing, <span class="emp3"><span>Yo</span></span>!</span></span> screamed the f<span class="emp2"><span>rog</span></span> to the princess as the witch started to stir the f<span class="emp2"><span>rog</span></span> soup--what an <span class="emp1"><span>im</span></span><span class="emp2"><span>brogl</span></span><span class="emp3"><span>io</span></span> for the f<span class="emp2"><span>rog</span></span>!
</p>
</div>

<div id='memhook-button-bar'>
<a href="imbroglio#" id="add-public-hook" style="" url="https://membean.com/mywords/imbroglio/memory_hooks">Use other public hook</a>
<a href="imbroglio#" id="memhook-use-own" url="https://membean.com/mywords/imbroglio/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='imbroglio#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The <b>imbroglio</b> illustrates the ethical dangers and pitfalls of doing science in the Internet age, where a little clicking can bring you a shocking amount of information about what your colleagues and rivals are up to.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
The company formerly known has WorldCom emerged from bankruptcy Tuesday after filing for Chapter 11 protection 21 months ago. An $11 billion accounting scandal at the Ashburn-based telecommunications giant led to its official name change, effective Tuesday, in order to distance itself from the <b>imbroglio</b>.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
More than 20 of the companies entangled in the stock option <b>imbroglio</b> are in Silicon Valley, where the incentives first became a staple of compensation packages for rank-and-file employees as well as top executives.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
It could have been just another sleepy municipal spat over what should be built on a vacant parcel in a hot D.C. neighborhood. But the debate over the old Shaw Junior High site had something for everyone—and residents and politicians fiercely latched on. The months-long <b>imbroglio</b> ignited conversations about gentrification, the fate of neighborhood schools and whether the city was doing enough to ensure that students of color are experiencing the benefits of living in a booming city.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/imbroglio/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='imbroglio#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='im_in' data-tree-url='//cdn1.membean.com/public/data/treexml' href='imbroglio#'>
<span class='common'></span>
im-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>in, on</td>
</tr>
</table>
<p>From a root word meaning &#8220;to bewilder, tangle.&#8221;  An <em>imbroglio</em> is therefore an &#8220;entanglement&#8221; that is often &#8220;bewildering&#8221; or &#8220;confusing&#8221; to put an end to.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='imbroglio#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Mind Your Language</strong><span> An imbroglio at headquarters over misunderstood names.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/imbroglio.jpg' video_url='examplevids/imbroglio' video_width='350'></span>
<div id='wt-container'>
<img alt="Imbroglio" height="288" src="https://cdn1.membean.com/video/examplevids/imbroglio.jpg" width="350" />
<div class='center'>
<a href="imbroglio#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='imbroglio#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Imbroglio" src="https://cdn1.membean.com/public/images/wordimages/cons2/imbroglio.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='imbroglio#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>altercation</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bedlam</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bellicose</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>belligerent</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>contentious</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>discordant</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>fracas</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>impasse</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>labyrinthine</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>melee</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>pandemonium</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>plight</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>polemic</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>predicament</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>quagmire</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>tumultuous</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>vendetta</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>chivalrous</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>complacent</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>convivial</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>countenance</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>disentangle</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>euphony</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="imbroglio" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>imbroglio</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="imbroglio#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

