
<!DOCTYPE html>
<html>
<head>
<title>Word: definitive | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx1' style='display:none'>The acme of something is its highest point of achievement or excellence.</p>
<p class='rw-defn idx2' style='display:none'>When you are adept at something, you are very good, skillful, or talented at it.</p>
<p class='rw-defn idx3' style='display:none'>Someone who is adroit is skillful and clever, especially in thought, behavior, or action.</p>
<p class='rw-defn idx4' style='display:none'>If you describe something as ambiguous, you mean that it is unclear or confusing because it can be understood in multiple ways.</p>
<p class='rw-defn idx5' style='display:none'>An aphorism is a short, witty statement that contains a wise idea.</p>
<p class='rw-defn idx6' style='display:none'>An archetype is a perfect or typical example of something because it has the most important qualities that belong to that type of thing; it can also describe essential qualities common to a particular class of things.</p>
<p class='rw-defn idx7' style='display:none'>An axiom is a wise saying or widely recognized general truth that is often self-evident; it can also be an established rule or law.</p>
<p class='rw-defn idx8' style='display:none'>A bravura performance, such as of a highly technical work for the violin, is done with consummate skill.</p>
<p class='rw-defn idx9' style='display:none'>The caliber of a person is the level or quality of their ability, intelligence, and other talents.</p>
<p class='rw-defn idx10' style='display:none'>A connoisseur is an appreciative, informed, and knowledgeable judge of the fine arts; they can also just have excellent or perceptive taste in the fine arts without being an expert.</p>
<p class='rw-defn idx11' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx12' style='display:none'>A dilettante is someone who frequently pursues an interest in a subject; nevertheless, they never spend the time and effort to study it thoroughly enough to master it.</p>
<p class='rw-defn idx13' style='display:none'>When one person eclipses another&#8217;s achievements, they surpass or outshine that person in that endeavor.</p>
<p class='rw-defn idx14' style='display:none'>Efficacy is the ability or power to produce an expected effect or result.</p>
<p class='rw-defn idx15' style='display:none'>An embodiment of something, such as a quality or idea, is a visible representation or concrete expression of it.</p>
<p class='rw-defn idx16' style='display:none'>When you make an emphatic declaration, you are insistent and absolute about it.</p>
<p class='rw-defn idx17' style='display:none'>When something encompasses something else, it includes all aspects of it or completely surrounds it.</p>
<p class='rw-defn idx18' style='display:none'>If you say that a person or thing is the epitome of something, you mean that they or it is the best possible example of that thing.</p>
<p class='rw-defn idx19' style='display:none'>When people equivocate, they avoid making a clear statement; they are deliberately vague in order to avoid giving a direct answer to a question.</p>
<p class='rw-defn idx20' style='display:none'>Errant things or people behave in a way that is unacceptable or wrong, such as a missile that travels in the wrong direction or a student who does not follow classroom rules.</p>
<p class='rw-defn idx21' style='display:none'>One thing that exemplifies another serves as an example of it, illustrates it, or demonstrates it.</p>
<p class='rw-defn idx22' style='display:none'>If you think something, such as a statement or idea, is fatuous, you consider it stupid or extremely silly; a fatuous hope is unrealistic.</p>
<p class='rw-defn idx23' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx24' style='display:none'>A hapless person is unlucky, so much so that a stream of unfortunate things has happened and continues to happen to them.</p>
<p class='rw-defn idx25' style='display:none'>If you describe someone&#8217;s appearance or behavior as impeccable, you mean that it is perfect and therefore impossible to criticize.</p>
<p class='rw-defn idx26' style='display:none'>If you describe someone&#8217;s behavior as inane, you think it is completely stupid or without much meaning.</p>
<p class='rw-defn idx27' style='display:none'>If the results of something are inconclusive, they are uncertain or provide no final answers.</p>
<p class='rw-defn idx28' style='display:none'>Someone who is ineffectual at a task is useless or ineffective when attempting to do it.</p>
<p class='rw-defn idx29' style='display:none'>If something is infallible, it is never wrong and so is incapable of making mistakes.</p>
<p class='rw-defn idx30' style='display:none'>An inference is a conclusion that is reached using available data.</p>
<p class='rw-defn idx31' style='display:none'>Someone, such as a performer or athlete, is inimitable when they are so good or unique in their talent that it is unlikely anyone else can be their equal.</p>
<p class='rw-defn idx32' style='display:none'>An irrefutable argument or statement cannot be proven wrong; therefore, it must be accepted because it is certain.</p>
<p class='rw-defn idx33' style='display:none'>If you describe ideas as jejune, you are saying they are childish and uninteresting; the adjective jejune also describes those having little experience, maturity, or knowledge.</p>
<p class='rw-defn idx34' style='display:none'>If you do something in a lackadaisical way, you do it carelessly and without putting much effort into it—thereby showing that you are not really interested in what you&#8217;re doing.</p>
<p class='rw-defn idx35' style='display:none'>A luminary is someone who is much admired in a particular profession because they are an accomplished expert in their field.</p>
<p class='rw-defn idx36' style='display:none'>Something mediocre is average or ordinary in quality; it’s just OK.</p>
<p class='rw-defn idx37' style='display:none'>Someone who is meritorious is worthy of receiving recognition or is praiseworthy because of what they have accomplished.</p>
<p class='rw-defn idx38' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx39' style='display:none'>Something nonpareil has no equal because it is much better than all others of its kind or type.</p>
<p class='rw-defn idx40' style='display:none'>A paragon is an example of a thing or person at its or their very best.</p>
<p class='rw-defn idx41' style='display:none'>A perfunctory action is done with little care or interest; consequently, it is only completed because it is expected of someone to do so.</p>
<p class='rw-defn idx42' style='display:none'>When you personify something, such as a quality, you are the perfect example of it.</p>
<p class='rw-defn idx43' style='display:none'>Something prosaic is dull, boring, and ordinary.</p>
<p class='rw-defn idx44' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>
<p class='rw-defn idx45' style='display:none'>When you have been remiss, you have been careless because you did not do something that you should have done.</p>
<p class='rw-defn idx46' style='display:none'>A sagacious person is wise, intelligent, and has the ability to make good practical decisions.</p>
<p class='rw-defn idx47' style='display:none'>If someone is unlettered, they are illiterate, unable to read and write.</p>
<p class='rw-defn idx48' style='display:none'>Something in the vanguard is in the leading spot of something, such as an army or institution.</p>
<p class='rw-defn idx49' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx50' style='display:none'>The verity of something is the truth or reality of it.</p>
<p class='rw-defn idx51' style='display:none'>A virtuoso is someone who is very skillful at something, especially playing a musical instrument.</p>
<p class='rw-defn idx52' style='display:none'>If someone vitiates something, they make it less effective, weaker, or reduced in quality.</p>
<p class='rw-defn idx53' style='display:none'>The zenith of something is its highest, most powerful, or most successful point.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>definitive</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='definitive#' id='pronounce-sound' path='audio/words/amy-definitive'></a>
dih-FIN-i-tiv
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='definitive#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='definitive#' id='context-sound' path='audio/wordcontexts/brian-definitive'></a>
When it comes to Greek mythology, the book by Robert Graves is the <em>definitive</em> or most authoritative work on the subject.  Graves was a great scholar, so any questions about any of the Greek gods should be answered by his book, which is both <em>definitive</em> and conclusive.  This <em>definitive</em> book, which is beyond questioning, is the final word on the world of the Greek myths.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>definitive</em> statement?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It becomes a popularly quoted phrase within a group of people.
</li>
<li class='choice answer '>
<span class='result'></span>
It comes from the highest authority and so cannot be questioned.
</li>
<li class='choice '>
<span class='result'></span>
It is proven false by a great scholar whom many people respect.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='definitive#' id='definition-sound' path='audio/wordmeanings/amy-definitive'></a>
A <em>definitive</em> opinion on an issue cannot be challenged; therefore, it is the final word or the most authoritative pronouncement on that issue.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>authoritative</em>
</span>
</span>
</div>
<a class='quick-help' href='definitive#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='definitive#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/definitive/memory_hooks/4708.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Definit</span></span>e Answers G<span class="emp2"><span>ive</span></span>n</span></span> A <span class="emp1"><span>definit</span></span><span class="emp2"><span>ive</span></span> book on any subject will g<span class="emp2"><span>ive</span></span> you <span class="emp1"><span>definit</span></span>e answers that you can trust.
</p>
</div>

<div id='memhook-button-bar'>
<a href="definitive#" id="add-public-hook" style="" url="https://membean.com/mywords/definitive/memory_hooks">Use other public hook</a>
<a href="definitive#" id="memhook-use-own" url="https://membean.com/mywords/definitive/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='definitive#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
This is hardly <b>definitive</b>, given the small sample, but it does caution against jumping to the conclusion that economists are biased simply because of their relationships with firms.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Officials say technical analysis suggests the voice and inflection and movements of the mouth may be the same as Saddam Hussein from past tapes—though there is not yet a <b>definitive</b> U.S. judgment . . . . The White House said no one in the U.S. government has drawn any <b>definitive</b> conclusions about whether the man seen addressing the Iraqi public was Saddam.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
"Whinny the Pooh" - The <b>definitive</b> answer to the question: What do you get when you cross a horse with a teddy bear? (from a Word Game/Homonym Quiz)
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/definitive/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='definitive#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='de_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='definitive#'>
<span class='common'></span>
de-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='fin_end' data-tree-url='//cdn1.membean.com/public/data/treexml' href='definitive#'>
<span class='common'></span>
fin
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>end, form a boundary</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ive_does' data-tree-url='//cdn1.membean.com/public/data/treexml' href='definitive#'>
<span class=''></span>
-ive
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or that which does something</td>
</tr>
</table>
<p>A <em>definitive</em> conclusion has formed a &#8220;thorough boundary&#8221; past which one cannot go; it has &#8220;thoroughly ended&#8221; any arguments about what it means.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='definitive#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Mean Mr. Mayo</strong><span> A definitive opinion on one of the greatest rock and roll songs of all time.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/definitive.jpg' video_url='examplevids/definitive' video_width='350'></span>
<div id='wt-container'>
<img alt="Definitive" height="288" src="https://cdn1.membean.com/video/examplevids/definitive.jpg" width="350" />
<div class='center'>
<a href="definitive#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='definitive#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Definitive" src="https://cdn2.membean.com/public/images/wordimages/cons2/definitive.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='definitive#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>acme</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>adept</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>adroit</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>aphorism</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>archetype</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>axiom</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>bravura</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>caliber</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>connoisseur</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>eclipse</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>efficacy</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>embodiment</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>emphatic</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>encompass</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>epitome</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>exemplify</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>impeccable</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>infallible</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>inference</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>inimitable</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>irrefutable</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>luminary</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>meritorious</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>nonpareil</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>paragon</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>personify</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>sagacious</span> &middot;</li><li data-idx='48' class = 'rw-wordform notlearned'><span>vanguard</span> &middot;</li><li data-idx='49' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='50' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li><li data-idx='51' class = 'rw-wordform notlearned'><span>virtuoso</span> &middot;</li><li data-idx='53' class = 'rw-wordform notlearned'><span>zenith</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>ambiguous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>dilettante</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>equivocate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>errant</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>fatuous</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>hapless</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>inane</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>inconclusive</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>ineffectual</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>jejune</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>lackadaisical</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>mediocre</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>perfunctory</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>prosaic</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>remiss</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>unlettered</span> &middot;</li><li data-idx='52' class = 'rw-wordform notlearned'><span>vitiate</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="definitive" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>definitive</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="definitive#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

