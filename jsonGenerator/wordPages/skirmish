
<!DOCTYPE html>
<html>
<head>
<title>Word: skirmish | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you accede to a demand or proposal, you agree to it, especially after first disagreeing with it.</p>
<p class='rw-defn idx1' style='display:none'>You accost a stranger when you move towards them and speak in an unpleasant or threatening way.</p>
<p class='rw-defn idx2' style='display:none'>An acrimonious meeting or discussion is bitter, resentful, and filled with angry contention.</p>
<p class='rw-defn idx3' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx4' style='display:none'>You affront someone by openly and intentionally offending or insulting him.</p>
<p class='rw-defn idx5' style='display:none'>An altercation is a noisy disagreement or heated argument.</p>
<p class='rw-defn idx6' style='display:none'>Amity is a peaceful friendship between two parties, particularly between two countries.</p>
<p class='rw-defn idx7' style='display:none'>If you have animus against someone, you have a strong feeling of dislike or hatred towards them, often without a good reason or based upon your personal prejudices.</p>
<p class='rw-defn idx8' style='display:none'>Your antagonist is an opponent in a competition or battle.</p>
<p class='rw-defn idx9' style='display:none'>Antipathy is a strong feeling of dislike or hostility towards someone or something.</p>
<p class='rw-defn idx10' style='display:none'>Bedlam is a situation with a great deal of noise and confusion.</p>
<p class='rw-defn idx11' style='display:none'>If you are bellicose, you behave in an aggressive way and are likely to start an argument or fight.</p>
<p class='rw-defn idx12' style='display:none'>A belligerent person or country is aggressive, very unfriendly, and likely to start a fight.</p>
<p class='rw-defn idx13' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx14' style='display:none'>Conciliation is a process intended to end an argument between two groups of people.</p>
<p class='rw-defn idx15' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx16' style='display:none'>When someone is contentious, they are argumentative and likely to provoke a fight.</p>
<p class='rw-defn idx17' style='display:none'>A situation or thing that is discordant does not fit in with other things; therefore, it is disagreeable, strange, or unpleasant.</p>
<p class='rw-defn idx18' style='display:none'>Dissension is a disagreement or difference of opinion among a group of people that can cause conflict.</p>
<p class='rw-defn idx19' style='display:none'>Dissonance is an unpleasant situation of opposition in which ideas or actions are not in agreement or harmony; dissonance also refers to a harsh combination of sounds.</p>
<p class='rw-defn idx20' style='display:none'>Effrontery is very rude behavior that shows a great lack of respect and is often insulting.</p>
<p class='rw-defn idx21' style='display:none'>Empathy is the ability to understand how people feel because you can imagine what it is to be like them.</p>
<p class='rw-defn idx22' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx23' style='display:none'>A fracas is a rough and noisy fight or loud argument that can involve multiple people.</p>
<p class='rw-defn idx24' style='display:none'>When two people are in a harmonious state, they are in agreement with each other; when a sound is harmonious, it is pleasant or agreeable to the ear.</p>
<p class='rw-defn idx25' style='display:none'>When there is havoc, there is great disorder, widespread destruction, and much confusion.</p>
<p class='rw-defn idx26' style='display:none'>If someone is inimical, they are unfriendly and will likely cause you harm.</p>
<p class='rw-defn idx27' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx28' style='display:none'>A maelstrom is either a large whirlpool in the sea or a violent or agitated state of affairs.</p>
<p class='rw-defn idx29' style='display:none'>A melee is a noisy, confusing, hand-to-hand fight involving a group of people.</p>
<p class='rw-defn idx30' style='display:none'>Pandemonium is a very noisy and uncontrolled situation, especially one that is caused by a lot of angry or excited people.</p>
<p class='rw-defn idx31' style='display:none'>A polemic is a strong written or spoken statement that usually attacks or less often defends a particular idea, opinion, or belief.</p>
<p class='rw-defn idx32' style='display:none'>To act in a pugnacious manner is to act in a combative and aggressive way.</p>
<p class='rw-defn idx33' style='display:none'>Rapprochement is the development of greater understanding and friendliness between two countries or groups of people after a period of unfriendly relations.</p>
<p class='rw-defn idx34' style='display:none'>A serene place or situation is peaceful and calm.</p>
<p class='rw-defn idx35' style='display:none'>Strife is struggle or conflict that sometimes turns violent.</p>
<p class='rw-defn idx36' style='display:none'>If something is tranquil, it is peaceful, calm, and quiet.</p>
<p class='rw-defn idx37' style='display:none'>A truculent person is bad-tempered, easily annoyed, and prone to arguing excessively.</p>
<p class='rw-defn idx38' style='display:none'>A tumultuous event or period of time is filled with great excitement, confusion, or violence; a tumultuous reaction to something is likewise very loud and noisy because people are happy and excited.</p>
<p class='rw-defn idx39' style='display:none'>A vendetta is a prolonged situation in which one person or group tries to harm another person or group—and vice versa.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>skirmish</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='skirmish#' id='pronounce-sound' path='audio/words/amy-skirmish'></a>
SKUR-mish
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='skirmish#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='skirmish#' id='context-sound' path='audio/wordcontexts/brian-skirmish'></a>
There was a <em>skirmish</em> or fight outside police headquarters.  In the <em>skirmish</em> or brief conflict, a few people suffered some minor bruising, but there were also some serious injuries.  Fortunately the fighters in the <em>skirmish</em> or violent quarrel were too few to overwhelm the authorities.  Hopefully next time any <em>skirmishes</em> will simply involve verbal combat and not physical blows!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>skirmish</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
The lecture your teacher gives your class daily on using neater handwriting.
</li>
<li class='choice answer '>
<span class='result'></span>
The noisy argument that happened at lunch today between older and younger students.
</li>
<li class='choice '>
<span class='result'></span>
The informal get-together that happens in your neighborhood park once a month.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='skirmish#' id='definition-sound' path='audio/wordmeanings/amy-skirmish'></a>
A <em>skirmish</em> is a minor battle between small army units; it can also be a fight or argument between individuals.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>fight</em>
</span>
</span>
</div>
<a class='quick-help' href='skirmish#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='skirmish#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/skirmish/memory_hooks/5956.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Skewer</span></span> F<span class="emp3"><span>ish</span></span></span></span> In the <span class="emp1"><span>skir</span></span>m<span class="emp3"><span>ish</span></span> we are going to <span class="emp1"><span>skewer</span></span> a few of the enemy like f<span class="emp3"><span>ish</span></span>, and then break off combat in the hopes of scaring away their larger force.
</p>
</div>

<div id='memhook-button-bar'>
<a href="skirmish#" id="add-public-hook" style="" url="https://membean.com/mywords/skirmish/memory_hooks">Use other public hook</a>
<a href="skirmish#" id="memhook-use-own" url="https://membean.com/mywords/skirmish/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='skirmish#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
A melee erupted between the Orioles and the Mariners that lasted longer—and was much scarier—than a typical baseball <b>skirmish</b>. The pleasing panorama of a sold-out Oriole park at Camden yards was defaced on Sunday by one of the ugliest baseball brawls in years, a violent fight between the Baltimore Orioles and the Seattle Mariners that interrupted their game for [twenty] minutes.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Muzzle-loading muskets and cannon clattered and smoked as Colonial-uniformed troops with fixed bayonets led a wild charge against a 200-year-old earthen fortress here this afternoon. The re-enacted <b>skirmish</b> was the first of daily exercises in the military pageantry being presented by the Yorktown Bicentennial Brigade.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/skirmish/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='skirmish#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>From a root word meaning &#8220;to fight with a sword.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='skirmish#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>BBC Wildlife</strong><span> Two silverback gorillas skirmish in the jungle.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/skirmish.jpg' video_url='examplevids/skirmish' video_width='350'></span>
<div id='wt-container'>
<img alt="Skirmish" height="198" src="https://cdn1.membean.com/video/examplevids/skirmish.jpg" width="350" />
<div class='center'>
<a href="skirmish#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='skirmish#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Skirmish" src="https://cdn0.membean.com/public/images/wordimages/cons2/skirmish.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='skirmish#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>accost</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>acrimonious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>affront</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>altercation</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>animus</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>antagonist</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>antipathy</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>bedlam</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>bellicose</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>belligerent</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>contentious</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>discordant</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>dissension</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>dissonance</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>effrontery</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>fracas</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>havoc</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>inimical</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>maelstrom</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>melee</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>pandemonium</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>polemic</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>pugnacious</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>strife</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>truculent</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>tumultuous</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>vendetta</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>accede</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>amity</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>conciliation</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>empathy</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>harmonious</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>rapprochement</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>serene</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>tranquil</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='skirmish#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
skirmish
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to engage in an argument or minor battle</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="skirmish" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>skirmish</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="skirmish#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

