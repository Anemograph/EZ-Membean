
<!DOCTYPE html>
<html>
<head>
<title>Word: laudatory | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Laudatory-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/laudatory-large.jpg?qdep8" />
</div>
<a href="laudatory#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>An acrimonious meeting or discussion is bitter, resentful, and filled with angry contention.</p>
<p class='rw-defn idx1' style='display:none'>Adulation is praise and admiration for someone that includes more than they deserve, usually for the purposes of flattery.</p>
<p class='rw-defn idx2' style='display:none'>When someone offers an appraisal of something, they indicate the value or worth of it.</p>
<p class='rw-defn idx3' style='display:none'>The adjective auspicious describes a positive beginning of something, such as a new business, or a certain time that looks to have a good chance of success or prosperity.</p>
<p class='rw-defn idx4' style='display:none'>Someone who has a bristling personality is easily offended, annoyed, or angered.</p>
<p class='rw-defn idx5' style='display:none'>To carp at someone is to complain about them to their face or to nag and criticize them in a whining voice.</p>
<p class='rw-defn idx6' style='display:none'>To cavil is to make unnecessary complaints about things that are unimportant.</p>
<p class='rw-defn idx7' style='display:none'>A commendation is a giving of praise to someone who has won approval for excellent work.</p>
<p class='rw-defn idx8' style='display:none'>If someone is deified, they have been either made into a god or are adored like one.</p>
<p class='rw-defn idx9' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx10' style='display:none'>If you disparage someone or something, you say unpleasant words that show you have no respect for that person or thing.</p>
<p class='rw-defn idx11' style='display:none'>An encomium strongly praises someone or something via oral or written communication.</p>
<p class='rw-defn idx12' style='display:none'>A eulogy is a speech or other piece of writing, often part of a funeral, in which someone or something is highly praised.</p>
<p class='rw-defn idx13' style='display:none'>When you exalt another person, you either praise them highly or promote them to a higher position in an organization.</p>
<p class='rw-defn idx14' style='display:none'>If you extol something or someone, you praise it or them very enthusiastically.</p>
<p class='rw-defn idx15' style='display:none'>If you believe someone&#8217;s behavior exhibits idolatry, you think they blindly admire someone or something too much.</p>
<p class='rw-defn idx16' style='display:none'>If you hurl invective at another person, you are verbally abusing or highly criticizing them.</p>
<p class='rw-defn idx17' style='display:none'>When someone is lionized, they are treated as being very important or famous—although they may not deserve to be.</p>
<p class='rw-defn idx18' style='display:none'>Something mediocre is average or ordinary in quality; it’s just OK.</p>
<p class='rw-defn idx19' style='display:none'>Someone who is meritorious is worthy of receiving recognition or is praiseworthy because of what they have accomplished.</p>
<p class='rw-defn idx20' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx21' style='display:none'>A paean is a piece of writing, verbal expression, or song that expresses enthusiastic praise, admiration, or joy.</p>
<p class='rw-defn idx22' style='display:none'>A panegyric is a speech or article that praises someone or something a lot.</p>
<p class='rw-defn idx23' style='display:none'>If you receive a plaudit, you receive admiration, praise, and approval from someone.</p>
<p class='rw-defn idx24' style='display:none'>Something prosaic is dull, boring, and ordinary.</p>
<p class='rw-defn idx25' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx26' style='display:none'>To remonstrate with someone is to tell that person that you strongly disapprove of something they have said or done.</p>
<p class='rw-defn idx27' style='display:none'>If you repudiate something, you state that you do not accept or agree with it and no longer want to be connected with it in any way.</p>
<p class='rw-defn idx28' style='display:none'>Something vapid is dull, boring, and/or tiresome.</p>
<p class='rw-defn idx29' style='display:none'>Venerable people command respect because they are old and wise.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>laudatory</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='laudatory#' id='pronounce-sound' path='audio/words/amy-laudatory'></a>
LAW-duh-tawr-ee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='laudatory#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='laudatory#' id='context-sound' path='audio/wordcontexts/brian-laudatory'></a>
In her <em>laudatory</em> address to the graduating class, the guest speaker praised the students&#8217; achievements and wished them well.  She <em>lauded</em> or complimented their energy, great work ethic, and wisdom, all of which the world needed.  Her <em>laudatory</em>, admiring, and flattering words touched the audience and even made some of the graduates blush as they were overwhelmed by gratitude.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What are <em>laudatory</em> remarks?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A speech that is part of a formal occasion.
</li>
<li class='choice '>
<span class='result'></span>
A farewell address by an honored guest.
</li>
<li class='choice answer '>
<span class='result'></span>
Words that praise and admire someone.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='laudatory#' id='definition-sound' path='audio/wordmeanings/amy-laudatory'></a>
A <em>laudatory</em> article or speech expresses praise or admiration for someone or something.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>complimentary</em>
</span>
</span>
</div>
<a class='quick-help' href='laudatory#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='laudatory#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/laudatory/memory_hooks/3266.json'></span>
<p>
<span class="emp0"><span>S<span class="emp3"><span>tory</span></span> App<span class="emp2"><span>laud</span></span>ed</span></span> The s<span class="emp3"><span>tory</span></span> told by the traveling s<span class="emp3"><span>tory</span></span>teller was so good that not only was there a great deal of app<span class="emp2"><span>laud</span></span>ing by the standing audience after the performance, but there were many <span class="emp2"><span>laud</span></span>a<span class="emp3"><span>tory</span></span> remarks afterward about her wonderful s<span class="emp3"><span>tory</span></span>telling ability.
</p>
</div>

<div id='memhook-button-bar'>
<a href="laudatory#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/laudatory/memory_hooks">Use other hook</a>
<a href="laudatory#" id="memhook-use-own" url="https://membean.com/mywords/laudatory/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='laudatory#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Pictures of Mrs Gandhi holding her hand when the announcement was made were splashed across the newspapers, with <b>laudatory</b> reports of Mrs Patil’s background as a socially conscious lawyer.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Charismatic, plain-spoken and accessible to the press, he has parlayed a bold idea, a huge marketing budget and <b>laudatory</b> clippings into a business that has a large following and an eye-popping valuation.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Roughly one-third of the book is literary critique and presentation of Cummings’s poetry, and the biographer’s stance is blatantly <b>laudatory</b> and sympathetic rather than objective and analytical.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
“When you never hear anything from him, you’re doing a great job,” says McMahon, “because he doesn’t constantly send you <b>laudatory</b> phrases or gestures. It’s just assumed you’re doing a good job, or you wouldn’t be there.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/laudatory/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='laudatory#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='laud_praise' data-tree-url='//cdn1.membean.com/public/data/treexml' href='laudatory#'>
<span class=''></span>
laud
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>praise</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ory_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='laudatory#'>
<span class=''></span>
-ory
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p><em>Laudatory</em> comments are &#8220;related to praise.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='laudatory#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Laudatory" src="https://cdn2.membean.com/public/images/wordimages/cons2/laudatory.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='laudatory#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>adulation</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>appraisal</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>auspicious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>commendation</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>deify</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>encomium</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>eulogy</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>exalt</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>extol</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>idolatry</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>lionize</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>meritorious</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>paean</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>panegyric</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>plaudit</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>venerable</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>acrimonious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>bristling</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>carp</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>cavil</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>disparage</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>invective</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>mediocre</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>prosaic</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>remonstrate</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>repudiate</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>vapid</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='laudatory#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
laud
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to praise</td>
</tr>
<tr>
<td class='wordform'>
<span>
laudable
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>worthy of praise</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="laudatory" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>laudatory</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="laudatory#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

