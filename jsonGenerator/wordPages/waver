
<!DOCTYPE html>
<html>
<head>
<title>Word: waver | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you accede to a demand or proposal, you agree to it, especially after first disagreeing with it.</p>
<p class='rw-defn idx1' style='display:none'>If you acquiesce to something, you allow it to happen by doing what someone wants without putting up a struggle or voicing your own concerns.</p>
<p class='rw-defn idx2' style='display:none'>If you describe something as ambiguous, you mean that it is unclear or confusing because it can be understood in multiple ways.</p>
<p class='rw-defn idx3' style='display:none'>If you deal with a difficult situation with aplomb, you deal with it in a confident and skillful way.</p>
<p class='rw-defn idx4' style='display:none'>When you capitulate, you accept or agree to do something after having resisted doing so for a long time.</p>
<p class='rw-defn idx5' style='display:none'>Someone cedes land or power to someone else by giving it to them, often because of political or military pressure.</p>
<p class='rw-defn idx6' style='display:none'>If you demur, you delay in doing or mildly object to something because you don&#8217;t really want to do it.</p>
<p class='rw-defn idx7' style='display:none'>When you make an emphatic declaration, you are insistent and absolute about it.</p>
<p class='rw-defn idx8' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx9' style='display:none'>When people equivocate, they avoid making a clear statement; they are deliberately vague in order to avoid giving a direct answer to a question.</p>
<p class='rw-defn idx10' style='display:none'>Things that fluctuate vary or change often, rising or falling seemingly at random.</p>
<p class='rw-defn idx11' style='display:none'>Fortitude is the determination or lasting courage to endure hardship or difficulty over an extended period of time.</p>
<p class='rw-defn idx12' style='display:none'>The word homogeneous is used to describe a group that has members or parts which are similar or are all of the same type.</p>
<p class='rw-defn idx13' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx14' style='display:none'>If someone is imperturbable, they are always calm and not easily upset or disturbed by any situation, even dangerous ones.</p>
<p class='rw-defn idx15' style='display:none'>The adjective inexorable describes a process that is impossible to stop once it has begun.</p>
<p class='rw-defn idx16' style='display:none'>Something that happens on an intermittent basis happens in irregular intervals, stopping and starting at unpredictable times.</p>
<p class='rw-defn idx17' style='display:none'>Someone who is irresolute is unable to decide what to do.</p>
<p class='rw-defn idx18' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx19' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx20' style='display:none'>If an object oscillates, it moves repeatedly from one point to another and then back again; if you oscillate between two moods or attitudes, you keep changing from one to the other.</p>
<p class='rw-defn idx21' style='display:none'>When you resolve a problem, you solve it or come to a decision about it.</p>
<p class='rw-defn idx22' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx23' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx24' style='display:none'>A stoic person does not show their emotions and does not complain when bad things happen to them.</p>
<p class='rw-defn idx25' style='display:none'>Something that is tortuous, such as a piece of writing, is long and complicated with many twists and turns in direction; a tortuous argument can be deceitful because it twists or turns the truth.</p>
<p class='rw-defn idx26' style='display:none'>Something that undulates moves or is shaped like waves with gentle curves that smoothly rise and fall.</p>
<p class='rw-defn idx27' style='display:none'>When someone vacillates, they go back and forth about a choice or opinion, unable to make a firm decision.</p>
<p class='rw-defn idx28' style='display:none'>A vagary is an unpredictable or unexpected change in action or behavior.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>waver</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='waver#' id='pronounce-sound' path='audio/words/amy-waver'></a>
WAY-ver
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='waver#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='waver#' id='context-sound' path='audio/wordcontexts/brian-waver'></a>
I <em>waver</em> on the issue of wearing deodorant because I can&#8217;t decide whether I should wear it or not.  I notice my friends, too, have <em>wavering</em> and shifting views on my personal hygiene choices.  When I do wear deodorant, no one seems to <em>waver</em> or hesitate about asking me to go to the movies or out to dinner.  When I don&#8217;t wear it, however, my friends seem to <em>waver</em> or go back and forth about whether they want to associate with me!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Why might someone <em>waver</em> on an issue?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They feel strongly about the issue and hope to convince others of their opinion.
</li>
<li class='choice answer '>
<span class='result'></span>
They don&#8217;t know which side of the issue they should take.
</li>
<li class='choice '>
<span class='result'></span>
They are tired of hearing about the issue and want to ignore it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='waver#' id='definition-sound' path='audio/wordmeanings/amy-waver'></a>
If you <em>waver</em>, you cannot decide between two things because you have serious doubts about which choice is better.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>be indecisive</em>
</span>
</span>
</div>
<a class='quick-help' href='waver#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='waver#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/waver/memory_hooks/5311.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Er</span></span> ... <span class="emp3"><span>Wav</span></span>e My Hand Which <span class="emp3"><span>Way</span></span>?</span></span>  "<span class="emp1"><span>Er</span></span>, now, <span class="emp1"><span>er</span></span>, did she say to <span class="emp3"><span>wav</span></span>e my hand to the left when I'm crossing the road?  No, <span class="emp1"><span>er</span></span>, I think she said to the right, no, <span class="emp1"><span>er</span></span>, well, <span class="emp3"><span>wav</span></span><span class="emp1"><span>er</span></span>ing concerning which <span class="emp3"><span>way</span></span> to <span class="emp3"><span>wav</span></span>e won't help, so I'll, <span class="emp1"><span>er</span></span>, <span class="emp3"><span>wav</span></span>e both <span class="emp3"><span>way</span></span>s ... oh no!!"
</p>
</div>

<div id='memhook-button-bar'>
<a href="waver#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/waver/memory_hooks">Use other hook</a>
<a href="waver#" id="memhook-use-own" url="https://membean.com/mywords/waver/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='waver#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Grief teaches the steadiest minds to <b>waver</b>.
<span class='attribution'>&mdash; Sophocles, _Antigone_</span>
<img alt="Sophocles, _antigone_" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Sophocles, _Antigone_.jpg?qdep8" width="80" />
</li>
<li>
There are basic principles that are universal; there are certain truths which are self-evident—and the United States of America will never <b>waver</b> in our efforts to stand up for the right of people everywhere to determine their own destiny.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Only just beginning her basketball career in her teens, Crystal [Langhorne] hoped to play whenever she could but accepted that her father likely wouldn’t <b>waver</b> from his decision.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
The treaty included provisions not entirely favorable to the Muslims, but the Prophet did not <b>waver</b> in his resolve to sign this agreement.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/waver/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='waver#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>wav</td>
<td>
&rarr;
</td>
<td class='meaning'>wobble, falter</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='er_do' data-tree-url='//cdn1.membean.com/public/data/treexml' href='waver#'>
<span class=''></span>
-er
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to do something</td>
</tr>
</table>
<p>One &#8220;falters&#8221; or &#8220;wobbles&#8221; when <em>wavering</em>.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='waver#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Waver" src="https://cdn3.membean.com/public/images/wordimages/cons2/waver.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='waver#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>ambiguous</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>demur</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>equivocate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>fluctuate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>intermittent</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>irresolute</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>oscillate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>tortuous</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>undulate</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>vacillate</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>vagary</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>accede</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>acquiesce</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>aplomb</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>capitulate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>cede</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>emphatic</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>fortitude</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>homogeneous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>imperturbable</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>inexorable</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>resolve</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>stoic</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="waver" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>waver</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="waver#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

