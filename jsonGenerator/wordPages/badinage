
<!DOCTYPE html>
<html>
<head>
<title>Word: badinage | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Someone who has an abrasive manner is unkind and rude, wearing away at you in an irritating fashion.</p>
<p class='rw-defn idx1' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx2' style='display:none'>When you bandy about ideas with someone, you casually discuss them without caring if the discussion is informed or effective in any way.</p>
<p class='rw-defn idx3' style='display:none'>Banter is friendly conversation during which people make friendly jokes and laugh at—and with—one another.</p>
<p class='rw-defn idx4' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx5' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx6' style='display:none'>Camaraderie is a feeling of friendship and trust among a group of people who have usually known each over a long period of time.</p>
<p class='rw-defn idx7' style='display:none'>Censure is written or verbal disapproval and harsh criticism of something that someone has done.</p>
<p class='rw-defn idx8' style='display:none'>To decry something is to speak against it and find fault with it.</p>
<p class='rw-defn idx9' style='display:none'>To defile something holy or important is to do something to it or say something about it that is highly offensive to others.</p>
<p class='rw-defn idx10' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx11' style='display:none'>If you deprecate something, you disapprove of it strongly.</p>
<p class='rw-defn idx12' style='display:none'>If you treat someone with derision, you mock or make fun of them because you think they are stupid, unimportant, or useless.</p>
<p class='rw-defn idx13' style='display:none'>If you disparage someone or something, you say unpleasant words that show you have no respect for that person or thing.</p>
<p class='rw-defn idx14' style='display:none'>Something that is dolorous, such as music or news, causes mental pain and sorrow because it itself is full of grief and sorrow.</p>
<p class='rw-defn idx15' style='display:none'>Someone who is dour is serious, stubborn, and unfriendly; they can also be gloomy.</p>
<p class='rw-defn idx16' style='display:none'>When something is droll, it is humorous in an odd way.</p>
<p class='rw-defn idx17' style='display:none'>Someone who is being facetious says things they intend to be funny but are nevertheless out of place.</p>
<p class='rw-defn idx18' style='display:none'>When a person is being flippant, they are not taking something as seriously as they should be; therefore, they tend to dismiss things they should respectfully pay attention to.</p>
<p class='rw-defn idx19' style='display:none'>The gravity of a situation or event is its seriousness or importance.</p>
<p class='rw-defn idx20' style='display:none'>Someone who is jocular is cheerful and often makes jokes or tries to make people laugh.</p>
<p class='rw-defn idx21' style='display:none'>Someone who is jovial is in a good humor, lighthearted, and jolly.</p>
<p class='rw-defn idx22' style='display:none'>Levity is an amusing way of speaking or behaving during a serious situation; it can lighten the moment but can also be considered inappropriate.</p>
<p class='rw-defn idx23' style='display:none'>If you describe something as ludicrous, you mean that it is extremely silly, stupid, or just plain ridiculous.</p>
<p class='rw-defn idx24' style='display:none'>If someone is lugubrious, they are looking very sad or gloomy.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is morose is unhappy, bad-tempered, and unwilling to talk very much.</p>
<p class='rw-defn idx26' style='display:none'>Someone who is saturnine is looking miserable and sad, sometimes in a threatening or unfriendly way.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>badinage</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='badinage#' id='pronounce-sound' path='audio/words/amy-badinage'></a>
BAD-n-azh
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='badinage#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='badinage#' id='context-sound' path='audio/wordcontexts/brian-badinage'></a>
The carefree, joking conversation or <em>badinage</em> among the band members eased their nervousness before going on stage.  The humorous <em>badinage</em> or verbal teasing between them reflected years of easy friendship and common experiences.  Part of their success as a musical group was their ability to exchange light, friendly <em>badinage</em> with each other even in stressful moments on tour or during a performance.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of <em>badinage</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
An exchange between friends who are joking with each other.
</li>
<li class='choice '>
<span class='result'></span>
A well-rehearsed statement made to the press by a politician.
</li>
<li class='choice '>
<span class='result'></span>
A targeted and cruel insult from a bully toward a student at recess.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='badinage#' id='definition-sound' path='audio/wordmeanings/amy-badinage'></a>
<em>Badinage</em> is lighthearted conversation that involves teasing, jokes, and humor.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>joking around</em>
</span>
</span>
</div>
<a class='quick-help' href='badinage#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='badinage#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/badinage/memory_hooks/4766.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Bad</span></span><span class="emp2"><span>in</span></span><span class="emp3"><span>age</span></span> Deafening</span></span>  I heard a <span class="emp1"><span>bad</span></span> and noisy <span class="emp2"><span>din</span></span> during the social hour of the Conference for Experimental Hearing Aids for the Extremely <span class="emp3"><span>Age</span></span>d; the joking <span class="emp1"><span>bad</span></span><span class="emp2"><span>in</span></span><span class="emp3"><span>age</span></span> was almost deafening in and of itself.
</p>
</div>

<div id='memhook-button-bar'>
<a href="badinage#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/badinage/memory_hooks">Use other hook</a>
<a href="badinage#" id="memhook-use-own" url="https://membean.com/mywords/badinage/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='badinage#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The patrol stops from time to time as its leader, a fresh-faced corporal from Chicago, engages passers-by, via Dave, the newly coiffed interpreter from Baghdad, in amiably stilted <b>badinage</b>.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Folger Theatre and director Richard Clifford have enlisted an ensemble with the chops for this sort of stylized aristocratic <b>badinage</b> and somewhat labored plot mechanics.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/badinage/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='badinage#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>badin</td>
<td>
&rarr;
</td>
<td class='meaning'>joker</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='age_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='badinage#'>
<span class=''></span>
-age
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state</td>
</tr>
</table>
<p>The idea behind <em>badinage</em> is that one can be a &#8220;joker&#8221; in conversation with others and not be taken seriously.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='badinage#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Badinage" src="https://cdn1.membean.com/public/images/wordimages/cons2/badinage.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='badinage#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bandy</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>banter</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>camaraderie</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>droll</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>facetious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>flippant</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>jocular</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>jovial</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>levity</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>ludicrous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abrasive</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>censure</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>decry</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>defile</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>deprecate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>derision</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>disparage</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>dolorous</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>dour</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>gravity</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>lugubrious</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>morose</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>saturnine</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="badinage" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>badinage</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="badinage#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

