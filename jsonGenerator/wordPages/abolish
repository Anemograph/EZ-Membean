
<!DOCTYPE html>
<html>
<head>
<title>Word: abolish | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>The abnegation of something is someone&#8217;s giving up their rights or claim to it or denying themselves of it; this action may or may not be in their best interest.</p>
<p class='rw-defn idx2' style='display:none'>To abrogate an act is to end it by official and sometimes legal means.</p>
<p class='rw-defn idx3' style='display:none'>An addendum is an additional section added to the end of a book, document, or speech that gives more information.</p>
<p class='rw-defn idx4' style='display:none'>To augment something is to increase its value or effectiveness by adding something to it.</p>
<p class='rw-defn idx5' style='display:none'>To corroborate something that has been said or reported means to give information or evidence that further supports it.</p>
<p class='rw-defn idx6' style='display:none'>If something, such as a sound, is undergoing a crescendo, it is getting louder and louder or increasing in intensity and is about to reach its peak strength.</p>
<p class='rw-defn idx7' style='display:none'>When you dispel a thought from your mind, you cause it to go away or disappear; when you do the same to a crowd, you cause it to scatter into different directions.</p>
<p class='rw-defn idx8' style='display:none'>Dissolution is the breaking up or official end of a group, such as a couple or institution; it can also be the act of separating something into smaller components.</p>
<p class='rw-defn idx9' style='display:none'>If something engenders a particular feeling or attitude, it causes that condition.</p>
<p class='rw-defn idx10' style='display:none'>If you ensconce yourself somewhere, you put yourself into a comfortable place or safe position and have no intention of moving or leaving for quite some time.</p>
<p class='rw-defn idx11' style='display:none'>When you eradicate something, you tear it up by the roots or remove it completely.</p>
<p class='rw-defn idx12' style='display:none'>If you expunge something, you completely remove it or do away with it.</p>
<p class='rw-defn idx13' style='display:none'>If you extirpate something, you completely destroy it because it is unpleasant or unwanted.</p>
<p class='rw-defn idx14' style='display:none'>Gestation is the process by which a new idea,  plan, or piece of work develops in your mind.</p>
<p class='rw-defn idx15' style='display:none'>Something that is ineluctable is impossible to avoid or escape, however much you try.</p>
<p class='rw-defn idx16' style='display:none'>An irrevocable action or decision is impossible to change, reverse, or stop.</p>
<p class='rw-defn idx17' style='display:none'>To liquidate a business or company is to close it down and sell the things that belong to it in order to pay off its debts.</p>
<p class='rw-defn idx18' style='display:none'>To nullify something is to cancel or negate it.</p>
<p class='rw-defn idx19' style='display:none'>When you obliterate something, you destroy it to such an extent that there is nothing or very little of it left.</p>
<p class='rw-defn idx20' style='display:none'>When you perpetuate something, you keep it going or continue it indefinitely.</p>
<p class='rw-defn idx21' style='display:none'>When things propagate, such as plants, animals, or ideas, they reproduce or generate greater numbers, causing them to spread.</p>
<p class='rw-defn idx22' style='display:none'>The ratification of a measure or agreement is its official approval or confirmation by all involved.</p>
<p class='rw-defn idx23' style='display:none'>When you ravage something, you completely destroy, wreck, or damage it.</p>
<p class='rw-defn idx24' style='display:none'>If you recant, you publicly announce that your once firmly held beliefs or statements were wrong and that you no longer agree with them.</p>
<p class='rw-defn idx25' style='display:none'>If you renege on a deal, agreement, or promise, you do not do what you promised or agreed to do.</p>
<p class='rw-defn idx26' style='display:none'>If you repudiate something, you state that you do not accept or agree with it and no longer want to be connected with it in any way.</p>
<p class='rw-defn idx27' style='display:none'>When someone in power rescinds a law or agreement, they officially end it and state that it is no longer valid.</p>
<p class='rw-defn idx28' style='display:none'>When something is scuttled, it is done away with or discarded.</p>
<p class='rw-defn idx29' style='display:none'>Something statutory, such as the power given to a governor or president, is created, established, and controlled by rules and laws; hence, it has the full force of the law behind it and must be followed.</p>
<p class='rw-defn idx30' style='display:none'>When you validate something, you confirm that it is sound, true, legal, or worthwhile.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>abolish</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='abolish#' id='pronounce-sound' path='audio/words/amy-abolish'></a>
uh-BOL-ish
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='abolish#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='abolish#' id='context-sound' path='audio/wordcontexts/brian-abolish'></a>
To <em>abolish</em> or end the cruel business of slavery in the United States, President Lincoln issued the Emancipation Proclamation, a document declaring slavery illegal.  By <em>abolishing</em> or doing away with the selling of human beings, Lincoln became a famous figure during his own lifetime and beyond.  In <em>abolishing</em> or putting an end to the long-standing slave trade, Lincoln made himself politically open to attack since slave owners were not supportive of this law.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If you <em>abolish</em> something, what are you doing?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
You are officially ending it.
</li>
<li class='choice '>
<span class='result'></span>
You are taking something old and making it seem new.
</li>
<li class='choice '>
<span class='result'></span>
You are telling everyone about it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='abolish#' id='definition-sound' path='audio/wordmeanings/amy-abolish'></a>
If someone in authority <em>abolishes</em> a law or practice, they formally put an end to it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>end</em>
</span>
</span>
</div>
<a class='quick-help' href='abolish#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='abolish#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/abolish/memory_hooks/5074.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Ab</span></span>e <span class="emp2"><span>Abol</span></span><span class="emp3"><span>ish</span></span>es the F<span class="emp3"><span>ish</span></span></span></span> <span class="emp2"><span>Able</span></span> <span class="emp2"><span>Ab</span></span>e was "<span class="emp2"><span>abol</span></span>" to <span class="emp2"><span>abol</span></span><span class="emp3"><span>ish</span></span> the smelly f<span class="emp3"><span>ish</span></span> on his d<span class="emp3"><span>ish</span></span> with one big sw<span class="emp3"><span>ish</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="abolish#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/abolish/memory_hooks">Use other hook</a>
<a href="abolish#" id="memhook-use-own" url="https://membean.com/mywords/abolish/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='abolish#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
And if Democracy is such an undesirable system of government, why is it that the Swiss, for example, have not used their democratic rights to <b>abolish</b> Democracy?
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
The long-term goal for social development is to <b>abolish</b> the death penalty but, until then, regulations need to protect prisoners' rights and desires....
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
Even in this past decade boxing has been in and out of [favor], suddenly glorified as some new star hits the scene, then as quickly assailed by legislators who would reform or <b>abolish</b> it.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/abolish/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='abolish#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='abol_destroy' data-tree-url='//cdn1.membean.com/public/data/treexml' href='abolish#'>
<span class=''></span>
abol
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>destroy, get rid of</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ish_do' data-tree-url='//cdn1.membean.com/public/data/treexml' href='abolish#'>
<span class=''></span>
-ish
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to do something</td>
</tr>
</table>
<p>To <em>abolish</em> is to &#8220;destroy&#8221; or &#8220;get rid of&#8221; something.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='abolish#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Back to the Future 2</strong><span> Lawyers have been abolished in the future.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/abolish.jpg' video_url='examplevids/abolish' video_width='350'></span>
<div id='wt-container'>
<img alt="Abolish" height="288" src="https://cdn1.membean.com/video/examplevids/abolish.jpg" width="350" />
<div class='center'>
<a href="abolish#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='abolish#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Abolish" src="https://cdn0.membean.com/public/images/wordimages/cons2/abolish.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='abolish#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abnegation</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abrogate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>dispel</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>dissolution</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>eradicate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>expunge</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>extirpate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>liquidate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>nullify</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>obliterate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>ravage</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>recant</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>renege</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>repudiate</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>rescind</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>scuttle</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>addendum</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>augment</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>corroborate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>crescendo</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>engender</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>ensconce</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>gestation</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>ineluctable</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>irrevocable</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>perpetuate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>propagate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>ratification</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>statutory</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>validate</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='abolish#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
abolition
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>the act of formally putting an end to</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="abolish" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>abolish</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="abolish#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

