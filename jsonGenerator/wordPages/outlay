
<!DOCTYPE html>
<html>
<head>
<title>Word: outlay | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Outlay-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/outlay-large.jpg?qdep8" />
</div>
<a href="outlay#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When you apportion something, such as blame or money, you decide how it should be shared among various people.</p>
<p class='rw-defn idx1' style='display:none'>Something austere is simple and plain in its style; an austere person is strict and severe with themselves.</p>
<p class='rw-defn idx2' style='display:none'>A deficit occurs when a person or government spends more money than has been received.</p>
<p class='rw-defn idx3' style='display:none'>To disburse is to pay out money, usually from a large fund that has been collected for a specific purpose.</p>
<p class='rw-defn idx4' style='display:none'>Emolument is money or another form of payment you get for work you have done.</p>
<p class='rw-defn idx5' style='display:none'>Someone who is frugal spends very little money—and even then only on things that are absolutely necessary.</p>
<p class='rw-defn idx6' style='display:none'>Two items are fungible if one can be exchanged for the other with no loss in inherent value; for example, one $10 bill and two $5 bills are fungible.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is impecunious has very little money, especially over a long period of time.</p>
<p class='rw-defn idx8' style='display:none'>An indigent person is very poor.</p>
<p class='rw-defn idx9' style='display:none'>If something is inordinate, it is much larger in amount or degree than is normally expected.</p>
<p class='rw-defn idx10' style='display:none'>A mercenary person is one whose sole interest is in earning money.</p>
<p class='rw-defn idx11' style='display:none'>A parsimonious person is not willing to give or spend money.</p>
<p class='rw-defn idx12' style='display:none'>Pecuniary means relating to or concerning money.</p>
<p class='rw-defn idx13' style='display:none'>Penury is the state of being extremely poor.</p>
<p class='rw-defn idx14' style='display:none'>Someone who behaves in a prodigal way spends a lot of money and/or time carelessly and wastefully with no concern for the future.</p>
<p class='rw-defn idx15' style='display:none'>When you offer recompense to someone, you give them something, usually money, for the trouble or loss that you have caused them or as payment for their help.</p>
<p class='rw-defn idx16' style='display:none'>If you redress a complaint or a bad situation, you correct or improve it for the person who has been wronged, usually by paying them money or offering an apology.</p>
<p class='rw-defn idx17' style='display:none'>Someone&#8217;s remuneration is the payment or other rewards they receive for work completed, goods provided, or services rendered.</p>
<p class='rw-defn idx18' style='display:none'>Restitution is the formal act of giving something that was taken away back to the rightful owner—or paying them money for the loss of the object.</p>
<p class='rw-defn idx19' style='display:none'>If you squander time, money, or other valuable resources, you use them wastefully and unwisely.</p>
<p class='rw-defn idx20' style='display:none'>Something that is sumptuous is impressive, grand, and very expensive.</p>
<p class='rw-defn idx21' style='display:none'>A wastrel is a lazy person who wastes time and money.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>outlay</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='outlay#' id='pronounce-sound' path='audio/words/amy-outlay'></a>
OUT-lay
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='outlay#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='outlay#' id='context-sound' path='audio/wordcontexts/brian-outlay'></a>
Although we all really want a new football stadium, we are wondering how much the <em>outlay</em> or expenses will be to build it.  Will an <em>outlay</em> or expenditure of one billion dollars even be enough?  We are also thinking about how long it will take to repay our initial <em>outlay</em> or investment.  Such an <em>outlay</em> or spending of money has much to be considered before we agree.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When is an <em>outlay</em> needed?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When a large storm is coming and people need to prepare for it.
</li>
<li class='choice '>
<span class='result'></span>
When a project must be done and people must agree on how to do it.
</li>
<li class='choice answer '>
<span class='result'></span>
When a large amount of money is needed for a big purchase.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='outlay#' id='definition-sound' path='audio/wordmeanings/amy-outlay'></a>
An <em>outlay</em> is how much money is spent on something.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>expenses</em>
</span>
</span>
</div>
<a class='quick-help' href='outlay#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='outlay#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/outlay/memory_hooks/5150.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Lay</span></span> <span class="emp2"><span>Out</span></span></span></span> An <span class="emp2"><span>out</span></span><span class="emp1"><span>lay</span></span> is how much money one <span class="emp1"><span>lay</span></span>s <span class="emp2"><span>out</span></span> upon the table to pay for something.
</p>
</div>

<div id='memhook-button-bar'>
<a href="outlay#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/outlay/memory_hooks">Use other hook</a>
<a href="outlay#" id="memhook-use-own" url="https://membean.com/mywords/outlay/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='outlay#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Proponents of this bill claim that the problem of Americans not being online today can only be resolved with a major <b>outlay</b> of government money in wireline networks.
<cite class='attribution'>
&mdash;
Forbes
</cite>
</li>
<li>
That’s the kind of <b>outlay</b> it takes to put on multiple plays, simultaneously, almost year-round, says Paul Nicholson, the theater’s executive director for 30 years.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
When Carson Wentz signed his four-year $128M extension with $107M of guarantees in June of 2019, many gasped at the largest <b>outlay</b> of guaranteed money in league history.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
The structuring of a new deal is being discussed in addition to the length and although it represents a considerable <b>outlay</b> for a player over 30, Arsenal recognise his central importance to head coach Mikel Arteta's plans.
<cite class='attribution'>
&mdash;
ESPN
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/outlay/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='outlay#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>An <em>outlay</em> is the money that one &#8220;lays out&#8221; for specific purposes.</p>
<a href="outlay#"><img alt="origin button" id="world-btn" src="https://www.membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> Old English</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='outlay#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Mo Vlogs</strong><span> The outlay for this car is paid in a quantity of something that you don't see every day.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/outlay.jpg' video_url='examplevids/outlay' video_width='350'></span>
<div id='wt-container'>
<img alt="Outlay" height="288" src="https://cdn1.membean.com/video/examplevids/outlay.jpg" width="350" />
<div class='center'>
<a href="outlay#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='outlay#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Outlay" src="https://cdn0.membean.com/public/images/wordimages/cons2/outlay.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='outlay#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>apportion</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>deficit</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>disburse</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>emolument</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>fungible</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>inordinate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>mercenary</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>pecuniary</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>prodigal</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>recompense</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>redress</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>remuneration</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>restitution</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>squander</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>sumptuous</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>wastrel</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>austere</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>frugal</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>impecunious</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>indigent</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>parsimonious</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>penury</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="outlay" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>outlay</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="outlay#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

