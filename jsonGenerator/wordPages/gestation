
<!DOCTYPE html>
<html>
<head>
<title>Word: gestation | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Gestation-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/gestation-large.jpg?qdep8" />
</div>
<a href="gestation#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>If someone in authority abolishes a law or practice, they formally put an end to it.</p>
<p class='rw-defn idx2' style='display:none'>To abrogate an act is to end it by official and sometimes legal means.</p>
<p class='rw-defn idx3' style='display:none'>When you beget something, you cause it to happen or create it.</p>
<p class='rw-defn idx4' style='display:none'>To efface something is to erase or remove it completely from recognition or memory.</p>
<p class='rw-defn idx5' style='display:none'>If something engenders a particular feeling or attitude, it causes that condition.</p>
<p class='rw-defn idx6' style='display:none'>If you expunge something, you completely remove it or do away with it.</p>
<p class='rw-defn idx7' style='display:none'>To expurgate part of a book, play, or other text is to remove parts of it before publishing because they are considered objectionable, harmful, or offensive.</p>
<p class='rw-defn idx8' style='display:none'>If you extirpate something, you completely destroy it because it is unpleasant or unwanted.</p>
<p class='rw-defn idx9' style='display:none'>If someone, such as a writer, is described as having fecundity, they have the ability to produce many original and different ideas.</p>
<p class='rw-defn idx10' style='display:none'>Something inanimate is dead, motionless, or not living; therefore, it does not display the same traits as an active and alive organism.</p>
<p class='rw-defn idx11' style='display:none'>To initiate something is to begin or start it.</p>
<p class='rw-defn idx12' style='display:none'>An innovation is something new that is created or is a novel process for doing something.</p>
<p class='rw-defn idx13' style='display:none'>Something that is interminable continues for a very long time in a boring or annoying way.</p>
<p class='rw-defn idx14' style='display:none'>To liquidate a business or company is to close it down and sell the things that belong to it in order to pay off its debts.</p>
<p class='rw-defn idx15' style='display:none'>Something luxuriant, such as plants or hair, is growing well and is very healthy.</p>
<p class='rw-defn idx16' style='display:none'>When someone or something undergoes the process of metamorphosis, there is a change in appearance, character, or even physical shape.</p>
<p class='rw-defn idx17' style='display:none'>When you obliterate something, you destroy it to such an extent that there is nothing or very little of it left.</p>
<p class='rw-defn idx18' style='display:none'>Progeny are children or descendants.</p>
<p class='rw-defn idx19' style='display:none'>Something or someone that is prolific is highly fruitful and so produces a lot of something.</p>
<p class='rw-defn idx20' style='display:none'>When things propagate, such as plants, animals, or ideas, they reproduce or generate greater numbers, causing them to spread.</p>
<p class='rw-defn idx21' style='display:none'>A ramification from an action is a result or consequence of it—and is often unanticipated.</p>
<p class='rw-defn idx22' style='display:none'>When you ravage something, you completely destroy, wreck, or damage it.</p>
<p class='rw-defn idx23' style='display:none'>If you recant, you publicly announce that your once firmly held beliefs or statements were wrong and that you no longer agree with them.</p>
<p class='rw-defn idx24' style='display:none'>If you renege on a deal, agreement, or promise, you do not do what you promised or agreed to do.</p>
<p class='rw-defn idx25' style='display:none'>When someone in power rescinds a law or agreement, they officially end it and state that it is no longer valid.</p>
<p class='rw-defn idx26' style='display:none'>Something that stymies you presents an obstacle that prevents you from doing what you need or want to do.</p>
<p class='rw-defn idx27' style='display:none'>Something that is vernal occurs in spring; since spring is the time when new plants start to grow, the adjective vernal can also be used to suggest youth and freshness.</p>
<p class='rw-defn idx28' style='display:none'>If someone vitiates something, they make it less effective, weaker, or reduced in quality.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>gestation</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='gestation#' id='pronounce-sound' path='audio/words/amy-gestation'></a>
JES-tay-shin
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='gestation#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='gestation#' id='context-sound' path='audio/wordcontexts/brian-gestation'></a>
Leopold allowed time for the <em>gestation</em> or early formation of the main character of his new novel while he washed dishes.  He found that the repetitive motion of cleaning inspired his brain with growing, <em>gestating</em> images and concepts.  A long walk home also allowed time for <em>gestation</em> or early development of the unformed story plot.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is happening while a plan for a new building is in <em>gestation</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Construction of it is put on hold while developers attempt to solve a problem.
</li>
<li class='choice '>
<span class='result'></span>
It is in its first stages of being built, but the plan for it has been finalized.
</li>
<li class='choice answer '>
<span class='result'></span>
The person in charge of building it is thinking about how best to build it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='gestation#' id='definition-sound' path='audio/wordmeanings/amy-gestation'></a>
<em>Gestation</em> is the process by which a new idea,  plan, or piece of work develops in your mind.
</li>
<li class='def-text'>
<em>Gestation</em> also refers to the period of time in which offspring develops in the womb.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>early growth</em>
</span>
</span>
</div>
<a class='quick-help' href='gestation#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='gestation#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/gestation/memory_hooks/5977.json'></span>
<p>
<img alt="Gestation" src="https://cdn0.membean.com/public/images/wordimages/hook/gestation.jpg?qdep8" />
<span class="emp0"><span><span class="emp1"><span>Jest</span></span> <span class="emp2"><span>Station</span></span></span></span> A "<span class="emp1"><span>jest</span></span> <span class="emp2"><span>station</span></span>" is an office where comedians go to conceive and <span class="emp1"><span>gest</span></span><span class="emp2"><span>ate</span></span> their elaborately funny ideas.
</p>
</div>

<div id='memhook-button-bar'>
<a href="gestation#" id="add-public-hook" style="" url="https://membean.com/mywords/gestation/memory_hooks">Use other public hook</a>
<a href="gestation#" id="memhook-use-own" url="https://membean.com/mywords/gestation/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='gestation#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
From a fan's perspective, the ability to trace a song's <b>gestation</b>, or hear a track that had been considered for inclusion on an album but cut before the release, can bring about a "this-changes-everything" revelation, the sense of long-missing puzzle pieces snapping into place.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Apart from the F-22, the other fighter in <b>gestation</b> over the past decade has been the "Joint Strike Fighter," known as the F-35 . . . .
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/gestation/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='gestation#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='gest_bear' data-tree-url='//cdn1.membean.com/public/data/treexml' href='gestation#'>
<span class=''></span>
gest
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>bear, carry</td>
</tr>
<tr>
<td class='partform'>-ation</td>
<td>
&rarr;
</td>
<td class='meaning'>act of doing something</td>
</tr>
</table>
<p>Since the <em>gestation</em> period of a sheep is five months, the ewe &#8220;carries&#8221; the lamb for that long before it is born.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='gestation#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Sherlock</strong><span> The process of mental gestation is pretty hard, even for Sherlock.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/gestation.jpg' video_url='examplevids/gestation' video_width='350'></span>
<div id='wt-container'>
<img alt="Gestation" height="288" src="https://cdn1.membean.com/video/examplevids/gestation.jpg" width="350" />
<div class='center'>
<a href="gestation#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='gestation#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Gestation" src="https://cdn2.membean.com/public/images/wordimages/cons2/gestation.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='gestation#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>beget</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>engender</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>fecundity</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>initiate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>innovation</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>luxuriant</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>metamorphosis</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>progeny</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>prolific</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>propagate</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>vernal</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abolish</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abrogate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>efface</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>expunge</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>expurgate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>extirpate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>inanimate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>interminable</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>liquidate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>obliterate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>ramification</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>ravage</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>recant</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>renege</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>rescind</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>stymie</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>vitiate</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="gestation" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>gestation</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="gestation#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

