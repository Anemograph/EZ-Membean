
<!DOCTYPE html>
<html>
<head>
<title>Word: seminal | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you claim that something is banal, you do not like it because you think it is ordinary, dull, commonplace, and boring.</p>
<p class='rw-defn idx1' style='display:none'>When you beget something, you cause it to happen or create it.</p>
<p class='rw-defn idx2' style='display:none'>A cardinal rule or quality is considered to be the most important or basic in a set of rules or qualities.</p>
<p class='rw-defn idx3' style='display:none'>A copious amount of something is a very large amount of it.</p>
<p class='rw-defn idx4' style='display:none'>The crux of a problem or argument is the most important or difficult part of it that affects everything else.</p>
<p class='rw-defn idx5' style='display:none'>A derivative is something borrowed from something else, such as an English word that comes from another language.</p>
<p class='rw-defn idx6' style='display:none'>Detritus is the small pieces of waste that remain after something has been destroyed or used.</p>
<p class='rw-defn idx7' style='display:none'>When something is diffuse, it is widely spread out and scattered.</p>
<p class='rw-defn idx8' style='display:none'>Someone who is effusive expresses happiness, pleasure, admiration, praise, etc., in an extremely enthusiastic way.</p>
<p class='rw-defn idx9' style='display:none'>If something engenders a particular feeling or attitude, it causes that condition.</p>
<p class='rw-defn idx10' style='display:none'>Something ephemeral, such as some insects or a sunset, lasts for only a short time or has a very short lifespan.</p>
<p class='rw-defn idx11' style='display:none'>Something that is epochal is highly significant or important; this adjective often refers to the bringing about or marking of the beginning of a new era.</p>
<p class='rw-defn idx12' style='display:none'>If you think something, such as a statement or idea, is fatuous, you consider it stupid or extremely silly; a fatuous hope is unrealistic.</p>
<p class='rw-defn idx13' style='display:none'>If someone, such as a writer, is described as having fecundity, they have the ability to produce many original and different ideas.</p>
<p class='rw-defn idx14' style='display:none'>Gestation is the process by which a new idea,  plan, or piece of work develops in your mind.</p>
<p class='rw-defn idx15' style='display:none'>Hackneyed words, images, ideas or sayings have been used so often that they no longer seem interesting or amusing.</p>
<p class='rw-defn idx16' style='display:none'>If you describe someone&#8217;s behavior as inane, you think it is completely stupid or without much meaning.</p>
<p class='rw-defn idx17' style='display:none'>An incipient situation or quality is just beginning to appear or develop.</p>
<p class='rw-defn idx18' style='display:none'>If you describe ideas as jejune, you are saying they are childish and uninteresting; the adjective jejune also describes those having little experience, maturity, or knowledge.</p>
<p class='rw-defn idx19' style='display:none'>Something luxuriant, such as plants or hair, is growing well and is very healthy.</p>
<p class='rw-defn idx20' style='display:none'>Something that is pivotal is more important than anything else in a situation or a system; consequently, it is the key to its success.</p>
<p class='rw-defn idx21' style='display:none'>A platitude is an unoriginal statement that has been used so many times that it is considered almost meaningless and pointless, even though it is presented as important.</p>
<p class='rw-defn idx22' style='display:none'>The adjective primordial is used to describe things that existed close to the formation of Earth or close to the origin or development of something.</p>
<p class='rw-defn idx23' style='display:none'>When someone exhibits profundity, they display great intellectual depth and understanding; profundity can also be the depth or complexity of something.</p>
<p class='rw-defn idx24' style='display:none'>A profusion of something is a very large quantity or variety of it.</p>
<p class='rw-defn idx25' style='display:none'>Progeny are children or descendants.</p>
<p class='rw-defn idx26' style='display:none'>Something or someone that is prolific is highly fruitful and so produces a lot of something.</p>
<p class='rw-defn idx27' style='display:none'>Something prosaic is dull, boring, and ordinary.</p>
<p class='rw-defn idx28' style='display:none'>A ramification from an action is a result or consequence of it—and is often unanticipated.</p>
<p class='rw-defn idx29' style='display:none'>A sagacious person is wise, intelligent, and has the ability to make good practical decisions.</p>
<p class='rw-defn idx30' style='display:none'>Something that is specious seems to be good, sound, or correct but is actually wrong or fake.</p>
<p class='rw-defn idx31' style='display:none'>Something that is superfluous is unnecessary; it is more than what is wanted or needed at the current time.</p>
<p class='rw-defn idx32' style='display:none'>Something that has the quality of transience lasts for only a short time or is constantly changing.</p>
<p class='rw-defn idx33' style='display:none'>If you refer to something as a trifle, you mean that it is of little or no importance or value.</p>
<p class='rw-defn idx34' style='display:none'>A trite remark or idea is so overused that it is no longer interesting or novel.</p>
<p class='rw-defn idx35' style='display:none'>Something that is vacuous is empty or blank, such as a mind or stare.</p>
<p class='rw-defn idx36' style='display:none'>Something vapid is dull, boring, and/or tiresome.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
Middle/High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>seminal</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='seminal#' id='pronounce-sound' path='audio/words/amy-seminal'></a>
SEM-uh-nl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='seminal#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='seminal#' id='context-sound' path='audio/wordcontexts/brian-seminal'></a>
The circle of artists did not know it at the time, but their work would have an influential or <em>seminal</em> effect on future generations.  They inspired each other as they met in the studio to discuss their new, important, and <em>seminal</em> concepts that would one day rock the world.  Such <em>seminal</em>, fundamental, and original contributions of art and theory can create new ways of understanding the world.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean to make a <em>seminal</em> contribution to a field of study?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It means to make a large donation to allow advances to be made in that field.
</li>
<li class='choice answer '>
<span class='result'></span>
It means to create something significant that influences future people in that field.
</li>
<li class='choice '>
<span class='result'></span>
It means to be part of a large team of researchers within that field of study.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='seminal#' id='definition-sound' path='audio/wordmeanings/amy-seminal'></a>
A <em>seminal</em> article, book, piece of music or other work has many new ideas; additionally, it has great influence on generating ideas for future work.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>influential</em>
</span>
</span>
</div>
<a class='quick-help' href='seminal#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='seminal#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/seminal/memory_hooks/5644.json'></span>
<p>
<span class="emp0"><span>Orig<span class="emp1"><span>inal</span></span> <span class="emp2"><span>Sem</span></span>ester</span></span> During that one orig<span class="emp1"><span>inal</span></span> <span class="emp2"><span>sem</span></span>ester Ganesha created such <span class="emp2"><span>sem</span></span><span class="emp1"><span>inal</span></span> works of art that they not only influenced her own future work, but also the works of her teacher!
</p>
</div>

<div id='memhook-button-bar'>
<a href="seminal#" id="add-public-hook" style="" url="https://membean.com/mywords/seminal/memory_hooks">Use other public hook</a>
<a href="seminal#" id="memhook-use-own" url="https://membean.com/mywords/seminal/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='seminal#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Many current Americans may not know this, but that <b>seminal</b> act—which made racial discrimination in public places and the workplace illegal—would not have been possible without the overwhelming support of northern Republicans such as Ford.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
Others, who insist that longevity must be considered, still favor Elgin Baylor, citing his excellence over 14 seasons, and a smaller group gives the nod to 15-year veteran Julius Erving for his <b>seminal</b> stylistic contributions [to the position].
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/seminal/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='seminal#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='semin_seed' data-tree-url='//cdn1.membean.com/public/data/treexml' href='seminal#'>
<span class=''></span>
semin
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>seed</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='al_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='seminal#'>
<span class=''></span>
-al
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>A <em>seminal</em> event or literary work is &#8220;like a seed&#8221; because it generates so many other plants or offshoots from it.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='seminal#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Vat 10</strong><span> The Mona Lisa is a hugely seminal work of art.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/seminal.jpg' video_url='examplevids/seminal' video_width='350'></span>
<div id='wt-container'>
<img alt="Seminal" height="288" src="https://cdn1.membean.com/video/examplevids/seminal.jpg" width="350" />
<div class='center'>
<a href="seminal#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='seminal#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Seminal" src="https://cdn1.membean.com/public/images/wordimages/cons2/seminal.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='seminal#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>beget</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>cardinal</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>copious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>crux</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>diffuse</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>effusive</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>engender</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>epochal</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>fecundity</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>gestation</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>incipient</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>luxuriant</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>pivotal</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>primordial</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>profundity</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>profusion</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>progeny</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>prolific</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>sagacious</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>banal</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>derivative</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>detritus</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>ephemeral</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>fatuous</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>hackneyed</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>inane</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>jejune</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>platitude</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>prosaic</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>ramification</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>specious</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>superfluous</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>transience</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>trifle</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>trite</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>vacuous</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>vapid</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="seminal" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>seminal</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="seminal#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

