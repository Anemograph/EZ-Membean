
<!DOCTYPE html>
<html>
<head>
<title>Word: alleviate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>You allay someone&#8217;s fear or concern by making them feel less afraid or worried.</p>
<p class='rw-defn idx2' style='display:none'>When you ameliorate a bad condition or situation, you make it better in some way.</p>
<p class='rw-defn idx3' style='display:none'>Your antagonist is an opponent in a competition or battle.</p>
<p class='rw-defn idx4' style='display:none'>When you assuage an unpleasant feeling, you make it less painful or severe, thereby calming it.</p>
<p class='rw-defn idx5' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx6' style='display:none'>To augment something is to increase its value or effectiveness by adding something to it.</p>
<p class='rw-defn idx7' style='display:none'>A convalescent person spends time resting to gain health and strength after a serious illness or operation.</p>
<p class='rw-defn idx8' style='display:none'>To corroborate something that has been said or reported means to give information or evidence that further supports it.</p>
<p class='rw-defn idx9' style='display:none'>If you curtail something, you reduce or limit it.</p>
<p class='rw-defn idx10' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx11' style='display:none'>If something exacerbates a problem or bad situation, it makes it even worse.</p>
<p class='rw-defn idx12' style='display:none'>When you exasperate another person, you annoy or anger them a great deal because you keep on doing something that is highly irritating.</p>
<p class='rw-defn idx13' style='display:none'>If circumstances extenuate someone’s actions in a questionable situation, you feel that it was reasonable for someone to break the usual rules; therefore, you partly excuse and sympathize with their wrongdoing.</p>
<p class='rw-defn idx14' style='display:none'>To foment is to encourage people to protest, fight, or cause trouble and violent opposition to something that is viewed by some as undesirable.</p>
<p class='rw-defn idx15' style='display:none'>An incendiary device causes objects to catch on fire; incendiary comments can cause riots to flare up.</p>
<p class='rw-defn idx16' style='display:none'>Something that is incessant continues on for a long time without stopping.</p>
<p class='rw-defn idx17' style='display:none'>Someone who is inconsolable has been so devastated by a terrible event that no one can help them feel better about it.</p>
<p class='rw-defn idx18' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx19' style='display:none'>If you mollify someone, you say or do something to make that person feel less angry or upset.</p>
<p class='rw-defn idx20' style='display:none'>A palliative action makes a bad situation seem better; nevertheless, it does not actually solve the problem.</p>
<p class='rw-defn idx21' style='display:none'>A panacea is something that people think will solve all problems and make everything better.</p>
<p class='rw-defn idx22' style='display:none'>To quell something is to stamp out, quiet, or overcome it.</p>
<p class='rw-defn idx23' style='display:none'>When something, such as a disease or storm, is in remission, its force is lessening or decreasing.</p>
<p class='rw-defn idx24' style='display:none'>A reprieve is a temporary relief from or cancellation of a punishment; it can also be a relief from something, such as pain or trouble of some kind.</p>
<p class='rw-defn idx25' style='display:none'>When something whets your appetite, it increases your desire for it, especially by giving you a little idea or pleasing hint of what it is like.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>alleviate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='alleviate#' id='pronounce-sound' path='audio/words/amy-alleviate'></a>
uh-LEE-vee-ayt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='alleviate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='alleviate#' id='context-sound' path='audio/wordcontexts/brian-alleviate'></a>
You can <em>alleviate</em> or lessen a co-worker&#8217;s stress by taking on some of her extra work.  To <em>alleviate</em> a headache, you need only take an aspirin, which will help get rid of the pain.  You can also <em>alleviate</em> or lessen a confusing situation by providing information that brings clarity to the process or project.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How could you <em>alleviate</em> hunger?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
You could eat something so you aren&#8217;t as hungry.
</li>
<li class='choice '>
<span class='result'></span>
You could avoid food and eventually get hungrier.
</li>
<li class='choice '>
<span class='result'></span>
You could study it to understand why hunger occurs.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='alleviate#' id='definition-sound' path='audio/wordmeanings/amy-alleviate'></a>
If you <em>alleviate</em> pain or suffering, you make it less intense or severe.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>relieve</em>
</span>
</span>
</div>
<a class='quick-help' href='alleviate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='alleviate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/alleviate/memory_hooks/4994.json'></span>
<p>
<img alt="Alleviate" src="https://cdn0.membean.com/public/images/wordimages/hook/alleviate.jpg?qdep8" />
<span class="emp0"><span><span class="emp3"><span>Alev</span></span>e He <span class="emp1"><span>Ate</span></span></span></span> He took the popular pain reliever <span class="emp3"><span>Alev</span></span>e to re<span class="emp3"><span>lieve</span></span> his pain, which helped to <span class="emp3"><span>allev</span></span>i<span class="emp1"><span>ate</span></span> his toothache, thus <span class="emp3"><span>leav</span></span>ing it behind.
</p>
</div>

<div id='memhook-button-bar'>
<a href="alleviate#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/alleviate/memory_hooks">Use other hook</a>
<a href="alleviate#" id="memhook-use-own" url="https://membean.com/mywords/alleviate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='alleviate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
[MacKenzie] Scott’s most recent public remarks were around her $4.1 billion in donations last year. “Though I’m far from completing my pledge, this year of giving began with exposure to leaders from historically marginalized groups fighting inequities, and ended with exposure to thousands of organizations working to <b>alleviate</b> suffering for those hardest hit by the pandemic,” she wrote in December.
<cite class='attribution'>
&mdash;
Forbes
</cite>
</li>
<li>
Critics of voter ID cards say the requirement could raise privacy issues and intimidate or discourage some Americans, particularly the elderly, the poor and minorities, from participating in elections. To <b>alleviate</b> those concerns, the Carter-Baker commission urges states to make it easy for non-drivers to obtain such cards and seeks measures to ensure privacy and security for all voters.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Disney wants to keep the district focused on tourist-related uses, while a majority of the Anaheim City Council supports construction of a 1,500-unit residential project that could help <b>alleviate</b> the city’s housing shortage.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
Consumers can’t exactly patrol commercial spinach fields, or the kitchens of their favorite restaurants, but they can make their at-home food preparation safer, both to prevent illness due to bacteria and to help <b>alleviate</b> concerns about pesticide residue.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/alleviate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='alleviate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='al_to' data-tree-url='//cdn1.membean.com/public/data/treexml' href='alleviate#'>
<span class=''></span>
al-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to, towards, near</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='lev_light' data-tree-url='//cdn1.membean.com/public/data/treexml' href='alleviate#'>
<span class='common'></span>
lev
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>light, of little weight</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ate_quality' data-tree-url='//cdn1.membean.com/public/data/treexml' href='alleviate#'>
<span class=''></span>
-ate
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make something have a certain quality</td>
</tr>
</table>
<p>To <em>alleviate</em> a bad situation is to make it have the quality of being &#8220;towards a lighter or less heavy side&#8221; of it, or being &#8220;near littler weight&#8221; instead of bearing its full effect. The idea of moving &#8220;towards littler weight&#8221; on one&#8217;s shoulders is the key idea.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='alleviate#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Star Trek Deep Space Nine</strong><span> The doctor has been successful in alleviating the pain.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/alleviate.jpg' video_url='examplevids/alleviate' video_width='350'></span>
<div id='wt-container'>
<img alt="Alleviate" height="288" src="https://cdn1.membean.com/video/examplevids/alleviate.jpg" width="350" />
<div class='center'>
<a href="alleviate#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='alleviate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Alleviate" src="https://cdn0.membean.com/public/images/wordimages/cons2/alleviate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='alleviate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>allay</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>ameliorate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>assuage</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>convalescent</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>curtail</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>extenuate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>mollify</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>palliative</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>panacea</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>quell</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>remission</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>reprieve</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>antagonist</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>augment</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>corroborate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>exacerbate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>exasperate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>foment</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>incendiary</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>incessant</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>inconsolable</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>whet</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="alleviate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>alleviate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="alleviate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

