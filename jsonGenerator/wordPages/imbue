
<!DOCTYPE html>
<html>
<head>
<title>Word: imbue | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abridge a book or play, you make it shorter by making cuts to the original.</p>
<p class='rw-defn idx1' style='display:none'>When you assimilate an idea, you completely understand it; likewise, when someone is assimilated into a new place, they become part of it by understanding what it is all about and by being accepted by others who live there.</p>
<p class='rw-defn idx2' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx3' style='display:none'>If you curtail something, you reduce or limit it.</p>
<p class='rw-defn idx4' style='display:none'>Someone defoliates a tree or plant by removing its leaves, usually by applying a chemical agent.</p>
<p class='rw-defn idx5' style='display:none'>To denude an area is to remove the plants and trees that cover it; it can also mean to make something bare.</p>
<p class='rw-defn idx6' style='display:none'>When something is diffuse, it is widely spread out and scattered.</p>
<p class='rw-defn idx7' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx8' style='display:none'>If you divest someone of power, rights, or authority, you take those things away from them.</p>
<p class='rw-defn idx9' style='display:none'>When you extricate yourself from a difficult or unpleasant situation, you manage to escape it.</p>
<p class='rw-defn idx10' style='display:none'>If something is gilded, it has been deceptively given a more attractive appearance than it normally possesses; a very thin layer of gold often covers something gilded.</p>
<p class='rw-defn idx11' style='display:none'>An immanent quality is present within something and is so much a part of it that the object cannot be imagined without that quality.</p>
<p class='rw-defn idx12' style='display:none'>To inculcate is to fix an idea or belief in someone&#8217;s mind by repeatedly teaching it.</p>
<p class='rw-defn idx13' style='display:none'>If you indoctrinate someone, you teach them a set of beliefs so thoroughly that they reject any other beliefs or ideas without any critical thought.</p>
<p class='rw-defn idx14' style='display:none'>An infusion is the pouring in or the introduction of something into something else so as to fill it up.</p>
<p class='rw-defn idx15' style='display:none'>Something that has been ingrained in your mind has been fixed or rooted there permanently.</p>
<p class='rw-defn idx16' style='display:none'>An inherent characteristic is one that exists in a person at birth or in a thing naturally.</p>
<p class='rw-defn idx17' style='display:none'>An innate quality of someone is present since birth or is a quality of something that is essential to it.</p>
<p class='rw-defn idx18' style='display:none'>When a substance permeates something, it enters into all parts of it.</p>
<p class='rw-defn idx19' style='display:none'>If something is pervasive, it appears to be everywhere.</p>
<p class='rw-defn idx20' style='display:none'>To rarefy something is to make it less dense, as in oxygen at high altitudes; this word also refers to purifying or refining something, thereby making it less ordinary or commonplace.</p>
<p class='rw-defn idx21' style='display:none'>If something, such as warmth, color, or liquid, suffuses a material, it gradually spreads through or covers it; if you are suffused with a feeling, you are full of that feeling.</p>
<p class='rw-defn idx22' style='display:none'>When something is transfused to another thing, it is given, put, or imparted to it; for example, you can transfuse blood or a love of reading from one person to another.</p>
<p class='rw-defn idx23' style='display:none'>To wallow in something is to immerse, roll, or absorb oneself lazily in it; wallow can also refer to being very involved in a negative feeling, such as wallowing in misery or self-pity.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>imbue</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='imbue#' id='pronounce-sound' path='audio/words/amy-imbue'></a>
im-BYOO
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='imbue#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='imbue#' id='context-sound' path='audio/wordcontexts/brian-imbue'></a>
As parents, we try to fill or <em>imbue</em> our children with a sense of responsibility.  It is difficult to do so when the culture around us is deeply bathed or <em>imbued</em> with images of selfish behavior.  Television and movies, in particular, are stained or <em>imbued</em> with too many scenes of violence.  I am pleased when I see film images of American youth touched by or <em>imbued</em> with patience, kindness and good sense.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How might a teacher <em>imbue</em> their students with curiosity?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
By discouraging them from ever feeling curious.
</li>
<li class='choice '>
<span class='result'></span>
By defining curiosity and giving several examples of what it is.
</li>
<li class='choice answer '>
<span class='result'></span>
By encouraging them to feel curious about things.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='imbue#' id='definition-sound' path='audio/wordmeanings/amy-imbue'></a>
To <em>imbue</em> someone with a quality, such as an emotion or idea, is to fill that person completely with it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>fully fill</em>
</span>
</span>
</div>
<a class='quick-help' href='imbue#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='imbue#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/imbue/memory_hooks/23825.json'></span>
<p>
<span class="emp0"><span>Put <span class="emp2"><span>in</span></span> <span class="emp3"><span>B</span></span>l<span class="emp3"><span>ue</span></span></span></span> When someone <span class="emp2"><span>im</span></span><span class="emp3"><span>bue</span></span>s you with sadness, she puts <span class="emp2"><span>in</span></span> <span class="emp3"><span>b</span></span>l<span class="emp3"><span>ue</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="imbue#" id="add-public-hook" style="" url="https://membean.com/mywords/imbue/memory_hooks">Use other public hook</a>
<a href="imbue#" id="memhook-use-own" url="https://membean.com/mywords/imbue/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='imbue#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
[Octavia Butler] decided to become a science fiction writer at age 9 after seeing the 1954 [low-budget] movie _Devil Girl From Mars_” . . . "It encouraged her to try her own hand at writing science fiction and <b>imbue</b> it with more ambitious ideas and themes, connected to Black history and the future of our planet," Streeby says.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
Madeline Gannon, a roboticist and artist, unveiled Manus [robots] at the World Economic Forum’s 2018 Annual Meeting of New Champions in Tianjin, China. The project is exploring ways to <b>imbue</b> machines with movements and behaviors that feel more natural to us, and which could help put humans at ease around our mechanical compatriots.
<cite class='attribution'>
&mdash;
Discover Magazine
</cite>
</li>
<li>
All parents can do is hope that the love of learning they <b>imbue</b> in their child takes hold, causing him to seek out the experiences and people that will keep stimulating his intelligence.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
On a summer evening in 1978, Donald Harris took his two young daughters to the Greek Theatre in Berkeley, Calif., to their first concert. . . . The experience was meant to be more than musical. Her father, a prominent Jamaican economics professor teaching at Stanford, was trying to <b>imbue</b> his two American-born girls with a sense of pride in their roots.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/imbue/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='imbue#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>imbu</td>
<td>
&rarr;
</td>
<td class='meaning'>steep, soak, saturate</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='imbue#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>When one <em>imbues</em> something with a certain quality, one &#8220;steeps, soaks, or saturates&#8221; it with that quality.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='imbue#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Mr. Magorium's Magic Emporium</strong><span> The store has been imbued with the characteristics of children.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/imbue.jpg' video_url='examplevids/imbue' video_width='350'></span>
<div id='wt-container'>
<img alt="Imbue" height="288" src="https://cdn1.membean.com/video/examplevids/imbue.jpg" width="350" />
<div class='center'>
<a href="imbue#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='imbue#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Imbue" src="https://cdn1.membean.com/public/images/wordimages/cons2/imbue.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='imbue#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>assimilate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>diffuse</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>gilded</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>immanent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>inculcate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>indoctrinate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>infusion</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>ingrained</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>inherent</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>innate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>permeate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>pervasive</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>suffuse</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>transfuse</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>wallow</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abridge</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>curtail</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>defoliate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>denude</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>divest</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>extricate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>rarefy</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="imbue" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>imbue</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="imbue#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

