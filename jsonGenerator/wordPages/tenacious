
<!DOCTYPE html>
<html>
<head>
<title>Word: tenacious | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An abortive attempt or action is cut short before it is finished; hence, it is unsuccessful.</p>
<p class='rw-defn idx1' style='display:none'>An adherent is a supporter or follower of a leader or a cause.</p>
<p class='rw-defn idx2' style='display:none'>If you are ambivalent about something, you are uncertain whether you like it or what you should do about it.</p>
<p class='rw-defn idx3' style='display:none'>An assiduous person works hard to ensure that something is done properly and completely.</p>
<p class='rw-defn idx4' style='display:none'>Someone who is capricious changes their mind repeatedly or behaves in unexpected ways; a capricious series of events follows no predictable pattern.</p>
<p class='rw-defn idx5' style='display:none'>Something that is desultory is done in a way that is unplanned, disorganized, and without direction.</p>
<p class='rw-defn idx6' style='display:none'>When you create a diversion, you cause someone to turn aside momentarily from what they are doing by distracting them.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is dour is serious, stubborn, and unfriendly; they can also be gloomy.</p>
<p class='rw-defn idx8' style='display:none'>If you describe a person, group, or civilization as effete, you mean it is weak, exhausted,  powerless, unproductive, and/or corrupt.</p>
<p class='rw-defn idx9' style='display:none'>Efficacy is the ability or power to produce an expected effect or result.</p>
<p class='rw-defn idx10' style='display:none'>When people equivocate, they avoid making a clear statement; they are deliberately vague in order to avoid giving a direct answer to a question.</p>
<p class='rw-defn idx11' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is impetuous does things quickly and rashly without thinking carefully first.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is impulsive tends to do things without thinking about them ahead of time; therefore, their actions can be unpredictable.</p>
<p class='rw-defn idx14' style='display:none'>If someone is indefatigable, they never shows signs of getting tired.</p>
<p class='rw-defn idx15' style='display:none'>Something that happens on an intermittent basis happens in irregular intervals, stopping and starting at unpredictable times.</p>
<p class='rw-defn idx16' style='display:none'>An intransigent person is stubborn; thus, they refuse to change their ideas or behavior, often in a way that seems unreasonable.</p>
<p class='rw-defn idx17' style='display:none'>Someone who is irresolute is unable to decide what to do.</p>
<p class='rw-defn idx18' style='display:none'>A mercurial person&#8217;s mind or mood changes often, quickly, and unpredictably.</p>
<p class='rw-defn idx19' style='display:none'>Someone is considered meticulous when they act with careful attention to detail.</p>
<p class='rw-defn idx20' style='display:none'>Someone who is obdurate is stubborn, unreasonable, or uncontrollable; hence, they simply refuse to change their behavior, actions, or beliefs to fit any situation whatsoever.</p>
<p class='rw-defn idx21' style='display:none'>An obstinate person refuses to change their mind, even when other people think they are being highly unreasonable.</p>
<p class='rw-defn idx22' style='display:none'>If an object oscillates, it moves repeatedly from one point to another and then back again; if you oscillate between two moods or attitudes, you keep changing from one to the other.</p>
<p class='rw-defn idx23' style='display:none'>Someone who is pertinacious is determined to continue doing something rather than giving up—even when it gets very difficult.</p>
<p class='rw-defn idx24' style='display:none'>When you procrastinate, you put off or delay doing something—usually because it is something unpleasant that you&#8217;d rather not do.</p>
<p class='rw-defn idx25' style='display:none'>A propensity is a natural tendency towards a particular behavior.</p>
<p class='rw-defn idx26' style='display:none'>If someone watches or listens to something with rapt attention, they are so involved with it that they do not notice anything else.</p>
<p class='rw-defn idx27' style='display:none'>Something or someone that shows resilience is able to recover quickly and easily from unpleasant, difficult, and damaging situations or events.</p>
<p class='rw-defn idx28' style='display:none'>A restive person is not willing or able to keep still or be patient because they are bored or dissatisfied with something; consequently, they are becoming difficult to control.</p>
<p class='rw-defn idx29' style='display:none'>A scrupulous person takes great care to do everything in an extremely honest, conscientious, and fair manner.</p>
<p class='rw-defn idx30' style='display:none'>Someone who is sedulous works hard and performs tasks very carefully and thoroughly, not stopping their work until it is completely accomplished.</p>
<p class='rw-defn idx31' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx32' style='display:none'>To be tentative is to be hesitant or uncertain about something; an agreement or decision of this kind is likely to have changes before it reaches its final form.</p>
<p class='rw-defn idx33' style='display:none'>If you are unflagging while doing a task, you are untiring when working upon it and do not stop until it is finished.</p>
<p class='rw-defn idx34' style='display:none'>If you are unrelenting in your desire to do something, you stop at nothing until you&#8217;ve done it.</p>
<p class='rw-defn idx35' style='display:none'>When someone vacillates, they go back and forth about a choice or opinion, unable to make a firm decision.</p>
<p class='rw-defn idx36' style='display:none'>Something that is volatile can change easily and vary widely.</p>
<p class='rw-defn idx37' style='display:none'>If you waver, you cannot decide between two things because you have serious doubts about which choice is better.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>tenacious</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='tenacious#' id='pronounce-sound' path='audio/words/amy-tenacious'></a>
tuh-NAY-shuhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='tenacious#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='tenacious#' id='context-sound' path='audio/wordcontexts/brian-tenacious'></a>
The dog was <em>tenacious</em> in his persistent efforts to get all the meat off the bone.  He <em>tenaciously</em> gnawed at it for hours, determined to get every scrap of meat he could.  When his owner tried to pry the bone out of his mouth, he held on with fierce <em>tenacity</em> and dogged stubbornness.  He only gave up his <em>tenacious</em>, unyielding hold on the bone when his owner offered him a ham sandwich in place of it.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If you are <em>tenacious</em>, what are you?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
You are quick to anger and often get into fights.
</li>
<li class='choice '>
<span class='result'></span>
You are easily convinced to change your mind.
</li>
<li class='choice answer '>
<span class='result'></span>
You are determined to reach the goals you&#8217;ve set.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='tenacious#' id='definition-sound' path='audio/wordmeanings/amy-tenacious'></a>
A <em>tenacious</em> person does not quit until they finish what they&#8217;ve started.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>putting in great effort</em>
</span>
</span>
</div>
<a class='quick-help' href='tenacious#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='tenacious#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/tenacious/memory_hooks/4913.json'></span>
<p>
<img alt="Tenacious" src="https://cdn2.membean.com/public/images/wordimages/hook/tenacious.jpg?qdep8" />
<span class="emp0"><span><span class="emp1"><span>Tenacious</span></span> D</span></span> The band "<span class="emp1"><span>Tenacious</span></span> D" was named as being <span class="emp1"><span>tenacious</span></span> for its continued insistence on being loud and vulgar in each and every show.
</p>
</div>

<div id='memhook-button-bar'>
<a href="tenacious#" id="add-public-hook" style="" url="https://membean.com/mywords/tenacious/memory_hooks">Use other public hook</a>
<a href="tenacious#" id="memhook-use-own" url="https://membean.com/mywords/tenacious/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='tenacious#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Fear had absorbed her completely and remained there, fixed, <b>tenacious</b>, almost corporeal, as if it were some invisible person who had made up his mind not to leave the room.
<span class='attribution'>&mdash; Gabriel García Márquez, Columbian writer, from _Collected Stories_</span>
<img alt="Gabriel garcía márquez, columbian writer, from _collected stories_" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Gabriel García Márquez, Columbian writer, from _Collected Stories_.jpg?qdep8" width="80" />
</li>
<li>
A surviving population [of Tasmanian devils] has thrived in Tasmania, a large island off the southeastern tip of Australia, but the <b>tenacious</b> scavengers have struggled in recent decades.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
This "Southern Operation" would seal off China from outside help, thus underwriting victory in Japan’s frustrating four-year war against Chiang Kai-shek’s feckless but <b>tenacious</b> Chinese army.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
The right to vote in political elections. A right long denied to women of the United States, Florida and Flagler County. A denial which ended on Aug. 26, 1920, as the 19th Amendment to the United States Constitution was ratified, thereby ending a 72-year-long battle waged by <b>tenacious</b> and trailblazing women who won the right to vote!
<cite class='attribution'>
&mdash;
The Daytona Beach News-Journal
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/tenacious/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='tenacious#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ten_hold' data-tree-url='//cdn1.membean.com/public/data/treexml' href='tenacious#'>
<span class='common'></span>
ten
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>have, hold</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='acious_inclined' data-tree-url='//cdn1.membean.com/public/data/treexml' href='tenacious#'>
<span class=''></span>
-acious
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>inclined to, abounding in</td>
</tr>
</table>
<p>When one is <em>tenacious</em>, one is &#8220;inclined to hold&#8221; onto something until it is completed.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='tenacious#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Tug of War</strong><span> This is one tenacious dog!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/tenacious.jpg' video_url='examplevids/tenacious' video_width='350'></span>
<div id='wt-container'>
<img alt="Tenacious" height="198" src="https://cdn1.membean.com/video/examplevids/tenacious.jpg" width="350" />
<div class='center'>
<a href="tenacious#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='tenacious#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Tenacious" src="https://cdn2.membean.com/public/images/wordimages/cons2/tenacious.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='tenacious#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>adherent</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>assiduous</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>dour</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>efficacy</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>indefatigable</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>intransigent</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>meticulous</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>obdurate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>obstinate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>pertinacious</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>propensity</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>rapt</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>resilience</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>scrupulous</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>sedulous</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>unflagging</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>unrelenting</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abortive</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>ambivalent</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>capricious</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>desultory</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>diversion</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>effete</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>equivocate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>impetuous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>impulsive</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>intermittent</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>irresolute</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>mercurial</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>oscillate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>procrastinate</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>restive</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>tentative</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>vacillate</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>volatile</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>waver</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='tenacious#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
tenacity
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>persistence; determination</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="tenacious" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>tenacious</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="tenacious#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

