
<!DOCTYPE html>
<html>
<head>
<title>Word: parry | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Parry-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/parry-large.jpg?qdep8" />
</div>
<a href="parry#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>You accost a stranger when you move towards them and speak in an unpleasant or threatening way.</p>
<p class='rw-defn idx1' style='display:none'>When something bedevils you, it causes you a lot of problems and difficulties over a period of time.</p>
<p class='rw-defn idx2' style='display:none'>When you beleaguer someone, you act with the intent to annoy or harass that person repeatedly until they finally give you what you want.</p>
<p class='rw-defn idx3' style='display:none'>If you are bellicose, you behave in an aggressive way and are likely to start an argument or fight.</p>
<p class='rw-defn idx4' style='display:none'>A belligerent person or country is aggressive, very unfriendly, and likely to start a fight.</p>
<p class='rw-defn idx5' style='display:none'>If you are beset by something, you are experiencing serious problems or dangers because of it.</p>
<p class='rw-defn idx6' style='display:none'>When a person is besieged, they are excessively bothered or overwhelmed by questions or other matters requiring their attention; likewise, when a city is besieged, it is surrounded by enemy forces.</p>
<p class='rw-defn idx7' style='display:none'>If you circumvent something, such as a rule or restriction, you try to get around it in a clever and perhaps dishonest way.</p>
<p class='rw-defn idx8' style='display:none'>If something confounds you, it makes you feel surprised and confused, often because it does not meet your predefined expectations.</p>
<p class='rw-defn idx9' style='display:none'>To confute an argument is to prove it to be thoroughly false; to confute a person is to prove them to be wrong.</p>
<p class='rw-defn idx10' style='display:none'>To contravene a law, rule, or agreement is to do something that is not allowed or is forbidden by that law, rule, or agreement.</p>
<p class='rw-defn idx11' style='display:none'>A deterrent keeps someone from doing something against you.</p>
<p class='rw-defn idx12' style='display:none'>If a fact or idea eludes you, you cannot remember or understand it; if you elude someone, you manage to escape or hide from them.</p>
<p class='rw-defn idx13' style='display:none'>When people equivocate, they avoid making a clear statement; they are deliberately vague in order to avoid giving a direct answer to a question.</p>
<p class='rw-defn idx14' style='display:none'>A fusillade is a rapid burst of gunfire from multiple guns fired at once or in succession; in turn, a fusillade can also be a rapid outburst or discharge of something.</p>
<p class='rw-defn idx15' style='display:none'>An impediment is something that blocks or obstructs progress; it can also be a weakness or disorder, such as having difficulty with speaking.</p>
<p class='rw-defn idx16' style='display:none'>The adjective inexorable describes a process that is impossible to stop once it has begun.</p>
<p class='rw-defn idx17' style='display:none'>Something that inhibits you from doing something restricts or keeps you from doing it.</p>
<p class='rw-defn idx18' style='display:none'>An irrefutable argument or statement cannot be proven wrong; therefore, it must be accepted because it is certain.</p>
<p class='rw-defn idx19' style='display:none'>If you prevaricate, you avoid giving a direct or honest answer, usually because you want to hide the truth or want to delay or avoid making a hard decision.</p>
<p class='rw-defn idx20' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx21' style='display:none'>A riposte is a quick and clever reply that is often made in answer to criticism of some kind.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>parry</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='parry#' id='pronounce-sound' path='audio/words/amy-parry'></a>
PAR-ee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='parry#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='parry#' id='context-sound' path='audio/wordcontexts/brian-parry'></a>
Rhonda the reporter noticed the interviewee continually tried to <em>parry</em> or avoid the questions she posed.  Rhonda decided to be very direct, and stated that by <em>parrying</em> or blocking her questions, the interviewee was trying to conceal something.  The interviewee replied, &#8220;No, I have nothing to conceal, but I choose to <em>parry</em> and shun such obviously inappropriate questions about my family because it&#8217;s none of your business!&#8221;
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If you <em>parry</em> a question from your mom, what are you doing?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
You are avoiding it by changing the subject.
</li>
<li class='choice '>
<span class='result'></span>
You are answering it as honestly as you can.
</li>
<li class='choice '>
<span class='result'></span>
You are anticipating she will ask it and trying to think of a good answer.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='parry#' id='definition-sound' path='audio/wordmeanings/amy-parry'></a>
To <em>parry</em> is to ward something off or deflect it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>ward off</em>
</span>
</span>
</div>
<a class='quick-help' href='parry#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='parry#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/parry/memory_hooks/5831.json'></span>
<p>
<span class="emp0"><span>H<span class="emp3"><span>arry</span></span>'s P<span class="emp3"><span>arry</span></span></span></span> When Lord Voldemort cast a curse at H<span class="emp3"><span>arry</span></span> Potter, H<span class="emp3"><span>arry</span></span> did p<span class="emp3"><span>arry</span></span> that curse with a shield spell which saved him from certain death.
</p>
</div>

<div id='memhook-button-bar'>
<a href="parry#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/parry/memory_hooks">Use other hook</a>
<a href="parry#" id="memhook-use-own" url="https://membean.com/mywords/parry/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='parry#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Last week in Manhattan a small clique of enthusiasts watched three of these men <b>parry</b> & thrust their way once more to national amateur [fencing] championships.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
<li>
Obama chose to <b>parry</b> this with a solid joke, but as Brian Beutler says, the real answer to the question is interesting and important.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
Asara and Schweitzer were forced to <b>parry</b> and retreat, admitting that statistical evidence for one of the protein fragments was too weak for them to claim they'd even found it.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/parry/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='parry#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='par_make' data-tree-url='//cdn1.membean.com/public/data/treexml' href='parry#'>
<span class=''></span>
par
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make, prepare</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='y_activity' data-tree-url='//cdn1.membean.com/public/data/treexml' href='parry#'>
<span class=''></span>
-y
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>activity</td>
</tr>
</table>
<p>If one can successfully <em>parry</em> a blow, one has fully &#8220;prepared&#8221; for it.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='parry#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Howcast</strong><span> One method of parrying with a sword.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/parry.jpg' video_url='examplevids/parry' video_width='350'></span>
<div id='wt-container'>
<img alt="Parry" height="288" src="https://cdn1.membean.com/video/examplevids/parry.jpg" width="350" />
<div class='center'>
<a href="parry#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='parry#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Parry" src="https://cdn0.membean.com/public/images/wordimages/cons2/parry.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='parry#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='7' class = 'rw-wordform notlearned'><span>circumvent</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>confound</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>confute</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>contravene</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>deterrent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>elude</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>equivocate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>impediment</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>inhibit</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>prevaricate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>riposte</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>accost</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>bedevil</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>beleaguer</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bellicose</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>belligerent</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>beset</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>besiege</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>fusillade</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>inexorable</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>irrefutable</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='parry#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
parry
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>a warding off of a blow</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="parry" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>parry</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="parry#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

