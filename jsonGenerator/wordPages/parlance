
<!DOCTYPE html>
<html>
<head>
<title>Word: parlance | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An argot is a special language or set of expressions used by a particular group of people that those outside the group find hard to understand.</p>
<p class='rw-defn idx1' style='display:none'>If you describe someone as articulate, you mean that they are able to express their thoughts, arguments, and ideas clearly and effectively.</p>
<p class='rw-defn idx2' style='display:none'>If you commune with something, you communicate without using words because you feel especially close or in tune with it.</p>
<p class='rw-defn idx3' style='display:none'>The word homogeneous is used to describe a group that has members or parts which are similar or are all of the same type.</p>
<p class='rw-defn idx4' style='display:none'>An interlocutor is the person with whom you are having a (usually formal) conversation or discussion.</p>
<p class='rw-defn idx5' style='display:none'>Jargon is language or terminology used by a specific group that might not be understandable to those people who are not of the group.</p>
<p class='rw-defn idx6' style='display:none'>The lexicon of a particular subject or language is all the words, phrases, and terms associated with it.</p>
<p class='rw-defn idx7' style='display:none'>Nomenclature is a specialized form of vocabulary that classifies or organizes things in the sciences or the arts into a clear and usable system.</p>
<p class='rw-defn idx8' style='display:none'>A patois is a form of language used by a people in a small area that is different from the national or standard language.</p>
<p class='rw-defn idx9' style='display:none'>Something prosaic is dull, boring, and ordinary.</p>
<p class='rw-defn idx10' style='display:none'>Rhetoric is the skill or art of using language to persuade or influence people, especially language that sounds impressive but may not be sincere or honest.</p>
<p class='rw-defn idx11' style='display:none'>A tacit agreement between two people is understood without having to use words to express it.</p>
<p class='rw-defn idx12' style='display:none'>Verbiage is an excessive use of words to convey something that could be expressed using fewer words; it can also be the manner or style in which someone uses words.</p>
<p class='rw-defn idx13' style='display:none'>A vernacular is the ordinary and everyday language spoken by people in a particular country or region that differs from the literary or standard written language of that area.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>parlance</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='parlance#' id='pronounce-sound' path='audio/words/amy-parlance'></a>
PAHR-luhns
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='parlance#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='parlance#' id='context-sound' path='audio/wordcontexts/brian-parlance'></a>
In sports language or <em>parlance</em>, one would say &#8220;he&#8217;s been KO&#8217;d,&#8221; which means &#8220;knocked out.&#8221;  So much of the language we use these days has its origins in the chosen phrases or <em>parlance</em> of sports and games, such as &#8220;slam dunk,&#8221; &#8220;get sacked,&#8221; or &#8220;strike out.&#8221;  All of these terms from sports, samples of a modern <em>parlance</em>, have now been adopted as everyday expressions.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When might you use a certain <em>parlance</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When you need to use specific tools to complete a task.
</li>
<li class='choice answer '>
<span class='result'></span>
When you are among people who all share a common vocabulary.
</li>
<li class='choice '>
<span class='result'></span>
When you need to dress appropriately for an occasion.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='parlance#' id='definition-sound' path='audio/wordmeanings/amy-parlance'></a>
You are applying <em>parlance</em> when you use words or expressions that are used by a particular group of people who have a unique way of speaking; for example, &#8220;descendants&#8221; is often used in place of &#8220;children&#8221; in legal <em>parlance</em>.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>special language</em>
</span>
</span>
</div>
<a class='quick-help' href='parlance#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='parlance#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/parlance/memory_hooks/4636.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Lance</span></span> for <span class="emp2"><span>Par</span></span></span></span> Just as the knights of old would use their <span class="emp3"><span>lance</span></span>s for knocking opponents off their horses, Tiger Woods uses his putter as a <span class="emp3"><span>lance</span></span> for getting <span class="emp2"><span>par</span></span> on most holes, thereby unseating his competition as well.  Could we not adopt the <span class="emp3"><span>lance</span></span> for <span class="emp2"><span>par</span></span>, or the <span class="emp2"><span>par</span></span> <span class="emp3"><span>lance</span></span>, as <span class="emp2"><span>par</span></span><span class="emp3"><span>lance</span></span> for the putter of a fabulous golfer?
</p>
</div>

<div id='memhook-button-bar'>
<a href="parlance#" id="add-public-hook" style="" url="https://membean.com/mywords/parlance/memory_hooks">Use other public hook</a>
<a href="parlance#" id="memhook-use-own" url="https://membean.com/mywords/parlance/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='parlance#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
In government <b>parlance</b>, they’re “roadable aircraft” as opposed to flying cars, and they may be coming to airports and roads in and around Portsmouth and Rochester sooner than later. That’s because a bill signed by Gov. Chris Sununu on July 24 makes New Hampshire the first state in the country to create a legal framework for registering, licensing and inspecting these vehicles that are part car/part plane.
<cite class='attribution'>
&mdash;
Seacoastonline.com
</cite>
</li>
<li>
If you wanted an iPad, you could pay $329 for the base model with 32 gigabytes of storage. But it’s probably a better idea to spend $429 on the model with 128 gigabytes of storage—that’s quadruple the capacity, which you can use to hold apps, games, photos and videos for years to come. In tech <b>parlance</b>, this strategy is known as “futureproofing.”
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Campaign officials and various experts said the current state of the campaign proves that news coverage—earned media in the <b>parlance</b> of political professionals—can be more determinative than money.
<cite class='attribution'>
&mdash;
East Bay Times
</cite>
</li>
<li>
British lawmakers have been granted the power to move to the head of the line at restaurants, restrooms and elevators inside the Houses of Parliament, enraging those assistants, researchers, janitors and other workers who must only stand and wait. Workers warn that Parliament is in danger of appearing decidedly undemocratic in allowing the lawmakers, in British <b>parlance</b>, to "jump the queue."
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/parlance/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='parlance#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='parl_talk' data-tree-url='//cdn1.membean.com/public/data/treexml' href='parlance#'>
<span class=''></span>
parl
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>talk, speak</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ance_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='parlance#'>
<span class=''></span>
-ance
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state or condition</td>
</tr>
</table>
<p><em>Parlance</em> is a particular &#8220;state or condition of speaking or talking.&#8221;</p>
<a href="parlance#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> French</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='parlance#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>The Twilight Zone</strong><span> An example of parlance.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/parlance.jpg' video_url='examplevids/parlance' video_width='350'></span>
<div id='wt-container'>
<img alt="Parlance" height="288" src="https://cdn1.membean.com/video/examplevids/parlance.jpg" width="350" />
<div class='center'>
<a href="parlance#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='parlance#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Parlance" src="https://cdn0.membean.com/public/images/wordimages/cons2/parlance.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='parlance#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>argot</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>articulate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>commune</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>interlocutor</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>jargon</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>lexicon</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>nomenclature</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>patois</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>rhetoric</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>verbiage</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>vernacular</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>homogeneous</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>prosaic</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>tacit</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="parlance" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>parlance</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="parlance#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

