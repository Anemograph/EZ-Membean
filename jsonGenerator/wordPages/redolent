
<!DOCTYPE html>
<html>
<head>
<title>Word: redolent | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you take an acerbic tone with someone, you are criticizing them in a clever but critical and mean way.</p>
<p class='rw-defn idx1' style='display:none'>An acrid smell or taste is strong, unpleasant, and stings your nose or throat.</p>
<p class='rw-defn idx2' style='display:none'>Something brackish is distasteful and unpleasant usually because something else is mixed in with it; brackish water, for instance, is a mixture of fresh and salt water and cannot be drunk.</p>
<p class='rw-defn idx3' style='display:none'>A comestible is something that can be eaten.</p>
<p class='rw-defn idx4' style='display:none'>If you describe something, especially food and drink, as delectable, you mean that it is very pleasant, tasty, or attractive.</p>
<p class='rw-defn idx5' style='display:none'>When someone disinters a dead body, they dig it up; likewise, something disinterred is exposed or revealed to the public after having been hidden for some time.</p>
<p class='rw-defn idx6' style='display:none'>If you evince particular feelings, qualities, or attitudes, you show them, often clearly.</p>
<p class='rw-defn idx7' style='display:none'>An evocation of something creates or summons a clear mental image or impression of it through words, pictures, or music.</p>
<p class='rw-defn idx8' style='display:none'>Something insipid is dull, boring, and has no interesting features; for example, insipid food has no taste or little flavor.</p>
<p class='rw-defn idx9' style='display:none'>If you describe ideas as jejune, you are saying they are childish and uninteresting; the adjective jejune also describes those having little experience, maturity, or knowledge.</p>
<p class='rw-defn idx10' style='display:none'>Your olfactory senses pertain to your sense of smell.</p>
<p class='rw-defn idx11' style='display:none'>If you describe something as palatable, such as an idea or suggestion, you believe that people will accept it; palatable food or drink is agreeable to the taste.</p>
<p class='rw-defn idx12' style='display:none'>Something that is piquant is interesting and exciting; food that is piquant is pleasantly spicy.</p>
<p class='rw-defn idx13' style='display:none'>Potable water is clean and safe to drink.</p>
<p class='rw-defn idx14' style='display:none'>Something that is reminiscent of something else reminds you of it in some way.</p>
<p class='rw-defn idx15' style='display:none'>Repugnance is a strong feeling of dislike for something or someone you find horrible and offensive.</p>
<p class='rw-defn idx16' style='display:none'>Something that is tantalizing is desirable and exciting; nevertheless, it may be out of reach, perhaps due to being too expensive.</p>
<p class='rw-defn idx17' style='display:none'>Something is toothsome when it is tasty, attractive, or pleasing in some way; this adjective can apply to food, people, or objects.</p>
<p class='rw-defn idx18' style='display:none'>If you describe something as unsavory, you mean that it is unpleasant or morally unacceptable.</p>
<p class='rw-defn idx19' style='display:none'>Something vapid is dull, boring, and/or tiresome.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>redolent</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='redolent#' id='pronounce-sound' path='audio/words/amy-redolent'></a>
RED-l-uhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='redolent#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='redolent#' id='context-sound' path='audio/wordcontexts/brian-redolent'></a>
The old attic was <em>redolent</em> with the strong smell of cedar, musty clothing, and distant stories of the past.  The children spent the rainy day exploring its nooks and trunks which were <em>redolent</em> or suggestive of a time long before their lives.  The children found that their new &#8220;treasure trove&#8221; was <em>redolent</em> of a detailed family history since almost every object reminded their grandmother of a story from the past.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When is something <em>redolent</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When it belongs to the distant past.
</li>
<li class='choice '>
<span class='result'></span>
When it has remained hidden for some time and is at long last brought to light.
</li>
<li class='choice answer '>
<span class='result'></span>
When it makes you remember another thing.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='redolent#' id='definition-sound' path='audio/wordmeanings/amy-redolent'></a>
If something is <em>redolent</em> of something else, it has features that make you think of it; this word also refers to a particular odor or scent that can be pleasantly strong.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>suggesting</em>
</span>
</span>
</div>
<a class='quick-help' href='redolent#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='redolent#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/redolent/memory_hooks/4976.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Dol</span></span>e Sc<span class="emp3"><span>ent</span></span></span></span> The re<span class="emp2"><span>dol</span></span><span class="emp3"><span>ent</span></span> sc<span class="emp3"><span>ent</span></span>s of <span class="emp2"><span>Dol</span></span>e bananas and pineapples filled the air of the fruit aisle, which was re<span class="emp2"><span>dol</span></span><span class="emp3"><span>ent</span></span> of tropical islands.
</p>
</div>

<div id='memhook-button-bar'>
<a href="redolent#" id="add-public-hook" style="" url="https://membean.com/mywords/redolent/memory_hooks">Use other public hook</a>
<a href="redolent#" id="memhook-use-own" url="https://membean.com/mywords/redolent/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='redolent#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Featuring Speech’s finest vocal performance, 'Sunshine' taps casual, singsong optimism, <b>redolent</b> of Southern nostalgia, so that Speech and his mixed-gender crew can sing determinedly about utopia.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
<li>
There is U.S. Open tennis, <b>redolent</b> of McEnroe and Connors and all the other rogues who were buzzed at Flushing Meadow by those commercial air carriers that evoke a more glamorous era: Pan Am, Eastern and Braniff.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
The endeavor to keep it flat isn’t entirely successful—creases add a dimension to the images, giving them a curvature <b>redolent</b> of Salvador Dali.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
Lizzo's breakthrough album _Cuz I Love You_, her fourth release, is the fruit of her hard work, and as much a return to classic pop values as it is the platform for an exciting new voice. It is <b>redolent</b> of classic blues and old-school hip-hop, and full of jokes that soul elders like Millie Jackson would appreciate.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/redolent/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='redolent#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='re_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='redolent#'>
<span class=''></span>
re-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ol_smell' data-tree-url='//cdn1.membean.com/public/data/treexml' href='redolent#'>
<span class=''></span>
ol
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>smell</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ent_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='redolent#'>
<span class=''></span>
-ent
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p>Something which is <em>redolent</em> &#8220;smells thoroughly&#8221; good, or its &#8220;smell thoroughly&#8221; suggests something one has once known.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='redolent#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Redolent" src="https://cdn2.membean.com/public/images/wordimages/cons2/redolent.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='redolent#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>comestible</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>delectable</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>disinter</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>evince</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>evocation</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>olfactory</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>palatable</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>piquant</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>potable</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>reminiscent</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>tantalize</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>toothsome</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>acerbic</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>acrid</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>brackish</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>insipid</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>jejune</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>repugnance</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>unsavory</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>vapid</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="redolent" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>redolent</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="redolent#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

