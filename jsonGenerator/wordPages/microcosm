
<!DOCTYPE html>
<html>
<head>
<title>Word: microcosm | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abridge a book or play, you make it shorter by making cuts to the original.</p>
<p class='rw-defn idx1' style='display:none'>When you allude to something or someone, often events or characters from literature or history, you refer to them in an indirect way.</p>
<p class='rw-defn idx2' style='display:none'>If one thing is analogous to another, a comparison can be made between the two because they are similar in some way.</p>
<p class='rw-defn idx3' style='display:none'>The antithesis of something is its opposite.</p>
<p class='rw-defn idx4' style='display:none'>When something is apropos, it is fitting to the moment or occasion.</p>
<p class='rw-defn idx5' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx6' style='display:none'>Attrition is the process of gradually decreasing the strength of something (such as an enemy force) by continually attacking it.</p>
<p class='rw-defn idx7' style='display:none'>To augment something is to increase its value or effectiveness by adding something to it.</p>
<p class='rw-defn idx8' style='display:none'>If a word or behavior connotes something, it suggests an additional idea or emotion that is not part of its original literal meaning.</p>
<p class='rw-defn idx9' style='display:none'>A copious amount of something is a very large amount of it.</p>
<p class='rw-defn idx10' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx11' style='display:none'>A diorama is a scene, reproduced in three dimensions, that places objects and figures in front of a painted background.</p>
<p class='rw-defn idx12' style='display:none'>If a part of your body distends, it becomes swollen and unnaturally large.</p>
<p class='rw-defn idx13' style='display:none'>If you use words in a figurative way, they have an abstract or symbolic meaning beyond their literal interpretation.</p>
<p class='rw-defn idx14' style='display:none'>An idea or remark is germane to a situation if it is connected to it in an important or fitting way.</p>
<p class='rw-defn idx15' style='display:none'>Hyperbole is a way of emphasizing something that makes it sound much more impressive or much worse than it actually is.</p>
<p class='rw-defn idx16' style='display:none'>The juxtaposition of two objects is the act of positioning them side by side so that the differences between them are more readily visible.</p>
<p class='rw-defn idx17' style='display:none'>A macrocosm is a large, complex, and organized system or structure that is made of many small parts that form one unit, such as a society or the universe.</p>
<p class='rw-defn idx18' style='display:none'>A metaphor is a word or phrase that means one thing but is used to describe something else in order to highlight similar qualities; for example, if someone is very brave, you might say that they have the heart of a lion.</p>
<p class='rw-defn idx19' style='display:none'>Something minuscule is extremely small in size or amount.</p>
<p class='rw-defn idx20' style='display:none'>Someone has a parochial outlook when they are mostly concerned with narrow or limited issues that affect a small, local area or a very specific cause; they are also unconcerned with wider or more general issues.</p>
<p class='rw-defn idx21' style='display:none'>A plethora of something is too much of it.</p>
<p class='rw-defn idx22' style='display:none'>Something that is prodigious is very large, impressive, or great.</p>
<p class='rw-defn idx23' style='display:none'>A profusion of something is a very large quantity or variety of it.</p>
<p class='rw-defn idx24' style='display:none'>A semblance is an outward appearance of what is wanted or expected but is not exactly as hoped for.</p>
<p class='rw-defn idx25' style='display:none'>A surrogate is someone who temporarily takes the place of another person, usually acting as a representative because that person is not available to or cannot carry out a task.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>microcosm</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='microcosm#' id='pronounce-sound' path='audio/words/amy-microcosm'></a>
MAHY-kruh-koz-uhm
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='microcosm#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='microcosm#' id='context-sound' path='audio/wordcontexts/brian-microcosm'></a>
Young Rob and Bob built a <em>microcosm</em> or smaller version of their much larger hometown using Legos.  The brothers spent weeks perfecting the skyline with hundreds of plastic blocks, connecting them to recreate a <em>microcosm</em> or small representation of their city.  Their mother&#8217;s eyes shone with approval at the focus with which the twins designed this model <em>microcosm</em> of their entire city.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If your school&#8217;s culture is a <em>microcosm</em>, what is it?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is nothing like the culture of the outside world.
</li>
<li class='choice answer '>
<span class='result'></span>
It is a smaller version of the larger culture.
</li>
<li class='choice '>
<span class='result'></span>
It is so unusual that other schools try to copy it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='microcosm#' id='definition-sound' path='audio/wordmeanings/amy-microcosm'></a>
A <em>microcosm</em> is a small group, place, or activity that has all the same qualities as a much larger one; therefore, it seems like a smaller version of it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>miniature copy</em>
</span>
</span>
</div>
<a class='quick-help' href='microcosm#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='microcosm#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/microcosm/memory_hooks/5914.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Micro</span></span>scopic <span class="emp2"><span>Cosm</span></span>os</span></span> "Yes, Mrs. Galloway, I did construct a <span class="emp3"><span>micro</span></span><span class="emp2"><span>cosm</span></span> of the <span class="emp2"><span>cosm</span></span>os!  But my <span class="emp3"><span>micro</span></span><span class="emp2"><span>cosm</span></span> of the universe is so small that you have to look at my <span class="emp2"><span>cosm</span></span>ic <span class="emp3"><span>micro</span></span><span class="emp2"><span>cosm</span></span> underneath your <span class="emp3"><span>micro</span></span>scope!  Isn't that the ultimate?"
</p>
</div>

<div id='memhook-button-bar'>
<a href="microcosm#" id="add-public-hook" style="" url="https://membean.com/mywords/microcosm/memory_hooks">Use other public hook</a>
<a href="microcosm#" id="memhook-use-own" url="https://membean.com/mywords/microcosm/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='microcosm#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Overwhelmed by riots and tensions, Miami was a city in flux, and the University of Miami football team served as a <b>microcosm</b> for this evolution.
<cite class='attribution'>
&mdash;
ESPN
</cite>
</li>
<li>
"Once America was a <b>microcosm</b> of European nationalities," says Molefi Asante, chairman of the department of African-American studies at Temple University in Philadelphia. "Today America is a <b>microcosm</b> of the world."
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
<li>
The struggle here in Frederick County, a crossroads of the Civil War, not far from Antietam and Gettysburg, is a <b>microcosm</b> of national debate over the child health bill.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/microcosm/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='microcosm#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='micro_small' data-tree-url='//cdn1.membean.com/public/data/treexml' href='microcosm#'>
<span class=''></span>
micro-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>small</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='cosm_universe' data-tree-url='//cdn1.membean.com/public/data/treexml' href='microcosm#'>
<span class=''></span>
cosm
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>universe</td>
</tr>
</table>
<p>A <em>microcosm</em> is a &#8220;small universe&#8221; that represents a &#8220;whole universe,&#8221; that is, it&#8217;s a &#8220;smaller&#8221; representative copy of a larger, &#8220;universal&#8221; whole.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='microcosm#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Rick and Morty</strong><span> Rick has created a microcosm.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/microcosm.jpg' video_url='examplevids/microcosm' video_width='350'></span>
<div id='wt-container'>
<img alt="Microcosm" height="288" src="https://cdn1.membean.com/video/examplevids/microcosm.jpg" width="350" />
<div class='center'>
<a href="microcosm#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='microcosm#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Microcosm" src="https://cdn2.membean.com/public/images/wordimages/cons2/microcosm.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='microcosm#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abridge</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>allude</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>analogous</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>apropos</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>attrition</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>connote</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>diorama</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>figurative</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>germane</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>juxtaposition</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>metaphor</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>minuscule</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>parochial</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>semblance</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>surrogate</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>antithesis</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>augment</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>copious</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>distend</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>hyperbole</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>macrocosm</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>plethora</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>prodigious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>profusion</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="microcosm" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>microcosm</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="microcosm#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

