
<!DOCTYPE html>
<html>
<head>
<title>Word: entropy | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Entropy-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/entropy-large.jpg?qdep8" />
</div>
<a href="entropy#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When you ameliorate a bad condition or situation, you make it better in some way.</p>
<p class='rw-defn idx1' style='display:none'>A state of anarchy occurs when there is no organization or order in a nation or country, especially when no effective government exists.</p>
<p class='rw-defn idx2' style='display:none'>Bedlam is a situation with a great deal of noise and confusion.</p>
<p class='rw-defn idx3' style='display:none'>A cohesive argument sticks together, working as a consistent, unified whole.</p>
<p class='rw-defn idx4' style='display:none'>When something is diffuse, it is widely spread out and scattered.</p>
<p class='rw-defn idx5' style='display:none'>When you are in a state of disarray, you are disorganized, disordered, and in a state of confusion.</p>
<p class='rw-defn idx6' style='display:none'>When you are discombobulated, you are confused and upset because you have been thrown into a situation that you cannot temporarily handle.</p>
<p class='rw-defn idx7' style='display:none'>Dissolution is the breaking up or official end of a group, such as a couple or institution; it can also be the act of separating something into smaller components.</p>
<p class='rw-defn idx8' style='display:none'>Something ephemeral, such as some insects or a sunset, lasts for only a short time or has a very short lifespan.</p>
<p class='rw-defn idx9' style='display:none'>Something that is evanescent lasts for only a short time before disappearing from sight or memory.</p>
<p class='rw-defn idx10' style='display:none'>When there is havoc, there is great disorder, widespread destruction, and much confusion.</p>
<p class='rw-defn idx11' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx12' style='display:none'>If someone leaves an indelible impression on you, it will not be forgotten; an indelible mark is permanent or impossible to erase or remove.</p>
<p class='rw-defn idx13' style='display:none'>Something that is interminable continues for a very long time in a boring or annoying way.</p>
<p class='rw-defn idx14' style='display:none'>An inveterate person is always doing a particular thing, especially something questionable—and they are not likely to stop doing it.</p>
<p class='rw-defn idx15' style='display:none'>Someone who is irresolute is unable to decide what to do.</p>
<p class='rw-defn idx16' style='display:none'>A languid person is slow, relaxed, and shows little energy or interest in doing anything.</p>
<p class='rw-defn idx17' style='display:none'>Someone who is malleable is easily influenced or controlled by other people.</p>
<p class='rw-defn idx18' style='display:none'>Someone is considered meticulous when they act with careful attention to detail.</p>
<p class='rw-defn idx19' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx20' style='display:none'>Pandemonium is a very noisy and uncontrolled situation, especially one that is caused by a lot of angry or excited people.</p>
<p class='rw-defn idx21' style='display:none'>Something that is perennial lasts a very long time and is enduring.</p>
<p class='rw-defn idx22' style='display:none'>Someone who is pertinacious is determined to continue doing something rather than giving up—even when it gets very difficult.</p>
<p class='rw-defn idx23' style='display:none'>A person who is plastic can be easily influenced and molded by others.</p>
<p class='rw-defn idx24' style='display:none'>Something or someone that is protean is adaptable, variable, or changeable in nature.</p>
<p class='rw-defn idx25' style='display:none'>To be punctilious is to pay precise attention to detail.</p>
<p class='rw-defn idx26' style='display:none'>Someone who is sedulous works hard and performs tasks very carefully and thoroughly, not stopping their work until it is completely accomplished.</p>
<p class='rw-defn idx27' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx28' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx29' style='display:none'>Something that is temporal deals with the present and somewhat brief time of this world.</p>
<p class='rw-defn idx30' style='display:none'>If your body is affected by torpor, you are severely lacking in energy; therefore, you are idle—and can even be numb.</p>
<p class='rw-defn idx31' style='display:none'>Something that has the quality of transience lasts for only a short time or is constantly changing.</p>
<p class='rw-defn idx32' style='display:none'>When you experience turmoil, there is great confusion, disturbance, instability, and disorder in your life.</p>
<p class='rw-defn idx33' style='display:none'>If you are unflagging while doing a task, you are untiring when working upon it and do not stop until it is finished.</p>
<p class='rw-defn idx34' style='display:none'>When someone vacillates, they go back and forth about a choice or opinion, unable to make a firm decision.</p>
<p class='rw-defn idx35' style='display:none'>Something that is volatile can change easily and vary widely.</p>
<p class='rw-defn idx36' style='display:none'>If you waver, you cannot decide between two things because you have serious doubts about which choice is better.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>entropy</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='entropy#' id='pronounce-sound' path='audio/words/amy-entropy'></a>
EN-truh-pee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='entropy#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='entropy#' id='context-sound' path='audio/wordcontexts/brian-entropy'></a>
Steam has higher <em>entropy</em> when compared to liquid water or ice because the molecules in steam are freer to move in random directions, making it less organized.  When a system is left by itself without maintenance, it tends to move towards <em>entropy</em>, or disorder.  For example, just before an outbreak of a civil war, a society is in a high state of decline or <em>entropy</em>.  When I don&#8217;t clean my room, it tends quickly towards <em>entropy</em> or lack of orderliness because I&#8217;m not around to put the energy into keeping it neat and clean.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of <em>entropy</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
The use of a word or phrase in a non-literal way to create an image in a reader&#8217;s mind.
</li>
<li class='choice '>
<span class='result'></span>
The ability one person has to understand another person&#8217;s feelings in a certain situation.
</li>
<li class='choice answer '>
<span class='result'></span>
The gradual falling apart of a building that is not maintained over time.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='entropy#' id='definition-sound' path='audio/wordmeanings/amy-entropy'></a>
<em>Entropy</em> is the lack of organization or measure of disorder currently in a system.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>systemic degree of disorder</em>
</span>
</span>
</div>
<a class='quick-help' href='entropy#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='entropy#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/entropy/memory_hooks/6098.json'></span>
<p>
<span class="emp0"><span>B<span class="emp2"><span>ent</span></span> <span class="emp3"><span>Rop</span></span>e</span></span> A straight <span class="emp3"><span>rop</span></span>e is in a state of high, organized energy, whereas a b<span class="emp2"><span>ent</span></span> <span class="emp3"><span>rop</span></span>e is indicative of a state of <span class="emp2"><span>ent</span></span><span class="emp3"><span>rop</span></span>y, or the more natural, at rest, and lower energy state of a <span class="emp3"><span>rop</span></span>e.
</p>
</div>

<div id='memhook-button-bar'>
<a href="entropy#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/entropy/memory_hooks">Use other hook</a>
<a href="entropy#" id="memhook-use-own" url="https://membean.com/mywords/entropy/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='entropy#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
A neatly stacked collection of papers has a low <b>entropy</b>, while the same collection scattered across a desktop has a high <b>entropy</b>. The <b>entropy</b> of any system left to its own devices will either increase with time or stay constant; that is the celebrated second law of thermodynamics.
<cite class='attribution'>
&mdash;
Discover Magazine
</cite>
</li>
<li>
Just as the shuffling of cards increases the <b>entropy</b> of the deck, the bumping and jostling of particles and atoms and molecules increases <b>entropy</b> in the world around us, with each interaction acting to exchange or create information.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Analyzing 16 play-off basketball games from the 2010 season, the “researchers graphed player positions and ball movement among players, as well as shots taken,” says Arizona State University. . . . According to the scientists, “in the context of the 2010 play-offs, the values of clustering (connectedness across players) and network <b>entropy</b> (unpredictability of ball movement) had the most consistent association with team advancement.”
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
We all live in chaos. It’s the natural state of things. Physicists call it <b>entropy—everything</b> is always changing and unraveling and ultimately turning into cosmic mush. . . . Creativity is the act of shaping the mush of the world around us into something—of creating your own order.
<cite class='attribution'>
&mdash;
Danny Gregory, English author and artist, from _Art Before Breakfast_
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/entropy/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='entropy#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='en_in' data-tree-url='//cdn1.membean.com/public/data/treexml' href='entropy#'>
<span class='common'></span>
en-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>in, on</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='trop_turn' data-tree-url='//cdn1.membean.com/public/data/treexml' href='entropy#'>
<span class=''></span>
trop
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>turn, change</td>
</tr>
</table>
<p><em>Entropy</em> is the tendency towards &#8220;changing into&#8221; a state of disorder from order, or &#8220;turning&#8221; from organization to disorganization, or, in terms of physics, a natural &#8220;turning&#8221; from a high energy state to a low energy state.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='entropy#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Entropy" src="https://cdn3.membean.com/public/images/wordimages/cons2/entropy.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='entropy#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>anarchy</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bedlam</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>diffuse</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>disarray</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>discombobulated</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>dissolution</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>ephemeral</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>evanescent</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>havoc</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>irresolute</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>languid</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>malleable</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>pandemonium</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>plastic</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>protean</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>temporal</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>torpor</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>transience</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>turmoil</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>vacillate</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>volatile</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>waver</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>ameliorate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>cohesive</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>indelible</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>interminable</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>inveterate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>meticulous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>perennial</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>pertinacious</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>punctilious</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>sedulous</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>unflagging</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="entropy" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>entropy</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="entropy#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

