
<!DOCTYPE html>
<html>
<head>
<title>Word: conjecture | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you adduce, you give facts and examples in order to prove that something is true.</p>
<p class='rw-defn idx1' style='display:none'>To assay something, such as a substance, is to analyze the chemicals present in it; it can also refer to putting something to the test.</p>
<p class='rw-defn idx2' style='display:none'>If you aver that something is the case, you say firmly and strongly that you believe it is true.</p>
<p class='rw-defn idx3' style='display:none'>An axiom is a wise saying or widely recognized general truth that is often self-evident; it can also be an established rule or law.</p>
<p class='rw-defn idx4' style='display:none'>That which is circumstantial is not an essential or primary part of something; rather, it is merely incidental and perhaps insignificant to the situation at hand.</p>
<p class='rw-defn idx5' style='display:none'>To corroborate something that has been said or reported means to give information or evidence that further supports it.</p>
<p class='rw-defn idx6' style='display:none'>Credence is the mental act of believing or accepting that something is true.</p>
<p class='rw-defn idx7' style='display:none'>If you deliberate, you think about or discuss something very carefully, especially before making an important decision.</p>
<p class='rw-defn idx8' style='display:none'>Empirical evidence or study is based on real experience or scientific experiments rather than on unproven theories.</p>
<p class='rw-defn idx9' style='display:none'>A heuristic method of teaching encourages learning based on students&#8217; discovering and experiencing things for themselves.</p>
<p class='rw-defn idx10' style='display:none'>Something that is hypothetical is based on possible situations or events rather than actual ones.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is irresolute is unable to decide what to do.</p>
<p class='rw-defn idx12' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx13' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx14' style='display:none'>If someone is pedantic, they give too much importance to unimportant details and formal rules.</p>
<p class='rw-defn idx15' style='display:none'>If you postulate something, you assert that it is true without proof; therefore, it can be used as a basis for argument or reasoning—even though there is no factual basis for the assumption.</p>
<p class='rw-defn idx16' style='display:none'>Something putative is supposed to be real; for example, a putative leader is one who everyone assumes is the leader—even though they may not be in reality.</p>
<p class='rw-defn idx17' style='display:none'>If you surmise why something has occurred, you make a guess, offer a possibility, or have a theory about it.</p>
<p class='rw-defn idx18' style='display:none'>To be tentative is to be hesitant or uncertain about something; an agreement or decision of this kind is likely to have changes before it reaches its final form.</p>
<p class='rw-defn idx19' style='display:none'>Something that has the quality of transience lasts for only a short time or is constantly changing.</p>
<p class='rw-defn idx20' style='display:none'>A vagary is an unpredictable or unexpected change in action or behavior.</p>
<p class='rw-defn idx21' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx22' style='display:none'>Verisimilitude is something&#8217;s authenticity or appearance of being real or true.</p>
<p class='rw-defn idx23' style='display:none'>The verity of something is the truth or reality of it.</p>
<p class='rw-defn idx24' style='display:none'>Vicissitudes can be unexpected or simply normal changes and variations that happen during the course of people&#8217;s lives.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>conjecture</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='conjecture#' id='pronounce-sound' path='audio/words/amy-conjecture'></a>
kuhn-JEK-cher
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='conjecture#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='conjecture#' id='context-sound' path='audio/wordcontexts/brian-conjecture'></a>
Based upon the limited information that he had, the pilot formed a quick <em>conjecture</em> or theory about how the plane&#8217;s flight path should proceed.  Fortunately for the passengers and crew, the pilot&#8217;s years of experience supported his guess or <em>conjecture</em>, even though the instruments were inaccurate.  In the end, the pilot&#8217;s quick but informed <em>conjecture</em> or speculation and his steady actions landed the plane safely on the ground.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How would you most likely react to someone&#8217;s <em>conjecture</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
You&#8217;d want more convincing evidence to back up their theory.
</li>
<li class='choice '>
<span class='result'></span>
You&#8217;d appreciate their popular idea because it&#8217;s so interesting.
</li>
<li class='choice '>
<span class='result'></span>
You&#8217;d believe their argument was solid and clear, in need of no more explanation.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='conjecture#' id='definition-sound' path='audio/wordmeanings/amy-conjecture'></a>
A <em>conjecture</em> is a theory or guess that is based on information that is not certain or complete.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>guess</em>
</span>
</span>
</div>
<a class='quick-help' href='conjecture#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='conjecture#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/conjecture/memory_hooks/3482.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Con</span></span> L<span class="emp3"><span>ecture</span></span></span></span> I came away from the l<span class="emp3"><span>ecture</span></span> on the healing powers of crystals convinced that it was just one huge <span class="emp1"><span>con</span></span>.  Not a shred of concrete proof was offered, only a doubtful series of <span class="emp1"><span>con</span></span>j<span class="emp3"><span>ecture</span></span>s.
</p>
</div>

<div id='memhook-button-bar'>
<a href="conjecture#" id="add-public-hook" style="" url="https://membean.com/mywords/conjecture/memory_hooks">Use other public hook</a>
<a href="conjecture#" id="memhook-use-own" url="https://membean.com/mywords/conjecture/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='conjecture#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
To be willing to die for an idea is to set a rather high price on <b>conjecture</b>.
<span class='attribution'>&mdash; Anatole France, French writer</span>
<img alt="Anatole france, french writer" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Anatole France, French writer.jpg?qdep8" width="80" />
</li>
<li>
Becoming Jane, starring Anne Hathaway as the author and James McAvoy as her romantic interest Tom Lefroy, starts with the few known facts about Austen’s life, adds a bit of academic <b>conjecture</b>, and weaves in some outright invention.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
Blandford, known for his impeccable scholarship, stated: 'The <b>conjecture</b> that Beethoven was in any way influenced by the valve horn is as foolish as it is without foundation.'
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
The Poincare <b>conjecture</b> belongs to the field of topology, which studies properties that are preserved when a shape is stretched or twisted without tearing.
<cite class='attribution'>
&mdash;
Science News
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/conjecture/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='conjecture#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='con_with' data-tree-url='//cdn1.membean.com/public/data/treexml' href='conjecture#'>
<span class='common'></span>
con-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>with, together</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ject_thrown' data-tree-url='//cdn1.membean.com/public/data/treexml' href='conjecture#'>
<span class='common'></span>
ject
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thrown</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ure_shows' data-tree-url='//cdn1.membean.com/public/data/treexml' href='conjecture#'>
<span class=''></span>
-ure
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>shows actions or results</td>
</tr>
</table>
<p>A <em>conjecture</em> is a guess &#8220;thrown together&#8221; as only a possibility after incomplete research.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='conjecture#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>PBS Nova Science Now</strong><span> The Twin Prime Conjecture song.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/conjecture.jpg' video_url='examplevids/conjecture' video_width='350'></span>
<div id='wt-container'>
<img alt="Conjecture" height="198" src="https://cdn1.membean.com/video/examplevids/conjecture.jpg" width="350" />
<div class='center'>
<a href="conjecture#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='conjecture#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Conjecture" src="https://cdn3.membean.com/public/images/wordimages/cons2/conjecture.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='conjecture#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adduce</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>assay</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>circumstantial</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>credence</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>deliberate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>heuristic</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>hypothetical</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>irresolute</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>postulate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>putative</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>surmise</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>tentative</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>transience</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>vagary</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>vicissitude</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>aver</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>axiom</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>corroborate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>empirical</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>pedantic</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>verisimilitude</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='conjecture#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
conjecture
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to guess, hypothesize, or speculate</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="conjecture" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>conjecture</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="conjecture#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

