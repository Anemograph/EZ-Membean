
<!DOCTYPE html>
<html>
<head>
<title>Word: ingenuous | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you describe something as ambiguous, you mean that it is unclear or confusing because it can be understood in multiple ways.</p>
<p class='rw-defn idx1' style='display:none'>When someone speaks in an artless fashion, they are without deception; when someone acts artlessly, they are being totally natural.</p>
<p class='rw-defn idx2' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx3' style='display:none'>Someone who is callow is young, immature, and inexperienced; consequently, they possess little knowledge of the world.</p>
<p class='rw-defn idx4' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx5' style='display:none'>If you employ chicanery, you are devising and carrying out clever plans and trickery to cheat and deceive people.</p>
<p class='rw-defn idx6' style='display:none'>Circumlocution is a way of saying or writing something that uses too many words, especially in order to avoid stating the true meaning clearly.</p>
<p class='rw-defn idx7' style='display:none'>Something that is clandestine is hidden or kept a secret, often because it is illegal.</p>
<p class='rw-defn idx8' style='display:none'>If you collude with people, you work secretly with them to do something dishonest.</p>
<p class='rw-defn idx9' style='display:none'>If one person connives with another, they secretly plan to achieve something of mutual benefit, usually a thing that is illegal or immoral.</p>
<p class='rw-defn idx10' style='display:none'>Covert activities or situations are secret and hidden.</p>
<p class='rw-defn idx11' style='display:none'>Something that is delusive deceives you by giving a false belief about yourself or the situation you are in.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is disingenuous is not straightforward or is dishonest in what they say or do.</p>
<p class='rw-defn idx13' style='display:none'>If you accuse someone of duplicity, you think that they are dishonest and are intending to trick you.</p>
<p class='rw-defn idx14' style='display:none'>When people equivocate, they avoid making a clear statement; they are deliberately vague in order to avoid giving a direct answer to a question.</p>
<p class='rw-defn idx15' style='display:none'>If you feign something, such as a sickness or an attitude, you pretend and try to convince people that you have it—even though you don&#8217;t.</p>
<p class='rw-defn idx16' style='display:none'>A gullible person is easy to trick because they are too trusting of other people.</p>
<p class='rw-defn idx17' style='display:none'>A mendacious person does not tell the truth.</p>
<p class='rw-defn idx18' style='display:none'>If someone is naive, they are too trusting of others; they don&#8217;t have enough experience in life to know whom to believe.</p>
<p class='rw-defn idx19' style='display:none'>An overt act is not hidden or secret but is done in an open and public way.</p>
<p class='rw-defn idx20' style='display:none'>Someone who is perfidious is not loyal and cannot be trusted.</p>
<p class='rw-defn idx21' style='display:none'>If you prevaricate, you avoid giving a direct or honest answer, usually because you want to hide the truth or want to delay or avoid making a hard decision.</p>
<p class='rw-defn idx22' style='display:none'>A tyro has just begun learning something.</p>
<p class='rw-defn idx23' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx24' style='display:none'>An unctuous person acts in an overly deceptive manner that is obviously insincere because they want to convince you of something.</p>
<p class='rw-defn idx25' style='display:none'>If you behave in an urbane way, you are behaving in a polite, refined, and civilized fashion in social situations.</p>
<p class='rw-defn idx26' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx27' style='display:none'>Something that is vernal occurs in spring; since spring is the time when new plants start to grow, the adjective vernal can also be used to suggest youth and freshness.</p>
<p class='rw-defn idx28' style='display:none'>Wiles are clever tricks or cunning schemes that are used to persuade someone to do what you want.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>ingenuous</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='ingenuous#' id='pronounce-sound' path='audio/words/amy-ingenuous'></a>
in-JEN-yoo-uhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='ingenuous#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='ingenuous#' id='context-sound' path='audio/wordcontexts/brian-ingenuous'></a>
When Mona moved alone to the city, her brother was worried that her <em>ingenuous</em>, innocent, and trusting ways might make her an easy target.  He explained to her that she should not believe the best about everyone, and that being so open, unsophisticated, and <em>ingenuous</em> could be dangerous.  Mona argued that people were basically good at heart, and that if she chose to believe this in a trusting, <em>ingenuous</em> way, then it was her choice.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Which of the following illustrates an <em>ingenuous</em> quality in someone?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
An older woman who has experienced great loss and now only expects bad things to happen to her. 
</li>
<li class='choice answer '>
<span class='result'></span>
A teenage boy who is leaving home for the first time and who too easily trusts everyone he meets.
</li>
<li class='choice '>
<span class='result'></span>
A politician who is known for coming up with clever solutions to problems that others find overwhelming.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='ingenuous#' id='definition-sound' path='audio/wordmeanings/amy-ingenuous'></a>
People who are <em>ingenuous</em> are excessively trusting and believe almost everything that people tell them, especially because they have not had much life experience.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>too trusting</em>
</span>
</span>
</div>
<a class='quick-help' href='ingenuous#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='ingenuous#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/ingenuous/memory_hooks/5740.json'></span>
<p>
<span class="emp0"><span>G<span class="emp2"><span>en</span></span>er<span class="emp2"><span>ous</span></span> Th<span class="emp3"><span>ing</span></span></span></span> <span class="emp3"><span>Ing</span></span><span class="emp2"><span>en</span></span>u<span class="emp2"><span>ous</span></span> <span class="emp3"><span>Ing</span></span>a was such a g<span class="emp2"><span>en</span></span>er<span class="emp2"><span>ous</span></span> th<span class="emp3"><span>ing</span></span>, giving away all of her valuables and possessions because she <span class="emp3"><span>ing</span></span><span class="emp2"><span>en</span></span>u<span class="emp2"><span>ous</span></span>ly believed in all the bleeding heart stories of all the liars and con-men she met on the streets.
</p>
</div>

<div id='memhook-button-bar'>
<a href="ingenuous#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/ingenuous/memory_hooks">Use other hook</a>
<a href="ingenuous#" id="memhook-use-own" url="https://membean.com/mywords/ingenuous/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='ingenuous#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The movie’s rare moments of redemption come only from [Margaret] Langrick, an <b>ingenuous</b>, pie-faced actress whose pouts and eye-rolling capture everything in adolescence that is sulky, self-involved, irksome and even, at times, ingratiating.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Camilla's values aren't original—they're the values of the British upper class, and they're not as innocent as they seem. The shabby clothing and the perennial bad hair day are not the <b>ingenuous</b> result of indifference to fashion; they are a calculated statement of superiority to fashion.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/ingenuous/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='ingenuous#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ingenu_honest' data-tree-url='//cdn1.membean.com/public/data/treexml' href='ingenuous#'>
<span class=''></span>
ingenu
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>honest</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ous_possessing' data-tree-url='//cdn1.membean.com/public/data/treexml' href='ingenuous#'>
<span class=''></span>
-ous
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>possessing the nature of</td>
</tr>
</table>
<p>Someone who is <em>ingenuous</em> not only &#8220;possesses the nature of being honest,&#8221; but also believes that everyone else possesses that nature as well.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='ingenuous#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Ingenuous" src="https://cdn1.membean.com/public/images/wordimages/cons2/ingenuous.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='ingenuous#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>artless</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>callow</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>gullible</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>naive</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>overt</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>tyro</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>vernal</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>ambiguous</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>chicanery</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>circumlocution</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>clandestine</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>collude</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>connive</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>covert</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>delusive</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>disingenuous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>duplicity</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>equivocate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>feign</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>mendacious</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>perfidious</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>prevaricate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>unctuous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>urbane</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>wile</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="ingenuous" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>ingenuous</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="ingenuous#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

