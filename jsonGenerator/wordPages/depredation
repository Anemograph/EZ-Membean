
<!DOCTYPE html>
<html>
<head>
<title>Word: depredation | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Depredation-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/depredation-large.jpg?qdep8" />
</div>
<a href="depredation#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When you are acquisitive, you are driven to pursue and own wealth and possessions—often in a greedy fashion.</p>
<p class='rw-defn idx1' style='display:none'>If your behavior or manner is altruistic, you show you care more about other people and their interests than you care about yourself.</p>
<p class='rw-defn idx2' style='display:none'>If you appropriate something that does not belong to you, you take it for yourself without the right to do so.</p>
<p class='rw-defn idx3' style='display:none'>When you arrogate something, such as a position or privilege, you take it even though you don&#8217;t have the legal right to it.</p>
<p class='rw-defn idx4' style='display:none'>Avarice is the extremely strong desire to have a lot of money and possessions.</p>
<p class='rw-defn idx5' style='display:none'>A bastion is considered an important and effective defense of a way of life or principle; a bastion can also refer to a heavily fortified place, such as a castle.</p>
<p class='rw-defn idx6' style='display:none'>Bedlam is a situation with a great deal of noise and confusion.</p>
<p class='rw-defn idx7' style='display:none'>A benefaction is a charitable contribution of money or assistance that someone gives to a person or organization.</p>
<p class='rw-defn idx8' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx9' style='display:none'>When you bequeath something, you hand it down to someone in a will or pass it on from one generation to the next.</p>
<p class='rw-defn idx10' style='display:none'>When something is bestowed upon you—usually something valuable—you are given or presented it.</p>
<p class='rw-defn idx11' style='display:none'>When you are granted a boon, you are given a special gift or favor that is of great benefit to you.</p>
<p class='rw-defn idx12' style='display:none'>Mass carnage is the massacre or slaughter of many people at one time, usually in battle or from an unusually intense natural disaster.</p>
<p class='rw-defn idx13' style='display:none'>If you covet something that someone else has, you have a strong desire to have it for yourself.</p>
<p class='rw-defn idx14' style='display:none'>If you behave with deference towards someone, you show them respect and accept their opinions or decisions, especially because they have an important position.</p>
<p class='rw-defn idx15' style='display:none'>To defile something holy or important is to do something to it or say something about it that is highly offensive to others.</p>
<p class='rw-defn idx16' style='display:none'>To denude an area is to remove the plants and trees that cover it; it can also mean to make something bare.</p>
<p class='rw-defn idx17' style='display:none'>Something, such as a building, is derelict if it is empty, not used, and in bad condition or disrepair.</p>
<p class='rw-defn idx18' style='display:none'>If you desecrate something that is considered holy or very special, you deliberately spoil or damage it.</p>
<p class='rw-defn idx19' style='display:none'>A dilapidated building, vehicle, etc. is old, broken-down, and in very bad condition.</p>
<p class='rw-defn idx20' style='display:none'>If something engenders a particular feeling or attitude, it causes that condition.</p>
<p class='rw-defn idx21' style='display:none'>If you refer to the enormity of a situation, problem, or event, you mean that it is very evil or morally offensive; this word has nothing to do with physical size.</p>
<p class='rw-defn idx22' style='display:none'>If you expropriate something, you take it away for your own use although it does not belong to you; governments frequently expropriate private land to use for public purposes.</p>
<p class='rw-defn idx23' style='display:none'>When there is havoc, there is great disorder, widespread destruction, and much confusion.</p>
<p class='rw-defn idx24' style='display:none'>An incursion is an unpleasant intrusion, such as a sudden hostile attack or a land being flooded.</p>
<p class='rw-defn idx25' style='display:none'>Largess is the generous act of giving money or presents to a large number of people.</p>
<p class='rw-defn idx26' style='display:none'>If someone exhibits magnanimity towards another, they show them kindness and noble generosity, especially after defeating them in battle or after having been treated badly by them.</p>
<p class='rw-defn idx27' style='display:none'>A mercenary person is one whose sole interest is in earning money.</p>
<p class='rw-defn idx28' style='display:none'>A munificent person is extremely generous, especially with money.</p>
<p class='rw-defn idx29' style='display:none'>Pandemonium is a very noisy and uncontrolled situation, especially one that is caused by a lot of angry or excited people.</p>
<p class='rw-defn idx30' style='display:none'>Philanthropy is unselfish support in the form of donating money, work, or gifts to positive social purposes; philanthropy is also overall love for humans in general.</p>
<p class='rw-defn idx31' style='display:none'>If soldiers pillage a place, such as a town or museum, they steal a lot of things and damage it using violent methods.</p>
<p class='rw-defn idx32' style='display:none'>When you plunder, you forcefully take or rob others of their possessions, usually during times of war, natural disaster, or other unrest.</p>
<p class='rw-defn idx33' style='display:none'>When you proffer something to someone, you offer it up or hold it out to them to take.</p>
<p class='rw-defn idx34' style='display:none'>To purloin is to steal.</p>
<p class='rw-defn idx35' style='display:none'>To ransack a place is to thoroughly loot or rob items from it; it can also mean to look through something thoroughly by examining every bit of it.</p>
<p class='rw-defn idx36' style='display:none'>When you ravage something, you completely destroy, wreck, or damage it.</p>
<p class='rw-defn idx37' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>
<p class='rw-defn idx38' style='display:none'>If you say someone&#8217;s behavior is unseemly, you disapprove of it because it is not in good taste or not suitable for a particular situation.</p>
<p class='rw-defn idx39' style='display:none'>When you usurp someone else&#8217;s power, position, or role, you take it from them although you do not have the right to do so.</p>
<p class='rw-defn idx40' style='display:none'>Venerable people command respect because they are old and wise.</p>
<p class='rw-defn idx41' style='display:none'>A windfall is the sudden appearance of a large amount of money that is completely unexpected.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>depredation</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='depredation#' id='pronounce-sound' path='audio/words/amy-depredation'></a>
dep-ri-DAY-shun
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='depredation#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='depredation#' id='context-sound' path='audio/wordcontexts/brian-depredation'></a>
Monasteries in Europe during the Middle Ages suffered from the <em>depredation</em> of the Vikings as they looted and robbed them of their priceless books and religious items.  The Vikings would attack unawares; this <em>depredation</em> would create confusion amongst the peoples so assaulted, easing the victory of the Vikings.  Luckily monasteries in Ireland did not suffer such <em>depredation</em> or destruction, so they were able to save a great deal of the rich culture of medieval Europe that otherwise would have been lost.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If <em>depredation</em> occurred, what has happened?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
An animal has escaped a predator&#8217;s attack.
</li>
<li class='choice '>
<span class='result'></span>
An ancestor has handed down a legacy.
</li>
<li class='choice answer '>
<span class='result'></span>
An area has been plundered and left in ruins.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='depredation#' id='definition-sound' path='audio/wordmeanings/amy-depredation'></a>
<em>Depredation</em> can be the act of destroying or robbing a village—or the overall damage that time can do to things held dear.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>rob</em>
</span>
</span>
</div>
<a class='quick-help' href='depredation#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='depredation#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/depredation/memory_hooks/5751.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Predat</span></span>or <span class="emp3"><span>De</span></span>struct<span class="emp3"><span>ion</span></span></span></span> <span class="emp3"><span>De</span></span><span class="emp1"><span>predat</span></span><span class="emp3"><span>ion</span></span> is caused by the <span class="emp3"><span>de</span></span>struct<span class="emp3"><span>ion</span></span> of <span class="emp1"><span>predat</span></span>ors.
</p>
</div>

<div id='memhook-button-bar'>
<a href="depredation#" id="add-public-hook" style="" url="https://membean.com/mywords/depredation/memory_hooks">Use other public hook</a>
<a href="depredation#" id="memhook-use-own" url="https://membean.com/mywords/depredation/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='depredation#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Without unscrambled eggs, there was no time travel, no more <b>depredation</b> of the Now, and we could look to a brighter future of long-term thought—and more reading.
<span class='attribution'>&mdash; Jasper Fforde, British novelist, _First Among Sequels_</span>
<img alt="Jasper fforde, british novelist, _first among sequels_" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Jasper Fforde, British novelist, _First Among Sequels_.jpg?qdep8" width="80" />
</li>
<li>
While the major part of the natural environment in [Asia], Africa and Latin America is being plundered by the political and economic domination of certain powers, the situation of poverty and <b>depredation</b> is very alarming.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
Tuareg villages were attacked, looted and sometimes burned by government troops and mercenaries from other desert tribes. (Ber was spared.) Before the Tuaregs and the government concluded a peace deal in 1996, Ber's inhabitants dispersed all but a few hundred manuscripts to settlements deep in the Sahara, or buried them in the sand. It was a modern-day version of a story that has played out in Mali for centuries, a story of war, <b>depredation</b> and loss. "I'm starting to locate the manuscripts again," Mohammed tells me. "But it takes time."
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/depredation/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='depredation#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='de_off' data-tree-url='//cdn1.membean.com/public/data/treexml' href='depredation#'>
<span class='common'></span>
de-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>off, from</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pred_plunder' data-tree-url='//cdn1.membean.com/public/data/treexml' href='depredation#'>
<span class=''></span>
pred
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>loot, plunder, spoils</td>
</tr>
<tr>
<td class='partform'>-ation</td>
<td>
&rarr;
</td>
<td class='meaning'>act of doing something</td>
</tr>
</table>
<p><em>Depredation</em> is the &#8220;act of looting or plundering&#8221; conquered peoples.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='depredation#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>True Grit</strong><span> Mattie gives little thought to the end of Coke Hayes' life, since it signaled the end of his various depredations.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/depredation.jpg' video_url='examplevids/depredation' video_width='350'></span>
<div id='wt-container'>
<img alt="Depredation" height="288" src="https://cdn1.membean.com/video/examplevids/depredation.jpg" width="350" />
<div class='center'>
<a href="depredation#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='depredation#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Depredation" src="https://cdn2.membean.com/public/images/wordimages/cons2/depredation.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='depredation#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>acquisitive</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>appropriate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>arrogate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>avarice</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>bedlam</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>carnage</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>covet</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>defile</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>denude</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>derelict</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>desecrate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>dilapidated</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>enormity</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>expropriate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>havoc</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>incursion</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>mercenary</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>pandemonium</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>pillage</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>plunder</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>purloin</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>ransack</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>ravage</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>unseemly</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>usurp</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>altruistic</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>bastion</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>benefaction</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>bequeath</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>bestow</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>boon</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>deference</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>engender</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>largess</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>magnanimity</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>munificent</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>philanthropy</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>proffer</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>venerable</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>windfall</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='depredation#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
predator
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>that which preys on something else</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="depredation" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>depredation</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="depredation#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

