
<!DOCTYPE html>
<html>
<head>
<title>Word: nondescript | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Something that is amorphous has no clear shape, boundaries, or structure.</p>
<p class='rw-defn idx1' style='display:none'>The best or perfect example of something is its apotheosis; likewise, this word can be used to indicate the best or highest point in someone&#8217;s life or job.</p>
<p class='rw-defn idx2' style='display:none'>A bravura performance, such as of a highly technical work for the violin, is done with consummate skill.</p>
<p class='rw-defn idx3' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx4' style='display:none'>Ennui is the feeling of being bored, tired, and dissatisfied with life.</p>
<p class='rw-defn idx5' style='display:none'>If you say that a person or thing is the epitome of something, you mean that they or it is the best possible example of that thing.</p>
<p class='rw-defn idx6' style='display:none'>Something esoteric is known about or understood by very few people.</p>
<p class='rw-defn idx7' style='display:none'>An action that is flagrant shows that someone does not care if they obviously break the rules or highly offend people.</p>
<p class='rw-defn idx8' style='display:none'>A genre is a category, type, or class of artistic work.</p>
<p class='rw-defn idx9' style='display:none'>Hackneyed words, images, ideas or sayings have been used so often that they no longer seem interesting or amusing.</p>
<p class='rw-defn idx10' style='display:none'>If an idea, plan, or attitude is inchoate, it is vague and imperfectly formed because it is just starting to develop.</p>
<p class='rw-defn idx11' style='display:none'>Something that is inconspicuous does not attract attention and is not easily seen or noticed because it is small or ordinary.</p>
<p class='rw-defn idx12' style='display:none'>Something that has inestimable value or benefit has so much of it that it cannot be calculated.</p>
<p class='rw-defn idx13' style='display:none'>Something insipid is dull, boring, and has no interesting features; for example, insipid food has no taste or little flavor.</p>
<p class='rw-defn idx14' style='display:none'>Something mediocre is average or ordinary in quality; it’s just OK.</p>
<p class='rw-defn idx15' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx16' style='display:none'>Something nonpareil has no equal because it is much better than all others of its kind or type.</p>
<p class='rw-defn idx17' style='display:none'>A paradigm is a model or example of something that shows how it works or how it can be produced; a paradigm shift is a completely new way of thinking about something.</p>
<p class='rw-defn idx18' style='display:none'>A paragon is an example of a thing or person at its or their very best.</p>
<p class='rw-defn idx19' style='display:none'>A platitude is an unoriginal statement that has been used so many times that it is considered almost meaningless and pointless, even though it is presented as important.</p>
<p class='rw-defn idx20' style='display:none'>Something that is prodigious is very large, impressive, or great.</p>
<p class='rw-defn idx21' style='display:none'>When someone exhibits profundity, they display great intellectual depth and understanding; profundity can also be the depth or complexity of something.</p>
<p class='rw-defn idx22' style='display:none'>Something prosaic is dull, boring, and ordinary.</p>
<p class='rw-defn idx23' style='display:none'>A protuberance is an outgrowth, lump, or swelling of some kind.</p>
<p class='rw-defn idx24' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>
<p class='rw-defn idx25' style='display:none'>Recondite areas of knowledge are those that are very difficult to understand and/or are not known by many people.</p>
<p class='rw-defn idx26' style='display:none'>The salient qualities of an issue or feature are those that are most important and noticeable.</p>
<p class='rw-defn idx27' style='display:none'>If you refer to something as a trifle, you mean that it is of little or no importance or value.</p>
<p class='rw-defn idx28' style='display:none'>A trite remark or idea is so overused that it is no longer interesting or novel.</p>
<p class='rw-defn idx29' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>
<p class='rw-defn idx30' style='display:none'>Something vapid is dull, boring, and/or tiresome.</p>
<p class='rw-defn idx31' style='display:none'>A virtuoso is someone who is very skillful at something, especially playing a musical instrument.</p>
<p class='rw-defn idx32' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>nondescript</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='nondescript#' id='pronounce-sound' path='audio/words/amy-nondescript'></a>
non-di-SKRIPT
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='nondescript#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='nondescript#' id='context-sound' path='audio/wordcontexts/brian-nondescript'></a>
My sister&#8217;s flashy Porsche was recently stolen, so she bought a <em>nondescript</em> or plain used car.  She said she was going for something <em>nondescript</em> or ordinary so that thieves wouldn&#8217;t even notice it, let alone steal it.  My dramatic sister usually hates to spend money on anything <em>nondescript</em>, common, or undistinguished, but rather prefers the unusual and exotic.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What will most likely happen when something is <em>nondescript</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
No one will be able to find the words to describe its stunning qualities.
</li>
<li class='choice answer '>
<span class='result'></span>
No one will remember it because it does not stand out.
</li>
<li class='choice '>
<span class='result'></span>
Someone will steal it because it is worth a lot of money.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='nondescript#' id='definition-sound' path='audio/wordmeanings/amy-nondescript'></a>
Something is <em>nondescript</em> when its appearance is ordinary, dull, and not at all interesting or attractive.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>ordinary</em>
</span>
</span>
</div>
<a class='quick-help' href='nondescript#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='nondescript#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/nondescript/memory_hooks/3571.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>No</span></span> <span class="emp3"><span>Descript</span></span>ion</span></span> That new house you just bought is so <span class="emp1"><span>no</span></span>n<span class="emp3"><span>descript</span></span> that it's <span class="emp1"><span>no</span></span>t even worth giving a <span class="emp3"><span>descript</span></span>ion of it to your friends because there is nothing interesting to say about any of its features.
</p>
</div>

<div id='memhook-button-bar'>
<a href="nondescript#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/nondescript/memory_hooks">Use other hook</a>
<a href="nondescript#" id="memhook-use-own" url="https://membean.com/mywords/nondescript/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='nondescript#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Banging on what looks like a garage door produces reluctant admission that this is the foreign ministry, and eventually access to a dusty car park that leads on to a <b>nondescript</b> villa.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
The New American Home is on the edge of this historic district, across the street from two <b>nondescript</b>, 25-year-old buildings, one a seven-story condo and the other an eight-story office block.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/nondescript/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='nondescript#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='non_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='nondescript#'>
<span class=''></span>
non-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='de_off' data-tree-url='//cdn1.membean.com/public/data/treexml' href='nondescript#'>
<span class='common'></span>
de-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>about, concerning</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='script_written' data-tree-url='//cdn1.membean.com/public/data/treexml' href='nondescript#'>
<span class='common'></span>
script
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>written</td>
</tr>
</table>
<p>Something <em>nondescript</em> does &#8220;not&#8221; encourage &#8220;being written about&#8221; because there is nothing interesting about it.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='nondescript#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Nondescript" src="https://cdn0.membean.com/public/images/wordimages/cons2/nondescript.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='nondescript#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>amorphous</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>ennui</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>hackneyed</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>inchoate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>inconspicuous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>insipid</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>mediocre</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>platitude</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>prosaic</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>trifle</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>trite</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>vapid</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>apotheosis</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bravura</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>epitome</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>esoteric</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>flagrant</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>genre</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>inestimable</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>nonpareil</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>paradigm</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>paragon</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>prodigious</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>profundity</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>protuberance</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>recondite</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>salient</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>virtuoso</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="nondescript" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>nondescript</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="nondescript#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

