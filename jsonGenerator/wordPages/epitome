
<!DOCTYPE html>
<html>
<head>
<title>Word: epitome | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>The acme of something is its highest point of achievement or excellence.</p>
<p class='rw-defn idx1' style='display:none'>The apogee of something is its highest or greatest point, especially in reference to a culture or career.</p>
<p class='rw-defn idx2' style='display:none'>The best or perfect example of something is its apotheosis; likewise, this word can be used to indicate the best or highest point in someone&#8217;s life or job.</p>
<p class='rw-defn idx3' style='display:none'>An archetype is a perfect or typical example of something because it has the most important qualities that belong to that type of thing; it can also describe essential qualities common to a particular class of things.</p>
<p class='rw-defn idx4' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx5' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx6' style='display:none'>A cynosure is an object that serves as the center of attention.</p>
<p class='rw-defn idx7' style='display:none'>Debility is a state of being physically or mentally weak, usually due to illness.</p>
<p class='rw-defn idx8' style='display:none'>A definitive opinion on an issue cannot be challenged; therefore, it is the final word or the most authoritative pronouncement on that issue.</p>
<p class='rw-defn idx9' style='display:none'>A dilettante is someone who frequently pursues an interest in a subject; nevertheless, they never spend the time and effort to study it thoroughly enough to master it.</p>
<p class='rw-defn idx10' style='display:none'>If you describe a person, group, or civilization as effete, you mean it is weak, exhausted,  powerless, unproductive, and/or corrupt.</p>
<p class='rw-defn idx11' style='display:none'>Efficacy is the ability or power to produce an expected effect or result.</p>
<p class='rw-defn idx12' style='display:none'>An embodiment of something, such as a quality or idea, is a visible representation or concrete expression of it.</p>
<p class='rw-defn idx13' style='display:none'>If you emulate someone, you try to behave the same way they do because you admire them a great deal.</p>
<p class='rw-defn idx14' style='display:none'>An exemplary person is one that sets an ideal or praiseworthy example for others to follow.</p>
<p class='rw-defn idx15' style='display:none'>One thing that exemplifies another serves as an example of it, illustrates it, or demonstrates it.</p>
<p class='rw-defn idx16' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx17' style='display:none'>A fledgling business endeavor is just beginning or developing.</p>
<p class='rw-defn idx18' style='display:none'>The nadir of a situation is its lowest point.</p>
<p class='rw-defn idx19' style='display:none'>A neophyte is a person who is just beginning to learn a subject or skill—or how to do an activity of some kind.</p>
<p class='rw-defn idx20' style='display:none'>Something nonpareil has no equal because it is much better than all others of its kind or type.</p>
<p class='rw-defn idx21' style='display:none'>If you are a novice at an activity, you have just begun or started doing it.</p>
<p class='rw-defn idx22' style='display:none'>If something is obsolescent, it is slowly becoming no longer needed because something newer or more effective has been invented.</p>
<p class='rw-defn idx23' style='display:none'>A paradigm is a model or example of something that shows how it works or how it can be produced; a paradigm shift is a completely new way of thinking about something.</p>
<p class='rw-defn idx24' style='display:none'>A paragon is an example of a thing or person at its or their very best.</p>
<p class='rw-defn idx25' style='display:none'>When you personify something, such as a quality, you are the perfect example of it.</p>
<p class='rw-defn idx26' style='display:none'>If someone reaches a pinnacle of something—such as a career or mountain—they have arrived at the highest point of it.</p>
<p class='rw-defn idx27' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>
<p class='rw-defn idx28' style='display:none'>A superlative deed or act is excellent, outstanding, or the best of its kind.</p>
<p class='rw-defn idx29' style='display:none'>A tyro has just begun learning something.</p>
<p class='rw-defn idx30' style='display:none'>Something vintage is the best of its kind and of excellent quality; it is often of a past time and is considered a classic example of its type.</p>
<p class='rw-defn idx31' style='display:none'>The zenith of something is its highest, most powerful, or most successful point.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>epitome</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='epitome#' id='pronounce-sound' path='audio/words/amy-epitome'></a>
i-PIT-uh-mee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='epitome#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='epitome#' id='context-sound' path='audio/wordcontexts/brian-epitome'></a>
Theo is the <em>epitome</em> or perfect example of kindness; everyone adores him for the thoughtful and generous way he treats his friends.  Nobody can quite believe how nice he is, especially considering that his father Tad is the <em>epitome</em> or best example of a spoiled, rich person.  Tad seems to believe that he is the <em>epitome</em> of or last word in being &#8220;cool,&#8221; but he&#8217;s really just a thoughtless jerk.  It&#8217;s good that Theo is redefining the family name as the <em>epitome</em> or essence of goodness.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is the <em>epitome</em> of something?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
It is the best illustration of it.
</li>
<li class='choice '>
<span class='result'></span>
It is the easiest explanation of it.
</li>
<li class='choice '>
<span class='result'></span>
It is the simplest possible form of it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='epitome#' id='definition-sound' path='audio/wordmeanings/amy-epitome'></a>
If you say that a person or thing is the <em>epitome</em> of something, you mean that they or it is the best possible example of that thing.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>perfect example</em>
</span>
</span>
</div>
<a class='quick-help' href='epitome#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='epitome#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/epitome/memory_hooks/3673.json'></span>
<p>
<img alt="Epitome" src="https://cdn3.membean.com/public/images/wordimages/hook/epitome.jpg?qdep8" />
<span class="emp0"><span>A <span class="emp2"><span>Pie</span></span> <span class="emp3"><span>To</span></span> <span class="emp1"><span>Me</span></span></span></span> When she gave that sweet <span class="emp2"><span>pie</span></span> <span class="emp3"><span>to</span></span> <span class="emp1"><span>me</span></span> it was the <span class="emp2"><span>epi</span></span><span class="emp3"><span>to</span></span><span class="emp1"><span>me</span></span> of joy.
</p>
</div>

<div id='memhook-button-bar'>
<a href="epitome#" id="add-public-hook" style="" url="https://membean.com/mywords/epitome/memory_hooks">Use other public hook</a>
<a href="epitome#" id="memhook-use-own" url="https://membean.com/mywords/epitome/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='epitome#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
"Ann Richards was the <b>epitome</b> of Texas politics: a figure larger than life who had a gift for captivating the public with her great wit," said Gov. Rick Perry in a statement.
<cite class='attribution'>
&mdash;
The Texas Tribune
</cite>
</li>
<li>
Chadwick Boseman was remembered as a hometown hero who brought a sense of pride to his native Anderson, South Carolina. . . . “He is the <b>epitome</b> of black excellence,” said Deanna Brown-Thomas, the daughter of legendary singer James Brown and president of her father’s family foundation.
<cite class='attribution'>
&mdash;
The Augusta Chronicle
</cite>
</li>
<li>
Kim Royar, a wildlife biologist with the Vermont Fish and Wildlife Department, said King has been a great conservation partner for her organization. “He works to bring groups together under the broad umbrella of conservation,” she said. “He is the <b>epitome</b> of a great partner—he works hard, collaborates, is humble, and is a pleasure to work with.
<cite class='attribution'>
&mdash;
Addison County Independent
</cite>
</li>
<li>
"He spread his grace on everything and every one he came in contact with," former Brave Chipper Jones said. Noting the challenges [Hank] Aaron faced in his lifetime, Jones called him the "<b>epitome</b> of class and integrity."
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/epitome/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='epitome#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>epi-</td>
<td>
&rarr;
</td>
<td class='meaning'>near, short</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='tom_cut' data-tree-url='//cdn1.membean.com/public/data/treexml' href='epitome#'>
<span class=''></span>
tom
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>cut, cutting, slice</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='epitome#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>An <em>epitome</em> is a representative or characteristic &#8220;cutting or slice near&#8221; the essence or best example of a group of people.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='epitome#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Parks and Recreation</strong><span> The epitome of a desk for the head of a department.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/epitome.jpg' video_url='examplevids/epitome' video_width='350'></span>
<div id='wt-container'>
<img alt="Epitome" height="288" src="https://cdn1.membean.com/video/examplevids/epitome.jpg" width="350" />
<div class='center'>
<a href="epitome#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='epitome#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Epitome" src="https://cdn3.membean.com/public/images/wordimages/cons2/epitome.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='epitome#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>acme</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>apogee</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>apotheosis</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>archetype</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>cynosure</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>definitive</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>efficacy</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>embodiment</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>emulate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>exemplary</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>exemplify</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>nonpareil</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>paradigm</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>paragon</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>personify</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>pinnacle</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>superlative</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>vintage</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>zenith</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='4' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>debility</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>dilettante</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>effete</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>fledgling</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>nadir</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>neophyte</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>novice</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>obsolescent</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>tyro</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='epitome#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
epitomize
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to be an excellent representative example of something</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="epitome" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>epitome</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="epitome#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

