
<!DOCTYPE html>
<html>
<head>
<title>Word: imprimatur | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you acquiesce to something, you allow it to happen by doing what someone wants without putting up a struggle or voicing your own concerns.</p>
<p class='rw-defn idx1' style='display:none'>Adulation is praise and admiration for someone that includes more than they deserve, usually for the purposes of flattery.</p>
<p class='rw-defn idx2' style='display:none'>Approbation is official praise or approval of something.</p>
<p class='rw-defn idx3' style='display:none'>A captious person has a fondness for catching others at fault; hence, they are overly critical and raise unwarranted objections too often.</p>
<p class='rw-defn idx4' style='display:none'>To carp at someone is to complain about them to their face or to nag and criticize them in a whining voice.</p>
<p class='rw-defn idx5' style='display:none'>To cavil is to make unnecessary complaints about things that are unimportant.</p>
<p class='rw-defn idx6' style='display:none'>Someone cedes land or power to someone else by giving it to them, often because of political or military pressure.</p>
<p class='rw-defn idx7' style='display:none'>Censure is written or verbal disapproval and harsh criticism of something that someone has done.</p>
<p class='rw-defn idx8' style='display:none'>If someone will countenance something, they will approve, tolerate, or support it.</p>
<p class='rw-defn idx9' style='display:none'>If you behave with deference towards someone, you show them respect and accept their opinions or decisions, especially because they have an important position.</p>
<p class='rw-defn idx10' style='display:none'>To defile something holy or important is to do something to it or say something about it that is highly offensive to others.</p>
<p class='rw-defn idx11' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx12' style='display:none'>If you denounce people or actions, you criticize them severely in public because you feel strongly that they are wrong or evil.</p>
<p class='rw-defn idx13' style='display:none'>If you disparage someone or something, you say unpleasant words that show you have no respect for that person or thing.</p>
<p class='rw-defn idx14' style='display:none'>Dissension is a disagreement or difference of opinion among a group of people that can cause conflict.</p>
<p class='rw-defn idx15' style='display:none'>If you receive a plaudit, you receive admiration, praise, and approval from someone.</p>
<p class='rw-defn idx16' style='display:none'>When someone proscribes an activity, they prohibit it by establishing a rule against it.</p>
<p class='rw-defn idx17' style='display:none'>The ratification of a measure or agreement is its official approval or confirmation by all involved.</p>
<p class='rw-defn idx18' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx19' style='display:none'>If you repudiate something, you state that you do not accept or agree with it and no longer want to be connected with it in any way.</p>
<p class='rw-defn idx20' style='display:none'>When you validate something, you confirm that it is sound, true, legal, or worthwhile.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>imprimatur</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='imprimatur#' id='pronounce-sound' path='audio/words/amy-imprimatur'></a>
im-pri-MAH-ter
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='imprimatur#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='imprimatur#' id='context-sound' path='audio/wordcontexts/brian-imprimatur'></a>
The organic food company is fortunate to have the <em>imprimatur</em> or approval of its country&#8217;s president.  The company&#8217;s solid environmental practices have earned the permission or official <em>imprimatur</em> of its government to do business nationwide.  Having the influential president&#8217;s <em>imprimatur</em> or approved support is a bonus because it boosts market share.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean to be granted an <em>imprimatur</em> by a government leader? 
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
They have given their formal acceptance for something.
</li>
<li class='choice '>
<span class='result'></span>
They have publicly criticized someone for their criminal behavior.
</li>
<li class='choice '>
<span class='result'></span>
They have told their supporters that they don&#8217;t plan to run again in an upcoming election.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='imprimatur#' id='definition-sound' path='audio/wordmeanings/amy-imprimatur'></a>
If something, such as a product or book, has an official&#8217;s <em>imprimatur</em>, that authority has given it their official approval.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>official acceptance</em>
</span>
</span>
</div>
<a class='quick-help' href='imprimatur#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='imprimatur#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/imprimatur/memory_hooks/2833.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Prim</span></span>e <span class="emp1"><span>Tur</span></span>n</span></span> Because the new television show has received the im<span class="emp3"><span>prim</span></span>a<span class="emp1"><span>tur</span></span> of the mighty moguls of media, it now has the chance to <span class="emp1"><span>tur</span></span>n to <span class="emp3"><span>prim</span></span>e time on the major networks.
</p>
</div>

<div id='memhook-button-bar'>
<a href="imprimatur#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/imprimatur/memory_hooks">Use other hook</a>
<a href="imprimatur#" id="memhook-use-own" url="https://membean.com/mywords/imprimatur/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='imprimatur#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Criticisms of science labs are not new, but teachers say the report, coming with the <b>imprimatur</b> of the National Research Council, could give the matter a boost of urgency.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
The Brazilian soccer star Ronaldinho, for instance, currently lends his <b>imprimatur</b> to everything from the Chinese computer brand Lenovo to a Chinese brand of ice cream.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Everyone will be waiting to see whether she gives the concept thumbs up or down, because you know what can happen to a book when it gets Oprah’s <b>imprimatur</b>.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Allen has remained a torchbearer for native Irish cooking, and her <b>imprimatur</b> has been seized on by a subsequent generation of family members.
<cite class='attribution'>
&mdash;
Epicurious
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/imprimatur/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='imprimatur#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep5" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='im_in' data-tree-url='//cdn1.membean.com/public/data/treexml' href='imprimatur#'>
<span class='common'></span>
im-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>in, on</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='prim_press' data-tree-url='//cdn1.membean.com/public/data/treexml' href='imprimatur#'>
<span class=''></span>
prim
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>press</td>
</tr>
</table>
<p>The suffix <em>-atur</em> is a way of giving a command, so the word <em>imprimatur</em> formally means &#8220;let it be pressed on,&#8221; hence &#8220;let it be printed.&#8221;  An <em>imprimatur</em> therefore is an official approval to &#8220;print&#8221; something, and has become extended in meaning to an official approval for many different products.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='imprimatur#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep5" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Imprimatur" src="https://cdn0.membean.com/public/images/wordimages/cons2/imprimatur.png?qdep5" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='imprimatur#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>acquiesce</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>adulation</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>approbation</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>cede</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>countenance</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>deference</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>plaudit</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>ratification</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>validate</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>captious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>carp</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>cavil</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>censure</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>defile</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>denounce</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>disparage</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>dissension</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>proscribe</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>repudiate</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="imprimatur" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>imprimatur</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="imprimatur#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

