
<!DOCTYPE html>
<html>
<head>
<title>Word: admonish | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you adjure someone to do something, you persuade, eagerly appeal, or solemnly order them to do it.</p>
<p class='rw-defn idx1' style='display:none'>If something is done at someone&#8217;s behest, it is done because they urgently ask for it or authoritatively order it to happen.</p>
<p class='rw-defn idx2' style='display:none'>Blandishments are words or actions that are pleasant and complimentary, intended to persuade someone to do something via a use of flattery.</p>
<p class='rw-defn idx3' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx4' style='display:none'>You cajole people by gradually persuading them to do something, usually by being very nice to them.</p>
<p class='rw-defn idx5' style='display:none'>If you chastise someone, you speak to them angrily or punish them for doing something wrong.</p>
<p class='rw-defn idx6' style='display:none'>If someone is granted clemency, they are punished less severely than they could have been—they have been shown mercy.</p>
<p class='rw-defn idx7' style='display:none'>If you demur, you delay in doing or mildly object to something because you don&#8217;t really want to do it.</p>
<p class='rw-defn idx8' style='display:none'>If you eschew something, you deliberately avoid doing it, especially for moral reasons.</p>
<p class='rw-defn idx9' style='display:none'>If you exhort someone to do something, you try very hard to persuade them to do it.</p>
<p class='rw-defn idx10' style='display:none'>If you expostulate with someone, you express strong disagreement with or disapproval of what that person is doing.</p>
<p class='rw-defn idx11' style='display:none'>A harangue is a long, scolding speech.</p>
<p class='rw-defn idx12' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx13' style='display:none'>An injunction is a court order that prevents someone from doing something.</p>
<p class='rw-defn idx14' style='display:none'>Insouciance is a lack of concern or worry for something that should be shown more careful attention or consideration.</p>
<p class='rw-defn idx15' style='display:none'>If you do something in a lackadaisical way, you do it carelessly and without putting much effort into it—thereby showing that you are not really interested in what you&#8217;re doing.</p>
<p class='rw-defn idx16' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx17' style='display:none'>If you refrain from doing something, you do not do it—even though you want to.</p>
<p class='rw-defn idx18' style='display:none'>To remonstrate with someone is to tell that person that you strongly disapprove of something they have said or done.</p>
<p class='rw-defn idx19' style='display:none'>When you are given a reprimand, you are scolded, blamed, or given a talking-to by someone for something wrong that you did.</p>
<p class='rw-defn idx20' style='display:none'>An ultimatum is a final statement or offer on an issue; for example, the phrase &#8220;Do this, or you can forget it!&#8221; is an example of an ultimatum.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>admonish</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='admonish#' id='pronounce-sound' path='audio/words/amy-admonish'></a>
ad-MON-ish
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='admonish#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='admonish#' id='context-sound' path='audio/wordcontexts/brian-admonish'></a>
John&#8217;s mother <em>admonished</em> him for spending too much money on her birthday present.  John in turn <em>admonished</em> and advised her, saying that she should thank him and not complain!  Mothers often <em>admonish</em> or caution their children when they disapprove of their behavior.  Such <em>admonitions</em> or warnings seem an unavoidable part of parenting.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean to <em>admonish</em> someone?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
You voice a warning or caution.
</li>
<li class='choice '>
<span class='result'></span>
You welcome them to a new group.
</li>
<li class='choice '>
<span class='result'></span>
You praise them for their actions.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='admonish#' id='definition-sound' path='audio/wordmeanings/amy-admonish'></a>
When you <em>admonish</em> someone, you tell them gently but with seriousness that they have done something wrong; you usually caution and advise them not to do it again.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>warn</em>
</span>
</span>
</div>
<a class='quick-help' href='admonish#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='admonish#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/admonish/memory_hooks/3068.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Shed</span></span> <span class="emp2"><span>Mon</span></span>itor</span></span> Jack ad<span class="emp2"><span>mon</span></span>i<span class="emp3"><span>shed</span></span> Philbert to watch out for the giant man-eating <span class="emp2"><span>mon</span></span>itor lizard living in the <span class="emp3"><span>shed</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="admonish#" id="add-public-hook" style="" url="https://membean.com/mywords/admonish/memory_hooks">Use other public hook</a>
<a href="admonish#" id="memhook-use-own" url="https://membean.com/mywords/admonish/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='admonish#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
<b>Admonish</b> thy friends in secret, praise them openly.
<span class='attribution'>&mdash; Publilius Syrus, a Latin writer as quoted in _The Moral Sayings of Publius (sic) Syrus, a Roman Slave_</span>
<img alt="Publilius syrus, a latin writer as quoted in _the moral sayings of publius (sic) syrus, a roman slave_" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Publilius Syrus, a Latin writer as quoted in _The Moral Sayings of Publius (sic) Syrus, a Roman Slave_.jpg?qdep8" width="80" />
</li>
<li>
My conservative parents were quick to <b>admonish</b> me to stay out of trouble, and not get in the way. Thankfully, that era produced a person like John Lewis, who actually sought out "good trouble" and would, many times, deliberately get in the way—of injustice, inequality, and discrimination.
<cite class='attribution'>
&mdash;
Northern Public Radio
</cite>
</li>
<li>
Bell said in a statement: "We are pleased that the bi-partisan Ethics Committee acted to <b>admonish</b> Mr. DeLay for his unethical conduct in response to the complaint I filed in June."
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
His final work was an autobiographical one-man show, “Save It for the Stage: The Life of Reilly,” about his family life growing up in the Bronx. The title grew out of the fact that when he would act out as a child, his mother would often <b>admonish</b> him to "save it for the stage."
<cite class='attribution'>
&mdash;
Variety
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/admonish/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='admonish#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ad_towards' data-tree-url='//cdn1.membean.com/public/data/treexml' href='admonish#'>
<span class='common'></span>
ad-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to, towards</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='mon_warn' data-tree-url='//cdn1.membean.com/public/data/treexml' href='admonish#'>
<span class=''></span>
mon
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>warn, advise</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ish_do' data-tree-url='//cdn1.membean.com/public/data/treexml' href='admonish#'>
<span class=''></span>
-ish
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to do something</td>
</tr>
</table>
<p>To <em>admonish</em> is to give &#8220;warning or advice to&#8221; a person.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='admonish#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Are You Being Served</strong><span> Miss Brown is admonished.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/admonish.jpg' video_url='examplevids/admonish' video_width='350'></span>
<div id='wt-container'>
<img alt="Admonish" height="288" src="https://cdn1.membean.com/video/examplevids/admonish.jpg" width="350" />
<div class='center'>
<a href="admonish#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='admonish#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Admonish" src="https://cdn1.membean.com/public/images/wordimages/cons2/admonish.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='admonish#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adjure</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>behest</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>blandishment</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>cajole</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>chastise</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>exhort</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>expostulate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>harangue</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>injunction</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>remonstrate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>reprimand</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>ultimatum</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>clemency</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>demur</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>eschew</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>insouciance</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>lackadaisical</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>refrain</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='admonish#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
admonition
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>a mild warning or caution</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="admonish" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>admonish</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="admonish#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

