
<!DOCTYPE html>
<html>
<head>
<title>Word: supplant | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If someone abdicates, they give up their responsibility for something, such as a king&#8217;s transfer of power when he gives up his throne.</p>
<p class='rw-defn idx1' style='display:none'>If you appropriate something that does not belong to you, you take it for yourself without the right to do so.</p>
<p class='rw-defn idx2' style='display:none'>When you arrogate something, such as a position or privilege, you take it even though you don&#8217;t have the legal right to it.</p>
<p class='rw-defn idx3' style='display:none'>A bastion is considered an important and effective defense of a way of life or principle; a bastion can also refer to a heavily fortified place, such as a castle.</p>
<p class='rw-defn idx4' style='display:none'>Something ephemeral, such as some insects or a sunset, lasts for only a short time or has a very short lifespan.</p>
<p class='rw-defn idx5' style='display:none'>Something that is evanescent lasts for only a short time before disappearing from sight or memory.</p>
<p class='rw-defn idx6' style='display:none'>If you expropriate something, you take it away for your own use although it does not belong to you; governments frequently expropriate private land to use for public purposes.</p>
<p class='rw-defn idx7' style='display:none'>If something is formidable, it is impressive in size, power, or skill; therefore, it can make you feel frightened, alarmed, or in awe because it is so powerful or difficult to deal with.</p>
<p class='rw-defn idx8' style='display:none'>Two items are fungible if one can be exchanged for the other with no loss in inherent value; for example, one $10 bill and two $5 bills are fungible.</p>
<p class='rw-defn idx9' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx10' style='display:none'>If someone is implacable, they very stubbornly react to situations or the opinions of others because of strong feelings that make them unwilling to change their mind.</p>
<p class='rw-defn idx11' style='display:none'>If someone leaves an indelible impression on you, it will not be forgotten; an indelible mark is permanent or impossible to erase or remove.</p>
<p class='rw-defn idx12' style='display:none'>The adjective inexorable describes a process that is impossible to stop once it has begun.</p>
<p class='rw-defn idx13' style='display:none'>Something that is interminable continues for a very long time in a boring or annoying way.</p>
<p class='rw-defn idx14' style='display:none'>An inveterate person is always doing a particular thing, especially something questionable—and they are not likely to stop doing it.</p>
<p class='rw-defn idx15' style='display:none'>An irrevocable action or decision is impossible to change, reverse, or stop.</p>
<p class='rw-defn idx16' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx17' style='display:none'>Something that is perennial lasts a very long time and is enduring.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is pertinacious is determined to continue doing something rather than giving up—even when it gets very difficult.</p>
<p class='rw-defn idx19' style='display:none'>Something or someone that is protean is adaptable, variable, or changeable in nature.</p>
<p class='rw-defn idx20' style='display:none'>Sedition is the act of encouraging people to disobey and oppose the government currently in power.</p>
<p class='rw-defn idx21' style='display:none'>Someone who is sedulous works hard and performs tasks very carefully and thoroughly, not stopping their work until it is completely accomplished.</p>
<p class='rw-defn idx22' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx23' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx24' style='display:none'>If someone subjugates a group of people or country, they conquer and bring it under control by force.</p>
<p class='rw-defn idx25' style='display:none'>When something supersedes another thing, it takes the place of or succeeds it.</p>
<p class='rw-defn idx26' style='display:none'>Something that is temporal deals with the present and somewhat brief time of this world.</p>
<p class='rw-defn idx27' style='display:none'>Something that has the quality of transience lasts for only a short time or is constantly changing.</p>
<p class='rw-defn idx28' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx29' style='display:none'>If you are unflagging while doing a task, you are untiring when working upon it and do not stop until it is finished.</p>
<p class='rw-defn idx30' style='display:none'>When you usurp someone else&#8217;s power, position, or role, you take it from them although you do not have the right to do so.</p>
<p class='rw-defn idx31' style='display:none'>When someone vacillates, they go back and forth about a choice or opinion, unable to make a firm decision.</p>
<p class='rw-defn idx32' style='display:none'>Something that is volatile can change easily and vary widely.</p>
<p class='rw-defn idx33' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>
<p class='rw-defn idx34' style='display:none'>If you waver, you cannot decide between two things because you have serious doubts about which choice is better.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 4
<br>
<small>
Middle/High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>supplant</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='supplant#' id='pronounce-sound' path='audio/words/amy-supplant'></a>
suh-PLANT
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='supplant#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='supplant#' id='context-sound' path='audio/wordcontexts/brian-supplant'></a>
The secret council met to <em>supplant</em> or replace the reigning king with a member of their own choosing.  The king was a weak and unobservant ruler who did not realize that there was a movement to remove him from power and <em>supplant</em> him with a better monarch.  Such a transfer of power would ensure the council&#8217;s good fortune, but if the plot failed to <em>supplant</em> or overthrow the king, its members would be executed.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How can a new idea <em>supplant</em> another idea?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
People like both ideas a lot, so they combine the two ideas into one.
</li>
<li class='choice '>
<span class='result'></span>
People like one part of the new idea, so they add it to the old idea.
</li>
<li class='choice answer '>
<span class='result'></span>
People like the new idea better, so they accept it and reject the old one.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='supplant#' id='definition-sound' path='audio/wordmeanings/amy-supplant'></a>
To <em>supplant</em> someone is to replace them with someone else, usually because the latter is more powerful or better in some way.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>replace</em>
</span>
</span>
</div>
<a class='quick-help' href='supplant#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='supplant#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/supplant/memory_hooks/4286.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Plant</span></span>s for <span class="emp1"><span>Sup</span></span>per</span></span> Since I've become a vegetarian, <span class="emp2"><span>plant</span></span>s now <span class="emp1"><span>sup</span></span><span class="emp2"><span>plant</span></span> meat for my <span class="emp1"><span>sup</span></span>per.
</p>
</div>

<div id='memhook-button-bar'>
<a href="supplant#" id="add-public-hook" style="" url="https://membean.com/mywords/supplant/memory_hooks">Use other public hook</a>
<a href="supplant#" id="memhook-use-own" url="https://membean.com/mywords/supplant/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='supplant#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
California is well on the way to being a majority-minority state, and within the array of minorities, Latinos are poised to <b>supplant</b> African Americans as the single largest voter bloc.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
As digital books <b>supplant</b> physical ones, they argue, fusty old hierarchies like the Dewey Decimal System will give way to the liberating pixie dust of Google searches.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
While parallel bars will never <b>supplant</b> pigskin in the heart of Oklahomans, gymnastics has won over one of the biggest football names in the state. "The upper-body strength in gymnastics is really superior to that in any other sport," says former Sooners football coach Barry Switzer.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Enthusiasts confidently predicted that solar cells would soon <b>supplant</b> grimy old power plants, and spare the world the tiresome chore of digging for coal, uranium and natural gas.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/supplant/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='supplant#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='sup_under' data-tree-url='//cdn1.membean.com/public/data/treexml' href='supplant#'>
<span class=''></span>
sup-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>under</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='plant_plant' data-tree-url='//cdn1.membean.com/public/data/treexml' href='supplant#'>
<span class=''></span>
plant
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>plant, sprout, sole of the foot</td>
</tr>
</table>
<p>When something is <em>supplanted</em> by something else, it &#8220;grows under&#8221; that thing and subsequently uproots it from its current position.  Note that &#8220;plants&#8221; are &#8220;planted&#8221; by the &#8220;soles of the feet&#8221; as one sows seed.  <em>Supplant</em> can also be thought of as one person &#8220;tripping&#8221; up another with their feet in order to <em>supplant</em> him or her.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='supplant#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Hitchhiker's Guide to the Galaxy</strong><span> The Hitchhiker's Guide has supplanted the Encyclopedia Galactica.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/supplant.jpg' video_url='examplevids/supplant' video_width='350'></span>
<div id='wt-container'>
<img alt="Supplant" height="288" src="https://cdn1.membean.com/video/examplevids/supplant.jpg" width="350" />
<div class='center'>
<a href="supplant#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='supplant#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Supplant" src="https://cdn2.membean.com/public/images/wordimages/cons2/supplant.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='supplant#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abdicate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>appropriate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>arrogate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>ephemeral</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>evanescent</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>expropriate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>fungible</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>protean</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>sedition</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>subjugate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>supersede</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>temporal</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>transience</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>usurp</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>vacillate</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>volatile</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>waver</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>bastion</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>formidable</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>implacable</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>indelible</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>inexorable</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>interminable</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>inveterate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>irrevocable</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>perennial</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>pertinacious</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>sedulous</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>unflagging</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="supplant" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>supplant</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="supplant#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

