
<!DOCTYPE html>
<html>
<head>
<title>Word: norm | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx1' style='display:none'>An apostate has abandoned their religious faith, political party, or devotional cause.</p>
<p class='rw-defn idx2' style='display:none'>If something goes awry, it does not happen in the way that was planned.</p>
<p class='rw-defn idx3' style='display:none'>An axiom is a wise saying or widely recognized general truth that is often self-evident; it can also be an established rule or law.</p>
<p class='rw-defn idx4' style='display:none'>A bohemian is someone who lives outside mainstream society, often embracing an unconventional lifestyle of self-expression, creativity, and rebellion against conventional rules and ways of society.</p>
<p class='rw-defn idx5' style='display:none'>A conventional way of thinking or behaving is the one most commonly accepted by social groups.</p>
<p class='rw-defn idx6' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx7' style='display:none'>A dissident is someone who disagrees publicly with a government, especially in a country where this is not allowed.</p>
<p class='rw-defn idx8' style='display:none'>Divergent opinions differ from each other; divergent paths move apart from one another.</p>
<p class='rw-defn idx9' style='display:none'>If someone is eccentric, they behave in a strange and unusual way that is different from most people.</p>
<p class='rw-defn idx10' style='display:none'>Errant things or people behave in a way that is unacceptable or wrong, such as a missile that travels in the wrong direction or a student who does not follow classroom rules.</p>
<p class='rw-defn idx11' style='display:none'>A heretic is someone who doubts or acts in opposition to commonly or generally accepted beliefs.</p>
<p class='rw-defn idx12' style='display:none'>Heterodox beliefs, ideas, or practices are different from accepted or official ones.</p>
<p class='rw-defn idx13' style='display:none'>An iconoclast is someone who often attacks beliefs, ideas, or customs that are generally accepted by society.</p>
<p class='rw-defn idx14' style='display:none'>A maverick individual does not follow general opinions of society as a whole; rather, they think for themselves and often strike out on their own.</p>
<p class='rw-defn idx15' style='display:none'>A maxim is a recognized rule of conduct or a general statement of a truth or principle.</p>
<p class='rw-defn idx16' style='display:none'>A nonconformist is unwilling to believe in the same things other people do or act in a fashion that society sets as a standard.</p>
<p class='rw-defn idx17' style='display:none'>To be politic in a decision is to be socially wise and diplomatic.</p>
<p class='rw-defn idx18' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx19' style='display:none'>A radical solution to a problem goes to the heart or root of it; a radical solution is most often an extreme solution to the problem.</p>
<p class='rw-defn idx20' style='display:none'>You show rectitude if you behave or conduct yourself in an honest and morally correct manner.</p>
<p class='rw-defn idx21' style='display:none'>If you are staid, you are set in your ways; consequently, you are settled, serious, law-abiding, conservative, and traditional—and perhaps even a tad dull.</p>
<p class='rw-defn idx22' style='display:none'>Something statutory, such as the power given to a governor or president, is created, established, and controlled by rules and laws; hence, it has the full force of the law behind it and must be followed.</p>
<p class='rw-defn idx23' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx24' style='display:none'>A tenet is a belief held by a group, organization, or person.</p>
<p class='rw-defn idx25' style='display:none'>A touchstone is the standard or best example by which the value or quality of something else is judged.</p>
<p class='rw-defn idx26' style='display:none'>An unorthodox opinion is unusual, not customary, and goes against established ways of thinking.</p>
<p class='rw-defn idx27' style='display:none'>If you act in a wayward fashion, you are very headstrong, independent, disobedient, unpredictable, and practically unmanageable.</p>
<p class='rw-defn idx28' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>norm</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='norm#' id='pronounce-sound' path='audio/words/amy-norm'></a>
nawrm
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='norm#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='norm#' id='context-sound' path='audio/wordcontexts/brian-norm'></a>
We all must follow societal <em>norms</em> or rules so that we can live together in peace and harmony.  People who neglect to follow the <em>norms</em> or standards of conduct often either choose to live apart from society as a whole, or they end up in jail.  I think that <em>norms</em> or patterns of behavior are good things, otherwise our society would be even more messed up than it already is.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>norm</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Staying up late one night just to see what it would be like to do so.
</li>
<li class='choice '>
<span class='result'></span>
Building a new school building every decade to take advantage of the newest materials.
</li>
<li class='choice answer '>
<span class='result'></span>
Facing forward while walking up a flight of stairs in public–not backward.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='norm#' id='definition-sound' path='audio/wordmeanings/amy-norm'></a>
A social <em>norm</em> is the standard, model, or rule by which people conduct themselves.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>standard</em>
</span>
</span>
</div>
<a class='quick-help' href='norm#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='norm#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/norm/memory_hooks/3671.json'></span>
<p>
<img alt="Norm" src="https://cdn1.membean.com/public/images/wordimages/hook/norm.jpg?qdep8" />
<span class="emp0"><span><span class="emp1"><span>Norm</span></span>, the <span class="emp1"><span>Norm</span></span></span></span> There have been a few American TV shows where the character played by men named "<span class="emp1"><span>Norm</span></span>" represent the uninteresting <span class="emp1"><span>norm</span></span> for U.S. society.
</p>
</div>

<div id='memhook-button-bar'>
<a href="norm#" id="add-public-hook" style="" url="https://membean.com/mywords/norm/memory_hooks">Use other public hook</a>
<a href="norm#" id="memhook-use-own" url="https://membean.com/mywords/norm/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='norm#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Observations about people’s behavior can be funny if they poke fun at a social <b>norm</b> that is being broken in a relatively inoffensive way—such as hoarding toilet paper or binge-watching Netflix.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
The second thing that happened at Enron is that it wasn’t clear what was the right social <b>norm</b> to apply to this particular emerging energy market.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
Every culture has social <b>norms</b>, which people follow largely because of the negative consequences of appearing different.
<cite class='attribution'>
&mdash;
Psychology Today
</cite>
</li>
<li>
This experiment illustrates an unfortunate fact: when a social <b>norm</b> collides with a market <b>norm</b>, the social <b>norm</b> goes away for a long time.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/norm/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='norm#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='norm_standard' data-tree-url='//cdn1.membean.com/public/data/treexml' href='norm#'>
<span class=''></span>
norm
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>carpenter's square, pattern, standard</td>
</tr>
</table>
<p>The <em>norm</em> is the established &#8220;pattern&#8221; or &#8220;standard&#8221; set by a society&#8217;s laws and customs.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='norm#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: The Young Turks</strong><span> How norms can differ from one country to the next.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/norm.jpg' video_url='examplevids/norm' video_width='350'></span>
<div id='wt-container'>
<img alt="Norm" height="288" src="https://cdn1.membean.com/video/examplevids/norm.jpg" width="350" />
<div class='center'>
<a href="norm#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='norm#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Norm" src="https://cdn0.membean.com/public/images/wordimages/cons2/norm.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='norm#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>axiom</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>conventional</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>maxim</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>politic</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>rectitude</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>staid</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>statutory</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>tenet</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>touchstone</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>apostate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>awry</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>bohemian</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>dissident</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>divergent</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>eccentric</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>errant</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>heretic</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>heterodox</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>iconoclast</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>maverick</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>nonconformist</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>radical</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>unorthodox</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>wayward</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="norm" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>norm</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="norm#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

