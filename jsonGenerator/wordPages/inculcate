
<!DOCTYPE html>
<html>
<head>
<title>Word: inculcate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abridge a book or play, you make it shorter by making cuts to the original.</p>
<p class='rw-defn idx1' style='display:none'>When you assimilate an idea, you completely understand it; likewise, when someone is assimilated into a new place, they become part of it by understanding what it is all about and by being accepted by others who live there.</p>
<p class='rw-defn idx2' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx3' style='display:none'>If you are bemused, you are puzzled and confused; hence, you are lost or absorbed in puzzling thought.</p>
<p class='rw-defn idx4' style='display:none'>You cajole people by gradually persuading them to do something, usually by being very nice to them.</p>
<p class='rw-defn idx5' style='display:none'>You coerce people when you force them to do something that they don&#8217;t want to do.</p>
<p class='rw-defn idx6' style='display:none'>If you curtail something, you reduce or limit it.</p>
<p class='rw-defn idx7' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx8' style='display:none'>If you divest someone of power, rights, or authority, you take those things away from them.</p>
<p class='rw-defn idx9' style='display:none'>Someone is being dogmatic when they express opinions in an assertive and often overly self-important manner.</p>
<p class='rw-defn idx10' style='display:none'>If you expostulate with someone, you express strong disagreement with or disapproval of what that person is doing.</p>
<p class='rw-defn idx11' style='display:none'>When you extricate yourself from a difficult or unpleasant situation, you manage to escape it.</p>
<p class='rw-defn idx12' style='display:none'>To imbue someone with a quality, such as an emotion or idea, is to fill that person completely with it.</p>
<p class='rw-defn idx13' style='display:none'>An immanent quality is present within something and is so much a part of it that the object cannot be imagined without that quality.</p>
<p class='rw-defn idx14' style='display:none'>If you indoctrinate someone, you teach them a set of beliefs so thoroughly that they reject any other beliefs or ideas without any critical thought.</p>
<p class='rw-defn idx15' style='display:none'>Something that has been ingrained in your mind has been fixed or rooted there permanently.</p>
<p class='rw-defn idx16' style='display:none'>An inherent characteristic is one that exists in a person at birth or in a thing naturally.</p>
<p class='rw-defn idx17' style='display:none'>An innate quality of someone is present since birth or is a quality of something that is essential to it.</p>
<p class='rw-defn idx18' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx19' style='display:none'>The art of pedagogy is the methods used by a teacher to teach a subject.</p>
<p class='rw-defn idx20' style='display:none'>When a substance permeates something, it enters into all parts of it.</p>
<p class='rw-defn idx21' style='display:none'>If something is pervasive, it appears to be everywhere.</p>
<p class='rw-defn idx22' style='display:none'>A person who proselytizes tries to draw others to adopt their religion, beliefs, or causes.</p>
<p class='rw-defn idx23' style='display:none'>If someone watches or listens to something with rapt attention, they are so involved with it that they do not notice anything else.</p>
<p class='rw-defn idx24' style='display:none'>To rarefy something is to make it less dense, as in oxygen at high altitudes; this word also refers to purifying or refining something, thereby making it less ordinary or commonplace.</p>
<p class='rw-defn idx25' style='display:none'>To remonstrate with someone is to tell that person that you strongly disapprove of something they have said or done.</p>
<p class='rw-defn idx26' style='display:none'>Stupor is a state in which someone&#8217;s mind and senses are dulled; consequently, they are unable to think clearly or act normally, usually due to the use of alcohol or drugs.</p>
<p class='rw-defn idx27' style='display:none'>If something, such as warmth, color, or liquid, suffuses a material, it gradually spreads through or covers it; if you are suffused with a feeling, you are full of that feeling.</p>
<p class='rw-defn idx28' style='display:none'>When something is transfused to another thing, it is given, put, or imparted to it; for example, you can transfuse blood or a love of reading from one person to another.</p>
<p class='rw-defn idx29' style='display:none'>Something that is vacuous is empty or blank, such as a mind or stare.</p>
<p class='rw-defn idx30' style='display:none'>To wallow in something is to immerse, roll, or absorb oneself lazily in it; wallow can also refer to being very involved in a negative feeling, such as wallowing in misery or self-pity.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>inculcate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='inculcate#' id='pronounce-sound' path='audio/words/amy-inculcate'></a>
in-KULH-kayt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='inculcate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='inculcate#' id='context-sound' path='audio/wordcontexts/brian-inculcate'></a>
Despite repeated attempts, my father has been unable to <em>inculcate</em> in or impart to us a love for art.  He has taken us on countless family trips to museums nationwide to <em>inculcate</em> a love for fine art, but so far he has not been been able to impress that appreciation upon us.  Perhaps we will develop a love for art some day.  At that point I&#8217;m sure my father will smile and think that all his attempts at <em><em>inculcating</em></em> or teaching this love for art were well worth it.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How might someone <em>inculcate</em> a sense of responsibility?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
By repeatedly sharing the importance of taking responsibility.
</li>
<li class='choice '>
<span class='result'></span>
By taking responsibility themselves and hoping others do too.
</li>
<li class='choice '>
<span class='result'></span>
By helping others avoid taking any responsibility.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='inculcate#' id='definition-sound' path='audio/wordmeanings/amy-inculcate'></a>
To <em>inculcate</em> is to fix an idea or belief in someone&#8217;s mind by repeatedly teaching it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>impress upon</em>
</span>
</span>
</div>
<a class='quick-help' href='inculcate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='inculcate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/inculcate/memory_hooks/6065.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Calculat</span></span>or <span class="emp3"><span>Calc</span></span>ulus</span></span> I learned <span class="emp3"><span>calc</span></span>ulus by using my <span class="emp3"><span>calculat</span></span>or; <span class="emp3"><span>calc</span></span>ulus was in<span class="emp3"><span>culcat</span></span>ed in me by my <span class="emp3"><span>calculat</span></span>or, which I used every single day.
</p>
</div>

<div id='memhook-button-bar'>
<a href="inculcate#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/inculcate/memory_hooks">Use other hook</a>
<a href="inculcate#" id="memhook-use-own" url="https://membean.com/mywords/inculcate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='inculcate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
A small but growing number of schools [have] begun to <b>inculcate</b> students in the fundamentals of democratic freedom by teaching and practicing the principles of the First Amendment.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Every country tries to <b>inculcate</b> its values to children, a spirit of patriotism, a concept of unity, and higher moral values.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/inculcate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='inculcate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='in_in' data-tree-url='//cdn1.membean.com/public/data/treexml' href='inculcate#'>
<span class='common'></span>
in-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>in, on</td>
</tr>
<tr>
<td class='partform'>culc</td>
<td>
&rarr;
</td>
<td class='meaning'>tread underfoot, trample</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ate_quality' data-tree-url='//cdn1.membean.com/public/data/treexml' href='inculcate#'>
<span class=''></span>
-ate
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make someone have a certain quality</td>
</tr>
</table>
<p>When you try to <em>inculcate</em> an idea in someone, you try to &#8220;force on&#8221; him that idea by &#8220;trampling&#8221; him &#8220;underfoot&#8221; repeatedly until he gives in.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='inculcate#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Commercial Jingles: REPETITION</strong><span> This advertisement certainly inculcates its message for its viewers.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/inculcate.jpg' video_url='examplevids/inculcate' video_width='350'></span>
<div id='wt-container'>
<img alt="Inculcate" height="288" src="https://cdn1.membean.com/video/examplevids/inculcate.jpg" width="350" />
<div class='center'>
<a href="inculcate#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='inculcate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Inculcate" src="https://cdn1.membean.com/public/images/wordimages/cons2/inculcate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='inculcate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>assimilate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>cajole</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>coerce</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>dogmatic</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>expostulate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>imbue</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>immanent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>indoctrinate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>ingrained</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>inherent</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>innate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>pedagogy</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>permeate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>pervasive</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>proselytize</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>rapt</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>remonstrate</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>suffuse</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>transfuse</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>wallow</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abridge</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bemused</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>curtail</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>divest</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>extricate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>rarefy</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>stupor</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>vacuous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="inculcate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>inculcate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="inculcate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

