
<!DOCTYPE html>
<html>
<head>
<title>Word: verbiage | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abridge a book or play, you make it shorter by making cuts to the original.</p>
<p class='rw-defn idx1' style='display:none'>Brevity is communicating by using just a few words or by taking very little time to do so.</p>
<p class='rw-defn idx2' style='display:none'>A circuitous route, journey, or piece of writing is long and complicated rather than simple and direct.</p>
<p class='rw-defn idx3' style='display:none'>Circumlocution is a way of saying or writing something that uses too many words, especially in order to avoid stating the true meaning clearly.</p>
<p class='rw-defn idx4' style='display:none'>A piece of writing is discursive if it includes a lot of information that is not relevant to the main subject.</p>
<p class='rw-defn idx5' style='display:none'>Someone who is effusive expresses happiness, pleasure, admiration, praise, etc., in an extremely enthusiastic way.</p>
<p class='rw-defn idx6' style='display:none'>A euphemism is a polite synonym or expression that people use when they want to avoid talking about—or indirectly replace—an unpleasant or embarrassing thing.</p>
<p class='rw-defn idx7' style='display:none'>To expatiate upon a subject is to speak or write in detail and at length about it.</p>
<p class='rw-defn idx8' style='display:none'>A garrulous person talks a lot, especially about unimportant things.</p>
<p class='rw-defn idx9' style='display:none'>Grandiloquent speech is highly formal, exaggerated, and often seems both silly and hollow because it is expressly used to appear impressive and important.</p>
<p class='rw-defn idx10' style='display:none'>If something is gratuitous, it is freely given; nevertheless, it is usually unnecessary and can be harmful or upsetting.</p>
<p class='rw-defn idx11' style='display:none'>When something is implicit, it is understood without having to say it.</p>
<p class='rw-defn idx12' style='display:none'>If an idea or thought is incisive, it is expressed in a penetrating and knowledgeable manner that is clear and brief; additionally, it can demonstrate impressive understanding of related ideas or thoughts.</p>
<p class='rw-defn idx13' style='display:none'>Jargon is language or terminology used by a specific group that might not be understandable to those people who are not of the group.</p>
<p class='rw-defn idx14' style='display:none'>A person who is being laconic uses very few words to say something.</p>
<p class='rw-defn idx15' style='display:none'>The lexicon of a particular subject or language is all the words, phrases, and terms associated with it.</p>
<p class='rw-defn idx16' style='display:none'>Someone who is loquacious talks too much.</p>
<p class='rw-defn idx17' style='display:none'>A peroration is a long speech that sounds impressive but does not have much substance.</p>
<p class='rw-defn idx18' style='display:none'>A pithy statement or piece of writing is brief but intelligent; it is also precise and to the point.</p>
<p class='rw-defn idx19' style='display:none'>Prattle is mindless chatter that seems like it will never stop.</p>
<p class='rw-defn idx20' style='display:none'>Something that is prolix, such as a lecture or speech, is excessively wordy; consequently, it can be tiresome to read or listen to.</p>
<p class='rw-defn idx21' style='display:none'>If something is redundant, it exceeds what is necessary or is needlessly wordy or repetitive.</p>
<p class='rw-defn idx22' style='display:none'>Rhetoric is the skill or art of using language to persuade or influence people, especially language that sounds impressive but may not be sincere or honest.</p>
<p class='rw-defn idx23' style='display:none'>If someone is sententious, they are terse or brief in writing and speech; they also often use proverbs to appear morally upright and wise.</p>
<p class='rw-defn idx24' style='display:none'>Something that is succinct is clearly and briefly explained without using any unnecessary words.</p>
<p class='rw-defn idx25' style='display:none'>Something that is superfluous is unnecessary; it is more than what is wanted or needed at the current time.</p>
<p class='rw-defn idx26' style='display:none'>A taciturn person is quiet or silent by nature.</p>
<p class='rw-defn idx27' style='display:none'>When you employ a tautology, you needlessly and often unintentionally repeat a similar sense of one word when using other words to describe it, such as in the redundant phrases &#8220;free gift&#8221; or &#8220;usual custom.&#8221;</p>
<p class='rw-defn idx28' style='display:none'>To be terse in speech is to be short and to the point, often in an abrupt manner that may seem unfriendly.</p>
<p class='rw-defn idx29' style='display:none'>If you truncate something, you make it shorter or quicker by removing or cutting off part of it.</p>
<p class='rw-defn idx30' style='display:none'>If someone is unlettered, they are illiterate, unable to read and write.</p>
<p class='rw-defn idx31' style='display:none'>Something that is verbose, such as a speech or article, contains too many words.</p>
<p class='rw-defn idx32' style='display:none'>A voluble person can speak easily with speech that naturally flows; voluble people also tend to be quite chatty.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>verbiage</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='verbiage#' id='pronounce-sound' path='audio/words/amy-verbiage'></a>
VUR-bee-ij
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='verbiage#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='verbiage#' id='context-sound' path='audio/wordcontexts/brian-verbiage'></a>
Wordy legal documents are seemingly too full of <em>verbiage</em>.  To understand the meaning of a contract, for instance, one has to wade through all sorts of roundabout <em>verbiage</em> which leaves one&#8217;s head spinning.  Instead of using such lengthy <em>verbiage</em>, it would be great if the lawyers who write these contracts could get right to the point.  With so much seemingly unnecessary <em>verbiage</em>, it&#8217;s hard to know what we are actually agreeing to with lawyers; we can, on the other hand, be delighted by a good writer&#8217;s expert use of proper style.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of <em>verbiage</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A preference for having face-to-face conversations instead of written or online discussions.
</li>
<li class='choice answer '>
<span class='result'></span>
Either the use of too many words in an essay or the way in which the writer uses those words.
</li>
<li class='choice '>
<span class='result'></span>
A fondness for learning the different meanings of words as they changed throughout history.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='verbiage#' id='definition-sound' path='audio/wordmeanings/amy-verbiage'></a>
<em>Verbiage</em> is an excessive use of words to convey something that could be expressed using fewer words; it can also be the manner or style in which someone uses words.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>wordiness</em>
</span>
</span>
</div>
<a class='quick-help' href='verbiage#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='verbiage#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/verbiage/memory_hooks/5025.json'></span>
<p>
<span class="emp0"><span>Lawyers' Use of <span class="emp2"><span>Verb</span></span>s Makes Me <span class="emp1"><span>Age</span></span></span></span>  <span class="emp3"><span>I</span></span> get so frustrated when <span class="emp3"><span>I</span></span> read legal documents because there are too many confusing <span class="emp2"><span>verb</span></span>s in the <span class="emp2"><span>verb</span></span><span class="emp3"><span>i</span></span><span class="emp1"><span>age</span></span> that <span class="emp3"><span>I</span></span> prematurely <span class="emp1"><span>age</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="verbiage#" id="add-public-hook" style="" url="https://membean.com/mywords/verbiage/memory_hooks">Use other public hook</a>
<a href="verbiage#" id="memhook-use-own" url="https://membean.com/mywords/verbiage/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='verbiage#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Given all the <b>verbiage</b>, the millions of articles, blogs and the like around health care, is it really possible that the only views one could have on health reform align so perfectly with politics?
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
The latter, which is encumbered with much <b>verbiage</b>, in effect inquires whether the framers of the treaty of 1825 meant to exclude Great Britain from the coast.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Palin’s Twitter feed, which favors chock-a-block <b>verbiage</b>, and an utter disdain for established rules of punctuation, is extremely popular among conservatives.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/verbiage/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='verbiage#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='verb_word' data-tree-url='//cdn1.membean.com/public/data/treexml' href='verbiage#'>
<span class=''></span>
verb
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>word</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='i_connective' data-tree-url='//cdn1.membean.com/public/data/treexml' href='verbiage#'>
<span class=''></span>
-i-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>connective</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='age_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='verbiage#'>
<span class=''></span>
-age
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state</td>
</tr>
</table>
<p><em>Verbiage</em> is the &#8220;state of the words&#8221; which one uses.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='verbiage#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Verbiage" src="https://cdn1.membean.com/public/images/wordimages/cons2/verbiage.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='verbiage#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>circuitous</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>circumlocution</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>discursive</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>effusive</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>euphemism</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>expatiate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>garrulous</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>grandiloquent</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>gratuitous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>jargon</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>lexicon</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>loquacious</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>peroration</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>prattle</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>prolix</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>redundant</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>rhetoric</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>superfluous</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>tautology</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>verbose</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>voluble</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abridge</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>brevity</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>implicit</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>incisive</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>laconic</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>pithy</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>sententious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>succinct</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>taciturn</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>terse</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>truncate</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>unlettered</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="verbiage" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>verbiage</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="verbiage#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

