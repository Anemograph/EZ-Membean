
<!DOCTYPE html>
<html>
<head>
<title>Word: apposite | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>The word abject emphasizes a very bad situation or quality, thereby making it even worse.</p>
<p class='rw-defn idx1' style='display:none'>When something is apropos, it is fitting to the moment or occasion.</p>
<p class='rw-defn idx2' style='display:none'>A cogent reason or argument is strong and convincing.</p>
<p class='rw-defn idx3' style='display:none'>A commodious room or house is large and roomy, which makes it convenient and highly suitable for living.</p>
<p class='rw-defn idx4' style='display:none'>A condign reward or punishment is deserved by and appropriate or fitting for the person who receives it.</p>
<p class='rw-defn idx5' style='display:none'>A deft movement or action is made quickly and with skill.</p>
<p class='rw-defn idx6' style='display:none'>If something encumbers you, it makes it difficult for you to move freely or do what you want.</p>
<p class='rw-defn idx7' style='display:none'>Something ephemeral, such as some insects or a sunset, lasts for only a short time or has a very short lifespan.</p>
<p class='rw-defn idx8' style='display:none'>Something that is evanescent lasts for only a short time before disappearing from sight or memory.</p>
<p class='rw-defn idx9' style='display:none'>Something that is extraneous is not relevant or connected to something else, or it is not essential to a given situation.</p>
<p class='rw-defn idx10' style='display:none'>If you are experiencing felicity about something, you are very happy about it.</p>
<p class='rw-defn idx11' style='display:none'>An idea or remark is germane to a situation if it is connected to it in an important or fitting way.</p>
<p class='rw-defn idx12' style='display:none'>If two people are incompatible, they do not get along, tend to disagree, and are unable to cooperate with one another.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is inept is unable or unsuitable to do a job; hence, they are unfit for it.</p>
<p class='rw-defn idx14' style='display:none'>When something happens at an inopportune time it is inconvenient or not suitable.</p>
<p class='rw-defn idx15' style='display:none'>Irrelevant information is unrelated or unconnected to the situation at hand.</p>
<p class='rw-defn idx16' style='display:none'>If you describe something as palatable, such as an idea or suggestion, you believe that people will accept it; palatable food or drink is agreeable to the taste.</p>
<p class='rw-defn idx17' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx18' style='display:none'>If you handle something in a pragmatic way, you deal with it in a realistic and practical manner rather than just following unproven theories or ideas.</p>
<p class='rw-defn idx19' style='display:none'>A sagacious person is wise, intelligent, and has the ability to make good practical decisions.</p>
<p class='rw-defn idx20' style='display:none'>Something that is specious seems to be good, sound, or correct but is actually wrong or fake.</p>
<p class='rw-defn idx21' style='display:none'>A tangential point in an argument merely touches upon something that is not really relevant to the conversation at hand; rather, it is a minor or unimportant point.</p>
<p class='rw-defn idx22' style='display:none'>Something that is temporal deals with the present and somewhat brief time of this world.</p>
<p class='rw-defn idx23' style='display:none'>A utilitarian object is useful, serviceable, and practical—rather than fancy or unnecessary.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>apposite</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='apposite#' id='pronounce-sound' path='audio/words/amy-apposite'></a>
AP-uh-zit
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='apposite#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='apposite#' id='context-sound' path='audio/wordcontexts/brian-apposite'></a>
&#8220;Absent-minded professor&#8221; is an <em>apposite</em> description of my father, who can rarely find his glasses and has been known to put the newspaper in the refrigerator.  Despite his forgetfulness in daily matters, he is a brilliant scholar and can find an <em>apposite</em> quotation from the classics that would fit just about any situation.  I was surprised to hear him describe popular musical genres with a few suitable, <em>apposite</em> phrases just the other day.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an <em>apposite</em> comment during a class discussion?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
One that distracts from the discussion at hand.
</li>
<li class='choice '>
<span class='result'></span>
One that contradicts what everyone else in class has said.
</li>
<li class='choice answer '>
<span class='result'></span>
One that adds to the discussion in a positive fashion.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='apposite#' id='definition-sound' path='audio/wordmeanings/amy-apposite'></a>
Something that is <em>apposite</em> is relevant or suitable to what is happening or being discussed.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>suitable</em>
</span>
</span>
</div>
<a class='quick-help' href='apposite#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='apposite#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/apposite/memory_hooks/4393.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Ap</span></span>t?  <span class="emp2"><span>Posit</span></span>ively!</span></span>  Mrs. Mogg is completely <span class="emp1"><span>ap</span></span><span class="emp2"><span>posit</span></span>e for the teaching <span class="emp2"><span>posit</span></span>ion--there is no question that she is <span class="emp2"><span>posit</span></span>ively <span class="emp1"><span>ap</span></span>t!
</p>
</div>

<div id='memhook-button-bar'>
<a href="apposite#" id="add-public-hook" style="" url="https://membean.com/mywords/apposite/memory_hooks">Use other public hook</a>
<a href="apposite#" id="memhook-use-own" url="https://membean.com/mywords/apposite/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='apposite#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Already established as a short-story writer, she broadens her scope with insights as varied and <b>apposite</b> as the fabrics and designs that her protagonist, Carrie Bell, works with at her beloved sewing machine.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
While "Chicago Poems" is chock-full with <b>apposite</b> specimens [separating haves from have-nots], [Carl Sandburg’s] "Child of the Romans" proffers a fine example of this poetic odd-man-out framework.
<cite class='attribution'>
&mdash;
Chicago Tribune
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/apposite/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='apposite#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ap_to' data-tree-url='//cdn1.membean.com/public/data/treexml' href='apposite#'>
<span class=''></span>
ap-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to, towards, at, near</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='posit_placed' data-tree-url='//cdn1.membean.com/public/data/treexml' href='apposite#'>
<span class='common'></span>
posit
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>placed, put</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='apposite#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>An <em>apposite</em> comment is one that is suitably &#8220;placed towards&#8221; or &#8220;put at or near&#8221; a given situation.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='apposite#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Gary Lineker</strong><span> How apposite it was that Chelsea's greatest striker put his side ahead on such an important date.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/apposite.jpg' video_url='examplevids/apposite' video_width='350'></span>
<div id='wt-container'>
<img alt="Apposite" height="288" src="https://cdn1.membean.com/video/examplevids/apposite.jpg" width="350" />
<div class='center'>
<a href="apposite#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='apposite#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Apposite" src="https://cdn3.membean.com/public/images/wordimages/cons2/apposite.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='apposite#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>apropos</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>cogent</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>commodious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>condign</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>deft</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>felicity</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>germane</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>palatable</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>pragmatic</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>sagacious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>utilitarian</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abject</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>encumber</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>ephemeral</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>evanescent</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>extraneous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>incompatible</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>inept</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>inopportune</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>irrelevant</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>specious</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>tangential</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>temporal</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="apposite" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>apposite</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="apposite#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

