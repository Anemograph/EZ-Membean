
<!DOCTYPE html>
<html>
<head>
<title>Word: suborn | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abase yourself, people respect you less because you act in a way that is beneath you; due to this behavior, you are lowered or reduced in rank, esteem, or reputation. </p>
<p class='rw-defn idx1' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx2' style='display:none'>When you abet someone, you help them in a criminal or illegal act.</p>
<p class='rw-defn idx3' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx4' style='display:none'>Something that is clandestine is hidden or kept a secret, often because it is illegal.</p>
<p class='rw-defn idx5' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx6' style='display:none'>Depravity is behavior that is immoral, corrupt, or evil.</p>
<p class='rw-defn idx7' style='display:none'>Something, such as a building, is derelict if it is empty, not used, and in bad condition or disrepair.</p>
<p class='rw-defn idx8' style='display:none'>If you refer to the enormity of a situation, problem, or event, you mean that it is very evil or morally offensive; this word has nothing to do with physical size.</p>
<p class='rw-defn idx9' style='display:none'>If someone behaves in a furtive way, they do things sneakily and secretly in order to avoid being noticed.</p>
<p class='rw-defn idx10' style='display:none'>Someone who is indiscreet shows lack of judgment, especially because they say or do things in public that should only be said or done privately—if at all.</p>
<p class='rw-defn idx11' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx12' style='display:none'>Malfeasance is an unlawful act, especially one committed by a trusted public official.</p>
<p class='rw-defn idx13' style='display:none'>A mendacious person does not tell the truth.</p>
<p class='rw-defn idx14' style='display:none'>A peccadillo is a slight or minor offense that can be overlooked.</p>
<p class='rw-defn idx15' style='display:none'>Pecuniary means relating to or concerning money.</p>
<p class='rw-defn idx16' style='display:none'>If you prevaricate, you avoid giving a direct or honest answer, usually because you want to hide the truth or want to delay or avoid making a hard decision.</p>
<p class='rw-defn idx17' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is profligate is lacking in restraint, which can manifest in carelessly and wastefully using resources, such as energy or money; this kind of person can also act in an immoral way.</p>
<p class='rw-defn idx19' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx20' style='display:none'>You show rectitude if you behave or conduct yourself in an honest and morally correct manner.</p>
<p class='rw-defn idx21' style='display:none'>Something statutory, such as the power given to a governor or president, is created, established, and controlled by rules and laws; hence, it has the full force of the law behind it and must be followed.</p>
<p class='rw-defn idx22' style='display:none'>To taint is to give an undesirable quality that damages a person&#8217;s reputation or otherwise spoils something.</p>
<p class='rw-defn idx23' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>
<p class='rw-defn idx24' style='display:none'>Something that is unsullied is unstained and clean.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is venal is dishonest, corrupt, and ready to do anything for money.</p>
<p class='rw-defn idx26' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx27' style='display:none'>The verity of something is the truth or reality of it.</p>
<p class='rw-defn idx28' style='display:none'>If someone vitiates something, they make it less effective, weaker, or reduced in quality.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>suborn</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='suborn#' id='pronounce-sound' path='audio/words/amy-suborn'></a>
suh-BAWRN
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='suborn#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='suborn#' id='context-sound' path='audio/wordcontexts/brian-suborn'></a>
Rafe persuaded the blackjack dealer to rig the card game by <em>suborning</em> him into committing the illegal deed with a large sum of money.  Rafe promised to share his large winnings with the dealer, <em>suborning</em> or influencing him to use a faulty deck of cards in his favor.  It took a great deal of money to <em>suborn</em> or convince the dealer to commit the crime against the casino.  At the last possible moment, however, the dealer backed out and exposed Rafe&#8217;s attempt at <em>subornation</em> or bribery.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean to <em>suborn</em> someone?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
To share a large amount of money with that person, such as from the proceeds of a lottery.
</li>
<li class='choice answer '>
<span class='result'></span>
To entice them into committing an illegal deed, usually by paying them off.
</li>
<li class='choice '>
<span class='result'></span>
To alter a game so that the other players are tricked—at least for a while.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='suborn#' id='definition-sound' path='audio/wordmeanings/amy-suborn'></a>
If you <em>suborn</em> someone, you persuade them to do something illegal, especially by giving them money to do so.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>bribe</em>
</span>
</span>
</div>
<a class='quick-help' href='suborn#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='suborn#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/suborn/memory_hooks/4484.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Born</span></span> by <span class="emp2"><span>Su</span></span>m</span></span> The evil plot was hatched by a <span class="emp2"><span>su</span></span>m of money which <span class="emp2"><span>su</span></span><span class="emp3"><span>born</span></span>ed the local police, hence allowing the plot to be <span class="emp3"><span>born</span></span> and successfully carried out.
</p>
</div>

<div id='memhook-button-bar'>
<a href="suborn#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/suborn/memory_hooks">Use other hook</a>
<a href="suborn#" id="memhook-use-own" url="https://membean.com/mywords/suborn/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='suborn#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
A central tenet of the American Revolution had been that a corrupt British ministry had <b>suborned</b> Parliament through patronage and pensions and used the resulting excessive influence to tax the colonists and deprive them of their ancient English liberties.
<span class='attribution'>&mdash; Ron Chernow, American writer and historian, from _Alexander Hamilton_</span>
<img alt="Ron chernow, american writer and historian, from _alexander hamilton_" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Ron Chernow, American writer and historian, from _Alexander Hamilton_.jpg?qdep8" width="80" />
</li>
<li>
Clinton said narcotics traffickers may spend hundreds of millions of dollars a year to try to <b>suborn</b> Mexican police, most of who make less than $10,000 a year.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
Richard Norton Smith, a presidential scholar who is completing a biography of Rockefeller, wrote in an e-mail message, "One can readily see how, from the outside, his gifts/loans might appear to <b>suborn</b> his <b>subordinates</b>."
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
A senior Southeast Asian diplomat explained to me that China is using money and pressure to “<b>suborn”</b> countries in the region. He pointed out that aid is often carefully targeted, so that money to Malaysia, for example, is directed specifically to the state of Pahang, the political base of the prime minister.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/suborn/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='suborn#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='sub_under' data-tree-url='//cdn1.membean.com/public/data/treexml' href='suborn#'>
<span class='common'></span>
sub-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>under, from below</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='orn_decorate' data-tree-url='//cdn1.membean.com/public/data/treexml' href='suborn#'>
<span class=''></span>
orn
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>decorate, beautify</td>
</tr>
</table>
<p>To <em>suborn</em> another is to &#8220;decorate&#8221; that person&#8217;s purse with money &#8220;from below,&#8221; keeping the transaction secret or &#8220;under&#8221; the eyes of authorities to get that person to do something illegal for you.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='suborn#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Tijuana Racket</strong><span> These travelers figure out pretty quickly that they are going to have to suborn the police to get back their "stolen" car.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/suborn.jpg' video_url='examplevids/suborn' video_width='350'></span>
<div id='wt-container'>
<img alt="Suborn" height="288" src="https://cdn1.membean.com/video/examplevids/suborn.jpg" width="350" />
<div class='center'>
<a href="suborn#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='suborn#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Suborn" src="https://cdn2.membean.com/public/images/wordimages/cons2/suborn.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='suborn#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abase</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abet</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>clandestine</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>depravity</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>derelict</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>enormity</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>furtive</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>indiscreet</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>malfeasance</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>mendacious</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>pecuniary</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>prevaricate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>profligate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>taint</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>venal</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>vitiate</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>peccadillo</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>rectitude</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>statutory</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>unsullied</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='suborn#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
subornation
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>bribery</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="suborn" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>suborn</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="suborn#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

