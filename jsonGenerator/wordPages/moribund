
<!DOCTYPE html>
<html>
<head>
<title>Word: moribund | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Moribund-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/moribund-large.jpg?qdep8" />
</div>
<a href="moribund#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>Apathy is a lack of interest or unenthusiastic involvement in an activity; there is no effort to change or improve it at all.</p>
<p class='rw-defn idx1' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx2' style='display:none'>To augment something is to increase its value or effectiveness by adding something to it.</p>
<p class='rw-defn idx3' style='display:none'>A decadent person has low moral standards and is more interested in pleasure than serious matters.</p>
<p class='rw-defn idx4' style='display:none'>Something that is defunct is no longer in existence or does not function.</p>
<p class='rw-defn idx5' style='display:none'>A demise can be the death of someone or the slow end of something.</p>
<p class='rw-defn idx6' style='display:none'>A dirge is a slow and sad piece of music often performed at funerals.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx8' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx9' style='display:none'>Efficacy is the ability or power to produce an expected effect or result.</p>
<p class='rw-defn idx10' style='display:none'>If something enervates you, it makes you very tired and weak—almost to the point of collapse.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx12' style='display:none'>If something is formidable, it is impressive in size, power, or skill; therefore, it can make you feel frightened, alarmed, or in awe because it is so powerful or difficult to deal with.</p>
<p class='rw-defn idx13' style='display:none'>An inception of something is its beginning or start.</p>
<p class='rw-defn idx14' style='display:none'>If an idea, plan, or attitude is inchoate, it is vague and imperfectly formed because it is just starting to develop.</p>
<p class='rw-defn idx15' style='display:none'>An incipient situation or quality is just beginning to appear or develop.</p>
<p class='rw-defn idx16' style='display:none'>When something is inert, it has no power of movement or is inactive.</p>
<p class='rw-defn idx17' style='display:none'>To initiate something is to begin or start it.</p>
<p class='rw-defn idx18' style='display:none'>An innovation is something new that is created or is a novel process for doing something.</p>
<p class='rw-defn idx19' style='display:none'>A languid person is slow, relaxed, and shows little energy or interest in doing anything.</p>
<p class='rw-defn idx20' style='display:none'>If you are lethargic, you are tired, lack energy, and are unwilling to exert effort.</p>
<p class='rw-defn idx21' style='display:none'>When you are listless, you lack energy and interest and are unwilling to exert any effort.</p>
<p class='rw-defn idx22' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx23' style='display:none'>Something that is nascent is just starting to develop and is expected to become stronger and bigger in time.</p>
<p class='rw-defn idx24' style='display:none'>If something is obsolescent, it is slowly becoming no longer needed because something newer or more effective has been invented.</p>
<p class='rw-defn idx25' style='display:none'>A paradigm is a model or example of something that shows how it works or how it can be produced; a paradigm shift is a completely new way of thinking about something.</p>
<p class='rw-defn idx26' style='display:none'>When you perpetuate something, you keep it going or continue it indefinitely.</p>
<p class='rw-defn idx27' style='display:none'>Someone who is phlegmatic stays calm and unemotional even in dangerous or exciting situations.</p>
<p class='rw-defn idx28' style='display:none'>Something that is pristine has not lost any of its original perfection or purity.</p>
<p class='rw-defn idx29' style='display:none'>When someone is rehabilitated, they are restored to a more normal way of life, such as returning to good health or a crime-free life.</p>
<p class='rw-defn idx30' style='display:none'>A resurgence is a rising again or comeback of something.</p>
<p class='rw-defn idx31' style='display:none'>If you are somnolent, you are sleepy.</p>
<p class='rw-defn idx32' style='display:none'>If you are supine, you are lying on your back with your face upward.</p>
<p class='rw-defn idx33' style='display:none'>If your body is affected by torpor, you are severely lacking in energy; therefore, you are idle—and can even be numb.</p>
<p class='rw-defn idx34' style='display:none'>A vibrant person is lively and full of energy in a way that is exciting and attractive.</p>
<p class='rw-defn idx35' style='display:none'>If someone is described as vivacious, they are lively and have a happy, lighthearted manner.</p>
<p class='rw-defn idx36' style='display:none'>If something, such as clothing, is in vogue, it is currently in fashion or is one of the hottest new trends; hence, it is very popular.</p>
<p class='rw-defn idx37' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>
<p class='rw-defn idx38' style='display:none'>If you waver, you cannot decide between two things because you have serious doubts about which choice is better.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>moribund</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='moribund#' id='pronounce-sound' path='audio/words/amy-moribund'></a>
MOR-uh-buhnd
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='moribund#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='moribund#' id='context-sound' path='audio/wordcontexts/brian-moribund'></a>
The <em>moribund</em> committee that hosted the concert series was at long last coming to a slow end.  Interest for our town&#8217;s music was dying out or becoming <em>moribund</em> because there was a larger and better venue in a nearby town.  The members of the music committee threw a farewell party at which they toasted the <em>moribund</em> or fading days of their organization.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When would your computer be considered <em>moribund</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It continues to be consistently reliable each day.
</li>
<li class='choice '>
<span class='result'></span>
It has just been taken out of the box to use for the first time.
</li>
<li class='choice answer '>
<span class='result'></span>
It has slowed down so much that it is no longer useful.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='moribund#' id='definition-sound' path='audio/wordmeanings/amy-moribund'></a>
If you describe something as <em>moribund</em>, you imply that it is no longer effective; it may also be coming to the end of its useful existence.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>ending usefulness</em>
</span>
</span>
</div>
<a class='quick-help' href='moribund#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='moribund#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/moribund/memory_hooks/4301.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Mound</span></span> of <span class="emp3"><span>Rib</span></span>s</span></span> People used to burn bones or <span class="emp1"><span>mound</span></span>s of <span class="emp3"><span>rib</span></span>s in the olden days; these "bone fires" later became know as "bonfires."  Real bone fires where a <span class="emp1"><span>mound</span></span> of <span class="emp3"><span>rib</span></span>s are burned are now <span class="emp1"><span>mo</span></span><span class="emp3"><span>rib</span></span><span class="emp1"><span>und</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="moribund#" id="add-public-hook" style="" url="https://membean.com/mywords/moribund/memory_hooks">Use other public hook</a>
<a href="moribund#" id="memhook-use-own" url="https://membean.com/mywords/moribund/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='moribund#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Urban renewal planners plopped the big chunk of concrete in the middle of Pontiac’s <b>moribund</b> downtown, hoping that a giant city-owned parking structure would jump-start downtown development. That never happened. By the 1990s, the city began going broke.
<cite class='attribution'>
&mdash;
Detroit Free Press
</cite>
</li>
<li>
From the forests of New Brunswick to the outports of Newfoundland, rural communities are emptying out as residents head west to find steadier work and higher pay in the oil fields of northern Alberta. Tens of thousands have left Atlantic Canada in recent years, leaving behind an increasingly dire labor shortage that threatens to further undermine the region's <b>moribund</b> economy.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
The motivations behind dispatching expeditions of discovery involved plenty of pushes and pulls. . . . By the early 18th century, however, exploration was becoming <b>moribund</b>. . . . Like a rogue wave, the Great Age of Discovery had, it seemed, risen, spread, and subsided.
<cite class='attribution'>
&mdash;
History News Network
</cite>
</li>
<li>
During his tenure in office, Seeley oversaw the rebuilding of the town's Public Works Department after its physical plant was destroyed by fire, managed the town's response to flooding along Lake Ontario, and helped orchestrate new uses for what had become perhaps the town's biggest blight—the <b>moribund</b> Medley Centre mall, which has been renamed Skyview on the Ridge.
<cite class='attribution'>
&mdash;
Rochester City Newspaper
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/moribund/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='moribund#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='mort_death' data-tree-url='//cdn1.membean.com/public/data/treexml' href='moribund#'>
<span class=''></span>
mort
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>death</td>
</tr>
<tr>
<td class='partform'>-bund</td>
<td>
&rarr;
</td>
<td class='meaning'>acting in a certain way</td>
</tr>
</table>
<p>Something <em>moribund</em> is undergoing a slow but sure &#8220;death.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='moribund#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Moribund" src="https://cdn3.membean.com/public/images/wordimages/cons2/moribund.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='moribund#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>apathy</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>decadent</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>defunct</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>demise</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>dirge</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>enervate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>inert</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>languid</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>lethargic</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>listless</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>obsolescent</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>phlegmatic</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>somnolent</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>supine</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>torpor</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>waver</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>augment</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>efficacy</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>formidable</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>inception</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>inchoate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>incipient</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>initiate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>innovation</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>nascent</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>paradigm</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>perpetuate</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>pristine</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>rehabilitate</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>resurgence</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>vibrant</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>vivacious</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>vogue</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="moribund" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>moribund</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="moribund#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

