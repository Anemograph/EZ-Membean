
<!DOCTYPE html>
<html>
<head>
<title>Word: meritorious | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you are adept at something, you are very good, skillful, or talented at it.</p>
<p class='rw-defn idx1' style='display:none'>Someone who is adroit is skillful and clever, especially in thought, behavior, or action.</p>
<p class='rw-defn idx2' style='display:none'>A bravura performance, such as of a highly technical work for the violin, is done with consummate skill.</p>
<p class='rw-defn idx3' style='display:none'>The caliber of a person is the level or quality of their ability, intelligence, and other talents.</p>
<p class='rw-defn idx4' style='display:none'>If someone is competent in a job, they are able and skilled enough to do it well.</p>
<p class='rw-defn idx5' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx6' style='display:none'>A definitive opinion on an issue cannot be challenged; therefore, it is the final word or the most authoritative pronouncement on that issue.</p>
<p class='rw-defn idx7' style='display:none'>A degenerate person is immoral, wicked, or corrupt.</p>
<p class='rw-defn idx8' style='display:none'>Depravity is behavior that is immoral, corrupt, or evil.</p>
<p class='rw-defn idx9' style='display:none'>Efficacy is the ability or power to produce an expected effect or result.</p>
<p class='rw-defn idx10' style='display:none'>Errant things or people behave in a way that is unacceptable or wrong, such as a missile that travels in the wrong direction or a student who does not follow classroom rules.</p>
<p class='rw-defn idx11' style='display:none'>An exemplary person is one that sets an ideal or praiseworthy example for others to follow.</p>
<p class='rw-defn idx12' style='display:none'>One thing that exemplifies another serves as an example of it, illustrates it, or demonstrates it.</p>
<p class='rw-defn idx13' style='display:none'>A thing or action that is expedient is useful, advantageous, or appropriate to get something accomplished.</p>
<p class='rw-defn idx14' style='display:none'>If you think something, such as a statement or idea, is fatuous, you consider it stupid or extremely silly; a fatuous hope is unrealistic.</p>
<p class='rw-defn idx15' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx16' style='display:none'>A fruitless effort at doing something does not bring about a successful result.</p>
<p class='rw-defn idx17' style='display:none'>A futile attempt at doing something is hopeless and pointless because it simply cannot be accomplished.</p>
<p class='rw-defn idx18' style='display:none'>A hapless person is unlucky, so much so that a stream of unfortunate things has happened and continues to happen to them.</p>
<p class='rw-defn idx19' style='display:none'>Someone who has had an illustrious professional career is celebrated and outstanding in their given field of expertise.</p>
<p class='rw-defn idx20' style='display:none'>If you describe someone&#8217;s appearance or behavior as impeccable, you mean that it is perfect and therefore impossible to criticize.</p>
<p class='rw-defn idx21' style='display:none'>If you describe someone&#8217;s behavior as inane, you think it is completely stupid or without much meaning.</p>
<p class='rw-defn idx22' style='display:none'>An indolent person is lazy.</p>
<p class='rw-defn idx23' style='display:none'>Someone who is ineffectual at a task is useless or ineffective when attempting to do it.</p>
<p class='rw-defn idx24' style='display:none'>Someone who is inept is unable or unsuitable to do a job; hence, they are unfit for it.</p>
<p class='rw-defn idx25' style='display:none'>Something that has inestimable value or benefit has so much of it that it cannot be calculated.</p>
<p class='rw-defn idx26' style='display:none'>If something is infallible, it is never wrong and so is incapable of making mistakes.</p>
<p class='rw-defn idx27' style='display:none'>An infamous person has a very bad reputation because they have done disgraceful or dishonorable things.</p>
<p class='rw-defn idx28' style='display:none'>Ingenuity in solving a problem uses creativity, intelligence, and cleverness.</p>
<p class='rw-defn idx29' style='display:none'>Someone, such as a performer or athlete, is inimitable when they are so good or unique in their talent that it is unlikely anyone else can be their equal.</p>
<p class='rw-defn idx30' style='display:none'>If you describe ideas as jejune, you are saying they are childish and uninteresting; the adjective jejune also describes those having little experience, maturity, or knowledge.</p>
<p class='rw-defn idx31' style='display:none'>If you do something in a lackadaisical way, you do it carelessly and without putting much effort into it—thereby showing that you are not really interested in what you&#8217;re doing.</p>
<p class='rw-defn idx32' style='display:none'>A laudatory article or speech expresses praise or admiration for someone or something.</p>
<p class='rw-defn idx33' style='display:none'>Something mediocre is average or ordinary in quality; it’s just OK.</p>
<p class='rw-defn idx34' style='display:none'>A nefarious activity is considered evil and highly dishonest.</p>
<p class='rw-defn idx35' style='display:none'>A notorious person is well-known by the public at large; they are usually famous for doing something bad.</p>
<p class='rw-defn idx36' style='display:none'>If you receive a plaudit, you receive admiration, praise, and approval from someone.</p>
<p class='rw-defn idx37' style='display:none'>Someone who is profligate is lacking in restraint, which can manifest in carelessly and wastefully using resources, such as energy or money; this kind of person can also act in an immoral way.</p>
<p class='rw-defn idx38' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>
<p class='rw-defn idx39' style='display:none'>When you have been remiss, you have been careless because you did not do something that you should have done.</p>
<p class='rw-defn idx40' style='display:none'>A reprobate has a bad character and behaves in an immoral or improper way.</p>
<p class='rw-defn idx41' style='display:none'>A superlative deed or act is excellent, outstanding, or the best of its kind.</p>
<p class='rw-defn idx42' style='display:none'>An action or deed is unconscionable if it is excessively shameful, unfair, or unjust and its effects are more severe than is reasonable or acceptable.</p>
<p class='rw-defn idx43' style='display:none'>If you are unsurpassed in what you do, you are the best—period.</p>
<p class='rw-defn idx44' style='display:none'>A virtuoso is someone who is very skillful at something, especially playing a musical instrument.</p>
<p class='rw-defn idx45' style='display:none'>A wanton action deliberately harms someone or damages something for no apparent or good reason.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>meritorious</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='meritorious#' id='pronounce-sound' path='audio/words/amy-meritorious'></a>
mer-i-TAWR-ee-uhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='meritorious#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='meritorious#' id='context-sound' path='audio/wordcontexts/brian-meritorious'></a>
Some teaching systems now reward teachers financially for <em>meritorious</em> or praiseworthy work in the classroom.  This merit system, which rewards <em>meritorious</em> or excellent work on the job, is loved by some, and hated by others.  Some people might think it would be hard to tell a <em>meritorious</em> teacher over one who does not deserve special honor, but deep down everyone does know who the best teachers are, and the ones who slack off.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When would a person be considered <em>meritorious</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
They do something noble and are honored for it.
</li>
<li class='choice '>
<span class='result'></span>
They complete a task and are satisfied by their work.
</li>
<li class='choice '>
<span class='result'></span>
They make several mistakes and are frustrated by them.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='meritorious#' id='definition-sound' path='audio/wordmeanings/amy-meritorious'></a>
Someone who is <em>meritorious</em> is worthy of receiving recognition or is praiseworthy because of what they have accomplished.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>praiseworthy</em>
</span>
</span>
</div>
<a class='quick-help' href='meritorious#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='meritorious#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/meritorious/memory_hooks/5459.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Merit</span></span> Gl<span class="emp3"><span>orious</span></span></span></span> Her classroom <span class="emp1"><span>merit</span></span> was gl<span class="emp3"><span>orious</span></span> to behold--there is no doubt that she is a <span class="emp1"><span>merit</span></span><span class="emp3"><span>orious</span></span> teacher.
</p>
</div>

<div id='memhook-button-bar'>
<a href="meritorious#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/meritorious/memory_hooks">Use other hook</a>
<a href="meritorious#" id="memhook-use-own" url="https://membean.com/mywords/meritorious/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='meritorious#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
[T]he college gives to <b>meritorious</b> students of one year’s standing single copies of standard books, bound in crimson, with the college seal, as rewards for study.
<cite class='attribution'>
&mdash;
The New York Times (1893)
</cite>
</li>
<li>
Research that he undertook because of his wife’s illness fetched him the <b>meritorious</b> award for behavior modeling at the 1980 International Congress on Applied Systems Research and Cybernetics, in Acapulco, Mexico.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Equally <b>meritorious</b> is "Cry If You Want," which harks back to another Who classic, "I Can See for Miles," in that it can be taken at face value, or as something greater.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/meritorious/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='meritorious#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='merit_earned' data-tree-url='//cdn1.membean.com/public/data/treexml' href='meritorious#'>
<span class=''></span>
merit
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>earned, gained, deserved</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='or_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='meritorious#'>
<span class=''></span>
-or
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ious_nature' data-tree-url='//cdn1.membean.com/public/data/treexml' href='meritorious#'>
<span class=''></span>
-ious
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of the nature of</td>
</tr>
</table>
<p>Some who is <em>meritorious</em> is in a &#8220;state of having gained or earned&#8221; a reward.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='meritorious#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>The Wizard of Oz</strong><span> The Cowardly Lion earns a medal for meritorious conduct.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/meritorious.jpg' video_url='examplevids/meritorious' video_width='350'></span>
<div id='wt-container'>
<img alt="Meritorious" height="288" src="https://cdn1.membean.com/video/examplevids/meritorious.jpg" width="350" />
<div class='center'>
<a href="meritorious#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='meritorious#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Meritorious" src="https://cdn3.membean.com/public/images/wordimages/cons2/meritorious.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='meritorious#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adept</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>adroit</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bravura</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>caliber</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>competent</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>definitive</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>efficacy</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>exemplary</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>exemplify</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>expedient</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>illustrious</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>impeccable</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>inestimable</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>infallible</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>ingenuity</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>inimitable</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>laudatory</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>plaudit</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>superlative</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>unsurpassed</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>virtuoso</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='7' class = 'rw-wordform notlearned'><span>degenerate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>depravity</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>errant</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>fatuous</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>fruitless</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>futile</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>hapless</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>inane</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>indolent</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>ineffectual</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>inept</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>infamous</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>jejune</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>lackadaisical</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>mediocre</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>nefarious</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>notorious</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>profligate</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>remiss</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>reprobate</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>unconscionable</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>wanton</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="meritorious" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>meritorious</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="meritorious#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

