
<!DOCTYPE html>
<html>
<head>
<title>Word: annihilate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Annihilate-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/annihilate-large.jpg?qdep8" />
</div>
<a href="annihilate#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>If someone in authority abolishes a law or practice, they formally put an end to it.</p>
<p class='rw-defn idx2' style='display:none'>To abrogate an act is to end it by official and sometimes legal means.</p>
<p class='rw-defn idx3' style='display:none'>Something done under the aegis of an organization or person is done with their support, backing, and protection.</p>
<p class='rw-defn idx4' style='display:none'>A bastion is considered an important and effective defense of a way of life or principle; a bastion can also refer to a heavily fortified place, such as a castle.</p>
<p class='rw-defn idx5' style='display:none'>Bedlam is a situation with a great deal of noise and confusion.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is benevolent wishes others well, often by being kind, filled with goodwill, and charitable towards them.</p>
<p class='rw-defn idx7' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx8' style='display:none'>When you buttress an argument, idea, or even a building, you make it stronger by providing support.</p>
<p class='rw-defn idx9' style='display:none'>A cataclysm is a violent, sudden event that causes great change and/or harm.</p>
<p class='rw-defn idx10' style='display:none'>If you circumvent something, such as a rule or restriction, you try to get around it in a clever and perhaps dishonest way.</p>
<p class='rw-defn idx11' style='display:none'>When you commemorate a person, you honor them or cause them to be remembered in some way.</p>
<p class='rw-defn idx12' style='display:none'>A debacle is something, such as an event or attempt, that fails completely in an embarrassing way.</p>
<p class='rw-defn idx13' style='display:none'>If you decimate something, you destroy a large part of it, reducing its size and effectiveness greatly.</p>
<p class='rw-defn idx14' style='display:none'>Something that is deleterious has a harmful effect.</p>
<p class='rw-defn idx15' style='display:none'>A deluge is a sudden, heavy downfall of rain; it can also be a large number of things, such as papers or e-mails, that someone gets all at the same time, making them very difficult to handle.</p>
<p class='rw-defn idx16' style='display:none'>A demise can be the death of someone or the slow end of something.</p>
<p class='rw-defn idx17' style='display:none'>If something discomfits you, it makes you feel embarrassed, confused, uncomfortable, or frustrated.</p>
<p class='rw-defn idx18' style='display:none'>If something disconcerts you, it makes you feel anxious, worried, or confused.</p>
<p class='rw-defn idx19' style='display:none'>When you dismantle something, you take it apart or destroy it piece by piece. </p>
<p class='rw-defn idx20' style='display:none'>To efface something is to erase or remove it completely from recognition or memory.</p>
<p class='rw-defn idx21' style='display:none'>If a fact or idea eludes you, you cannot remember or understand it; if you elude someone, you manage to escape or hide from them.</p>
<p class='rw-defn idx22' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx23' style='display:none'>When you eradicate something, you tear it up by the roots or remove it completely.</p>
<p class='rw-defn idx24' style='display:none'>When you excise something, you remove it by cutting it out.</p>
<p class='rw-defn idx25' style='display:none'>If you expunge something, you completely remove it or do away with it.</p>
<p class='rw-defn idx26' style='display:none'>If you extirpate something, you completely destroy it because it is unpleasant or unwanted.</p>
<p class='rw-defn idx27' style='display:none'>To liquidate a business or company is to close it down and sell the things that belong to it in order to pay off its debts.</p>
<p class='rw-defn idx28' style='display:none'>A malignant thing or person, such as a tumor or criminal, is deadly or does great harm.</p>
<p class='rw-defn idx29' style='display:none'>A melee is a noisy, confusing, hand-to-hand fight involving a group of people.</p>
<p class='rw-defn idx30' style='display:none'>To nullify something is to cancel or negate it.</p>
<p class='rw-defn idx31' style='display:none'>When you obliterate something, you destroy it to such an extent that there is nothing or very little of it left.</p>
<p class='rw-defn idx32' style='display:none'>A pandemic disease is a far-reaching epidemic that affects people in a very wide geographic area.</p>
<p class='rw-defn idx33' style='display:none'>Pandemonium is a very noisy and uncontrolled situation, especially one that is caused by a lot of angry or excited people.</p>
<p class='rw-defn idx34' style='display:none'>When you perpetuate something, you keep it going or continue it indefinitely.</p>
<p class='rw-defn idx35' style='display:none'>When you procure something, you obtain or get it in some fashion.</p>
<p class='rw-defn idx36' style='display:none'>A prophylactic is used as a preventative or protective agent to keep someone free from disease or infection.</p>
<p class='rw-defn idx37' style='display:none'>To purge something is to get rid of or remove it.</p>
<p class='rw-defn idx38' style='display:none'>To quell something is to stamp out, quiet, or overcome it.</p>
<p class='rw-defn idx39' style='display:none'>When you ravage something, you completely destroy, wreck, or damage it.</p>
<p class='rw-defn idx40' style='display:none'>A salutary experience is beneficial to you since it strengthens you in some way, although it may be unpleasant as you undergo it; this word also refers to promoting good health.</p>
<p class='rw-defn idx41' style='display:none'>A scourge was originally a whip used for torture and now refers to something that torments or causes serious trouble or devastation.</p>
<p class='rw-defn idx42' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx43' style='display:none'>A tempestuous storm, temper, or crowd is violent, wild, and in an uproar.</p>
<p class='rw-defn idx44' style='display:none'>Severe problems, suffering, or difficulties experienced in a particular situation are all examples of tribulations.</p>
<p class='rw-defn idx45' style='display:none'>A tumultuous event or period of time is filled with great excitement, confusion, or violence; a tumultuous reaction to something is likewise very loud and noisy because people are happy and excited.</p>
<p class='rw-defn idx46' style='display:none'>When you are vigilant, you are keenly watchful, careful, or attentive to a task or situation.</p>
<p class='rw-defn idx47' style='display:none'>If someone vitiates something, they make it less effective, weaker, or reduced in quality.</p>
<p class='rw-defn idx48' style='display:none'>A vortex is a force, such as a tornado or whirlpool, that draws things to its center and overwhelms anything caught within it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Verb</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>annihilate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='annihilate#' id='pronounce-sound' path='audio/words/amy-annihilate'></a>
uh-NAHY-uh-layt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='annihilate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='annihilate#' id='context-sound' path='audio/wordcontexts/brian-annihilate'></a>
When the Borg attacked planet Earth, their intent was to <em>annihilate</em> or completely destroy the human species as it currently existed.  In order to <em>annihilate</em> or wipe it out, they would make all humans into half-human, half-machine cyborgs that would be totally brought under the control of the Borg collective.  Fortunately, the human species was not so easy to <em>annihilate</em> or totally get rid of, so the Borg had to suspend their plans to the future.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean to <em>annihilate</em> something?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
It means to totally do away with it.
</li>
<li class='choice '>
<span class='result'></span>
It means to make plans to destroy it.
</li>
<li class='choice '>
<span class='result'></span>
It means to transport it to a safer location.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='annihilate#' id='definition-sound' path='audio/wordmeanings/amy-annihilate'></a>
When something is <em>annihilated</em>, it is completely destroyed or wiped out.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>thoroughly destroy</em>
</span>
</span>
</div>
<a class='quick-help' href='annihilate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='annihilate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/annihilate/memory_hooks/5076.json'></span>
<p>
<span class="emp0"><span>Giantess <span class="emp1"><span>Ann</span></span> <span class="emp3"><span>Ate</span></span> the <span class="emp2"><span>Hil</span></span>l</span></span> <span class="emp1"><span>Ann</span></span> <span class="emp3"><span>ate</span></span> the <span class="emp2"><span>hil</span></span>l, thus it was <span class="emp1"><span>ann</span></span>i<span class="emp2"><span>hil</span></span><span class="emp3"><span>ate</span></span>d by <span class="emp1"><span>Ann</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="annihilate#" id="add-public-hook" style="" url="https://membean.com/mywords/annihilate/memory_hooks">Use other public hook</a>
<a href="annihilate#" id="memhook-use-own" url="https://membean.com/mywords/annihilate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='annihilate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
This strategy seeks to reduce mosquito numbers rather than <b>annihilate</b> the insects entirely, which could have unintended ecological consequences. . . . But not everyone agrees that eradicating mosquitoes is a bad idea. "A recent study suggested that malaria mosquitoes do not play a vital role in the ecosystem, and therefore their removal would have minimal impact," Logan says.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
It’s because soap—regular soap, fancy honeysuckle soap, artisan soap, just any soap—absolutely <b>annihilates</b> viruses. . . . When viruses interact with soap, [their] fat coating gets ripped out by the soap molecules. Soap literally demolishes viruses.
<cite class='attribution'>
&mdash;
Vox
</cite>
</li>
<li>
The Marlins and Phillies have been leading the pack most of the month but merely treading water, vulnerable for a shark to rise from the depths and <b>annihilate</b> them.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
In the early experiments it looked like the virus called VP882 was . . . intercepting molecular messages exchanged by its host bacteria, and reading them to determine the best time to <b>annihilate</b> the whole bacterial colony. . . . opening the possibility that the virus could be engineered into an ideal killing machine for dangerous pathogens.
<cite class='attribution'>
&mdash;
Scientific American
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/annihilate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='annihilate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='an_to' data-tree-url='//cdn1.membean.com/public/data/treexml' href='annihilate#'>
<span class=''></span>
an-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to, towards, at, near</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='nihil_nothing' data-tree-url='//cdn1.membean.com/public/data/treexml' href='annihilate#'>
<span class=''></span>
nihil
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>nothing</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ate_quality' data-tree-url='//cdn1.membean.com/public/data/treexml' href='annihilate#'>
<span class=''></span>
-ate
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to make someone or something have a certain quality</td>
</tr>
</table>
<p>To <em>annihilate</em> someone or something is to &#8220;make him, her, or it (move) towards or (be) at the state of nothingness.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='annihilate#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>By Any Other Name</strong><span> These aliens annihilate two members of Captain Kirk's crew.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/annihilate.jpg' video_url='examplevids/annihilate' video_width='350'></span>
<div id='wt-container'>
<img alt="Annihilate" height="288" src="https://cdn1.membean.com/video/examplevids/annihilate.jpg" width="350" />
<div class='center'>
<a href="annihilate#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='annihilate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Annihilate" src="https://cdn0.membean.com/public/images/wordimages/cons2/annihilate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='annihilate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abolish</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abrogate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>bedlam</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>cataclysm</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>debacle</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>decimate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>deleterious</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>deluge</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>demise</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>discomfit</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>disconcert</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>dismantle</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>efface</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>eradicate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>excise</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>expunge</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>extirpate</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>liquidate</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>malignant</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>melee</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>nullify</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>obliterate</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>pandemic</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>pandemonium</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>purge</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>quell</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>ravage</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>scourge</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>tempestuous</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>tribulation</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>tumultuous</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>vitiate</span> &middot;</li><li data-idx='48' class = 'rw-wordform notlearned'><span>vortex</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>aegis</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>bastion</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>benevolent</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>buttress</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>circumvent</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>commemorate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>elude</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>perpetuate</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>procure</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>prophylactic</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>salutary</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>vigilant</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="annihilate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>annihilate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="annihilate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

