
<!DOCTYPE html>
<html>
<head>
<title>Word: jargon | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An argot is a special language or set of expressions used by a particular group of people that those outside the group find hard to understand.</p>
<p class='rw-defn idx1' style='display:none'>A cabal is a group of people who secretly meet to plan things in order to gain power.</p>
<p class='rw-defn idx2' style='display:none'>A cadre is a group specifically trained to lead, formalize, and accomplish a particular task or to train others as part of a larger organization; it can also be used to describe an elite military force.</p>
<p class='rw-defn idx3' style='display:none'>A clique is a group of people that spends a lot of time together and tends to be unfriendly towards people who are not part of the group.</p>
<p class='rw-defn idx4' style='display:none'>Dialectic is a dialogue between two people who present logical arguments in order to persuade each other of the truth of their opinion; during this interchange, the debaters arrive at a truth, which often involves parts of each debater&#8217;s opinion.</p>
<p class='rw-defn idx5' style='display:none'>An egalitarian social position states that all people should be equal or treated in the same way when it comes to economic, political, and social rights.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is erudite is steeped in knowledge because they have read and studied extensively.</p>
<p class='rw-defn idx7' style='display:none'>Idiosyncratic tendencies, behavior, or habits are unusual and strange.</p>
<p class='rw-defn idx8' style='display:none'>If someone is insular, they are either unwilling to meet anyone outside their own small group or they are not interested in learning new ideas.</p>
<p class='rw-defn idx9' style='display:none'>The intelligentsia of a society are those individuals who are the most highly educated.</p>
<p class='rw-defn idx10' style='display:none'>An interlocutor is the person with whom you are having a (usually formal) conversation or discussion.</p>
<p class='rw-defn idx11' style='display:none'>The lexicon of a particular subject or language is all the words, phrases, and terms associated with it.</p>
<p class='rw-defn idx12' style='display:none'>Nomenclature is a specialized form of vocabulary that classifies or organizes things in the sciences or the arts into a clear and usable system.</p>
<p class='rw-defn idx13' style='display:none'>You are applying parlance when you use words or expressions that are used by a particular group of people who have a unique way of speaking; for example, &#8220;descendants&#8221; is often used in place of &#8220;children&#8221; in legal parlance.</p>
<p class='rw-defn idx14' style='display:none'>Someone has a parochial outlook when they are mostly concerned with narrow or limited issues that affect a small, local area or a very specific cause; they are also unconcerned with wider or more general issues.</p>
<p class='rw-defn idx15' style='display:none'>A patois is a form of language used by a people in a small area that is different from the national or standard language.</p>
<p class='rw-defn idx16' style='display:none'>Something prosaic is dull, boring, and ordinary.</p>
<p class='rw-defn idx17' style='display:none'>Rhetoric is the skill or art of using language to persuade or influence people, especially language that sounds impressive but may not be sincere or honest.</p>
<p class='rw-defn idx18' style='display:none'>A sagacious person is wise, intelligent, and has the ability to make good practical decisions.</p>
<p class='rw-defn idx19' style='display:none'>A savant is a person who knows a lot about a subject, either via a lifetime of learning or by considerable natural ability.</p>
<p class='rw-defn idx20' style='display:none'>If someone is unlettered, they are illiterate, unable to read and write.</p>
<p class='rw-defn idx21' style='display:none'>Verbiage is an excessive use of words to convey something that could be expressed using fewer words; it can also be the manner or style in which someone uses words.</p>
<p class='rw-defn idx22' style='display:none'>A vernacular is the ordinary and everyday language spoken by people in a particular country or region that differs from the literary or standard written language of that area.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>jargon</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='jargon#' id='pronounce-sound' path='audio/words/amy-jargon'></a>
JAHR-gon
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='jargon#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='jargon#' id='context-sound' path='audio/wordcontexts/brian-jargon'></a>
Medical <em>jargon</em>, or language of the medical profession, can be difficult to understand.  When a doctor asks a patient if she has pain of the medial popliteal, this <em>jargon</em> or terminology is confusing.  Instead of using this <em>jargon</em> or specialist language, he could simply ask whether or not it hurts behind one&#8217;s knee.  Using everyday speech that everyone understands would make the question much clearer instead of resorting to confusing <em>jargon</em> that only physicians comprehend.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of professional <em>jargon</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
The words and phrases that lawyers use during a trial.
</li>
<li class='choice '>
<span class='result'></span>
The required exam that a person must pass to become a lawyer.
</li>
<li class='choice '>
<span class='result'></span>
The contract a lawyer signs when they are hired by a law firm.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='jargon#' id='definition-sound' path='audio/wordmeanings/amy-jargon'></a>
<em>Jargon</em> is language or terminology used by a specific group that might not be understandable to those people who are not of the group.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>special language</em>
</span>
</span>
</div>
<a class='quick-help' href='jargon#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='jargon#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/jargon/memory_hooks/5795.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Argon</span></span> Chemistry J<span class="emp3"><span>argon</span></span></span></span> <span class="emp3"><span>Argon</span></span> must be chemistry j<span class="emp3"><span>argon</span></span>, cause I don't know what it is.
</p>
</div>

<div id='memhook-button-bar'>
<a href="jargon#" id="add-public-hook" style="" url="https://membean.com/mywords/jargon/memory_hooks">Use other public hook</a>
<a href="jargon#" id="memhook-use-own" url="https://membean.com/mywords/jargon/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='jargon#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
<b>Jargon</b> allows us to camouflage intellectual poverty with verbal extravagance.
<span class='attribution'>&mdash; David Pratt, American businessman</span>
<img alt="David pratt, american businessman" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/David Pratt, American businessman.jpg?qdep8" width="80" />
</li>
<li>
Think hard about our objections to political <b>jargon</b> and you will be led to consider a fundamental modern political problem: What is an "informed" electorate in this age, and how does it get that way?
<cite class='attribution'>
&mdash;
PBS
</cite>
</li>
<li>
Actor Kevin Costner told Congress that his company has developed a high-tech machine for separating oil and water that could slurp up as much as 200 gallons of oil every minute from the massive spill in the Gulf. . . . Sounds great! How does it work? . . . Greg Lowry, an associate professor of civil and environmental engineering at Carnegie Mellon University, helped me translate the <b>jargon</b> into simpler English.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
If terms like ''administrivia'' and ''elephant hunt'' make you scratch your head, you are not alone. It sometimes seems that American businesses coin new <b>jargon</b> faster than anyone can keep up with. Dr. Folsom, a business professor at the University of South Carolina-Aiken, makes a study of business <b>jargon</b>, which he said appears to be becoming both more prevalent and more confusing.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/jargon/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='jargon#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>It&#8217;s possible that this word came about because it was similar to noises being made in the throat.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='jargon#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: TV Guide: Grey's Anatomy</strong><span> Does the cast of this medical TV show know medical jargon?</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/jargon.jpg' video_url='examplevids/jargon' video_width='350'></span>
<div id='wt-container'>
<img alt="Jargon" height="288" src="https://cdn1.membean.com/video/examplevids/jargon.jpg" width="350" />
<div class='center'>
<a href="jargon#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='jargon#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Jargon" src="https://cdn0.membean.com/public/images/wordimages/cons2/jargon.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='jargon#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>argot</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>cabal</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>cadre</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>clique</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>dialectic</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>erudite</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>idiosyncratic</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>insular</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>intelligentsia</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>interlocutor</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>lexicon</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>nomenclature</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>parlance</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>parochial</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>patois</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>rhetoric</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>sagacious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>savant</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>verbiage</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>vernacular</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='5' class = 'rw-wordform notlearned'><span>egalitarian</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>prosaic</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>unlettered</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="jargon" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>jargon</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="jargon#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

