
<!DOCTYPE html>
<html>
<head>
<title>Word: progeny | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Progeny-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/progeny-large.jpg?qdep8" />
</div>
<a href="progeny#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When you beget something, you cause it to happen or create it.</p>
<p class='rw-defn idx1' style='display:none'>If something engenders a particular feeling or attitude, it causes that condition.</p>
<p class='rw-defn idx2' style='display:none'>If someone, such as a writer, is described as having fecundity, they have the ability to produce many original and different ideas.</p>
<p class='rw-defn idx3' style='display:none'>Gestation is the process by which a new idea,  plan, or piece of work develops in your mind.</p>
<p class='rw-defn idx4' style='display:none'>A matriarch is an older and powerful woman who controls a family, community, or other social group.</p>
<p class='rw-defn idx5' style='display:none'>Something that is nascent is just starting to develop and is expected to become stronger and bigger in time.</p>
<p class='rw-defn idx6' style='display:none'>A patriarch is a male leader of a family or tribe; a patriarch can also be a man who is the founder of a group or organization.</p>
<p class='rw-defn idx7' style='display:none'>A pedigree is your list of ancestors or family tree; it can also be the lineage of a purebred animal.</p>
<p class='rw-defn idx8' style='display:none'>Your posterity are your children; they are also your future generations.</p>
<p class='rw-defn idx9' style='display:none'>A progenitor is someone&#8217;s ancestor; they can also be the originator of something.</p>
<p class='rw-defn idx10' style='display:none'>Something or someone that is prolific is highly fruitful and so produces a lot of something.</p>
<p class='rw-defn idx11' style='display:none'>A senescent person is becoming old and showing the effects of getting older.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>progeny</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='progeny#' id='pronounce-sound' path='audio/words/amy-progeny'></a>
PROJ-uh-nee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='progeny#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='progeny#' id='context-sound' path='audio/wordcontexts/brian-progeny'></a>
Giselle expected great things from her <em>progeny</em>, but these expectations she had of her children were hard to live up to.  Giselle made sure her <em>progeny</em> were taught several languages in their childhood.  She also expected her <em>progeny</em> to learn several musical instruments, which became a lot of work once her offspring started school.  All of this backfired when Giselle&#8217;s <em>progeny</em> rebelled against their mother&#8217;s ridiculous demands.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When would you be visiting your <em>progeny</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When you visit others you have mentored.
</li>
<li class='choice '>
<span class='result'></span>
When you go to see your grandparents.
</li>
<li class='choice answer '>
<span class='result'></span>
When you spend time with your kids.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='progeny#' id='definition-sound' path='audio/wordmeanings/amy-progeny'></a>
<em>Progeny</em> are children or descendants.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>offspring</em>
</span>
</span>
</div>
<a class='quick-help' href='progeny#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='progeny#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/progeny/memory_hooks/4906.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Roy</span></span>'s <span class="emp3"><span>Gen</span></span>es</span></span> <span class="emp1"><span>Roy</span></span>'s <span class="emp3"><span>gen</span></span>es were given to his p<span class="emp1"><span>ro</span></span><span class="emp3"><span>gen</span></span><span class="emp1"><span>y</span></span>, to whom, when they grew, he eventually also gave his jeans.
</p>
</div>

<div id='memhook-button-bar'>
<a href="progeny#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/progeny/memory_hooks">Use other hook</a>
<a href="progeny#" id="memhook-use-own" url="https://membean.com/mywords/progeny/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='progeny#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
This is probably why many parental units drop their <b>progeny</b> off at Comic-Con and when at home simply close the door and think calming golf thoughts.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
No matter how noble, a soldier who is also the father of a soldier must weigh parental pride against the fear that his <b>progeny</b> is heading into harm’s way.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
But Latin America's relationship with the Spanish motherland is considerably more complex and conflicted than the "special relationship" between Britain and the United States. Relations between Spain and its <b>progeny</b> have improved greatly in recent decades, as Spain's governments, banks and businesses have invested heavily, from Cuba to Patagonia.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/progeny/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='progeny#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pro_forward' data-tree-url='//cdn1.membean.com/public/data/treexml' href='progeny#'>
<span class='common'></span>
pro-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>forward, forth</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='gen_born' data-tree-url='//cdn1.membean.com/public/data/treexml' href='progeny#'>
<span class='common'></span>
gen
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>born, produced</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='y_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='progeny#'>
<span class=''></span>
-y
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>body, group, collection</td>
</tr>
</table>
<p><em>Progeny</em> are &#8220;brought forth&#8221; or &#8220;produced&#8221; by their parents.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='progeny#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>The Brady Bunch</strong><span> Carol and Mike and their respective progeny.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/progeny.jpg' video_url='examplevids/progeny' video_width='350'></span>
<div id='wt-container'>
<img alt="Progeny" height="288" src="https://cdn1.membean.com/video/examplevids/progeny.jpg" width="350" />
<div class='center'>
<a href="progeny#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='progeny#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Progeny" src="https://cdn1.membean.com/public/images/wordimages/cons2/progeny.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='progeny#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>beget</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>engender</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>fecundity</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>gestation</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>nascent</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>pedigree</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>posterity</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>prolific</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='4' class = 'rw-wordform notlearned'><span>matriarch</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>patriarch</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>progenitor</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>senescent</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='progeny#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
progenitor
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>ancestor; originator of something</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="progeny" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>progeny</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="progeny#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

