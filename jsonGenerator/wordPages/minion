
<!DOCTYPE html>
<html>
<head>
<title>Word: minion | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An acolyte is someone who serves a leader as a devoted assistant or believes in the leader&#8217;s ideas.</p>
<p class='rw-defn idx1' style='display:none'>An adherent is a supporter or follower of a leader or a cause.</p>
<p class='rw-defn idx2' style='display:none'>An autonomous person makes their own decisions without being influenced by anyone else; an autonomous country or organization is independent and has the power to govern itself.</p>
<p class='rw-defn idx3' style='display:none'>To conscript someone is to force them into military service.</p>
<p class='rw-defn idx4' style='display:none'>Someone who is contumacious is purposely stubborn, contrary, or disobedient.</p>
<p class='rw-defn idx5' style='display:none'>If you behave with deference towards someone, you show them respect and accept their opinions or decisions, especially because they have an important position.</p>
<p class='rw-defn idx6' style='display:none'>A dissident is someone who disagrees publicly with a government, especially in a country where this is not allowed.</p>
<p class='rw-defn idx7' style='display:none'>If someone is fractious, they are easily upset or annoyed over unimportant things.</p>
<p class='rw-defn idx8' style='display:none'>Someone who is incorrigible has bad habits or does bad things and is unlikely to ever change; this word is often used in a humorous way.</p>
<p class='rw-defn idx9' style='display:none'>A lackey is a person who follows their superior&#8217;s orders so completely that they never openly question those commands.</p>
<p class='rw-defn idx10' style='display:none'>If someone is being obsequious, they are trying so hard to please someone that they lack sincerity in their actions towards that person.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is obstreperous is noisy, unruly, and difficult to control.</p>
<p class='rw-defn idx12' style='display:none'>A perfunctory action is done with little care or interest; consequently, it is only completed because it is expected of someone to do so.</p>
<p class='rw-defn idx13' style='display:none'>A recalcitrant animal or person is difficult to control and refuses to obey orders—even after stiff punishment.</p>
<p class='rw-defn idx14' style='display:none'>Refractory people deliberately don&#8217;t obey someone in authority and so are difficult to deal with or control.</p>
<p class='rw-defn idx15' style='display:none'>A restive person is not willing or able to keep still or be patient because they are bored or dissatisfied with something; consequently, they are becoming difficult to control.</p>
<p class='rw-defn idx16' style='display:none'>If you say that someone is servile, you don&#8217;t respect them because they are too obedient, too agreeable, and too willing to do anything for another person.</p>
<p class='rw-defn idx17' style='display:none'>If you are subservient, you are too eager and willing to do what other people want and often put your own wishes aside.</p>
<p class='rw-defn idx18' style='display:none'>Sycophants praise people in authority or those who have considerable influence in order to seek some favor from them in return.</p>
<p class='rw-defn idx19' style='display:none'>An unctuous person acts in an overly deceptive manner that is obviously insincere because they want to convince you of something.</p>
<p class='rw-defn idx20' style='display:none'>In the Middle Ages, a vassal was a worker who served a lord in exchange for land to live upon; today you are a vassal if you serve another and are largely controlled by them.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>minion</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='minion#' id='pronounce-sound' path='audio/words/amy-minion'></a>
MIN-yuhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='minion#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='minion#' id='context-sound' path='audio/wordcontexts/brian-minion'></a>
As he reviewed his evil plan to take over the world, Darth Onion realized that he needed an assistant or <em>minion</em> to support and carry out his sneaky deeds.  &#8220;A good servant or obedient <em>minion</em> is just so hard to find these days,&#8221; the villain complained.  After placing an ad in The Evil Times, Darth Onion held midnight interviews for the position of a new <em>minion</em>, aid, or helper.  The mumbling, bowing, eager Shallot got the job.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>minion</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Someone who joins a group of like-minded individuals.
</li>
<li class='choice '>
<span class='result'></span>
Someone who intends to do evil in the world.
</li>
<li class='choice answer '>
<span class='result'></span>
Someone who does all that their superior commands.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='minion#' id='definition-sound' path='audio/wordmeanings/amy-minion'></a>
A <em>minion</em> is an unimportant person who merely obeys the orders of someone else; they  usually perform boring tasks that require little skill.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>servant</em>
</span>
</span>
</div>
<a class='quick-help' href='minion#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='minion#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/minion/memory_hooks/4226.json'></span>
<p>
<span class="emp0"><span>Filet <span class="emp1"><span>Mi</span></span>g<span class="emp1"><span>non</span></span>?</span></span> Give my <span class="emp1"><span>min</span></span>i<span class="emp1"><span>on</span></span> a filet <span class="emp1"><span>mi</span></span>g<span class="emp1"><span>non</span></span>, the choicest cut of steak?  You must be joking.
</p>
</div>

<div id='memhook-button-bar'>
<a href="minion#" id="add-public-hook" style="" url="https://membean.com/mywords/minion/memory_hooks">Use other public hook</a>
<a href="minion#" id="memhook-use-own" url="https://membean.com/mywords/minion/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='minion#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The <b>Minions</b>, having been the best part of the two previous _Despicable Me_ movies, have swarmed the screen in _<b>Minions_</b>. As candidates for center stage, they are seemingly ill-suited. Slavishly—if rarely competently—devoted lackeys, they're underlings by both definition and verticality.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
Another <b>minion</b> was serving him food, including a plate of cantaloupe and pineapple, a serving of rice and a strawberry Popsicle, all of which Woods scarfed down as he floated from one function to the next.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/minion/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='minion#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>From a root word meaning &#8220;a favorite.&#8221;  Another, lesser used meaning of the word <em>minion</em> is a &#8220;favored person.&#8221;</p>
<a href="minion#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep6" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> French</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='minion#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>12 Monkeys</strong><span> Goines gets excited about having minions to contact his father.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/minion.jpg' video_url='examplevids/minion' video_width='350'></span>
<div id='wt-container'>
<img alt="Minion" height="198" src="https://cdn1.membean.com/video/examplevids/minion.jpg" width="350" />
<div class='center'>
<a href="minion#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='minion#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Minion" src="https://cdn1.membean.com/public/images/wordimages/cons2/minion.png?qdep6" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='minion#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>acolyte</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>adherent</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>conscript</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>deference</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>lackey</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>obsequious</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>perfunctory</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>servile</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>subservient</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>sycophant</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>unctuous</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>vassal</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>autonomous</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>contumacious</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>dissident</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>fractious</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>incorrigible</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>obstreperous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>recalcitrant</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>refractory</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>restive</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="minion" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>minion</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="minion#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

