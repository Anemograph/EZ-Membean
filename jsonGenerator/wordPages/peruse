
<!DOCTYPE html>
<html>
<head>
<title>Word: peruse | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Acumen is the ability to make quick decisions and keen, precise judgments.</p>
<p class='rw-defn idx1' style='display:none'>An assiduous person works hard to ensure that something is done properly and completely.</p>
<p class='rw-defn idx2' style='display:none'>A cursory examination or reading of something is very quick, incomplete, and does not pay close attention to detail.</p>
<p class='rw-defn idx3' style='display:none'>Something that is desultory is done in a way that is unplanned, disorganized, and without direction.</p>
<p class='rw-defn idx4' style='display:none'>When you discern something, you notice, detect, or understand it, often after thinking about it carefully or studying it for some time.  </p>
<p class='rw-defn idx5' style='display:none'>Someone who is erudite is steeped in knowledge because they have read and studied extensively.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is exacting expects others to work very hard and carefully.</p>
<p class='rw-defn idx7' style='display:none'>When a person is being flippant, they are not taking something as seriously as they should be; therefore, they tend to dismiss things they should respectfully pay attention to.</p>
<p class='rw-defn idx8' style='display:none'>If an idea or thought is incisive, it is expressed in a penetrating and knowledgeable manner that is clear and brief; additionally, it can demonstrate impressive understanding of related ideas or thoughts.</p>
<p class='rw-defn idx9' style='display:none'>Something inscrutable is very hard to figure out, discover, or understand what it is all about.</p>
<p class='rw-defn idx10' style='display:none'>If you do something in a lackadaisical way, you do it carelessly and without putting much effort into it—thereby showing that you are not really interested in what you&#8217;re doing.</p>
<p class='rw-defn idx11' style='display:none'>Someone is considered meticulous when they act with careful attention to detail.</p>
<p class='rw-defn idx12' style='display:none'>A missive is a written letter, especially a formal, legal, or important one.</p>
<p class='rw-defn idx13' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx14' style='display:none'>If someone is pedantic, they give too much importance to unimportant details and formal rules.</p>
<p class='rw-defn idx15' style='display:none'>A perfunctory action is done with little care or interest; consequently, it is only completed because it is expected of someone to do so.</p>
<p class='rw-defn idx16' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx17' style='display:none'>Someone who is pertinacious is determined to continue doing something rather than giving up—even when it gets very difficult.</p>
<p class='rw-defn idx18' style='display:none'>To be punctilious is to pay precise attention to detail.</p>
<p class='rw-defn idx19' style='display:none'>A scrupulous person takes great care to do everything in an extremely honest, conscientious, and fair manner.</p>
<p class='rw-defn idx20' style='display:none'>Someone who is sedulous works hard and performs tasks very carefully and thoroughly, not stopping their work until it is completely accomplished.</p>
<p class='rw-defn idx21' style='display:none'>If someone is unlettered, they are illiterate, unable to read and write.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>peruse</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='peruse#' id='pronounce-sound' path='audio/words/amy-peruse'></a>
puh-ROOZ
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='peruse#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='peruse#' id='context-sound' path='audio/wordcontexts/brian-peruse'></a>
I have often dreamed of the opportunity to <em>peruse</em> or carefully examine one of Shakespeare&#8217;s lost plays.  Imagine going through a bookstore and coming upon the lost play <em>Cardenio</em>; I would sit right down and carefully <em>peruse</em> it before I revealed it to the public.  What an amazing thing it would be to be able to <em>peruse</em> or study literature that hasn&#8217;t been seen for 400 years!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of <em>perusing</em> a magazine?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A woman reading every article in it with intense interest.
</li>
<li class='choice '>
<span class='result'></span>
A teen texting everyone they know to see who has a copy of it.
</li>
<li class='choice '>
<span class='result'></span>
A team of writers who decide they can publish it on their own.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='peruse#' id='definition-sound' path='audio/wordmeanings/amy-peruse'></a>
If you <em>peruse</em> some written text, you read it over carefully.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>read thoroughly</em>
</span>
</span>
</div>
<a class='quick-help' href='peruse#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='peruse#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/peruse/memory_hooks/4799.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Peru</span></span>k<span class="emp3"><span>e</span></span> <span class="emp3"><span>Peru</span></span>sal</span></span> A <span class="emp3"><span>peru</span></span>k<span class="emp3"><span>e</span></span> is a type of wig for men.  One day I decided that I really wanted to wear a <span class="emp3"><span>peru</span></span>k<span class="emp3"><span>e</span></span>, so I got a <span class="emp3"><span>peru</span></span>k<span class="emp3"><span>e</span></span> catalog and <span class="emp3"><span>peru</span></span>s<span class="emp3"><span>e</span></span>d it until I found the <span class="emp3"><span>per</span></span>f<span class="emp3"><span>e</span></span>ct <span class="emp3"><span>peru</span></span>k<span class="emp3"><span>e</span></span>, which I couldn't have done unless I had <span class="emp3"><span>peru</span></span>s<span class="emp3"><span>e</span></span>d the <span class="emp3"><span>peru</span></span>k<span class="emp3"><span>e</span></span> catalog very well.
</p>
</div>

<div id='memhook-button-bar'>
<a href="peruse#" id="add-public-hook" style="" url="https://membean.com/mywords/peruse/memory_hooks">Use other public hook</a>
<a href="peruse#" id="memhook-use-own" url="https://membean.com/mywords/peruse/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='peruse#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Cooks more interested in the atmospheric than the authentic can <b>peruse</b> a selection of _Modern Recipes for Beginners_, which includes such dishes as Honey Cakes and Beef Barley Soup.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
The biggest and most crucial expenditure for a new label is creating the sample garments and accessories that buyers from boutiques and bigger chains <b>peruse</b> before placing orders. These samples, which serve as the template for a collection, are displayed in the showroom and are featured in the label's "look book" catalogue.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/peruse/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='peruse#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='per_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='peruse#'>
<span class='common'></span>
per-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='us_use' data-tree-url='//cdn1.membean.com/public/data/treexml' href='peruse#'>
<span class=''></span>
us
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>use</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='peruse#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>To <em>peruse</em> a book is to &#8220;thoroughly use&#8221; it by carefully reading every last word of its text.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='peruse#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>The Fellowship of the Ring</strong><span> Gandalf perusing ancient writings.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/peruse.jpg' video_url='examplevids/peruse' video_width='350'></span>
<div id='wt-container'>
<img alt="Peruse" height="288" src="https://cdn1.membean.com/video/examplevids/peruse.jpg" width="350" />
<div class='center'>
<a href="peruse#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='peruse#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Peruse" src="https://cdn1.membean.com/public/images/wordimages/cons2/peruse.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='peruse#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>acumen</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>assiduous</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>discern</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>erudite</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>exacting</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>incisive</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>meticulous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>missive</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>pedantic</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>pertinacious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>punctilious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>scrupulous</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>sedulous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>cursory</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>desultory</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>flippant</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>inscrutable</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>lackadaisical</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>perfunctory</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>unlettered</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='peruse#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
perusal
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>a thorough reading</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="peruse" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>peruse</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="peruse#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

