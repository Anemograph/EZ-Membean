
<!DOCTYPE html>
<html>
<head>
<title>Word: compulsion | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>When you accede to a demand or proposal, you agree to it, especially after first disagreeing with it.</p>
<p class='rw-defn idx2' style='display:none'>When something bedevils you, it causes you a lot of problems and difficulties over a period of time.</p>
<p class='rw-defn idx3' style='display:none'>When you beleaguer someone, you act with the intent to annoy or harass that person repeatedly until they finally give you what you want.</p>
<p class='rw-defn idx4' style='display:none'>If you are beset by something, you are experiencing serious problems or dangers because of it.</p>
<p class='rw-defn idx5' style='display:none'>When you buttress an argument, idea, or even a building, you make it stronger by providing support.</p>
<p class='rw-defn idx6' style='display:none'>When you capitulate, you accept or agree to do something after having resisted doing so for a long time.</p>
<p class='rw-defn idx7' style='display:none'>If you circumvent something, such as a rule or restriction, you try to get around it in a clever and perhaps dishonest way.</p>
<p class='rw-defn idx8' style='display:none'>You coerce people when you force them to do something that they don&#8217;t want to do.</p>
<p class='rw-defn idx9' style='display:none'>When something is commandeered, it is taken or seized, usually by force.</p>
<p class='rw-defn idx10' style='display:none'>When you are constrained, you are forced to do something or are kept from doing it.</p>
<p class='rw-defn idx11' style='display:none'>To contravene a law, rule, or agreement is to do something that is not allowed or is forbidden by that law, rule, or agreement.</p>
<p class='rw-defn idx12' style='display:none'>If something daunts you, it makes you worried about dealing with it because you think it will be very difficult or dangerous.</p>
<p class='rw-defn idx13' style='display:none'>If something discomfits you, it makes you feel embarrassed, confused, uncomfortable, or frustrated.</p>
<p class='rw-defn idx14' style='display:none'>If something disconcerts you, it makes you feel anxious, worried, or confused.</p>
<p class='rw-defn idx15' style='display:none'>When someone feels disquiet about a situation, they feel very worried or nervous.</p>
<p class='rw-defn idx16' style='display:none'>Duress is the application or threat of force to compel someone to act in a particular way.</p>
<p class='rw-defn idx17' style='display:none'>If a fact or idea eludes you, you cannot remember or understand it; if you elude someone, you manage to escape or hide from them.</p>
<p class='rw-defn idx18' style='display:none'>If something encumbers you, it makes it difficult for you to move freely or do what you want.</p>
<p class='rw-defn idx19' style='display:none'>If something fetters you, it restricts you from doing something you want to do.</p>
<p class='rw-defn idx20' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx21' style='display:none'>If someone is imperturbable, they are always calm and not easily upset or disturbed by any situation, even dangerous ones.</p>
<p class='rw-defn idx22' style='display:none'>If you are impervious to things, such as someone&#8217;s actions or words, you are not affected by them or do not notice them.</p>
<p class='rw-defn idx23' style='display:none'>The adjective inexorable describes a process that is impossible to stop once it has begun.</p>
<p class='rw-defn idx24' style='display:none'>An irrevocable action or decision is impossible to change, reverse, or stop.</p>
<p class='rw-defn idx25' style='display:none'>A juggernaut is a very powerful force, organization, or group whose influence cannot be stopped; its strength and power can overwhelm or crush any competition or anything else that stands in its way.</p>
<p class='rw-defn idx26' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx27' style='display:none'>To parry is to ward something off or deflect it.</p>
<p class='rw-defn idx28' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx29' style='display:none'>If you repudiate something, you state that you do not accept or agree with it and no longer want to be connected with it in any way.</p>
<p class='rw-defn idx30' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx31' style='display:none'>A stoic person does not show their emotions and does not complain when bad things happen to them.</p>
<p class='rw-defn idx32' style='display:none'>Something that stymies you presents an obstacle that prevents you from doing what you need or want to do.</p>
<p class='rw-defn idx33' style='display:none'>If someone subjugates a group of people or country, they conquer and bring it under control by force.</p>
<p class='rw-defn idx34' style='display:none'>When you trammel something or someone, you bring about limits to freedom or hinder movements or activities in some fashion.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>compulsion</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='compulsion#' id='pronounce-sound' path='audio/words/amy-compulsion'></a>
kuhm-PUHL-shun
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='compulsion#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='compulsion#' id='context-sound' path='audio/wordcontexts/brian-compulsion'></a>
Maya Angelou frequently feels a <em>compulsion</em> or inner drive to break out and speak in poetic verse, which she cannot resist doing.  Sometimes a scenic view will bring about this <em>compulsion</em>, and she is forced to recite a poem that reminds her of the beauty she has seen.  Mostly it is an overwhelming emotion that brings out this <em>compulsion</em> or necessity, and she may speak in verse through tears as she honors the feeling within.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>compulsion</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is being responsible for someone else&#8217;s safety.
</li>
<li class='choice '>
<span class='result'></span>
It is a feeling of accomplishment or pride.
</li>
<li class='choice answer '>
<span class='result'></span>
It is a pressure or urge to do something.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='compulsion#' id='definition-sound' path='audio/wordmeanings/amy-compulsion'></a>
If you feel a <em>compulsion</em> to do something, you feel like you must do it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>force</em>
</span>
</span>
</div>
<a class='quick-help' href='compulsion#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='compulsion#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/compulsion/memory_hooks/4941.json'></span>
<p>
<span class="emp0"><span>Pro<span class="emp1"><span>pulsion</span></span></span></span> A com<span class="emp1"><span>pulsion</span></span> is an inner pro<span class="emp1"><span>pulsion</span></span> to do something.
</p>
</div>

<div id='memhook-button-bar'>
<a href="compulsion#" id="add-public-hook" style="" url="https://membean.com/mywords/compulsion/memory_hooks">Use other public hook</a>
<a href="compulsion#" id="memhook-use-own" url="https://membean.com/mywords/compulsion/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='compulsion#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
In the Internet Age, more and more companies live by the mantra "create an obsession, then exploit it." Gaming companies talk openly about creating a "<b>compulsion</b> loop," which works roughly as follows: the player plays the game; the player achieves the goal; the player is awarded new content; which causes the player to want to continue playing with the new content and re-enter the loop.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
Each day the 43-year-old recluse piled the new with the old, until floor-to-ceiling stacks of disorganized paper nearly filled his windowless 10-by-10-foot apartment in New York City. . . . That <b>compulsion</b>, scientists now theorize, is a natural and adaptive instinct gone amok.
<cite class='attribution'>
&mdash;
Discover Magazine
</cite>
</li>
<li>
Allen noted that she has always had an interest in World War II history, ever since she read _The Hiding Place_ in school, which is about Corrie ten Boom, a Dutch woman who hid Jewish people during the Holocaust. “It’s just something that has been inside of me for a long time,” Allen said. “I have had a strong <b>compulsion</b> to try to understand how a group of people could be responsible for World War II.”
<cite class='attribution'>
&mdash;
Houston Chronicle
</cite>
</li>
<li>
Sometimes creativity is a <b>compulsion</b>, not an ambition.
<cite class='attribution'>
&mdash;
Edward Norton, American actor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/compulsion/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='compulsion#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='com_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='compulsion#'>
<span class='common'></span>
com-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='puls_pushed' data-tree-url='//cdn1.membean.com/public/data/treexml' href='compulsion#'>
<span class=''></span>
puls
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>pushed</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ion_act' data-tree-url='//cdn1.membean.com/public/data/treexml' href='compulsion#'>
<span class=''></span>
-ion
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>act, state, or result of doing something</td>
</tr>
</table>
<p>One who suffers from a <em>compulsion</em> is &#8220;thoroughly pushed&#8221; into doing something.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='compulsion#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>MONK</strong><span> This guy has an extreme compulsion.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/compulsion.jpg' video_url='examplevids/compulsion' video_width='350'></span>
<div id='wt-container'>
<img alt="Compulsion" height="288" src="https://cdn1.membean.com/video/examplevids/compulsion.jpg" width="350" />
<div class='center'>
<a href="compulsion#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='compulsion#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Compulsion" src="https://cdn1.membean.com/public/images/wordimages/cons2/compulsion.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='compulsion#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>accede</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bedevil</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>beleaguer</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>beset</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>capitulate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>coerce</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>commandeer</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>constrain</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>daunt</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>discomfit</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>disconcert</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>disquiet</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>duress</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>encumber</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>fetter</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>inexorable</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>irrevocable</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>juggernaut</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>subjugate</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>trammel</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>buttress</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>circumvent</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>contravene</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>elude</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>imperturbable</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>impervious</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>parry</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>repudiate</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>stoic</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>stymie</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='compulsion#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
compulsive
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>irresistible; overwhelming</td>
</tr>
<tr>
<td class='wordform'>
<span>
compel
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to exert a strong force on</td>
</tr>
<tr>
<td class='wordform'>
<span>
compulsory
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>required; necessary</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="compulsion" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>compulsion</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="compulsion#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

