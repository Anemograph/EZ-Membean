
<!DOCTYPE html>
<html>
<head>
<title>Word: heinous | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abhor something, you dislike it very much, usually because you think it&#8217;s immoral.</p>
<p class='rw-defn idx1' style='display:none'>The word abject emphasizes a very bad situation or quality, thereby making it even worse.</p>
<p class='rw-defn idx2' style='display:none'>If you abominate something, you hate it because you think it is extremely wrong and unacceptable.</p>
<p class='rw-defn idx3' style='display:none'>A benefaction is a charitable contribution of money or assistance that someone gives to a person or organization.</p>
<p class='rw-defn idx4' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx5' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx6' style='display:none'>Depravity is behavior that is immoral, corrupt, or evil.</p>
<p class='rw-defn idx7' style='display:none'>If you say a person&#8217;s actions are despicable, you think they are extremely unpleasant or nasty.</p>
<p class='rw-defn idx8' style='display:none'>If you say something is diabolical, you are emphasizing that it is evil, cruel, or very bad.</p>
<p class='rw-defn idx9' style='display:none'>An egregious mistake, failure, or problem is an extremely bad and very noticeable one.</p>
<p class='rw-defn idx10' style='display:none'>When you execrate someone, you curse them to show your intense dislike or hatred of them.</p>
<p class='rw-defn idx11' style='display:none'>An action that is flagrant shows that someone does not care if they obviously break the rules or highly offend people.</p>
<p class='rw-defn idx12' style='display:none'>Something that is inconspicuous does not attract attention and is not easily seen or noticed because it is small or ordinary.</p>
<p class='rw-defn idx13' style='display:none'>An infamous person has a very bad reputation because they have done disgraceful or dishonorable things.</p>
<p class='rw-defn idx14' style='display:none'>Iniquity is an immoral act, wickedness, or evil.</p>
<p class='rw-defn idx15' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx16' style='display:none'>Something loathsome is offensive, disgusting, and brings about intense dislike.</p>
<p class='rw-defn idx17' style='display:none'>Something macabre is so gruesome or frightening that it causes great horror in those who see it.</p>
<p class='rw-defn idx18' style='display:none'>A malevolent person or thing is evil due to deliberate attempts to cause harm.</p>
<p class='rw-defn idx19' style='display:none'>Malfeasance is an unlawful act, especially one committed by a trusted public official.</p>
<p class='rw-defn idx20' style='display:none'>Malice is a type of hatred that causes a strong desire to harm others both physically and emotionally.</p>
<p class='rw-defn idx21' style='display:none'>A nefarious activity is considered evil and highly dishonest.</p>
<p class='rw-defn idx22' style='display:none'>If something is noisome, it is extremely unpleasant, especially because it is very dirty or has an awful smell.</p>
<p class='rw-defn idx23' style='display:none'>Something that is noxious is harmful, extremely unpleasant, and often poisonous.</p>
<p class='rw-defn idx24' style='display:none'>If you describe people or things as odious, you think that they are extremely unpleasant.</p>
<p class='rw-defn idx25' style='display:none'>Something that is pernicious is very harmful or evil, often in a way that is hidden or not quickly noticed.</p>
<p class='rw-defn idx26' style='display:none'>Philanthropy is unselfish support in the form of donating money, work, or gifts to positive social purposes; philanthropy is also overall love for humans in general.</p>
<p class='rw-defn idx27' style='display:none'>Something that is propitious shows favorable conditions.</p>
<p class='rw-defn idx28' style='display:none'>If you think a type of behavior or idea is reprehensible, you think that it is very bad, morally wrong, and deserves to be strongly criticized.</p>
<p class='rw-defn idx29' style='display:none'>Repugnance is a strong feeling of dislike for something or someone you find horrible and offensive.</p>
<p class='rw-defn idx30' style='display:none'>Something that is repulsive is offensive, highly unpleasant, or just plain disgusting.</p>
<p class='rw-defn idx31' style='display:none'>A salutary experience is beneficial to you since it strengthens you in some way, although it may be unpleasant as you undergo it; this word also refers to promoting good health.</p>
<p class='rw-defn idx32' style='display:none'>The sanctity of something is its holiness or sacred quality.</p>
<p class='rw-defn idx33' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx34' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>
<p class='rw-defn idx35' style='display:none'>An unsightly person presents an ugly, unattractive, or disagreeable face to the world.</p>
<p class='rw-defn idx36' style='display:none'>A vicious person is corrupt, violent, aggressive, and/or highly immoral.</p>
<p class='rw-defn idx37' style='display:none'>Something vile is evil, very unpleasant, horrible, or extremely bad.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>heinous</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='heinous#' id='pronounce-sound' path='audio/words/amy-heinous'></a>
HAY-nuhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='heinous#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='heinous#' id='context-sound' path='audio/wordcontexts/brian-heinous'></a>
Chief medical examiner Clarins was called to the morgue to investigate the tragic results of a shocking, hateful, <em>heinous</em> crime.  A young man had been left to die painfully in the bitter cold, an unspeakably tragic and <em>heinous</em> event that caused a huge stir in the small town.  Dr. Clarins was eventually able to provide evidence that helped solve the baffling case and punish those who committed the evil, unthinkable, and <em>heinous</em> deed.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a likely consequence of <em>heinous</em> behavior?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A warning not to make a similar mistake again.
</li>
<li class='choice '>
<span class='result'></span>
A widespread show of public support.
</li>
<li class='choice answer '>
<span class='result'></span>
A severe punishment and public anger.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='heinous#' id='definition-sound' path='audio/wordmeanings/amy-heinous'></a>
If you describe something as <em>heinous</em>, you mean that is extremely evil, shocking, and bad.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>horrifying</em>
</span>
</span>
</div>
<a class='quick-help' href='heinous#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='heinous#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/heinous/memory_hooks/5237.json'></span>
<p>
<span class="emp0"><span>Sam<span class="emp1"><span>hain</span></span> <span class="emp1"><span>Hein</span></span>ous?</span></span> The ancient Celtic festival of the dead, or Sam<span class="emp1"><span>hain</span></span>, which is today combined in All Saints' Day and Halloween, was believed by some scholars to have included <span class="emp1"><span>hein</span></span>ous sacrifices of living humans to satisfy the Celtic god Saman, the Lord of the Dead.
</p>
</div>

<div id='memhook-button-bar'>
<a href="heinous#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/heinous/memory_hooks">Use other hook</a>
<a href="heinous#" id="memhook-use-own" url="https://membean.com/mywords/heinous/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='heinous#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
It was on Maya Angelou’s birthday, April 4, 1968, that her friend, civil rights activist Martin Luther King, Jr., was assassinated. After this <b>heinous</b> act, Angelou stopped celebrating her birthday. However, she sent flowers to King’s wife Coretta Scott King on that date until Mrs. King passed in 2006.
<cite class='attribution'>
&mdash;
Reader’s Digest
</cite>
</li>
<li>
“This memorial is sacred to the Charleston Fire Department, is sacred to the service, but it’s an important part of our community. And to think that somebody would deface this memorial is absolutely <b>heinous</b>. It’s disgusting. And on behalf of the Charleston Fire Department. I deplore the act.”
<cite class='attribution'>
&mdash;
WECT News
</cite>
</li>
<li>
"For more than a year, the Asian American community has been fighting two viruses, the COVID-19 pandemic and anti-Asian hate," [Rep. Grace] Meng told reporters . . . "We've heard about and seen videos of both young and elderly Asian Americans being shoved to the ground, stomped on, being spat on and shunned. These <b>heinous</b> acts have been outrageous, unconscionable and they must end."
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
“Voter suppression is one of the oldest and most <b>heinous</b> forms of racism in our institutions,” Jones said in a news release.
<cite class='attribution'>
&mdash;
13News Now
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/heinous/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='heinous#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ous_full' data-tree-url='//cdn1.membean.com/public/data/treexml' href='heinous#'>
<span class=''></span>
-ous
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>full of, having</td>
</tr>
</table>
<p>From a root word meaning &#8220;to hate&#8221;:  A <em>heinous</em> crime is surely something to be &#8220;hated.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='heinous#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Heinous" src="https://cdn3.membean.com/public/images/wordimages/cons2/heinous.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='heinous#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abhor</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abject</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abominate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>depravity</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>despicable</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>diabolical</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>egregious</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>execrate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>flagrant</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>infamous</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>iniquity</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>loathsome</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>macabre</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>malevolent</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>malfeasance</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>malice</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>nefarious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>noisome</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>noxious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>odious</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>pernicious</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>reprehensible</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>repugnance</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>repulsive</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>unsightly</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>vicious</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>vile</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>benefaction</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>inconspicuous</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>philanthropy</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>propitious</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>salutary</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>sanctity</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="heinous" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>heinous</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="heinous#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

