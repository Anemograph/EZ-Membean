
<!DOCTYPE html>
<html>
<head>
<title>Word: entourage | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Entourage-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/entourage-large.jpg?qdep8" />
</div>
<a href="entourage#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>An adjunct is something that is added to or joined to something else that is larger or more important.</p>
<p class='rw-defn idx1' style='display:none'>When you advocate a plan or action, you publicly push for implementing it.</p>
<p class='rw-defn idx2' style='display:none'>Ambient sound, light, and temperature are those parts of the environment that surround you.</p>
<p class='rw-defn idx3' style='display:none'>When something is ancillary to something else, such as a workbook to a textbook, it supports it but is less important than that which it supports.</p>
<p class='rw-defn idx4' style='display:none'>An autonomous person makes their own decisions without being influenced by anyone else; an autonomous country or organization is independent and has the power to govern itself.</p>
<p class='rw-defn idx5' style='display:none'>Something is concomitant when it happens at the same time as something else and is connected with it in some fashion.</p>
<p class='rw-defn idx6' style='display:none'>A coterie is a small group of people who are close friends; therefore, when its members do things together, they do not want other people to join them.</p>
<p class='rw-defn idx7' style='display:none'>A cynosure is an object that serves as the center of attention.</p>
<p class='rw-defn idx8' style='display:none'>A luminary is someone who is much admired in a particular profession because they are an accomplished expert in their field.</p>
<p class='rw-defn idx9' style='display:none'>Your milieu includes the things and people that surround you and influence the way in which you behave.</p>
<p class='rw-defn idx10' style='display:none'>A paragon is an example of a thing or person at its or their very best.</p>
<p class='rw-defn idx11' style='display:none'>A recluse is someone who chooses to live alone and deliberately avoids other people.</p>
<p class='rw-defn idx12' style='display:none'>A retinue is the group of people, such as friends or servants, who travel with someone important to help and support that person.</p>
<p class='rw-defn idx13' style='display:none'>If you say that someone is servile, you don&#8217;t respect them because they are too obedient, too agreeable, and too willing to do anything for another person.</p>
<p class='rw-defn idx14' style='display:none'>If you are subservient, you are too eager and willing to do what other people want and often put your own wishes aside.</p>
<p class='rw-defn idx15' style='display:none'>Sycophants praise people in authority or those who have considerable influence in order to seek some favor from them in return.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>entourage</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='entourage#' id='pronounce-sound' path='audio/words/amy-entourage'></a>
ahn-too-RAHZH
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='entourage#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='entourage#' id='context-sound' path='audio/wordcontexts/brian-entourage'></a>
It has become the fashion in Hollywood for celebrities to be surrounded by an <em>entourage</em>, or group of supportive attendants.  This energetic <em>entourage</em> or body of followers can be partly made up of childhood friends who are given simple jobs.  The function of the surrounding companions or <em>entourage</em> is to keep the star nourished, taken from place to place, protected, and entertained.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an <em>entourage</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
People who follow and assist someone important. 
</li>
<li class='choice '>
<span class='result'></span>
A fan club that obsessively follows a famous person.
</li>
<li class='choice '>
<span class='result'></span>
The equipment someone requires to perform.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='entourage#' id='definition-sound' path='audio/wordmeanings/amy-entourage'></a>
An <em>entourage</em> is a group of assistants, servants, and other people who tag along with an important person.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>followers</em>
</span>
</span>
</div>
<a class='quick-help' href='entourage#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='entourage#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/entourage/memory_hooks/5794.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Tour</span></span> <span class="emp2"><span>Age</span></span> T<span class="emp3"><span>en</span></span></span></span> To be part of the traveling <span class="emp3"><span>en</span></span><span class="emp1"><span>tour</span></span><span class="emp2"><span>age</span></span>, your <span class="emp2"><span>age</span></span> must be t<span class="emp3"><span>en</span></span> at the time of the <span class="emp1"><span>tour</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="entourage#" id="add-public-hook" style="" url="https://membean.com/mywords/entourage/memory_hooks">Use other public hook</a>
<a href="entourage#" id="memhook-use-own" url="https://membean.com/mywords/entourage/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='entourage#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Even going out to get milk becomes a little bit challenging, just because there is a whole <b>entourage</b> that then travels with me for this simple thing. So I tend to try and find ways not to inconvenience a whole raft of other people, so it changes my mindset a little bit.
<span class='attribution'>&mdash; Jacinda Ardern, prime minister of New Zealand</span>
<img alt="Jacinda ardern, prime minister of new zealand" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Jacinda Ardern, prime minister of New Zealand.jpg?qdep8" width="80" />
</li>
<li>
The singer, who is in her early 20s, said the band had to take care of everything—outfits, dance training, hair and makeup—by themselves, unlike more popular K-pop acts that have entire <b>entourages</b> of staff members.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
Princess Maxima of the Netherlands at the W Hotel Saturday night. . . . She was the one who looked exactly like a princess—golden gown, security detail, an <b>entourage</b> of diplomats—who lingered on the hotel’s festive rooftop until midnight.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Wolsey orchestrated the 1520 summit much like he’d spearheaded the treaty of 1518, overseeing the complex web of preparations needed to transport, feed, house and entertain an English <b>entourage</b> of more than 5,000.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/entourage/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='entourage#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='en_in' data-tree-url='//cdn1.membean.com/public/data/treexml' href='entourage#'>
<span class='common'></span>
en-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>in, on</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='tour_circle' data-tree-url='//cdn1.membean.com/public/data/treexml' href='entourage#'>
<span class=''></span>
tour
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>circle</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='age_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='entourage#'>
<span class=''></span>
-age
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state</td>
</tr>
</table>
<p>An <em>entourage</em> is in the &#8220;state of&#8221; being &#8220;in the inner circle&#8221; of a celebrity or other important figure.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='entourage#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Arrested Development</strong><span> Joe has acquired an entourage.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/entourage.jpg' video_url='examplevids/entourage' video_width='350'></span>
<div id='wt-container'>
<img alt="Entourage" height="288" src="https://cdn1.membean.com/video/examplevids/entourage.jpg" width="350" />
<div class='center'>
<a href="entourage#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='entourage#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Entourage" src="https://cdn2.membean.com/public/images/wordimages/cons2/entourage.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='entourage#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adjunct</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>advocate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>ambient</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>ancillary</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>concomitant</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>coterie</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>milieu</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>retinue</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>servile</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>subservient</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>sycophant</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='4' class = 'rw-wordform notlearned'><span>autonomous</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>cynosure</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>luminary</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>paragon</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>recluse</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="entourage" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>entourage</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="entourage#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

