
<!DOCTYPE html>
<html>
<head>
<title>Word: pontificate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx1' style='display:none'>When a person is speaking or writing in a bombastic fashion, they are very self-centered, extremely showy, and excessively proud.</p>
<p class='rw-defn idx2' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx3' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx4' style='display:none'>A man who is debonair wears fashionable clothes and is sophisticated, charming, friendly, and confident.</p>
<p class='rw-defn idx5' style='display:none'>If you describe someone, usually a young woman, as demure, you mean that she is quiet, shy, and always behaves modestly.</p>
<p class='rw-defn idx6' style='display:none'>An animal or person that is docile is not aggressive; rather, they are well-behaved, quiet, and easy to control.</p>
<p class='rw-defn idx7' style='display:none'>An exorbitant price or fee is much higher than what it should be or what is considered reasonable.</p>
<p class='rw-defn idx8' style='display:none'>If someone or something is flamboyant, the former is trying to show off in a way that deliberately attracts attention, and the latter is brightly colored and highly decorated.</p>
<p class='rw-defn idx9' style='display:none'>Something florid has too much decoration or is too elaborate.</p>
<p class='rw-defn idx10' style='display:none'>Praise, an apology, or gratitude is fulsome if it is so exaggerated and elaborate that it does not seem sincere.</p>
<p class='rw-defn idx11' style='display:none'>Grandiloquent speech is highly formal, exaggerated, and often seems both silly and hollow because it is expressly used to appear impressive and important.</p>
<p class='rw-defn idx12' style='display:none'>A harangue is a long, scolding speech.</p>
<p class='rw-defn idx13' style='display:none'>Hyperbole is a way of emphasizing something that makes it sound much more impressive or much worse than it actually is.</p>
<p class='rw-defn idx14' style='display:none'>An injunction is a court order that prevents someone from doing something.</p>
<p class='rw-defn idx15' style='display:none'>If something is inordinate, it is much larger in amount or degree than is normally expected.</p>
<p class='rw-defn idx16' style='display:none'>To opine is to state your opinion on something.</p>
<p class='rw-defn idx17' style='display:none'>If you describe an action as ostentatious, you think it is an extreme and exaggerated way of impressing people.</p>
<p class='rw-defn idx18' style='display:none'>Someone is overweening when they are not modest; rather, they think way too much of themselves and let everyone know about it.</p>
<p class='rw-defn idx19' style='display:none'>To be politic in a decision is to be socially wise and diplomatic.</p>
<p class='rw-defn idx20' style='display:none'>Ponderous writing or speech is boring, highly serious, and seems very long and wordy; it definitely lacks both grace and style.</p>
<p class='rw-defn idx21' style='display:none'>To rant is to go on an angry verbal attack.</p>
<p class='rw-defn idx22' style='display:none'>To remonstrate with someone is to tell that person that you strongly disapprove of something they have said or done.</p>
<p class='rw-defn idx23' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx24' style='display:none'>Someone&#8217;s sardonic smile, expression, or comment shows a lack of respect for what someone else has said or done; this occurs because the former thinks they are too important to consider or discuss such a supposedly trivial matter of the latter.</p>
<p class='rw-defn idx25' style='display:none'>A tirade is a prolonged, verbal outburst that severely criticizes someone or something.</p>
<p class='rw-defn idx26' style='display:none'>Turgid writing or speech is excessively complicated, being filled with too many needlessly difficult words; consequently, such verbiage is boring and difficult to understand.</p>
<p class='rw-defn idx27' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>pontificate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='pontificate#' id='pronounce-sound' path='audio/words/amy-pontificate'></a>
pon-TIF-i-kayt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='pontificate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='pontificate#' id='context-sound' path='audio/wordcontexts/brian-pontificate'></a>
Marian has turned some of her friends away because she <em>pontificates</em> or preaches too much about health!  Just yesterday her friend Alice was eating a strawberry when Marian got right in her face and started <em><em>pontificating</em></em> and lecturing her on the ill effects of pesticides.  This <em><em>pontificating</em></em> made Alice uncomfortable, so she decided to just go home rather than listen to Marian&#8217;s heavy sermon.  Marian seems to think she&#8217;s right all the time, so she <em>pontificates</em> and always seems to think her opinion is the only one that matters.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean to <em>pontificate</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
To show concern for the health of another.
</li>
<li class='choice '>
<span class='result'></span>
To ask insightful questions during a discussion.
</li>
<li class='choice answer '>
<span class='result'></span>
To lecture instead of just stating an opinion.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='pontificate#' id='definition-sound' path='audio/wordmeanings/amy-pontificate'></a>
When someone <em>pontificates</em>, they give their opinions in a heavy-handed way that shows they think they are always right.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>lecture</em>
</span>
</span>
</div>
<a class='quick-help' href='pontificate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='pontificate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/pontificate/memory_hooks/4850.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Cate</span></span>'s <span class="emp2"><span>Pon</span></span>ytail <span class="emp1"><span>Tif</span></span>f</span></span> <span class="emp3"><span>Cate</span></span> often got in <span class="emp1"><span>tif</span></span>fs, or fights with her mother, who always <span class="emp2"><span>pon</span></span><span class="emp1"><span>tif</span></span>i<span class="emp3"><span>cate</span></span>d about why it was best for <span class="emp3"><span>Cate</span></span> to always wear her hair in a <span class="emp2"><span>pon</span></span>ytail: "It won't get in your way, <span class="emp3"><span>Cate</span></span>!" she would always <span class="emp2"><span>pon</span></span><span class="emp1"><span>tif</span></span>i<span class="emp3"><span>cate</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="pontificate#" id="add-public-hook" style="" url="https://membean.com/mywords/pontificate/memory_hooks">Use other public hook</a>
<a href="pontificate#" id="memhook-use-own" url="https://membean.com/mywords/pontificate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='pontificate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Capt. Chesley "Sully" Sullenberger earned the right to <b>pontificate</b> on pretty much anything he wants after he successfully landed US Airways Flight 1549 in Hudson River. OK, we kid, but when Sullenberger talks about air safety, we listen.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
State premiers wheel and deal in the Bundesrat, the upper chamber of the federal parliament, to stymie government legislation they do not fancy or to <b>pontificate</b> on foreign policy, which is not supposed to be their affair.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/pontificate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='pontificate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>pont</td>
<td>
&rarr;
</td>
<td class='meaning'>bridge</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='fic_make' data-tree-url='//cdn1.membean.com/public/data/treexml' href='pontificate#'>
<span class='common'></span>
fic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make, do</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ate_quality' data-tree-url='//cdn1.membean.com/public/data/treexml' href='pontificate#'>
<span class=''></span>
-ate
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make something have a certain quality</td>
</tr>
</table>
<p>A <em>pontifex</em>, or &#8220;high priest,&#8221; was the &#8220;maker of a bridge&#8221; between his &#8220;flock&#8221; and the divine.  When a high priest <em>pontificates</em>, he &#8220;makes&#8221; the rules which people will listen and obey unquestionably.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='pontificate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Pontificate" src="https://cdn0.membean.com/public/images/wordimages/cons2/pontificate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='pontificate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>bombastic</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>exorbitant</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>flamboyant</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>florid</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>fulsome</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>grandiloquent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>harangue</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>hyperbole</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>injunction</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>inordinate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>opine</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>ostentatious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>overweening</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>ponderous</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>rant</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>remonstrate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>sardonic</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>tirade</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>turgid</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>debonair</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>demure</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>docile</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>politic</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="pontificate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>pontificate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="pontificate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

