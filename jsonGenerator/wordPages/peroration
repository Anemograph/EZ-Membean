
<!DOCTYPE html>
<html>
<head>
<title>Word: peroration | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abridge a book or play, you make it shorter by making cuts to the original.</p>
<p class='rw-defn idx1' style='display:none'>An addendum is an additional section added to the end of a book, document, or speech that gives more information.</p>
<p class='rw-defn idx2' style='display:none'>When a person is speaking or writing in a bombastic fashion, they are very self-centered, extremely showy, and excessively proud.</p>
<p class='rw-defn idx3' style='display:none'>Brevity is communicating by using just a few words or by taking very little time to do so.</p>
<p class='rw-defn idx4' style='display:none'>A diatribe is a speech or piece of writing that angrily attacks someone&#8217;s ideas or activities at length.</p>
<p class='rw-defn idx5' style='display:none'>Grandiloquent speech is highly formal, exaggerated, and often seems both silly and hollow because it is expressly used to appear impressive and important.</p>
<p class='rw-defn idx6' style='display:none'>A harangue is a long, scolding speech.</p>
<p class='rw-defn idx7' style='display:none'>Something that is interminable continues for a very long time in a boring or annoying way.</p>
<p class='rw-defn idx8' style='display:none'>A person who is being laconic uses very few words to say something.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is loquacious talks too much.</p>
<p class='rw-defn idx10' style='display:none'>A pithy statement or piece of writing is brief but intelligent; it is also precise and to the point.</p>
<p class='rw-defn idx11' style='display:none'>When someone pontificates, they give their opinions in a heavy-handed way that shows they think they are always right.</p>
<p class='rw-defn idx12' style='display:none'>Prattle is mindless chatter that seems like it will never stop.</p>
<p class='rw-defn idx13' style='display:none'>If you refrain from doing something, you do not do it—even though you want to.</p>
<p class='rw-defn idx14' style='display:none'>Rhetoric is the skill or art of using language to persuade or influence people, especially language that sounds impressive but may not be sincere or honest.</p>
<p class='rw-defn idx15' style='display:none'>Something that is succinct is clearly and briefly explained without using any unnecessary words.</p>
<p class='rw-defn idx16' style='display:none'>To be terse in speech is to be short and to the point, often in an abrupt manner that may seem unfriendly.</p>
<p class='rw-defn idx17' style='display:none'>A tirade is a prolonged, verbal outburst that severely criticizes someone or something.</p>
<p class='rw-defn idx18' style='display:none'>If you truncate something, you make it shorter or quicker by removing or cutting off part of it.</p>
<p class='rw-defn idx19' style='display:none'>Something that is verbose, such as a speech or article, contains too many words.</p>
<p class='rw-defn idx20' style='display:none'>A voluble person can speak easily with speech that naturally flows; voluble people also tend to be quite chatty.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>peroration</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='peroration#' id='pronounce-sound' path='audio/words/amy-peroration'></a>
per-uh-RAY-shuhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='peroration#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='peroration#' id='context-sound' path='audio/wordcontexts/brian-peroration'></a>
As I sat through the lengthy <em>peroration</em>, I was glad to have a fan to keep myself cool during the seemingly never-ending speech.  During this <em>peroration</em> or public address, I wondered how somebody could say so many words, sound so very impressive, and yet deliver so little substance.  The speaker&#8217;s two-hour <em>peroration</em> had the ultimate effect of making us all just pretty angry at having to sit through something oh so pointless.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>peroration</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A place to sit during a speech.
</li>
<li class='choice answer '>
<span class='result'></span>
A lengthy talk delivered in public by one person.
</li>
<li class='choice '>
<span class='result'></span>
Something that never seems to end.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='peroration#' id='definition-sound' path='audio/wordmeanings/amy-peroration'></a>
A <em>peroration</em> is a long speech that sounds impressive but does not have much substance.
</li>
<li class='def-text'>
A <em>peroration</em> can also be the last part of a speech when the speaker sums up their argument.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>extended speech</em>
</span>
</span>
</div>
<a class='quick-help' href='peroration#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='peroration#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/peroration/memory_hooks/5084.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Pair</span></span> <span class="emp3"><span>Oration</span></span></span></span> Geez, it was bad enough listening to one senator go on and on about the issue, but then it really became an unbearable <span class="emp1"><span>per</span></span><span class="emp3"><span>oration</span></span> when they joined together in a truly deadening <span class="emp1"><span>pair</span></span> <span class="emp3"><span>oration</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="peroration#" id="add-public-hook" style="" url="https://membean.com/mywords/peroration/memory_hooks">Use other public hook</a>
<a href="peroration#" id="memhook-use-own" url="https://membean.com/mywords/peroration/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='peroration#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
If you chose the wrong time to go to the [bathroom], you would get a 20-minute <b>peroration</b> on every 18th century print lining the stairs. I cannot tell you how uninterested I was in the window tax, which was another of his [favorite] subjects.
<cite class='attribution'>
&mdash;
The Guardian
</cite>
</li>
<li>
Bezos’s ventures are by now so large and varied that it is difficult to truly comprehend the nature of his empire, much less the end point of his ambitions. What exactly does Jeff Bezos want? . . . Bezos rallies the public with passionate <b>peroration</b> and convincing command of detail. Yet a human hole remains in his presentation.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
Politics, he now claims in an eloquent but hardly off-the-cuff <b>peroration</b>, is the natural culmination of his life. . . . Cut through the campaignspeak and a new kind of Russian politician begins to emerge: an intelligent, educated, successful, worldly guy who understands the value of competition . . . .
<cite class='attribution'>
&mdash;
Forbes
</cite>
</li>
<li>
I did not see Lillard’s <b>peroration</b> live on TV but a day later, on Twitter, where it played in heavy rotation, accompanied by sentiments like “class act” and “no better role model.” . . . Public performance and spectacle—rallies, speeches, news conferences—are as inherent to politics as they are to sports.
<cite class='attribution'>
&mdash;
The New York Times Magazine
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/peroration/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='peroration#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='per_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='peroration#'>
<span class='common'></span>
per-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='or_speak' data-tree-url='//cdn1.membean.com/public/data/treexml' href='peroration#'>
<span class=''></span>
or
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>speak</td>
</tr>
<tr>
<td class='partform'>-ation</td>
<td>
&rarr;
</td>
<td class='meaning'>act of doing something</td>
</tr>
</table>
<p>A <em>peroration</em> is the &#8220;act of thoroughly speaking.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='peroration#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Peroration" src="https://cdn1.membean.com/public/images/wordimages/cons2/peroration.png?qdep6" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='peroration#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>addendum</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bombastic</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>diatribe</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>grandiloquent</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>harangue</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>interminable</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>loquacious</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>pontificate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>prattle</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>rhetoric</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>tirade</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>verbose</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>voluble</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abridge</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>brevity</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>laconic</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>pithy</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>refrain</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>succinct</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>terse</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>truncate</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="peroration" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>peroration</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="peroration#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

