
<!DOCTYPE html>
<html>
<head>
<title>Word: perplexed | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Acuity is sharpness or clearness of vision, hearing, or thought.</p>
<p class='rw-defn idx1' style='display:none'>Acumen is the ability to make quick decisions and keen, precise judgments.</p>
<p class='rw-defn idx2' style='display:none'>If someone is addled by something, they are confused by it and unable to think properly.</p>
<p class='rw-defn idx3' style='display:none'>If you are bemused, you are puzzled and confused; hence, you are lost or absorbed in puzzling thought.</p>
<p class='rw-defn idx4' style='display:none'>If something bewilders you, you are very confused or puzzled by it.</p>
<p class='rw-defn idx5' style='display:none'>If something confounds you, it makes you feel surprised and confused, often because it does not meet your predefined expectations.</p>
<p class='rw-defn idx6' style='display:none'>A conundrum is a problem or puzzle that is difficult or impossible to solve.</p>
<p class='rw-defn idx7' style='display:none'>When you decipher a message or piece of writing, you work out what it says, even though it is very difficult to read or understand.</p>
<p class='rw-defn idx8' style='display:none'>When you discern something, you notice, detect, or understand it, often after thinking about it carefully or studying it for some time.  </p>
<p class='rw-defn idx9' style='display:none'>When you are discombobulated, you are confused and upset because you have been thrown into a situation that you cannot temporarily handle.</p>
<p class='rw-defn idx10' style='display:none'>To divulge something is to reveal information that was once private or secret.</p>
<p class='rw-defn idx11' style='display:none'>If you elucidate something, you make it easier to understand by giving more relevant information.</p>
<p class='rw-defn idx12' style='display:none'>Someone or something that is enigmatic is mysterious and difficult to understand.</p>
<p class='rw-defn idx13' style='display:none'>To explicate an idea or plan is to make it clear by explaining it.</p>
<p class='rw-defn idx14' style='display:none'>If you fathom something complicated or mysterious, you are able to understand it.</p>
<p class='rw-defn idx15' style='display:none'>If an idea or thought is incisive, it is expressed in a penetrating and knowledgeable manner that is clear and brief; additionally, it can demonstrate impressive understanding of related ideas or thoughts.</p>
<p class='rw-defn idx16' style='display:none'>If you describe a situation or process as labyrinthine, you mean that it is very complicated, involved, and difficult to understand.</p>
<p class='rw-defn idx17' style='display:none'>If someone is lucid, they are able to understand things and think clearly; this adjective also describes writing or speech that is crystal clear and easy to understand.</p>
<p class='rw-defn idx18' style='display:none'>Someone is obtuse when they are slow to understand things; someone can choose to be obtuse when they deliberately do not try to figure things out.</p>
<p class='rw-defn idx19' style='display:none'>A paradox is a statement that appears to be self-contradictory or unrealistic but may surprisingly express a possible truth.</p>
<p class='rw-defn idx20' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx21' style='display:none'>If you are in a quandary, you are in a difficult situation in which you have to make a decision but don&#8217;t know what to do.</p>
<p class='rw-defn idx22' style='display:none'>When you resolve a problem, you solve it or come to a decision about it.</p>
<p class='rw-defn idx23' style='display:none'>A sagacious person is wise, intelligent, and has the ability to make good practical decisions.</p>
<p class='rw-defn idx24' style='display:none'>An aged person who is senile is less mentally aware than they were in the past; therefore, they tend to be confused and forgetful at times.</p>
<p class='rw-defn idx25' style='display:none'>Something that stymies you presents an obstacle that prevents you from doing what you need or want to do.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>perplexed</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='perplexed#' id='pronounce-sound' path='audio/words/amy-perplexed'></a>
per-PLEKSD
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='perplexed#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='perplexed#' id='context-sound' path='audio/wordcontexts/brian-perplexed'></a>
The riddle that Bilbo gave me simply <em>perplexed</em> or confused me.  The notion of what he had in his pocket <em>perplexed</em> or baffled me so much, although usually I was very good at solving difficult riddles.  That <em>perplexing</em> problem stumped me, and I didn&#8217;t ever figure it out.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How does someone who is <em>perplexed</em> likely act?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
They act like they aren&#8217;t sure what is happening.
</li>
<li class='choice '>
<span class='result'></span>
They act like they are having a very exciting time.
</li>
<li class='choice '>
<span class='result'></span>
They act like they aren&#8217;t happy about what&#8217;s happening.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='perplexed#' id='definition-sound' path='audio/wordmeanings/amy-perplexed'></a>
When you are <em>perplexed</em> about something, you are completely confused or baffled by it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>confused</em>
</span>
</span>
</div>
<a class='quick-help' href='perplexed#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='perplexed#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/perplexed/memory_hooks/4686.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Per</span></span>fectly Com<span class="emp3"><span>plex</span></span></span></span>  If you are <span class="emp1"><span>per</span></span><span class="emp3"><span>plex</span></span>ed about a problem, it is <span class="emp1"><span>per</span></span>fectly com<span class="emp3"><span>plex</span></span>, at least for you!
</p>
</div>

<div id='memhook-button-bar'>
<a href="perplexed#" id="add-public-hook" style="" url="https://membean.com/mywords/perplexed/memory_hooks">Use other public hook</a>
<a href="perplexed#" id="memhook-use-own" url="https://membean.com/mywords/perplexed/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='perplexed#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
To have little is to possess. To have plenty is to be <b>perplexed</b>.
<span class='attribution'>&mdash; Lao-Tzu, Chinese Philosopher</span>
<img alt="Lao-tzu, chinese philosopher" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Lao-Tzu, Chinese Philosopher.jpg?qdep8" width="80" />
</li>
<li>
Rice farmers throughout Northern California are <b>perplexed</b> that companies and scientists are continuing to experiment with a technology so thoroughly rejected by the market.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
At least 10 such cases were brought to the notice of the police—although no cases were registered as the officials were <b>perplexed</b>, not knowing what to do.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
The Vatican said Tuesday that it is "<b>perplexed</b> and baffled" by the public prosecutor’s actions and that the Holy See aims for "complete transparency" in its financial operations.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/perplexed/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='perplexed#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='per_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='perplexed#'>
<span class='common'></span>
per-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='plex_weave' data-tree-url='//cdn1.membean.com/public/data/treexml' href='perplexed#'>
<span class=''></span>
plex
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>weave, entwine</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ed_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='perplexed#'>
<span class=''></span>
-ed
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>having a particular state</td>
</tr>
</table>
<p>When one is <em>perplexed</em> about something, one&#8217;s mind is &#8220;thoroughly woven or entwined&#8221; in difficulty.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='perplexed#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Star Trek Voyager</strong><span> Tuvok is perplexed by the investigation.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/perplexed.jpg' video_url='examplevids/perplexed' video_width='350'></span>
<div id='wt-container'>
<img alt="Perplexed" height="288" src="https://cdn1.membean.com/video/examplevids/perplexed.jpg" width="350" />
<div class='center'>
<a href="perplexed#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='perplexed#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Perplexed" src="https://cdn1.membean.com/public/images/wordimages/cons2/perplexed.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='perplexed#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>addle</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bemused</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>bewilder</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>confound</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>conundrum</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>discombobulated</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>enigmatic</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>labyrinthine</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>obtuse</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>paradox</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>quandary</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>senile</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>stymie</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>acuity</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>acumen</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>decipher</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>discern</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>divulge</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>elucidate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>explicate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>fathom</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>incisive</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>lucid</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>resolve</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>sagacious</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='perplexed#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
perplex
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>confuse</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="perplexed" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>perplexed</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="perplexed#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

