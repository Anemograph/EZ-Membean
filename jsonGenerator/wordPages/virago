
<!DOCTYPE html>
<html>
<head>
<title>Word: virago | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Antipathy is a strong feeling of dislike or hostility towards someone or something.</p>
<p class='rw-defn idx1' style='display:none'>If you are bellicose, you behave in an aggressive way and are likely to start an argument or fight.</p>
<p class='rw-defn idx2' style='display:none'>A belligerent person or country is aggressive, very unfriendly, and likely to start a fight.</p>
<p class='rw-defn idx3' style='display:none'>When you berate someone, you speak to them angrily because they have done something wrong.</p>
<p class='rw-defn idx4' style='display:none'>If you describe a person&#8217;s behavior or speech as brusque, you mean that they say or do things abruptly or too quickly in a somewhat rude and impolite way.</p>
<p class='rw-defn idx5' style='display:none'>A caustic remark is unkind, critical, cruel, and mocking.</p>
<p class='rw-defn idx6' style='display:none'>When someone is contentious, they are argumentative and likely to provoke a fight.</p>
<p class='rw-defn idx7' style='display:none'>A harridan is a mean old woman. <i>Note: This is an outdated term</i></p>
<p class='rw-defn idx8' style='display:none'>If you hurl invective at another person, you are verbally abusing or highly criticizing them.</p>
<p class='rw-defn idx9' style='display:none'>To act in a pugnacious manner is to act in a combative and aggressive way.</p>
<p class='rw-defn idx10' style='display:none'>Trenchant comments or criticisms are expressed forcefully, directly, and clearly, even though they may be hurtful to the receiver.</p>
<p class='rw-defn idx11' style='display:none'>A truculent person is bad-tempered, easily annoyed, and prone to arguing excessively.</p>
<p class='rw-defn idx12' style='display:none'>When you have a vehement feeling about something, you feel very strongly or intensely about it.</p>
<p class='rw-defn idx13' style='display:none'>A virulent disease is very dangerous and spreads very quickly.</p>
<p class='rw-defn idx14' style='display:none'>Vitriolic words are bitter, unkind, and mean-spirited.</p>
<p class='rw-defn idx15' style='display:none'>Vituperative remarks are full of hate, anger, and cruel criticism.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 6
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>virago</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='virago#' id='pronounce-sound' path='audio/words/amy-virago'></a>
vi-RAH-goh
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='virago#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='virago#' id='context-sound' path='audio/wordcontexts/brian-virago'></a>
Today the word &#8220;<em>virago</em>&#8221; means a woman of strong character who doesn&#8217;t back down during a difficult situation, but it didn&#8217;t always have a positive connotation. The word &#8220;<em>virago</em>&#8221; used to refer to a combative woman. In fact, four British navy warships were given the name &#8220;<em>virago</em>&#8221; to symbolize their aggressiveness. Meanings of words in dictionaries will often change throughout long periods of time; in this case, the shift of the word &#8220;<em>virago</em>&#8221; from &#8220;shrew&#8221; to &#8220;courageous woman&#8221; is most welcome.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What might a modern-day <em>virago</em> do?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
She typically avoids engaging in any type of conflict.
</li>
<li class='choice '>
<span class='result'></span>
She quietly enjoys isolation in an idyllic location.
</li>
<li class='choice answer '>
<span class='result'></span>
She bravely fights against an unjust law.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='virago#' id='definition-sound' path='audio/wordmeanings/amy-virago'></a>
The word <em>virago</em> used to refer to an ill-tempered and combative woman who tended to be quarrelsome; in current usage, this word usually refers to a courageous and strong woman.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>fierce female</em>
</span>
</span>
</div>
<a class='quick-help' href='virago#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='virago#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/virago/memory_hooks/116705.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Veer</span></span> to a Time Long <span class="emp3"><span>Ago</span></span></span></span> Bessie has become such a <span class="emp3"><span>vir</span></span><span class="emp1"><span>ago</span></span> that I want to <span class="emp1"><span>veer</span></span> away to a long time <span class="emp3"><span>ago</span></span> just so I won't hear her loud voice scolding us all anymore!
</p>
</div>

<div id='memhook-button-bar'>
<a href="virago#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/virago/memory_hooks">Use other hook</a>
<a href="virago#" id="memhook-use-own" url="https://membean.com/mywords/virago/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='virago#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
A new publishing company began at her kitchen table, and its name was coined, she says, when she and Boycott, "were sitting on the floor of my flat, going through a book of goddesses. Rosie came across <b>Virago</b>: 'a war-like woman', and I said: 'That's fine! I love it.'"
<cite class='attribution'>
&mdash;
The Guardian
</cite>
</li>
<li>
<b>Virago</b> is a term typically used to describe women—heroic, strong and courageous to some; shrewd and overbearing to others. In either interpretation, the word implies a transgression of gender norms. <b>Viragoes</b> are women who buck the stereotype of the docile and demure lady, and choreographer Cynthia Oliver has spent much of her career doing just that.
<cite class='attribution'>
&mdash;
Chicago Tribune
</cite>
</li>
<li>
A <b>virago</b>, [Gertrude Bell] was an Edwardian Great Explorer of the stamp history usually records and lauds in men. The British heiress applied her energy, intellect and funds to any problem she felt was worth solving, which over the course of her life included social issues at home, the Wounded and Missing department in the Great War, espionage and mountain climbing.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
He paid two dollars and a half a month rent for the small room he got from his Portuguese landlady, Maria Silva, a <b>virago</b> and a widow, hard working and harsher tempered, rearing her large brood of children somehow. . . . From detesting her and her foul tongue at first, Martin grew to admire her as he observed the brave fight she made.
<cite class='attribution'>
&mdash;
Jack London, American novelist in _Martin Eden_
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/virago/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='virago#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep5" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>vir</td>
<td>
&rarr;
</td>
<td class='meaning'>man</td>
</tr>
</table>
<p>A <em>virago</em> is a woman who has what were once considered only qualities of a &#8220;man:&#8221; positive ones such as strength and courage, and negative ones such as being domineering and overbearing.  Unfortunately, the usage of this word tends to fall on the negative qualities a vast majority of the time.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='virago#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep5" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Star Trek the Original Series</strong><span> The mercenary Harcourt Fenton Mudd keeps an android copy of his wife, a virago whom he enjoys turning off with a simple command.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/virago.jpg' video_url='examplevids/virago' video_width='350'></span>
<div id='wt-container'>
<img alt="Virago" height="288" src="https://cdn1.membean.com/video/examplevids/virago.jpg" width="350" />
<div class='center'>
<a href="virago#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='virago#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep5" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Virago" src="https://cdn1.membean.com/public/images/wordimages/cons2/virago.png?qdep5" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='virago#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>antipathy</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>bellicose</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>belligerent</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>berate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>brusque</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>caustic</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>contentious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>harridan</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>invective</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>pugnacious</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>trenchant</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>truculent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>vehement</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>virulent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>vitriolic</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>vituperative</span> &middot;</li></ul>
<ul class='related-ants'></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="virago" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>virago</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="virago#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

