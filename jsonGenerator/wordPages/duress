
<!DOCTYPE html>
<html>
<head>
<title>Word: duress | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Duress-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/duress-large.jpg?qdep8" />
</div>
<a href="duress#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>When you abscond, you leave suddenly from a place that has imprisoned or persecuted you, or you leave from a place with something that doesn&#8217;t belong to you.</p>
<p class='rw-defn idx2' style='display:none'>You accost a stranger when you move towards them and speak in an unpleasant or threatening way.</p>
<p class='rw-defn idx3' style='display:none'>An affliction is something that causes pain and mental suffering, especially a medical condition.</p>
<p class='rw-defn idx4' style='display:none'>A bastion is considered an important and effective defense of a way of life or principle; a bastion can also refer to a heavily fortified place, such as a castle.</p>
<p class='rw-defn idx5' style='display:none'>When something bedevils you, it causes you a lot of problems and difficulties over a period of time.</p>
<p class='rw-defn idx6' style='display:none'>When you beleaguer someone, you act with the intent to annoy or harass that person repeatedly until they finally give you what you want.</p>
<p class='rw-defn idx7' style='display:none'>If you are beset by something, you are experiencing serious problems or dangers because of it.</p>
<p class='rw-defn idx8' style='display:none'>When you buttress an argument, idea, or even a building, you make it stronger by providing support.</p>
<p class='rw-defn idx9' style='display:none'>If you circumvent something, such as a rule or restriction, you try to get around it in a clever and perhaps dishonest way.</p>
<p class='rw-defn idx10' style='display:none'>You coerce people when you force them to do something that they don&#8217;t want to do.</p>
<p class='rw-defn idx11' style='display:none'>When you are constrained, you are forced to do something or are kept from doing it.</p>
<p class='rw-defn idx12' style='display:none'>If something daunts you, it makes you worried about dealing with it because you think it will be very difficult or dangerous.</p>
<p class='rw-defn idx13' style='display:none'>If something discomfits you, it makes you feel embarrassed, confused, uncomfortable, or frustrated.</p>
<p class='rw-defn idx14' style='display:none'>If something disconcerts you, it makes you feel anxious, worried, or confused.</p>
<p class='rw-defn idx15' style='display:none'>When someone feels disquiet about a situation, they feel very worried or nervous.</p>
<p class='rw-defn idx16' style='display:none'>If a fact or idea eludes you, you cannot remember or understand it; if you elude someone, you manage to escape or hide from them.</p>
<p class='rw-defn idx17' style='display:none'>If something encumbers you, it makes it difficult for you to move freely or do what you want.</p>
<p class='rw-defn idx18' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx19' style='display:none'>When one nation extradites someone, it hands them over into the custody of a second nation, usually to stand trial for crimes committed against that second nation.</p>
<p class='rw-defn idx20' style='display:none'>If someone is imperturbable, they are always calm and not easily upset or disturbed by any situation, even dangerous ones.</p>
<p class='rw-defn idx21' style='display:none'>If you incarcerate someone, you put them in prison or jail.</p>
<p class='rw-defn idx22' style='display:none'>Insouciance is a lack of concern or worry for something that should be shown more careful attention or consideration.</p>
<p class='rw-defn idx23' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx24' style='display:none'>If you nettle someone, you irritate or annoy them.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx26' style='display:none'>If you are oppressed by someone or something, you are beaten down, troubled, or burdened by them or it.</p>
<p class='rw-defn idx27' style='display:none'>To parry is to ward something off or deflect it.</p>
<p class='rw-defn idx28' style='display:none'>If someone is <em>pilloried</em>, they are publicly criticized or ridiculed by a lot of people, especially in the media.</p>
<p class='rw-defn idx29' style='display:none'>If governments, companies, or other institutions retrench, they reduce costs and/or decrease the amount that they spend in order to save money.</p>
<p class='rw-defn idx30' style='display:none'>When you stifle someone&#8217;s creativity or inner drive, you prevent it from being expressed.</p>
<p class='rw-defn idx31' style='display:none'>When you subdue something, such as an enemy or emotions, you defeat or bring them under control.</p>
<p class='rw-defn idx32' style='display:none'>If someone subjugates a group of people or country, they conquer and bring it under control by force.</p>
<p class='rw-defn idx33' style='display:none'>Something that is unsullied is unstained and clean.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>duress</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='duress#' id='pronounce-sound' path='audio/words/amy-duress'></a>
DOOR-ess
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='duress#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='duress#' id='context-sound' path='audio/wordcontexts/brian-duress'></a>
The boys finally admitted that they&#8217;d been at the scene of the crime; their confession came unwillingly under the <em>duress</em> of many hours of police interrogation.  They claimed they&#8217;d been part of the robbery, but had been forced into it under <em>duress</em>, since they&#8217;d been threatened by the actual robbers.  The judge believed that they were good kids who wouldn&#8217;t do wrong unless they were the victims of extreme threat or <em>duress</em>.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean to do something under <em>duress</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
You don&#8217;t remember doing it afterward.
</li>
<li class='choice '>
<span class='result'></span>
You do it secretly and hope no one finds out.
</li>
<li class='choice answer '>
<span class='result'></span>
You don&#8217;t do it willingly but out of fear.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='duress#' id='definition-sound' path='audio/wordmeanings/amy-duress'></a>
<em>Duress</em> is the application or threat of force to compel someone to act in a particular way.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>threat</em>
</span>
</span>
</div>
<a class='quick-help' href='duress#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='duress#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/duress/memory_hooks/3400.json'></span>
<p>
<span class="emp0"><span>En<span class="emp1"><span>du</span></span>re the St<span class="emp2"><span>ress</span></span></span></span> "I can't en<span class="emp1"><span>du</span></span>re the st<span class="emp2"><span>ress</span></span>!  The p<span class="emp2"><span>ress</span></span>ure of <span class="emp1"><span>du</span></span><span class="emp2"><span>ress</span></span> is killing me!  OK, OK, OK ... I'll eat your fruitcake!  AAAAAAHHHHHHHHHHH ..."
</p>
</div>

<div id='memhook-button-bar'>
<a href="duress#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/duress/memory_hooks">Use other hook</a>
<a href="duress#" id="memhook-use-own" url="https://membean.com/mywords/duress/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='duress#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Kirchgessner says he plans to argue that hospital contracts, often signed under <b>duress</b> during a medical crisis, aren't valid.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Hamilton, who has broken his left ankle five times in wipeouts while surfing, motorcycling and snowboarding and has learned to hold his breath for well over a minute under <b>duress</b>, calls riding waves with seven-story faces "the ultimate sensation."
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
If a defendant tries to retract or deny a confession in front of a jury, it will spotlight the issue and may lead to an eventual realization that confessions taken under such <b>duress</b> are inappropriate as evidence of guilt.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
Stephen Curry scored 27 points with four 3-pointers and became the [Golden State] Warriors' franchise assists leader . . . "He can make passes with either hand and he gets blitzed so often that a lot of his passes come in traffic and under <b>duress</b> and he does an incredible job of getting the ball out of the traps and finding the right guys," Kerr said before the game.
<cite class='attribution'>
&mdash;
Minneapolis Star Tribune
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/duress/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='duress#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='dur_hard' data-tree-url='//cdn1.membean.com/public/data/treexml' href='duress#'>
<span class=''></span>
dur
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>hard, harsh</td>
</tr>
<tr>
<td class='partform'>-ess</td>
<td>
&rarr;
</td>
<td class='meaning'>forms a Middle English noun</td>
</tr>
</table>
<p><em>Duress</em> is the application of &#8220;hardness&#8221; or &#8220;harshness&#8221; when one is trying to forcefully get someone to do something.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='duress#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Outer Limits</strong><span> A man under severe duress.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/duress.jpg' video_url='examplevids/duress' video_width='350'></span>
<div id='wt-container'>
<img alt="Duress" height="288" src="https://cdn1.membean.com/video/examplevids/duress.jpg" width="350" />
<div class='center'>
<a href="duress#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='duress#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Duress" src="https://cdn3.membean.com/public/images/wordimages/cons2/duress.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='duress#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>accost</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>affliction</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>bedevil</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>beleaguer</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>beset</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>coerce</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>constrain</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>daunt</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>discomfit</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>disconcert</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>disquiet</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>encumber</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>extradite</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>incarcerate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>nettle</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>oppress</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>pillory</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>stifle</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>subdue</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>subjugate</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abscond</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>bastion</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>buttress</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>circumvent</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>elude</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>imperturbable</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>insouciance</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>parry</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>retrench</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>unsullied</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="duress" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>duress</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="duress#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

