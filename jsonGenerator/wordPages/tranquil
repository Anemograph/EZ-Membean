
<!DOCTYPE html>
<html>
<head>
<title>Word: tranquil | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Bedlam is a situation with a great deal of noise and confusion.</p>
<p class='rw-defn idx1' style='display:none'>When you beleaguer someone, you act with the intent to annoy or harass that person repeatedly until they finally give you what you want.</p>
<p class='rw-defn idx2' style='display:none'>If you are beset by something, you are experiencing serious problems or dangers because of it.</p>
<p class='rw-defn idx3' style='display:none'>The adjective bucolic is used to describe things that are related to or characteristic of rural or country life.</p>
<p class='rw-defn idx4' style='display:none'>A chaotic state of affairs is in a state of confusion and complete disorder.</p>
<p class='rw-defn idx5' style='display:none'>Consternation is the feeling of anxiety or fear, sometimes paralyzing in its effect, and often caused by something unexpected that has happened.</p>
<p class='rw-defn idx6' style='display:none'>When you are in a state of disarray, you are disorganized, disordered, and in a state of confusion.</p>
<p class='rw-defn idx7' style='display:none'>If something discomfits you, it makes you feel embarrassed, confused, uncomfortable, or frustrated.</p>
<p class='rw-defn idx8' style='display:none'>When someone feels disquiet about a situation, they feel very worried or nervous.</p>
<p class='rw-defn idx9' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx10' style='display:none'>Febrile behavior is full of nervous energy and activity; a sick person can be febrile as well, that is, feverish or hot.</p>
<p class='rw-defn idx11' style='display:none'>To foment is to encourage people to protest, fight, or cause trouble and violent opposition to something that is viewed by some as undesirable.</p>
<p class='rw-defn idx12' style='display:none'>Frenetic activity is done quickly with lots of energy but is also uncontrolled and disorganized; someone who is in a huge hurry often displays this type of behavior.</p>
<p class='rw-defn idx13' style='display:none'>A fusillade is a rapid burst of gunfire from multiple guns fired at once or in succession; in turn, a fusillade can also be a rapid outburst or discharge of something.</p>
<p class='rw-defn idx14' style='display:none'>A halcyon time is calm, peaceful, and undisturbed.</p>
<p class='rw-defn idx15' style='display:none'>When there is havoc, there is great disorder, widespread destruction, and much confusion.</p>
<p class='rw-defn idx16' style='display:none'>An idyll is a place or situation that is extremely pleasant, peaceful, and has no problems.</p>
<p class='rw-defn idx17' style='display:none'>An impending event is approaching fast or is about to occur; this word usually has a negative implication, referring to something threatening or harmful coming.</p>
<p class='rw-defn idx18' style='display:none'>An incendiary device causes objects to catch on fire; incendiary comments can cause riots to flare up.</p>
<p class='rw-defn idx19' style='display:none'>An incursion is an unpleasant intrusion, such as a sudden hostile attack or a land being flooded.</p>
<p class='rw-defn idx20' style='display:none'>A maelstrom is either a large whirlpool in the sea or a violent or agitated state of affairs.</p>
<p class='rw-defn idx21' style='display:none'>An ominous sign predicts or shows that something bad will happen in the near future.</p>
<p class='rw-defn idx22' style='display:none'>Pandemonium is a very noisy and uncontrolled situation, especially one that is caused by a lot of angry or excited people.</p>
<p class='rw-defn idx23' style='display:none'>A pastoral environment is rural, peaceful, simple, and natural.</p>
<p class='rw-defn idx24' style='display:none'>A placid scene or person is calm, quiet, and undisturbed.</p>
<p class='rw-defn idx25' style='display:none'>A state of quiescence is one of quiet and restful inaction.</p>
<p class='rw-defn idx26' style='display:none'>If you are in a state of repose, your mind is at peace or your body is at rest.</p>
<p class='rw-defn idx27' style='display:none'>If you are sedate, you are calm, unhurried, and unlikely to be disturbed by anything.</p>
<p class='rw-defn idx28' style='display:none'>A serene place or situation is peaceful and calm.</p>
<p class='rw-defn idx29' style='display:none'>Skittish persons or animals are made easily nervous or alarmed; they are likely to change behavior quickly and unpredictably.</p>
<p class='rw-defn idx30' style='display:none'>If someone is squeamish, they are easily nauseated or shocked by things that are tolerated by most people; they can also be oversensitive.</p>
<p class='rw-defn idx31' style='display:none'>A tempestuous storm, temper, or crowd is violent, wild, and in an uproar.</p>
<p class='rw-defn idx32' style='display:none'>Someone is tremulous when they are shaking slightly from nervousness or fear; they may also simply be afraid of something.</p>
<p class='rw-defn idx33' style='display:none'>Trepidation is fear or uneasiness about something that is going to happen.</p>
<p class='rw-defn idx34' style='display:none'>A tumultuous event or period of time is filled with great excitement, confusion, or violence; a tumultuous reaction to something is likewise very loud and noisy because people are happy and excited.</p>
<p class='rw-defn idx35' style='display:none'>When you experience turmoil, there is great confusion, disturbance, instability, and disorder in your life.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>tranquil</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='tranquil#' id='pronounce-sound' path='audio/words/amy-tranquil'></a>
TRANG-kwil
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='tranquil#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='tranquil#' id='context-sound' path='audio/wordcontexts/brian-tranquil'></a>
My time in the country was so <em>tranquil</em> and peaceful.  I stayed at a mountain cottage in a <em>tranquil</em> setting that was both calm and restful.  I feel like my <em>tranquil</em> and relaxing stay has made me stronger for the crazily busy life that I must lead in the city.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>tranquil</em> setting?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A calm, peaceful lake at sunrise.
</li>
<li class='choice '>
<span class='result'></span>
A creepy dark house at midnight.
</li>
<li class='choice '>
<span class='result'></span>
A crowded mall during a weekend.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='tranquil#' id='definition-sound' path='audio/wordmeanings/amy-tranquil'></a>
If something is <em>tranquil</em>, it is peaceful, calm, and quiet.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>peaceful</em>
</span>
</span>
</div>
<a class='quick-help' href='tranquil#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='tranquil#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/tranquil/memory_hooks/4985.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Tranquil</span></span>izer</span></span> A doctor gives a patient a <span class="emp3"><span>tranquil</span></span>izer to calm or make a patient <span class="emp3"><span>tranquil</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="tranquil#" id="add-public-hook" style="" url="https://membean.com/mywords/tranquil/memory_hooks">Use other public hook</a>
<a href="tranquil#" id="memhook-use-own" url="https://membean.com/mywords/tranquil/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='tranquil#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The terrible stories that come out of Iraq make it hard to remember that the country has beautiful, <b>tranquil</b> places as well—but young artist Ahmad AlKarkhi says we must not forget that these spots exist.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
There are few sights as peaceful as the lakes atop the Greenland ice sheet, <b>tranquil</b> pools of sapphire meltwater on a bed of sparkling white.
<cite class='attribution'>
&mdash;
Scientific American
</cite>
</li>
<li>
By a <b>tranquil</b> mind I mean nothing else than a mind well ordered.
<cite class='attribution'>
&mdash;
Marcus Aurelius, Roman emperor and philosopher, from _Meditations_
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/tranquil/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='tranquil#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p><em>Tranquil</em> comes from a root word meaning &#8220;calm, still.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='tranquil#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Meditation Relax Clip</strong><span> A tranquil scene.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/tranquil.jpg' video_url='examplevids/tranquil' video_width='350'></span>
<div id='wt-container'>
<img alt="Tranquil" height="288" src="https://cdn1.membean.com/video/examplevids/tranquil.jpg" width="350" />
<div class='center'>
<a href="tranquil#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='tranquil#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Tranquil" src="https://cdn3.membean.com/public/images/wordimages/cons2/tranquil.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='tranquil#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>bucolic</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>halcyon</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>idyll</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>pastoral</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>placid</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>quiescence</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>repose</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>sedate</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>serene</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>bedlam</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>beleaguer</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>beset</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>chaotic</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>consternation</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>disarray</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>discomfit</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>disquiet</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>febrile</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>foment</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>frenetic</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>fusillade</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>havoc</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>impending</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>incendiary</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>incursion</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>maelstrom</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>ominous</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>pandemonium</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>skittish</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>squeamish</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>tempestuous</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>tremulous</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>trepidation</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>tumultuous</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>turmoil</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="tranquil" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>tranquil</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="tranquil#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

