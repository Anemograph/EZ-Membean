
<!DOCTYPE html>
<html>
<head>
<title>Word: candor | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you describe something as ambiguous, you mean that it is unclear or confusing because it can be understood in multiple ways.</p>
<p class='rw-defn idx1' style='display:none'>When someone speaks in an artless fashion, they are without deception; when someone acts artlessly, they are being totally natural.</p>
<p class='rw-defn idx2' style='display:none'>If you employ chicanery, you are devising and carrying out clever plans and trickery to cheat and deceive people.</p>
<p class='rw-defn idx3' style='display:none'>A circuitous route, journey, or piece of writing is long and complicated rather than simple and direct.</p>
<p class='rw-defn idx4' style='display:none'>Circumlocution is a way of saying or writing something that uses too many words, especially in order to avoid stating the true meaning clearly.</p>
<p class='rw-defn idx5' style='display:none'>Something that is clandestine is hidden or kept a secret, often because it is illegal.</p>
<p class='rw-defn idx6' style='display:none'>If you collude with people, you work secretly with them to do something dishonest.</p>
<p class='rw-defn idx7' style='display:none'>If one person connives with another, they secretly plan to achieve something of mutual benefit, usually a thing that is illegal or immoral.</p>
<p class='rw-defn idx8' style='display:none'>When something is contrived, it is obviously thought about or planned beforehand, although it tries to be passed off as not being so.</p>
<p class='rw-defn idx9' style='display:none'>Covert activities or situations are secret and hidden.</p>
<p class='rw-defn idx10' style='display:none'>Something that is delusive deceives you by giving a false belief about yourself or the situation you are in.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is disingenuous is not straightforward or is dishonest in what they say or do.</p>
<p class='rw-defn idx12' style='display:none'>If you feign something, such as a sickness or an attitude, you pretend and try to convince people that you have it—even though you don&#8217;t.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is hypocritical pretends to be a person they are not.</p>
<p class='rw-defn idx14' style='display:none'>People who are ingenuous are excessively trusting and believe almost everything that people tell them, especially because they have not had much life experience.</p>
<p class='rw-defn idx15' style='display:none'>Legerdemain is the skillful use of one&#8217;s hands or the employment of another form of cleverness for the purpose of deceiving someone.</p>
<p class='rw-defn idx16' style='display:none'>A mendacious person does not tell the truth.</p>
<p class='rw-defn idx17' style='display:none'>If you are being noncommittal on an issue, you are not revealing what your opinion is and are being reserved on purpose.</p>
<p class='rw-defn idx18' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx19' style='display:none'>An oblique statement is not straightforward or direct; rather, it is purposely roundabout, vague, or misleading.</p>
<p class='rw-defn idx20' style='display:none'>An overt act is not hidden or secret but is done in an open and public way.</p>
<p class='rw-defn idx21' style='display:none'>Someone who is perfidious is not loyal and cannot be trusted.</p>
<p class='rw-defn idx22' style='display:none'>If you prevaricate, you avoid giving a direct or honest answer, usually because you want to hide the truth or want to delay or avoid making a hard decision.</p>
<p class='rw-defn idx23' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx24' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx25' style='display:none'>A scrupulous person takes great care to do everything in an extremely honest, conscientious, and fair manner.</p>
<p class='rw-defn idx26' style='display:none'>A stratagem is a clever trick or deception that is used to fool someone.</p>
<p class='rw-defn idx27' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx28' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx29' style='display:none'>The verity of something is the truth or reality of it.</p>
<p class='rw-defn idx30' style='display:none'>Wiles are clever tricks or cunning schemes that are used to persuade someone to do what you want.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>candor</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='candor#' id='pronounce-sound' path='audio/words/amy-candor'></a>
KAN-der
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='candor#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='candor#' id='context-sound' path='audio/wordcontexts/brian-candor'></a>
Sarah spoke to her parents with honesty and <em>candor</em> about her plans to put off college for a year, and they responded to her sincere announcement with respect.  Sarah had considered this issue for some time, and her openness and <em>candor</em> about what she wanted in the upcoming year reflected her maturity.  Since she spoke and acted with truth and <em>candor</em> about her dreams and intentions, her parents were not worried that she might never get her degree.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If you appreciate your friend&#8217;s <em>candor</em>, what do you like about them?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
That they are not afraid to take charge of a situation.
</li>
<li class='choice '>
<span class='result'></span>
That they are willing and open to trying new things.
</li>
<li class='choice answer '>
<span class='result'></span>
That they are sincere and honest with you. 
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='candor#' id='definition-sound' path='audio/wordmeanings/amy-candor'></a>
<em>Candor</em> is the quality of being honest and open in speech or action.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>honesty</em>
</span>
</span>
</div>
<a class='quick-help' href='candor#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='candor#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/candor/memory_hooks/3825.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Cand</span></span>y is D<span class="emp1"><span>and</span></span>y and/<span class="emp2"><span>or</span></span> Bl<span class="emp1"><span>and</span></span>?</span></span>  <span class="emp1"><span>Cand</span></span>y is D<span class="emp1"><span>and</span></span>y!  And if one were speaking with complete <span class="emp1"><span>cand</span></span><span class="emp2"><span>or</span></span>, one would say that <span class="emp1"><span>cand</span></span>y is nothing but d<span class="emp1"><span>and</span></span>y, and certainly NOT <span class="emp1"><span>and</span></span>/<span class="emp2"><span>or</span></span> bl<span class="emp1"><span>and</span></span> but definitely gr<span class="emp1"><span>and</span></span>!
</p>
</div>

<div id='memhook-button-bar'>
<a href="candor#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/candor/memory_hooks">Use other hook</a>
<a href="candor#" id="memhook-use-own" url="https://membean.com/mywords/candor/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='candor#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Charlotte Chandler has long sought out the movie world’s legends and displayed a knack of getting them to open up to her with a <b>candor</b> that is always amazing.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
This kind of <b>candor</b> does not come easily even to great record makers, and Beck, one of our sharpest, has never had much cause for such direct reflection.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
<li>
Parkinson’s disease is mercifully rare in people [Michael J.] Fox’s age, but his <b>candor</b> has helped create a new sense of urgency about the condition.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
What remains to be seen is whether the <b>candor</b> [Obama] offered in his early memoir will be greeted with a new-style acceptance by voters.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/candor/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='candor#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='cand_shine' data-tree-url='//cdn1.membean.com/public/data/treexml' href='candor#'>
<span class=''></span>
cand
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>be of brilliant whiteness, shine, be hot</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='or_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='candor#'>
<span class=''></span>
-or
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state</td>
</tr>
</table>
<p>When one speaks with <em>candor</em>, one uses words that are both in a state of &#8220;brilliant whiteness&#8221; or purity and are &#8220;shining&#8221; bright with truth&#8212;there are no dark impurities or falsehoods in <em>candid</em> statements.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='candor#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Bulworth</strong><span> This candidate's candor is both shocking and unexpected.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/candor.jpg' video_url='examplevids/candor' video_width='350'></span>
<div id='wt-container'>
<img alt="Candor" height="288" src="https://cdn1.membean.com/video/examplevids/candor.jpg" width="350" />
<div class='center'>
<a href="candor#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='candor#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Candor" src="https://cdn1.membean.com/public/images/wordimages/cons2/candor.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='candor#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>artless</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>ingenuous</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>overt</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>scrupulous</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>ambiguous</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>chicanery</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>circuitous</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>circumlocution</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>clandestine</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>collude</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>connive</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>contrived</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>covert</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>delusive</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>disingenuous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>feign</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>hypocritical</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>legerdemain</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>mendacious</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>noncommittal</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>oblique</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>perfidious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>prevaricate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>stratagem</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>wile</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='candor#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
candid
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>sincere and straightforward</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="candor" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>candor</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="candor#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

