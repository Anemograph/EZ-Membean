
<!DOCTYPE html>
<html>
<head>
<title>Word: parody | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Badinage is lighthearted conversation that involves teasing, jokes, and humor.</p>
<p class='rw-defn idx1' style='display:none'>When you bandy about ideas with someone, you casually discuss them without caring if the discussion is informed or effective in any way.</p>
<p class='rw-defn idx2' style='display:none'>A caricature of someone is an exaggerated drawing or description that is designed to make that person look silly; it can also be an exaggerated description of a situation or event.</p>
<p class='rw-defn idx3' style='display:none'>A chimerical idea is unrealistic, imaginary, or unlikely to be fulfilled.</p>
<p class='rw-defn idx4' style='display:none'>If you treat someone with derision, you mock or make fun of them because you think they are stupid, unimportant, or useless.</p>
<p class='rw-defn idx5' style='display:none'>When something is droll, it is humorous in an odd way.</p>
<p class='rw-defn idx6' style='display:none'>Empirical evidence or study is based on real experience or scientific experiments rather than on unproven theories.</p>
<p class='rw-defn idx7' style='display:none'>If you emulate someone, you try to behave the same way they do because you admire them a great deal.</p>
<p class='rw-defn idx8' style='display:none'>An encomium strongly praises someone or something via oral or written communication.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is being facetious says things they intend to be funny but are nevertheless out of place.</p>
<p class='rw-defn idx10' style='display:none'>A situation or event is a farce if it is very badly organized and consequently unsuccessful—or so silly and ridiculous that you cannot take it seriously.</p>
<p class='rw-defn idx11' style='display:none'>When a person is being flippant, they are not taking something as seriously as they should be; therefore, they tend to dismiss things they should respectfully pay attention to.</p>
<p class='rw-defn idx12' style='display:none'>Two items are fungible if one can be exchanged for the other with no loss in inherent value; for example, one $10 bill and two $5 bills are fungible.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is jocular is cheerful and often makes jokes or tries to make people laugh.</p>
<p class='rw-defn idx14' style='display:none'>When someone is lionized, they are treated as being very important or famous—although they may not deserve to be.</p>
<p class='rw-defn idx15' style='display:none'>If you describe something as ludicrous, you mean that it is extremely silly, stupid, or just plain ridiculous.</p>
<p class='rw-defn idx16' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx17' style='display:none'>A panegyric is a speech or article that praises someone or something a lot.</p>
<p class='rw-defn idx18' style='display:none'>A pastiche is a piece of writing, music, or film, etc. that is deliberately made in the style of other works; it can also use a variety of such copied material in order to laugh in a gentle way at those works.</p>
<p class='rw-defn idx19' style='display:none'>If someone is <em>pilloried</em>, they are publicly criticized or ridiculed by a lot of people, especially in the media.</p>
<p class='rw-defn idx20' style='display:none'>Satire is a way of criticizing people or ideas in a humorous way by going beyond the truth; it is often used to make people see their faults.</p>
<p class='rw-defn idx21' style='display:none'>A semblance is an outward appearance of what is wanted or expected but is not exactly as hoped for.</p>
<p class='rw-defn idx22' style='display:none'>If you say that one thing is a travesty of a second thing, you mean that the first is an extremely bad, crude, or absurd version of the second.</p>
<p class='rw-defn idx23' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>parody</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='parody#' id='pronounce-sound' path='audio/words/amy-parody'></a>
PAR-uh-dee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='parody#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='parody#' id='context-sound' path='audio/wordcontexts/brian-parody'></a>
Weird Al Yankovic has made a living by performing <em><em>parodies</em></em>, or humorous imitations of popular music.  The hilarious lyrics of his <em><em>parodies</em></em> make fun of common everyday occurrences, while keeping the same tone and melody of the original songs that he is imitating.  Recording artists approve these <em><em>parodies</em></em> or musical jokes before they are made available to the public, and find enjoyment in the humorous changes Weird Al makes to their songs.  These <em><em>parodies</em></em> of other songs poke fun in a lighthearted manner that most find enjoyable and very funny.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>parody</em> of a political debate?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A humorous but fake debate that makes fun of the real one.
</li>
<li class='choice '>
<span class='result'></span>
An opinion article that analyzes important aspects of the debate.
</li>
<li class='choice '>
<span class='result'></span>
A fact check of all the things that were said during the debate.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='parody#' id='definition-sound' path='audio/wordmeanings/amy-parody'></a>
A <em>parody</em> is a humorous imitation of more serious writing or music.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>humorous imitation</em>
</span>
</span>
</div>
<a class='quick-help' href='parody#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='parody#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/parody/memory_hooks/4595.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Od</span></span>d <span class="emp3"><span>Pair</span></span>, or <span class="emp3"><span>Pair</span></span> <span class="emp1"><span>OD</span></span>?</span></span> Come on, guys, we all know that Harold and Maude were not just an <span class="emp1"><span>od</span></span>d <span class="emp3"><span>pair</span></span>, but that the <span class="emp3"><span>pair</span></span> was probably <span class="emp1"><span>OD</span></span>ing or <span class="emp1"><span>o</span></span>ver<span class="emp1"><span>d</span></span>osing on drugs as well ... but would a <span class="emp3"><span>par</span></span><span class="emp1"><span>od</span></span>y say that they were <span class="emp1"><span>OD</span></span>ing on drugs, or on love?  Only a reading of the <span class="emp3"><span>par</span></span><span class="emp1"><span>od</span></span>y would reveal that!
</p>
</div>

<div id='memhook-button-bar'>
<a href="parody#" id="add-public-hook" style="" url="https://membean.com/mywords/parody/memory_hooks">Use other public hook</a>
<a href="parody#" id="memhook-use-own" url="https://membean.com/mywords/parody/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='parody#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
There are a lot of songs that would ostensibly be a good candidate for <b>parody</b>, yet I can't think of a clever enough idea. Some songs are too repetitive for me to be able to fashion a humorous set of lyrics around.
<cite class='attribution'>
&mdash;
“Weird Al” Yankovic, American musician known for humorous songs
</cite>
</li>
<li>
The internet is full of fake news and <b>parody</b> sites. For a long time, people knew them for what they were: sites aimed at entertaining readers with a satirical take on the news.
<cite class='attribution'>
&mdash;
US News & World Report
</cite>
</li>
<li>
This was not The New York Times; and that, in fact, is exactly what the <b>parody</b> called itself: Not The New York Times. . . . The <b>parody</b> featured three full sections, 24 joke advertisements, 73 spoof articles and 155 fake news briefs, all meticulously edited to mimic The Times’s style.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
The irony is that, in order for _The Onion_ to effectively poke fun at a newspaper, it has look and feel like one. . . . “For our <b>parody</b> to sing, and our satire to work, it has to mimic what everyone else is doing down to every level,” Bolton says. “That’s one of the secret ingredients to making it work: We look like the other news organizations, and then we take the air out of them.”
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/parody/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='parody#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='para_beside' data-tree-url='//cdn1.membean.com/public/data/treexml' href='parody#'>
<span class='common'></span>
para-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>beside, alongside</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='od_song' data-tree-url='//cdn1.membean.com/public/data/treexml' href='parody#'>
<span class=''></span>
od
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>song</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='y_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='parody#'>
<span class=''></span>
-y
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state or condition</td>
</tr>
</table>
<p>A <em>parody</em> is one &#8220;song alongside&#8221; another, usually making fun of it.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='parody#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Music Videos: Michael Jackson's "Beat It" vs. Weird Al Yankovic's "Eat It"</strong><span> Weird Al makes a parody of Michael Jackson's song "Eat It."</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/parody.jpg' video_url='examplevids/parody' video_width='350'></span>
<div id='wt-container'>
<img alt="Parody" height="288" src="https://cdn1.membean.com/video/examplevids/parody.jpg" width="350" />
<div class='center'>
<a href="parody#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='parody#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Parody" src="https://cdn3.membean.com/public/images/wordimages/cons2/parody.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='parody#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>badinage</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>bandy</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>caricature</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>chimerical</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>derision</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>droll</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>facetious</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>farce</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>flippant</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>jocular</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>ludicrous</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>pastiche</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>pillory</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>satire</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>semblance</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>travesty</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='6' class = 'rw-wordform notlearned'><span>empirical</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>emulate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>encomium</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>fungible</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>lionize</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>panegyric</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='parody#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
parody
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to humorously imitate something</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="parody" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>parody</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="parody#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

