
<!DOCTYPE html>
<html>
<head>
<title>Word: unparalleled | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>The acme of something is its highest point of achievement or excellence.</p>
<p class='rw-defn idx1' style='display:none'>When you are adept at something, you are very good, skillful, or talented at it.</p>
<p class='rw-defn idx2' style='display:none'>Someone who is adroit is skillful and clever, especially in thought, behavior, or action.</p>
<p class='rw-defn idx3' style='display:none'>The apex of anything, such as an organization or system, is its highest part or most important position.</p>
<p class='rw-defn idx4' style='display:none'>The apogee of something is its highest or greatest point, especially in reference to a culture or career.</p>
<p class='rw-defn idx5' style='display:none'>The best or perfect example of something is its apotheosis; likewise, this word can be used to indicate the best or highest point in someone&#8217;s life or job.</p>
<p class='rw-defn idx6' style='display:none'>An atrocious deed is outrageously bad, extremely evil, or shocking in its wrongness.</p>
<p class='rw-defn idx7' style='display:none'>When you claim that something is banal, you do not like it because you think it is ordinary, dull, commonplace, and boring.</p>
<p class='rw-defn idx8' style='display:none'>A bravura performance, such as of a highly technical work for the violin, is done with consummate skill.</p>
<p class='rw-defn idx9' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx10' style='display:none'>A definitive opinion on an issue cannot be challenged; therefore, it is the final word or the most authoritative pronouncement on that issue.</p>
<p class='rw-defn idx11' style='display:none'>A dilettante is someone who frequently pursues an interest in a subject; nevertheless, they never spend the time and effort to study it thoroughly enough to master it.</p>
<p class='rw-defn idx12' style='display:none'>When one person eclipses another&#8217;s achievements, they surpass or outshine that person in that endeavor.</p>
<p class='rw-defn idx13' style='display:none'>An exemplary person is one that sets an ideal or praiseworthy example for others to follow.</p>
<p class='rw-defn idx14' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx15' style='display:none'>Someone who has had an illustrious professional career is celebrated and outstanding in their given field of expertise.</p>
<p class='rw-defn idx16' style='display:none'>If you describe someone&#8217;s appearance or behavior as impeccable, you mean that it is perfect and therefore impossible to criticize.</p>
<p class='rw-defn idx17' style='display:none'>Inconsequential matters are unimportant or are of little to no significance.</p>
<p class='rw-defn idx18' style='display:none'>An indomitable foe cannot be beaten.</p>
<p class='rw-defn idx19' style='display:none'>Someone who is ineffectual at a task is useless or ineffective when attempting to do it.</p>
<p class='rw-defn idx20' style='display:none'>Someone who is inept is unable or unsuitable to do a job; hence, they are unfit for it.</p>
<p class='rw-defn idx21' style='display:none'>Something that has inestimable value or benefit has so much of it that it cannot be calculated.</p>
<p class='rw-defn idx22' style='display:none'>Someone, such as a performer or athlete, is inimitable when they are so good or unique in their talent that it is unlikely anyone else can be their equal.</p>
<p class='rw-defn idx23' style='display:none'>Something mediocre is average or ordinary in quality; it’s just OK.</p>
<p class='rw-defn idx24' style='display:none'>Someone who is meritorious is worthy of receiving recognition or is praiseworthy because of what they have accomplished.</p>
<p class='rw-defn idx25' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx26' style='display:none'>The nadir of a situation is its lowest point.</p>
<p class='rw-defn idx27' style='display:none'>Something nonpareil has no equal because it is much better than all others of its kind or type.</p>
<p class='rw-defn idx28' style='display:none'>An omnipotent being or entity is unlimited in its power.</p>
<p class='rw-defn idx29' style='display:none'>A paragon is an example of a thing or person at its or their very best.</p>
<p class='rw-defn idx30' style='display:none'>Anything picayune is unimportant, insignificant, or minor.</p>
<p class='rw-defn idx31' style='display:none'>If someone reaches a pinnacle of something—such as a career or mountain—they have arrived at the highest point of it.</p>
<p class='rw-defn idx32' style='display:none'>Something that is prodigious is very large, impressive, or great.</p>
<p class='rw-defn idx33' style='display:none'>Something prosaic is dull, boring, and ordinary.</p>
<p class='rw-defn idx34' style='display:none'>If you have prowess, you possess considerable skill or ability in something.</p>
<p class='rw-defn idx35' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>
<p class='rw-defn idx36' style='display:none'>A stupendous event or accomplishment is amazing, wonderful, or spectacular.</p>
<p class='rw-defn idx37' style='display:none'>A superlative deed or act is excellent, outstanding, or the best of its kind.</p>
<p class='rw-defn idx38' style='display:none'>Something trivial is of little value or is not important.</p>
<p class='rw-defn idx39' style='display:none'>Something unprecedented has never occurred; therefore, it is unusual, original, or new.</p>
<p class='rw-defn idx40' style='display:none'>If you are unsurpassed in what you do, you are the best—period.</p>
<p class='rw-defn idx41' style='display:none'>A virtuoso is someone who is very skillful at something, especially playing a musical instrument.</p>
<p class='rw-defn idx42' style='display:none'>The zenith of something is its highest, most powerful, or most successful point.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>unparalleled</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='unparalleled#' id='pronounce-sound' path='audio/words/amy-unparalleled'></a>
uhn-PAR-uh-leld
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='unparalleled#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='unparalleled#' id='context-sound' path='audio/wordcontexts/brian-unparalleled'></a>
Brazil&#8217;s performance in the World Cup since it began in 1930 is <em>unparalleled</em> or unmatched by any other country.  It is <em>unparalleled</em> or has no equal in the number of matches the country has won: sixty-seven.  Brazil is also <em>unparalleled</em> or is the all-time champion for number of World Cup wins in the final: five.  Most people will argue that Brazil also had an <em>unparalleled</em> or unique player, the best there ever was: Pele.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of an <em>unparalleled</em> accomplishment?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A swimmer who holds the most world record times.
</li>
<li class='choice '>
<span class='result'></span>
A swimmer who cheats in order to break a world record. 
</li>
<li class='choice '>
<span class='result'></span>
A swimmer who tries but fails to break a world record.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='unparalleled#' id='definition-sound' path='audio/wordmeanings/amy-unparalleled'></a>
An <em>unparalleled</em> accomplishment has not been equaled by anyone or cannot be compared to anything that anyone else has ever done.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>unequaled</em>
</span>
</span>
</div>
<a class='quick-help' href='unparalleled#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='unparalleled#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/unparalleled/memory_hooks/4434.json'></span>
<p>
<span class="emp0"><span>Best <span class="emp3"><span>Parallel</span></span>ogram</span></span> Janie drew the best free-hand <span class="emp3"><span>parallel</span></span>ogram; everyone thought she had traced or drew it with a ruler because it was so un<span class="emp3"><span>parallel</span></span>ed in its perfection.
</p>
</div>

<div id='memhook-button-bar'>
<a href="unparalleled#" id="add-public-hook" style="" url="https://membean.com/mywords/unparalleled/memory_hooks">Use other public hook</a>
<a href="unparalleled#" id="memhook-use-own" url="https://membean.com/mywords/unparalleled/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='unparalleled#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
The unleashed power of the atom has changed everything save our modes of thinking and we thus drift toward <b>unparalleled</b> catastrophe.
<span class='attribution'>&mdash; Albert Einstein</span>
<img alt="Albert einstein" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Albert Einstein.jpg?qdep8" width="80" />
</li>
<li>
Increased official scrutiny of corruption, greater responsiveness to the needs of the people and, perhaps, even an <b>unparalleled</b> economic boom are all part of the June 4 [1989 Tiananmen Square protests] legacy.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
<li>
Another point of fascination is the <b>unparalleled</b> security detail to accompany Obama as he moves from bilateral meetings with Portuguese leaders to the summit venue to the Lisbon Marriott Hotel . . . .
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/unparalleled/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='unparalleled#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='un_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='unparalleled#'>
<span class='common'></span>
un-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not, opposite of</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='para_beside' data-tree-url='//cdn1.membean.com/public/data/treexml' href='unparalleled#'>
<span class='common'></span>
para-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>beside, alongside</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='all_other' data-tree-url='//cdn1.membean.com/public/data/treexml' href='unparalleled#'>
<span class=''></span>
all
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>other, different</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ed_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='unparalleled#'>
<span class=''></span>
-ed
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>having a particular state</td>
</tr>
</table>
<p><em>Parallel</em> lines are lines written exactly &#8220;beside each other;&#8221; so, <em>unparalleled</em> events &#8220;have the state of not (ever having been) beside each other,&#8221; that is, they have never happened before.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='unparalleled#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: NASA First Moon Landing 1969</strong><span> An unparalleled achievement for the human race.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/unparalleled.jpg' video_url='examplevids/unparalleled' video_width='350'></span>
<div id='wt-container'>
<img alt="Unparalleled" height="288" src="https://cdn1.membean.com/video/examplevids/unparalleled.jpg" width="350" />
<div class='center'>
<a href="unparalleled#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='unparalleled#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Unparalleled" src="https://cdn2.membean.com/public/images/wordimages/cons2/unparalleled.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='unparalleled#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>acme</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>adept</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>adroit</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>apex</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>apogee</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>apotheosis</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>bravura</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>definitive</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>eclipse</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>exemplary</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>illustrious</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>impeccable</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>indomitable</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>inestimable</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>inimitable</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>meritorious</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>nonpareil</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>omnipotent</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>paragon</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>pinnacle</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>prodigious</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>prowess</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>stupendous</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>superlative</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>unprecedented</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>unsurpassed</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>virtuoso</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>zenith</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='6' class = 'rw-wordform notlearned'><span>atrocious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>banal</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>dilettante</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>inconsequential</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>ineffectual</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>inept</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>mediocre</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>nadir</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>picayune</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>prosaic</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>trivial</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="unparalleled" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>unparalleled</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="unparalleled#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

