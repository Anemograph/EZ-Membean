
<!DOCTYPE html>
<html>
<head>
<title>Word: longevity | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An anachronism is something that is out of place because it is set in the wrong time period.</p>
<p class='rw-defn idx1' style='display:none'>Something antediluvian is so old or old-fashioned that it seems to belong to a much earlier period in history.</p>
<p class='rw-defn idx2' style='display:none'>Something antiquated is old-fashioned and not suitable for modern needs or conditions.</p>
<p class='rw-defn idx3' style='display:none'>An apprentice is someone who trains under a master in order to learn a trade or other skill.</p>
<p class='rw-defn idx4' style='display:none'>Something that is archaic is out of date or not currently used anymore because it is no longer considered useful or efficient.</p>
<p class='rw-defn idx5' style='display:none'>Someone who is callow is young, immature, and inexperienced; consequently, they possess little knowledge of the world.</p>
<p class='rw-defn idx6' style='display:none'>A centenary period has lasted one hundred years.</p>
<p class='rw-defn idx7' style='display:none'>A chronic illness or pain is serious and lasts for a long time; a chronic problem is always happening or returning and is very difficult to solve.</p>
<p class='rw-defn idx8' style='display:none'>Decrepitude is the state of being very old, worn out, or very ill; therefore, something or someone is no longer in good physical condition or good health.</p>
<p class='rw-defn idx9' style='display:none'>A dilapidated building, vehicle, etc. is old, broken-down, and in very bad condition.</p>
<p class='rw-defn idx10' style='display:none'>Something ephemeral, such as some insects or a sunset, lasts for only a short time or has a very short lifespan.</p>
<p class='rw-defn idx11' style='display:none'>Something that is evanescent lasts for only a short time before disappearing from sight or memory.</p>
<p class='rw-defn idx12' style='display:none'>A fledgling business endeavor is just beginning or developing.</p>
<p class='rw-defn idx13' style='display:none'>A fleeting moment lasts but a short time before it fades away.</p>
<p class='rw-defn idx14' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx15' style='display:none'>An inception of something is its beginning or start.</p>
<p class='rw-defn idx16' style='display:none'>Something that is incessant continues on for a long time without stopping.</p>
<p class='rw-defn idx17' style='display:none'>To initiate something is to begin or start it.</p>
<p class='rw-defn idx18' style='display:none'>Something that is interminable continues for a very long time in a boring or annoying way.</p>
<p class='rw-defn idx19' style='display:none'>If you describe something as moribund, you imply that it is no longer effective; it may also be coming to the end of its useful existence.</p>
<p class='rw-defn idx20' style='display:none'>If something is obsolescent, it is slowly becoming no longer needed because something newer or more effective has been invented.</p>
<p class='rw-defn idx21' style='display:none'>Something that is perennial lasts a very long time and is enduring.</p>
<p class='rw-defn idx22' style='display:none'>Something that is pristine has not lost any of its original perfection or purity.</p>
<p class='rw-defn idx23' style='display:none'>A provisional measure is temporary or conditional until more permanent action is taken.</p>
<p class='rw-defn idx24' style='display:none'>A senescent person is becoming old and showing the effects of getting older.</p>
<p class='rw-defn idx25' style='display:none'>Something that is superannuated is so old and worn out that it is no longer working or useful.</p>
<p class='rw-defn idx26' style='display:none'>Something that is temporal deals with the present and somewhat brief time of this world.</p>
<p class='rw-defn idx27' style='display:none'>Something that has the quality of transience lasts for only a short time or is constantly changing.</p>
<p class='rw-defn idx28' style='display:none'>A tyro has just begun learning something.</p>
<p class='rw-defn idx29' style='display:none'>Venerable people command respect because they are old and wise.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>longevity</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='longevity#' id='pronounce-sound' path='audio/words/amy-longevity'></a>
lon-JEV-i-tee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='longevity#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='longevity#' id='context-sound' path='audio/wordcontexts/brian-longevity'></a>
The <em>longevity</em> of the computer battery is about two years; after that, it can die at any time.  To improve its <em>longevity</em> or life span for another year or so, it&#8217;s best to completely let it drain before recharging.  It&#8217;s good to know, with such a short <em>longevity</em> or duration of usefulness, that many can be recycled rather than dumped in landfills.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean if a book has <em>longevity</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is large in size and is not easy to carry around.
</li>
<li class='choice answer '>
<span class='result'></span>
It has been in print and out in the world for a long time.
</li>
<li class='choice '>
<span class='result'></span>
It has many pages and takes a long time to read.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='longevity#' id='definition-sound' path='audio/wordmeanings/amy-longevity'></a>
<em>Longevity</em> is the life span of a person or object; it can also refer to a particularly long life.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>life span</em>
</span>
</span>
</div>
<a class='quick-help' href='longevity#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='longevity#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/longevity/memory_hooks/5062.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Long</span></span> Acti<span class="emp1"><span>vity</span></span></span></span> The greater the <span class="emp2"><span>long</span></span>e<span class="emp1"><span>vity</span></span> of a product, the <span class="emp2"><span>long</span></span>er its useful acti<span class="emp1"><span>vity</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="longevity#" id="add-public-hook" style="" url="https://membean.com/mywords/longevity/memory_hooks">Use other public hook</a>
<a href="longevity#" id="memhook-use-own" url="https://membean.com/mywords/longevity/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='longevity#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
The quality, not the <b>longevity</b>, of one's life is what is important.
<span class='attribution'>&mdash; Martin Luther King, Jr. </span>
<img alt="Martin luther king, jr" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Martin Luther King, Jr. .jpg?qdep8" width="80" />
</li>
<li>
China is an ideal setting to study <b>longevity</b> because its population of 1.3 billion has one of the largest elderly populations in the world.
<cite class='attribution'>
&mdash;
WebMD
</cite>
</li>
<li>
These built-in support systems are key components of <b>longevity</b>, too, Buettner believes, and just as important as the good food.
<cite class='attribution'>
&mdash;
INSIDER
</cite>
</li>
<li>
The prize has two categories: a <b>longevity</b> prize for research in developing mice that live much longer; and a rejuvenation prize for finding ways to extend the life of an adult mouse, which Spindler won.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/longevity/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='longevity#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='long_long' data-tree-url='//cdn1.membean.com/public/data/treexml' href='longevity#'>
<span class=''></span>
long
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>long, stretched out</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ev_time' data-tree-url='//cdn1.membean.com/public/data/treexml' href='longevity#'>
<span class=''></span>
ev
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>time, life, age</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ity_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='longevity#'>
<span class=''></span>
-ity
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state or quality</td>
</tr>
</table>
<p><em>Longevity</em> is the &#8220;quality or state of possessing a long life,&#8221; or how &#8220;long the life&#8221; of something or someone is.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='longevity#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>World's Oldest Man</strong><span> Jirouemon Kimura's longevity is incredible--114 years and still ticking!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/longevity.jpg' video_url='examplevids/longevity' video_width='350'></span>
<div id='wt-container'>
<img alt="Longevity" height="198" src="https://cdn1.membean.com/video/examplevids/longevity.jpg" width="350" />
<div class='center'>
<a href="longevity#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='longevity#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Longevity" src="https://cdn1.membean.com/public/images/wordimages/cons2/longevity.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='longevity#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>anachronism</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>antediluvian</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>antiquated</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>archaic</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>centenary</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>chronic</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>decrepitude</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>dilapidated</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>incessant</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>interminable</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>moribund</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>obsolescent</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>perennial</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>senescent</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>superannuated</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>temporal</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>venerable</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>apprentice</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>callow</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>ephemeral</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>evanescent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>fledgling</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>fleeting</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>inception</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>initiate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>pristine</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>provisional</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>transience</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>tyro</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="longevity" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>longevity</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="longevity#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

