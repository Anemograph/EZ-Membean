
<!DOCTYPE html>
<html>
<head>
<title>Word: loathsome | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abhor something, you dislike it very much, usually because you think it&#8217;s immoral.</p>
<p class='rw-defn idx1' style='display:none'>If you abominate something, you hate it because you think it is extremely wrong and unacceptable.</p>
<p class='rw-defn idx2' style='display:none'>To accentuate something is to emphasize it or make it more noticeable.</p>
<p class='rw-defn idx3' style='display:none'>If you are amenable to doing something, you willingly accept it without arguing.</p>
<p class='rw-defn idx4' style='display:none'>If you have animus against someone, you have a strong feeling of dislike or hatred towards them, often without a good reason or based upon your personal prejudices.</p>
<p class='rw-defn idx5' style='display:none'>Antipathy is a strong feeling of dislike or hostility towards someone or something.</p>
<p class='rw-defn idx6' style='display:none'>When you are captivated by someone or something, you are enchanted, fascinated, or delighted by them or it.</p>
<p class='rw-defn idx7' style='display:none'>A person who is comely is attractive; this adjective is usually used with females, but not always.</p>
<p class='rw-defn idx8' style='display:none'>A man who is dapper has a very neat and clean appearance; he is both fashionable and stylish.</p>
<p class='rw-defn idx9' style='display:none'>A man who is debonair wears fashionable clothes and is sophisticated, charming, friendly, and confident.</p>
<p class='rw-defn idx10' style='display:none'>If you say a person&#8217;s actions are despicable, you think they are extremely unpleasant or nasty.</p>
<p class='rw-defn idx11' style='display:none'>When you are disposed towards a particular action or thing, you are inclined or partial towards it.</p>
<p class='rw-defn idx12' style='display:none'>If something enthralls you, it makes you so interested and excited that you give it all your attention.</p>
<p class='rw-defn idx13' style='display:none'>When you execrate someone, you curse them to show your intense dislike or hatred of them.</p>
<p class='rw-defn idx14' style='display:none'>Something grotesque is so distorted or misshapen that it is disturbing, bizarre, gross, or very ugly.</p>
<p class='rw-defn idx15' style='display:none'>If you describe something as heinous, you mean that is extremely evil, shocking, and bad.</p>
<p class='rw-defn idx16' style='display:none'>Something macabre is so gruesome or frightening that it causes great horror in those who see it.</p>
<p class='rw-defn idx17' style='display:none'>If something is noisome, it is extremely unpleasant, especially because it is very dirty or has an awful smell.</p>
<p class='rw-defn idx18' style='display:none'>Something that is noxious is harmful, extremely unpleasant, and often poisonous.</p>
<p class='rw-defn idx19' style='display:none'>If you describe people or things as odious, you think that they are extremely unpleasant.</p>
<p class='rw-defn idx20' style='display:none'>If you describe something as palatable, such as an idea or suggestion, you believe that people will accept it; palatable food or drink is agreeable to the taste.</p>
<p class='rw-defn idx21' style='display:none'>If you have a predilection for something, you have a preference for it.</p>
<p class='rw-defn idx22' style='display:none'>Animals preen when they smooth out their fur or their feathers; humans preen by making themselves beautiful in front of a mirror.</p>
<p class='rw-defn idx23' style='display:none'>When you possess pulchritude, you are beautiful.</p>
<p class='rw-defn idx24' style='display:none'>Repugnance is a strong feeling of dislike for something or someone you find horrible and offensive.</p>
<p class='rw-defn idx25' style='display:none'>Something that is repulsive is offensive, highly unpleasant, or just plain disgusting.</p>
<p class='rw-defn idx26' style='display:none'>People or things that are resplendent are beautiful, bright, and impressive in appearance.</p>
<p class='rw-defn idx27' style='display:none'>When you experience revulsion, you feel a great deal of disgust or extreme dislike for something.</p>
<p class='rw-defn idx28' style='display:none'>Something that is tantalizing is desirable and exciting; nevertheless, it may be out of reach, perhaps due to being too expensive.</p>
<p class='rw-defn idx29' style='display:none'>Something is toothsome when it is tasty, attractive, or pleasing in some way; this adjective can apply to food, people, or objects.</p>
<p class='rw-defn idx30' style='display:none'>If you describe something as unsavory, you mean that it is unpleasant or morally unacceptable.</p>
<p class='rw-defn idx31' style='display:none'>An unsightly person presents an ugly, unattractive, or disagreeable face to the world.</p>
<p class='rw-defn idx32' style='display:none'>Something vile is evil, very unpleasant, horrible, or extremely bad.</p>
<p class='rw-defn idx33' style='display:none'>Someone who is winsome is attractive and charming.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>loathsome</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='loathsome#' id='pronounce-sound' path='audio/words/amy-loathsome'></a>
LOHTH-suhm
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='loathsome#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='loathsome#' id='context-sound' path='audio/wordcontexts/brian-loathsome'></a>
Cleaning out garbage cans can be a <em>loathsome</em> or gross activity, especially if food has been thrown in the cans and maggots are infesting it.  Another <em>loathsome</em> or disgusting job is cleaning gunk out of the sink drain, especially when it&#8217;s all black and sticky with hair attached to it.  Imagine how <em>loathsome</em> or truly nasty it must be to clean out port-a-potties, especially ones with puke in them!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How would you feel about being given a <em>loathsome</em> task to do?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
You would be honored that someone else believes you can take on such an important task.
</li>
<li class='choice answer '>
<span class='result'></span>
You would be revolted and possibly offended about having to do such a disgusting job.
</li>
<li class='choice '>
<span class='result'></span>
You would be worried that you may not be able to finish such a large task by yourself.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='loathsome#' id='definition-sound' path='audio/wordmeanings/amy-loathsome'></a>
Something <em>loathsome</em> is offensive, disgusting, and brings about intense dislike.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>highly offensive</em>
</span>
</span>
</div>
<a class='quick-help' href='loathsome#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='loathsome#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/loathsome/memory_hooks/4427.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Lot</span></span>s of <span class="emp2"><span>S</span></span>c<span class="emp2"><span>um</span></span></span></span> <span class="emp1"><span>Lot</span></span>s of <span class="emp2"><span>s</span></span>c<span class="emp2"><span>um</span></span> in the bathtub is just <span class="emp1"><span>lo</span></span>a<span class="emp1"><span>t</span></span>h<span class="emp2"><span>som</span></span>e!
</p>
</div>

<div id='memhook-button-bar'>
<a href="loathsome#" id="add-public-hook" style="" url="https://membean.com/mywords/loathsome/memory_hooks">Use other public hook</a>
<a href="loathsome#" id="memhook-use-own" url="https://membean.com/mywords/loathsome/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='loathsome#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Instead of being presented with stereotypes by age, sex, color, class, or religion, children must have the opportunity to learn that within each range, some people are <b>loathsome</b> and some are delightful.
<span class='attribution'>&mdash; Margaret Mead, American cultural anthropologist</span>
<img alt="Margaret mead, american cultural anthropologist" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Margaret Mead, American cultural anthropologist.jpg?qdep8" width="80" />
</li>
<li>
The anthropologist Marvin Harris devotes a chapter in his book _Good to Eat_ to insect eating, exploring the many reasons why insects can become a taboo or reviled foodstuff (cleanliness, difficulty to collect, cultural attitudes, etc.). For example, Harris writes "The reason we don’t eat them is not that they are dirty and <b>loathsome</b>; rather, they are dirty and <b>loathsome</b> because we don’t eat them."
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
The brew fermented for days, killing off soil bacteria introduced by the vegetables, reeking of garlic and turning downright nasty. “With the nine-day waiting period, the preparation turned into a kind of <b>loathsome</b>, odorous slime," reported a colleague.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
"Well, my dear Frank," begins her letter, "I see that we have arrived at that dreadful time when all you sports authorities are employing that horrid phrase —'on the bubble'—surely the most <b>loathsome</b> term to soil the beauty of athletics. If you could please let your brethren in your benighted profession know how distasteful to both the ear and eye is this wretched expression, I would forever be in your debt."
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/loathsome/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='loathsome#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>loath</td>
<td>
&rarr;
</td>
<td class='meaning'>be hateful, feel disgust</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='some_having' data-tree-url='//cdn1.membean.com/public/data/treexml' href='loathsome#'>
<span class=''></span>
-some
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>having a certain quality</td>
</tr>
</table>
<p>A <em>loathsome</em> person has the &#8220;quality of being disgusting&#8221; to other people, and so much &#8220;hate&#8221; is felt towards him.</p>
<a href="loathsome#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> Old English</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='loathsome#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Return of the Jedi</strong><span> Jabba the Hutt is one loathsome creature.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/loathsome.jpg' video_url='examplevids/loathsome' video_width='350'></span>
<div id='wt-container'>
<img alt="Loathsome" height="288" src="https://cdn1.membean.com/video/examplevids/loathsome.jpg" width="350" />
<div class='center'>
<a href="loathsome#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='loathsome#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Loathsome" src="https://cdn2.membean.com/public/images/wordimages/cons2/loathsome.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='loathsome#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abhor</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abominate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>animus</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>antipathy</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>despicable</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>execrate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>grotesque</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>heinous</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>macabre</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>noisome</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>noxious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>odious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>repugnance</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>repulsive</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>revulsion</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>unsavory</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>unsightly</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>vile</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>accentuate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>amenable</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>captivate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>comely</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>dapper</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>debonair</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>disposed</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>enthrall</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>palatable</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>predilection</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>preen</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>pulchritude</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>resplendent</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>tantalize</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>toothsome</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>winsome</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='loathsome#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
loathe
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to hate or greatly dislike</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="loathsome" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>loathsome</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="loathsome#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

