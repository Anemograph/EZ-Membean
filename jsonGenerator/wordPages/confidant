
<!DOCTYPE html>
<html>
<head>
<title>Word: confidant | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abjure a belief or a way of behaving, you state publicly that you will give it up or reject it.</p>
<p class='rw-defn idx1' style='display:none'>To abrogate an act is to end it by official and sometimes legal means.</p>
<p class='rw-defn idx2' style='display:none'>When you are in accord with someone, you are in agreement or harmony with them.</p>
<p class='rw-defn idx3' style='display:none'>An adherent is a supporter or follower of a leader or a cause.</p>
<p class='rw-defn idx4' style='display:none'>When you advocate a plan or action, you publicly push for implementing it.</p>
<p class='rw-defn idx5' style='display:none'>If you have an affiliation with a group or another person, you are officially involved or connected with them.</p>
<p class='rw-defn idx6' style='display:none'>When you act in an aloof fashion towards others, you purposely do not participate in activities with them; instead, you remain distant and detached.</p>
<p class='rw-defn idx7' style='display:none'>Amity is a peaceful friendship between two parties, particularly between two countries.</p>
<p class='rw-defn idx8' style='display:none'>Antipathy is a strong feeling of dislike or hostility towards someone or something.</p>
<p class='rw-defn idx9' style='display:none'>An autonomous person makes their own decisions without being influenced by anyone else; an autonomous country or organization is independent and has the power to govern itself.</p>
<p class='rw-defn idx10' style='display:none'>Someone who has a bristling personality is easily offended, annoyed, or angered.</p>
<p class='rw-defn idx11' style='display:none'>If you describe a person&#8217;s behavior or speech as brusque, you mean that they say or do things abruptly or too quickly in a somewhat rude and impolite way.</p>
<p class='rw-defn idx12' style='display:none'>A cadre is a group specifically trained to lead, formalize, and accomplish a particular task or to train others as part of a larger organization; it can also be used to describe an elite military force.</p>
<p class='rw-defn idx13' style='display:none'>You describe someone as a charlatan if they pretend to have special knowledge or skill that they don&#8217;t actually possess.</p>
<p class='rw-defn idx14' style='display:none'>A coalition is a temporary union of different political or social groups that agrees to work together to achieve a shared aim.</p>
<p class='rw-defn idx15' style='display:none'>The adjective conjugal refers to marriage or the relationship between two married people.</p>
<p class='rw-defn idx16' style='display:none'>When you consign someone into another&#8217;s care, you sign them over or entrust them to that person&#8217;s protection.</p>
<p class='rw-defn idx17' style='display:none'>A coterie is a small group of people who are close friends; therefore, when its members do things together, they do not want other people to join them.</p>
<p class='rw-defn idx18' style='display:none'>Credence is the mental act of believing or accepting that something is true.</p>
<p class='rw-defn idx19' style='display:none'>Something that is delusive deceives you by giving a false belief about yourself or the situation you are in.</p>
<p class='rw-defn idx20' style='display:none'>A situation or thing that is discordant does not fit in with other things; therefore, it is disagreeable, strange, or unpleasant.</p>
<p class='rw-defn idx21' style='display:none'>Someone who is disingenuous is not straightforward or is dishonest in what they say or do.</p>
<p class='rw-defn idx22' style='display:none'>Dissolution is the breaking up or official end of a group, such as a couple or institution; it can also be the act of separating something into smaller components.</p>
<p class='rw-defn idx23' style='display:none'>An entourage is a group of assistants, servants, and other people who tag along with an important person.</p>
<p class='rw-defn idx24' style='display:none'>If you espouse an idea, principle, or belief, you support it wholeheartedly.</p>
<p class='rw-defn idx25' style='display:none'>Fidelity towards something, such as a marriage or promise, is faithfulness or loyalty towards it.</p>
<p class='rw-defn idx26' style='display:none'>A fiduciary relationship arises when one individual has the trust and confidence of another, such as a financial client; subsequently, that trustworthy person is entrusted with the management or protection of the client&#8217;s money.</p>
<p class='rw-defn idx27' style='display:none'>If you flout a rule or custom, you deliberately refuse to conform to it.</p>
<p class='rw-defn idx28' style='display:none'>If you gainsay something, you say that it is not true and therefore reject it.</p>
<p class='rw-defn idx29' style='display:none'>People who are ingenuous are excessively trusting and believe almost everything that people tell them, especially because they have not had much life experience.</p>
<p class='rw-defn idx30' style='display:none'>If someone is inimical, they are unfriendly and will likely cause you harm.</p>
<p class='rw-defn idx31' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx32' style='display:none'>Liaison is the cooperation and exchange of information between people or organizations in order to facilitate their joint efforts; this word also refers to a person who coordinates a connection between people or organizations.</p>
<p class='rw-defn idx33' style='display:none'>A mendacious person does not tell the truth.</p>
<p class='rw-defn idx34' style='display:none'>A mountebank is a smooth-talking, dishonest person who tricks and cheats people.</p>
<p class='rw-defn idx35' style='display:none'>If someone is ostracized from a group, its members deliberately refuse to talk or listen to them and do not allow them to take part in any of their social activities.</p>
<p class='rw-defn idx36' style='display:none'>If you are afflicted with paranoia, you have extreme distrust of others and feel like they are always out to get you.</p>
<p class='rw-defn idx37' style='display:none'>A poseur pretends to have a certain quality or social position, usually because they want to influence others in some way.</p>
<p class='rw-defn idx38' style='display:none'>If you prevaricate, you avoid giving a direct or honest answer, usually because you want to hide the truth or want to delay or avoid making a hard decision.</p>
<p class='rw-defn idx39' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx40' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx41' style='display:none'>If you reciprocate, you give something to someone because they have given something similar to you.</p>
<p class='rw-defn idx42' style='display:none'>A recluse is someone who chooses to live alone and deliberately avoids other people.</p>
<p class='rw-defn idx43' style='display:none'>If you repudiate something, you state that you do not accept or agree with it and no longer want to be connected with it in any way.</p>
<p class='rw-defn idx44' style='display:none'>Synergy is the extra energy or additional effectiveness gained when two groups or organizations combine their efforts instead of working separately.</p>
<p class='rw-defn idx45' style='display:none'>Doing something in unison is doing it all together at one time.</p>
<p class='rw-defn idx46' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx47' style='display:none'>The verity of something is the truth or reality of it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>confidant</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='confidant#' id='pronounce-sound' path='audio/words/amy-confidant'></a>
KON-fi-dant
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='confidant#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='confidant#' id='context-sound' path='audio/wordcontexts/brian-confidant'></a>
Zelda, my trusted friend, is my one and only <em>confidant</em>&#8212;I can tell her anything!  As my <em>confidant</em>, she knows what secrets to keep; because of that, I have the utmost faith in her.  I am her <em>confidant</em> or close friend as well, so any secrets we share will go with us to the grave!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If you are a <em>confidant</em> to someone, what are you?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
You are someone with whom they can speak honestly.
</li>
<li class='choice '>
<span class='result'></span>
You are someone they admire and look to for advice.
</li>
<li class='choice '>
<span class='result'></span>
You are someone who encourages them to take risks.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='confidant#' id='definition-sound' path='audio/wordmeanings/amy-confidant'></a>
A <em>confidant</em> is a close and trusted friend to whom you can tell secret matters of importance and remain assured that they will be kept safe.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>trusted person</em>
</span>
</span>
</div>
<a class='quick-help' href='confidant#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='confidant#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/confidant/memory_hooks/6251.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Confident</span></span></span></span> You would tell your deepest, darkest secret to a trusted <span class="emp3"><span>confidant</span></span> because you are <span class="emp3"><span>confident</span></span> that he will not tell anyone else.
</p>
</div>

<div id='memhook-button-bar'>
<a href="confidant#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/confidant/memory_hooks">Use other hook</a>
<a href="confidant#" id="memhook-use-own" url="https://membean.com/mywords/confidant/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='confidant#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The lesson to those around [Mark] Zuckerberg was clear: Nobody, not even the college roommate who had once been his closest <b>confidant</b>, was going to stand in his way.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
<li>
General Fabian Ver, [age] 64, chief of staff of the armed forces and a loyal <b>confidant</b> of President Ferdinand Marcos[,] was the official ultimately responsible for security at the airport.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/confidant/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='confidant#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='con_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='confidant#'>
<span class='common'></span>
con-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='fid_trust' data-tree-url='//cdn1.membean.com/public/data/treexml' href='confidant#'>
<span class=''></span>
fid
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>trust, faith</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ant_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='confidant#'>
<span class=''></span>
-ant
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p>A <em>confidant</em> is in the &#8220;state or condition&#8221; of being able to be &#8220;thoroughly trusted&#8221; by another.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='confidant#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Community</strong><span> It makes life easier to have a confidant that you can trust.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/confidant.jpg' video_url='examplevids/confidant' video_width='350'></span>
<div id='wt-container'>
<img alt="Confidant" height="288" src="https://cdn1.membean.com/video/examplevids/confidant.jpg" width="350" />
<div class='center'>
<a href="confidant#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='confidant#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Confidant" src="https://cdn0.membean.com/public/images/wordimages/cons2/confidant.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='confidant#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>accord</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>adherent</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>advocate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>affiliation</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>amity</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>cadre</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>coalition</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>conjugal</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>consign</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>coterie</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>credence</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>entourage</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>espouse</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>fidelity</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>fiduciary</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>ingenuous</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>liaison</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>reciprocate</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>synergy</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>unison</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abjure</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abrogate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>aloof</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>antipathy</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>autonomous</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>bristling</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>brusque</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>charlatan</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>delusive</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>discordant</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>disingenuous</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>dissolution</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>flout</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>gainsay</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>inimical</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>mendacious</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>mountebank</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>ostracize</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>paranoia</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>poseur</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>prevaricate</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>recluse</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>repudiate</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='confidant#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
confidante
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>a specifically female confidant</td>
</tr>
<tr>
<td class='wordform'>
<span>
confide
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to trust someone with a private affair</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="confidant" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>confidant</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="confidant#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

