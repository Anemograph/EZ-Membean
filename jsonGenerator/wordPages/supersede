
<!DOCTYPE html>
<html>
<head>
<title>Word: supersede | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If someone abdicates, they give up their responsibility for something, such as a king&#8217;s transfer of power when he gives up his throne.</p>
<p class='rw-defn idx1' style='display:none'>An antecedent of something, such as an event or organization, has happened or existed before it and can be similar to it.</p>
<p class='rw-defn idx2' style='display:none'>When one person eclipses another&#8217;s achievements, they surpass or outshine that person in that endeavor.</p>
<p class='rw-defn idx3' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx4' style='display:none'>If someone is implacable, they very stubbornly react to situations or the opinions of others because of strong feelings that make them unwilling to change their mind.</p>
<p class='rw-defn idx5' style='display:none'>If someone leaves an indelible impression on you, it will not be forgotten; an indelible mark is permanent or impossible to erase or remove.</p>
<p class='rw-defn idx6' style='display:none'>The adjective inexorable describes a process that is impossible to stop once it has begun.</p>
<p class='rw-defn idx7' style='display:none'>An insurrection is a rebellion or open uprising against an established form of government.</p>
<p class='rw-defn idx8' style='display:none'>Something that is interminable continues for a very long time in a boring or annoying way.</p>
<p class='rw-defn idx9' style='display:none'>An inveterate person is always doing a particular thing, especially something questionable—and they are not likely to stop doing it.</p>
<p class='rw-defn idx10' style='display:none'>An irrevocable action or decision is impossible to change, reverse, or stop.</p>
<p class='rw-defn idx11' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx12' style='display:none'>Something that is perennial lasts a very long time and is enduring.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is pertinacious is determined to continue doing something rather than giving up—even when it gets very difficult.</p>
<p class='rw-defn idx14' style='display:none'>A first event is a precursor to a second event if the first event is responsible for the development or existence of the second.</p>
<p class='rw-defn idx15' style='display:none'>A predecessor comes before someone else in a job or is an ancestor of someone.</p>
<p class='rw-defn idx16' style='display:none'>When you relinquish something, you give it up or let it go.</p>
<p class='rw-defn idx17' style='display:none'>Sabotage is the deliberate or intentional destruction or damage of property or equipment in order to defeat or slow down a cause or other endeavor you don&#8217;t agree with.</p>
<p class='rw-defn idx18' style='display:none'>Sedition is the act of encouraging people to disobey and oppose the government currently in power.</p>
<p class='rw-defn idx19' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx20' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx21' style='display:none'>To supplant someone is to replace them with someone else, usually because the latter is more powerful or better in some way.</p>
<p class='rw-defn idx22' style='display:none'>To undermine a plan or endeavor is to weaken it or make it unstable in a gradual but purposeful fashion.</p>
<p class='rw-defn idx23' style='display:none'>When you usurp someone else&#8217;s power, position, or role, you take it from them although you do not have the right to do so.</p>
<p class='rw-defn idx24' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>supersede</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='supersede#' id='pronounce-sound' path='audio/words/amy-supersede'></a>
soo-per-SEED
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='supersede#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='supersede#' id='context-sound' path='audio/wordcontexts/brian-supersede'></a>
It&#8217;s amazing how often some products are <em>superseded</em> or replaced by other products.  For instance, certain models of running shoes are soon <em>superseded</em> or displaced by an update or another model altogether, often after being on the market for a very short time.  Smartphones tend to be very quickly <em>superseded</em> or taken over by updated versions of the same phone, usually because its technology has already become outdated.  I hope that my new girlfriend doesn&#8217;t decide that I need to be <em>superseded</em> or succeeded by someone else who she thinks is better than I am!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If your team <em>supersedes</em> the first-place team in a tournament, what has happened?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
You have been chosen to play that team for the championship.
</li>
<li class='choice '>
<span class='result'></span>
You lost an important game to that team.
</li>
<li class='choice answer '>
<span class='result'></span>
Your team is now in first place.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='supersede#' id='definition-sound' path='audio/wordmeanings/amy-supersede'></a>
When something <em>supersedes</em> another thing, it takes the place of or succeeds it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>replace</em>
</span>
</span>
</div>
<a class='quick-help' href='supersede#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='supersede#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/supersede/memory_hooks/3327.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Succeed</span></span>s</span></span> When one CEO <span class="emp2"><span>su</span></span>per<span class="emp2"><span>sede</span></span>s another, she <span class="emp2"><span>succeed</span></span>s him in that position.
</p>
</div>

<div id='memhook-button-bar'>
<a href="supersede#" id="add-public-hook" style="" url="https://membean.com/mywords/supersede/memory_hooks">Use other public hook</a>
<a href="supersede#" id="memhook-use-own" url="https://membean.com/mywords/supersede/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='supersede#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Charles asks whether contract law can <b>supersede</b> copyright law since copyright law is federal, and contract law is not, and federal law trumps all inconsistent law that is not federal. A simple answer, though misleading, is no: contract law cannot <b>supersede</b> copyright law. But the problem is in determining what types of contracts would <b>supersede</b> copyright law.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
Supporters of the Schindlers accused the courts of infringing on Schiavo’s religious freedom, saying, as they have before, that "God’s law should <b>supersede</b> man’s law."
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
"It’s difficult to see how an administrative order can <b>supersede</b> an explicit provision of law," says Alicia Trost, press attaché for Senate President Pro Tem Darrell Steinberg.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/supersede/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='supersede#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='super_over' data-tree-url='//cdn1.membean.com/public/data/treexml' href='supersede#'>
<span class='common'></span>
super-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>over, above</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='sed_sit' data-tree-url='//cdn1.membean.com/public/data/treexml' href='supersede#'>
<span class=''></span>
sed
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>sit, settle, rest</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='supersede#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>When one thing <em>supersedes</em> another, it &#8220;settles above&#8221; or &#8220;sits over&#8221; it.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='supersede#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>TV Show: LOCAL 12</strong><span> Stephen Colbert supersedes David Letterman on the Late Show.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/supersede.jpg' video_url='examplevids/supersede' video_width='350'></span>
<div id='wt-container'>
<img alt="Supersede" height="288" src="https://cdn1.membean.com/video/examplevids/supersede.jpg" width="350" />
<div class='center'>
<a href="supersede#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='supersede#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Supersede" src="https://cdn2.membean.com/public/images/wordimages/cons2/supersede.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='supersede#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>eclipse</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>insurrection</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>sabotage</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>sedition</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>supplant</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>undermine</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>usurp</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abdicate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>antecedent</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>implacable</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>indelible</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>inexorable</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>interminable</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>inveterate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>irrevocable</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>perennial</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>pertinacious</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>precursor</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>predecessor</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>relinquish</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="supersede" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>supersede</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="supersede#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

