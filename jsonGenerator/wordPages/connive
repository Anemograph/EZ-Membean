
<!DOCTYPE html>
<html>
<head>
<title>Word: connive | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you employ artifice, you use clever tricks and cunning to deceive someone.</p>
<p class='rw-defn idx1' style='display:none'>An autonomous person makes their own decisions without being influenced by anyone else; an autonomous country or organization is independent and has the power to govern itself.</p>
<p class='rw-defn idx2' style='display:none'>A cabal is a group of people who secretly meet to plan things in order to gain power.</p>
<p class='rw-defn idx3' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx4' style='display:none'>Something that is clandestine is hidden or kept a secret, often because it is illegal.</p>
<p class='rw-defn idx5' style='display:none'>If you collude with people, you work secretly with them to do something dishonest.</p>
<p class='rw-defn idx6' style='display:none'>Complicity is the involvement in or knowledge of a situation that is illegal or bad.</p>
<p class='rw-defn idx7' style='display:none'>A conclave is a meeting between a group of people who discuss something secretly.</p>
<p class='rw-defn idx8' style='display:none'>If you are culpable for an action, you are held responsible for something wrong or bad that has happened.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is disingenuous is not straightforward or is dishonest in what they say or do.</p>
<p class='rw-defn idx10' style='display:none'>If you exculpate someone, you prove that they are not guilty of a crime.</p>
<p class='rw-defn idx11' style='display:none'>When someone is exonerated from guilt, a task, or a responsibility, they are set free or excused from it.</p>
<p class='rw-defn idx12' style='display:none'>If someone behaves in a furtive way, they do things sneakily and secretly in order to avoid being noticed.</p>
<p class='rw-defn idx13' style='display:none'>To gerrymander a voting district is to change its physical boundaries in order to include more people who vote in a particular way.</p>
<p class='rw-defn idx14' style='display:none'>People who are ingenuous are excessively trusting and believe almost everything that people tell them, especially because they have not had much life experience.</p>
<p class='rw-defn idx15' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx16' style='display:none'>A machination is a secretive plan or clever plot that is carefully designed to control events or people.</p>
<p class='rw-defn idx17' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx18' style='display:none'>An overt act is not hidden or secret but is done in an open and public way.</p>
<p class='rw-defn idx19' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx20' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx21' style='display:none'>To be punctilious is to pay precise attention to detail.</p>
<p class='rw-defn idx22' style='display:none'>A scrupulous person takes great care to do everything in an extremely honest, conscientious, and fair manner.</p>
<p class='rw-defn idx23' style='display:none'>Something statutory, such as the power given to a governor or president, is created, established, and controlled by rules and laws; hence, it has the full force of the law behind it and must be followed.</p>
<p class='rw-defn idx24' style='display:none'>If you employ subterfuge, you use a secret plan or action to get what you want by outwardly doing one thing that cleverly hides your true intentions.</p>
<p class='rw-defn idx25' style='display:none'>A surreptitious deed is done secretly to avoid bringing any attention to it.</p>
<p class='rw-defn idx26' style='display:none'>Synergy is the extra energy or additional effectiveness gained when two groups or organizations combine their efforts instead of working separately.</p>
<p class='rw-defn idx27' style='display:none'>A tacit agreement between two people is understood without having to use words to express it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>connive</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='connive#' id='pronounce-sound' path='audio/words/amy-connive'></a>
kuh-NAHYV
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='connive#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='connive#' id='context-sound' path='audio/wordcontexts/brian-connive'></a>
There once was a famous case of baggage handlers who secretly schemed or <em>connived</em> to smuggle illegal green gophers into the luggage of innocent travelers.  The <em>conniving</em> baggage handlers at one airport plotted to put the sleeping gophers into marked suitcases.  At the receiving end, the other <em>conniving</em> and dishonestly cooperating handlers would look out for the marked luggage pieces and extract the gophers before the pieces of luggage went off to customs to be inspected.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When have you <em>connived</em> with someone?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When you agree to work together on an upcoming project.
</li>
<li class='choice '>
<span class='result'></span>
When you cooperate with them in an ongoing investigation.
</li>
<li class='choice answer '>
<span class='result'></span>
When you plot with them to do something against the law.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='connive#' id='definition-sound' path='audio/wordmeanings/amy-connive'></a>
If one person <em>connives</em> with another, they secretly plan to achieve something of mutual benefit, usually a thing that is illegal or immoral.
</li>
<li class='def-text'>
One can also <em>connive</em> at a wrongdoing by others by not taking action against it or not reporting it to authorities.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>scheme</em>
</span>
</span>
</div>
<a class='quick-help' href='connive#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='connive#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/connive/memory_hooks/4650.json'></span>
<p>
<img alt="Connive" src="https://cdn1.membean.com/public/images/wordimages/hook/connive.jpg?qdep8" />
<span class="emp0"><span><span class="emp2"><span>Con</span></span> Men Smuggle K<span class="emp3"><span>niv</span></span>es</span></span> Officials <span class="emp2"><span>con</span></span><span class="emp3"><span>niv</span></span>ed at the secret <span class="emp2"><span>con</span></span><span class="emp3"><span>niv</span></span>ing of <span class="emp2"><span>con</span></span> men smuggling k<span class="emp3"><span>niv</span></span>es because those <span class="emp2"><span>con</span></span><span class="emp3"><span>niv</span></span>ing and corrupt authorities received a percentage of the profit when the <span class="emp2"><span>con</span></span><span class="emp3"><span>niv</span></span>ing <span class="emp2"><span>con</span></span> men sold the illegal k<span class="emp3"><span>niv</span></span>es across the border.
</p>
</div>

<div id='memhook-button-bar'>
<a href="connive#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/connive/memory_hooks">Use other hook</a>
<a href="connive#" id="memhook-use-own" url="https://membean.com/mywords/connive/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='connive#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
[U]nder no circumstances, can any true-hearted abolitionist engage in or <b>connive</b> at any compromise involving the slightest concession to any pro-slavery requisition.
<cite class='attribution'>
&mdash;
The New York Times from 1852
</cite>
</li>
<li>
One of Warren’s obsessions in the book, reflected in both movies, is the dilemma that arises when moral people <b>connive</b> with the immoral to try to do good.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
"If [a government] was weak and temperate, it would resort to violence only at the last extremity and would <b>connive</b> at many partial acts of insubordination; then the state would gradually fall into anarchy."
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
When Kenny comes upon some movie reviews by Roger Ebert, which he considers brilliant, he's moved, at age 18—sofa surfing and just fired from Home Depot—to <b>connive</b> to meet the critic.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/connive/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='connive#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='con_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='connive#'>
<span class='common'></span>
con-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td class='partform'>niv</td>
<td>
&rarr;
</td>
<td class='meaning'>wink, close the eyes</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='connive#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>When you <em>connive</em>, you &#8220;thoroughly wink&#8221; or &#8220;close the eyes&#8221; at something illegal that&#8217;s going on.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='connive#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>The Sting</strong><span> Some pretty devious and convincing conniving is going on here!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/connive.jpg' video_url='examplevids/connive' video_width='350'></span>
<div id='wt-container'>
<img alt="Connive" height="288" src="https://cdn1.membean.com/video/examplevids/connive.jpg" width="350" />
<div class='center'>
<a href="connive#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='connive#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Connive" src="https://cdn2.membean.com/public/images/wordimages/cons2/connive.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='connive#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>artifice</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>cabal</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>clandestine</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>collude</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>complicity</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>conclave</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>culpable</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>disingenuous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>furtive</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>gerrymander</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>machination</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>subterfuge</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>surreptitious</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>synergy</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>tacit</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>autonomous</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>exculpate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>exonerate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>ingenuous</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>overt</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>punctilious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>scrupulous</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>statutory</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="connive" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>connive</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="connive#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

