
<!DOCTYPE html>
<html>
<head>
<title>Word: explicate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>You can describe something as abstruse if you find it highly complicated and difficult to understand.</p>
<p class='rw-defn idx1' style='display:none'>If someone is addled by something, they are confused by it and unable to think properly.</p>
<p class='rw-defn idx2' style='display:none'>When you adduce, you give facts and examples in order to prove that something is true.</p>
<p class='rw-defn idx3' style='display:none'>To <em>adumbrate</em> is to describe something incompletely or to suggest future events based on limited current knowledge.</p>
<p class='rw-defn idx4' style='display:none'>If you describe something as ambiguous, you mean that it is unclear or confusing because it can be understood in multiple ways.</p>
<p class='rw-defn idx5' style='display:none'>If you describe someone as articulate, you mean that they are able to express their thoughts, arguments, and ideas clearly and effectively.</p>
<p class='rw-defn idx6' style='display:none'>If something confounds you, it makes you feel surprised and confused, often because it does not meet your predefined expectations.</p>
<p class='rw-defn idx7' style='display:none'>A conundrum is a problem or puzzle that is difficult or impossible to solve.</p>
<p class='rw-defn idx8' style='display:none'>Covert activities or situations are secret and hidden.</p>
<p class='rw-defn idx9' style='display:none'>When you decipher a message or piece of writing, you work out what it says, even though it is very difficult to read or understand.</p>
<p class='rw-defn idx10' style='display:none'>A denouement is the end of a book, play, or series of events when everything is explained and comes to a conclusion.</p>
<p class='rw-defn idx11' style='display:none'>A dilemma is a difficult situation or problem that consists of a choice between two equally disagreeable or unfavorable alternatives.</p>
<p class='rw-defn idx12' style='display:none'>To divulge something is to reveal information that was once private or secret.</p>
<p class='rw-defn idx13' style='display:none'>If you elucidate something, you make it easier to understand by giving more relevant information.</p>
<p class='rw-defn idx14' style='display:none'>Someone or something that is enigmatic is mysterious and difficult to understand.</p>
<p class='rw-defn idx15' style='display:none'>When you enunciate, you speak or state something clearly so that you can be easily understood.</p>
<p class='rw-defn idx16' style='display:none'>An epiphany is the moment when someone suddenly realizes or understands something of great significance or importance.</p>
<p class='rw-defn idx17' style='display:none'>When people equivocate, they avoid making a clear statement; they are deliberately vague in order to avoid giving a direct answer to a question.</p>
<p class='rw-defn idx18' style='display:none'>Exegesis is a detailed explanation or interpretation of a piece of writing, especially a religious one.</p>
<p class='rw-defn idx19' style='display:none'>One thing that exemplifies another serves as an example of it, illustrates it, or demonstrates it.</p>
<p class='rw-defn idx20' style='display:none'>To expatiate upon a subject is to speak or write in detail and at length about it.</p>
<p class='rw-defn idx21' style='display:none'>An exposition is a detailed explanation or setting forth of an idea, theory, or problem, either in a written or spoken format.</p>
<p class='rw-defn idx22' style='display:none'>When you expound something, you explain it in great detail, often taking a while to do so.</p>
<p class='rw-defn idx23' style='display:none'>If you fathom something complicated or mysterious, you are able to understand it.</p>
<p class='rw-defn idx24' style='display:none'>If you gloss a difficult word, phrase, or other text, you provide an explanation for it in the form of a note.</p>
<p class='rw-defn idx25' style='display:none'>If an idea or thought is incisive, it is expressed in a penetrating and knowledgeable manner that is clear and brief; additionally, it can demonstrate impressive understanding of related ideas or thoughts.</p>
<p class='rw-defn idx26' style='display:none'>Someone who is speaking in an incoherent fashion cannot be understood or is very hard to understand.</p>
<p class='rw-defn idx27' style='display:none'>Something that is latent, such as an undiscovered talent or undiagnosed disease, exists but is not active or has not developed at the moment—it may develop or become active in the future.</p>
<p class='rw-defn idx28' style='display:none'>If someone is lucid, they are able to understand things and think clearly; this adjective also describes writing or speech that is crystal clear and easy to understand.</p>
<p class='rw-defn idx29' style='display:none'>A misapprehension is a misunderstanding or false impression that you are under, especially concerning another person&#8217;s intentions towards you.</p>
<p class='rw-defn idx30' style='display:none'>If you describe something as nebulous, you mean that it is unclear, vague, and not clearly defined; a shape that is nebulous has no clear boundaries.</p>
<p class='rw-defn idx31' style='display:none'>To obfuscate something is to make it deliberately unclear, confusing, and difficult to understand.</p>
<p class='rw-defn idx32' style='display:none'>Someone is obtuse when they are slow to understand things; someone can choose to be obtuse when they deliberately do not try to figure things out.</p>
<p class='rw-defn idx33' style='display:none'>Something is opaque if it is either difficult to understand or is not transparent.</p>
<p class='rw-defn idx34' style='display:none'>An overt act is not hidden or secret but is done in an open and public way.</p>
<p class='rw-defn idx35' style='display:none'>A paradox is a statement that appears to be self-contradictory or unrealistic but may surprisingly express a possible truth.</p>
<p class='rw-defn idx36' style='display:none'>A parenthetical remark further explains or qualifies information.</p>
<p class='rw-defn idx37' style='display:none'>The art of pedagogy is the methods used by a teacher to teach a subject.</p>
<p class='rw-defn idx38' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx39' style='display:none'>A rationale is a reason or explanation for doing something.</p>
<p class='rw-defn idx40' style='display:none'>Recondite areas of knowledge are those that are very difficult to understand and/or are not known by many people.</p>
<p class='rw-defn idx41' style='display:none'>A subtle point is so clever or small that it is hard to notice or understand; it can also be very wise or deep in meaning.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>explicate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='explicate#' id='pronounce-sound' path='audio/words/amy-explicate'></a>
EK-spli-kayt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='explicate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='explicate#' id='context-sound' path='audio/wordcontexts/brian-explicate'></a>
For years scholars have tried to <em>explicate</em> or explain the many amazing inventions and writings of Leonardo Da Vinci.  Da Vinci himself provided many detailed notes on his inventions, but unfortunately many of his <em><em>explications</em></em> or clarifying interpretations are in unsolvable or <em>inexplicable</em> code!  Dan Brown has done a good job of presenting possibilities that might <em>explicate</em> or clear up some of Da Vinci&#8217;s puzzles, but there is still much work to be done.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean to <em>explicate</em> a plan?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
You describe it in great detail.
</li>
<li class='choice '>
<span class='result'></span>
You make several copies of it for a meeting.
</li>
<li class='choice '>
<span class='result'></span>
You add something to it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='explicate#' id='definition-sound' path='audio/wordmeanings/amy-explicate'></a>
To <em>explicate</em> an idea or plan is to make it clear by explaining it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>clarify</em>
</span>
</span>
</div>
<a class='quick-help' href='explicate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='explicate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/explicate/memory_hooks/4802.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Cate</span></span> <span class="emp3"><span>Expl</span></span>a<span class="emp3"><span>i</span></span>ns Mona Lisa</span></span> <span class="emp2"><span>Cate</span></span> Sharpley is such an artistic wizard that she thinks she can <span class="emp3"><span>expli</span></span><span class="emp2"><span>cate</span></span> the age-old mystery behind the Mona Lisa; <span class="emp2"><span>Cate</span></span> claims to <span class="emp3"><span>expl</span></span>a<span class="emp3"><span>i</span></span>n and solve once and for all the question of whether or not the Mona Lisa is a self-portrait of Leonardo Da Vinci.
</p>
</div>

<div id='memhook-button-bar'>
<a href="explicate#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/explicate/memory_hooks">Use other hook</a>
<a href="explicate#" id="memhook-use-own" url="https://membean.com/mywords/explicate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='explicate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Those places honor and <b>explicate</b> regional traditions that have deeply influenced American music while showcasing a culture that stubbornly insists on its place in the contemporary world.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Along with a short retrospective essay by Ms. Lippard, chapters by the exhibition’s organizers, Catherine Morris and Vincent Bonin, clearly and engagingly <b>explicate</b> a genre that casual observers often find stupefyingly obscure.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Untouched by cynicism and irony, _Little Buddha_ has the wide-eyed, innocent feel of a fable, perhaps because Bertolucci has set out to <b>explicate</b> Buddhism to skeptical Westerners as if to an audience of children.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Somewhere in the 1980s, says Prof. Lathbury, "Eagleton began to be a hero to some" and "theory became the object of study more than the works it purportedly was designed to <b>explicate</b>."
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/explicate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='explicate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ex_out' data-tree-url='//cdn1.membean.com/public/data/treexml' href='explicate#'>
<span class='common'></span>
ex-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>out of, from, off</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='plic_fold' data-tree-url='//cdn1.membean.com/public/data/treexml' href='explicate#'>
<span class='common'></span>
plic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>fold</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='explicate#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>To <em>explicate</em> something is to &#8220;fold it out&#8221; in explanation, thus revealing what once was hidden.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='explicate#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Hardware Wars</strong><span> What an explication of what to do!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/explicate.jpg' video_url='examplevids/explicate' video_width='350'></span>
<div id='wt-container'>
<img alt="Explicate" height="288" src="https://cdn1.membean.com/video/examplevids/explicate.jpg" width="350" />
<div class='center'>
<a href="explicate#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='explicate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Explicate" src="https://cdn2.membean.com/public/images/wordimages/cons2/explicate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='explicate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>adduce</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>adumbrate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>articulate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>decipher</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>denouement</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>divulge</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>elucidate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>enunciate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>epiphany</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>exegesis</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>exemplify</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>expatiate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>exposition</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>expound</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>fathom</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>gloss</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>incisive</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>lucid</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>overt</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>parenthetical</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>pedagogy</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>rationale</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abstruse</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>addle</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>ambiguous</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>confound</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>conundrum</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>covert</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>dilemma</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>enigmatic</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>equivocate</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>incoherent</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>latent</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>misapprehension</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>nebulous</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>obfuscate</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>obtuse</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>opaque</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>paradox</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>recondite</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>subtle</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='explicate#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
inexplicable
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not able to be explained</td>
</tr>
<tr>
<td class='wordform'>
<span>
explicable
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>able to be explained</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="explicate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>explicate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="explicate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

