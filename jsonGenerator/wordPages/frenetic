
<!DOCTYPE html>
<html>
<head>
<title>Word: frenetic | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you do something with alacrity, you do it eagerly and quickly.</p>
<p class='rw-defn idx1' style='display:none'>If you deal with a difficult situation with aplomb, you deal with it in a confident and skillful way.</p>
<p class='rw-defn idx2' style='display:none'>When you have ardor for something, you have an intense feeling of love, excitement, and admiration for it.</p>
<p class='rw-defn idx3' style='display:none'>A catatonic person is in a state of suspended action; therefore, they are rigid, immobile, and unresponsive.</p>
<p class='rw-defn idx4' style='display:none'>If something moves or grows with celerity, it does so rapidly.</p>
<p class='rw-defn idx5' style='display:none'>When you are discombobulated, you are confused and upset because you have been thrown into a situation that you cannot temporarily handle.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx7' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx8' style='display:none'>If you describe a person, group, or civilization as effete, you mean it is weak, exhausted,  powerless, unproductive, and/or corrupt.</p>
<p class='rw-defn idx9' style='display:none'>If something enervates you, it makes you very tired and weak—almost to the point of collapse.</p>
<p class='rw-defn idx10' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx11' style='display:none'>If you show exuberance, you display great excitement, energy, and enthusiasm.</p>
<p class='rw-defn idx12' style='display:none'>Febrile behavior is full of nervous energy and activity; a sick person can be febrile as well, that is, feverish or hot.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx14' style='display:none'>A fervid person has strong feelings about something, such as a humanitarian cause; therefore, they are very sincere and enthusiastic about it.</p>
<p class='rw-defn idx15' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx16' style='display:none'>If someone is imperturbable, they are always calm and not easily upset or disturbed by any situation, even dangerous ones.</p>
<p class='rw-defn idx17' style='display:none'>An indolent person is lazy.</p>
<p class='rw-defn idx18' style='display:none'>When something is inert, it has no power of movement or is inactive.</p>
<p class='rw-defn idx19' style='display:none'>If you do something in a lackadaisical way, you do it carelessly and without putting much effort into it—thereby showing that you are not really interested in what you&#8217;re doing.</p>
<p class='rw-defn idx20' style='display:none'>A languid person is slow, relaxed, and shows little energy or interest in doing anything.</p>
<p class='rw-defn idx21' style='display:none'>If you are lethargic, you are tired, lack energy, and are unwilling to exert effort.</p>
<p class='rw-defn idx22' style='display:none'>When you are listless, you lack energy and interest and are unwilling to exert any effort.</p>
<p class='rw-defn idx23' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx24' style='display:none'>Someone who is phlegmatic stays calm and unemotional even in dangerous or exciting situations.</p>
<p class='rw-defn idx25' style='display:none'>If you are in a state of repose, your mind is at peace or your body is at rest.</p>
<p class='rw-defn idx26' style='display:none'>When you saunter along, you are taking a stroll or slow walk from one place to another.</p>
<p class='rw-defn idx27' style='display:none'>If you are sedate, you are calm, unhurried, and unlikely to be disturbed by anything.</p>
<p class='rw-defn idx28' style='display:none'>Someone who has a sedentary habit, job, or lifestyle spends a lot of time sitting down without moving or exercising often.</p>
<p class='rw-defn idx29' style='display:none'>A serene place or situation is peaceful and calm.</p>
<p class='rw-defn idx30' style='display:none'>A tempestuous storm, temper, or crowd is violent, wild, and in an uproar.</p>
<p class='rw-defn idx31' style='display:none'>If your body is affected by torpor, you are severely lacking in energy; therefore, you are idle—and can even be numb.</p>
<p class='rw-defn idx32' style='display:none'>If something is tranquil, it is peaceful, calm, and quiet.</p>
<p class='rw-defn idx33' style='display:none'>When you are unflappable, you remain calm, cool, and collected in even the most trying of situations.</p>
<p class='rw-defn idx34' style='display:none'>Someone who is zealous spends a lot of time, energy, and effort to support something—  notably that of a political or religious nature—because they believe in it very strongly.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>frenetic</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='frenetic#' id='pronounce-sound' path='audio/words/amy-frenetic'></a>
fruh-NET-ik
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='frenetic#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='frenetic#' id='context-sound' path='audio/wordcontexts/brian-frenetic'></a>
After consuming three energy drinks and two dark coffees, Jack rushed about the space in a whirlwind of hurried or <em>frenetic</em> activity.  Jean had asked him to clean out his overstuffed garage, and after drinking so much caffeine, Jack&#8217;s energy level was unbelievably <em>frenetic</em>: wild and wired!  As he excitedly, madly, and <em>frenetically</em> cleaned out the garage, Jack misjudged the position of a shelf and knocked a can of paint on his own head.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of <em>frenetic</em> behavior?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Staying alert as you watch for your parent to pick you up after practice.
</li>
<li class='choice '>
<span class='result'></span>
Playing a constant, steady beat on the drums during your band&#8217;s performance.
</li>
<li class='choice answer '>
<span class='result'></span>
Running through each room in your home as you desperately search for your keys.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='frenetic#' id='definition-sound' path='audio/wordmeanings/amy-frenetic'></a>
<em>Frenetic</em> activity is done quickly with lots of energy but is also uncontrolled and disorganized; someone who is in a huge hurry often displays this type of behavior.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>very hurried</em>
</span>
</span>
</div>
<a class='quick-help' href='frenetic#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='frenetic#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/frenetic/memory_hooks/5537.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Ene</span></span>rge<span class="emp1"><span>tic</span></span> <span class="emp3"><span>Fr</span></span>oth</span></span> Mildred's <span class="emp3"><span>fr</span></span><span class="emp1"><span>enetic</span></span> activity was marked by two things: wildly <span class="emp1"><span>ene</span></span>rge<span class="emp1"><span>tic</span></span> running about, and <span class="emp3"><span>fr</span></span>othy foam coming out of her panting, open mouth.
</p>
</div>

<div id='memhook-button-bar'>
<a href="frenetic#" id="add-public-hook" style="" url="https://membean.com/mywords/frenetic/memory_hooks">Use other public hook</a>
<a href="frenetic#" id="memhook-use-own" url="https://membean.com/mywords/frenetic/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='frenetic#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Since mid-July, equity and fixed-income markets across the world have endured sickening declines and startling volatility. . . . A buying panic develops. This <b>frenetic</b> activity is rationalized by the participants' murmuring "the market acts well" when it’s going up and that "it acts badly" when the beast is falling.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
"The human animal responds to the illusion of scarcity with <b>frenetic</b> attempts to be first on line, whether for food, housing, concert tickets or admission to Harvard," Ms. Vail continued.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
The Shea Stadium fans, <b>frenetic</b> but orderly for a change, were on their feet crying for the final blow, and the mounted police were preparing a charge from the bullpen to barricade the field.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Unafraid of playing at Baylor's <b>frenetic</b> pace, Purdue took advantage of the porous defense and scored a season high. . . . "We got a little winded at times, like we were playing the Phoenix Suns," said Grant, who scored a team-high 17 points. "But it shows that we can play up-tempo, and if we need to grind it out and play physical ball, we can do that, too."
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/frenetic/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='frenetic#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>frenet</td>
<td>
&rarr;
</td>
<td class='meaning'>brain disease, delirium</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ic_like' data-tree-url='//cdn1.membean.com/public/data/treexml' href='frenetic#'>
<span class=''></span>
-ic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>nature of, like</td>
</tr>
</table>
<p>Someone who displays <em>frenetic</em> behavior seems to have something wrong with his or her brain, almost as if it is in a &#8220;delirious&#8221; or &#8220;mentally imbalanced&#8221; state.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='frenetic#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Frenetic" src="https://cdn1.membean.com/public/images/wordimages/cons2/frenetic.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='frenetic#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>alacrity</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>ardor</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>celerity</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>discombobulated</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>exuberance</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>febrile</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>fervid</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>tempestuous</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>zealous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>aplomb</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>catatonic</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>effete</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>enervate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>imperturbable</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>indolent</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>inert</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>lackadaisical</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>languid</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>lethargic</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>listless</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>phlegmatic</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>repose</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>saunter</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>sedate</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>sedentary</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>serene</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>torpor</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>tranquil</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>unflappable</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="frenetic" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>frenetic</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="frenetic#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

