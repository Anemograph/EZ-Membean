
<!DOCTYPE html>
<html>
<head>
<title>Word: unbridled | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Someone who is abstemious avoids doing too much of something enjoyable, such as eating or drinking; rather, they consume in a moderate fashion.</p>
<p class='rw-defn idx1' style='display:none'>Abstinence is the practice of keeping away from or avoiding something you enjoy—such as the physical pleasures of excessive food and drink—usually for health or religious reasons.</p>
<p class='rw-defn idx2' style='display:none'>Something austere is simple and plain in its style; an austere person is strict and severe with themselves.</p>
<p class='rw-defn idx3' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx4' style='display:none'>If one&#8217;s powers or rights are circumscribed, they are limited or restricted.</p>
<p class='rw-defn idx5' style='display:none'>Someone who is dour is serious, stubborn, and unfriendly; they can also be gloomy.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is effusive expresses happiness, pleasure, admiration, praise, etc., in an extremely enthusiastic way.</p>
<p class='rw-defn idx8' style='display:none'>An egregious mistake, failure, or problem is an extremely bad and very noticeable one.</p>
<p class='rw-defn idx9' style='display:none'>An exorbitant price or fee is much higher than what it should be or what is considered reasonable.</p>
<p class='rw-defn idx10' style='display:none'>Fanaticism is the condition of being overly enthusiastic or eager about a cause to the point of being extreme and unreasonable about it.</p>
<p class='rw-defn idx11' style='display:none'>When someone flaunts their good looks, they show them off or boast about them in a very proud and shameless way.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is frugal spends very little money—and even then only on things that are absolutely necessary.</p>
<p class='rw-defn idx13' style='display:none'>Hyperbole is a way of emphasizing something that makes it sound much more impressive or much worse than it actually is.</p>
<p class='rw-defn idx14' style='display:none'>If something is inordinate, it is much larger in amount or degree than is normally expected.</p>
<p class='rw-defn idx15' style='display:none'>If you describe ideas as jejune, you are saying they are childish and uninteresting; the adjective jejune also describes those having little experience, maturity, or knowledge.</p>
<p class='rw-defn idx16' style='display:none'>When you behave with moderation, you live in a balanced and measured way; you do nothing to excess.</p>
<p class='rw-defn idx17' style='display:none'>Someone is overweening when they are not modest; rather, they think way too much of themselves and let everyone know about it.</p>
<p class='rw-defn idx18' style='display:none'>A parsimonious person is not willing to give or spend money.</p>
<p class='rw-defn idx19' style='display:none'>A paucity of something is not enough of it.</p>
<p class='rw-defn idx20' style='display:none'>Someone who behaves in a prodigal way spends a lot of money and/or time carelessly and wastefully with no concern for the future.</p>
<p class='rw-defn idx21' style='display:none'>Someone who is profligate is lacking in restraint, which can manifest in carelessly and wastefully using resources, such as energy or money; this kind of person can also act in an immoral way.</p>
<p class='rw-defn idx22' style='display:none'>If something bad, such as crime or disease, is rampant, there is a lot of it—and it is increasing in a way that is difficult to control.</p>
<p class='rw-defn idx23' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx24' style='display:none'>When you stifle someone&#8217;s creativity or inner drive, you prevent it from being expressed.</p>
<p class='rw-defn idx25' style='display:none'>A stricture is a rule or condition that imposes restrictions or limits on what you can do.</p>
<p class='rw-defn idx26' style='display:none'>If you show temperance, you limit yourself so that you don’t do too much of something; you act in a controlled and well-balanced way.</p>
<p class='rw-defn idx27' style='display:none'>A tempestuous storm, temper, or crowd is violent, wild, and in an uproar.</p>
<p class='rw-defn idx28' style='display:none'>A tumultuous event or period of time is filled with great excitement, confusion, or violence; a tumultuous reaction to something is likewise very loud and noisy because people are happy and excited.</p>
<p class='rw-defn idx29' style='display:none'>Someone who is unrestrained is free to do as they please; they are not controlled by anyone but themselves, which can lead to excessive behavior.</p>
<p class='rw-defn idx30' style='display:none'>A wanton action deliberately harms someone or damages something for no apparent or good reason.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>unbridled</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='unbridled#' id='pronounce-sound' path='audio/words/amy-unbridled'></a>
uhn-BRAHYD-ld
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='unbridled#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='unbridled#' id='context-sound' path='audio/wordcontexts/brian-unbridled'></a>
Brett erupted in <em>unbridled</em>, unchecked joy because the Kansas City Chiefs had at long last won the Super Bowl!  His <em>unbridled</em> and wildly enthusiastic celebration was due to the fact that he had been waiting for 50 years for the Chiefs to win that big game.  Even weeks after the Super Bowl had ended, his crazy joy for their victory remained <em>unbridled</em> and unmanageable: he couldn&#8217;t stop telling people about it!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If you are known for <em>unbridled</em> reactions, what do you often do?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
React in ways that are inappropriate for the situation or setting.
</li>
<li class='choice answer '>
<span class='result'></span>
React with an outpouring of uncontrolled emotion.
</li>
<li class='choice '>
<span class='result'></span>
React in such a way that no one can tell how you really feel.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='unbridled#' id='definition-sound' path='audio/wordmeanings/amy-unbridled'></a>
A feeling that is <em>unbridled</em> is enthusiastic and unlimited in its expression.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>unchecked</em>
</span>
</span>
</div>
<a class='quick-help' href='unbridled#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='unbridled#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/unbridled/memory_hooks/5625.json'></span>
<p>
<img alt="Unbridled" src="https://cdn3.membean.com/public/images/wordimages/hook/unbridled.jpg?qdep8" />
<span class="emp0"><span>Ecstatic Horse Leaves <span class="emp1"><span>Bridle</span></span> Far Behind</span></span>  When Wayne took the <span class="emp1"><span>bridle</span></span> off his horse for the last time before setting her loose, he felt un<span class="emp1"><span>bridle</span></span>d enthusiasm at her un<span class="emp1"><span>bridle</span></span>d joy as she whinnied and ran through the plains in utter, unrestrained, wild, and unchecked freedom.
</p>
</div>

<div id='memhook-button-bar'>
<a href="unbridled#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/unbridled/memory_hooks">Use other hook</a>
<a href="unbridled#" id="memhook-use-own" url="https://membean.com/mywords/unbridled/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='unbridled#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Humor is a spontaneous, wonderful bit of an outburst that just comes. It's <b>unbridled</b>, it's unplanned, it's full of surprises.
<span class='attribution'>&mdash; Erma Bombeck, twentieth century American humorist</span>
<img alt="Erma bombeck, twentieth century american humorist" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/Erma Bombeck, twentieth century American humorist.jpg?qdep8" width="80" />
</li>
<li>
Hikers glimpse tensed, silver-gray beasts in whose yellow eyes they see the <b>unbridled</b> spirit of the wilderness, the embodiment of all that is wild and free.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
Charles Grassley, who heads the Senate Finance Committee, has decried the "<b>unbridled</b> greed" of those responsible and has vowed to end these abuses.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Parker could come across without them, of course, and it'd be fascinating to hear him crude and <b>unbridled</b> on an independent label that'd be overjoyed with six-figure sales.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/unbridled/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='unbridled#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='un_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='unbridled#'>
<span class='common'></span>
un-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not, opposite of</td>
</tr>
<tr>
<td class='partform'>bridl</td>
<td>
&rarr;
</td>
<td class='meaning'>harness for the head of a horse, bridle</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ed_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='unbridled#'>
<span class=''></span>
-ed
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>having a particular state</td>
</tr>
</table>
<p><em>Unbridled</em> joy does &#8220;not have a bridle&#8221; upon it so it is allowed to flow freely.</p>
<a href="unbridled#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> Old English</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='unbridled#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Double Rainbow</strong><span> An unbridled reaction to a rare occurrence!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/unbridled.jpg' video_url='examplevids/unbridled' video_width='350'></span>
<div id='wt-container'>
<img alt="Unbridled" height="288" src="https://cdn1.membean.com/video/examplevids/unbridled.jpg" width="350" />
<div class='center'>
<a href="unbridled#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='unbridled#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Unbridled" src="https://cdn3.membean.com/public/images/wordimages/cons2/unbridled.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='unbridled#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>effusive</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>egregious</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>exorbitant</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>fanaticism</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>flaunt</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>hyperbole</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>inordinate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>overweening</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>prodigal</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>profligate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>rampant</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>tempestuous</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>tumultuous</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>unrestrained</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>wanton</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abstemious</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abstinence</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>austere</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>circumscribe</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>dour</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>frugal</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>jejune</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>moderation</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>parsimonious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>paucity</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>stifle</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>stricture</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>temperance</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="unbridled" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>unbridled</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="unbridled#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

