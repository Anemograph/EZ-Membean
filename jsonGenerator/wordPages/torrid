
<!DOCTYPE html>
<html>
<head>
<title>Word: torrid | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Torrid-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/torrid-large.jpg?qdep8" />
</div>
<a href="torrid#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When you perform your ablutions, you wash yourself; this can be part of a religious ceremony as well.</p>
<p class='rw-defn idx1' style='display:none'>An arid landscape is dry and lacking in sufficient rainfall to support much life.</p>
<p class='rw-defn idx2' style='display:none'>Something is a blight if it spoils or damages things severely; blight is often used to describe something that ruins or kills crops.</p>
<p class='rw-defn idx3' style='display:none'>A conflagration is a fire that burns over a large area and is highly destructive.</p>
<p class='rw-defn idx4' style='display:none'>A dank room or building is unpleasantly damp and chilly.</p>
<p class='rw-defn idx5' style='display:none'>Someone defoliates a tree or plant by removing its leaves, usually by applying a chemical agent.</p>
<p class='rw-defn idx6' style='display:none'>A deluge is a sudden, heavy downfall of rain; it can also be a large number of things, such as papers or e-mails, that someone gets all at the same time, making them very difficult to handle.</p>
<p class='rw-defn idx7' style='display:none'>To denude an area is to remove the plants and trees that cover it; it can also mean to make something bare.</p>
<p class='rw-defn idx8' style='display:none'>Something that is desiccated has had all the water taken out of it; this is a common process used for food in order to preserve it.</p>
<p class='rw-defn idx9' style='display:none'>When an area is devoid of life, it is empty or completely lacking in it.</p>
<p class='rw-defn idx10' style='display:none'>A fissure is a narrow and long crack or opening, usually in a rock face.</p>
<p class='rw-defn idx11' style='display:none'>If something is inordinate, it is much larger in amount or degree than is normally expected.</p>
<p class='rw-defn idx12' style='display:none'>If you are inundated with something, you have so much of it that you cannot easily deal with it; likewise, if too much rain inundates an area, it causes flooding.</p>
<p class='rw-defn idx13' style='display:none'>A lattice is a pattern or structure that has parallel sets of lines of material crossing each other, often at right angles.</p>
<p class='rw-defn idx14' style='display:none'>A maelstrom is either a large whirlpool in the sea or a violent or agitated state of affairs.</p>
<p class='rw-defn idx15' style='display:none'>A succulent food, such as sweet fruit or a good tomato, is juicy and tasty.</p>
<p class='rw-defn idx16' style='display:none'>A tempestuous storm, temper, or crowd is violent, wild, and in an uproar.</p>
<p class='rw-defn idx17' style='display:none'>A torrential downpour of rain is very heavy and intense.</p>
<p class='rw-defn idx18' style='display:none'>A wizened person is very old, shrunken with age, and has a lot of wrinkles on their skin.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>torrid</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='torrid#' id='pronounce-sound' path='audio/words/amy-torrid'></a>
TOR-id
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='torrid#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='torrid#' id='context-sound' path='audio/wordcontexts/brian-torrid'></a>
The Dust Bowl in the United States was caused by <em>torrid</em> or extremely dry and hot weather that lasted for an unusually long time.  The <em>torrid</em> conditions or blazing heat, coupled with lack of rainfall, destroyed and withered crops.  After a time the <em>torrid</em> earth, baked and scorched by such weather, bore no plant life at all because it was so very dry.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How should you prepare for <em>torrid</em> weather?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Bring extra water and a lot of sunscreen.
</li>
<li class='choice '>
<span class='result'></span>
Bring an umbrella and waterproof shoes.
</li>
<li class='choice '>
<span class='result'></span>
Bring an extra sweater and warm socks.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='torrid#' id='definition-sound' path='audio/wordmeanings/amy-torrid'></a>
The adjective <em>torrid</em> can refer to weather that is very hot and dry; it can also refer to earth that has been baked or scorched by such weather.
</li>
<li class='def-text'>
The adjective <em>torrid</em> can also refer to a very passionate love affair.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>scorching</em>
</span>
</span>
</div>
<a class='quick-help' href='torrid#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='torrid#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/torrid/memory_hooks/5277.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Tor</span></span>ched G<span class="emp3"><span>rid</span></span></span></span> The earth around my house looks like a <span class="emp1"><span>tor</span></span>ched g<span class="emp3"><span>rid</span></span> due to the <span class="emp1"><span>tor</span></span><span class="emp3"><span>rid</span></span> weather lately.
</p>
</div>

<div id='memhook-button-bar'>
<a href="torrid#" id="add-public-hook" style="" url="https://membean.com/mywords/torrid/memory_hooks">Use other public hook</a>
<a href="torrid#" id="memhook-use-own" url="https://membean.com/mywords/torrid/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='torrid#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
It’s the heat and the humidity. Alexander Frater settles that question early on in “Tales From the <b>Torrid</b> Zone” as he wanders, in seven-league boots, across the earth’s fat, sweaty midsection, swatting flies and mosquitoes all the way . . . . One of his more fascinating chapters describes, with a certain relish, several of the 40 tropical diseases that flourish in the <b>torrid</b> zone.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Each summer as 120-degree heat shimmers silver waves across the Mojave Desert, residents of this inferno crank the thermostat down . . . . The problem is that many air conditioners in this <b>torrid</b> zone are huge power hogs.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/torrid/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='torrid#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='torr_burn' data-tree-url='//cdn1.membean.com/public/data/treexml' href='torrid#'>
<span class=''></span>
torr
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>burn, dry up</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='id_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='torrid#'>
<span class=''></span>
-id
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p><em>Torrid</em> heat is so &#8220;burning&#8221; that it &#8220;dries up&#8221; the land.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='torrid#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Torrid" src="https://cdn1.membean.com/public/images/wordimages/cons2/torrid.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='torrid#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>arid</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>blight</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>conflagration</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>defoliate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>denude</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>desiccate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>devoid</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>fissure</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>inordinate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>lattice</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>wizened</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>ablution</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>dank</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>deluge</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>inundate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>maelstrom</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>succulent</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>tempestuous</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>torrential</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="torrid" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>torrid</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="torrid#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

