
<!DOCTYPE html>
<html>
<head>
<title>Word: clientele | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Accretion is the slow, gradual process by which new things are added and something gets bigger.</p>
<p class='rw-defn idx1' style='display:none'>An acolyte is someone who serves a leader as a devoted assistant or believes in the leader&#8217;s ideas.</p>
<p class='rw-defn idx2' style='display:none'>An adherent is a supporter or follower of a leader or a cause.</p>
<p class='rw-defn idx3' style='display:none'>An adjunct is something that is added to or joined to something else that is larger or more important.</p>
<p class='rw-defn idx4' style='display:none'>Affluence is the state of having a lot of money and many possessions, granting one a high economic standard of living.</p>
<p class='rw-defn idx5' style='display:none'>To agglomerate a group of things is to gather them together without any noticeable order.</p>
<p class='rw-defn idx6' style='display:none'>An aggregate is the final or sum total after many different amounts or scores have been added together.</p>
<p class='rw-defn idx7' style='display:none'>When two or more things, such as organizations, amalgamate, they combine to become one large thing.</p>
<p class='rw-defn idx8' style='display:none'>An autonomous person makes their own decisions without being influenced by anyone else; an autonomous country or organization is independent and has the power to govern itself.</p>
<p class='rw-defn idx9' style='display:none'>A bevy is group or collection most often of people, but it can also refer to a group of animals.</p>
<p class='rw-defn idx10' style='display:none'>A clique is a group of people that spends a lot of time together and tends to be unfriendly towards people who are not part of the group.</p>
<p class='rw-defn idx11' style='display:none'>Something is concomitant when it happens at the same time as something else and is connected with it in some fashion.</p>
<p class='rw-defn idx12' style='display:none'>A coterie is a small group of people who are close friends; therefore, when its members do things together, they do not want other people to join them.</p>
<p class='rw-defn idx13' style='display:none'>To decry something is to speak against it and find fault with it.</p>
<p class='rw-defn idx14' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx15' style='display:none'>If you denounce people or actions, you criticize them severely in public because you feel strongly that they are wrong or evil.</p>
<p class='rw-defn idx16' style='display:none'>Dissension is a disagreement or difference of opinion among a group of people that can cause conflict.</p>
<p class='rw-defn idx17' style='display:none'>An entourage is a group of assistants, servants, and other people who tag along with an important person.</p>
<p class='rw-defn idx18' style='display:none'>An exponent of a particular idea or cause is someone who supports or defends it.</p>
<p class='rw-defn idx19' style='display:none'>If you show fealty to a queen, you show your loyalty to her.</p>
<p class='rw-defn idx20' style='display:none'>Someone who is impecunious has very little money, especially over a long period of time.</p>
<p class='rw-defn idx21' style='display:none'>Something that is inconspicuous does not attract attention and is not easily seen or noticed because it is small or ordinary.</p>
<p class='rw-defn idx22' style='display:none'>A luminary is someone who is much admired in a particular profession because they are an accomplished expert in their field.</p>
<p class='rw-defn idx23' style='display:none'>A magnate is a rich and powerful person in an industry or business.</p>
<p class='rw-defn idx24' style='display:none'>Your milieu includes the things and people that surround you and influence the way in which you behave.</p>
<p class='rw-defn idx25' style='display:none'>A minion is an unimportant person who merely obeys the orders of someone else; they  usually perform boring tasks that require little skill.</p>
<p class='rw-defn idx26' style='display:none'>If someone patronizes you, they talk or behave in a way that seems friendly; nevertheless, they also act as if they were more intelligent or important than you are.</p>
<p class='rw-defn idx27' style='display:none'>Pecuniary means relating to or concerning money.</p>
<p class='rw-defn idx28' style='display:none'>A recluse is someone who chooses to live alone and deliberately avoids other people.</p>
<p class='rw-defn idx29' style='display:none'>A retinue is the group of people, such as friends or servants, who travel with someone important to help and support that person.</p>
<p class='rw-defn idx30' style='display:none'>A skeptic is a person who doubts popular claims or facts about things that other people believe to be true.</p>
<p class='rw-defn idx31' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>clientele</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='clientele#' id='pronounce-sound' path='audio/words/amy-clientele'></a>
klahy-uhn-TEL
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='clientele#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='clientele#' id='context-sound' path='audio/wordcontexts/brian-clientele'></a>
When I first started my business selling software, I had a small <em>clientele</em> or base of customers, primarily people that I already knew.  Over time, however, my <em>clientele</em> or number of clients began to grow, especially when I expanded my sales to the Internet.  My <em>clientele</em> is now huge; people buy products from my business from all over the globe.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If you are part of the <em>clientele</em> of a business, what are you?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
You are a frequent customer of that business.
</li>
<li class='choice '>
<span class='result'></span>
You are a part owner of that business.
</li>
<li class='choice '>
<span class='result'></span>
You are a competitor of that business.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='clientele#' id='definition-sound' path='audio/wordmeanings/amy-clientele'></a>
The <em>clientele</em> of a business are those clients or frequent customers who support that business.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>customers</em>
</span>
</span>
</div>
<a class='quick-help' href='clientele#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='clientele#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/clientele/memory_hooks/3112.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Client</span></span>s</span></span> The <span class="emp2"><span>client</span></span>ele of a business is simply its customer or <span class="emp2"><span>client</span></span> base.
</p>
</div>

<div id='memhook-button-bar'>
<a href="clientele#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/clientele/memory_hooks">Use other hook</a>
<a href="clientele#" id="memhook-use-own" url="https://membean.com/mywords/clientele/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='clientele#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
I am very honored for all the distinctions and accolades, but what I am most sensitive to is my <b>clientele</b> and the fact they are pleased with my food and my restaurants.
<span class='attribution'>&mdash; Joël Robuchon, French chef and restaurateur</span>
<img alt="Joël robuchon, french chef and restaurateur" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Joël Robuchon, French chef and restaurateur.jpg?qdep8" width="80" />
</li>
<li>
The coffeehouse <b>clientele</b> harks back to a noble tradition. The world's first coffeehouse is said to have opened in Damascus in 1530. Coffee began appearing in Europe in the early 17th century, with each country evolving a distinctive coffeehouse culture, from England to Austria.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
“We decided on SouthPark area because the <b>clientele</b> here is our target range,” Christopher Carelli told CharlotteFive. “There is a great over-45 community here, and that is what we are looking for.”
<cite class='attribution'>
&mdash;
The Charlotte Observer
</cite>
</li>
<li>
We have tried to provide goods and services that meet the standards of our <b>clientele</b>. We ship our candy and gifts both locally and nationally.
<cite class='attribution'>
&mdash;
Chicago Tribune
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/clientele/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='clientele#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>client</td>
<td>
&rarr;
</td>
<td class='meaning'>client, dependent</td>
</tr>
</table>
<p>A merchant&#8217;s <em>clientele</em> are her &#8220;clients,&#8221; or people that she is &#8220;dependent&#8221; upon to purchase products from her.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='clientele#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Clientele" src="https://cdn1.membean.com/public/images/wordimages/cons2/clientele.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='clientele#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>accretion</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>acolyte</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>adherent</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>adjunct</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>affluence</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>agglomerate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>aggregate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>amalgamate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>bevy</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>clique</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>concomitant</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>coterie</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>entourage</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>exponent</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>fealty</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>milieu</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>minion</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>patronize</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>pecuniary</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>retinue</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='8' class = 'rw-wordform notlearned'><span>autonomous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>decry</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>denounce</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>dissension</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>impecunious</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>inconspicuous</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>luminary</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>magnate</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>recluse</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>skeptic</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="clientele" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>clientele</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="clientele#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

