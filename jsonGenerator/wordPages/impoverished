
<!DOCTYPE html>
<html>
<head>
<title>Word: impoverished | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you are acquisitive, you are driven to pursue and own wealth and possessions—often in a greedy fashion.</p>
<p class='rw-defn idx1' style='display:none'>Affluence is the state of having a lot of money and many possessions, granting one a high economic standard of living.</p>
<p class='rw-defn idx2' style='display:none'>Something austere is simple and plain in its style; an austere person is strict and severe with themselves.</p>
<p class='rw-defn idx3' style='display:none'>Avarice is the extremely strong desire to have a lot of money and possessions.</p>
<p class='rw-defn idx4' style='display:none'>A bereaved person is someone whose close friend or relative has recently died.</p>
<p class='rw-defn idx5' style='display:none'>Something is a blight if it spoils or damages things severely; blight is often used to describe something that ruins or kills crops.</p>
<p class='rw-defn idx6' style='display:none'>When you are granted a boon, you are given a special gift or favor that is of great benefit to you.</p>
<p class='rw-defn idx7' style='display:none'>Bourgeois members of society are from the upper middle class and are typically conservative, traditional, and materialistic.</p>
<p class='rw-defn idx8' style='display:none'>A copious amount of something is a very large amount of it.</p>
<p class='rw-defn idx9' style='display:none'>A cornucopia is a large quantity and variety of something good and nourishing.</p>
<p class='rw-defn idx10' style='display:none'>When there is a dearth of something, there is a scarcity or lack of it.</p>
<p class='rw-defn idx11' style='display:none'>Deprivation is a state during which people lack something, especially adequate food and shelter; deprivation can also describe something being taken away from someone.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is destitute lives in extreme poverty and thus lacks the basic necessities of life.</p>
<p class='rw-defn idx13' style='display:none'>An exiguous amount of something is meager, small, or limited in nature.</p>
<p class='rw-defn idx14' style='display:none'>A genteel person is well-mannered, has good taste, and is very polite in social situations; a genteel person is often from an upper-class background.</p>
<p class='rw-defn idx15' style='display:none'>The gentry are the elite, nobility, or upper-class members of a society.</p>
<p class='rw-defn idx16' style='display:none'>Someone who is impecunious has very little money, especially over a long period of time.</p>
<p class='rw-defn idx17' style='display:none'>An indigent person is very poor.</p>
<p class='rw-defn idx18' style='display:none'>Something luxuriant, such as plants or hair, is growing well and is very healthy.</p>
<p class='rw-defn idx19' style='display:none'>A mendicant is a beggar who asks for money by day on the streets.</p>
<p class='rw-defn idx20' style='display:none'>If you have a myriad of things, you have so many and such a great variety of them that it&#8217;s hard or impossible to keep track of or count them all.</p>
<p class='rw-defn idx21' style='display:none'>Something that is opulent is very impressive, grand, and is made from expensive materials.</p>
<p class='rw-defn idx22' style='display:none'>A panoply is a large and impressive collection of people or things.</p>
<p class='rw-defn idx23' style='display:none'>A paucity of something is not enough of it.</p>
<p class='rw-defn idx24' style='display:none'>Pecuniary means relating to or concerning money.</p>
<p class='rw-defn idx25' style='display:none'>Penury is the state of being extremely poor.</p>
<p class='rw-defn idx26' style='display:none'>A plethora of something is too much of it.</p>
<p class='rw-defn idx27' style='display:none'>If something plummets, it falls down from a high position very quickly; for example, a piano can plummet from a window and a stock value can plummet during a market crash.</p>
<p class='rw-defn idx28' style='display:none'>If you suffer privation, you live without many of the basic things required for a comfortable life.</p>
<p class='rw-defn idx29' style='display:none'>A profusion of something is a very large quantity or variety of it.</p>
<p class='rw-defn idx30' style='display:none'>When you replenish something, you refill or restock it.</p>
<p class='rw-defn idx31' style='display:none'>The word squalor describes very dirty and unpleasant conditions that people live or work in, usually due to poverty or neglect.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>impoverished</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='impoverished#' id='pronounce-sound' path='audio/words/amy-impoverished'></a>
im-POV-er-ishd
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='impoverished#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='impoverished#' id='context-sound' path='audio/wordcontexts/brian-impoverished'></a>
We visited the <em>impoverished</em> or very poor neighborhood to see where we could lend a helping hand.  The <em>impoverished</em> people there had no food or money, and barely had clothes on their backs.  They lived in cardboard huts in such <em>impoverished</em> conditions that our hearts broke to see them living in such a way.  One of the problems in that poor nation was that the soil itself was <em>impoverished</em>, having been stripped of all its nutrients by years of poor farming methods.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If a neighborhood is <em>impoverished</em>, what is it?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
It is one in which a large number of poor families live.
</li>
<li class='choice '>
<span class='result'></span>
It is one in which a large number of older people live.
</li>
<li class='choice '>
<span class='result'></span>
It is one in which a large number of rich families live.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='impoverished#' id='definition-sound' path='audio/wordmeanings/amy-impoverished'></a>
An <em>impoverished</em> person or nation is very poor and stricken by poverty.
</li>
<li class='def-text'>
<em>Impoverished</em> soil has been exhausted or drained of its nutrients.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>very poor</em>
</span>
</span>
</div>
<a class='quick-help' href='impoverished#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='impoverished#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/impoverished/memory_hooks/4868.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>In</span></span> <span class="emp3"><span>Pover</span></span>ty</span></span> A nation that is <span class="emp1"><span>im</span></span><span class="emp3"><span>pover</span></span>ished is <span class="emp1"><span>in</span></span> <span class="emp3"><span>pover</span></span>ty.
</p>
</div>

<div id='memhook-button-bar'>
<a href="impoverished#" id="add-public-hook" style="" url="https://membean.com/mywords/impoverished/memory_hooks">Use other public hook</a>
<a href="impoverished#" id="memhook-use-own" url="https://membean.com/mywords/impoverished/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='impoverished#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
A few years ago, Ms. Arjun founded a telehealth program to provide free health care to members of India’s Indigenous tribes and to Dalits, who are at the lowest rungs of India’s entrenched caste system and have long faced discrimination. Dalits are typically the last to receive assistance in humanitarian disasters and often live in <b>impoverished</b> rural villages with no hospitals, medical care or schools.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
A quarter of people globally currently have incomes of less than one U.S. dollar a day, he said. Carter noted that the ratio of poor people to rich people has risen dramatically in the last three decades and now stands at 65 to 1, while in the world's most <b>impoverished</b> lands, such as Ethiopia, the ratio is 100 to 1.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
One result of these failed systems is that women have always been more likely to be <b>impoverished</b> compared with men. According to the U.S. Census Bureau, of the 38.1 million people living in poverty in 2018, 56%—or 21.4 million—were women.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
Hunter’s goal this year is to collect 150,000 pairs of shoes, 50,000 of which will go to Houston’s <b>impoverished</b> children during the Final Four.
<cite class='attribution'>
&mdash;
New York Daily News
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/impoverished/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='impoverished#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='im_in' data-tree-url='//cdn1.membean.com/public/data/treexml' href='impoverished#'>
<span class='common'></span>
im-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>in, into</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pover_poor' data-tree-url='//cdn1.membean.com/public/data/treexml' href='impoverished#'>
<span class=''></span>
pover
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>poor</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ish_do' data-tree-url='//cdn1.membean.com/public/data/treexml' href='impoverished#'>
<span class=''></span>
-ish
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to do something</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ed_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='impoverished#'>
<span class=''></span>
-ed
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>having a particular state</td>
</tr>
</table>
<p>When one has become <em>impoverished</em>, one has descended &#8220;into the state of (being) poor.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='impoverished#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Vanguard: Destination Anywhere</strong><span> While regional countries advance, the Philippines is still impoverished.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/impoverished.jpg' video_url='examplevids/impoverished' video_width='350'></span>
<div id='wt-container'>
<img alt="Impoverished" height="288" src="https://cdn1.membean.com/video/examplevids/impoverished.jpg" width="350" />
<div class='center'>
<a href="impoverished#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='impoverished#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Impoverished" src="https://cdn1.membean.com/public/images/wordimages/cons2/impoverished.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='impoverished#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>austere</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>bereaved</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>blight</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>dearth</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>deprivation</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>destitute</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>exiguous</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>genteel</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>impecunious</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>indigent</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>mendicant</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>paucity</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>penury</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>plummet</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>privation</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>squalor</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>acquisitive</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>affluence</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>avarice</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>boon</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>bourgeois</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>copious</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>cornucopia</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>gentry</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>luxuriant</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>myriad</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>opulent</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>panoply</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>pecuniary</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>plethora</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>profusion</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>replenish</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="impoverished" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>impoverished</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="impoverished#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

