
<!DOCTYPE html>
<html>
<head>
<title>Word: benevolent | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you are acquisitive, you are driven to pursue and own wealth and possessions—often in a greedy fashion.</p>
<p class='rw-defn idx1' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx2' style='display:none'>If your behavior or manner is altruistic, you show you care more about other people and their interests than you care about yourself.</p>
<p class='rw-defn idx3' style='display:none'>An amicable person is very friendly and agreeable towards others.</p>
<p class='rw-defn idx4' style='display:none'>The granting of an amnesty is forgiveness by a government for criminal political offenses.</p>
<p class='rw-defn idx5' style='display:none'>Avarice is the extremely strong desire to have a lot of money and possessions.</p>
<p class='rw-defn idx6' style='display:none'>An avuncular person is either someone&#8217;s uncle or has the qualities of an uncle.</p>
<p class='rw-defn idx7' style='display:none'>A benefaction is a charitable contribution of money or assistance that someone gives to a person or organization.</p>
<p class='rw-defn idx8' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx9' style='display:none'>Bigotry is the expression of strong and unreasonable opinions without accepting or tolerating opposing views.</p>
<p class='rw-defn idx10' style='display:none'>Someone who is cantankerous is bad-tempered and always finds things to complain about.</p>
<p class='rw-defn idx11' style='display:none'>A cavalier person does not seem to care about rules, principles, or other people&#8217;s feelings, no matter how dire or serious a situation may be.</p>
<p class='rw-defn idx12' style='display:none'>A chivalrous man behaves in a polite, kind, generous, and honorable way, especially towards women.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is churlish is impolite and unfriendly, especially towards another who has done nothing to deserve ill-treatment.</p>
<p class='rw-defn idx14' style='display:none'>If someone is granted clemency, they are punished less severely than they could have been—they have been shown mercy.</p>
<p class='rw-defn idx15' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx16' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx17' style='display:none'>A cordial greeting is warm, friendly, and polite.</p>
<p class='rw-defn idx18' style='display:none'>If you covet something that someone else has, you have a strong desire to have it for yourself.</p>
<p class='rw-defn idx19' style='display:none'>Depravity is behavior that is immoral, corrupt, or evil.</p>
<p class='rw-defn idx20' style='display:none'>A despot is a leader who has a lot of power and uses it in a cruel and unreasonable way.</p>
<p class='rw-defn idx21' style='display:none'>Draconian rules and laws are extremely strict and harsh.</p>
<p class='rw-defn idx22' style='display:none'>If you refer to the enormity of a situation, problem, or event, you mean that it is very evil or morally offensive; this word has nothing to do with physical size.</p>
<p class='rw-defn idx23' style='display:none'>When you execrate someone, you curse them to show your intense dislike or hatred of them.</p>
<p class='rw-defn idx24' style='display:none'>An infamous person has a very bad reputation because they have done disgraceful or dishonorable things.</p>
<p class='rw-defn idx25' style='display:none'>Largess is the generous act of giving money or presents to a large number of people.</p>
<p class='rw-defn idx26' style='display:none'>If someone exhibits magnanimity towards another, they show them kindness and noble generosity, especially after defeating them in battle or after having been treated badly by them.</p>
<p class='rw-defn idx27' style='display:none'>A malevolent person or thing is evil due to deliberate attempts to cause harm.</p>
<p class='rw-defn idx28' style='display:none'>A malignant thing or person, such as a tumor or criminal, is deadly or does great harm.</p>
<p class='rw-defn idx29' style='display:none'>A misanthrope is someone who hates and mistrusts people.</p>
<p class='rw-defn idx30' style='display:none'>A misogynist is someone who hates women or is highly critical about the female gender.</p>
<p class='rw-defn idx31' style='display:none'>A munificent person is extremely generous, especially with money.</p>
<p class='rw-defn idx32' style='display:none'>If you describe a person&#8217;s behavior as rapacious, you disapprove of them because they always want more money, goods, or possessions than they really need.</p>
<p class='rw-defn idx33' style='display:none'>A truculent person is bad-tempered, easily annoyed, and prone to arguing excessively.</p>
<p class='rw-defn idx34' style='display:none'>Someone who is venal is dishonest, corrupt, and ready to do anything for money.</p>
<p class='rw-defn idx35' style='display:none'>A vicious person is corrupt, violent, aggressive, and/or highly immoral.</p>
<p class='rw-defn idx36' style='display:none'>Something vile is evil, very unpleasant, horrible, or extremely bad.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>benevolent</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='benevolent#' id='pronounce-sound' path='audio/words/amy-benevolent'></a>
buh-NEV-uh-luhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='benevolent#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='benevolent#' id='context-sound' path='audio/wordcontexts/brian-benevolent'></a>
Cecelia is a <em>benevolent</em>, kindhearted person who helps people in so many ways.  Her <em>benevolent</em> and caring deeds can be seen in her volunteering and community service which she does with such goodwill.  Her <em>benevolent</em>, helpful ways have earned her many awards over the years, but we all know that she would just keep on serving others anyway.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How would you describe a <em>benevolent</em> person?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
They are generous and compassionate.
</li>
<li class='choice '>
<span class='result'></span>
They are very busy and involved in many activities.
</li>
<li class='choice '>
<span class='result'></span>
They spend too much money on needless things.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='benevolent#' id='definition-sound' path='audio/wordmeanings/amy-benevolent'></a>
Someone who is <em>benevolent</em> wishes others well, often by being kind, filled with goodwill, and charitable towards them.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>kind</em>
</span>
</span>
</div>
<a class='quick-help' href='benevolent#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='benevolent#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/benevolent/memory_hooks/4189.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Bene</span></span>ficial <span class="emp3"><span>Volunt</span></span>eer</span></span> Often the most <span class="emp1"><span>bene</span></span><span class="emp3"><span>volent</span></span> people are those who do <span class="emp1"><span>bene</span></span>ficial things for those less fortunate as they <span class="emp3"><span>volunt</span></span>eer to help them ... such <span class="emp1"><span>bene</span></span>fical <span class="emp3"><span>volunt</span></span>eering makes these people <span class="emp1"><span>bene</span></span><span class="emp3"><span>volent</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="benevolent#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/benevolent/memory_hooks">Use other hook</a>
<a href="benevolent#" id="memhook-use-own" url="https://membean.com/mywords/benevolent/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='benevolent#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Americans are <b>benevolently</b> ignorant about Canada, while Canadians are malevolently well informed about the United States.
<span class='attribution'>&mdash; J. Bartlett Brebner</span>
<img alt="J" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/J. Bartlett Brebner.jpg?qdep8" width="80" />
</li>
<li>
O, Tyler Gregory, my <b>benevolent</b> keeper, my guide and my provider — you have been nothing but generous to me in spite of my hateful nature.
<cite class='attribution'>
&mdash;
The Onion
</cite>
</li>
<li>
a wise and <b>benevolent</b> ruler, who comes in the name of peace, but will, in fact be just the opposite of that in the long run.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
This case was not about <b>benevolent</b> compassion, and as a disability rights group we are not asking for compassion — we are asking for equal opportunity and that is what the seven justices granted today.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/benevolent/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='benevolent#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='bene_well' data-tree-url='//cdn1.membean.com/public/data/treexml' href='benevolent#'>
<span class=''></span>
bene-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>well</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='vol_wish' data-tree-url='//cdn1.membean.com/public/data/treexml' href='benevolent#'>
<span class=''></span>
vol
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>wish, want</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ent_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='benevolent#'>
<span class=''></span>
-ent
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p>A <em>benevolent</em> person is often in a &#8220;state of wishing (someone or something) well.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='benevolent#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Groundhog Day</strong><span> This is a benevolent way to treat a homeless person.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/benevolent.jpg' video_url='examplevids/benevolent' video_width='350'></span>
<div id='wt-container'>
<img alt="Benevolent" height="288" src="https://cdn1.membean.com/video/examplevids/benevolent.jpg" width="350" />
<div class='center'>
<a href="benevolent#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='benevolent#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Benevolent" src="https://cdn1.membean.com/public/images/wordimages/cons2/benevolent.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='benevolent#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>altruistic</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>amicable</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>amnesty</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>avuncular</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>benefaction</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>chivalrous</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>clemency</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>cordial</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>largess</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>magnanimity</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>munificent</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>acquisitive</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>avarice</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>bigotry</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>cantankerous</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>cavalier</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>churlish</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>covet</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>depravity</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>despot</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>draconian</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>enormity</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>execrate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>infamous</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>malevolent</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>malignant</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>misanthrope</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>misogynist</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>rapacious</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>truculent</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>venal</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>vicious</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>vile</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="benevolent" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>benevolent</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="benevolent#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

