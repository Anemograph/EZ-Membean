
<!DOCTYPE html>
<html>
<head>
<title>Word: feint | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you employ artifice, you use clever tricks and cunning to deceive someone.</p>
<p class='rw-defn idx1' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx2' style='display:none'>If you employ chicanery, you are devising and carrying out clever plans and trickery to cheat and deceive people.</p>
<p class='rw-defn idx3' style='display:none'>If you circumvent something, such as a rule or restriction, you try to get around it in a clever and perhaps dishonest way.</p>
<p class='rw-defn idx4' style='display:none'>Someone who is disingenuous is not straightforward or is dishonest in what they say or do.</p>
<p class='rw-defn idx5' style='display:none'>When people dissemble, they hide their real thoughts, feelings, or intentions.</p>
<p class='rw-defn idx6' style='display:none'>When you dupe another person, you trick them into believing something that is not true.</p>
<p class='rw-defn idx7' style='display:none'>If a fact or idea eludes you, you cannot remember or understand it; if you elude someone, you manage to escape or hide from them.</p>
<p class='rw-defn idx8' style='display:none'>If you eschew something, you deliberately avoid doing it, especially for moral reasons.</p>
<p class='rw-defn idx9' style='display:none'>A facade is a false outward appearance or way of behaving that hides what someone or something is really like.</p>
<p class='rw-defn idx10' style='display:none'>If you feign something, such as a sickness or an attitude, you pretend and try to convince people that you have it—even though you don&#8217;t.</p>
<p class='rw-defn idx11' style='display:none'>A gambit is something that you say or do in order to gain an advantage in a given situation or game.</p>
<p class='rw-defn idx12' style='display:none'>People who are ingenuous are excessively trusting and believe almost everything that people tell them, especially because they have not had much life experience.</p>
<p class='rw-defn idx13' style='display:none'>A mendacious person does not tell the truth.</p>
<p class='rw-defn idx14' style='display:none'>To obfuscate something is to make it deliberately unclear, confusing, and difficult to understand.</p>
<p class='rw-defn idx15' style='display:none'>If you prevaricate, you avoid giving a direct or honest answer, usually because you want to hide the truth or want to delay or avoid making a hard decision.</p>
<p class='rw-defn idx16' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx17' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx18' style='display:none'>A ruse is a clever trick or deception used to fool someone.</p>
<p class='rw-defn idx19' style='display:none'>A semblance is an outward appearance of what is wanted or expected but is not exactly as hoped for.</p>
<p class='rw-defn idx20' style='display:none'>A stratagem is a clever trick or deception that is used to fool someone.</p>
<p class='rw-defn idx21' style='display:none'>If you employ subterfuge, you use a secret plan or action to get what you want by outwardly doing one thing that cleverly hides your true intentions.</p>
<p class='rw-defn idx22' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx23' style='display:none'>The verity of something is the truth or reality of it.</p>
<p class='rw-defn idx24' style='display:none'>Wiles are clever tricks or cunning schemes that are used to persuade someone to do what you want.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>feint</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='feint#' id='pronounce-sound' path='audio/words/amy-feint'></a>
faynt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='feint#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='feint#' id='context-sound' path='audio/wordcontexts/brian-feint'></a>
My opponent made several false movements or <em>feints</em>; his deceptive maneuvers quickly caught me off-balance and allowed him to gain the advantage.  One of the strategies of fencing is to put your attacker off her guard by <em>feinting</em> or making your opponent think you are planning a different move than the one you actually make.  With the clever and proper use of a well-placed dodge or <em>feint</em>, one can gain the upper hand during a match.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Why might someone resort to using a <em>feint</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They are exhausted after a long match against an opponent.
</li>
<li class='choice answer '>
<span class='result'></span>
They want to deceive an opponent by distracting them.
</li>
<li class='choice '>
<span class='result'></span>
They have to establish some rules with their opponent.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='feint#' id='definition-sound' path='audio/wordmeanings/amy-feint'></a>
A <em>feint</em> is the act of pretending to make a movement in one direction while actually moving in the other, especially to trick an opponent; a <em>feint</em> can also be a deceptive act meant to turn attention away from one&#8217;s true purpose.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>deception</em>
</span>
</span>
</div>
<a class='quick-help' href='feint#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='feint#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/feint/memory_hooks/3402.json'></span>
<p>
<span class="emp0"><span>Sa<span class="emp2"><span>int</span></span>'s Pa<span class="emp2"><span>int</span></span> Fe<span class="emp2"><span>int</span></span></span></span> The sa<span class="emp2"><span>int</span></span>'s pa<span class="emp2"><span>int</span></span> fe<span class="emp2"><span>int</span></span> was so clever that no one ever figured out exactly what he did, which is why I have no more information about it.
</p>
</div>

<div id='memhook-button-bar'>
<a href="feint#" id="add-public-hook" style="" url="https://membean.com/mywords/feint/memory_hooks">Use other public hook</a>
<a href="feint#" id="memhook-use-own" url="https://membean.com/mywords/feint/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='feint#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The only charitable way to interpret such a position is that it could be a clever <b>feint</b> to tempt Mr Hussein to show his true nature by modifying his offer again in the belief that the world’s great powers are disunited once more.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
The 2nd Brigade early at dawn had conducted a <b>feint</b> toward Hilla, 25 miles north, to draw attention away from the 3rd Infantry Division's movements near Karbala.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
The Germans concluded that the Allies were preparing a <b>feint</b>, a secondary invasion intended to trick them into thinking Normandy was the main attack.
<cite class='attribution'>
&mdash;
Matthew B. Caffery, On Wargaming
</cite>
</li>
<li>
A shoulder <b>feint</b> left, followed by a lightning-quick cut back to the right, and the big man might as well have been a statue.
<cite class='attribution'>
&mdash;
Associated Press
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/feint/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='feint#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='fig_shape' data-tree-url='//cdn1.membean.com/public/data/treexml' href='feint#'>
<span class=''></span>
feint
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>pretend, invent</td>
</tr>
</table>
<p>A <em>feint</em> is a trick that &#8220;pretends&#8221; to move one way or &#8220;invents&#8221; a false step to fool an opponent.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='feint#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Feint" src="https://cdn3.membean.com/public/images/wordimages/cons2/feint.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='feint#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>artifice</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>chicanery</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>circumvent</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>disingenuous</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>dissemble</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>dupe</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>elude</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>eschew</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>facade</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>feign</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>gambit</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>mendacious</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>obfuscate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>prevaricate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>ruse</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>semblance</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>stratagem</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>subterfuge</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>wile</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>ingenuous</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='feint#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
feint
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to make a tricky or deceptive move that someone does not expect</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="feint" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>feint</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="feint#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

