
<!DOCTYPE html>
<html>
<head>
<title>Word: mendicant | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>You accost a stranger when you move towards them and speak in an unpleasant or threatening way.</p>
<p class='rw-defn idx1' style='display:none'>Affluence is the state of having a lot of money and many possessions, granting one a high economic standard of living.</p>
<p class='rw-defn idx2' style='display:none'>When you bequeath something, you hand it down to someone in a will or pass it on from one generation to the next.</p>
<p class='rw-defn idx3' style='display:none'>When something is bestowed upon you—usually something valuable—you are given or presented it.</p>
<p class='rw-defn idx4' style='display:none'>When you are granted a boon, you are given a special gift or favor that is of great benefit to you.</p>
<p class='rw-defn idx5' style='display:none'>When there is a dearth of something, there is a scarcity or lack of it.</p>
<p class='rw-defn idx6' style='display:none'>Deprivation is a state during which people lack something, especially adequate food and shelter; deprivation can also describe something being taken away from someone.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is destitute lives in extreme poverty and thus lacks the basic necessities of life.</p>
<p class='rw-defn idx8' style='display:none'>Someone who is impecunious has very little money, especially over a long period of time.</p>
<p class='rw-defn idx9' style='display:none'>An impoverished person or nation is very poor and stricken by poverty.</p>
<p class='rw-defn idx10' style='display:none'>An indigent person is very poor.</p>
<p class='rw-defn idx11' style='display:none'>Largess is the generous act of giving money or presents to a large number of people.</p>
<p class='rw-defn idx12' style='display:none'>If someone exhibits magnanimity towards another, they show them kindness and noble generosity, especially after defeating them in battle or after having been treated badly by them.</p>
<p class='rw-defn idx13' style='display:none'>A munificent person is extremely generous, especially with money.</p>
<p class='rw-defn idx14' style='display:none'>A paucity of something is not enough of it.</p>
<p class='rw-defn idx15' style='display:none'>Pecuniary means relating to or concerning money.</p>
<p class='rw-defn idx16' style='display:none'>Penury is the state of being extremely poor.</p>
<p class='rw-defn idx17' style='display:none'>Philanthropy is unselfish support in the form of donating money, work, or gifts to positive social purposes; philanthropy is also overall love for humans in general.</p>
<p class='rw-defn idx18' style='display:none'>If you say that you&#8217;ve received a pittance, you mean that you received a small amount of something—and you know that you deserved more.</p>
<p class='rw-defn idx19' style='display:none'>If you suffer privation, you live without many of the basic things required for a comfortable life.</p>
<p class='rw-defn idx20' style='display:none'>When you proffer something to someone, you offer it up or hold it out to them to take.</p>
<p class='rw-defn idx21' style='display:none'>A supplicant is someone who humbly and respectfully asks for something from another who is powerful enough to grant the request.</p>
<p class='rw-defn idx22' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>
<p class='rw-defn idx23' style='display:none'>If you say someone&#8217;s behavior is unseemly, you disapprove of it because it is not in good taste or not suitable for a particular situation.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>mendicant</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='mendicant#' id='pronounce-sound' path='audio/words/amy-mendicant'></a>
MEN-di-kuhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='mendicant#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='mendicant#' id='context-sound' path='audio/wordcontexts/brian-mendicant'></a>
The increasing homeless problem in the United States has led to more and more <em>mendicants</em> or beggars living their lives on the street.  Did you know that the largest homeless shelter that feeds and houses <em>mendicants</em> or street people is located about three blocks from the White House?  <em>Mendicants</em>, or those who ask for handouts on a daily basis, often live their entire lives not knowing for sure where their next dollar will come from, although most are well fed by soup kitchens.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a mendicant?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
He is someone who spends his days wandering the streets.
</li>
<li class='choice answer '>
<span class='result'></span>
He is a person who begs for money.
</li>
<li class='choice '>
<span class='result'></span>
She is someone who mends clothing for a living.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='mendicant#' id='definition-sound' path='audio/wordmeanings/amy-mendicant'></a>
A <em>mendicant</em> is a beggar who asks for money by day on the streets.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>beggar</em>
</span>
</span>
</div>
<a class='quick-help' href='mendicant#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='mendicant#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/mendicant/memory_hooks/4010.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Can't</span></span> <span class="emp2"><span>Mend</span></span></span></span> A <span class="emp2"><span>mend</span></span>i<span class="emp1"><span>cant</span></span> <span class="emp1"><span>can't</span></span> <span class="emp2"><span>mend</span></span> his financial life, and so he has to beg for money.
</p>
</div>

<div id='memhook-button-bar'>
<a href="mendicant#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/mendicant/memory_hooks">Use other hook</a>
<a href="mendicant#" id="memhook-use-own" url="https://membean.com/mywords/mendicant/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='mendicant#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
[Moondog] also tells Schwartz that he’s comfortable being thought of as a beggar. The radio broadcaster Walter Winchell “calls me a <b>mendicant</b>, but . . . I don’t feel self-conscious or apologetic about begging for a living.
<cite class='attribution'>
&mdash;
The New Yorker
</cite>
</li>
<li>
But even as [Washington's subway system] Metro continues along as a public sector <b>mendicant</b>, begging for alms year after year from state and local authorities, it would stand to benefit enormously from the sort of independent oversight to which federal government agencies are subject in the form of an inspector general.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
In 1911, the [Chicago police department] had issued its own edict "prohibiting blind <b>mendicants</b>, legless unfortunates and other seekers of alms from exhibiting their misfortunes to the public view," but after World War I ended in 1918, no new ugly laws were passed. Instead, plans were made to help manage veterans' physical and mental care.
<cite class='attribution'>
&mdash;
Chicago Tribune
</cite>
</li>
<li>
Legend has it that one day he went out from the palace and for the first time saw poverty, sickness, and death. Overwhelmed by these realities, he renounced his worldly position and became a wandering <b>mendicant</b>, seeking the meaning of life. After years of fasting, begging, and traveling, he sat down under a _bodhi_ tree and sank into a deep meditation lasting 49 days. At last he achieved enlightenment, and Siddhartha became a buddha.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/mendicant/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='mendicant#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='mend_fault' data-tree-url='//cdn1.membean.com/public/data/treexml' href='mendicant#'>
<span class=''></span>
mend
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>fault, defect</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ant_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='mendicant#'>
<span class=''></span>
-ant
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p>A <em>mendicant</em> is in &#8220;defective&#8221; or &#8220;wanting&#8221; circumstances.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='mendicant#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Mendicant" src="https://cdn1.membean.com/public/images/wordimages/cons2/mendicant.png?qdep6" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='mendicant#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>accost</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>dearth</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>deprivation</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>destitute</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>impecunious</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>impoverished</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>indigent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>paucity</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>penury</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>privation</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>supplicant</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>unseemly</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>affluence</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bequeath</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bestow</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>boon</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>largess</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>magnanimity</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>munificent</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>pecuniary</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>philanthropy</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>pittance</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>proffer</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='mendicant#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
mendicant
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>begging; living on charity</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="mendicant" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>mendicant</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="mendicant#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

