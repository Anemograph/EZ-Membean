
<!DOCTYPE html>
<html>
<head>
<title>Word: travesty | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you adulterate something, you lessen its quality by adding inferior ingredients or elements to it.</p>
<p class='rw-defn idx1' style='display:none'>To aggrandize someone means to make them seem richer, more powerful, and more important than they really are.</p>
<p class='rw-defn idx2' style='display:none'>When you ameliorate a bad condition or situation, you make it better in some way.</p>
<p class='rw-defn idx3' style='display:none'>When one thing belies a second, it hides the true situation, producing a false idea or impression about that second thing.</p>
<p class='rw-defn idx4' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx5' style='display:none'>A caricature of someone is an exaggerated drawing or description that is designed to make that person look silly; it can also be an exaggerated description of a situation or event.</p>
<p class='rw-defn idx6' style='display:none'>If you treat someone with derision, you mock or make fun of them because you think they are stupid, unimportant, or useless.</p>
<p class='rw-defn idx7' style='display:none'>Empirical evidence or study is based on real experience or scientific experiments rather than on unproven theories.</p>
<p class='rw-defn idx8' style='display:none'>A situation or event is a farce if it is very badly organized and consequently unsuccessful—or so silly and ridiculous that you cannot take it seriously.</p>
<p class='rw-defn idx9' style='display:none'>Two items are fungible if one can be exchanged for the other with no loss in inherent value; for example, one $10 bill and two $5 bills are fungible.</p>
<p class='rw-defn idx10' style='display:none'>Hyperbole is a way of emphasizing something that makes it sound much more impressive or much worse than it actually is.</p>
<p class='rw-defn idx11' style='display:none'>Someone, such as a performer or athlete, is inimitable when they are so good or unique in their talent that it is unlikely anyone else can be their equal.</p>
<p class='rw-defn idx12' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx13' style='display:none'>When someone or something undergoes the process of metamorphosis, there is a change in appearance, character, or even physical shape.</p>
<p class='rw-defn idx14' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx15' style='display:none'>If you mollify someone, you say or do something to make that person feel less angry or upset.</p>
<p class='rw-defn idx16' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx17' style='display:none'>A parody is a humorous imitation of more serious writing or music.</p>
<p class='rw-defn idx18' style='display:none'>A pastiche is a piece of writing, music, or film, etc. that is deliberately made in the style of other works; it can also use a variety of such copied material in order to laugh in a gentle way at those works.</p>
<p class='rw-defn idx19' style='display:none'>Satire is a way of criticizing people or ideas in a humorous way by going beyond the truth; it is often used to make people see their faults.</p>
<p class='rw-defn idx20' style='display:none'>A semblance is an outward appearance of what is wanted or expected but is not exactly as hoped for.</p>
<p class='rw-defn idx21' style='display:none'>A simulacrum is an image or representation of something that can be a true copy or may just have a vague similarity to it.</p>
<p class='rw-defn idx22' style='display:none'>Something transmutes when it changes from one form or state into another.</p>
<p class='rw-defn idx23' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx24' style='display:none'>Verisimilitude is something&#8217;s authenticity or appearance of being real or true.</p>
<p class='rw-defn idx25' style='display:none'>The verity of something is the truth or reality of it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>travesty</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='travesty#' id='pronounce-sound' path='audio/words/amy-travesty'></a>
TRAV-uh-stee
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='travesty#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='travesty#' id='context-sound' path='audio/wordcontexts/brian-travesty'></a>
We found the production of Shakespeare&#8217;s <em>Hamlet</em> to be a complete <em>travesty</em> that shamed the honorable Bard.  How could actors lend their talents to such a ridiculous and absurd version of this wonderful play, such a <em>travesty</em> of good theater?  We all wondered how such a group of horrible actors and even worse director could survive such a <em>travesty</em> or terrible representation that they had put on the night before.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>travesty</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A ridiculously bad revival of a play.
</li>
<li class='choice '>
<span class='result'></span>
A play on words that is somewhat funny.
</li>
<li class='choice '>
<span class='result'></span>
A poorly worded remark about a play.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='travesty#' id='definition-sound' path='audio/wordmeanings/amy-travesty'></a>
If you say that one thing is a <em>travesty</em> of a second thing, you mean that the first is an extremely bad, crude, or absurd version of the second.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>really bad version</em>
</span>
</span>
</div>
<a class='quick-help' href='travesty#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='travesty#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/travesty/memory_hooks/2780.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Trav</span></span>eling Dishon<span class="emp2"><span>est</span></span>l<span class="emp2"><span>y</span></span></span></span> That <span class="emp1"><span>trav</span></span>eling doctor makes a <span class="emp1"><span>trav</span></span><span class="emp2"><span>esty</span></span> out of the medical profession because he dishon<span class="emp2"><span>est</span></span>l<span class="emp2"><span>y</span></span> says that he's a doctor when he's really not.
</p>
</div>

<div id='memhook-button-bar'>
<a href="travesty#" id="add-public-hook" style="" url="https://membean.com/mywords/travesty/memory_hooks">Use other public hook</a>
<a href="travesty#" id="memhook-use-own" url="https://membean.com/mywords/travesty/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='travesty#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Mr. Hinton’s conviction was eventually reversed by a unanimous U.S. Supreme Court—though only after he had served three decades on Alabama’s death row. The <b>travesty</b> of justice in his case may not have been the specific result of the racial cues and codes he saw all around him. Yet “perception is often deemed reality to those participating in the justice system.”
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
The film would be a laughable <b>travesty</b> were it not directed by Carol Reed, who made such superb films as The Third Man, Odd Man Out and Outcast of the Islands. That makes it a sad <b>travesty</b>.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
<li>
In short, the mechanic, who as the model maker elaborates the inventor's idea, is often the real inventor. The crude, unworkmanlike contrivance of the inventor, that in his unskillful hands is merely a <b>travesty</b> [sitting] on a machine, is made to assume form, proportions, elegance, and efficiency.
<cite class='attribution'>
&mdash;
Scientific American
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/travesty/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='travesty#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='trans_across' data-tree-url='//cdn1.membean.com/public/data/treexml' href='travesty#'>
<span class='common'></span>
trans-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>across, through</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='vest_clothing' data-tree-url='//cdn1.membean.com/public/data/treexml' href='travesty#'>
<span class=''></span>
vest
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>clothing, garments</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='y_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='travesty#'>
<span class=''></span>
-y
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state or condition</td>
</tr>
</table>
<p>A <em>travesty</em> of something is a &#8220;cross dressing&#8221; or inferior imitation of that thing.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='travesty#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Travesty" src="https://cdn2.membean.com/public/images/wordimages/cons2/travesty.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='travesty#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adulterate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>aggrandize</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>belie</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>caricature</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>derision</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>farce</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>hyperbole</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>metamorphosis</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>parody</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>pastiche</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>satire</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>semblance</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>simulacrum</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>transmute</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>ameliorate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>empirical</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>fungible</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>inimitable</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>mollify</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>verisimilitude</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="travesty" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>travesty</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="travesty#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

