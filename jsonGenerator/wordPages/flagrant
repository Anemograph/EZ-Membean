
<!DOCTYPE html>
<html>
<head>
<title>Word: flagrant | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Flagrant-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/flagrant-large.jpg?qdep8" />
</div>
<a href="flagrant#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>The word abject emphasizes a very bad situation or quality, thereby making it even worse.</p>
<p class='rw-defn idx1' style='display:none'>You accost a stranger when you move towards them and speak in an unpleasant or threatening way.</p>
<p class='rw-defn idx2' style='display:none'>You affront someone by openly and intentionally offending or insulting him.</p>
<p class='rw-defn idx3' style='display:none'>If you are amenable to doing something, you willingly accept it without arguing.</p>
<p class='rw-defn idx4' style='display:none'>If you describe a person&#8217;s behavior as brazen, you mean that they are not embarrassed by anything they do—they simply don&#8217;t care what other people think about them.</p>
<p class='rw-defn idx5' style='display:none'>Covert activities or situations are secret and hidden.</p>
<p class='rw-defn idx6' style='display:none'>If you are culpable for an action, you are held responsible for something wrong or bad that has happened.</p>
<p class='rw-defn idx7' style='display:none'>A man who is debonair wears fashionable clothes and is sophisticated, charming, friendly, and confident.</p>
<p class='rw-defn idx8' style='display:none'>A deft movement or action is made quickly and with skill.</p>
<p class='rw-defn idx9' style='display:none'>If you say a person&#8217;s actions are despicable, you think they are extremely unpleasant or nasty.</p>
<p class='rw-defn idx10' style='display:none'>If you say something is diabolical, you are emphasizing that it is evil, cruel, or very bad.</p>
<p class='rw-defn idx11' style='display:none'>Effrontery is very rude behavior that shows a great lack of respect and is often insulting.</p>
<p class='rw-defn idx12' style='display:none'>An egregious mistake, failure, or problem is an extremely bad and very noticeable one.</p>
<p class='rw-defn idx13' style='display:none'>If someone exhibits finesse in something, they do it with great skill and care; this most often refers to handling difficult situations that might easily offend people.</p>
<p class='rw-defn idx14' style='display:none'>If you describe something as heinous, you mean that is extremely evil, shocking, and bad.</p>
<p class='rw-defn idx15' style='display:none'>Something that is imperceptible is either impossible to be perceived by the mind or is very difficult to perceive.</p>
<p class='rw-defn idx16' style='display:none'>If someone behaves in an impertinent way, they behave rudely and disrespectfully.</p>
<p class='rw-defn idx17' style='display:none'>Something that is inconspicuous does not attract attention and is not easily seen or noticed because it is small or ordinary.</p>
<p class='rw-defn idx18' style='display:none'>Something that inhibits you from doing something restricts or keeps you from doing it.</p>
<p class='rw-defn idx19' style='display:none'>A nefarious activity is considered evil and highly dishonest.</p>
<p class='rw-defn idx20' style='display:none'>If something is noisome, it is extremely unpleasant, especially because it is very dirty or has an awful smell.</p>
<p class='rw-defn idx21' style='display:none'>If you describe people or things as odious, you think that they are extremely unpleasant.</p>
<p class='rw-defn idx22' style='display:none'>To be politic in a decision is to be socially wise and diplomatic.</p>
<p class='rw-defn idx23' style='display:none'>If you think a type of behavior or idea is reprehensible, you think that it is very bad, morally wrong, and deserves to be strongly criticized.</p>
<p class='rw-defn idx24' style='display:none'>A reprobate has a bad character and behaves in an immoral or improper way.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx26' style='display:none'>An action or deed is unconscionable if it is excessively shameful, unfair, or unjust and its effects are more severe than is reasonable or acceptable.</p>
<p class='rw-defn idx27' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>
<p class='rw-defn idx28' style='display:none'>If you are being wary about a situation, you are being careful, cautious, or on your guard because you are concerned about something.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>flagrant</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='flagrant#' id='pronounce-sound' path='audio/words/amy-flagrant'></a>
FLAY-gruhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='flagrant#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='flagrant#' id='context-sound' path='audio/wordcontexts/brian-flagrant'></a>
In the headline of the newspaper there was a <em>flagrant</em>, very bad error: the name of the President had been misspelled!  When the senior editor saw the mistake, he knew that one of his employees had <em>flagrantly</em> and shockingly ignored his final instructions in the editing room.  Whoever had caused this damaging misprint clearly had a <em>flagrant</em>, offensive disregard for his authority and for the reputation of the famous paper.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If a player committed a <em>flagrant</em> foul in a game, what did they do?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
They deliberately fouled without concern for the rules.
</li>
<li class='choice '>
<span class='result'></span>
They fouled in such a way as to not get caught by an official.
</li>
<li class='choice '>
<span class='result'></span>
They did not commit a foul on purpose.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='flagrant#' id='definition-sound' path='audio/wordmeanings/amy-flagrant'></a>
An action that is <em>flagrant</em> shows that someone does not care if they obviously break the rules or highly offend people.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>highly offensive</em>
</span>
</span>
</div>
<a class='quick-help' href='flagrant#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='flagrant#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/flagrant/memory_hooks/5868.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Flag</span></span> <span class="emp2"><span>Rant</span></span></span></span> When the man committed the <span class="emp1"><span>flag</span></span><span class="emp2"><span>rant</span></span> deed of <span class="emp2"><span>rant</span></span>ing against his own country's <span class="emp1"><span>flag</span></span>, he was heavily fined for such a <span class="emp1"><span>flag</span></span><span class="emp2"><span>rant</span></span> <span class="emp1"><span>flag</span></span> <span class="emp2"><span>rant</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="flagrant#" id="add-public-hook" style="" url="https://membean.com/mywords/flagrant/memory_hooks">Use other public hook</a>
<a href="flagrant#" id="memhook-use-own" url="https://membean.com/mywords/flagrant/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='flagrant#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Fined 7,500 and assessed one <b>flagrant</b> foul point for throwing Denver’s Dikembe Mutombo to the floor in a game.
<cite class='attribution'>
&mdash;
LA Times
</cite>
</li>
<li>
Los Angeles Mayor Eric Garcetti said on Wednesday that he had authorized the city to disconnect utility service at a Hollywood Hills house after it hosted several large parties in "<b>flagrant</b> violation" of COVID-19 public health orders. The announcement comes two weeks after Garcetti first warned that properties hosting "un-permitted large gatherings" could have their water and power service shut off as a consequence.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/flagrant/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='flagrant#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='flagr_burn' data-tree-url='//cdn1.membean.com/public/data/treexml' href='flagrant#'>
<span class=''></span>
flagr
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>burn</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ant_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='flagrant#'>
<span class=''></span>
-ant
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p>A <em>flagrant</em> penalty or crime is so &#8220;bad&#8221; and &#8220;easily noticed&#8221; that it is &#8220;burning&#8221; or &#8220;blazing&#8221; in its visual intensity.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='flagrant#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Black Books</strong><span> This shopkeeper is acting in a flagrant fashion towards his customers.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/flagrant.jpg' video_url='examplevids/flagrant' video_width='350'></span>
<div id='wt-container'>
<img alt="Flagrant" height="288" src="https://cdn1.membean.com/video/examplevids/flagrant.jpg" width="350" />
<div class='center'>
<a href="flagrant#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='flagrant#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Flagrant" src="https://cdn1.membean.com/public/images/wordimages/cons2/flagrant.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='flagrant#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abject</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>accost</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>affront</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>brazen</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>culpable</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>despicable</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>diabolical</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>effrontery</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>egregious</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>heinous</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>impertinent</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>nefarious</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>noisome</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>odious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>reprehensible</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>reprobate</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>unconscionable</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>amenable</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>covert</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>debonair</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>deft</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>finesse</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>imperceptible</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>inconspicuous</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>inhibit</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>politic</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>wary</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="flagrant" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>flagrant</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="flagrant#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

