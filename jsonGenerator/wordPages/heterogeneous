
<!DOCTYPE html>
<html>
<head>
<title>Word: heterogeneous | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>A chronic illness or pain is serious and lasts for a long time; a chronic problem is always happening or returning and is very difficult to solve.</p>
<p class='rw-defn idx1' style='display:none'>When you correlate two things, you compare, associate, or equate them together in some way.</p>
<p class='rw-defn idx2' style='display:none'>Things that are disparate are clearly different from each other and belong to different groups or classes.</p>
<p class='rw-defn idx3' style='display:none'>When there is a disparity between two things, they are not of equal status; therefore, they are different or unlike in some way.</p>
<p class='rw-defn idx4' style='display:none'>Divergent opinions differ from each other; divergent paths move apart from one another.</p>
<p class='rw-defn idx5' style='display:none'>An eclectic assortment of things or people comes from many varied sources; additionally, it usually includes the best of those sources.</p>
<p class='rw-defn idx6' style='display:none'>Ecumenical activities and ideas encourage different religions or congregations to work and worship together in order to unite them in friendship.</p>
<p class='rw-defn idx7' style='display:none'>A farrago is a confused collection or mixture of different types of things.</p>
<p class='rw-defn idx8' style='display:none'>The word homogeneous is used to describe a group that has members or parts which are similar or are all of the same type.</p>
<p class='rw-defn idx9' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx10' style='display:none'>One thing that is incommensurate with another is different in its level, size, or quality from the second; this may lead to an unfair situation.</p>
<p class='rw-defn idx11' style='display:none'>Something that is incongruous is strange when considered along with its group because it seems very different from the other things in its group.</p>
<p class='rw-defn idx12' style='display:none'>Living things are indigenous to a region or country if they originated there, rather than coming from another area of the world.</p>
<p class='rw-defn idx13' style='display:none'>Two irreconcilable opinions or points of view are so opposed to each other that it is not possible to accept both of them or reach a settlement between them.</p>
<p class='rw-defn idx14' style='display:none'>A medley is a collection of various things, such as different tunes in a song or different types of food in a meal.</p>
<p class='rw-defn idx15' style='display:none'>A melange is a collection of different kinds of things.</p>
<p class='rw-defn idx16' style='display:none'>A monochrome painting is comprised of different shades of only one color or is done in black and white.</p>
<p class='rw-defn idx17' style='display:none'>A collection of things is motley if its parts are so different that they do not seem to belong in the same space; groups of widely variegated people can also be described as motley.</p>
<p class='rw-defn idx18' style='display:none'>Something that is multifarious is made up of many kinds of different things.</p>
<p class='rw-defn idx19' style='display:none'>If you have a myriad of things, you have so many and such a great variety of them that it&#8217;s hard or impossible to keep track of or count them all.</p>
<p class='rw-defn idx20' style='display:none'>A panoply is a large and impressive collection of people or things.</p>
<p class='rw-defn idx21' style='display:none'>Someone has a parochial outlook when they are mostly concerned with narrow or limited issues that affect a small, local area or a very specific cause; they are also unconcerned with wider or more general issues.</p>
<p class='rw-defn idx22' style='display:none'>A pastiche is a piece of writing, music, or film, etc. that is deliberately made in the style of other works; it can also use a variety of such copied material in order to laugh in a gentle way at those works.</p>
<p class='rw-defn idx23' style='display:none'>Something that is perennial lasts a very long time and is enduring.</p>
<p class='rw-defn idx24' style='display:none'>A smorgasbord is a wide variety of things, generally but not always pertaining to a sizable buffet.</p>
<p class='rw-defn idx25' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx26' style='display:none'>Sundry people or things are all different from each other and cannot form a single group; nevertheless, they can be thought of as being together for the sake of ease, such as the various items in a garage.</p>
<p class='rw-defn idx27' style='display:none'>Something that is variegated has various tones or colors; it can also mean filled with variety.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>heterogeneous</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='heterogeneous#' id='pronounce-sound' path='audio/words/amy-heterogeneous'></a>
het-er-uh-JEE-nee-uhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='heterogeneous#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='heterogeneous#' id='context-sound' path='audio/wordcontexts/brian-heterogeneous'></a>
My reading tastes are highly <em>heterogeneous</em> or widely differing in style.  This <em>heterogeneous</em> or varied mixture includes such widely different writing as urban fantasy, Sanskrit poetry, and the 19th-century Russian novel.  My friends wonder at the assorted or <em>heterogeneous</em> titles in my library, but I think the wide number of subjects makes me a well-read person.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If a teacher creates <em>heterogeneous</em> groups of students, what are they doing?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Forming groups of students who all have something in common with each other. 
</li>
<li class='choice '>
<span class='result'></span>
Forming groups that all have the same number of students in them.
</li>
<li class='choice answer '>
<span class='result'></span>
Forming groups of students who have differing backgrounds and perspectives.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='heterogeneous#' id='definition-sound' path='audio/wordmeanings/amy-heterogeneous'></a>
A <em>heterogeneous</em> grouping is made up of many differing or unlike parts.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>varied</em>
</span>
</span>
</div>
<a class='quick-help' href='heterogeneous#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='heterogeneous#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/heterogeneous/memory_hooks/3795.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Heater</span></span> <span class="emp3"><span>Generous</span></span></span></span> I have <span class="emp1"><span>heter</span></span>o<span class="emp3"><span>geneous</span></span> sources of <span class="emp1"><span>heat</span></span> in my house, which is <span class="emp3"><span>generous</span></span> in numbers of <span class="emp1"><span>heater</span></span>s, including space <span class="emp1"><span>heater</span></span>s, a wood stove, a propane furnace, and a wood pellet stove.
</p>
</div>

<div id='memhook-button-bar'>
<a href="heterogeneous#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/heterogeneous/memory_hooks">Use other hook</a>
<a href="heterogeneous#" id="memhook-use-own" url="https://membean.com/mywords/heterogeneous/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='heterogeneous#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
We are a <b>heterogeneous</b> party made up of Americans of diverse backgrounds.
<cite class='attribution'>
&mdash;
 Barbara Jordan, civil rights leader and U.S. Congresswoman, from her keynote address at the 1976 Democratic National Convention 
</cite>
</li>
<li>
One of the things I do with my time off is run the Francophone Choir of Beijing. There are about 40 of us, a <b>heterogeneous</b> bunch of Chinese and foreigners who speak French and enjoy singing, and on Monday we were invited to perform at China’s Shanghai World Expo.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
Barratt told _Recode_ that Fiber is not fixated on a single technology approach to broadband service: “We think, over time, there will be a sort of <b>heterogeneous</b> mix of technologies that we can use, depending upon the type of problem we’re trying to solve.”
<cite class='attribution'>
&mdash;
CNBC
</cite>
</li>
<li>
Many people have experienced that decisions are better when they come from a group with diverse backgrounds and perspectives. A study from the Kellogg School of Management concludes that <b>heterogeneous</b> groups get better results than homogeneous groups because the resulting tension or discomfort leads to more careful processing of information.
<cite class='attribution'>
&mdash;
Huffington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/heterogeneous/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='heterogeneous#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='hetero_other' data-tree-url='//cdn1.membean.com/public/data/treexml' href='heterogeneous#'>
<span class=''></span>
hetero-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>other</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='gen_kind' data-tree-url='//cdn1.membean.com/public/data/treexml' href='heterogeneous#'>
<span class=''></span>
gen
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>kind, type, class</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ous_possessing' data-tree-url='//cdn1.membean.com/public/data/treexml' href='heterogeneous#'>
<span class=''></span>
-ous
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>possessing the nature of</td>
</tr>
</table>
<p><em>Heterogeneous</em> means consisting of &#8220;other kinds or types&#8221; of components, rather than all the same.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='heterogeneous#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Coca Cola Advertisement: America the Beautiful</strong><span> Heterogeneous North America.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/heterogeneous.jpg' video_url='examplevids/heterogeneous' video_width='350'></span>
<div id='wt-container'>
<img alt="Heterogeneous" height="288" src="https://cdn1.membean.com/video/examplevids/heterogeneous.jpg" width="350" />
<div class='center'>
<a href="heterogeneous#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='heterogeneous#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Heterogeneous" src="https://cdn1.membean.com/public/images/wordimages/cons2/heterogeneous.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='heterogeneous#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>disparate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>disparity</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>divergent</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>eclectic</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>ecumenical</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>farrago</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>incommensurate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>incongruous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>irreconcilable</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>medley</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>melange</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>motley</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>multifarious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>myriad</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>panoply</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>pastiche</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>smorgasbord</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>sundry</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>variegated</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>chronic</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>correlate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>homogeneous</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>indigenous</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>monochrome</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>parochial</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>perennial</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="heterogeneous" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>heterogeneous</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="heterogeneous#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

