
<!DOCTYPE html>
<html>
<head>
<title>Word: somatic | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Somatic-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/somatic-large.jpg?qdep8" />
</div>
<a href="somatic#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>Something anthropomorphic is not human, but is shaped like a human or has human characteristics, such as behavior.</p>
<p class='rw-defn idx1' style='display:none'>Cogitating about something is thinking deeply about it for a long time.</p>
<p class='rw-defn idx2' style='display:none'>Cognitive describes those things related to judgment, memory, and other mental processes of knowing.</p>
<p class='rw-defn idx3' style='display:none'>The word corporeal refers to the physical or material world rather than the spiritual; it also means characteristic of the body rather than the mind or feelings.</p>
<p class='rw-defn idx4' style='display:none'>Someone who is corpulent is extremely fat.</p>
<p class='rw-defn idx5' style='display:none'>A person or animal that is emaciated is extremely thin because of a serious illness or lack of food.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is erudite is steeped in knowledge because they have read and studied extensively.</p>
<p class='rw-defn idx7' style='display:none'>A gaunt person is extremely thin, often due to an extended illness or a lack of food over a long period of time.</p>
<p class='rw-defn idx8' style='display:none'>If someone is introspective, they spend a lot of time examining their own feelings, thoughts, or ideas.</p>
<p class='rw-defn idx9' style='display:none'>If you are pensive, you are deeply thoughtful, often in a sad and/or serious way.</p>
<p class='rw-defn idx10' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx11' style='display:none'>A reverie is a state of pleasant dreamlike thoughts that makes you forget what you are doing and what is happening around you.</p>
<p class='rw-defn idx12' style='display:none'>A sagacious person is wise, intelligent, and has the ability to make good practical decisions.</p>
<p class='rw-defn idx13' style='display:none'>A savant is a person who knows a lot about a subject, either via a lifetime of learning or by considerable natural ability.</p>
<p class='rw-defn idx14' style='display:none'>A sentient being is able to experience the world through its senses; it may have emotional feelings as well.</p>
<p class='rw-defn idx15' style='display:none'>Something that is tangible is able to be touched and thus is considered real.</p>
<p class='rw-defn idx16' style='display:none'>Taxonomy is the science of classifying plants and animals into an organized system.</p>
<p class='rw-defn idx17' style='display:none'>A visceral feeling or reaction is strong and difficult to control or ignore; it arises through instinct or &#8220;the gut&#8221; rather than through careful thought.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>somatic</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='somatic#' id='pronounce-sound' path='audio/words/amy-somatic'></a>
soh-MAT-ik
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='somatic#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='somatic#' id='context-sound' path='audio/wordcontexts/brian-somatic'></a>
When James pursued his veterinary studies, he learned the <em>somatic</em> or physical structure of many animal species with which he might never work.  He was determined, however, to absorb every piece of <em>somatic</em> or bodily knowledge that he could in case it would ever be needed.  James may never have a wounded Bengal tiger in his clinic, but he nevertheless memorized the <em>somatic</em> workings of the exotic creature&#8217;s muscles, skeletal system, and internal organs anyhow.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If you take a class to learn about <em>somatic</em> diseases, what are you going to learn the most about?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Diseases that affect plants.
</li>
<li class='choice '>
<span class='result'></span>
Diseases that affect people&#8217;s ability to sleep.
</li>
<li class='choice answer '>
<span class='result'></span>
Diseases that affect the human body.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='somatic#' id='definition-sound' path='audio/wordmeanings/amy-somatic'></a>
If you say that something is <em>somatic</em>, you mean that it relates to or affects the body and not the mind.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>bodily</em>
</span>
</span>
</div>
<a class='quick-help' href='somatic#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='somatic#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/somatic/memory_hooks/4551.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Som</span></span>e in the <span class="emp2"><span>Attic</span></span></span></span> We found <span class="emp1"><span>som</span></span>e dead bodies in the <span class="emp2"><span>attic</span></span> of the mass murderer-- talk about <span class="emp1"><span>som</span></span><span class="emp2"><span>atic</span></span> <span class="emp2"><span>attic</span></span>s!
</p>
</div>

<div id='memhook-button-bar'>
<a href="somatic#" id="add-public-hook" style="" url="https://membean.com/mywords/somatic/memory_hooks">Use other public hook</a>
<a href="somatic#" id="memhook-use-own" url="https://membean.com/mywords/somatic/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='somatic#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
In the end, skin color and other <b>somatic</b> differences mean so many different things at different times and places that it's hard to generalize about them.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Many people avoid any reminders or thoughts about the trauma, causing <b>somatic</b> delusions in which emotions appear as physical manifestations of the mental disorder.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
[T]he dancers assigned to each audience member acted as intimate <b>somatic</b> guides. At one point, putting fingers to our chins to signal that we should open our mouths, the dancers placed on our tongues a gelatinous capsule at once bland, watery and slightly acidic. (It was made with bergamot tea, lemon, and baobab fruit.)
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
Today, in these digital and globalized days, Mayan healers are still employed by both indigenous and non-indigenous people. One of the reasons for this, perhaps the main one, is the integral, warm attention that these Mayan medical practitioners give to the patient, by dealing with both, the emotional and the <b>somatic</b> aspect of the illness.
<cite class='attribution'>
&mdash;
The Yucatan Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/somatic/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='somatic#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='somat_body' data-tree-url='//cdn1.membean.com/public/data/treexml' href='somatic#'>
<span class=''></span>
somat
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>body</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ic_like' data-tree-url='//cdn1.membean.com/public/data/treexml' href='somatic#'>
<span class=''></span>
-ic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>nature of, like</td>
</tr>
</table>
<p>The word <em>somatic</em> pertains to the &#8220;nature of the body&#8221; instead of the mind.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='somatic#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Somatic" src="https://cdn0.membean.com/public/images/wordimages/cons2/somatic.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='somatic#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>anthropomorphic</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>corporeal</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>corpulent</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>emaciated</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>gaunt</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>tangible</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>taxonomy</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>visceral</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>cogitate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>cognitive</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>erudite</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>introspective</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>pensive</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>reverie</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>sagacious</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>savant</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>sentient</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="somatic" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>somatic</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="somatic#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

