
<!DOCTYPE html>
<html>
<head>
<title>Word: disarray | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you act with abandon, you give in to the impulse of the moment and behave in a wild, uncontrolled way.</p>
<p class='rw-defn idx1' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx2' style='display:none'>An abortive attempt or action is cut short before it is finished; hence, it is unsuccessful.</p>
<p class='rw-defn idx3' style='display:none'>When you are in accord with someone, you are in agreement or harmony with them.</p>
<p class='rw-defn idx4' style='display:none'>Something that is amorphous has no clear shape, boundaries, or structure.</p>
<p class='rw-defn idx5' style='display:none'>A state of anarchy occurs when there is no organization or order in a nation or country, especially when no effective government exists.</p>
<p class='rw-defn idx6' style='display:none'>If you describe a decision, rule, or plan as arbitrary, you think that it was decided without any thought, standard, or system to guide it; therefore, it can seem unfair.</p>
<p class='rw-defn idx7' style='display:none'>Bedlam is a situation with a great deal of noise and confusion.</p>
<p class='rw-defn idx8' style='display:none'>Someone who is boisterous is noisy, excitable, and full of boundless energy; therefore, they show a lack of disciplined restraint at times.</p>
<p class='rw-defn idx9' style='display:none'>A cataclysm is a violent, sudden event that causes great change and/or harm.</p>
<p class='rw-defn idx10' style='display:none'>A chaotic state of affairs is in a state of confusion and complete disorder.</p>
<p class='rw-defn idx11' style='display:none'>A cohesive argument sticks together, working as a consistent, unified whole.</p>
<p class='rw-defn idx12' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx13' style='display:none'>Something, such as a building, is derelict if it is empty, not used, and in bad condition or disrepair.</p>
<p class='rw-defn idx14' style='display:none'>A dilapidated building, vehicle, etc. is old, broken-down, and in very bad condition.</p>
<p class='rw-defn idx15' style='display:none'>When you are discombobulated, you are confused and upset because you have been thrown into a situation that you cannot temporarily handle.</p>
<p class='rw-defn idx16' style='display:none'>A situation or thing that is discordant does not fit in with other things; therefore, it is disagreeable, strange, or unpleasant.</p>
<p class='rw-defn idx17' style='display:none'>When someone feels disquiet about a situation, they feel very worried or nervous.</p>
<p class='rw-defn idx18' style='display:none'>Dissolution is the breaking up or official end of a group, such as a couple or institution; it can also be the act of separating something into smaller components.</p>
<p class='rw-defn idx19' style='display:none'>Entropy is the lack of organization or measure of disorder currently in a system.</p>
<p class='rw-defn idx20' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx21' style='display:none'>Euphony is a pleasing sound in speech or music.</p>
<p class='rw-defn idx22' style='display:none'>To foment is to encourage people to protest, fight, or cause trouble and violent opposition to something that is viewed by some as undesirable.</p>
<p class='rw-defn idx23' style='display:none'>A fracas is a rough and noisy fight or loud argument that can involve multiple people.</p>
<p class='rw-defn idx24' style='display:none'>If someone is fractious, they are easily upset or annoyed over unimportant things.</p>
<p class='rw-defn idx25' style='display:none'>A halcyon time is calm, peaceful, and undisturbed.</p>
<p class='rw-defn idx26' style='display:none'>When two people are in a harmonious state, they are in agreement with each other; when a sound is harmonious, it is pleasant or agreeable to the ear.</p>
<p class='rw-defn idx27' style='display:none'>When there is havoc, there is great disorder, widespread destruction, and much confusion.</p>
<p class='rw-defn idx28' style='display:none'>If someone is imperturbable, they are always calm and not easily upset or disturbed by any situation, even dangerous ones.</p>
<p class='rw-defn idx29' style='display:none'>If an idea, plan, or attitude is inchoate, it is vague and imperfectly formed because it is just starting to develop.</p>
<p class='rw-defn idx30' style='display:none'>An insurrection is a rebellion or open uprising against an established form of government.</p>
<p class='rw-defn idx31' style='display:none'>A maelstrom is either a large whirlpool in the sea or a violent or agitated state of affairs.</p>
<p class='rw-defn idx32' style='display:none'>A melee is a noisy, confusing, hand-to-hand fight involving a group of people.</p>
<p class='rw-defn idx33' style='display:none'>Pandemonium is a very noisy and uncontrolled situation, especially one that is caused by a lot of angry or excited people.</p>
<p class='rw-defn idx34' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx35' style='display:none'>Someone who is profligate is lacking in restraint, which can manifest in carelessly and wastefully using resources, such as energy or money; this kind of person can also act in an immoral way.</p>
<p class='rw-defn idx36' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx37' style='display:none'>A state of quiescence is one of quiet and restful inaction.</p>
<p class='rw-defn idx38' style='display:none'>If something bad, such as crime or disease, is rampant, there is a lot of it—and it is increasing in a way that is difficult to control.</p>
<p class='rw-defn idx39' style='display:none'>You show rectitude if you behave or conduct yourself in an honest and morally correct manner.</p>
<p class='rw-defn idx40' style='display:none'>A rubric is a set of instructions at the beginning of a document, such as an examination or term paper, that is usually printed in a different style so as to highlight its importance.</p>
<p class='rw-defn idx41' style='display:none'>Sedition is the act of encouraging people to disobey and oppose the government currently in power.</p>
<p class='rw-defn idx42' style='display:none'>A serene place or situation is peaceful and calm.</p>
<p class='rw-defn idx43' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx44' style='display:none'>Something statutory, such as the power given to a governor or president, is created, established, and controlled by rules and laws; hence, it has the full force of the law behind it and must be followed.</p>
<p class='rw-defn idx45' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx46' style='display:none'>A stoic person does not show their emotions and does not complain when bad things happen to them.</p>
<p class='rw-defn idx47' style='display:none'>A tempestuous storm, temper, or crowd is violent, wild, and in an uproar.</p>
<p class='rw-defn idx48' style='display:none'>A tenet is a belief held by a group, organization, or person.</p>
<p class='rw-defn idx49' style='display:none'>A touchstone is the standard or best example by which the value or quality of something else is judged.</p>
<p class='rw-defn idx50' style='display:none'>If something is tranquil, it is peaceful, calm, and quiet.</p>
<p class='rw-defn idx51' style='display:none'>A tumultuous event or period of time is filled with great excitement, confusion, or violence; a tumultuous reaction to something is likewise very loud and noisy because people are happy and excited.</p>
<p class='rw-defn idx52' style='display:none'>When you experience turmoil, there is great confusion, disturbance, instability, and disorder in your life.</p>
<p class='rw-defn idx53' style='display:none'>An unkempt person or thing is untidy and has not been kept neat.</p>
<p class='rw-defn idx54' style='display:none'>A welter of something is a large, overwhelming, and confusing amount of it; this word can also refer to a state of confusion, disorder, or turmoil.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>disarray</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='disarray#' id='pronounce-sound' path='audio/words/amy-disarray'></a>
dis-uh-RAY
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='disarray#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='disarray#' id='context-sound' path='audio/wordcontexts/brian-disarray'></a>
The teenager&#8217;s room was in such <em>disarray</em>&#8212;the mess was unbelievable!  The room&#8217;s <em>disarray</em> or state of disorder was so bad that it was even hard to walk across the floor&#8212;the dresser looked like it had exploded, spraying clothes everywhere.  How anyone can live in such huge <em>disarray</em> or state of disorganization is beyond me&#8212;how can they even find their socks, let alone their homework, computer, or nightstand?
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is <em>disarray</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is bookshelf that needs to be dusted.
</li>
<li class='choice answer '>
<span class='result'></span>
It is a state of confusion and disorderliness.
</li>
<li class='choice '>
<span class='result'></span>
It is a wide variety of something from which to choose.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='disarray#' id='definition-sound' path='audio/wordmeanings/amy-disarray'></a>
When you are in a state of <em>disarray</em>, you are disorganized, disordered, and in a state of confusion.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>disorder</em>
</span>
</span>
</div>
<a class='quick-help' href='disarray#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='disarray#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/disarray/memory_hooks/3385.json'></span>
<p>
<span class="emp0"><span>Hoor<span class="emp3"><span>ay</span></span>!  <span class="emp1"><span>Disor</span></span>de<span class="emp1"><span>r</span></span> D<span class="emp3"><span>ay</span></span>!</span></span> <span class="emp1"><span>Disarr</span></span><span class="emp3"><span>ay</span></span> d<span class="emp3"><span>ay</span></span> is <span class="emp1"><span>disor</span></span>de<span class="emp1"><span>r</span></span> d<span class="emp3"><span>ay</span></span>--hoor<span class="emp3"><span>ay</span></span>!!
</p>
</div>

<div id='memhook-button-bar'>
<a href="disarray#" id="add-public-hook" style="" url="https://membean.com/mywords/disarray/memory_hooks">Use other public hook</a>
<a href="disarray#" id="memhook-use-own" url="https://membean.com/mywords/disarray/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='disarray#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
"The administration’s preparations for avian flu have been in such prolonged <b>disarray</b> that they're releasing their third new plan this week," he said.
<cite class='attribution'>
&mdash;
WebMD
</cite>
</li>
<li>
Dissidents complain that the revision process is in <b>disarray</b> and that the preliminary results, made public for the first time in February 2010, are filled with potential clinical and public relations nightmares.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
Many analysts say the <b>disarray</b> within the Taliban could open up a power struggle, especially between the older, original members and the younger generation.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
China’s health-care system is in <b>disarray</b>, a side effect of the market reforms that have spurred private enterprise and rapid growth since 1980.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/disarray/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='disarray#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='dis_apart' data-tree-url='//cdn1.membean.com/public/data/treexml' href='disarray#'>
<span class='common'></span>
dis-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>apart, not, away from</td>
</tr>
<tr>
<td class='partform'>array</td>
<td>
&rarr;
</td>
<td class='meaning'>arrange</td>
</tr>
</table>
<p>A situation that is in <em>disarray</em> is &#8220;not arranged&#8221; correctly any more.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='disarray#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Sarah Buckwalter: Extreme Office Makeover</strong><span> Her office is in disarray.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/disarray.jpg' video_url='examplevids/disarray' video_width='350'></span>
<div id='wt-container'>
<img alt="Disarray" height="288" src="https://cdn1.membean.com/video/examplevids/disarray.jpg" width="350" />
<div class='center'>
<a href="disarray#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='disarray#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Disarray" src="https://cdn3.membean.com/public/images/wordimages/cons2/disarray.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='disarray#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abandon</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abortive</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>amorphous</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>anarchy</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>arbitrary</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>bedlam</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>boisterous</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>cataclysm</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>chaotic</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>derelict</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>dilapidated</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>discombobulated</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>discordant</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>disquiet</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>dissolution</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>entropy</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>foment</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>fracas</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>fractious</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>havoc</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>inchoate</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>insurrection</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>maelstrom</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>melee</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>pandemonium</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>profligate</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>rampant</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>sedition</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>tempestuous</span> &middot;</li><li data-idx='51' class = 'rw-wordform notlearned'><span>tumultuous</span> &middot;</li><li data-idx='52' class = 'rw-wordform notlearned'><span>turmoil</span> &middot;</li><li data-idx='53' class = 'rw-wordform notlearned'><span>unkempt</span> &middot;</li><li data-idx='54' class = 'rw-wordform notlearned'><span>welter</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>accord</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>cohesive</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>euphony</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>halcyon</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>harmonious</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>imperturbable</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>quiescence</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>rectitude</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>rubric</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>serene</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>statutory</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>stoic</span> &middot;</li><li data-idx='48' class = 'rw-wordform notlearned'><span>tenet</span> &middot;</li><li data-idx='49' class = 'rw-wordform notlearned'><span>touchstone</span> &middot;</li><li data-idx='50' class = 'rw-wordform notlearned'><span>tranquil</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="disarray" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>disarray</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="disarray#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

