
<!DOCTYPE html>
<html>
<head>
<title>Word: unnerve | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Unnerve-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/unnerve-large.jpg?qdep8" />
</div>
<a href="unnerve#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When you abash someone, you make them feel uncomfortable, ashamed, embarrassed, or inferior.</p>
<p class='rw-defn idx1' style='display:none'>If you deal with a difficult situation with aplomb, you deal with it in a confident and skillful way.</p>
<p class='rw-defn idx2' style='display:none'>When you are apprehensive about a future event, you are nervous or fearful about it.</p>
<p class='rw-defn idx3' style='display:none'>An audacious person acts with great daring and sometimes reckless bravery—despite risks and warnings from other people—in order to achieve something.</p>
<p class='rw-defn idx4' style='display:none'>When something bedevils you, it causes you a lot of problems and difficulties over a period of time.</p>
<p class='rw-defn idx5' style='display:none'>If you are beset by something, you are experiencing serious problems or dangers because of it.</p>
<p class='rw-defn idx6' style='display:none'>Complacent persons are too confident and relaxed because they think that they can deal with a situation easily; however, in many circumstances, this is not the case.</p>
<p class='rw-defn idx7' style='display:none'>If something confounds you, it makes you feel surprised and confused, often because it does not meet your predefined expectations.</p>
<p class='rw-defn idx8' style='display:none'>Someone who is craven is very cowardly.</p>
<p class='rw-defn idx9' style='display:none'>If something daunts you, it makes you worried about dealing with it because you think it will be very difficult or dangerous.</p>
<p class='rw-defn idx10' style='display:none'>Someone who is diffident is shy, does not want to draw notice to themselves, and is lacking in self-confidence.</p>
<p class='rw-defn idx11' style='display:none'>When you are discombobulated, you are confused and upset because you have been thrown into a situation that you cannot temporarily handle.</p>
<p class='rw-defn idx12' style='display:none'>If something discomfits you, it makes you feel embarrassed, confused, uncomfortable, or frustrated.</p>
<p class='rw-defn idx13' style='display:none'>If something disconcerts you, it makes you feel anxious, worried, or confused.</p>
<p class='rw-defn idx14' style='display:none'>When someone feels disquiet about a situation, they feel very worried or nervous.</p>
<p class='rw-defn idx15' style='display:none'>If you are distraught about a situation, you are very upset or worried about it.</p>
<p class='rw-defn idx16' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx17' style='display:none'>A harrowing experience is highly distressing, terrifying, or very disturbing.</p>
<p class='rw-defn idx18' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx19' style='display:none'>If someone is imperturbable, they are always calm and not easily upset or disturbed by any situation, even dangerous ones.</p>
<p class='rw-defn idx20' style='display:none'>An intrepid person is willing to do dangerous things or go to dangerous places because they are brave.</p>
<p class='rw-defn idx21' style='display:none'>Someone or something that is invulnerable cannot be harmed in any way; hence, they or it is completely safe or secure.</p>
<p class='rw-defn idx22' style='display:none'>A neurotic person is too anxious or worried about events in everyday life.</p>
<p class='rw-defn idx23' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx24' style='display:none'>If you are in a quandary, you are in a difficult situation in which you have to make a decision but don&#8217;t know what to do.</p>
<p class='rw-defn idx25' style='display:none'>Skittish persons or animals are made easily nervous or alarmed; they are likely to change behavior quickly and unpredictably.</p>
<p class='rw-defn idx26' style='display:none'>To be tentative is to be hesitant or uncertain about something; an agreement or decision of this kind is likely to have changes before it reaches its final form.</p>
<p class='rw-defn idx27' style='display:none'>To be timorous is to be fearful.</p>
<p class='rw-defn idx28' style='display:none'>Someone is tremulous when they are shaking slightly from nervousness or fear; they may also simply be afraid of something.</p>
<p class='rw-defn idx29' style='display:none'>When you are unflappable, you remain calm, cool, and collected in even the most trying of situations.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>unnerve</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='unnerve#' id='pronounce-sound' path='audio/words/amy-unnerve'></a>
uhn-NURV
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='unnerve#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='unnerve#' id='context-sound' path='audio/wordcontexts/brian-unnerve'></a>
I was completely <em>unnerved</em> or frightened when I heard the strange howling in the woods.  I knew that we were in Bigfoot country, and had steeled myself for encountering him, but this actual experience truly <em>unnerved</em> or upset me.  I became so <em>unnerved</em> that I lost my courage, and hightailed it out of those woods back to civilization.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How does someone feel if they have been <em>unnerved</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They feel re-energized and excited.
</li>
<li class='choice answer '>
<span class='result'></span>
They feel confused and uncertain.
</li>
<li class='choice '>
<span class='result'></span>
They feel defeated and resigned.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='unnerve#' id='definition-sound' path='audio/wordmeanings/amy-unnerve'></a>
If something <em>unnerves</em> you, it makes you upset or nervous; it can also make you lose your courage because it frightens you so much.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>upset</em>
</span>
</span>
</div>
<a class='quick-help' href='unnerve#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='unnerve#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/unnerve/memory_hooks/5403.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Un</span></span>do Your <span class="emp2"><span>Nerve</span></span>s</span></span> When something <span class="emp1"><span>un</span></span>does your <span class="emp2"><span>nerve</span></span>s, you lose your courage and become <span class="emp1"><span>un</span></span><span class="emp2"><span>nerve</span></span>d.
</p>
</div>

<div id='memhook-button-bar'>
<a href="unnerve#" id="add-public-hook" style="" url="https://membean.com/mywords/unnerve/memory_hooks">Use other public hook</a>
<a href="unnerve#" id="memhook-use-own" url="https://membean.com/mywords/unnerve/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='unnerve#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Gazing into the bleachers, up at the clouds, winking at friends, he will do anything he can to <b>unnerve</b> the shooter.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Olympic teammate Louie Vito even tried to <b>unnerve</b> him a bit by blasting Miley Cyrus's "Party in the U.S.A." in the van while White was setting his mental stereo to rock & roll.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
<li>
Shirky describes this protest movement in breathless terms: "When teenage girls take to the streets to <b>unnerve</b> national governments, without needing professional organizations or organizers to get the ball rolling, we are in new territory" he writes.
<cite class='attribution'>
&mdash;
The New Atlantis
</cite>
</li>
<li>
Obama said that "any one" of the difficulties facing the country—from the pair of wars to the economic troubles to the Gulf spill—would <b>unnerve</b> a nation.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/unnerve/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='unnerve#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='un_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='unnerve#'>
<span class='common'></span>
un-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not, opposite of</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='nerv_vigor' data-tree-url='//cdn1.membean.com/public/data/treexml' href='unnerve#'>
<span class=''></span>
nerv
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>vigor, determination</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='unnerve#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>To <em>unnerve</em> someone is the &#8220;opposite of&#8221; giving him &#8220;determination.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='unnerve#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Unnerve" src="https://cdn1.membean.com/public/images/wordimages/cons2/unnerve.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='unnerve#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abash</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>apprehensive</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>bedevil</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>beset</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>confound</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>craven</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>daunt</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>diffident</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>discombobulated</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>discomfit</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>disconcert</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>disquiet</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>distraught</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>harrowing</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>neurotic</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>quandary</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>skittish</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>tentative</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>timorous</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>tremulous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>aplomb</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>audacious</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>complacent</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>imperturbable</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>intrepid</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>invulnerable</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>unflappable</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='unnerve#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
nerve
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>courage or control</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="unnerve" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>unnerve</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="unnerve#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

