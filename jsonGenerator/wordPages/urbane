
<!DOCTYPE html>
<html>
<head>
<title>Word: urbane | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you are adept at something, you are very good, skillful, or talented at it.</p>
<p class='rw-defn idx1' style='display:none'>Someone who is adroit is skillful and clever, especially in thought, behavior, or action.</p>
<p class='rw-defn idx2' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx3' style='display:none'>If you are bellicose, you behave in an aggressive way and are likely to start an argument or fight.</p>
<p class='rw-defn idx4' style='display:none'>A bohemian is someone who lives outside mainstream society, often embracing an unconventional lifestyle of self-expression, creativity, and rebellion against conventional rules and ways of society.</p>
<p class='rw-defn idx5' style='display:none'>A cavalier person does not seem to care about rules, principles, or other people&#8217;s feelings, no matter how dire or serious a situation may be.</p>
<p class='rw-defn idx6' style='display:none'>A chivalrous man behaves in a polite, kind, generous, and honorable way, especially towards women.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is churlish is impolite and unfriendly, especially towards another who has done nothing to deserve ill-treatment.</p>
<p class='rw-defn idx8' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx9' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx10' style='display:none'>A cosmopolitan area represents large cultural diversity and people from around the globe; a cosmopolitan person is broad-minded and wise in the ways of the world.</p>
<p class='rw-defn idx11' style='display:none'>A man who is dapper has a very neat and clean appearance; he is both fashionable and stylish.</p>
<p class='rw-defn idx12' style='display:none'>A man who is debonair wears fashionable clothes and is sophisticated, charming, friendly, and confident.</p>
<p class='rw-defn idx13' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx14' style='display:none'>A deft movement or action is made quickly and with skill.</p>
<p class='rw-defn idx15' style='display:none'>Something, such as a building, is derelict if it is empty, not used, and in bad condition or disrepair.</p>
<p class='rw-defn idx16' style='display:none'>If someone is eccentric, they behave in a strange and unusual way that is different from most people.</p>
<p class='rw-defn idx17' style='display:none'>If someone is fractious, they are easily upset or annoyed over unimportant things.</p>
<p class='rw-defn idx18' style='display:none'>A gauche person behaves in an awkward and unsuitable way in public because they lack social skills.</p>
<p class='rw-defn idx19' style='display:none'>A genteel person is well-mannered, has good taste, and is very polite in social situations; a genteel person is often from an upper-class background.</p>
<p class='rw-defn idx20' style='display:none'>If someone behaves in an impertinent way, they behave rudely and disrespectfully.</p>
<p class='rw-defn idx21' style='display:none'>When you act in an imprudent fashion, you do something that is unwise, is lacking in good judgment, or has no forethought.</p>
<p class='rw-defn idx22' style='display:none'>One thing that is incommensurate with another is different in its level, size, or quality from the second; this may lead to an unfair situation.</p>
<p class='rw-defn idx23' style='display:none'>Someone who is morose is unhappy, bad-tempered, and unwilling to talk very much.</p>
<p class='rw-defn idx24' style='display:none'>Someone who is obstreperous is noisy, unruly, and difficult to control.</p>
<p class='rw-defn idx25' style='display:none'>If you do something with panache, you do it in a way that shows great skill, style, and confidence.</p>
<p class='rw-defn idx26' style='display:none'>A Philistine is someone who does not care about and can even speak against the arts; they are considered unintellectual and ignorant—and are proud of it.</p>
<p class='rw-defn idx27' style='display:none'>To be politic in a decision is to be socially wise and diplomatic.</p>
<p class='rw-defn idx28' style='display:none'>Protocol is the rules of conduct or proper behavior that a social situation demands.</p>
<p class='rw-defn idx29' style='display:none'>A provincial outlook is narrow-minded and unsophisticated, limited to the opinions of a relatively local area.</p>
<p class='rw-defn idx30' style='display:none'>Sectarian views tend to be intolerant or unaccepting of points of view that differ from their own.</p>
<p class='rw-defn idx31' style='display:none'>A person who is solicitous behaves in a way that shows great concern about someone&#8217;s health, feelings, safety, etc.</p>
<p class='rw-defn idx32' style='display:none'>If you are suave, you are charming and very polite; you are also agreeable, perhaps not always sincerely, to all you meet.</p>
<p class='rw-defn idx33' style='display:none'>A troglodyte is reclusive, severely lacking in social skills, and is out of step with current times.</p>
<p class='rw-defn idx34' style='display:none'>A tyro has just begun learning something.</p>
<p class='rw-defn idx35' style='display:none'>To act in an uncouth manner is to be awkward and unmannerly.</p>
<p class='rw-defn idx36' style='display:none'>An unkempt person or thing is untidy and has not been kept neat.</p>
<p class='rw-defn idx37' style='display:none'>If someone is unlettered, they are illiterate, unable to read and write.</p>
<p class='rw-defn idx38' style='display:none'>If you say someone&#8217;s behavior is unseemly, you disapprove of it because it is not in good taste or not suitable for a particular situation.</p>
<p class='rw-defn idx39' style='display:none'>A yokel is uneducated, naive, and unsophisticated; they do not know much about modern life or ideas because they sequester themselves in a rural setting.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>urbane</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='urbane#' id='pronounce-sound' path='audio/words/amy-urbane'></a>
ur-BAYN
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='urbane#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='urbane#' id='context-sound' path='audio/wordcontexts/brian-urbane'></a>
Ursula is <em>urbane</em> and charming, in contrast to her brother Ralph, who is unmannerly and unsophisticated.  Ursula&#8217;s <em>urbane</em> and elegant character is probably due to years of living in culturally-rich New York and attending some of the country&#8217;s most elite schools.  Ursula receives frequent invitations to parties because she is known for her <em>urbane</em>, well-bred conversation and wit, but also because she leaves her brother Ralph behind!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How would an <em>urbane</em> person act?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They would be boastful and arrogant.
</li>
<li class='choice answer '>
<span class='result'></span>
They would be well-mannered and gracious.
</li>
<li class='choice '>
<span class='result'></span>
They would be loud and obnoxious.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='urbane#' id='definition-sound' path='audio/wordmeanings/amy-urbane'></a>
If you behave in an <em>urbane</em> way, you are behaving in a polite, refined, and civilized fashion in social situations.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>pleasantly sophisticated</em>
</span>
</span>
</div>
<a class='quick-help' href='urbane#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='urbane#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/urbane/memory_hooks/5407.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Urban</span></span> Sophistication</span></span> <span class="emp3"><span>Urban</span></span> locations are often the centers of civilization, so we could say that <span class="emp3"><span>urban</span></span> centers are most <span class="emp3"><span>urban</span></span>e.
</p>
</div>

<div id='memhook-button-bar'>
<a href="urbane#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/urbane/memory_hooks">Use other hook</a>
<a href="urbane#" id="memhook-use-own" url="https://membean.com/mywords/urbane/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='urbane#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
In a policy shift which the historian Guy de la Bédoyère has compared with Western Imperialism, the Romans converted militant Britons to their way of life with consumer enticements, introducing them to the <b>urbane</b> pleasures of hot spas and fine dining, encouraging them to wear togas and speak Latin.
<span class='attribution'>&mdash; Catharine Arnold, British author and journalist, from _Necropolis: London and Its Dead_</span>
<img alt="Catharine arnold, british author and journalist, from _necropolis: london and its dead_" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Catharine Arnold, British author and journalist, from _Necropolis: London and Its Dead_.jpg?qdep8" width="80" />
</li>
<li>
The charismatic, silver-maned editor, possessed of a keen wit and a sly delivery, would have been a worthy subject of his own publication had he not led it. Carter embodies the <b>urbane</b> style and sensibility reflected in _Vanity Fair’s_ pages, at once engaged and detached, immersed in observing the world swirling around him.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Mattis has long been a living legend in the Marine Corps, earning the odd nickname of “the Warrior Monk.” Robert H. Scales, a retired Army major general, described him as “one of the most <b>urbane</b> and polished men I have known.”
<cite class='attribution'>
&mdash;
Sarasota Herald-Tribune
</cite>
</li>
<li>
James Dyson is disarmingly polite and <b>urbane</b>. This is my abiding memory of first meeting him as a business journalist.
<cite class='attribution'>
&mdash;
The London Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/urbane/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='urbane#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='urb_city' data-tree-url='//cdn1.membean.com/public/data/treexml' href='urbane#'>
<span class=''></span>
urb
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>city</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ane_pertaining' data-tree-url='//cdn1.membean.com/public/data/treexml' href='urbane#'>
<span class=''></span>
-ane
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or pertaining to</td>
</tr>
</table>
<p>If one is <em>urbane</em>, one is of the city, that is, one knows how to get along with many different people, and is sophisticated because of one&#8217;s broader experiences and exposure to culture while dwelling in a &#8220;city.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='urbane#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Star of Midnight</strong><span> Quite the urbane fellow.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/urbane.jpg' video_url='examplevids/urbane' video_width='350'></span>
<div id='wt-container'>
<img alt="Urbane" height="288" src="https://cdn1.membean.com/video/examplevids/urbane.jpg" width="350" />
<div class='center'>
<a href="urbane#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='urbane#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Urbane" src="https://cdn1.membean.com/public/images/wordimages/cons2/urbane.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='urbane#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adept</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>adroit</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>chivalrous</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>cosmopolitan</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>dapper</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>debonair</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>deft</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>genteel</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>panache</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>politic</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>protocol</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>suave</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>bellicose</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>bohemian</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>cavalier</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>churlish</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>derelict</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>eccentric</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>fractious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>gauche</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>impertinent</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>imprudent</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>incommensurate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>morose</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>obstreperous</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>philistine</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>provincial</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>sectarian</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>solicitous</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>troglodyte</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>tyro</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>uncouth</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>unkempt</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>unlettered</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>unseemly</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>yokel</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="urbane" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>urbane</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="urbane#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

