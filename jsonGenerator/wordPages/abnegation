
<!DOCTYPE html>
<html>
<head>
<title>Word: abnegation | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If someone abdicates, they give up their responsibility for something, such as a king&#8217;s transfer of power when he gives up his throne.</p>
<p class='rw-defn idx1' style='display:none'>If you abjure a belief or a way of behaving, you state publicly that you will give it up or reject it.</p>
<p class='rw-defn idx2' style='display:none'>If someone in authority abolishes a law or practice, they formally put an end to it.</p>
<p class='rw-defn idx3' style='display:none'>Someone who is abstemious avoids doing too much of something enjoyable, such as eating or drinking; rather, they consume in a moderate fashion.</p>
<p class='rw-defn idx4' style='display:none'>Abstinence is the practice of keeping away from or avoiding something you enjoy—such as the physical pleasures of excessive food and drink—usually for health or religious reasons.</p>
<p class='rw-defn idx5' style='display:none'>When you accede to a demand or proposal, you agree to it, especially after first disagreeing with it.</p>
<p class='rw-defn idx6' style='display:none'>If you acquiesce to something, you allow it to happen by doing what someone wants without putting up a struggle or voicing your own concerns.</p>
<p class='rw-defn idx7' style='display:none'>When you advocate a plan or action, you publicly push for implementing it.</p>
<p class='rw-defn idx8' style='display:none'>When you arrogate something, such as a position or privilege, you take it even though you don&#8217;t have the legal right to it.</p>
<p class='rw-defn idx9' style='display:none'>When you assimilate an idea, you completely understand it; likewise, when someone is assimilated into a new place, they become part of it by understanding what it is all about and by being accepted by others who live there.</p>
<p class='rw-defn idx10' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx11' style='display:none'>Something austere is simple and plain in its style; an austere person is strict and severe with themselves.</p>
<p class='rw-defn idx12' style='display:none'>When you beget something, you cause it to happen or create it.</p>
<p class='rw-defn idx13' style='display:none'>Someone cedes land or power to someone else by giving it to them, often because of political or military pressure.</p>
<p class='rw-defn idx14' style='display:none'>If someone will countenance something, they will approve, tolerate, or support it.</p>
<p class='rw-defn idx15' style='display:none'>If you curtail something, you reduce or limit it.</p>
<p class='rw-defn idx16' style='display:none'>If something engenders a particular feeling or attitude, it causes that condition.</p>
<p class='rw-defn idx17' style='display:none'>If you expropriate something, you take it away for your own use although it does not belong to you; governments frequently expropriate private land to use for public purposes.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is frugal spends very little money—and even then only on things that are absolutely necessary.</p>
<p class='rw-defn idx19' style='display:none'>If you gainsay something, you say that it is not true and therefore reject it.</p>
<p class='rw-defn idx20' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx21' style='display:none'>An irrefutable argument or statement cannot be proven wrong; therefore, it must be accepted because it is certain.</p>
<p class='rw-defn idx22' style='display:none'>An irrevocable action or decision is impossible to change, reverse, or stop.</p>
<p class='rw-defn idx23' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx24' style='display:none'>To nullify something is to cancel or negate it.</p>
<p class='rw-defn idx25' style='display:none'>If you suffer privation, you live without many of the basic things required for a comfortable life.</p>
<p class='rw-defn idx26' style='display:none'>The ratification of a measure or agreement is its official approval or confirmation by all involved.</p>
<p class='rw-defn idx27' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx28' style='display:none'>If you recant, you publicly announce that your once firmly held beliefs or statements were wrong and that you no longer agree with them.</p>
<p class='rw-defn idx29' style='display:none'>When you relinquish something, you give it up or let it go.</p>
<p class='rw-defn idx30' style='display:none'>If you renege on a deal, agreement, or promise, you do not do what you promised or agreed to do.</p>
<p class='rw-defn idx31' style='display:none'>To renounce something, such as a position or practice, is to let go of it or reject it.</p>
<p class='rw-defn idx32' style='display:none'>If you repudiate something, you state that you do not accept or agree with it and no longer want to be connected with it in any way.</p>
<p class='rw-defn idx33' style='display:none'>When someone in power rescinds a law or agreement, they officially end it and state that it is no longer valid.</p>
<p class='rw-defn idx34' style='display:none'>If governments, companies, or other institutions retrench, they reduce costs and/or decrease the amount that they spend in order to save money.</p>
<p class='rw-defn idx35' style='display:none'>If you truncate something, you make it shorter or quicker by removing or cutting off part of it.</p>
<p class='rw-defn idx36' style='display:none'>When you usurp someone else&#8217;s power, position, or role, you take it from them although you do not have the right to do so.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>abnegation</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='abnegation#' id='pronounce-sound' path='audio/words/amy-abnegation'></a>
ab-ni-GAY-shuhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='abnegation#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='abnegation#' id='context-sound' path='audio/wordcontexts/brian-abnegation'></a>
When Abigail broke up with her boyfriend, her rejection or <em>abnegation</em> of their relationship hurt them both.  Giving up or abnegating the bond between them would be best over time, since she was traveling abroad for a year alone.  Abigail found that the sights and sounds of travel eased the painful <em>abnegation</em> of setting aside her love life.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When would someone engage in <em>abnegation</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
When they felt it necessary to deny themselves of something.
</li>
<li class='choice '>
<span class='result'></span>
When they wanted to adopt a healthier attitude to their life.
</li>
<li class='choice '>
<span class='result'></span>
When they wished to avoid making the same mistake twice.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='abnegation#' id='definition-sound' path='audio/wordmeanings/amy-abnegation'></a>
The <em>abnegation</em> of something is someone&#8217;s giving up their rights or claim to it or denying themselves of it; this action may or may not be in their best interest.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>deny oneself of</em>
</span>
</span>
</div>
<a class='quick-help' href='abnegation#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='abnegation#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/abnegation/memory_hooks/6164.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Ab</span></span> <span class="emp2"><span>Negation</span></span></span></span> <span class="emp1"><span>Ab</span></span><span class="emp2"><span>negation</span></span> of junk food results in <span class="emp1"><span>ab</span></span>domen <span class="emp2"><span>negation</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="abnegation#" id="add-public-hook" style="" url="https://membean.com/mywords/abnegation/memory_hooks">Use other public hook</a>
<a href="abnegation#" id="memhook-use-own" url="https://membean.com/mywords/abnegation/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='abnegation#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
"Red Dust" enjoys yet another distinction: Hollywood movies of the period are awash in self-sacrifice, but this is one of the few in which the <b>abnegation</b> doesn’t seem maudlin or contrived.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
The officials had arranged the draw with proper British self-<b>abnegation</b>, contriving to make it as difficult as possible for a British player to reach the finals.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Socrates, of course, stands for the principle of self-<b>abnegation</b>, of the wise suggestion to concentrate on the journey itself, not the goal.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/abnegation/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='abnegation#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ab_away' data-tree-url='//cdn1.membean.com/public/data/treexml' href='abnegation#'>
<span class='common'></span>
ab-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>away, from</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='neg_deny' data-tree-url='//cdn1.membean.com/public/data/treexml' href='abnegation#'>
<span class=''></span>
neg
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>deny, say not</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ion_act' data-tree-url='//cdn1.membean.com/public/data/treexml' href='abnegation#'>
<span class=''></span>
-ion
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>act, state, or result of doing something</td>
</tr>
</table>
<p><em>Abnegation</em> is the &#8220;state or condition&#8221; of having &#8220;denied or said no&#8221; to something, thus taking it &#8220;away or from&#8221; you.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='abnegation#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Beverly Hills 90210</strong><span> Adrianna abnegating her child to adopting parents was the hardest thing she ever did.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/abnegation.jpg' video_url='examplevids/abnegation' video_width='350'></span>
<div id='wt-container'>
<img alt="Abnegation" height="198" src="https://cdn1.membean.com/video/examplevids/abnegation.jpg" width="350" />
<div class='center'>
<a href="abnegation#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='abnegation#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Abnegation" src="https://cdn2.membean.com/public/images/wordimages/cons2/abnegation.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='abnegation#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abdicate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abjure</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abolish</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>abstemious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>abstinence</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>austere</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>cede</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>curtail</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>frugal</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>gainsay</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>nullify</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>privation</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>recant</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>relinquish</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>renege</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>renounce</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>repudiate</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>rescind</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>truncate</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='5' class = 'rw-wordform notlearned'><span>accede</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>acquiesce</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>advocate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>arrogate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>assimilate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>beget</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>countenance</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>engender</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>expropriate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>irrefutable</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>irrevocable</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>ratification</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>retrench</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>usurp</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='abnegation#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
abnegate
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>give up</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="abnegation" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>abnegation</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="abnegation#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

