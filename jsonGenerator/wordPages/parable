
<!DOCTYPE html>
<html>
<head>
<title>Word: parable | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Parable-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/parable-large.jpg?qdep8" />
</div>
<a href="parable#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>An adage is an inherited saying or phrase that has been historically used to express a common experience.</p>
<p class='rw-defn idx1' style='display:none'>An allegorical poem or story employs allegory, that is, a literary device that uses literal events and characters to represent abstract ideas or deeper meanings.</p>
<p class='rw-defn idx2' style='display:none'>When you allude to something or someone, often events or characters from literature or history, you refer to them in an indirect way.</p>
<p class='rw-defn idx3' style='display:none'>An aphorism is a short, witty statement that contains a wise idea.</p>
<p class='rw-defn idx4' style='display:none'>An axiom is a wise saying or widely recognized general truth that is often self-evident; it can also be an established rule or law.</p>
<p class='rw-defn idx5' style='display:none'>A dictum is a saying that people often repeat because it says something interesting or wise about a subject.</p>
<p class='rw-defn idx6' style='display:none'>Didactic speech or writing is intended to teach something, especially a moral lesson.</p>
<p class='rw-defn idx7' style='display:none'>If something is done for someone&#8217;s edification, it is done to benefit that person by teaching them something that improves or enlightens their knowledge or character.</p>
<p class='rw-defn idx8' style='display:none'>Someone who is erudite is steeped in knowledge because they have read and studied extensively.</p>
<p class='rw-defn idx9' style='display:none'>If you use words in a figurative way, they have an abstract or symbolic meaning beyond their literal interpretation.</p>
<p class='rw-defn idx10' style='display:none'>A genre is a category, type, or class of artistic work.</p>
<p class='rw-defn idx11' style='display:none'>Hackneyed words, images, ideas or sayings have been used so often that they no longer seem interesting or amusing.</p>
<p class='rw-defn idx12' style='display:none'>Inconsequential matters are unimportant or are of little to no significance.</p>
<p class='rw-defn idx13' style='display:none'>A maxim is a recognized rule of conduct or a general statement of a truth or principle.</p>
<p class='rw-defn idx14' style='display:none'>A metaphor is a word or phrase that means one thing but is used to describe something else in order to highlight similar qualities; for example, if someone is very brave, you might say that they have the heart of a lion.</p>
<p class='rw-defn idx15' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx16' style='display:none'>The art of pedagogy is the methods used by a teacher to teach a subject.</p>
<p class='rw-defn idx17' style='display:none'>A platitude is an unoriginal statement that has been used so many times that it is considered almost meaningless and pointless, even though it is presented as important.</p>
<p class='rw-defn idx18' style='display:none'>When someone exhibits profundity, they display great intellectual depth and understanding; profundity can also be the depth or complexity of something.</p>
<p class='rw-defn idx19' style='display:none'>Something prosaic is dull, boring, and ordinary.</p>
<p class='rw-defn idx20' style='display:none'>A raconteur is a person who tells stories with great skill.</p>
<p class='rw-defn idx21' style='display:none'>A sagacious person is wise, intelligent, and has the ability to make good practical decisions.</p>
<p class='rw-defn idx22' style='display:none'>If you are sapient, you are wise or very learned.</p>
<p class='rw-defn idx23' style='display:none'>If someone is sententious, they are terse or brief in writing and speech; they also often use proverbs to appear morally upright and wise.</p>
<p class='rw-defn idx24' style='display:none'>If you refer to something as a trifle, you mean that it is of little or no importance or value.</p>
<p class='rw-defn idx25' style='display:none'>A trite remark or idea is so overused that it is no longer interesting or novel.</p>
<p class='rw-defn idx26' style='display:none'>To be under someone&#8217;s tutelage is to be under their guidance and teaching.</p>
<p class='rw-defn idx27' style='display:none'>If someone is unlettered, they are illiterate, unable to read and write.</p>
<p class='rw-defn idx28' style='display:none'>Something vapid is dull, boring, and/or tiresome.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>parable</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='parable#' id='pronounce-sound' path='audio/words/amy-parable'></a>
PAR-uh-buhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='parable#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='parable#' id='context-sound' path='audio/wordcontexts/brian-parable'></a>
My grandfather tells me <em>parables</em> or tales that teach me important lessons&#8212;usually to illustrate a rule of conduct. His <em>parables</em> or short stories with a moral inform me how to act, such as treating everyone with kindness. The <em>parables</em> are mostly equivalent to my grandmother&#8217;s fables, both of which have led me to surprising pieces of wisdom about life and different ways of looking at the world. I always remember his <em>parables</em> or instructive lessons because his simple stories contain meaningful and powerful messages.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>parable</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A long story that explains how something began.
</li>
<li class='choice answer '>
<span class='result'></span>
A simple story about the importance of telling the truth.
</li>
<li class='choice '>
<span class='result'></span>
A poem that celebrates someone&#8217;s heroic actions.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='parable#' id='definition-sound' path='audio/wordmeanings/amy-parable'></a>
A <em>parable</em> is a short story that usually is told to illustrate a moral or lesson of some kind.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>moral tale</em>
</span>
</span>
</div>
<a class='quick-help' href='parable#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='parable#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/parable/memory_hooks/2835.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Par</span></span>t F<span class="emp3"><span>able</span></span></span></span> A <span class="emp1"><span>par</span></span><span class="emp3"><span>able</span></span> is <span class="emp1"><span>par</span></span>t f<span class="emp3"><span>able</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="parable#" id="add-public-hook" style="" url="https://membean.com/mywords/parable/memory_hooks">Use other public hook</a>
<a href="parable#" id="memhook-use-own" url="https://membean.com/mywords/parable/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='parable#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Readers who loved the moral ambiguity, crisp writing and ruthless pacing of the first three books might be less interested in an overworked <b>parable</b> about the value of Enlightenment thinking.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Bastiat’s essay is most famous for the “<b>parable</b> of the broken window,” in which a young boy shatters a shopkeeper’s window and, after some initial outrage, the villagers conclude that the rascal helped the local economy.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
And that is what set into motion what we're calling the <b>parable</b> of the piston.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
The Guardian found it a “strangely mismanaged <b>parable</b> about property management that renders its stars all but unrecognizable”.
<cite class='attribution'>
&mdash;
BBC News
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/parable/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='parable#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='para_beside' data-tree-url='//cdn1.membean.com/public/data/treexml' href='parable#'>
<span class='common'></span>
para-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>beside, alongside</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='bol_throw' data-tree-url='//cdn1.membean.com/public/data/treexml' href='parable#'>
<span class=''></span>
bol
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>throw</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='parable#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>The word <em>parable</em> is ultimately from a root word meaning &#8220;speech, proverb, comparison;&#8221; a <em>parable</em>, in the form of a story, imparts wisdom like a &#8220;proverb,&#8221; often with illuminating &#8220;comparisons&#8221; that are made when different concepts are &#8220;thrown beside&#8221; each other.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='parable#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Parables -- Chuck Knows Church</strong><span> What parables are all about.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/parable.jpg' video_url='examplevids/parable' video_width='350'></span>
<div id='wt-container'>
<img alt="Parable" height="288" src="https://cdn1.membean.com/video/examplevids/parable.jpg" width="350" />
<div class='center'>
<a href="parable#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='parable#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Parable" src="https://cdn3.membean.com/public/images/wordimages/cons2/parable.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='parable#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adage</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>allegorical</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>allude</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>aphorism</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>axiom</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>dictum</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>didactic</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>edification</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>erudite</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>figurative</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>genre</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>maxim</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>metaphor</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>pedagogy</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>profundity</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>raconteur</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>sagacious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>sapient</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>sententious</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>tutelage</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='11' class = 'rw-wordform notlearned'><span>hackneyed</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>inconsequential</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>platitude</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>prosaic</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>trifle</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>trite</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>unlettered</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>vapid</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="parable" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>parable</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="parable#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

