
<!DOCTYPE html>
<html>
<head>
<title>Word: antagonist | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Antagonist-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/antagonist-large.jpg?qdep8" />
</div>
<a href="antagonist#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>If you acquiesce to something, you allow it to happen by doing what someone wants without putting up a struggle or voicing your own concerns.</p>
<p class='rw-defn idx1' style='display:none'>An adversary is a person who goes against you in some way, such as an opponent in a contest or a personal enemy in a conflict.</p>
<p class='rw-defn idx2' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx3' style='display:none'>Amity is a peaceful friendship between two parties, particularly between two countries.</p>
<p class='rw-defn idx4' style='display:none'>If you have animus against someone, you have a strong feeling of dislike or hatred towards them, often without a good reason or based upon your personal prejudices.</p>
<p class='rw-defn idx5' style='display:none'>Antipathy is a strong feeling of dislike or hostility towards someone or something.</p>
<p class='rw-defn idx6' style='display:none'>If you are bellicose, you behave in an aggressive way and are likely to start an argument or fight.</p>
<p class='rw-defn idx7' style='display:none'>A belligerent person or country is aggressive, very unfriendly, and likely to start a fight.</p>
<p class='rw-defn idx8' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx9' style='display:none'>If someone is granted clemency, they are punished less severely than they could have been—they have been shown mercy.</p>
<p class='rw-defn idx10' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx11' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx12' style='display:none'>When someone is contentious, they are argumentative and likely to provoke a fight.</p>
<p class='rw-defn idx13' style='display:none'>To contravene a law, rule, or agreement is to do something that is not allowed or is forbidden by that law, rule, or agreement.</p>
<p class='rw-defn idx14' style='display:none'>A situation or thing that is discordant does not fit in with other things; therefore, it is disagreeable, strange, or unpleasant.</p>
<p class='rw-defn idx15' style='display:none'>Dissension is a disagreement or difference of opinion among a group of people that can cause conflict.</p>
<p class='rw-defn idx16' style='display:none'>Empathy is the ability to understand how people feel because you can imagine what it is to be like them.</p>
<p class='rw-defn idx17' style='display:none'>If something exacerbates a problem or bad situation, it makes it even worse.</p>
<p class='rw-defn idx18' style='display:none'>A small group or issue that is factious is controversial and promotes strong disagreement or dissatisfaction within a larger group.</p>
<p class='rw-defn idx19' style='display:none'>When two people are in a harmonious state, they are in agreement with each other; when a sound is harmonious, it is pleasant or agreeable to the ear.</p>
<p class='rw-defn idx20' style='display:none'>If two people are incompatible, they do not get along, tend to disagree, and are unable to cooperate with one another.</p>
<p class='rw-defn idx21' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx22' style='display:none'>Malice is a type of hatred that causes a strong desire to harm others both physically and emotionally.</p>
<p class='rw-defn idx23' style='display:none'>If you have a nemesis, it is an opponent or rival whom you cannot beat or is a source of ruin that causes your downfall.</p>
<p class='rw-defn idx24' style='display:none'>Something that is propitious shows favorable conditions.</p>
<p class='rw-defn idx25' style='display:none'>A protagonist is the main character in a play, novel, or real event such as a battle or struggle.</p>
<p class='rw-defn idx26' style='display:none'>Rapprochement is the development of greater understanding and friendliness between two countries or groups of people after a period of unfriendly relations.</p>
<p class='rw-defn idx27' style='display:none'>A truculent person is bad-tempered, easily annoyed, and prone to arguing excessively.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>antagonist</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='antagonist#' id='pronounce-sound' path='audio/words/amy-antagonist'></a>
an-TAG-uh-nist
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='antagonist#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='antagonist#' id='context-sound' path='audio/wordcontexts/brian-antagonist'></a>
Sebastian and Tom had been <em>antagonists</em> for as long as they could remember, competing against each other for awards and honors at school.  When Sebastian won the title of captain on the wrestling team, his <em>antagonist</em> Tom, not to be outdone, followed by being elected class president.  Soon the boys&#8217; ongoing <em><em>antagonism</em></em> affected their class in a negative way, and the principal had to speak to them about how such heated hostility was ruining the spirit of learning and hard work.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does an <em>antagonist</em> do?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They make convincing and logical arguments.
</li>
<li class='choice '>
<span class='result'></span>
They challenge you to try harder and do better.
</li>
<li class='choice answer '>
<span class='result'></span>
They compete against you in some way.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='antagonist#' id='definition-sound' path='audio/wordmeanings/amy-antagonist'></a>
Your <em>antagonist</em> is an opponent in a competition or battle.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>opponent</em>
</span>
</span>
</div>
<a class='quick-help' href='antagonist#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='antagonist#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/antagonist/memory_hooks/6045.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>A</span></span>u<span class="emp2"><span>nt</span></span> <span class="emp1"><span>Zing</span></span>s Red Hot Pokers</span></span>  My <span class="emp2"><span>A</span></span>u<span class="emp2"><span>nt</span></span> <span class="emp3"><span>Gon</span></span>eril caused me great a<span class="emp3"><span>gon</span></span>y by <span class="emp2"><span>ant</span></span><span class="emp3"><span>agon</span></span>i<span class="emp1"><span>zing</span></span> me with <span class="emp1"><span>zing</span></span>ing red hot pokers--ouch!
</p>
</div>

<div id='memhook-button-bar'>
<a href="antagonist#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/antagonist/memory_hooks">Use other hook</a>
<a href="antagonist#" id="memhook-use-own" url="https://membean.com/mywords/antagonist/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='antagonist#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Amidst trolls, orcs and sundry other dangers, one memorable <b>antagonist</b> stands out in _The Hobbit:_ the giant, dwarf-eating, hobbit-scaring, treasure-hoarding dragon Smaug.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
“Grounded” is stressful in ways similar to most survival games. . . . Your foes are all gargantuan insects, and spiders are a frequent <b>antagonist—among</b> the toughest to overcome.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
And the familiar old rivalries endure as well. The big prerace shuck last week was the putative Petty-Allison feud, a latter-day confrontation as intense as that of the Hatfields and McCoys if one was to believe the <b>antagonists</b> and the papers.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
He who wrestles with us strengthens our nerves and sharpens our skill. Our <b>antagonist</b> is our helper.
<cite class='attribution'>
&mdash;
Edmund Burke, Irish philosopher and politician, from _Reflections on the French Revolution_
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/antagonist/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='antagonist#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ant_opposite' data-tree-url='//cdn1.membean.com/public/data/treexml' href='antagonist#'>
<span class=''></span>
ant-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>opposite, against</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='agon_struggle' data-tree-url='//cdn1.membean.com/public/data/treexml' href='antagonist#'>
<span class=''></span>
agon
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>struggle, contest, conflict</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ist_performer' data-tree-url='//cdn1.membean.com/public/data/treexml' href='antagonist#'>
<span class=''></span>
-ist
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>one who performs a certain action</td>
</tr>
</table>
<p>An <em>antagonist</em> is a person who is in a &#8220;contest, struggle, or conflict against or opposite&#8221; you.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='antagonist#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Chungdahm Knowledge Design Media</strong><span> What an antagonist is.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/antagonist.jpg' video_url='examplevids/antagonist' video_width='350'></span>
<div id='wt-container'>
<img alt="Antagonist" height="288" src="https://cdn1.membean.com/video/examplevids/antagonist.jpg" width="350" />
<div class='center'>
<a href="antagonist#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='antagonist#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Antagonist" src="https://cdn3.membean.com/public/images/wordimages/cons2/antagonist.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='antagonist#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>adversary</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>animus</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>antipathy</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>bellicose</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>belligerent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>contentious</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>contravene</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>discordant</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>dissension</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>exacerbate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>factious</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>incompatible</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>malice</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>nemesis</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>truculent</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>acquiesce</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>amity</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>clemency</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>empathy</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>harmonious</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>propitious</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>protagonist</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>rapprochement</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='antagonist#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
antagonism
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>hostility</td>
</tr>
<tr>
<td class='wordform'>
<span>
antagonize
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>provoke hostility</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="antagonist" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>antagonist</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="antagonist#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

