
<!DOCTYPE html>
<html>
<head>
<title>Word: incumbent | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An affliction is something that causes pain and mental suffering, especially a medical condition.</p>
<p class='rw-defn idx1' style='display:none'>If you alleviate pain or suffering, you make it less intense or severe.</p>
<p class='rw-defn idx2' style='display:none'>When you ameliorate a bad condition or situation, you make it better in some way.</p>
<p class='rw-defn idx3' style='display:none'>Something that is arduous is extremely difficult; hence, it involves a lot of effort.</p>
<p class='rw-defn idx4' style='display:none'>If you circumvent something, such as a rule or restriction, you try to get around it in a clever and perhaps dishonest way.</p>
<p class='rw-defn idx5' style='display:none'>If a fact or idea eludes you, you cannot remember or understand it; if you elude someone, you manage to escape or hide from them.</p>
<p class='rw-defn idx6' style='display:none'>If something encumbers you, it makes it difficult for you to move freely or do what you want.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is exacting expects others to work very hard and carefully.</p>
<p class='rw-defn idx8' style='display:none'>An exigent situation is urgent and demands a lot of attention, often in ways that are expensive in time, cost, and effort.</p>
<p class='rw-defn idx9' style='display:none'>When you extricate yourself from a difficult or unpleasant situation, you manage to escape it.</p>
<p class='rw-defn idx10' style='display:none'>If something fetters you, it restricts you from doing something you want to do.</p>
<p class='rw-defn idx11' style='display:none'>If something is formidable, it is impressive in size, power, or skill; therefore, it can make you feel frightened, alarmed, or in awe because it is so powerful or difficult to deal with.</p>
<p class='rw-defn idx12' style='display:none'>A laborious job or process takes a long time, requires a lot of effort, and is often boring.</p>
<p class='rw-defn idx13' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx14' style='display:none'>If you mollify someone, you say or do something to make that person feel less angry or upset.</p>
<p class='rw-defn idx15' style='display:none'>If you nettle someone, you irritate or annoy them.</p>
<p class='rw-defn idx16' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx17' style='display:none'>To obviate a problem is to eliminate it or do something that makes solving the problem unnecessary.</p>
<p class='rw-defn idx18' style='display:none'>A task or responsibility is onerous if you dislike having to do it because it is difficult or tiring.</p>
<p class='rw-defn idx19' style='display:none'>Ponderous writing or speech is boring, highly serious, and seems very long and wordy; it definitely lacks both grace and style.</p>
<p class='rw-defn idx20' style='display:none'>When you trammel something or someone, you bring about limits to freedom or hinder movements or activities in some fashion.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>incumbent</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='incumbent#' id='pronounce-sound' path='audio/words/amy-incumbent'></a>
in-KUHM-buhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='incumbent#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='incumbent#' id='context-sound' path='audio/wordcontexts/brian-incumbent'></a>
As my father&#8217;s only surviving child, it is <em>incumbent</em> upon me to deal with the responsibility of his large cattle ranch.  I live in a separate country, yet I flew to Montana to fulfill my <em>incumbent</em> or binding responsibilities.  Even though I was grieving my father&#8217;s death, the necessary, <em>incumbent</em> task of sorting through his papers and property kept me busy.  It was <em>incumbent</em> upon me to be the executor of his will; this official role was helpful in keeping me going during this sad time.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If a task is <em>incumbent</em> on you, what should you do?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
You should ask for help because the task is too large for you to do alone.
</li>
<li class='choice answer '>
<span class='result'></span>
You should complete the task because it is your obligation to do so.
</li>
<li class='choice '>
<span class='result'></span>
You should volunteer to do the task because it&#8217;s something you enjoy doing.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='incumbent#' id='definition-sound' path='audio/wordmeanings/amy-incumbent'></a>
If it is <em>incumbent</em> upon you to do something, it is your duty or responsibility to do it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>necessary</em>
</span>
</span>
</div>
<a class='quick-help' href='incumbent#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='incumbent#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/incumbent/memory_hooks/4709.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>In</span></span>jury! D<span class="emp1"><span>ent</span></span>ed Cu<span class="emp2"><span>cumb</span></span>er!</span></span> Since I d<span class="emp1"><span>ent</span></span>ed the cu<span class="emp2"><span>cumb</span></span>er at the party, it was <span class="emp3"><span>in</span></span><span class="emp2"><span>cumb</span></span><span class="emp1"><span>ent</span></span> upon me to tell the hostess about the veggie <span class="emp3"><span>in</span></span>jury; I also felt it <span class="emp3"><span>in</span></span><span class="emp2"><span>cumb</span></span><span class="emp1"><span>ent</span></span> upon me to perform 20 hours of community service for d<span class="emp1"><span>ent</span></span>ing the cu<span class="emp2"><span>cumb</span></span>er, as well as giving the hostess a new pearl necklace in exchange for the <span class="emp3"><span>in</span></span>jured and d<span class="emp1"><span>ent</span></span>ed cu<span class="emp2"><span>cumb</span></span>er.
</p>
</div>

<div id='memhook-button-bar'>
<a href="incumbent#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/incumbent/memory_hooks">Use other hook</a>
<a href="incumbent#" id="memhook-use-own" url="https://membean.com/mywords/incumbent/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='incumbent#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
I’ve said it before and I’ll say it again—it is <b>incumbent</b> upon every elected leader at every level of government to tone down our rhetoric. Furthermore, we must try as much as we can to find common ground and work together.
<cite class='attribution'>
&mdash;
The Bloomington Pantagraph
</cite>
</li>
<li>
There’s no doubt Maryland businesses must adapt to the online shopping culture. “It’s here to stay; it’s not going away,” Lockair-Toll said. “So it’s <b>incumbent</b> upon business owners if they want to stay competitive to create those experiences and use online retail to grow their business.”
<cite class='attribution'>
&mdash;
CBS Baltimore
</cite>
</li>
<li>
The proposal requires state agencies to follow security guidelines set forth by the OCS and report cyber security incidents to the office within 24 hours. The office would then investigate attacks and coordinate related communications. . . . “I’m not usually one to grow government, but I think in this instance, it’s <b>incumbent</b> upon us to make sure that we can protect this information,” said Sen. Shelly Short, R-Addy, during an executive session on the bill.
<cite class='attribution'>
&mdash;
Sequim Gazette
</cite>
</li>
<li>
It is <b>incumbent</b> on you, reader, to commit to anti-racism, in yourself, your work, your daily practice. The policies and reforms that are required for transformative change need a critical mass of support.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/incumbent/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='incumbent#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='in_in' data-tree-url='//cdn1.membean.com/public/data/treexml' href='incumbent#'>
<span class='common'></span>
in-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>in, on</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='cumb_lie' data-tree-url='//cdn1.membean.com/public/data/treexml' href='incumbent#'>
<span class=''></span>
cumb
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>lie down</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ent_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='incumbent#'>
<span class=''></span>
-ent
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being in a state or condition</td>
</tr>
</table>
<p>An <em>incumbent</em> task refers to a duty or obligation that &#8220;rests upon&#8221; or &#8220;lies down&#8221; upon one&#8217;s shoulders.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='incumbent#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: The Washington Examiner </strong><span> The incumbent advantage is huge in a state like Virginia.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/incumbent.jpg' video_url='examplevids/incumbent' video_width='350'></span>
<div id='wt-container'>
<img alt="Incumbent" height="288" src="https://cdn1.membean.com/video/examplevids/incumbent.jpg" width="350" />
<div class='center'>
<a href="incumbent#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='incumbent#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Incumbent" src="https://cdn0.membean.com/public/images/wordimages/cons2/incumbent.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='incumbent#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>affliction</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>arduous</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>encumber</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>exacting</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>exigent</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>fetter</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>formidable</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>laborious</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>nettle</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>onerous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>ponderous</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>trammel</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>alleviate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>ameliorate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>circumvent</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>elude</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>extricate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>mollify</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>obviate</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='incumbent#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
incumbent
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>a person who currently holds a political office</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="incumbent" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>incumbent</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="incumbent#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

