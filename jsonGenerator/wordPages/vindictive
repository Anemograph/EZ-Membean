
<!DOCTYPE html>
<html>
<head>
<title>Word: vindictive | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you absolve someone, you publicly and formally say that they are not guilty or responsible for any wrongdoing.</p>
<p class='rw-defn idx1' style='display:none'>An affliction is something that causes pain and mental suffering, especially a medical condition.</p>
<p class='rw-defn idx2' style='display:none'>If one thing is analogous to another, a comparison can be made between the two because they are similar in some way.</p>
<p class='rw-defn idx3' style='display:none'>If someone is granted clemency, they are punished less severely than they could have been—they have been shown mercy.</p>
<p class='rw-defn idx4' style='display:none'>Conciliation is a process intended to end an argument between two groups of people.</p>
<p class='rw-defn idx5' style='display:none'>Two items are fungible if one can be exchanged for the other with no loss in inherent value; for example, one $10 bill and two $5 bills are fungible.</p>
<p class='rw-defn idx6' style='display:none'>If someone is implacable, they very stubbornly react to situations or the opinions of others because of strong feelings that make them unwilling to change their mind.</p>
<p class='rw-defn idx7' style='display:none'>If someone is inimical, they are unfriendly and will likely cause you harm.</p>
<p class='rw-defn idx8' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx9' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx10' style='display:none'>If someone exhibits magnanimity towards another, they show them kindness and noble generosity, especially after defeating them in battle or after having been treated badly by them.</p>
<p class='rw-defn idx11' style='display:none'>Malice is a type of hatred that causes a strong desire to harm others both physically and emotionally.</p>
<p class='rw-defn idx12' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx13' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx14' style='display:none'>A punitive action is intended to punish someone.</p>
<p class='rw-defn idx15' style='display:none'>If two people have established a good rapport, their connection is such that they have a good understanding of and can communicate well with one other.</p>
<p class='rw-defn idx16' style='display:none'>Rapprochement is the development of greater understanding and friendliness between two countries or groups of people after a period of unfriendly relations.</p>
<p class='rw-defn idx17' style='display:none'>When you rebuke someone, you harshly scold or criticize them for something they&#8217;ve done.</p>
<p class='rw-defn idx18' style='display:none'>If you reciprocate, you give something to someone because they have given something similar to you.</p>
<p class='rw-defn idx19' style='display:none'>A recrimination is a retaliatory accusation you make against someone who has accused you of something first.</p>
<p class='rw-defn idx20' style='display:none'>You show rectitude if you behave or conduct yourself in an honest and morally correct manner.</p>
<p class='rw-defn idx21' style='display:none'>A rejoinder is a quick answer to a reply or remark that can be rude, angry, clever, or defensive.</p>
<p class='rw-defn idx22' style='display:none'>Restitution is the formal act of giving something that was taken away back to the rightful owner—or paying them money for the loss of the object.</p>
<p class='rw-defn idx23' style='display:none'>When you retaliate, you get back at or get even with someone for something that they did to you.</p>
<p class='rw-defn idx24' style='display:none'>When you give a retort to what someone has said, you reply in a quick and witty fashion.</p>
<p class='rw-defn idx25' style='display:none'>A riposte is a quick and clever reply that is often made in answer to criticism of some kind.</p>
<p class='rw-defn idx26' style='display:none'>If one thing is tantamount to another thing, it means that it is equivalent to the other.</p>
<p class='rw-defn idx27' style='display:none'>If you feel unrequited love for another, you love that person, but they don&#8217;t love you in return.</p>
<p class='rw-defn idx28' style='display:none'>A vendetta is a prolonged situation in which one person or group tries to harm another person or group—and vice versa.</p>
<p class='rw-defn idx29' style='display:none'>If you are vengeful, you want to get back at someone for something that they did to you because you are unwilling to forgive them.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>vindictive</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='vindictive#' id='pronounce-sound' path='audio/words/amy-vindictive'></a>
vin-DIK-tiv
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='vindictive#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='vindictive#' id='context-sound' path='audio/wordcontexts/brian-vindictive'></a>
The ugly stepsisters in Cinderella are so <em>vindictive</em> or filled with revenge.  Anything the beautiful Cinderella did caused the stepsisters to react in the most <em>vindictive</em> or hateful ways.  The <em>vindictive</em> or spiteful stepsisters made Cinderella&#8217;s life very miserable until she was eventually rescued by Prince Charming.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of <em>vindictive</em> behavior?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Being so jealous of someone else that you constantly try to imitate them.
</li>
<li class='choice answer '>
<span class='result'></span>
Spilling paint on your sister&#8217;s favorite shirt after she ruined yours.
</li>
<li class='choice '>
<span class='result'></span>
Accidentally dropping your mom&#8217;s phone into a large puddle of water.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='vindictive#' id='definition-sound' path='audio/wordmeanings/amy-vindictive'></a>
If you are <em>vindictive</em>, you want to take revenge upon someone who has done something bad to you.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>revengeful</em>
</span>
</span>
</div>
<a class='quick-help' href='vindictive#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='vindictive#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/vindictive/memory_hooks/4265.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Vin</span></span>egar <span class="emp2"><span>Dicti</span></span>onary</span></span> Gilbert the Horrid poured <span class="emp1"><span>vin</span></span>egar over the evil English teacher's favorite <span class="emp2"><span>dicti</span></span>onary in a <span class="emp1"><span>vin</span></span><span class="emp2"><span>dicti</span></span>ve act for receiving an F- in the class.
</p>
</div>

<div id='memhook-button-bar'>
<a href="vindictive#" id="add-public-hook" style="" url="https://membean.com/mywords/vindictive/memory_hooks">Use other public hook</a>
<a href="vindictive#" id="memhook-use-own" url="https://membean.com/mywords/vindictive/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='vindictive#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Likewise, put-down humor can at times be an adaptive, healthy response: Employees suffering under a <b>vindictive</b> boss will often make the office more bearable by secretly ridiculing their tyrant.
<cite class='attribution'>
&mdash;
Psychology Today
</cite>
</li>
<li>
All of them appear to have been taken down, presumable after some <b>vindictive</b> copyright vampires asked Google to pull the videos from YouTube.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
Ying’s family are branded bourgeois enemies of the state and Ying discovers that she is at the mercy of <b>vindictive</b> classmates and traitorous neighbors.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/vindictive/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='vindictive#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='vindic_avenger' data-tree-url='//cdn1.membean.com/public/data/treexml' href='vindictive#'>
<span class=''></span>
vindic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>avenger, defender, revenge</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ive_does' data-tree-url='//cdn1.membean.com/public/data/treexml' href='vindictive#'>
<span class=''></span>
-ive
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or that which does something</td>
</tr>
</table>
<p>A <em>vindictive</em> person is bent on &#8220;revenge.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='vindictive#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Vindictive" src="https://cdn2.membean.com/public/images/wordimages/cons2/vindictive.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='vindictive#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>affliction</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>analogous</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>fungible</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>implacable</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>inimical</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>malice</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>punitive</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>rebuke</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>reciprocate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>recrimination</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>rejoinder</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>restitution</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>retaliate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>retort</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>riposte</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>tantamount</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>vendetta</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>vengeful</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>absolve</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>clemency</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>conciliation</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>magnanimity</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>rapport</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>rapprochement</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>rectitude</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>unrequited</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="vindictive" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>vindictive</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="vindictive#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

