
<!DOCTYPE html>
<html>
<head>
<title>Word: solicitous | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx1' style='display:none'>If your behavior or manner is altruistic, you show you care more about other people and their interests than you care about yourself.</p>
<p class='rw-defn idx2' style='display:none'>Amity is a peaceful friendship between two parties, particularly between two countries.</p>
<p class='rw-defn idx3' style='display:none'>Apathy is a lack of interest or unenthusiastic involvement in an activity; there is no effort to change or improve it at all.</p>
<p class='rw-defn idx4' style='display:none'>When you have ardor for something, you have an intense feeling of love, excitement, and admiration for it.</p>
<p class='rw-defn idx5' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx6' style='display:none'>If you describe a person&#8217;s behavior as brazen, you mean that they are not embarrassed by anything they do—they simply don&#8217;t care what other people think about them.</p>
<p class='rw-defn idx7' style='display:none'>A cavalier person does not seem to care about rules, principles, or other people&#8217;s feelings, no matter how dire or serious a situation may be.</p>
<p class='rw-defn idx8' style='display:none'>A chivalrous man behaves in a polite, kind, generous, and honorable way, especially towards women.</p>
<p class='rw-defn idx9' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx10' style='display:none'>If you feel compunction about doing something, you feel that you should not do it because it is bad or wrong.</p>
<p class='rw-defn idx11' style='display:none'>A man who is debonair wears fashionable clothes and is sophisticated, charming, friendly, and confident.</p>
<p class='rw-defn idx12' style='display:none'>If you behave with deference towards someone, you show them respect and accept their opinions or decisions, especially because they have an important position.</p>
<p class='rw-defn idx13' style='display:none'>If something discomfits you, it makes you feel embarrassed, confused, uncomfortable, or frustrated.</p>
<p class='rw-defn idx14' style='display:none'>Someone does something in a disinterested way when they have no personal involvement or attachment to the action.</p>
<p class='rw-defn idx15' style='display:none'>When someone feels disquiet about a situation, they feel very worried or nervous.</p>
<p class='rw-defn idx16' style='display:none'>When a person is being flippant, they are not taking something as seriously as they should be; therefore, they tend to dismiss things they should respectfully pay attention to.</p>
<p class='rw-defn idx17' style='display:none'>To handle an object gingerly is to be careful and cautious with it.</p>
<p class='rw-defn idx18' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx19' style='display:none'>Insouciance is a lack of concern or worry for something that should be shown more careful attention or consideration.</p>
<p class='rw-defn idx20' style='display:none'>If you do something in a lackadaisical way, you do it carelessly and without putting much effort into it—thereby showing that you are not really interested in what you&#8217;re doing.</p>
<p class='rw-defn idx21' style='display:none'>Largess is the generous act of giving money or presents to a large number of people.</p>
<p class='rw-defn idx22' style='display:none'>If someone exhibits magnanimity towards another, they show them kindness and noble generosity, especially after defeating them in battle or after having been treated badly by them.</p>
<p class='rw-defn idx23' style='display:none'>Malaise is a feeling of discontent, general unease, or even illness in a person or group for which there does not seem to be a quick and easy solution because the cause of it is not clear.</p>
<p class='rw-defn idx24' style='display:none'>A munificent person is extremely generous, especially with money.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx26' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx27' style='display:none'>If you have qualms about doing something, you are having doubts about it because you are worried that it might be the wrong thing to do.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>solicitous</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='solicitous#' id='pronounce-sound' path='audio/words/amy-solicitous'></a>
suh-LIS-i-tuhs
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='solicitous#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='solicitous#' id='context-sound' path='audio/wordcontexts/brian-solicitous'></a>
In <em>solicitous</em>, thoughtful concern for her well-being, Manfred reminded Melissa to wear a warm coat that cold and snowy night.  Manfred&#8217;s awareness of Melissa&#8217;s needs and <em>solicitous</em> attention to her health earned her affection.  Melissa could always count on Manfred to behave in a <em>solicitous</em>, mindful, and loving way.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What might a <em>solicitous</em> person do?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Use an unfair advantage to succeed in business.
</li>
<li class='choice '>
<span class='result'></span>
Maintain politeness and proper manners at all times.
</li>
<li class='choice answer '>
<span class='result'></span>
Bring supplies to help you when you are feeling sick.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='solicitous#' id='definition-sound' path='audio/wordmeanings/amy-solicitous'></a>
A person who is <em>solicitous</em> behaves in a way that shows great concern about someone&#8217;s health, feelings, safety, etc.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>concerned</em>
</span>
</span>
</div>
<a class='quick-help' href='solicitous#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='solicitous#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/solicitous/memory_hooks/4508.json'></span>
<p>
<img alt="Solicitous" src="https://cdn3.membean.com/public/images/wordimages/hook/solicitous.jpg?qdep8" />
<span class="emp0"><span><span class="emp1"><span>So</span></span><span class="emp2"><span>lic</span></span><span class="emp3"><span>it</span></span>o<span class="emp1"><span>us</span></span> If <span class="emp1"><span>So</span></span>me <span class="emp2"><span>Lice</span></span> B<span class="emp3"><span>it</span></span> <span class="emp1"><span>Us</span></span></span></span> When our teacher saw me scratch my head she became worried and <span class="emp1"><span>so</span></span><span class="emp2"><span>lic</span></span><span class="emp3"><span>it</span></span>o<span class="emp1"><span>us</span></span>ly asked <span class="emp1"><span>us</span></span> all if <span class="emp1"><span>so</span></span>me <span class="emp2"><span>lice</span></span> b<span class="emp3"><span>it</span></span> <span class="emp1"><span>us</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="solicitous#" id="add-public-hook" style="" url="https://membean.com/mywords/solicitous/memory_hooks">Use other public hook</a>
<a href="solicitous#" id="memhook-use-own" url="https://membean.com/mywords/solicitous/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='solicitous#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Very proudly, he showed me his ritual: he dusted the custom-made case that housed [the Stanley Cup trophy], and of which he was so <b>solicitous</b>, with its foam Cup-shaped packing to protect it and cushion it from any ruts along the way. “Made the foam rubber especially for the Cup—look at how it fits!” Reid said, beaming.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
What we're going to see is increasingly <b>solicitous</b> machines defining our environment—machines that sense and respond to our needs "intelligently." But it will be the intelligence of the serving hand rather than the commanding brain, and we're only at risk of disaster if we [harbor] self-destructive impulses.
<cite class='attribution'>
&mdash;
Discover Magazine
</cite>
</li>
<li>
Sure pet-friendly hotels are nice, but what about those properties that go that extra mile to ensure guests enjoy all manner of creature comforts by having a friendly dog, cat or even a pony in residence. At these unique hotels, <b>solicitous</b> service may just include a tail wag, a gentle gallop or a welcoming purr.
<cite class='attribution'>
&mdash;
Forbes
</cite>
</li>
<li>
Vinnie Carbone takes top billing as chef-owner, Bob Peterson as head chef. Service is <b>solicitous</b>, with managers circulating throughout the dining areas.
<cite class='attribution'>
&mdash;
Hartford Courant
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/solicitous/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='solicitous#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='sol_whole' data-tree-url='//cdn1.membean.com/public/data/treexml' href='solicitous#'>
<span class=''></span>
sol
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>solid, whole, firm</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='i_connective' data-tree-url='//cdn1.membean.com/public/data/treexml' href='solicitous#'>
<span class=''></span>
-i-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>connective</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='cit_moved' data-tree-url='//cdn1.membean.com/public/data/treexml' href='solicitous#'>
<span class='common'></span>
cit
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>moved, stirred up</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ous_possessing' data-tree-url='//cdn1.membean.com/public/data/treexml' href='solicitous#'>
<span class=''></span>
-ous
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>possessing the nature of</td>
</tr>
</table>
<p>When one person is being <em>solicitous</em> about the health of another, she is &#8220;firmly or solidly moved or stirred up&#8221; about it, and tries to help that person as much as she can.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='solicitous#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Bones</strong><span> Dr. Hodgins' solicitous offering exposes his affection for Angela.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/solicitous.jpg' video_url='examplevids/solicitous' video_width='350'></span>
<div id='wt-container'>
<img alt="Solicitous" height="288" src="https://cdn1.membean.com/video/examplevids/solicitous.jpg" width="350" />
<div class='center'>
<a href="solicitous#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='solicitous#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Solicitous" src="https://cdn0.membean.com/public/images/wordimages/cons2/solicitous.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='solicitous#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>altruistic</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>amity</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>ardor</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>chivalrous</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>compunction</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>debonair</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>deference</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>discomfit</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>disquiet</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>gingerly</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>largess</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>magnanimity</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>malaise</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>munificent</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>qualm</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>apathy</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>brazen</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>cavalier</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>disinterested</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>flippant</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>insouciance</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>lackadaisical</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='solicitous#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
solicit
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to repeatedly ask somebody for something, thus "worrying" him</td>
</tr>
<tr>
<td class='wordform'>
<span>
solicitude
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>worry or concern for the well-being of another</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="solicitous" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>solicitous</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="solicitous#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

