
<!DOCTYPE html>
<html>
<head>
<title>Word: august | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="August-large" id="bk-img" src="https://cdn2.membean.com/public/images/wordimages/bkgd2/august-large.jpg?qdep8" />
</div>
<a href="august#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When you arraign someone, you make them come to court and answer criminal charges made against them.</p>
<p class='rw-defn idx1' style='display:none'>Someone who is callow is young, immature, and inexperienced; consequently, they possess little knowledge of the world.</p>
<p class='rw-defn idx2' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx3' style='display:none'>If you behave with deference towards someone, you show them respect and accept their opinions or decisions, especially because they have an important position.</p>
<p class='rw-defn idx4' style='display:none'>To defile something holy or important is to do something to it or say something about it that is highly offensive to others.</p>
<p class='rw-defn idx5' style='display:none'>If someone is deified, they have been either made into a god or are adored like one.</p>
<p class='rw-defn idx6' style='display:none'>If you denounce people or actions, you criticize them severely in public because you feel strongly that they are wrong or evil.</p>
<p class='rw-defn idx7' style='display:none'>If you desecrate something that is considered holy or very special, you deliberately spoil or damage it.</p>
<p class='rw-defn idx8' style='display:none'>Something that is hallowed is respected and admired, usually because it is holy or important in some way.</p>
<p class='rw-defn idx9' style='display:none'>When you pay homage to another person, you show them great admiration or respect; you might even worship them.</p>
<p class='rw-defn idx10' style='display:none'>If you believe someone&#8217;s behavior exhibits idolatry, you think they blindly admire someone or something too much.</p>
<p class='rw-defn idx11' style='display:none'>Something that has inestimable value or benefit has so much of it that it cannot be calculated.</p>
<p class='rw-defn idx12' style='display:none'>When someone is lionized, they are treated as being very important or famous—although they may not deserve to be.</p>
<p class='rw-defn idx13' style='display:none'>If someone exhibits magnanimity towards another, they show them kindness and noble generosity, especially after defeating them in battle or after having been treated badly by them.</p>
<p class='rw-defn idx14' style='display:none'>A munificent person is extremely generous, especially with money.</p>
<p class='rw-defn idx15' style='display:none'>Philanthropy is unselfish support in the form of donating money, work, or gifts to positive social purposes; philanthropy is also overall love for humans in general.</p>
<p class='rw-defn idx16' style='display:none'>When someone proscribes an activity, they prohibit it by establishing a rule against it.</p>
<p class='rw-defn idx17' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>
<p class='rw-defn idx18' style='display:none'>Something that is sacrosanct is considered to be so important, special, or holy that no one is allowed to criticize, tamper with, or change it in any way.</p>
<p class='rw-defn idx19' style='display:none'>Something that is sublime is so strikingly beautiful that it seems not of this world; therefore, it affects the emotions deeply.</p>
<p class='rw-defn idx20' style='display:none'>To taint is to give an undesirable quality that damages a person&#8217;s reputation or otherwise spoils something.</p>
<p class='rw-defn idx21' style='display:none'>A tirade is a prolonged, verbal outburst that severely criticizes someone or something.</p>
<p class='rw-defn idx22' style='display:none'>A troglodyte is reclusive, severely lacking in social skills, and is out of step with current times.</p>
<p class='rw-defn idx23' style='display:none'>A tyro has just begun learning something.</p>
<p class='rw-defn idx24' style='display:none'>To act in an uncouth manner is to be awkward and unmannerly.</p>
<p class='rw-defn idx25' style='display:none'>An unkempt person or thing is untidy and has not been kept neat.</p>
<p class='rw-defn idx26' style='display:none'>Venerable people command respect because they are old and wise.</p>
<p class='rw-defn idx27' style='display:none'>If someone vitiates something, they make it less effective, weaker, or reduced in quality.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>august</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='august#' id='pronounce-sound' path='audio/words/amy-august'></a>
AW-guhst
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='august#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='august#' id='context-sound' path='audio/wordcontexts/brian-august'></a>
The dignified and <em>august</em> presence of the great king&#8217;s emerging from his strong castle caused a hush to drop over the waiting crowd.  His <em>august</em> influence was so highly respected by his subjects that he seemed larger than life.  His servants felt such awe in the <em>august</em> and impressive presence of his majesty that they remained silent for some time even after he left them.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Which person would be described as <em>august</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A leader who abuses certain aspects of their office for personal gain.
</li>
<li class='choice answer '>
<span class='result'></span>
A writer who is respected and honored for their brilliant work.
</li>
<li class='choice '>
<span class='result'></span>
A student who attends summer school to improve their grades.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='august#' id='definition-sound' path='audio/wordmeanings/amy-august'></a>
Someone or something that is <em>august</em> is impressive, dignified, and highly respected.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>dignified</em>
</span>
</span>
</div>
<a class='quick-help' href='august#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='august#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/august/memory_hooks/5587.json'></span>
<p>
<span class="emp0"><span>Emperor <span class="emp1"><span>August</span></span>us Given Name of Month</span></span>  The first emperor of Rome, <span class="emp1"><span>August</span></span>us, was so highly thought of and considered to be so <span class="emp1"><span>august</span></span> that the month of <span class="emp1"><span>August</span></span> was named after him.
</p>
</div>

<div id='memhook-button-bar'>
<a href="august#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/august/memory_hooks">Use other hook</a>
<a href="august#" id="memhook-use-own" url="https://membean.com/mywords/august/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='august#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
There are plenty of books about evolution, fossils, and the history of science—nearly all authored by more <b>august</b> and respected writers than myself—and it took a long time for me to devise an original approach to these topics.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
It is a distinct . . . honor to work with the <b>august</b> Regina King. I would call her Queen King. This woman is one of our treasures.
<cite class='attribution'>
&mdash;
Russell Hornsby, American actor in an interview about the crime drama _Seven Seconds_
</cite>
</li>
<li>
The Juilliard String Quartet, among the most <b>august</b> and respected of American chamber music institutions, began a farewell of sorts before a sizable audience at Alice Tully Hall on Tuesday night.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Yet a closer look reveals these <b>august</b> institutions to be as subject to stress, conflict and uncertainty as any other human enterprise, and complicated by artistic temperament.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/august/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='august#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>aug</td>
<td>
&rarr;
</td>
<td class='meaning'>increase, make greater</td>
</tr>
</table>
<p>One who is <em>august</em> has &#8220;increased&#8221; power or has been &#8220;made greater&#8221; than others.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='august#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="August" src="https://cdn2.membean.com/public/images/wordimages/cons2/august.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='august#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>deference</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>deify</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>hallowed</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>homage</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>idolatry</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>inestimable</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>lionize</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>magnanimity</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>munificent</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>philanthropy</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>sacrosanct</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>sublime</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>venerable</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>arraign</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>callow</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>defile</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>denounce</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>desecrate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>proscribe</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>taint</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>tirade</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>troglodyte</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>tyro</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>uncouth</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>unkempt</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>vitiate</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="august" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>august</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="august#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

