
<!DOCTYPE html>
<html>
<head>
<title>Word: behemoth | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Behemoth-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/behemoth-large.jpg?qdep8" />
</div>
<a href="behemoth#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>If you abridge a book or play, you make it shorter by making cuts to the original.</p>
<p class='rw-defn idx1' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx2' style='display:none'>Attrition is the process of gradually decreasing the strength of something (such as an enemy force) by continually attacking it.</p>
<p class='rw-defn idx3' style='display:none'>To augment something is to increase its value or effectiveness by adding something to it.</p>
<p class='rw-defn idx4' style='display:none'>Something colossal is extremely big, gigantic, or huge.</p>
<p class='rw-defn idx5' style='display:none'>A copious amount of something is a very large amount of it.</p>
<p class='rw-defn idx6' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx7' style='display:none'>If something is formidable, it is impressive in size, power, or skill; therefore, it can make you feel frightened, alarmed, or in awe because it is so powerful or difficult to deal with.</p>
<p class='rw-defn idx8' style='display:none'>Something gargantuan is extremely large.</p>
<p class='rw-defn idx9' style='display:none'>Something that is imperceptible is either impossible to be perceived by the mind or is very difficult to perceive.</p>
<p class='rw-defn idx10' style='display:none'>Something infinitesimal is so extremely small or minute that it is very hard to measure it.</p>
<p class='rw-defn idx11' style='display:none'>A leviathan is something that is very large, powerful, difficult to control, and rather frightening.</p>
<p class='rw-defn idx12' style='display:none'>Something minuscule is extremely small in size or amount.</p>
<p class='rw-defn idx13' style='display:none'>An organization or system that is a monolith is extremely large; additionally, it is unwilling or very slow to change or adopt something new.</p>
<p class='rw-defn idx14' style='display:none'>An omnipotent being or entity is unlimited in its power.</p>
<p class='rw-defn idx15' style='display:none'>Something that is prodigious is very large, impressive, or great.</p>
<p class='rw-defn idx16' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx17' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>behemoth</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='behemoth#' id='pronounce-sound' path='audio/words/amy-behemoth'></a>
bi-HEE-muth
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='behemoth#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='behemoth#' id='context-sound' path='audio/wordcontexts/brian-behemoth'></a>
The business my father started has become an industrial <em>behemoth</em>, one of the largest and most powerful of its kind.  He manufactures <em>behemoth</em>-sized tractors, whose wheels are over ten feet tall and are used in the building of superhighways.  When I see those huge vehicles, it reminds me of the gigantic, reptilian <em><em>behemoths</em></em> of prehistoric Earth, such as the mighty dinosaur Supersaurus, which was about one hundred feet in length.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>behemoth</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
An enormous beast that cannot be controlled.
</li>
<li class='choice '>
<span class='result'></span>
A large suitcase that is difficult for one person to lift.
</li>
<li class='choice '>
<span class='result'></span>
An old and worn copy of a significant book.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='behemoth#' id='definition-sound' path='audio/wordmeanings/amy-behemoth'></a>
A <em>behemoth</em> is monstrously large in size or power, such as a gigantic whale or corporation; sometimes this word is given a negative implication in the sense that a <em>behemoth</em> is so large that it is difficult or impossible to manage.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>giant</em>
</span>
</span>
</div>
<a class='quick-help' href='behemoth#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='behemoth#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/behemoth/memory_hooks/2892.json'></span>
<p>
<span class="emp0"><span>"<span class="emp2"><span>Ehem</span></span>, You Want <span class="emp3"><span>Both</span></span>?"</span></span>  Today it was announced that the financial titan Bill Gates wanted to buy Apple Computer to create an absolutely monstrous <span class="emp3"><span>b</span></span><span class="emp2"><span>ehem</span></span><span class="emp3"><span>oth</span></span> that would completely dominate the computer market, to which pronouncement my son, after an "<span class="emp2"><span>ehem</span></span>" on my part to get his attention, quipped: "He wants <span class="emp3"><span>both</span></span>?"
</p>
</div>

<div id='memhook-button-bar'>
<a href="behemoth#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/behemoth/memory_hooks">Use other hook</a>
<a href="behemoth#" id="memhook-use-own" url="https://membean.com/mywords/behemoth/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='behemoth#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
The turn of the century was the age of the banker, so much so that the leading bankers of the day had become legendary figures in the public imagination-vast, overshadowing <b>behemoths</b> whose colossal power seemed to reach everywhere.
<span class='attribution'>&mdash; Doris Kearns Goodwin, American historian and biographer, from _The Fitzgeralds and the Kennedys_</span>
<img alt="Doris kearns goodwin, american historian and biographer, from _the fitzgeralds and the kennedys_" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Doris Kearns Goodwin, American historian and biographer, from _The Fitzgeralds and the Kennedys_.jpg?qdep8" width="80" />
</li>
<li>
A series of mergers has transformed the communications marketplace in the space of five months, producing a handful of giants capable of delivering an all-in-one package of phone and Internet services to businesses and consumers alike. . . . "All the services will be primarily under the umbrella of <b>behemoths</b>," he said, and with fewer competitors, "it means prices aren’t going to be in the free fall they have been."
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Bezos has propelled the online bookseller he started in his Bellevue, Wash., garage in 1994 into a <b>behemoth</b> worth $1.7 trillion employing over 1.3 million people, with physical stores, delivery trucks, movies, Internet-connected cameras, a cloud business that props up the CIA and enough paying subscribers to populate the ninth-largest country on Earth.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
The United States is—by the scale of modern nations—an ancient <b>behemoth</b>. Virtually unchanged constitutionally in 230 years, it has grown to an unwieldy size. It is so large that holding together a sense of national identity has become almost impossible.
<cite class='attribution'>
&mdash;
ABC News
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/behemoth/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='behemoth#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>From a root word meaning &#8220;beast.&#8221;</p>
<a href="behemoth#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> Hebrew</strong>
<span id='wordorigin-map'></span>
<span>The Behemoth was a monstrous beast mentioned in the Book of Job in the Old Testament.</span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='behemoth#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Behemoth" src="https://cdn3.membean.com/public/images/wordimages/cons2/behemoth.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='behemoth#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>augment</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>colossal</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>copious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>formidable</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>gargantuan</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>infinitesimal</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>leviathan</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>monolith</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>omnipotent</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>prodigious</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abridge</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>attrition</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>imperceptible</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>minuscule</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="behemoth" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>behemoth</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="behemoth#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

