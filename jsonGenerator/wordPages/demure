
<!DOCTYPE html>
<html>
<head>
<title>Word: demure | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>You affront someone by openly and intentionally offending or insulting him.</p>
<p class='rw-defn idx1' style='display:none'>An audacious person acts with great daring and sometimes reckless bravery—despite risks and warnings from other people—in order to achieve something.</p>
<p class='rw-defn idx2' style='display:none'>Someone who is boisterous is noisy, excitable, and full of boundless energy; therefore, they show a lack of disciplined restraint at times.</p>
<p class='rw-defn idx3' style='display:none'>When a person is speaking or writing in a bombastic fashion, they are very self-centered, extremely showy, and excessively proud.</p>
<p class='rw-defn idx4' style='display:none'>Braggadocio is an excessively proud way of talking about your achievements or possessions that can annoy others.</p>
<p class='rw-defn idx5' style='display:none'>If you describe a person&#8217;s behavior as brazen, you mean that they are not embarrassed by anything they do—they simply don&#8217;t care what other people think about them.</p>
<p class='rw-defn idx6' style='display:none'>Bumptious people are annoying because they are too proud of their abilities or opinions—they are full of themselves.</p>
<p class='rw-defn idx7' style='display:none'>A decadent person has low moral standards and is more interested in pleasure than serious matters.</p>
<p class='rw-defn idx8' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is diffident is shy, does not want to draw notice to themselves, and is lacking in self-confidence.</p>
<p class='rw-defn idx10' style='display:none'>When you make an emphatic declaration, you are insistent and absolute about it.</p>
<p class='rw-defn idx11' style='display:none'>When someone flaunts their good looks, they show them off or boast about them in a very proud and shameless way.</p>
<p class='rw-defn idx12' style='display:none'>If someone behaves in an impertinent way, they behave rudely and disrespectfully.</p>
<p class='rw-defn idx13' style='display:none'>Something that inhibits you from doing something restricts or keeps you from doing it.</p>
<p class='rw-defn idx14' style='display:none'>To interject is to insert a comment during a conversation that interrupts its flow.</p>
<p class='rw-defn idx15' style='display:none'>When you interpose, you interrupt or interfere in some fashion.</p>
<p class='rw-defn idx16' style='display:none'>An intrusive person intrudes, butts in, or interferes where they are not welcome.</p>
<p class='rw-defn idx17' style='display:none'>When you are acting in a melodramatic way, you are overreacting to something in an overly dramatic and exaggerated way.</p>
<p class='rw-defn idx18' style='display:none'>A notorious person is well-known by the public at large; they are usually famous for doing something bad.</p>
<p class='rw-defn idx19' style='display:none'>If you are pompous, you think that you are better than other people; therefore, you tend to show off and be highly egocentric.</p>
<p class='rw-defn idx20' style='display:none'>If you are pretentious, you think you are really great in some way and let everyone know about it, despite the fact that it&#8217;s not the case at all.</p>
<p class='rw-defn idx21' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx22' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx23' style='display:none'>Rambunctious conduct is wild, unruly, very active, and sometimes hard to control.</p>
<p class='rw-defn idx24' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx25' style='display:none'>Skittish persons or animals are made easily nervous or alarmed; they are likely to change behavior quickly and unpredictably.</p>
<p class='rw-defn idx26' style='display:none'>When you strut, you move as though you own the world by walking in a confident and showy fashion.</p>
<p class='rw-defn idx27' style='display:none'>To be timorous is to be fearful.</p>
<p class='rw-defn idx28' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx29' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>
<p class='rw-defn idx30' style='display:none'>If you are vainglorious, you are very proud of yourself and let other people know about it.</p>
<p class='rw-defn idx31' style='display:none'>Something that is vaunted, such as someone&#8217;s ability, is too highly praised and boasted about.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>demure</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='demure#' id='pronounce-sound' path='audio/words/amy-demure'></a>
di-MYOOR
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='demure#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='demure#' id='context-sound' path='audio/wordcontexts/brian-demure'></a>
The Donovan twins looked the same, but one sister was bold and the other shy and <em>demure</em>.  While Deirdre sat <em>demurely</em> and spoke politely at dinner parties, Devon interrupted guests loudly and reached over them for more cake.  Even though Devon was more popular and outgoing, Deirdre remained a favorite with her grandfather who preferred reserved, modest, well-mannered, and <em>demure</em> young ladies.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How would someone behave if they were <em>demure</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
They would be shy and reserved in most social situations.
</li>
<li class='choice '>
<span class='result'></span>
They would walk into any room confidently and take control.
</li>
<li class='choice '>
<span class='result'></span>
They would lie to make themselves seem more accomplished.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='demure#' id='definition-sound' path='audio/wordmeanings/amy-demure'></a>
If you describe someone, usually a young woman, as <em>demure</em>, you mean that she is quiet, shy, and always behaves modestly.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>reserved</em>
</span>
</span>
</div>
<a class='quick-help' href='demure#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='demure#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/demure/memory_hooks/5313.json'></span>
<p>
<span class="emp0"><span>De<span class="emp3"><span>mur</span></span>e <span class="emp3"><span>Mur</span></span>al</span></span> "Did you see that large wall <span class="emp3"><span>mur</span></span>al that you'd pretty much miss if there weren't flashing lights all around it?  I can't believe that a painting that covers an entire wall could hardly be noticed, but hey, that's just the way it is ... a de<span class="emp3"><span>mur</span></span>e <span class="emp3"><span>mur</span></span>al.  What do you think the artist meant by that?"
</p>
</div>

<div id='memhook-button-bar'>
<a href="demure#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/demure/memory_hooks">Use other hook</a>
<a href="demure#" id="memhook-use-own" url="https://membean.com/mywords/demure/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='demure#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The 71-year-old Hopi woman deflects when asked what makes her jewelry so special. She’s also <b>demure</b> when talking about the first retrospective of her work, which was held in 2019 at the Heard Museum in Phoenix. She’s more chatty about the craft of jewelry-making itself.
<cite class='attribution'>
&mdash;
5280 City Magazine
</cite>
</li>
<li>
[T]he cover of _Life_ magazine, 28 October 1920: The drawings of the illustrator Charles Dana Gibson epitomized the “New Woman,” the era’s ideal of educated, upper-class, white femininity. On this cover of _Life_ magazine, the female embodiment of America shakes hands with a <b>demure</b> New Woman holding a ballot. This representation largely encapsulated the demographic who had secured the vote.
<cite class='attribution'>
&mdash;
Apollo
</cite>
</li>
<li>
Jane [Austen], played by Anne Hathaway, is 20 when the film takes place. Independent-minded and already a budding writer, she bridles at the late 18th-century etiquette that requires well-to-do young ladies to be <b>demure</b> and marry for position rather than love.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
“We are living in a present that they willed into existence with their pencils, their slide rules, their mechanical calculating machines—and, of course, their brilliant minds,” she said of Johnson and her fellow human computers, according to the NASA press release. . . . Johnson was considerably more <b>demure</b> about her achievements during a pre-taped interview with NASA, when she was asked to share her thoughts about the new facility named in her honor. “You want my honest answer?” she said with a laugh. “I think they’re crazy.”
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/demure/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='demure#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='de_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='demure#'>
<span class='common'></span>
de-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='mur_delay' data-tree-url='//cdn1.membean.com/public/data/treexml' href='demure#'>
<span class=''></span>
mur
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>delay, stay behind</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='demure#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>When someone acts in a <em>demure</em> fashion, they &#8220;thoroughly stay behind&#8221; in a social scene; they do not want to be in the forefront but instead modestly stay in the background so as not to call attention to themselves.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='demure#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Snow White</strong><span> Snow White is being quite demure around Prince Charming.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/demure.jpg' video_url='examplevids/demure' video_width='350'></span>
<div id='wt-container'>
<img alt="Demure" height="288" src="https://cdn1.membean.com/video/examplevids/demure.jpg" width="350" />
<div class='center'>
<a href="demure#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='demure#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Demure" src="https://cdn2.membean.com/public/images/wordimages/cons2/demure.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='demure#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='8' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>diffident</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>inhibit</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>skittish</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>timorous</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>affront</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>audacious</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>boisterous</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bombastic</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>braggadocio</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>brazen</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>bumptious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>decadent</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>emphatic</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>flaunt</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>impertinent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>interject</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>interpose</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>intrusive</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>melodramatic</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>notorious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>pompous</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>pretentious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>rambunctious</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>strut</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>vainglorious</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>vaunted</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="demure" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>demure</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="demure#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

