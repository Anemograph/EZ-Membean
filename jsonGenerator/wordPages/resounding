
<!DOCTYPE html>
<html>
<head>
<title>Word: resounding | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Resounding-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/resounding-large.jpg?qdep8" />
</div>
<a href="resounding#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>A situation or condition that is abysmal is extremely bad or of wretched quality.</p>
<p class='rw-defn idx1' style='display:none'>The acme of something is its highest point of achievement or excellence.</p>
<p class='rw-defn idx2' style='display:none'>When you are adept at something, you are very good, skillful, or talented at it.</p>
<p class='rw-defn idx3' style='display:none'>Someone who is adroit is skillful and clever, especially in thought, behavior, or action.</p>
<p class='rw-defn idx4' style='display:none'>The apex of anything, such as an organization or system, is its highest part or most important position.</p>
<p class='rw-defn idx5' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx6' style='display:none'>A bravura performance, such as of a highly technical work for the violin, is done with consummate skill.</p>
<p class='rw-defn idx7' style='display:none'>A clarion call is a stirring, emotional, and strong appeal to people to take action on something.</p>
<p class='rw-defn idx8' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx9' style='display:none'>If you curtail something, you reduce or limit it.</p>
<p class='rw-defn idx10' style='display:none'>A decadent person has low moral standards and is more interested in pleasure than serious matters.</p>
<p class='rw-defn idx11' style='display:none'>A deft movement or action is made quickly and with skill.</p>
<p class='rw-defn idx12' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx13' style='display:none'>If you describe a person, group, or civilization as effete, you mean it is weak, exhausted,  powerless, unproductive, and/or corrupt.</p>
<p class='rw-defn idx14' style='display:none'>Something is flaccid when it is unpleasantly soft and weak, hangs limply, or lacks vigor and energy.</p>
<p class='rw-defn idx15' style='display:none'>If something is formidable, it is impressive in size, power, or skill; therefore, it can make you feel frightened, alarmed, or in awe because it is so powerful or difficult to deal with.</p>
<p class='rw-defn idx16' style='display:none'>If something is your forte, you are very good at it or know a lot about it.</p>
<p class='rw-defn idx17' style='display:none'>If you describe someone&#8217;s appearance or behavior as impeccable, you mean that it is perfect and therefore impossible to criticize.</p>
<p class='rw-defn idx18' style='display:none'>Someone, such as a performer or athlete, is inimitable when they are so good or unique in their talent that it is unlikely anyone else can be their equal.</p>
<p class='rw-defn idx19' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx20' style='display:none'>A languid person is slow, relaxed, and shows little energy or interest in doing anything.</p>
<p class='rw-defn idx21' style='display:none'>When you are listless, you lack energy and interest and are unwilling to exert any effort.</p>
<p class='rw-defn idx22' style='display:none'>Something is mawkish when it is overly sentimental and silly in an embarrassing way.</p>
<p class='rw-defn idx23' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx24' style='display:none'>Something is nondescript when its appearance is ordinary, dull, and not at all interesting or attractive.</p>
<p class='rw-defn idx25' style='display:none'>If something plummets, it falls down from a high position very quickly; for example, a piano can plummet from a window and a stock value can plummet during a market crash.</p>
<p class='rw-defn idx26' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>
<p class='rw-defn idx27' style='display:none'>When you have been remiss, you have been careless because you did not do something that you should have done.</p>
<p class='rw-defn idx28' style='display:none'>If you say that something, such as an event or a message, resonates with you, you mean that it has an emotional effect or a special meaning for you that is significant.</p>
<p class='rw-defn idx29' style='display:none'>A scintillating conversation, speech, or performance is brilliantly clever, interesting, and lively.</p>
<p class='rw-defn idx30' style='display:none'>A sonorous sound is pleasantly full, strong, and rich.</p>
<p class='rw-defn idx31' style='display:none'>A stentorian voice is extremely loud and strong.</p>
<p class='rw-defn idx32' style='display:none'>A virtuoso is someone who is very skillful at something, especially playing a musical instrument.</p>
<p class='rw-defn idx33' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>
<p class='rw-defn idx34' style='display:none'>The zenith of something is its highest, most powerful, or most successful point.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>resounding</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='resounding#' id='pronounce-sound' path='audio/words/amy-resounding'></a>
ri-ZOUN-ding
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='resounding#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='resounding#' id='context-sound' path='audio/wordcontexts/brian-resounding'></a>
The <em>resounding</em> or complete victory of our soccer team began when Dominique scored two goals in the first five minutes of the game. I will never forget the <em>resounding</em> cheers and yelling that echoed throughout the stadium as Dominique’s perfect shots sailed past the keeper. Then Ximena followed with two more goals of her own to begin the second half; the success of her shots was so <em>resounding</em> or very great because the keeper had no chance to block her shots. You should have heard the <em>resounding</em> shouting when Brianna scored the fifth and final goal for a 5-0 win—I’ll bet people two miles away heard us!  
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If something is <em>resounding</em>, what is it?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is repeated at regular intervals.
</li>
<li class='choice '>
<span class='result'></span>
It is painful to remember or think about.
</li>
<li class='choice answer '>
<span class='result'></span>
It is impressive or great in size.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='resounding#' id='definition-sound' path='audio/wordmeanings/amy-resounding'></a>
A <em>resounding</em> success, victory, or defeat is very great or complete, whereas a noise of this kind is loud, powerful, and ringing.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>very great</em>
</span>
</span>
</div>
<a class='quick-help' href='resounding#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='resounding#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/resounding/memory_hooks/4260.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Sounding</span></span>, <span class="emp3"><span>Sounding</span></span>, <span class="emp3"><span>Sounding</span></span></span></span> Gilbert was such a fabulous reader and student that he became a re<span class="emp3"><span>sounding</span></span> success; his re<span class="emp3"><span>sounding</span></span> academic career was <span class="emp3"><span>sounding</span></span>, <span class="emp3"><span>sounding</span></span>, and <span class="emp3"><span>sounding</span></span> over and over again throughout many years of complete and utter satisfaction for students and fellow professors alike.
</p>
</div>

<div id='memhook-button-bar'>
<a href="resounding#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/resounding/memory_hooks">Use other hook</a>
<a href="resounding#" id="memhook-use-own" url="https://membean.com/mywords/resounding/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='resounding#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
In the 1960s, I personally lived the <b>resounding</b> impact of President Nasser's vision of constructing Aswan's High Dam as a “national project” for controlling the Nile irrigation and the production of electricity.
<span class='attribution'>&mdash; Ahmed Zewail, Egyptian chemist and Nobel laureate</span>
<img alt="Ahmed zewail, egyptian chemist and nobel laureate" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Ahmed Zewail, Egyptian chemist and Nobel laureate.jpg?qdep8" width="80" />
</li>
<li>
Hill County Clerk and Recorder Sue Armstrong said that the 2020 election process was a <b>resounding</b> success, with massive turn-out and an overwhelming majority of mail-in ballots having been returned and tabulated without issue.
<cite class='attribution'>
&mdash;
Havre Daily News
</cite>
</li>
<li>
Emboldened by a <b>resounding</b> victory in snap elections last summer, the party has embarked on a wholesale overhaul of the hard-wiring of [Turkey’s] political system.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/resounding/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='resounding#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='re_back' data-tree-url='//cdn1.membean.com/public/data/treexml' href='resounding#'>
<span class='common'></span>
re-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>back, again</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='son_sound' data-tree-url='//cdn1.membean.com/public/data/treexml' href='resounding#'>
<span class='common'></span>
son
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>sound</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ing_action' data-tree-url='//cdn1.membean.com/public/data/treexml' href='resounding#'>
<span class=''></span>
-ing
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>a or of a continuous action</td>
</tr>
</table>
<p>A <em>resounding</em> success &#8220;sounds again and again&#8221; because it was so complete and wonderful.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='resounding#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Dirty Jobs</strong><span> Resounding clangs demand ears covered!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/resounding.jpg' video_url='examplevids/resounding' video_width='350'></span>
<div id='wt-container'>
<img alt="Resounding" height="288" src="https://cdn1.membean.com/video/examplevids/resounding.jpg" width="350" />
<div class='center'>
<a href="resounding#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='resounding#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Resounding" src="https://cdn0.membean.com/public/images/wordimages/cons2/resounding.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='resounding#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>acme</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>adept</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>adroit</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>apex</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>bravura</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>clarion</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>deft</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>formidable</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>forte</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>impeccable</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>inimitable</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>resonate</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>scintillating</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>sonorous</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>stentorian</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>virtuoso</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>zenith</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abysmal</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>curtail</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>decadent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>effete</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>flaccid</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>languid</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>listless</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>mawkish</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>nondescript</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>plummet</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>remiss</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="resounding" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>resounding</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="resounding#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

