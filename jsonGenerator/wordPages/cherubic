
<!DOCTYPE html>
<html>
<head>
<title>Word: cherubic | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Cherubic-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/cherubic-large.jpg?qdep8" />
</div>
<a href="cherubic#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>A beatific expression, look, or smile shows great peace and happiness—it is angelic and saintly.</p>
<p class='rw-defn idx1' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx2' style='display:none'>Someone who has a bilious personality is highly irritable and bad-tempered.</p>
<p class='rw-defn idx3' style='display:none'>Someone who has a bristling personality is easily offended, annoyed, or angered.</p>
<p class='rw-defn idx4' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx5' style='display:none'>Someone who is corpulent is extremely fat.</p>
<p class='rw-defn idx6' style='display:none'>If you say something is diabolical, you are emphasizing that it is evil, cruel, or very bad.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is disingenuous is not straightforward or is dishonest in what they say or do.</p>
<p class='rw-defn idx8' style='display:none'>Someone who is dour is serious, stubborn, and unfriendly; they can also be gloomy.</p>
<p class='rw-defn idx9' style='display:none'>A dulcet sound is soft and pleasant in a gentle way.</p>
<p class='rw-defn idx10' style='display:none'>A person or animal that is emaciated is extremely thin because of a serious illness or lack of food.</p>
<p class='rw-defn idx11' style='display:none'>Something ethereal has a delicate beauty that makes it seem not part of the real world.</p>
<p class='rw-defn idx12' style='display:none'>Someone who has a haggard appearance looks very tired, worn, thin, and exhausted.</p>
<p class='rw-defn idx13' style='display:none'>Something that is hallowed is respected and admired, usually because it is holy or important in some way.</p>
<p class='rw-defn idx14' style='display:none'>People who are ingenuous are excessively trusting and believe almost everything that people tell them, especially because they have not had much life experience.</p>
<p class='rw-defn idx15' style='display:none'>When something is luminous, it is bright and glowing.</p>
<p class='rw-defn idx16' style='display:none'>A malevolent person or thing is evil due to deliberate attempts to cause harm.</p>
<p class='rw-defn idx17' style='display:none'>Malice is a type of hatred that causes a strong desire to harm others both physically and emotionally.</p>
<p class='rw-defn idx18' style='display:none'>If someone is rotund, they have a round and fat body.</p>
<p class='rw-defn idx19' style='display:none'>If you are sanguine about a situation, especially a difficult one, you are confident and cheerful that everything will work out the way you want it to.</p>
<p class='rw-defn idx20' style='display:none'>Someone&#8217;s sardonic smile, expression, or comment shows a lack of respect for what someone else has said or done; this occurs because the former thinks they are too important to consider or discuss such a supposedly trivial matter of the latter.</p>
<p class='rw-defn idx21' style='display:none'>A troglodyte is reclusive, severely lacking in social skills, and is out of step with current times.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>cherubic</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='cherubic#' id='pronounce-sound' path='audio/words/amy-cherubic'></a>
chuh-ROO-bik
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='cherubic#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='cherubic#' id='context-sound' path='audio/wordcontexts/brian-cherubic'></a>
The little boy&#8217;s round face and bright eyes gave him a sweet and <em>cherubic</em> expression.  The guests smiled kindly when the boy interrupted the dinner party, looking around the room for his mother with an innocent and <em>cherubic</em> gaze.  It was clear that he had woken up from a deep sleep; his messy hair, flushed cheeks, and angelic, <em>cherubic</em> appearance made him look even younger than he actually was.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When might a child be called <em>cherubic</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When they are laughing and playing with their friends.
</li>
<li class='choice '>
<span class='result'></span>
When they follow directions and never forget to do something.
</li>
<li class='choice answer '>
<span class='result'></span>
When they have a sweet, kind face like a chubby baby&#8217;s.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='cherubic#' id='definition-sound' path='audio/wordmeanings/amy-cherubic'></a>
Someone is considered <em>cherubic</em> when they appear angelic in the natural and slightly fat way of a small child or baby.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>angelic</em>
</span>
</span>
</div>
<a class='quick-help' href='cherubic#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='cherubic#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/cherubic/memory_hooks/6075.json'></span>
<p>
<span class="emp0"><span>My <span class="emp1"><span>Chub</span></span>by <span class="emp1"><span>Cherub</span></span>'s <span class="emp1"><span>Cher</span></span>ry Face</span></span> I love to kiss my <span class="emp1"><span>chub</span></span>by <span class="emp1"><span>cherub</span></span>'s <span class="emp1"><span>cher</span></span>ry face, and I often <span class="emp1"><span>rub</span></span> in a red ointment to make it even more <span class="emp1"><span>cher</span></span>ry in color!
</p>
</div>

<div id='memhook-button-bar'>
<a href="cherubic#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/cherubic/memory_hooks">Use other hook</a>
<a href="cherubic#" id="memhook-use-own" url="https://membean.com/mywords/cherubic/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='cherubic#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
An early family photograph shows a <b>cherubic</b> little boy in the uniform of a Soviet naval cadet, grinning as he stands nestled between his father and mother.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
<li>
It was the height of the Cabbage Patch Kids craze, and toy stores were filled with their <b>cherubic</b> white faces; the few Black dolls scattered among them were made with the same shape and features, but used brown fabric.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/cherubic/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='cherubic#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>From a root word meaning &#8220;gracious.&#8221;  A <em>cherubic</em> or &#8220;angelic&#8221; person is filled with &#8220;grace.&#8221;</p>
<a href="cherubic#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> Akkadian</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='cherubic#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Cherubic" src="https://cdn1.membean.com/public/images/wordimages/cons2/cherubic.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='cherubic#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>beatific</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>corpulent</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>dulcet</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>ethereal</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>hallowed</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>ingenuous</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>luminous</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>rotund</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>sanguine</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>bilious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bristling</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>diabolical</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>disingenuous</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>dour</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>emaciated</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>haggard</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>malevolent</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>malice</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>sardonic</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>troglodyte</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='cherubic#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
cherub
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>an innocent child with a chubby, red face</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="cherubic" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>cherubic</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="cherubic#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

