
<!DOCTYPE html>
<html>
<head>
<title>Word: purview | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If something is in abeyance, it has been put on hold and is not proceeding or being used at the present time.</p>
<p class='rw-defn idx1' style='display:none'>Acumen is the ability to make quick decisions and keen, precise judgments.</p>
<p class='rw-defn idx2' style='display:none'>When you are adept at something, you are very good, skillful, or talented at it.</p>
<p class='rw-defn idx3' style='display:none'>Someone who is adroit is skillful and clever, especially in thought, behavior, or action.</p>
<p class='rw-defn idx4' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx5' style='display:none'>If one&#8217;s powers or rights are circumscribed, they are limited or restricted.</p>
<p class='rw-defn idx6' style='display:none'>If you curtail something, you reduce or limit it.</p>
<p class='rw-defn idx7' style='display:none'>If you delineate something, such as an idea or situation or border, you describe it in great detail.</p>
<p class='rw-defn idx8' style='display:none'>When you discern something, you notice, detect, or understand it, often after thinking about it carefully or studying it for some time.  </p>
<p class='rw-defn idx9' style='display:none'>Efficacy is the ability or power to produce an expected effect or result.</p>
<p class='rw-defn idx10' style='display:none'>An emissary is someone who acts as a representative from one government or leader to another.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx12' style='display:none'>If something fetters you, it restricts you from doing something you want to do.</p>
<p class='rw-defn idx13' style='display:none'>A gamut is a complete range of things of a particular type or the full extent of possibilities of some experience or action.</p>
<p class='rw-defn idx14' style='display:none'>A hapless person is unlucky, so much so that a stream of unfortunate things has happened and continues to happen to them.</p>
<p class='rw-defn idx15' style='display:none'>Hegemony manifests when a country, group, or organization has more political control or influence than others.</p>
<p class='rw-defn idx16' style='display:none'>A hierarchy is a system of organization in a society, company, or other group in which people are divided into different ranks or levels of importance.</p>
<p class='rw-defn idx17' style='display:none'>A myopic person is unable to visualize the long-term negative consequences of their current actions; rather, they are narrowly focused upon short-term results.</p>
<p class='rw-defn idx18' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx19' style='display:none'>A plenipotentiary is someone who has full power to take action or make decisions on behalf of their government in a foreign country.</p>
<p class='rw-defn idx20' style='display:none'>A potentate is a ruler who has great power over people.</p>
<p class='rw-defn idx21' style='display:none'>A stricture is a rule or condition that imposes restrictions or limits on what you can do.</p>
<p class='rw-defn idx22' style='display:none'>A tether is a restraint, such as a leash, rope, or chain, that holds something in place.</p>
<p class='rw-defn idx23' style='display:none'>When you trammel something or someone, you bring about limits to freedom or hinder movements or activities in some fashion.</p>
<p class='rw-defn idx24' style='display:none'>If something—such as power, influence, or feeling—wanes, it gradually becomes weaker or less important, often so much so that it eventually disappears.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>purview</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='purview#' id='pronounce-sound' path='audio/words/amy-purview'></a>
PUR-vyoo
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='purview#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='purview#' id='context-sound' path='audio/wordcontexts/brian-purview'></a>
My daughter wants me to cancel the study of algebra in the seventh grade, but I told her that&#8217;s beyond my power or <em>purview</em>.  Although I know that she was joking with her request, she does have the unrealistic idea that as a school board member my <em>purview</em> or scope extends beyond my council work to school curriculum.  In fact, the <em>purview</em> or authority of the board is frustratingly small; we are much more an advisory group than a governing body.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Which statement relates to a <em>purview</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Yes, it is within my power to accommodate your request. 
</li>
<li class='choice '>
<span class='result'></span>
I can&#8217;t help but admire this gorgeous vista—let&#8217;s take a picture.
</li>
<li class='choice '>
<span class='result'></span>
I need some help in this class, or there&#8217;ll be a failing grade in my future.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='purview#' id='definition-sound' path='audio/wordmeanings/amy-purview'></a>
Something that is within the <em>purview</em> of an individual or organization is within the range of its operation, authority, control, or concerns that it deals with.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>range of authority</em>
</span>
</span>
</div>
<a class='quick-help' href='purview#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='purview#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/purview/memory_hooks/3856.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>View</span></span> <span class="emp2"><span>Pur</span></span>ring</span></span> It is within the <span class="emp2"><span>pur</span></span><span class="emp3"><span>view</span></span> of each and every cat lover and owner to not only care for one's cat, but also to <span class="emp3"><span>view</span></span> it <span class="emp2"><span>pur</span></span>ring in <span class="emp2"><span>per</span></span>fect contentment.
</p>
</div>

<div id='memhook-button-bar'>
<a href="purview#" id="add-public-hook" style="" url="https://membean.com/mywords/purview/memory_hooks">Use other public hook</a>
<a href="purview#" id="memhook-use-own" url="https://membean.com/mywords/purview/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='purview#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
One happy story is that of Nepal, where poverty and isolation had left some 3.4m people, some ethnically non-Nepali, outside the <b>purview</b> of the state. This year, the government managed to register 2.6m of them.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Cisco Systems has sold China much of the equipment authorities use to block access to such sites, though the company maintains that China’s use of the gear is beyond its <b>purview</b>.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/purview/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='purview#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>pur-</td>
<td>
&rarr;
</td>
<td class='meaning'>before</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='view_see' data-tree-url='//cdn1.membean.com/public/data/treexml' href='purview#'>
<span class=''></span>
view
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>see, take care</td>
</tr>
</table>
<p>One&#8217;s <em>purview</em> is what one &#8220;sees before&#8221; one, and hence is able to control or &#8220;take care&#8221; of.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='purview#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep6" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Purview" src="https://cdn0.membean.com/public/images/wordimages/cons2/purview.png?qdep6" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='purview#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>acumen</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>adept</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>adroit</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>circumscribe</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>delineate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>discern</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>efficacy</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>emissary</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>gamut</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>hegemony</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>hierarchy</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>plenipotentiary</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>potentate</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abeyance</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>curtail</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>fetter</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>hapless</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>myopic</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>stricture</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>tether</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>trammel</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>wane</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="purview" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>purview</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="purview#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

