
<!DOCTYPE html>
<html>
<head>
<title>Word: evince | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>To accentuate something is to emphasize it or make it more noticeable.</p>
<p class='rw-defn idx1' style='display:none'>To <em>adumbrate</em> is to describe something incompletely or to suggest future events based on limited current knowledge.</p>
<p class='rw-defn idx2' style='display:none'>When you are apprised of something, you are given information about it.</p>
<p class='rw-defn idx3' style='display:none'>When you broach a subject, especially one that may be embarrassing or unpleasant, you mention it in order to begin a discussion about it.</p>
<p class='rw-defn idx4' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx5' style='display:none'>Something that is clandestine is hidden or kept a secret, often because it is illegal.</p>
<p class='rw-defn idx6' style='display:none'>Covert activities or situations are secret and hidden.</p>
<p class='rw-defn idx7' style='display:none'>When someone disinters a dead body, they dig it up; likewise, something disinterred is exposed or revealed to the public after having been hidden for some time.</p>
<p class='rw-defn idx8' style='display:none'>When people dissemble, they hide their real thoughts, feelings, or intentions.</p>
<p class='rw-defn idx9' style='display:none'>To divulge something is to reveal information that was once private or secret.</p>
<p class='rw-defn idx10' style='display:none'>Someone who is effusive expresses happiness, pleasure, admiration, praise, etc., in an extremely enthusiastic way.</p>
<p class='rw-defn idx11' style='display:none'>When you elicit a response from someone, you draw it forth or cause it to happen by something you say or do.</p>
<p class='rw-defn idx12' style='display:none'>If you elucidate something, you make it easier to understand by giving more relevant information.</p>
<p class='rw-defn idx13' style='display:none'>Empirical evidence or study is based on real experience or scientific experiments rather than on unproven theories.</p>
<p class='rw-defn idx14' style='display:none'>If you ensconce yourself somewhere, you put yourself into a comfortable place or safe position and have no intention of moving or leaving for quite some time.</p>
<p class='rw-defn idx15' style='display:none'>An evocation of something creates or summons a clear mental image or impression of it through words, pictures, or music.</p>
<p class='rw-defn idx16' style='display:none'>To explicate an idea or plan is to make it clear by explaining it.</p>
<p class='rw-defn idx17' style='display:none'>If you exude a quality or feeling, people easily notice that you have a large quantity of it because it flows from you; if a smell or liquid exudes from something, it flows steadily and slowly from it.</p>
<p class='rw-defn idx18' style='display:none'>If someone behaves in a furtive way, they do things sneakily and secretly in order to avoid being noticed.</p>
<p class='rw-defn idx19' style='display:none'>An immanent quality is present within something and is so much a part of it that the object cannot be imagined without that quality.</p>
<p class='rw-defn idx20' style='display:none'>People who are ingenuous are excessively trusting and believe almost everything that people tell them, especially because they have not had much life experience.</p>
<p class='rw-defn idx21' style='display:none'>Something that is latent, such as an undiscovered talent or undiagnosed disease, exists but is not active or has not developed at the moment—it may develop or become active in the future.</p>
<p class='rw-defn idx22' style='display:none'>To obfuscate something is to make it deliberately unclear, confusing, and difficult to understand.</p>
<p class='rw-defn idx23' style='display:none'>To obviate a problem is to eliminate it or do something that makes solving the problem unnecessary.</p>
<p class='rw-defn idx24' style='display:none'>Something is opaque if it is either difficult to understand or is not transparent.</p>
<p class='rw-defn idx25' style='display:none'>An overt act is not hidden or secret but is done in an open and public way.</p>
<p class='rw-defn idx26' style='display:none'>A state of quiescence is one of quiet and restful inaction.</p>
<p class='rw-defn idx27' style='display:none'>If you are stolid, you have or show little emotion about anything at all.</p>
<p class='rw-defn idx28' style='display:none'>If you employ subterfuge, you use a secret plan or action to get what you want by outwardly doing one thing that cleverly hides your true intentions.</p>
<p class='rw-defn idx29' style='display:none'>A surreptitious deed is done secretly to avoid bringing any attention to it.</p>
<p class='rw-defn idx30' style='display:none'>The veracity of something is its truthfulness.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>evince</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='evince#' id='pronounce-sound' path='audio/words/amy-evince'></a>
i-VINS
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='evince#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='evince#' id='context-sound' path='audio/wordcontexts/brian-evince'></a>
When I saw Alexandra for the first time after many years, my face <em>evinced</em> or displayed feelings of delight and wonder: my eyes lit up and I grinned widely.  Alexandra caught my delight, which was <em>evinced</em> or indicated by a slight blush in her clear complexion.  Soon a few tears of joy <em>evinced</em>, revealed, and made clear the fact that although many years had passed, our feelings of affection for one another had not changed.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How might you <em>evince</em> your sadness?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
By avoiding feeling sad by thinking of things for which you are grateful.
</li>
<li class='choice '>
<span class='result'></span>
By going off by yourself so others cannot see that you are distressed.
</li>
<li class='choice answer '>
<span class='result'></span>
By not being reluctant to cry in public.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='evince#' id='definition-sound' path='audio/wordmeanings/amy-evince'></a>
If you <em>evince</em> particular feelings, qualities, or attitudes, you show them, often clearly.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>show clearly</em>
</span>
</span>
</div>
<a class='quick-help' href='evince#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='evince#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/evince/memory_hooks/3931.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>E</span></span>va Con<span class="emp3"><span>vince</span></span>s</span></span> When I told <span class="emp2"><span>E</span></span>va that I enjoyed her company, her eyes caught fire and a killer grin lit up her face, easily con<span class="emp3"><span>vinc</span></span>ing me that <span class="emp2"><span>E</span></span>va found my company pleasurable as well, <span class="emp2"><span>e</span></span><span class="emp3"><span>vince</span></span>d by those obvious gestures, which, as I now recall, also included nodding her head.
</p>
</div>

<div id='memhook-button-bar'>
<a href="evince#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/evince/memory_hooks">Use other hook</a>
<a href="evince#" id="memhook-use-own" url="https://membean.com/mywords/evince/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='evince#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
If faced with two competitive candidates, every company will hire the person who <b>evinces</b> more enthusiasm.
<span class='attribution'>&mdash; Neil Blumenthal, American entrepreneur, known for being one of the originators of an innovative online retail model</span>
<img alt="Neil blumenthal, american entrepreneur, known for being one of the originators of an innovative online retail model" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Neil Blumenthal, American entrepreneur, known for being one of the originators of an innovative online retail model.jpg?qdep8" width="80" />
</li>
<li>
High energy costs <b>evince</b> strong global demand and boost the profits of oil and gas companies, while a weaker dollar benefits U.S. companies that draw revenue overseas.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Few records of any kind <b>evince</b> this much head-spinning joy, and coming on the heels of _Licensed to Ill,_ their self-consciously idiotic debut, it announced that the Beasties were an innovative force.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
<li>
“Extreme partisan gerrymandering—namely redistricting plans that entrench politicians in power, that <b>evince</b> a fundamental distrust of voters by serving the self-interest of political parties over the public good, and that dilute and devalue votes of some citizens compared to others—is contrary to the fundamental right of North Carolina citizens to have elections conducted freely and honestly to ascertain, fairly and truthfully, the will of the people,” the panel wrote.
<cite class='attribution'>
&mdash;
Huffington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/evince/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='evince#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='evince#'>
<span class=''></span>
e-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='vinc_conquer' data-tree-url='//cdn1.membean.com/public/data/treexml' href='evince#'>
<span class=''></span>
vinc
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>conquer, win</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='evince#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>When one <em>evinces</em> a particular attitude towards something, one is &#8220;thoroughly conquered or won over&#8221; by it because one gives a clear indication of one&#8217;s feelings towards it.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='evince#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Evince" src="https://cdn0.membean.com/public/images/wordimages/cons2/evince.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='evince#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>accentuate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>adumbrate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>apprise</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>broach</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>disinter</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>divulge</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>effusive</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>elicit</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>elucidate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>empirical</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>evocation</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>explicate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>exude</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>ingenuous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>overt</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='5' class = 'rw-wordform notlearned'><span>clandestine</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>covert</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>dissemble</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>ensconce</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>furtive</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>immanent</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>latent</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>obfuscate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>obviate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>opaque</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>quiescence</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>stolid</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>subterfuge</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>surreptitious</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="evince" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>evince</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="evince#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

