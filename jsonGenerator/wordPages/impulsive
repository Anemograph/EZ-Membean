
<!DOCTYPE html>
<html>
<head>
<title>Word: impulsive | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx1' style='display:none'>If you adjudicate a competition or dispute, you officially decide who is right or what should be done concerning any difficulties that may arise.</p>
<p class='rw-defn idx2' style='display:none'>If you describe a decision, rule, or plan as arbitrary, you think that it was decided without any thought, standard, or system to guide it; therefore, it can seem unfair.</p>
<p class='rw-defn idx3' style='display:none'>An assiduous person works hard to ensure that something is done properly and completely.</p>
<p class='rw-defn idx4' style='display:none'>Someone who is capricious changes their mind repeatedly or behaves in unexpected ways; a capricious series of events follows no predictable pattern.</p>
<p class='rw-defn idx5' style='display:none'>Cogitating about something is thinking deeply about it for a long time.</p>
<p class='rw-defn idx6' style='display:none'>If you deliberate, you think about or discuss something very carefully, especially before making an important decision.</p>
<p class='rw-defn idx7' style='display:none'>If you delineate something, such as an idea or situation or border, you describe it in great detail.</p>
<p class='rw-defn idx8' style='display:none'>Something that is desultory is done in a way that is unplanned, disorganized, and without direction.</p>
<p class='rw-defn idx9' style='display:none'>Erratic behavior is irregular, unpredictable, and unusual.</p>
<p class='rw-defn idx10' style='display:none'>Fidelity towards something, such as a marriage or promise, is faithfulness or loyalty towards it.</p>
<p class='rw-defn idx11' style='display:none'>A fiduciary relationship arises when one individual has the trust and confidence of another, such as a financial client; subsequently, that trustworthy person is entrusted with the management or protection of the client&#8217;s money.</p>
<p class='rw-defn idx12' style='display:none'>A fleeting moment lasts but a short time before it fades away.</p>
<p class='rw-defn idx13' style='display:none'>Things that fluctuate vary or change often, rising or falling seemingly at random.</p>
<p class='rw-defn idx14' style='display:none'>A foible is a small weakness or character flaw in a person that is considered somewhat unusual; that said, it is also viewed as unimportant and harmless.</p>
<p class='rw-defn idx15' style='display:none'>When you formulate a plan of action, you carefully work it out or design it in great detail ahead of time.</p>
<p class='rw-defn idx16' style='display:none'>Fortitude is the determination or lasting courage to endure hardship or difficulty over an extended period of time.</p>
<p class='rw-defn idx17' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is impetuous does things quickly and rashly without thinking carefully first.</p>
<p class='rw-defn idx19' style='display:none'>An impromptu speech is unplanned or spontaneous—it has not been practiced in any way beforehand.</p>
<p class='rw-defn idx20' style='display:none'>When someone improvises, they make something up at once because an unexpected situation has arisen.</p>
<p class='rw-defn idx21' style='display:none'>When you act in an imprudent fashion, you do something that is unwise, is lacking in good judgment, or has no forethought.</p>
<p class='rw-defn idx22' style='display:none'>Someone who is indiscreet shows lack of judgment, especially because they say or do things in public that should only be said or done privately—if at all.</p>
<p class='rw-defn idx23' style='display:none'>Someone who is irresolute is unable to decide what to do.</p>
<p class='rw-defn idx24' style='display:none'>A mercurial person&#8217;s mind or mood changes often, quickly, and unpredictably.</p>
<p class='rw-defn idx25' style='display:none'>Someone is considered meticulous when they act with careful attention to detail.</p>
<p class='rw-defn idx26' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx27' style='display:none'>Someone who is obdurate is stubborn, unreasonable, or uncontrollable; hence, they simply refuse to change their behavior, actions, or beliefs to fit any situation whatsoever.</p>
<p class='rw-defn idx28' style='display:none'>An obstinate person refuses to change their mind, even when other people think they are being highly unreasonable.</p>
<p class='rw-defn idx29' style='display:none'>If an object oscillates, it moves repeatedly from one point to another and then back again; if you oscillate between two moods or attitudes, you keep changing from one to the other.</p>
<p class='rw-defn idx30' style='display:none'>Something that is perennial lasts a very long time and is enduring.</p>
<p class='rw-defn idx31' style='display:none'>Someone who is pertinacious is determined to continue doing something rather than giving up—even when it gets very difficult.</p>
<p class='rw-defn idx32' style='display:none'>Something or someone that is protean is adaptable, variable, or changeable in nature.</p>
<p class='rw-defn idx33' style='display:none'>If you ruminate on something, you think carefully and deeply about it over a long period of time.</p>
<p class='rw-defn idx34' style='display:none'>A scrupulous person takes great care to do everything in an extremely honest, conscientious, and fair manner.</p>
<p class='rw-defn idx35' style='display:none'>Someone who is sedulous works hard and performs tasks very carefully and thoroughly, not stopping their work until it is completely accomplished.</p>
<p class='rw-defn idx36' style='display:none'>Spontaneity is freedom to act when and how you want to, often in an unpredictable or unplanned way.</p>
<p class='rw-defn idx37' style='display:none'>If you are stalwart, you are dependable, sturdy, and firm in what you do or promise.</p>
<p class='rw-defn idx38' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx39' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx40' style='display:none'>If you are temperamental, you tend to become easily upset and experience unpredictable mood swings.</p>
<p class='rw-defn idx41' style='display:none'>A tempestuous storm, temper, or crowd is violent, wild, and in an uproar.</p>
<p class='rw-defn idx42' style='display:none'>A tenacious person does not quit until they finish what they&#8217;ve started.</p>
<p class='rw-defn idx43' style='display:none'>Something that has the quality of transience lasts for only a short time or is constantly changing.</p>
<p class='rw-defn idx44' style='display:none'>Something that undulates moves or is shaped like waves with gentle curves that smoothly rise and fall.</p>
<p class='rw-defn idx45' style='display:none'>If you are unflagging while doing a task, you are untiring when working upon it and do not stop until it is finished.</p>
<p class='rw-defn idx46' style='display:none'>When someone vacillates, they go back and forth about a choice or opinion, unable to make a firm decision.</p>
<p class='rw-defn idx47' style='display:none'>A vagary is an unpredictable or unexpected change in action or behavior.</p>
<p class='rw-defn idx48' style='display:none'>When you are vigilant, you are keenly watchful, careful, or attentive to a task or situation.</p>
<p class='rw-defn idx49' style='display:none'>A wanton action deliberately harms someone or damages something for no apparent or good reason.</p>
<p class='rw-defn idx50' style='display:none'>If you waver, you cannot decide between two things because you have serious doubts about which choice is better.</p>
<p class='rw-defn idx51' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>impulsive</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='impulsive#' id='pronounce-sound' path='audio/words/amy-impulsive'></a>
im-PUHL-siv
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='impulsive#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='impulsive#' id='context-sound' path='audio/wordcontexts/brian-impulsive'></a>
Steve is so <em>impulsive</em> that he rarely thinks about anything before he acts.  One time he was heading to Florida for vacation, but <em>impulsively</em> or unpredictably changed his mind thirty miles from the border and headed off for Texas instead!  When asked how he can be so <em>impulsive</em> or unexpected in his behavior, he says that he just follows his gut and throws his reason out the window.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How does someone act if they are <em>impulsive</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They take a long time to make even simple decisions.
</li>
<li class='choice answer '>
<span class='result'></span>
They make choices without thinking through all the options first.
</li>
<li class='choice '>
<span class='result'></span>
They plan a lot of fun activities to do with family and friends.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='impulsive#' id='definition-sound' path='audio/wordmeanings/amy-impulsive'></a>
Someone who is <em>impulsive</em> tends to do things without thinking about them ahead of time; therefore, their actions can be unpredictable.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>acting without thinking beforehand</em>
</span>
</span>
</div>
<a class='quick-help' href='impulsive#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='impulsive#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/impulsive/memory_hooks/5005.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>On</span></span> <span class="emp1"><span>Im</span></span><span class="emp3"><span>pulse</span></span></span></span> If one acts in an <span class="emp1"><span>im</span></span><span class="emp3"><span>puls</span></span>ive fashion, one acts unexpectedly or <span class="emp1"><span>on</span></span> <span class="emp1"><span>im</span></span><span class="emp3"><span>puls</span></span>e.
</p>
</div>

<div id='memhook-button-bar'>
<a href="impulsive#" id="add-public-hook" style="" url="https://membean.com/mywords/impulsive/memory_hooks">Use other public hook</a>
<a href="impulsive#" id="memhook-use-own" url="https://membean.com/mywords/impulsive/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='impulsive#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Using rats, researchers have discovered that a specific circuit in the brain seems to drive <b>impulsive</b> eating.
<cite class='attribution'>
&mdash;
Medical News Today
</cite>
</li>
<li>
Turns out the students who had <b>impulsive</b> behavioral problems back in kindergarten were the same kids who were beginning to gamble by the sixth grade.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
The epidemic increase in obesity suggests that regulating <b>impulsive</b> purchases and consumption of unhealthy food products is a steep challenge for many consumers.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
<li>
If we are feeling anxious, for example, we use <b>impulsive</b> buying as a way to feel better.
<cite class='attribution'>
&mdash;
Psychology Today
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/impulsive/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='impulsive#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='im_in' data-tree-url='//cdn1.membean.com/public/data/treexml' href='impulsive#'>
<span class='common'></span>
im-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>against</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='puls_pushed' data-tree-url='//cdn1.membean.com/public/data/treexml' href='impulsive#'>
<span class=''></span>
puls
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>pushed</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ive_does' data-tree-url='//cdn1.membean.com/public/data/treexml' href='impulsive#'>
<span class=''></span>
-ive
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or that which does something</td>
</tr>
</table>
<p>When one has been <em>impulsive</em> in doing something, one has &#8220;pushed against&#8221; something without any forethought.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='impulsive#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: A Place of Our Own</strong><span> All about impulsive behavior in children.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/impulsive.jpg' video_url='examplevids/impulsive' video_width='350'></span>
<div id='wt-container'>
<img alt="Impulsive" height="288" src="https://cdn1.membean.com/video/examplevids/impulsive.jpg" width="350" />
<div class='center'>
<a href="impulsive#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='impulsive#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Impulsive" src="https://cdn0.membean.com/public/images/wordimages/cons2/impulsive.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='impulsive#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>arbitrary</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>capricious</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>desultory</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>erratic</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>fleeting</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>fluctuate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>foible</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>impetuous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>impromptu</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>improvise</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>imprudent</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>indiscreet</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>irresolute</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>mercurial</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>oscillate</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>protean</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>spontaneity</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>temperamental</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>tempestuous</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>transience</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>undulate</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>vacillate</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>vagary</span> &middot;</li><li data-idx='49' class = 'rw-wordform notlearned'><span>wanton</span> &middot;</li><li data-idx='50' class = 'rw-wordform notlearned'><span>waver</span> &middot;</li><li data-idx='51' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>adjudicate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>assiduous</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>cogitate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>deliberate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>delineate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>fidelity</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>fiduciary</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>formulate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>fortitude</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>meticulous</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>obdurate</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>obstinate</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>perennial</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>pertinacious</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>ruminate</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>scrupulous</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>sedulous</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>stalwart</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>tenacious</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>unflagging</span> &middot;</li><li data-idx='48' class = 'rw-wordform notlearned'><span>vigilant</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="impulsive" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>impulsive</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="impulsive#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

