
<!DOCTYPE html>
<html>
<head>
<title>Word: dupe | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Acuity is sharpness or clearness of vision, hearing, or thought.</p>
<p class='rw-defn idx1' style='display:none'>Acumen is the ability to make quick decisions and keen, precise judgments.</p>
<p class='rw-defn idx2' style='display:none'>When you employ artifice, you use clever tricks and cunning to deceive someone.</p>
<p class='rw-defn idx3' style='display:none'>When you describe someone as astute, you think they quickly understand situations or behavior and use it to their advantage.</p>
<p class='rw-defn idx4' style='display:none'>If someone beguiles you, you are charmed by and attracted to them; you can also be tricked or deceived by someone who beguiles you.</p>
<p class='rw-defn idx5' style='display:none'>If something bewilders you, you are very confused or puzzled by it.</p>
<p class='rw-defn idx6' style='display:none'>Someone who is callow is young, immature, and inexperienced; consequently, they possess little knowledge of the world.</p>
<p class='rw-defn idx7' style='display:none'>A canard is a piece of news or information that is false; it is deliberately spread either to harm someone or as a hoax.</p>
<p class='rw-defn idx8' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx9' style='display:none'>You describe someone as a charlatan if they pretend to have special knowledge or skill that they don&#8217;t actually possess.</p>
<p class='rw-defn idx10' style='display:none'>If you employ chicanery, you are devising and carrying out clever plans and trickery to cheat and deceive people.</p>
<p class='rw-defn idx11' style='display:none'>If you circumvent something, such as a rule or restriction, you try to get around it in a clever and perhaps dishonest way.</p>
<p class='rw-defn idx12' style='display:none'>A credulous person is very ready to believe what people tell them; therefore, they can be easily tricked or cheated.</p>
<p class='rw-defn idx13' style='display:none'>If you disabuse someone of an idea or notion, you persuade them that the idea is in fact untrue.</p>
<p class='rw-defn idx14' style='display:none'>When you discern something, you notice, detect, or understand it, often after thinking about it carefully or studying it for some time.  </p>
<p class='rw-defn idx15' style='display:none'>Someone who is disingenuous is not straightforward or is dishonest in what they say or do.</p>
<p class='rw-defn idx16' style='display:none'>When people dissemble, they hide their real thoughts, feelings, or intentions.</p>
<p class='rw-defn idx17' style='display:none'>To divulge something is to reveal information that was once private or secret.</p>
<p class='rw-defn idx18' style='display:none'>If you accuse someone of duplicity, you think that they are dishonest and are intending to trick you.</p>
<p class='rw-defn idx19' style='display:none'>A feint is the act of pretending to make a movement in one direction while actually moving in the other, especially to trick an opponent; a feint can also be a deceptive act meant to turn attention away from one&#8217;s true purpose.</p>
<p class='rw-defn idx20' style='display:none'>A gullible person is easy to trick because they are too trusting of other people.</p>
<p class='rw-defn idx21' style='display:none'>If an idea or thought is incisive, it is expressed in a penetrating and knowledgeable manner that is clear and brief; additionally, it can demonstrate impressive understanding of related ideas or thoughts.</p>
<p class='rw-defn idx22' style='display:none'>People who are ingenuous are excessively trusting and believe almost everything that people tell them, especially because they have not had much life experience.</p>
<p class='rw-defn idx23' style='display:none'>Legerdemain is the skillful use of one&#8217;s hands or the employment of another form of cleverness for the purpose of deceiving someone.</p>
<p class='rw-defn idx24' style='display:none'>A machination is a secretive plan or clever plot that is carefully designed to control events or people.</p>
<p class='rw-defn idx25' style='display:none'>A mendacious person does not tell the truth.</p>
<p class='rw-defn idx26' style='display:none'>A mountebank is a smooth-talking, dishonest person who tricks and cheats people.</p>
<p class='rw-defn idx27' style='display:none'>If someone is naive, they are too trusting of others; they don&#8217;t have enough experience in life to know whom to believe.</p>
<p class='rw-defn idx28' style='display:none'>Someone is obtuse when they are slow to understand things; someone can choose to be obtuse when they deliberately do not try to figure things out.</p>
<p class='rw-defn idx29' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx30' style='display:none'>A poseur pretends to have a certain quality or social position, usually because they want to influence others in some way.</p>
<p class='rw-defn idx31' style='display:none'>If you prevaricate, you avoid giving a direct or honest answer, usually because you want to hide the truth or want to delay or avoid making a hard decision.</p>
<p class='rw-defn idx32' style='display:none'>A ruse is a clever trick or deception used to fool someone.</p>
<p class='rw-defn idx33' style='display:none'>A sagacious person is wise, intelligent, and has the ability to make good practical decisions.</p>
<p class='rw-defn idx34' style='display:none'>Sophistry is the clever use of arguments that seem correct but are in fact unsound and misleading, used with the intent to deceive people.</p>
<p class='rw-defn idx35' style='display:none'>A stratagem is a clever trick or deception that is used to fool someone.</p>
<p class='rw-defn idx36' style='display:none'>If you employ subterfuge, you use a secret plan or action to get what you want by outwardly doing one thing that cleverly hides your true intentions.</p>
<p class='rw-defn idx37' style='display:none'>A surreptitious deed is done secretly to avoid bringing any attention to it.</p>
<p class='rw-defn idx38' style='display:none'>Wiles are clever tricks or cunning schemes that are used to persuade someone to do what you want.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>dupe</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='dupe#' id='pronounce-sound' path='audio/words/amy-dupe'></a>
doop
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='dupe#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='dupe#' id='context-sound' path='audio/wordcontexts/brian-dupe'></a>
I often receive stock newsletters in the mail that try to <em>dupe</em> or trick me into buying the stock of an unknown company.  These advertisements for penny stocks try to <em>dupe</em> or deceive unsuspecting investors into buying a given company&#8217;s stock, which artificially inflates the price.  Then, after enough investors have been <em>duped</em> or fooled into buying the stocks, making the price rise much too high, the owner of the newsletter sells his stake in the company for a very handsome profit, making the price for everyone else low once again.  This method of <em>duping</em> or cheating people is called a &#8220;pump and dump&#8221; scheme.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of someone&#8217;s <em>duping</em> another person?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A crook who convinces an art collector that a fake painting is real.
</li>
<li class='choice '>
<span class='result'></span>
A girl who talks her sister into sneaking out of the house with her.
</li>
<li class='choice '>
<span class='result'></span>
An athlete who breaks a world record that was set by another athlete.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='dupe#' id='definition-sound' path='audio/wordmeanings/amy-dupe'></a>
When you <em>dupe</em> another person, you trick them into believing something that is not true.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>fool</em>
</span>
</span>
</div>
<a class='quick-help' href='dupe#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='dupe#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/dupe/memory_hooks/5515.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>D</span></span>oubtful R<span class="emp2"><span>upe</span></span>es</span></span> I was <span class="emp1"><span>d</span></span><span class="emp2"><span>upe</span></span>d into buying <span class="emp1"><span>d</span></span>oubtful r<span class="emp2"><span>upe</span></span>es which I found out later were counterfeit.
</p>
</div>

<div id='memhook-button-bar'>
<a href="dupe#" id="add-public-hook" style="" url="https://membean.com/mywords/dupe/memory_hooks">Use other public hook</a>
<a href="dupe#" id="memhook-use-own" url="https://membean.com/mywords/dupe/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='dupe#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
International law enforcement organizations snared alleged criminals around the world after duping them into using phones loaded with an encrypted messaging app controlled by the FBI.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Customers for several VPN apps, which allegedly protect users’ data, complained in Apple App Store reviews that the apps told users their devices [had] been infected by a virus to <b>dupe</b> them into downloading and paying for software they [didn’t] need.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/dupe/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='dupe#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p><em>Dupe</em> comes from a root word meaning <em>hoopoe</em>.  A <em>hoopoe</em> is a ridiculous looking bird, and so often was made fun of, and thus became the target of &#8220;tricks.&#8221;</p>
<a href="dupe#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> French</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='dupe#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Fool Us (Penn and Teller)</strong><span> They've been duped.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/dupe.jpg' video_url='examplevids/dupe' video_width='350'></span>
<div id='wt-container'>
<img alt="Dupe" height="288" src="https://cdn1.membean.com/video/examplevids/dupe.jpg" width="350" />
<div class='center'>
<a href="dupe#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='dupe#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Dupe" src="https://cdn3.membean.com/public/images/wordimages/cons2/dupe.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='dupe#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>artifice</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>beguile</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>bewilder</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>callow</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>canard</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>charlatan</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>chicanery</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>circumvent</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>credulous</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>disingenuous</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>dissemble</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>duplicity</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>feint</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>gullible</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>ingenuous</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>legerdemain</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>machination</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>mendacious</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>mountebank</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>naive</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>obtuse</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>poseur</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>prevaricate</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>ruse</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>sophistry</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>stratagem</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>subterfuge</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>surreptitious</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>wile</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>acuity</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>acumen</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>astute</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>disabuse</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>discern</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>divulge</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>incisive</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>sagacious</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='dupe#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
dupe
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>one who is easily deceived by trickery</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="dupe" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>dupe</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="dupe#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

