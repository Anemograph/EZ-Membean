
<!DOCTYPE html>
<html>
<head>
<title>Word: genre | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Genre-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/genre-large.jpg?qdep8" />
</div>
<a href="genre#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>Something that is amorphous has no clear shape, boundaries, or structure.</p>
<p class='rw-defn idx1' style='display:none'>An appellation is a name or title given to a person, place, or thing.</p>
<p class='rw-defn idx2' style='display:none'>An archetype is a perfect or typical example of something because it has the most important qualities that belong to that type of thing; it can also describe essential qualities common to a particular class of things.</p>
<p class='rw-defn idx3' style='display:none'>If you delineate something, such as an idea or situation or border, you describe it in great detail.</p>
<p class='rw-defn idx4' style='display:none'>Empirical evidence or study is based on real experience or scientific experiments rather than on unproven theories.</p>
<p class='rw-defn idx5' style='display:none'>If you describe something as nebulous, you mean that it is unclear, vague, and not clearly defined; a shape that is nebulous has no clear boundaries.</p>
<p class='rw-defn idx6' style='display:none'>Nomenclature is a specialized form of vocabulary that classifies or organizes things in the sciences or the arts into a clear and usable system.</p>
<p class='rw-defn idx7' style='display:none'>Something is nondescript when its appearance is ordinary, dull, and not at all interesting or attractive.</p>
<p class='rw-defn idx8' style='display:none'>Taxonomy is the science of classifying plants and animals into an organized system.</p>
<p class='rw-defn idx9' style='display:none'>Tropes are expressions, phrases, or words that writers use as a clever method for expressing everyday ideas in a nonliteral and creative way.</p>
<p class='rw-defn idx10' style='display:none'>Verbiage is an excessive use of words to convey something that could be expressed using fewer words; it can also be the manner or style in which someone uses words.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>genre</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='genre#' id='pronounce-sound' path='audio/words/amy-genre'></a>
ZHAHN-ruh
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='genre#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='genre#' id='context-sound' path='audio/wordcontexts/brian-genre'></a>
The horror film is a <em>genre</em> or category of movie that has a huge following.  What people find so appealing about this <em>genre</em> or type of film is the scare factor.  Many know the scenes in this <em>genre</em> or kind of film are fake, but they love the thrill of being scared anyway.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>genre</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A meal with many courses.
</li>
<li class='choice '>
<span class='result'></span>
A list of items in a store.
</li>
<li class='choice answer '>
<span class='result'></span>
A category of films.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='genre#' id='definition-sound' path='audio/wordmeanings/amy-genre'></a>
A <em>genre</em> is a category, type, or class of artistic work.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>category</em>
</span>
</span>
</div>
<a class='quick-help' href='genre#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='genre#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/genre/memory_hooks/4164.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>G</span></span>ypsy <span class="emp1"><span>J</span></span>azz <span class="emp1"><span>G</span></span>enre by <span class="emp1"><span>Dj</span></span>ango</span></span> <span class="emp1"><span>G</span></span>ypsy <span class="emp1"><span>j</span></span>azz is a popular music <span class="emp1"><span>g</span></span>enre whose best known star was <span class="emp1"><span>Dj</span></span>ango Reinhardt.
</p>
</div>

<div id='memhook-button-bar'>
<a href="genre#" id="add-public-hook" style="" url="https://membean.com/mywords/genre/memory_hooks">Use other public hook</a>
<a href="genre#" id="memhook-use-own" url="https://membean.com/mywords/genre/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='genre#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
At the same time, the qualities that draw many K-pop fans to the <b>genre</b> also tend to align with more progressive causes.
<cite class='attribution'>
&mdash;
TIME Magazine
</cite>
</li>
<li>
But there's another <b>genre</b> that Swift borrows techniques from, which is perhaps equally responsible for her sound. And that <b>genre</b> is hip-hop.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
They talk about what inspired Fried to start following the group around with a camera all those years ago, the trust needed to support taking creative risks and the quandary of using hip-hop, a <b>genre</b> rooted in giving a voice to the disenfranchised, as a mode of artist expression once you've become successful.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Once our own real-life pandemic took full hold, I found myself unable to read more than five pages of any other <b>genre</b>.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/genre/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='genre#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='gen_kind' data-tree-url='//cdn1.membean.com/public/data/treexml' href='genre#'>
<span class=''></span>
gen
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>kind, type, class</td>
</tr>
</table>
<p>A <em>genre</em> is a &#8220;kind, type, or class&#8221; of literature.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='genre#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Molding Minds</strong><span> Talking about different book genres.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/genre.jpg' video_url='examplevids/genre' video_width='350'></span>
<div id='wt-container'>
<img alt="Genre" height="288" src="https://cdn1.membean.com/video/examplevids/genre.jpg" width="350" />
<div class='center'>
<a href="genre#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='genre#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Genre" src="https://cdn2.membean.com/public/images/wordimages/cons2/genre.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='genre#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>appellation</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>archetype</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>delineate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>empirical</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>nomenclature</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>taxonomy</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>trope</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>verbiage</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>amorphous</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>nebulous</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>nondescript</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="genre" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>genre</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="genre#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

