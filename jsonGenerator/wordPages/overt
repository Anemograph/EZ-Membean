
<!DOCTYPE html>
<html>
<head>
<title>Word: overt | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>You can describe something as abstruse if you find it highly complicated and difficult to understand.</p>
<p class='rw-defn idx1' style='display:none'>If you describe something as ambiguous, you mean that it is unclear or confusing because it can be understood in multiple ways.</p>
<p class='rw-defn idx2' style='display:none'>Something that is amorphous has no clear shape, boundaries, or structure.</p>
<p class='rw-defn idx3' style='display:none'>Something that is arcane is mysterious and secret, known and understood by only a few people.</p>
<p class='rw-defn idx4' style='display:none'>If you describe a person&#8217;s behavior as brazen, you mean that they are not embarrassed by anything they do—they simply don&#8217;t care what other people think about them.</p>
<p class='rw-defn idx5' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx6' style='display:none'>Something that is clandestine is hidden or kept a secret, often because it is illegal.</p>
<p class='rw-defn idx7' style='display:none'>A clarion call is a stirring, emotional, and strong appeal to people to take action on something.</p>
<p class='rw-defn idx8' style='display:none'>To cloister someone is to remove and isolate them from the rest of the world.</p>
<p class='rw-defn idx9' style='display:none'>Covert activities or situations are secret and hidden.</p>
<p class='rw-defn idx10' style='display:none'>If you describe someone, usually a young woman, as demure, you mean that she is quiet, shy, and always behaves modestly.</p>
<p class='rw-defn idx11' style='display:none'>To divulge something is to reveal information that was once private or secret.</p>
<p class='rw-defn idx12' style='display:none'>Someone or something that is enigmatic is mysterious and difficult to understand.</p>
<p class='rw-defn idx13' style='display:none'>When people equivocate, they avoid making a clear statement; they are deliberately vague in order to avoid giving a direct answer to a question.</p>
<p class='rw-defn idx14' style='display:none'>Something esoteric is known about or understood by very few people.</p>
<p class='rw-defn idx15' style='display:none'>If you exude a quality or feeling, people easily notice that you have a large quantity of it because it flows from you; if a smell or liquid exudes from something, it flows steadily and slowly from it.</p>
<p class='rw-defn idx16' style='display:none'>An action that is flagrant shows that someone does not care if they obviously break the rules or highly offend people.</p>
<p class='rw-defn idx17' style='display:none'>If someone behaves in a furtive way, they do things sneakily and secretly in order to avoid being noticed.</p>
<p class='rw-defn idx18' style='display:none'>Something that is imperceptible is either impossible to be perceived by the mind or is very difficult to perceive.</p>
<p class='rw-defn idx19' style='display:none'>When something is implicit, it is understood without having to say it.</p>
<p class='rw-defn idx20' style='display:none'>Something that is inconspicuous does not attract attention and is not easily seen or noticed because it is small or ordinary.</p>
<p class='rw-defn idx21' style='display:none'>People who are ingenuous are excessively trusting and believe almost everything that people tell them, especially because they have not had much life experience.</p>
<p class='rw-defn idx22' style='display:none'>Something that inhibits you from doing something restricts or keeps you from doing it.</p>
<p class='rw-defn idx23' style='display:none'>Something that is latent, such as an undiscovered talent or undiagnosed disease, exists but is not active or has not developed at the moment—it may develop or become active in the future.</p>
<p class='rw-defn idx24' style='display:none'>If someone is lucid, they are able to understand things and think clearly; this adjective also describes writing or speech that is crystal clear and easy to understand.</p>
<p class='rw-defn idx25' style='display:none'>When something is luminous, it is bright and glowing.</p>
<p class='rw-defn idx26' style='display:none'>If you describe something as nebulous, you mean that it is unclear, vague, and not clearly defined; a shape that is nebulous has no clear boundaries.</p>
<p class='rw-defn idx27' style='display:none'>If you are being noncommittal on an issue, you are not revealing what your opinion is and are being reserved on purpose.</p>
<p class='rw-defn idx28' style='display:none'>An oblique statement is not straightforward or direct; rather, it is purposely roundabout, vague, or misleading.</p>
<p class='rw-defn idx29' style='display:none'>Something is opaque if it is either difficult to understand or is not transparent.</p>
<p class='rw-defn idx30' style='display:none'>If a mood or feeling is palpable, it is so strong and intense that it is easily noticed and is almost able to be physically felt.</p>
<p class='rw-defn idx31' style='display:none'>Recondite areas of knowledge are those that are very difficult to understand and/or are not known by many people.</p>
<p class='rw-defn idx32' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx33' style='display:none'>A subtle point is so clever or small that it is hard to notice or understand; it can also be very wise or deep in meaning.</p>
<p class='rw-defn idx34' style='display:none'>A surreptitious deed is done secretly to avoid bringing any attention to it.</p>
<p class='rw-defn idx35' style='display:none'>A tacit agreement between two people is understood without having to use words to express it.</p>
<p class='rw-defn idx36' style='display:none'>Something that is tangible is able to be touched and thus is considered real.</p>
<p class='rw-defn idx37' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>overt</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='overt#' id='pronounce-sound' path='audio/words/amy-overt'></a>
oh-VURT
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='overt#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='overt#' id='context-sound' path='audio/wordcontexts/brian-overt'></a>
It used to be that politicians didn&#8217;t make <em>overt</em>, public statements about their personal beliefs.  Perhaps they didn&#8217;t like to be open or <em>overt</em> about anything, especially their religious leanings.  When Jimmy Carter visibly or <em>overtly</em> announced in 1976 that he was a born-again Christian, the public and press alike were surprised by his unconcealed telling of once closely kept information.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When would you want an <em>overt</em> apology from someone?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
When you want that person to apologize in public.
</li>
<li class='choice '>
<span class='result'></span>
When you want that person to apologize to you in a private space.
</li>
<li class='choice '>
<span class='result'></span>
When you want that person to apologize immediately.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='overt#' id='definition-sound' path='audio/wordmeanings/amy-overt'></a>
An <em>overt</em> act is not hidden or secret but is done in an open and public way.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>obvious</em>
</span>
</span>
</div>
<a class='quick-help' href='overt#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='overt#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/overt/memory_hooks/2898.json'></span>
<p>
<span class="emp0"><span>Not <span class="emp1"><span>C</span></span><span class="emp3"><span>overt</span></span> or Se<span class="emp1"><span>c</span></span><span class="emp3"><span>ret</span></span></span></span> An <span class="emp3"><span>overt</span></span> act is not <span class="emp1"><span>c</span></span><span class="emp3"><span>overt</span></span>, or se<span class="emp1"><span>c</span></span><span class="emp3"><span>ret</span></span>, but is visible to all.
</p>
</div>

<div id='memhook-button-bar'>
<a href="overt#" id="add-public-hook" style="" url="https://membean.com/mywords/overt/memory_hooks">Use other public hook</a>
<a href="overt#" id="memhook-use-own" url="https://membean.com/mywords/overt/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='overt#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
For much of the history of the USDA, Black, Hispanic, Native American, Asian American and other minority farmers have faced discrimination—sometimes <b>overt</b> and sometimes through deeply embedded rules and policies—that have prevented them from achieving as much as their counterparts who do not face these documented acts of discrimination.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
Stanford political scientist David Brady said that in terms of <b>overt</b> corruption, this era does not compare to the gilded age of "robber barons" in the 19th century. Simon Cameron, a senator from Pennsylvania at that time, said of his colleagues: "An honest politician is one who, when he is bought, will stay bought."
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Indonesians seeking a more <b>overt</b> expression of their faith, as many do nowadays, can still believe in separation of mosque and state.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/overt/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='overt#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='overt_opened' data-tree-url='//cdn1.membean.com/public/data/treexml' href='overt#'>
<span class=''></span>
overt
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>opened</td>
</tr>
</table>
<p>An <em>overt</em> act has been &#8220;opened&#8221; for all to view.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='overt#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Harold and Maude</strong><span> Maude is overt about not having a license and stealing the car ... and the tree.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/overt.jpg' video_url='examplevids/overt' video_width='350'></span>
<div id='wt-container'>
<img alt="Overt" height="288" src="https://cdn1.membean.com/video/examplevids/overt.jpg" width="350" />
<div class='center'>
<a href="overt#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='overt#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Overt" src="https://cdn2.membean.com/public/images/wordimages/cons2/overt.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='overt#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='4' class = 'rw-wordform notlearned'><span>brazen</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>clarion</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>divulge</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>exude</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>flagrant</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>ingenuous</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>lucid</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>luminous</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>palpable</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>tangible</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abstruse</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>ambiguous</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>amorphous</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>arcane</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>clandestine</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>cloister</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>covert</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>demure</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>enigmatic</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>equivocate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>esoteric</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>furtive</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>imperceptible</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>implicit</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>inconspicuous</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>inhibit</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>latent</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>nebulous</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>noncommittal</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>oblique</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>opaque</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>recondite</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>subtle</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>surreptitious</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>tacit</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="overt" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>overt</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="overt#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

