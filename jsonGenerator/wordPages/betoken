
<!DOCTYPE html>
<html>
<head>
<title>Word: betoken | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Augury is the process or art of reading omens about the future through specific ceremonies.</p>
<p class='rw-defn idx1' style='display:none'>If a word or behavior connotes something, it suggests an additional idea or emotion that is not part of its original literal meaning.</p>
<p class='rw-defn idx2' style='display:none'>When you decipher a message or piece of writing, you work out what it says, even though it is very difficult to read or understand.</p>
<p class='rw-defn idx3' style='display:none'>If you delineate something, such as an idea or situation or border, you describe it in great detail.</p>
<p class='rw-defn idx4' style='display:none'>To divulge something is to reveal information that was once private or secret.</p>
<p class='rw-defn idx5' style='display:none'>If you elucidate something, you make it easier to understand by giving more relevant information.</p>
<p class='rw-defn idx6' style='display:none'>When you envisage something, you imagine or consider its future possibility.</p>
<p class='rw-defn idx7' style='display:none'>If you evince particular feelings, qualities, or attitudes, you show them, often clearly.</p>
<p class='rw-defn idx8' style='display:none'>An evocation of something creates or summons a clear mental image or impression of it through words, pictures, or music.</p>
<p class='rw-defn idx9' style='display:none'>To explicate an idea or plan is to make it clear by explaining it.</p>
<p class='rw-defn idx10' style='display:none'>If you fathom something complicated or mysterious, you are able to understand it.</p>
<p class='rw-defn idx11' style='display:none'>When something is implicit, it is understood without having to say it.</p>
<p class='rw-defn idx12' style='display:none'>Inductive reasoning involves the observation of data to arrive at a general conclusion or principle.</p>
<p class='rw-defn idx13' style='display:none'>An inference is a conclusion that is reached using available data.</p>
<p class='rw-defn idx14' style='display:none'>If someone makes an insinuation, they say something bad or unpleasant in a sly and indirect way.</p>
<p class='rw-defn idx15' style='display:none'>To prognosticate is to predict or forecast something.</p>
<p class='rw-defn idx16' style='display:none'>If something is redolent of something else, it has features that make you think of it; this word also refers to a particular odor or scent that can be pleasantly strong.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 6
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>betoken</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='betoken#' id='pronounce-sound' path='audio/words/amy-betoken'></a>
bih-TOH-kuhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='betoken#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='betoken#' id='context-sound' path='audio/wordcontexts/brian-betoken'></a>
The fact that Jamie can do very complex math problems in his head <em>betokens</em> or shows his great mathematical aptitude.  The fact that Jamie also gets a 100% on each math test he takes <em>betokens</em> or indicates his superior mathematical talent.  That mathematical ability also <em>betokens</em> or foretells a probable career in math, perhaps in teaching it.  The fact that he is already two years ahead in math also <em>betokens</em> or suggests that possible career in math, or at least a profession that relies on it heavily.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does something do if it betokens it?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
It shows that it is or will be.
</li>
<li class='choice '>
<span class='result'></span>
It states its probable reality.
</li>
<li class='choice '>
<span class='result'></span>
It denies its existence.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='betoken#' id='definition-sound' path='audio/wordmeanings/amy-betoken'></a>
If something <em>betokens</em> something else, it shows that it either exists now or will exist.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>indicate</em>
</span>
</span>
</div>
<a class='quick-help' href='betoken#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='betoken#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/betoken/memory_hooks/116565.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Be</span></span> a <span class="emp3"><span>Token</span></span></span></span> The black clothing that all were wearing <span class="emp1"><span>be</span></span><span class="emp3"><span>token</span></span>ed the fact of someone's death; black clothing can <span class="emp1"><span>be</span></span> a <span class="emp3"><span>token</span></span> of or <span class="emp1"><span>be</span></span><span class="emp3"><span>token</span></span> death!
</p>
</div>

<div id='memhook-button-bar'>
<a href="betoken#" id="add-public-hook" style="" url="https://membean.com/mywords/betoken/memory_hooks">Use other public hook</a>
<a href="betoken#" id="memhook-use-own" url="https://membean.com/mywords/betoken/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='betoken#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Outside, several laborers were repairing the street, among them a tall, dignified old man of [seventy-five] named Lawrence Smith, whose white mane and long white mustache seemed to <b>betoken</b> a distinguished past.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
<li>
The economy is improving—yes, Virginia, an increase in jobs, a rising stock market and brisker housing sales all <b>betoken</b> recovery—but American cities are still staggering in the aftermath of the Great Recession.
<cite class='attribution'>
&mdash;
MinnPost
</cite>
</li>
<li>
. . . the film's director, Shekhar Kapur, suffers from an advanced case of restless camera syndrome. Tracking shots, twisting boom shots, placements that are either radically high or low—they all <b>betoken</b> a director who doesn't trust his material.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/betoken/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='betoken#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='be_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='betoken#'>
<span class=''></span>
be-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td class='partform'>token</td>
<td>
&rarr;
</td>
<td class='meaning'>sign, evidence, indication</td>
</tr>
</table>
<p>If something <em>betokens</em> something else, it shows &#8220;thorough evidence&#8221; of its present state or a &#8220;thorough sign&#8221; of its coming into being.</p>
<a href="betoken#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> Old English</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='betoken#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Betoken" src="https://cdn2.membean.com/public/images/wordimages/cons2/betoken.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='betoken#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>augury</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>connote</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>decipher</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>delineate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>divulge</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>elucidate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>envisage</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>evince</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>evocation</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>explicate</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>fathom</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>implicit</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>inductive</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>inference</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>insinuation</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>prognosticate</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>redolent</span> &middot;</li></ul>
<ul class='related-ants'></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="betoken" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>betoken</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="betoken#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

