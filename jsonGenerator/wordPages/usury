
<!DOCTYPE html>
<html>
<head>
<title>Word: usury | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Usury-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/usury-large.jpg?qdep8" />
</div>
<a href="usury#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>If you appropriate something that does not belong to you, you take it for yourself without the right to do so.</p>
<p class='rw-defn idx1' style='display:none'>Avarice is the extremely strong desire to have a lot of money and possessions.</p>
<p class='rw-defn idx2' style='display:none'>When something bedevils you, it causes you a lot of problems and difficulties over a period of time.</p>
<p class='rw-defn idx3' style='display:none'>When you beleaguer someone, you act with the intent to annoy or harass that person repeatedly until they finally give you what you want.</p>
<p class='rw-defn idx4' style='display:none'>If you are beset by something, you are experiencing serious problems or dangers because of it.</p>
<p class='rw-defn idx5' style='display:none'>If you describe a person&#8217;s behavior as brazen, you mean that they are not embarrassed by anything they do—they simply don&#8217;t care what other people think about them.</p>
<p class='rw-defn idx6' style='display:none'>You coerce people when you force them to do something that they don&#8217;t want to do.</p>
<p class='rw-defn idx7' style='display:none'>Depredation can be the act of destroying or robbing a village—or the overall damage that time can do to things held dear.</p>
<p class='rw-defn idx8' style='display:none'>When you dragoon someone into doing something, you force or compel them to do it.</p>
<p class='rw-defn idx9' style='display:none'>Duress is the application or threat of force to compel someone to act in a particular way.</p>
<p class='rw-defn idx10' style='display:none'>If you refer to the enormity of a situation, problem, or event, you mean that it is very evil or morally offensive; this word has nothing to do with physical size.</p>
<p class='rw-defn idx11' style='display:none'>An exorbitant price or fee is much higher than what it should be or what is considered reasonable.</p>
<p class='rw-defn idx12' style='display:none'>If you are oppressed by someone or something, you are beaten down, troubled, or burdened by them or it.</p>
<p class='rw-defn idx13' style='display:none'>If soldiers pillage a place, such as a town or museum, they steal a lot of things and damage it using violent methods.</p>
<p class='rw-defn idx14' style='display:none'>When you plunder, you forcefully take or rob others of their possessions, usually during times of war, natural disaster, or other unrest.</p>
<p class='rw-defn idx15' style='display:none'>When you procure something, you obtain or get it in some fashion.</p>
<p class='rw-defn idx16' style='display:none'>When you proffer something to someone, you offer it up or hold it out to them to take.</p>
<p class='rw-defn idx17' style='display:none'>To ransack a place is to thoroughly loot or rob items from it; it can also mean to look through something thoroughly by examining every bit of it.</p>
<p class='rw-defn idx18' style='display:none'>If someone subjugates a group of people or country, they conquer and bring it under control by force.</p>
<p class='rw-defn idx19' style='display:none'>When you trammel something or someone, you bring about limits to freedom or hinder movements or activities in some fashion.</p>
<p class='rw-defn idx20' style='display:none'>If you say someone&#8217;s behavior is unseemly, you disapprove of it because it is not in good taste or not suitable for a particular situation.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 6
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>usury</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='usury#' id='pronounce-sound' path='audio/words/amy-usury'></a>
YOO-zhuh-ree
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='usury#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='usury#' id='context-sound' path='audio/wordcontexts/brian-usury'></a>
Some businesses that offer loans are notorious for practicing <em>usury</em> or charging excessively for interest on those loans.  One of the worst forms of <em>usury</em> is the borrowing of cash for a short period of time right before payday, for which privilege borrowers are saddled with exorbitant interest charges.  For example, one business that offers payday loans in Texas practices <em>usury</em> in a stunning way: borrow $100, and pay an extreme annual percentage rate of 664%!  Another Texas company who also practices <em>usury</em> in the form of excessively overcharging on interest will let you borrow $1000, but you will pay an astounding 682% <span class="caps">APR</span> for the life of the loan!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is <em>usury</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is a very convenient form of borrowing a micro-loan on a short-term basis.
</li>
<li class='choice '>
<span class='result'></span>
It is a refusal to loan money to a borrower because their ability to repay is in doubt.
</li>
<li class='choice answer '>
<span class='result'></span>
It is a demand of an unreasonable amount of extra money to be repaid by a borrower.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='usury#' id='definition-sound' path='audio/wordmeanings/amy-usury'></a>
<em>Usury</em> is the practice of lending money at an exorbitant rate of interest.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>moneylending</em>
</span>
</span>
</div>
<a class='quick-help' href='usury#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='usury#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/usury/memory_hooks/116702.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Us</span></span>e and B<span class="emp2"><span>ury</span></span></span></span> <span class="emp1"><span>Us</span></span><span class="emp2"><span>ury</span></span> <span class="emp1"><span>us</span></span>es an unwary borrower, and will b<span class="emp2"><span>ury</span></span> her financially.
</p>
</div>

<div id='memhook-button-bar'>
<a href="usury#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/usury/memory_hooks">Use other hook</a>
<a href="usury#" id="memhook-use-own" url="https://membean.com/mywords/usury/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='usury#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
For at least another hundred years we must pretend to ourselves and to every one that fair is foul and foul is fair; for foul is useful and fair is not. Avarice and <b>usury</b> and precaution must be our gods for a little longer still.
<span class='attribution'>&mdash; John Maynard Keynes, twentieth-century British economist</span>
<img alt="John maynard keynes, twentieth-century british economist" height="80" src="https://cdn3.membean.com/public/images/wordimages/quotes/John Maynard Keynes, twentieth-century British economist.jpg?qdep8" width="80" />
</li>
<li>
In West Virginia he's charged with breaking <b>usury</b> laws, assessing annual interest as high as, yes, 99 percent.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/usury/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='usury#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='us_use' data-tree-url='//cdn1.membean.com/public/data/treexml' href='usury#'>
<span class=''></span>
us
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>use</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='y_activity' data-tree-url='//cdn1.membean.com/public/data/treexml' href='usury#'>
<span class=''></span>
-y
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>activity</td>
</tr>
</table>
<p><em>Usury</em> is the &#8220;using&#8221; of money to lend while charging excessive interest rates, a good &#8220;use&#8221; of money lent, at least for the lenders!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='usury#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Usury" src="https://cdn0.membean.com/public/images/wordimages/cons2/usury.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='usury#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>appropriate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>avarice</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bedevil</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>beleaguer</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>beset</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>brazen</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>coerce</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>depredation</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>dragoon</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>duress</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>enormity</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>exorbitant</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>oppress</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>pillage</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>plunder</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>procure</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>proffer</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>ransack</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>subjugate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>trammel</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>unseemly</span> &middot;</li></ul>
<ul class='related-ants'></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='usury#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
usurious
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>relating to usury</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="usury" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>usury</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="usury#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

