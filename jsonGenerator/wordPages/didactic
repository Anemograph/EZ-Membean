
<!DOCTYPE html>
<html>
<head>
<title>Word: didactic | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you describe a decision, rule, or plan as arbitrary, you think that it was decided without any thought, standard, or system to guide it; therefore, it can seem unfair.</p>
<p class='rw-defn idx1' style='display:none'>If you describe someone as articulate, you mean that they are able to express their thoughts, arguments, and ideas clearly and effectively.</p>
<p class='rw-defn idx2' style='display:none'>Something that is desultory is done in a way that is unplanned, disorganized, and without direction.</p>
<p class='rw-defn idx3' style='display:none'>If something is done for someone&#8217;s edification, it is done to benefit that person by teaching them something that improves or enlightens their knowledge or character.</p>
<p class='rw-defn idx4' style='display:none'>Empirical evidence or study is based on real experience or scientific experiments rather than on unproven theories.</p>
<p class='rw-defn idx5' style='display:none'>Something fortuitous happens purely by chance and produces a successful or pleasant result.</p>
<p class='rw-defn idx6' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx7' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx8' style='display:none'>A parable is a short story that usually is told to illustrate a moral or lesson of some kind.</p>
<p class='rw-defn idx9' style='display:none'>The art of pedagogy is the methods used by a teacher to teach a subject.</p>
<p class='rw-defn idx10' style='display:none'>If someone is pedantic, they give too much importance to unimportant details and formal rules.</p>
<p class='rw-defn idx11' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx12' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx13' style='display:none'>Rhetoric is the skill or art of using language to persuade or influence people, especially language that sounds impressive but may not be sincere or honest.</p>
<p class='rw-defn idx14' style='display:none'>A rubric is a set of instructions at the beginning of a document, such as an examination or term paper, that is usually printed in a different style so as to highlight its importance.</p>
<p class='rw-defn idx15' style='display:none'>If someone is sententious, they are terse or brief in writing and speech; they also often use proverbs to appear morally upright and wise.</p>
<p class='rw-defn idx16' style='display:none'>Something statutory, such as the power given to a governor or president, is created, established, and controlled by rules and laws; hence, it has the full force of the law behind it and must be followed.</p>
<p class='rw-defn idx17' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>didactic</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='didactic#' id='pronounce-sound' path='audio/words/amy-didactic'></a>
dahy-DAK-tik
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='didactic#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='didactic#' id='context-sound' path='audio/wordcontexts/brian-didactic'></a>
As Karen Clove the clever storyteller shared her educational and <em>didactic</em> tales, the children were so caught up in the action that they never suspected they were learning anything.  If the children had known that Miss Clove&#8217;s stories were brimming with <em>didactic</em>, moral messages, they might have tuned her out.  As it was, Karen caught their attention with humor and slipped in the thoughtful, <em>didactic</em> teachings that were the center of her art.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is the purpose of a <em>didactic</em> piece of literature?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
To create a piece of fiction that is based on a period in history.
</li>
<li class='choice answer '>
<span class='result'></span>
To provide instruction of some kind for the readers thereof.
</li>
<li class='choice '>
<span class='result'></span>
To make fun of some aspect of society or politics.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='didactic#' id='definition-sound' path='audio/wordmeanings/amy-didactic'></a>
<em>Didactic</em> speech or writing is intended to teach something, especially a moral lesson.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>educational</em>
</span>
</span>
</div>
<a class='quick-help' href='didactic#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='didactic#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/didactic/memory_hooks/4513.json'></span>
<p>
<img alt="Didactic" src="https://cdn0.membean.com/public/images/wordimages/hook/didactic.jpg?qdep8" />
<span class="emp0"><span>"<span class="emp1"><span>Did</span></span>-<span class="emp2"><span>act</span></span>-<span class="emp3"><span>it</span></span>" Professor</span></span> Our <span class="emp1"><span>did</span></span><span class="emp2"><span>act</span></span><span class="emp3"><span>ic</span></span> English professor was so effective because instead of just explaining the literature, she actually <span class="emp1"><span>did</span></span> <span class="emp2"><span>act</span></span> <span class="emp3"><span>it</span></span> out.
</p>
</div>

<div id='memhook-button-bar'>
<a href="didactic#" id="add-public-hook" style="" url="https://membean.com/mywords/didactic/memory_hooks">Use other public hook</a>
<a href="didactic#" id="memhook-use-own" url="https://membean.com/mywords/didactic/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='didactic#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
But increasingly, these [primary school play and creative] activities are being abandoned for the teacher-led, <b>didactic</b> instruction typically used in higher grades.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
The question about the movie was, how could you fit all this [theological and historical] exposition into a mile-a-minute thriller without bringing everything to a crashing, <b>didactic</b> halt?
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
If so many books for children tend toward the <b>didactic</b> (well, maybe not the ones about beasts consuming tacos), that may be because you’re never too young to have problems.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/didactic/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='didactic#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='didact_apt' data-tree-url='//cdn1.membean.com/public/data/treexml' href='didactic#'>
<span class=''></span>
didact
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>apt at teaching</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ic_like' data-tree-url='//cdn1.membean.com/public/data/treexml' href='didactic#'>
<span class=''></span>
-ic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>like</td>
</tr>
</table>
<p><em>Didactic</em> commentary is &#8220;apt at teaching&#8221; important rules of conduct about life.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='didactic#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>A Didactic Storytelling</strong><span> The Lion and the Mouse</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/didactic.jpg' video_url='examplevids/didactic' video_width='350'></span>
<div id='wt-container'>
<img alt="Didactic" height="288" src="https://cdn1.membean.com/video/examplevids/didactic.jpg" width="350" />
<div class='center'>
<a href="didactic#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='didactic#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Didactic" src="https://cdn3.membean.com/public/images/wordimages/cons2/didactic.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='didactic#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>articulate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>edification</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>empirical</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>parable</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>pedagogy</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>pedantic</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>rhetoric</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>rubric</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>sententious</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>statutory</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>arbitrary</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>desultory</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>fortuitous</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="didactic" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>didactic</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="didactic#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

