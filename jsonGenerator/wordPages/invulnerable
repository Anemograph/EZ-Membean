
<!DOCTYPE html>
<html>
<head>
<title>Word: invulnerable | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you abash someone, you make them feel uncomfortable, ashamed, embarrassed, or inferior.</p>
<p class='rw-defn idx1' style='display:none'>If someone is addled by something, they are confused by it and unable to think properly.</p>
<p class='rw-defn idx2' style='display:none'>Something done under the aegis of an organization or person is done with their support, backing, and protection.</p>
<p class='rw-defn idx3' style='display:none'>If you deal with a difficult situation with aplomb, you deal with it in a confident and skillful way.</p>
<p class='rw-defn idx4' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx5' style='display:none'>When you are under another person&#8217;s auspices, you are being guided and protected by them.</p>
<p class='rw-defn idx6' style='display:none'>A bastion is considered an important and effective defense of a way of life or principle; a bastion can also refer to a heavily fortified place, such as a castle.</p>
<p class='rw-defn idx7' style='display:none'>When something bedevils you, it causes you a lot of problems and difficulties over a period of time.</p>
<p class='rw-defn idx8' style='display:none'>When you beleaguer someone, you act with the intent to annoy or harass that person repeatedly until they finally give you what you want.</p>
<p class='rw-defn idx9' style='display:none'>If you are beset by something, you are experiencing serious problems or dangers because of it.</p>
<p class='rw-defn idx10' style='display:none'>When you buttress an argument, idea, or even a building, you make it stronger by providing support.</p>
<p class='rw-defn idx11' style='display:none'>Complacent persons are too confident and relaxed because they think that they can deal with a situation easily; however, in many circumstances, this is not the case.</p>
<p class='rw-defn idx12' style='display:none'>If something confounds you, it makes you feel surprised and confused, often because it does not meet your predefined expectations.</p>
<p class='rw-defn idx13' style='display:none'>If something daunts you, it makes you worried about dealing with it because you think it will be very difficult or dangerous.</p>
<p class='rw-defn idx14' style='display:none'>Debility is a state of being physically or mentally weak, usually due to illness.</p>
<p class='rw-defn idx15' style='display:none'>Decrepitude is the state of being very old, worn out, or very ill; therefore, something or someone is no longer in good physical condition or good health.</p>
<p class='rw-defn idx16' style='display:none'>Someone who is diffident is shy, does not want to draw notice to themselves, and is lacking in self-confidence.</p>
<p class='rw-defn idx17' style='display:none'>A dilapidated building, vehicle, etc. is old, broken-down, and in very bad condition.</p>
<p class='rw-defn idx18' style='display:none'>If something discomfits you, it makes you feel embarrassed, confused, uncomfortable, or frustrated.</p>
<p class='rw-defn idx19' style='display:none'>If something disconcerts you, it makes you feel anxious, worried, or confused.</p>
<p class='rw-defn idx20' style='display:none'>When someone feels disquiet about a situation, they feel very worried or nervous.</p>
<p class='rw-defn idx21' style='display:none'>If you describe a person, group, or civilization as effete, you mean it is weak, exhausted,  powerless, unproductive, and/or corrupt.</p>
<p class='rw-defn idx22' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx23' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx24' style='display:none'>If something is formidable, it is impressive in size, power, or skill; therefore, it can make you feel frightened, alarmed, or in awe because it is so powerful or difficult to deal with.</p>
<p class='rw-defn idx25' style='display:none'>A fortification is a structure or building that is used in defense against an invading army.</p>
<p class='rw-defn idx26' style='display:none'>The adjective hermetic describes something that is set apart, isolated, or separate from the influence or interference of society at large.</p>
<p class='rw-defn idx27' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx28' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx29' style='display:none'>An impenetrable barrier cannot be gotten through by any means; this word can refer to parts of a building such as walls and doors—or to a problem of some kind that cannot be solved.</p>
<p class='rw-defn idx30' style='display:none'>If someone is imperturbable, they are always calm and not easily upset or disturbed by any situation, even dangerous ones.</p>
<p class='rw-defn idx31' style='display:none'>If you are impervious to things, such as someone&#8217;s actions or words, you are not affected by them or do not notice them.</p>
<p class='rw-defn idx32' style='display:none'>An impregnable fortress or castle is very difficult to defeat or overcome; an opinion or argument of that same quality is almost impossible to successfully change or challenge.</p>
<p class='rw-defn idx33' style='display:none'>If someone leaves an indelible impression on you, it will not be forgotten; an indelible mark is permanent or impossible to erase or remove.</p>
<p class='rw-defn idx34' style='display:none'>An indomitable foe cannot be beaten.</p>
<p class='rw-defn idx35' style='display:none'>When a spy infiltrates enemy lines, they creep in or penetrate them so as to gather information.</p>
<p class='rw-defn idx36' style='display:none'>An infusion is the pouring in or the introduction of something into something else so as to fill it up.</p>
<p class='rw-defn idx37' style='display:none'>When you are in jeopardy, you are in danger or trouble of some kind.</p>
<p class='rw-defn idx38' style='display:none'>If you nettle someone, you irritate or annoy them.</p>
<p class='rw-defn idx39' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx40' style='display:none'>An omnipotent being or entity is unlimited in its power.</p>
<p class='rw-defn idx41' style='display:none'>When a substance permeates something, it enters into all parts of it.</p>
<p class='rw-defn idx42' style='display:none'>Skittish persons or animals are made easily nervous or alarmed; they are likely to change behavior quickly and unpredictably.</p>
<p class='rw-defn idx43' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx44' style='display:none'>A stoic person does not show their emotions and does not complain when bad things happen to them.</p>
<p class='rw-defn idx45' style='display:none'>If you are susceptible to something, such as a disease or emotion, you are likely or inclined to be affected by it.</p>
<p class='rw-defn idx46' style='display:none'>To be timorous is to be fearful.</p>
<p class='rw-defn idx47' style='display:none'>Someone is tremulous when they are shaking slightly from nervousness or fear; they may also simply be afraid of something.</p>
<p class='rw-defn idx48' style='display:none'>When you are unflappable, you remain calm, cool, and collected in even the most trying of situations.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>invulnerable</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='invulnerable#' id='pronounce-sound' path='audio/words/amy-invulnerable'></a>
in-VUHL-ner-uh-buhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='invulnerable#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='invulnerable#' id='context-sound' path='audio/wordcontexts/brian-invulnerable'></a>
Computers are not <em>invulnerable</em> or safe from viruses if they do not have the proper anti-virus software installed.  It is so important to run virus scans because it&#8217;s unwise to think your computer is <em>invulnerable</em> or unable to be attacked and harmed.  Even the most secure computers are not <em>invulnerable</em> or completely secure, so everyone should be in the habit of running their anti-virus software, and keeping it updated.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If someone feels their home is <em>invulnerable</em>, what do they think it is?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
They think it is extremely well protected.
</li>
<li class='choice '>
<span class='result'></span>
They think it is slowly falling apart and needs updating.
</li>
<li class='choice '>
<span class='result'></span>
They worry that someone could easily break into it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='invulnerable#' id='definition-sound' path='audio/wordmeanings/amy-invulnerable'></a>
Someone or something that is <em>invulnerable</em> cannot be harmed in any way; hence, they or it is completely safe or secure.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>completely secure</em>
</span>
</span>
</div>
<a class='quick-help' href='invulnerable#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='invulnerable#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/invulnerable/memory_hooks/4203.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Inv</span></span>aders <span class="emp2"><span>Un</span></span>conqu<span class="emp2"><span>erable</span></span></span></span> The <span class="emp1"><span>inv</span></span><span class="emp2"><span>ulnerable</span></span> <span class="emp1"><span>inv</span></span>aders were <span class="emp2"><span>un</span></span>conqu<span class="emp2"><span>erable</span></span>, so we had no choice but to surrender our country to them.
</p>
</div>

<div id='memhook-button-bar'>
<a href="invulnerable#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/invulnerable/memory_hooks">Use other hook</a>
<a href="invulnerable#" id="memhook-use-own" url="https://membean.com/mywords/invulnerable/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='invulnerable#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Just as some bacteria have become resistant to antibiotic drugs, a growing number of superweeds and superbugs in the nation's farm fields are proving <b>invulnerable</b> to the tons of pesticides that go hand in hand with genetically modified seeds.
<cite class='attribution'>
&mdash;
Minneapolis Star Tribune
</cite>
</li>
<li>
Larson tells NPR's Scott Simon that before that day in 1915, the Lusitania was seen as <b>invulnerable</b>: "At the time, it was the fastest and most glamorous ocean liner then in service. And, you know, given the hubris of the time, [it] was thought to be so fast and so large that no submarine A) could catch it, B) could sink it.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
“We just need to make sure the message is getting out, particularly to young people, particularly to children and young adults, that you are not <b>invulnerable</b> to this and you can get infected,” said Maria Van Kerkhove, head of the WHO’s emerging disease and zoonosis unit. “We are seeing young people who are dying from this virus.”
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
The once <b>invulnerable</b> Steeler defensive line is suffering the erosion of time, and [Coach Buddy] Parker needs help where help is hard to come by. He has a strong nucleus headed by End Dan LaRose, but the line lacks depth.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/invulnerable/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='invulnerable#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='in_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='invulnerable#'>
<span class='common'></span>
in-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not</td>
</tr>
<tr>
<td class='partform'>vulner</td>
<td>
&rarr;
</td>
<td class='meaning'>wound</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='able_handy' data-tree-url='//cdn1.membean.com/public/data/treexml' href='invulnerable#'>
<span class=''></span>
-able
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>handy, capable of</td>
</tr>
</table>
<p>Someone <em>invulnerable</em> is &#8220;not capable of being wounded.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='invulnerable#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Superman The Movie</strong><span> Superman is invulnerable to firing and fire.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/invulnerable.jpg' video_url='examplevids/invulnerable' video_width='350'></span>
<div id='wt-container'>
<img alt="Invulnerable" height="288" src="https://cdn1.membean.com/video/examplevids/invulnerable.jpg" width="350" />
<div class='center'>
<a href="invulnerable#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='invulnerable#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Invulnerable" src="https://cdn0.membean.com/public/images/wordimages/cons2/invulnerable.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='invulnerable#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>aegis</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>aplomb</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>auspices</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>bastion</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>buttress</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>complacent</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>formidable</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>fortification</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>hermetic</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>impenetrable</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>imperturbable</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>impervious</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>impregnable</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>indelible</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>indomitable</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>omnipotent</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>stoic</span> &middot;</li><li data-idx='48' class = 'rw-wordform notlearned'><span>unflappable</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abash</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>addle</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>bedevil</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>beleaguer</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>beset</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>confound</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>daunt</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>debility</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>decrepitude</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>diffident</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>dilapidated</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>discomfit</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>disconcert</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>disquiet</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>effete</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>infiltrate</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>infusion</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>jeopardy</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>nettle</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>permeate</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>skittish</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>susceptible</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>timorous</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>tremulous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='invulnerable#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
vulnerable
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>able to be wounded or hurt</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="invulnerable" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>invulnerable</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="invulnerable#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

