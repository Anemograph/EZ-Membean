
<!DOCTYPE html>
<html>
<head>
<title>Word: dirge | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Dirge-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/dirge-large.jpg?qdep8" />
</div>
<a href="dirge#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>An anodyne is a medicine that soothes or relieves pain.</p>
<p class='rw-defn idx1' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx2' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx3' style='display:none'>Someone who is dour is serious, stubborn, and unfriendly; they can also be gloomy.</p>
<p class='rw-defn idx4' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx5' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx6' style='display:none'>An elegy is a poem or other piece of writing expressing sadness; it is often about someone who has died.</p>
<p class='rw-defn idx7' style='display:none'>A eulogy is a speech or other piece of writing, often part of a funeral, in which someone or something is highly praised.</p>
<p class='rw-defn idx8' style='display:none'>If you show exuberance, you display great excitement, energy, and enthusiasm.</p>
<p class='rw-defn idx9' style='display:none'>An idyll is a place or situation that is extremely pleasant, peaceful, and has no problems.</p>
<p class='rw-defn idx10' style='display:none'>Someone who is jocular is cheerful and often makes jokes or tries to make people laugh.</p>
<p class='rw-defn idx11' style='display:none'>If someone is lugubrious, they are looking very sad or gloomy.</p>
<p class='rw-defn idx12' style='display:none'>If you describe something as moribund, you imply that it is no longer effective; it may also be coming to the end of its useful existence.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is morose is unhappy, bad-tempered, and unwilling to talk very much.</p>
<p class='rw-defn idx14' style='display:none'>A plaintive sound or voice expresses sadness.</p>
<p class='rw-defn idx15' style='display:none'>A requiem is a mass at a funeral or a piece of music written for the dead.</p>
<p class='rw-defn idx16' style='display:none'>A scintillating conversation, speech, or performance is brilliantly clever, interesting, and lively.</p>
<p class='rw-defn idx17' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is zealous spends a lot of time, energy, and effort to support something—  notably that of a political or religious nature—because they believe in it very strongly.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>dirge</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='dirge#' id='pronounce-sound' path='audio/words/amy-dirge'></a>
durj
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='dirge#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='dirge#' id='context-sound' path='audio/wordcontexts/brian-dirge'></a>
As the four brothers carried the coffin out of the church, the organist played a sad and measured <em>dirge</em>.  The four men in black walked in slow time to the organ&#8217;s sorrowful <em>dirge</em> as they carried the body of their beloved deceased friend.  A large gathering of mourners slowly walked behind the coffin as the <em>dirge</em> played on and filled the air with the funeral song that only aided in helping tears flow.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Which of these lyrics is most likely part of a <em>dirge</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
“We shall come rejoicing, bringing in the sheaves.”
</li>
<li class='choice '>
<span class='result'></span>
“O beautiful for spacious skies, for amber waves of grain.”
</li>
<li class='choice answer '>
<span class='result'></span>
“How our hearts ache with grief as we say goodbye.”
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='dirge#' id='definition-sound' path='audio/wordmeanings/amy-dirge'></a>
A <em>dirge</em> is a slow and sad piece of music often performed at funerals.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>mournful song</em>
</span>
</span>
</div>
<a class='quick-help' href='dirge#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='dirge#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/dirge/memory_hooks/5362.json'></span>
<p>
<span class="emp0"><span>Di<span class="emp3"><span>rge</span></span> U<span class="emp3"><span>rge</span></span></span></span> I don't know why, but after I failed three tests on one horrible day I had this incredible u<span class="emp3"><span>rge</span></span> to play a di<span class="emp3"><span>rge</span></span> on my guitar, and so I quickly gave in to my di<span class="emp3"><span>rge</span></span> u<span class="emp3"><span>rge</span></span>--soon mournful sounds began filling my room.
</p>
</div>

<div id='memhook-button-bar'>
<a href="dirge#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/dirge/memory_hooks">Use other hook</a>
<a href="dirge#" id="memhook-use-own" url="https://membean.com/mywords/dirge/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='dirge#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
The petals drift; she is weary; and soon the darkness falls. / A nightingale is singing a <b>dirge</b> for the death of spring, / And moonlight steals through the casement and dapples the silent walls.
<span class='attribution'>&mdash; Cao Xueqin, 18th century Chinese writer</span>
<img alt="Cao xueqin, 18th century chinese writer" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Cao Xueqin, 18th century Chinese writer.jpg?qdep8" width="80" />
</li>
<li>
The young musicians making a name for themselves busking on the streets in the French Quarter are gone. Jazz funerals where mourners send off loved ones with a slow <b>dirge</b> and then an uplifting rendition of “When the Saints Go Marching In” are over.
<cite class='attribution'>
&mdash;
PBS News Hour
</cite>
</li>
<li>
"Britain’s national anthem is something of an embarrassment in a modern age, not least because of the <b>dirge</b>-like tune," he said.
<cite class='attribution'>
&mdash;
The Independent
</cite>
</li>
<li>
The "first line" comprises the casket, family and close friends and a band with parade-friendly—that is, portable —instrumentation: bass and snare drums and horns. The second line is the group of onlookers that follows, walking first to the somber rhythm of a <b>dirge</b>, and then, after the body is "cut loose" at the cemetery, dancing to a joyous, upbeat brass-band whomp.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/dirge/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='dirge#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>The original <em>dirge</em> was based upon a verse from the fifth Psalm (a lament), which begins <em>Dirige, Domine</em> &#8220;Direct, o Lord&#8230;;&#8221; <em>dirge</em> is simply a variation of that frequently used first word <em>Dirige</em>.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='dirge#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>James Bond</strong><span> This funeral march features a Dixieland dirge for the dearly departed.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/dirge.jpg' video_url='examplevids/dirge' video_width='350'></span>
<div id='wt-container'>
<img alt="Dirge" height="198" src="https://cdn1.membean.com/video/examplevids/dirge.jpg" width="350" />
<div class='center'>
<a href="dirge#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='dirge#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Dirge" src="https://cdn0.membean.com/public/images/wordimages/cons2/dirge.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='dirge#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>dour</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>elegy</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>eulogy</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>lugubrious</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>moribund</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>morose</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>plaintive</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>requiem</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>anodyne</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>exuberance</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>idyll</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>jocular</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>scintillating</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>zealous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="dirge" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>dirge</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="dirge#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

