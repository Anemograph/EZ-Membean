
<!DOCTYPE html>
<html>
<head>
<title>Word: misgiving | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you accede to a demand or proposal, you agree to it, especially after first disagreeing with it.</p>
<p class='rw-defn idx1' style='display:none'>If you acquiesce to something, you allow it to happen by doing what someone wants without putting up a struggle or voicing your own concerns.</p>
<p class='rw-defn idx2' style='display:none'>If you describe something as ambiguous, you mean that it is unclear or confusing because it can be understood in multiple ways.</p>
<p class='rw-defn idx3' style='display:none'>An audacious person acts with great daring and sometimes reckless bravery—despite risks and warnings from other people—in order to achieve something.</p>
<p class='rw-defn idx4' style='display:none'>If you aver that something is the case, you say firmly and strongly that you believe it is true.</p>
<p class='rw-defn idx5' style='display:none'>If you describe a person&#8217;s behavior as brazen, you mean that they are not embarrassed by anything they do—they simply don&#8217;t care what other people think about them.</p>
<p class='rw-defn idx6' style='display:none'>A callous person&#8217;s attitude is cruel, uncaring, and shows no concern for others whatsoever.</p>
<p class='rw-defn idx7' style='display:none'>When you capitulate, you accept or agree to do something after having resisted doing so for a long time.</p>
<p class='rw-defn idx8' style='display:none'>Someone who is capricious changes their mind repeatedly or behaves in unexpected ways; a capricious series of events follows no predictable pattern.</p>
<p class='rw-defn idx9' style='display:none'>A cavalier person does not seem to care about rules, principles, or other people&#8217;s feelings, no matter how dire or serious a situation may be.</p>
<p class='rw-defn idx10' style='display:none'>If you feel compunction about doing something, you feel that you should not do it because it is bad or wrong.</p>
<p class='rw-defn idx11' style='display:none'>If you condone someone&#8217;s behavior, you go along with it and provide silent support for it—despite having doubts about it.</p>
<p class='rw-defn idx12' style='display:none'>To corroborate something that has been said or reported means to give information or evidence that further supports it.</p>
<p class='rw-defn idx13' style='display:none'>If someone will countenance something, they will approve, tolerate, or support it.</p>
<p class='rw-defn idx14' style='display:none'>Credence is the mental act of believing or accepting that something is true.</p>
<p class='rw-defn idx15' style='display:none'>If you demur, you delay in doing or mildly object to something because you don&#8217;t really want to do it.</p>
<p class='rw-defn idx16' style='display:none'>If something discomfits you, it makes you feel embarrassed, confused, uncomfortable, or frustrated.</p>
<p class='rw-defn idx17' style='display:none'>When someone feels disquiet about a situation, they feel very worried or nervous.</p>
<p class='rw-defn idx18' style='display:none'>When you make an emphatic declaration, you are insistent and absolute about it.</p>
<p class='rw-defn idx19' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx20' style='display:none'>If you fathom something complicated or mysterious, you are able to understand it.</p>
<p class='rw-defn idx21' style='display:none'>Things that fluctuate vary or change often, rising or falling seemingly at random.</p>
<p class='rw-defn idx22' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx23' style='display:none'>If someone is imperturbable, they are always calm and not easily upset or disturbed by any situation, even dangerous ones.</p>
<p class='rw-defn idx24' style='display:none'>If an idea or thought is incisive, it is expressed in a penetrating and knowledgeable manner that is clear and brief; additionally, it can demonstrate impressive understanding of related ideas or thoughts.</p>
<p class='rw-defn idx25' style='display:none'>If the results of something are inconclusive, they are uncertain or provide no final answers.</p>
<p class='rw-defn idx26' style='display:none'>An intrepid person is willing to do dangerous things or go to dangerous places because they are brave.</p>
<p class='rw-defn idx27' style='display:none'>Someone who is irresolute is unable to decide what to do.</p>
<p class='rw-defn idx28' style='display:none'>Malaise is a feeling of discontent, general unease, or even illness in a person or group for which there does not seem to be a quick and easy solution because the cause of it is not clear.</p>
<p class='rw-defn idx29' style='display:none'>A mercurial person&#8217;s mind or mood changes often, quickly, and unpredictably.</p>
<p class='rw-defn idx30' style='display:none'>If you nettle someone, you irritate or annoy them.</p>
<p class='rw-defn idx31' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx32' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx33' style='display:none'>If an object oscillates, it moves repeatedly from one point to another and then back again; if you oscillate between two moods or attitudes, you keep changing from one to the other.</p>
<p class='rw-defn idx34' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx35' style='display:none'>If you have qualms about doing something, you are having doubts about it because you are worried that it might be the wrong thing to do.</p>
<p class='rw-defn idx36' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx37' style='display:none'>To be tentative is to be hesitant or uncertain about something; an agreement or decision of this kind is likely to have changes before it reaches its final form.</p>
<p class='rw-defn idx38' style='display:none'>When someone vacillates, they go back and forth about a choice or opinion, unable to make a firm decision.</p>
<p class='rw-defn idx39' style='display:none'>Something that is volatile can change easily and vary widely.</p>
<p class='rw-defn idx40' style='display:none'>If you waver, you cannot decide between two things because you have serious doubts about which choice is better.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>misgiving</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='misgiving#' id='pronounce-sound' path='audio/words/amy-misgiving'></a>
mis-GIV-ing
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='misgiving#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='misgiving#' id='context-sound' path='audio/wordcontexts/brian-misgiving'></a>
I had some <em>misgivings</em> or concerns about the class when I left the first day.  It seemed we were going to have to do a lot more work than the course description had indicated, and so I had <em>misgivings</em> or doubts about being able to do it all.  Another <em>misgiving</em> or suspicion was the tone of the instructor, who seemed harsh and eager to criticize.  Perhaps these <em>misgivings</em> or worries are what led me to drop the class.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If you have <em>misgivings</em> about something, what would you do?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
You might delay doing it since you have an uneasy feeling about it.
</li>
<li class='choice '>
<span class='result'></span>
You might apologize for it because you know you did it poorly.
</li>
<li class='choice '>
<span class='result'></span>
You might try to learn more about it since it interests you. 
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='misgiving#' id='definition-sound' path='audio/wordmeanings/amy-misgiving'></a>
A <em>misgiving</em> is a doubt or uncertainty that you have about something, usually something that you are about to do.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>doubt</em>
</span>
</span>
</div>
<a class='quick-help' href='misgiving#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='misgiving#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/misgiving/memory_hooks/4405.json'></span>
<p>
<span class="emp0"><span>L<span class="emp2"><span>iving</span></span> in <span class="emp1"><span>Mis</span></span>trust</span></span> I have so many <span class="emp1"><span>mis</span></span>g<span class="emp2"><span>iving</span></span>s about so many things that I feel like I am l<span class="emp2"><span>iving</span></span> or swimming in <span class="emp1"><span>mis</span></span>trust.
</p>
</div>

<div id='memhook-button-bar'>
<a href="misgiving#" id="add-public-hook" style="" url="https://membean.com/mywords/misgiving/memory_hooks">Use other public hook</a>
<a href="misgiving#" id="memhook-use-own" url="https://membean.com/mywords/misgiving/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='misgiving#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
At every crisis in one's life, it is absolute salvation to have some sympathetic friend to whom you can think aloud without restraint or <b>misgiving</b>.
<span class='attribution'>&mdash; Woodrow Wilson, U.S. President during World War I</span>
<img alt="Woodrow wilson, u" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Woodrow Wilson, U.S. President during World War I.jpg?qdep8" width="80" />
</li>
<li>
There was a sense almost of a noble mission at first, and North Dakota has sent an awful lot of people from its National Guard. But now I think the mood has changed dramatically, and there’s a real <b>misgiving</b>, I think, about where we are in Iraq at this point.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Minus a <b>misgiving</b> here and there related to the [computer] system’s connections, there really isn’t anything to dislike about this wickedly fast system. It doesn't top our performance charts, but it comes pretty close without hurling a ninja star at your bank account.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Aikman also endured a dreadful second half, completing only six of 22 passes for 45 yards, and while he defended the fourth-down call right after the game, he later expressed a broader <b>misgiving</b> about the Cowboys. "I don't know where this football team is right now," he said.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/misgiving/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='misgiving#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='mis_wrong' data-tree-url='//cdn1.membean.com/public/data/treexml' href='misgiving#'>
<span class=''></span>
mis-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>wrong, wrongly</td>
</tr>
<tr>
<td class='partform'>giv</td>
<td>
&rarr;
</td>
<td class='meaning'>give</td>
</tr>
<tr>
<td class='partform'>-ing</td>
<td>
&rarr;
</td>
<td class='meaning'>one of a specified kind</td>
</tr>
</table>
<p>A <em>misgiving</em> feels like a &#8220;wrong gift&#8221; or something &#8220;given wrongly.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='misgiving#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>The IT Crowd</strong><span> The driver is having some misgivings about driving up the ramps into the truck.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/misgiving.jpg' video_url='examplevids/misgiving' video_width='350'></span>
<div id='wt-container'>
<img alt="Misgiving" height="288" src="https://cdn1.membean.com/video/examplevids/misgiving.jpg" width="350" />
<div class='center'>
<a href="misgiving#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='misgiving#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Misgiving" src="https://cdn3.membean.com/public/images/wordimages/cons2/misgiving.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='misgiving#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>ambiguous</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>capricious</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>compunction</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>condone</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>countenance</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>demur</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>discomfit</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>disquiet</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>fluctuate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>inconclusive</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>irresolute</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>malaise</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>mercurial</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>nettle</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>oscillate</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>qualm</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>tentative</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>vacillate</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>volatile</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>waver</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>accede</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>acquiesce</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>audacious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>aver</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>brazen</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>callous</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>capitulate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>cavalier</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>corroborate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>credence</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>emphatic</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>fathom</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>imperturbable</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>incisive</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>intrepid</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="misgiving" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>misgiving</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="misgiving#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

