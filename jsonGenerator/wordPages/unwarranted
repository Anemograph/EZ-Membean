
<!DOCTYPE html>
<html>
<head>
<title>Word: unwarranted | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you describe a decision, rule, or plan as arbitrary, you think that it was decided without any thought, standard, or system to guide it; therefore, it can seem unfair.</p>
<p class='rw-defn idx1' style='display:none'>Someone who is capricious changes their mind repeatedly or behaves in unexpected ways; a capricious series of events follows no predictable pattern.</p>
<p class='rw-defn idx2' style='display:none'>A chimerical idea is unrealistic, imaginary, or unlikely to be fulfilled.</p>
<p class='rw-defn idx3' style='display:none'>A conjecture is a theory or guess that is based on information that is not certain or complete.</p>
<p class='rw-defn idx4' style='display:none'>To corroborate something that has been said or reported means to give information or evidence that further supports it.</p>
<p class='rw-defn idx5' style='display:none'>If you act in a credible fashion, you are easy to believe or trust.</p>
<p class='rw-defn idx6' style='display:none'>Something that is delusive deceives you by giving a false belief about yourself or the situation you are in.</p>
<p class='rw-defn idx7' style='display:none'>An egregious mistake, failure, or problem is an extremely bad and very noticeable one.</p>
<p class='rw-defn idx8' style='display:none'>Empirical evidence or study is based on real experience or scientific experiments rather than on unproven theories.</p>
<p class='rw-defn idx9' style='display:none'>A fallacy is an idea or belief that is false.</p>
<p class='rw-defn idx10' style='display:none'>If you think something, such as a statement or idea, is fatuous, you consider it stupid or extremely silly; a fatuous hope is unrealistic.</p>
<p class='rw-defn idx11' style='display:none'>If something is gratuitous, it is freely given; nevertheless, it is usually unnecessary and can be harmful or upsetting.</p>
<p class='rw-defn idx12' style='display:none'>Something illusory appears real or possible; in fact, it is neither.</p>
<p class='rw-defn idx13' style='display:none'>Something that is implausible is unlikely to be true or hard to believe that it&#8217;s true.</p>
<p class='rw-defn idx14' style='display:none'>When you act in an imprudent fashion, you do something that is unwise, is lacking in good judgment, or has no forethought.</p>
<p class='rw-defn idx15' style='display:none'>Something that is inconceivable cannot be imagined or thought of; that is, it is beyond reason or unbelievable.</p>
<p class='rw-defn idx16' style='display:none'>Someone who is indiscreet shows lack of judgment, especially because they say or do things in public that should only be said or done privately—if at all.</p>
<p class='rw-defn idx17' style='display:none'>Inductive reasoning involves the observation of data to arrive at a general conclusion or principle.</p>
<p class='rw-defn idx18' style='display:none'>An inference is a conclusion that is reached using available data.</p>
<p class='rw-defn idx19' style='display:none'>A judicious person shows good or sound judgment because they are wise and careful in making decisions.</p>
<p class='rw-defn idx20' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx21' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx22' style='display:none'>If you handle something in a pragmatic way, you deal with it in a realistic and practical manner rather than just following unproven theories or ideas.</p>
<p class='rw-defn idx23' style='display:none'>Quixotic plans or ideas are not very practical or realistic; they are often based on unreasonable hopes for improving the world.</p>
<p class='rw-defn idx24' style='display:none'>Something that is specious seems to be good, sound, or correct but is actually wrong or fake.</p>
<p class='rw-defn idx25' style='display:none'>A spurious statement is false because it is not based on sound thinking; therefore, it is not what it appears to be.</p>
<p class='rw-defn idx26' style='display:none'>A person or subject that is superficial is shallow, without depth, obvious, and concerned only with surface matters.</p>
<p class='rw-defn idx27' style='display:none'>A tenable argument is able to be maintained or defended because there is sufficient evidence to support it.</p>
<p class='rw-defn idx28' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx29' style='display:none'>An action or deed is unconscionable if it is excessively shameful, unfair, or unjust and its effects are more severe than is reasonable or acceptable.</p>
<p class='rw-defn idx30' style='display:none'>Something unfeasible cannot be made or achieved.</p>
<p class='rw-defn idx31' style='display:none'>An unfounded claim is not based upon evidence; instead, it is unproven or groundless.</p>
<p class='rw-defn idx32' style='display:none'>A utilitarian object is useful, serviceable, and practical—rather than fancy or unnecessary.</p>
<p class='rw-defn idx33' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx34' style='display:none'>Verisimilitude is something&#8217;s authenticity or appearance of being real or true.</p>
<p class='rw-defn idx35' style='display:none'>Something that is veritable is authentic, actual, genuine, or without doubt.</p>
<p class='rw-defn idx36' style='display:none'>The verity of something is the truth or reality of it.</p>
<p class='rw-defn idx37' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>unwarranted</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='unwarranted#' id='pronounce-sound' path='audio/words/amy-unwarranted'></a>
uhn-WAWR-uhn-tid
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='unwarranted#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='unwarranted#' id='context-sound' path='audio/wordcontexts/brian-unwarranted'></a>
Your complaint is <em>unwarranted</em> or groundless because it is not based on reality.  If you ask for hot coffee at a restaurant and then complain because it is too hot, your complaint is <em>unwarranted</em> or unjustified because you got exactly what you wanted.  Your decision to complain to the manager would be <em>unwarranted</em> or uncalled-for in this situation, so you&#8217;d better just blow on it to cool if off to your taste instead of being such a jerk.  I could also make a very bad pun and say your complaint is groundless, but that, too, would be <em>unwarranted</em>.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When might a statement be described as <em>unwarranted</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is an attempt to discover hidden details about a person.
</li>
<li class='choice '>
<span class='result'></span>
It is spoken in an angry, irritated tone to convey annoyance.
</li>
<li class='choice answer '>
<span class='result'></span>
It is unfair and not based on truth or valid evidence.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='unwarranted#' id='definition-sound' path='audio/wordmeanings/amy-unwarranted'></a>
An <em>unwarranted</em> decision cannot be justified because there is no evidence to back it up; therefore, it is needless and uncalled-for.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>unjustified</em>
</span>
</span>
</div>
<a class='quick-help' href='unwarranted#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='unwarranted#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/unwarranted/memory_hooks/5605.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>N</span></span>ot <span class="emp3"><span>Wanted</span></span></span></span> An u<span class="emp1"><span>n</span></span><span class="emp3"><span>warranted</span></span> statement by a witness is <span class="emp1"><span>n</span></span>ot <span class="emp3"><span>wanted</span></span> in court because it would have nothing to do with the case at hand.
</p>
</div>

<div id='memhook-button-bar'>
<a href="unwarranted#" id="add-public-hook" style="" url="https://membean.com/mywords/unwarranted/memory_hooks">Use other public hook</a>
<a href="unwarranted#" id="memhook-use-own" url="https://membean.com/mywords/unwarranted/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='unwarranted#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
A stigma that can be erased by victory in a few tennis matches hardly has the potency to destroy the sport of women's tennis, . . . [which] continues to prosper mightily, and Billie Jean [King's] fear that when people hear her name "they will think of scandal before championships" is also <b>unwarranted</b>. She has fashioned a remarkable record both as player and advocate of her sport, and cannot fail to be remembered, first and foremost, for that.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
A friend once told me of a time when everything in his life seemed to be falling apart. Yet he unexpectedly felt hope. He spoke of an apparently <b>unwarranted</b> cheerfulness that kept breaking through. And in the end his hope was justified as his situation turned around.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
We're extremely gratified that, after a thoughtful and careful review of our writ, at least five judges on the Court of Criminal Appeals recognized that we were entitled to a stay of the Fifth Court of Appeals' order," said Brian Wice, one of three special prosecutors . . . "We're cautiously optimistic that the Court will ultimately conclude that the Fifth Court's <b>unwarranted</b> decision to scuttle the fee schedules of over two-thirds of all Texas counties was a clear abuse of discretion," he said in a statement.
<cite class='attribution'>
&mdash;
Houston Chronicle
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/unwarranted/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='unwarranted#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='un_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='unwarranted#'>
<span class='common'></span>
un-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not, opposite of</td>
</tr>
<tr>
<td class='partform'>warrant</td>
<td>
&rarr;
</td>
<td class='meaning'>protector, guarantee</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ed_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='unwarranted#'>
<span class=''></span>
-ed
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>having a particular state</td>
</tr>
</table>
<p><em>Unwarranted</em> criticism is in the &#8220;state or condition of not protecting&#8221; someone&#8217;s good name, or &#8220;not guaranteeing&#8221; justice; hence, it is &#8220;groundless&#8221; criticism.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='unwarranted#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Murder She Wrote</strong><span> Most of them feel like the lieutenant's questioning is unwarranted.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/unwarranted.jpg' video_url='examplevids/unwarranted' video_width='350'></span>
<div id='wt-container'>
<img alt="Unwarranted" height="288" src="https://cdn1.membean.com/video/examplevids/unwarranted.jpg" width="350" />
<div class='center'>
<a href="unwarranted#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='unwarranted#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Unwarranted" src="https://cdn2.membean.com/public/images/wordimages/cons2/unwarranted.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='unwarranted#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>arbitrary</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>capricious</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>chimerical</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>conjecture</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>delusive</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>egregious</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>fallacy</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>fatuous</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>gratuitous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>illusory</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>implausible</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>imprudent</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>inconceivable</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>indiscreet</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>quixotic</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>specious</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>spurious</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>superficial</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>unconscionable</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>unfeasible</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>unfounded</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='4' class = 'rw-wordform notlearned'><span>corroborate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>credible</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>empirical</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>inductive</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>inference</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>judicious</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>pragmatic</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>tenable</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>utilitarian</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>verisimilitude</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>veritable</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='unwarranted#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
warrant
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>serve as a reason to believe in something</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="unwarranted" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>unwarranted</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="unwarranted#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

