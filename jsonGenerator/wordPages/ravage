
<!DOCTYPE html>
<html>
<head>
<title>Word: ravage | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If someone in authority abolishes a law or practice, they formally put an end to it.</p>
<p class='rw-defn idx1' style='display:none'>When something is annihilated, it is completely destroyed or wiped out.</p>
<p class='rw-defn idx2' style='display:none'>Bedlam is a situation with a great deal of noise and confusion.</p>
<p class='rw-defn idx3' style='display:none'>When you beget something, you cause it to happen or create it.</p>
<p class='rw-defn idx4' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx5' style='display:none'>Mass carnage is the massacre or slaughter of many people at one time, usually in battle or from an unusually intense natural disaster.</p>
<p class='rw-defn idx6' style='display:none'>If you decimate something, you destroy a large part of it, reducing its size and effectiveness greatly.</p>
<p class='rw-defn idx7' style='display:none'>To defile something holy or important is to do something to it or say something about it that is highly offensive to others.</p>
<p class='rw-defn idx8' style='display:none'>Someone defoliates a tree or plant by removing its leaves, usually by applying a chemical agent.</p>
<p class='rw-defn idx9' style='display:none'>Something that is deleterious has a harmful effect.</p>
<p class='rw-defn idx10' style='display:none'>To denude an area is to remove the plants and trees that cover it; it can also mean to make something bare.</p>
<p class='rw-defn idx11' style='display:none'>Depredation can be the act of destroying or robbing a village—or the overall damage that time can do to things held dear.</p>
<p class='rw-defn idx12' style='display:none'>Deprivation is a state during which people lack something, especially adequate food and shelter; deprivation can also describe something being taken away from someone.</p>
<p class='rw-defn idx13' style='display:none'>Something, such as a building, is derelict if it is empty, not used, and in bad condition or disrepair.</p>
<p class='rw-defn idx14' style='display:none'>If you desecrate something that is considered holy or very special, you deliberately spoil or damage it.</p>
<p class='rw-defn idx15' style='display:none'>Something detrimental causes damage, harm, or loss to someone or something.</p>
<p class='rw-defn idx16' style='display:none'>A dilapidated building, vehicle, etc. is old, broken-down, and in very bad condition.</p>
<p class='rw-defn idx17' style='display:none'>When you dismantle something, you take it apart or destroy it piece by piece. </p>
<p class='rw-defn idx18' style='display:none'>To efface something is to erase or remove it completely from recognition or memory.</p>
<p class='rw-defn idx19' style='display:none'>If something engenders a particular feeling or attitude, it causes that condition.</p>
<p class='rw-defn idx20' style='display:none'>If you refer to the enormity of a situation, problem, or event, you mean that it is very evil or morally offensive; this word has nothing to do with physical size.</p>
<p class='rw-defn idx21' style='display:none'>When you eradicate something, you tear it up by the roots or remove it completely.</p>
<p class='rw-defn idx22' style='display:none'>If you expunge something, you completely remove it or do away with it.</p>
<p class='rw-defn idx23' style='display:none'>If you extirpate something, you completely destroy it because it is unpleasant or unwanted.</p>
<p class='rw-defn idx24' style='display:none'>Gestation is the process by which a new idea,  plan, or piece of work develops in your mind.</p>
<p class='rw-defn idx25' style='display:none'>When there is havoc, there is great disorder, widespread destruction, and much confusion.</p>
<p class='rw-defn idx26' style='display:none'>An incursion is an unpleasant intrusion, such as a sudden hostile attack or a land being flooded.</p>
<p class='rw-defn idx27' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx28' style='display:none'>Largess is the generous act of giving money or presents to a large number of people.</p>
<p class='rw-defn idx29' style='display:none'>If someone exhibits magnanimity towards another, they show them kindness and noble generosity, especially after defeating them in battle or after having been treated badly by them.</p>
<p class='rw-defn idx30' style='display:none'>A mercenary person is one whose sole interest is in earning money.</p>
<p class='rw-defn idx31' style='display:none'>A munificent person is extremely generous, especially with money.</p>
<p class='rw-defn idx32' style='display:none'>When you obliterate something, you destroy it to such an extent that there is nothing or very little of it left.</p>
<p class='rw-defn idx33' style='display:none'>Pandemonium is a very noisy and uncontrolled situation, especially one that is caused by a lot of angry or excited people.</p>
<p class='rw-defn idx34' style='display:none'>Something that is pernicious is very harmful or evil, often in a way that is hidden or not quickly noticed.</p>
<p class='rw-defn idx35' style='display:none'>When you perpetuate something, you keep it going or continue it indefinitely.</p>
<p class='rw-defn idx36' style='display:none'>Philanthropy is unselfish support in the form of donating money, work, or gifts to positive social purposes; philanthropy is also overall love for humans in general.</p>
<p class='rw-defn idx37' style='display:none'>If soldiers pillage a place, such as a town or museum, they steal a lot of things and damage it using violent methods.</p>
<p class='rw-defn idx38' style='display:none'>When you plunder, you forcefully take or rob others of their possessions, usually during times of war, natural disaster, or other unrest.</p>
<p class='rw-defn idx39' style='display:none'>When you proffer something to someone, you offer it up or hold it out to them to take.</p>
<p class='rw-defn idx40' style='display:none'>When things propagate, such as plants, animals, or ideas, they reproduce or generate greater numbers, causing them to spread.</p>
<p class='rw-defn idx41' style='display:none'>Something that is propitious shows favorable conditions.</p>
<p class='rw-defn idx42' style='display:none'>To purge something is to get rid of or remove it.</p>
<p class='rw-defn idx43' style='display:none'>To purloin is to steal.</p>
<p class='rw-defn idx44' style='display:none'>To ransack a place is to thoroughly loot or rob items from it; it can also mean to look through something thoroughly by examining every bit of it.</p>
<p class='rw-defn idx45' style='display:none'>A scourge was originally a whip used for torture and now refers to something that torments or causes serious trouble or devastation.</p>
<p class='rw-defn idx46' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>
<p class='rw-defn idx47' style='display:none'>When you usurp someone else&#8217;s power, position, or role, you take it from them although you do not have the right to do so.</p>
<p class='rw-defn idx48' style='display:none'>A virulent disease is very dangerous and spreads very quickly.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>ravage</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='ravage#' id='pronounce-sound' path='audio/words/amy-ravage'></a>
RAV-ij
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='ravage#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='ravage#' id='context-sound' path='audio/wordcontexts/brian-ravage'></a>
The bear <em>ravaged</em> or destroyed the beehive by not only eating all the honey, but also smashing it to bits.  The bees tried to protect their home as the bear began to <em>ravage</em> or wreck it, but the bear didn&#8217;t seem to notice their presence in the least.  Although the bees stung him at least one-hundred times, he didn&#8217;t seem to notice as he <em>ravaged</em> or tore the hive apart in his greedy desire for honey.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean to <em>ravage</em> something?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
To improve it so it works better.
</li>
<li class='choice '>
<span class='result'></span>
To move it to a new location.
</li>
<li class='choice answer '>
<span class='result'></span>
To break it into a million pieces.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='ravage#' id='definition-sound' path='audio/wordmeanings/amy-ravage'></a>
When you <em>ravage</em> something, you completely destroy, wreck, or damage it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>destroy</em>
</span>
</span>
</div>
<a class='quick-help' href='ravage#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='ravage#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/ravage/memory_hooks/5612.json'></span>
<p>
<span class="emp0"><span>S<span class="emp1"><span>avage</span></span></span></span>  What s<span class="emp1"><span>avage</span></span> behavior!  Those s<span class="emp1"><span>avages</span></span> r<span class="emp1"><span>avage</span></span>d my small village, utterly destroying it and taking all our worldly possessions!
</p>
</div>

<div id='memhook-button-bar'>
<a href="ravage#" id="add-public-hook" style="" url="https://membean.com/mywords/ravage/memory_hooks">Use other public hook</a>
<a href="ravage#" id="memhook-use-own" url="https://membean.com/mywords/ravage/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='ravage#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Bella, the Australian shepherd who was missing for 57 days, was found on Sunday, WTVF-TV reported. She alerted Eric and Faith Johnson to a tornado that <b>ravaged</b> Putnam County moments before it flattened the Johnson’s family home.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
Scientists say the spread of white-nose into West Virginia—and the discovery of suspected sites this week in neighboring Virginia—means the fungus could <b>ravage</b> cave-dwelling bats across the U.S.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/ravage/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='ravage#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='rav_snatch' data-tree-url='//cdn1.membean.com/public/data/treexml' href='ravage#'>
<span class=''></span>
rav
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>snatch, grab, seize</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='age_do' data-tree-url='//cdn1.membean.com/public/data/treexml' href='ravage#'>
<span class=''></span>
-age
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to do something</td>
</tr>
</table>
<p>To <em>ravage</em> a land is to &#8220;snatch or seize&#8221; all things there, thereby causing great destruction.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='ravage#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Citizen Kane</strong><span> Watch as this man ravages the room.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/ravage.jpg' video_url='examplevids/ravage' video_width='350'></span>
<div id='wt-container'>
<img alt="Ravage" height="288" src="https://cdn1.membean.com/video/examplevids/ravage.jpg" width="350" />
<div class='center'>
<a href="ravage#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='ravage#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Ravage" src="https://cdn2.membean.com/public/images/wordimages/cons2/ravage.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='ravage#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abolish</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>annihilate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bedlam</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>carnage</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>decimate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>defile</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>defoliate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>deleterious</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>denude</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>depredation</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>deprivation</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>derelict</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>desecrate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>detrimental</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>dilapidated</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>dismantle</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>efface</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>enormity</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>eradicate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>expunge</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>extirpate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>havoc</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>incursion</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>mercenary</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>obliterate</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>pandemonium</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>pernicious</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>pillage</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>plunder</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>purge</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>purloin</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>ransack</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>scourge</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>usurp</span> &middot;</li><li data-idx='48' class = 'rw-wordform notlearned'><span>virulent</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>beget</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>engender</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>gestation</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>largess</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>magnanimity</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>munificent</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>perpetuate</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>philanthropy</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>proffer</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>propagate</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>propitious</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="ravage" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>ravage</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="ravage#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

