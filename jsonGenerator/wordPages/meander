
<!DOCTYPE html>
<html>
<head>
<title>Word: meander | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Ambulatory activities involve walking or moving around.</p>
<p class='rw-defn idx1' style='display:none'>A catatonic person is in a state of suspended action; therefore, they are rigid, immobile, and unresponsive.</p>
<p class='rw-defn idx2' style='display:none'>An excursion is a short trip or outing from which you return home fairly quickly.</p>
<p class='rw-defn idx3' style='display:none'>If someone gambols, they run, jump, and play like a young child or animal.</p>
<p class='rw-defn idx4' style='display:none'>If you incarcerate someone, you put them in prison or jail.</p>
<p class='rw-defn idx5' style='display:none'>An indolent person is lazy.</p>
<p class='rw-defn idx6' style='display:none'>When something is inert, it has no power of movement or is inactive.</p>
<p class='rw-defn idx7' style='display:none'>When someone is described as itinerant, they are characterized by traveling or moving about from place to place.</p>
<p class='rw-defn idx8' style='display:none'>Something kinetic is moving, active, and using energy.</p>
<p class='rw-defn idx9' style='display:none'>A languid person is slow, relaxed, and shows little energy or interest in doing anything.</p>
<p class='rw-defn idx10' style='display:none'>Lassitude is a state of tiredness, lack of energy, and having little interest in what&#8217;s going on around you.</p>
<p class='rw-defn idx11' style='display:none'>If you are lethargic, you are tired, lack energy, and are unwilling to exert effort.</p>
<p class='rw-defn idx12' style='display:none'>When you are listless, you lack energy and interest and are unwilling to exert any effort.</p>
<p class='rw-defn idx13' style='display:none'>A peregrination is a long journey or act of traveling from place to place, especially by foot.</p>
<p class='rw-defn idx14' style='display:none'>If someone leads a peripatetic life, they travel from place to place, living and working only for a short time in each place before moving on.</p>
<p class='rw-defn idx15' style='display:none'>When people or animals are quarantined, they are put in isolation so that they will not spread disease.</p>
<p class='rw-defn idx16' style='display:none'>A state of quiescence is one of quiet and restful inaction.</p>
<p class='rw-defn idx17' style='display:none'>To ramble is to wander about leisurely, with no specific destination in mind; to ramble while speaking is to talk with no particular aim or point intended.</p>
<p class='rw-defn idx18' style='display:none'>When you saunter along, you are taking a stroll or slow walk from one place to another.</p>
<p class='rw-defn idx19' style='display:none'>Someone who has a sedentary habit, job, or lifestyle spends a lot of time sitting down without moving or exercising often.</p>
<p class='rw-defn idx20' style='display:none'>A serpentine figure has a winding or twisting form, much like that of a slithering snake.</p>
<p class='rw-defn idx21' style='display:none'>If you are somnolent, you are sleepy.</p>
<p class='rw-defn idx22' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx23' style='display:none'>If you are supine, you are lying on your back with your face upward.</p>
<p class='rw-defn idx24' style='display:none'>If your body is affected by torpor, you are severely lacking in energy; therefore, you are idle—and can even be numb.</p>
<p class='rw-defn idx25' style='display:none'>When you traverse something, such as the land or sea, you go across or travel through it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>meander</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='meander#' id='pronounce-sound' path='audio/words/amy-meander'></a>
mee-AN-der
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='meander#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='meander#' id='context-sound' path='audio/wordcontexts/brian-meander'></a>
Someday I would like to <em>meander</em> or wander about small towns in Vermont just to see what I could see, with no particular goal in mind.  I would like to <em>meander</em> much like the stream near my house twists and turns in its winding journey towards the sea.  <em>Meandering</em> or drifting about through these small towns would give me a great deal of relaxing pleasure, and who knows what I might find while doing so!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What do you do when you <em>meander</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
You walk around without a destination and are in no hurry to find one.
</li>
<li class='choice '>
<span class='result'></span>
You take an extensive trip for which you&#8217;ve planned every detail.
</li>
<li class='choice '>
<span class='result'></span>
You get lost in a crowd and have to ask for help to find your way out.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='meander#' id='definition-sound' path='audio/wordmeanings/amy-meander'></a>
When you <em>meander</em>, you either wander about with no particular goal or follow a path that twists and turns a great deal.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>wander</em>
</span>
</span>
</div>
<a class='quick-help' href='meander#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='meander#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/meander/memory_hooks/3772.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Me</span></span> W<span class="emp2"><span>ander</span></span></span></span> Watch <span class="emp1"><span>me</span></span> w<span class="emp2"><span>ander</span></span> as I <span class="emp1"><span>me</span></span><span class="emp2"><span>ander</span></span> aimlessly about the large central square.
</p>
</div>

<div id='memhook-button-bar'>
<a href="meander#" id="add-public-hook" style="" url="https://membean.com/mywords/meander/memory_hooks">Use other public hook</a>
<a href="meander#" id="memhook-use-own" url="https://membean.com/mywords/meander/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='meander#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Here wheat is grown, hard red and durum, and herds of beef cattle <b>meander</b> across far-ranging pastures, silhouetted against low horizons; here more than 40,000 shining combines work 63,000 well-kept farms.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
<li>
After clocking off, office workers <b>meander</b> through Windhoek, Namibia's well-kept streets toward an old warehouse.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
There is a great deal of pleasure in <b>meandering</b> through the Berkshires, in western Massachusetts, as my family and I found out last month—but only during daylight.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
The narrow roads that <b>meander</b> through the valleys and undulating chalky hills east of Tyre were places of terror Sunday as Israeli helicopters attacked vehicles fleeing Israel’s 12-day onslaught against south Lebanon.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/meander/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='meander#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>The word <em>meander</em> comes from the name of a winding river in Phrygia, the Maiandros.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='meander#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Shrink That Footprint</strong><span> Just meandering about.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/meander.jpg' video_url='examplevids/meander' video_width='350'></span>
<div id='wt-container'>
<img alt="Meander" height="288" src="https://cdn1.membean.com/video/examplevids/meander.jpg" width="350" />
<div class='center'>
<a href="meander#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='meander#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Meander" src="https://cdn0.membean.com/public/images/wordimages/cons2/meander.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='meander#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>ambulatory</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>excursion</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>gambol</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>itinerant</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>kinetic</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>peregrination</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>peripatetic</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>ramble</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>saunter</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>serpentine</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>traverse</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>catatonic</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>incarcerate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>indolent</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>inert</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>languid</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>lassitude</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>lethargic</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>listless</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>quarantine</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>quiescence</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>sedentary</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>somnolent</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>supine</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>torpor</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="meander" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>meander</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="meander#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

