
<!DOCTYPE html>
<html>
<head>
<title>Word: bohemian | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Bohemian-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/bohemian-large.jpg?qdep8" />
</div>
<a href="bohemian#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx1' style='display:none'>If something is an anomaly, it is different from what is usual or expected; hence, it is highly noticeable.</p>
<p class='rw-defn idx2' style='display:none'>An apostate has abandoned their religious faith, political party, or devotional cause.</p>
<p class='rw-defn idx3' style='display:none'>If something goes awry, it does not happen in the way that was planned.</p>
<p class='rw-defn idx4' style='display:none'>Bourgeois members of society are from the upper middle class and are typically conservative, traditional, and materialistic.</p>
<p class='rw-defn idx5' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx6' style='display:none'>A dissident is someone who disagrees publicly with a government, especially in a country where this is not allowed.</p>
<p class='rw-defn idx7' style='display:none'>An animal or person that is docile is not aggressive; rather, they are well-behaved, quiet, and easy to control.</p>
<p class='rw-defn idx8' style='display:none'>Someone is being dogmatic when they express opinions in an assertive and often overly self-important manner.</p>
<p class='rw-defn idx9' style='display:none'>If someone is eccentric, they behave in a strange and unusual way that is different from most people.</p>
<p class='rw-defn idx10' style='display:none'>A foible is a small weakness or character flaw in a person that is considered somewhat unusual; that said, it is also viewed as unimportant and harmless.</p>
<p class='rw-defn idx11' style='display:none'>A heretic is someone who doubts or acts in opposition to commonly or generally accepted beliefs.</p>
<p class='rw-defn idx12' style='display:none'>Heterodox beliefs, ideas, or practices are different from accepted or official ones.</p>
<p class='rw-defn idx13' style='display:none'>An iconoclast is someone who often attacks beliefs, ideas, or customs that are generally accepted by society.</p>
<p class='rw-defn idx14' style='display:none'>Idiosyncratic tendencies, behavior, or habits are unusual and strange.</p>
<p class='rw-defn idx15' style='display:none'>If you believe someone&#8217;s behavior exhibits idolatry, you think they blindly admire someone or something too much.</p>
<p class='rw-defn idx16' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx17' style='display:none'>Something that is incongruous is strange when considered along with its group because it seems very different from the other things in its group.</p>
<p class='rw-defn idx18' style='display:none'>A maverick individual does not follow general opinions of society as a whole; rather, they think for themselves and often strike out on their own.</p>
<p class='rw-defn idx19' style='display:none'>A mercurial person&#8217;s mind or mood changes often, quickly, and unpredictably.</p>
<p class='rw-defn idx20' style='display:none'>A nonconformist is unwilling to believe in the same things other people do or act in a fashion that society sets as a standard.</p>
<p class='rw-defn idx21' style='display:none'>A social norm is the standard, model, or rule by which people conduct themselves.</p>
<p class='rw-defn idx22' style='display:none'>A pariah is a social outcast.</p>
<p class='rw-defn idx23' style='display:none'>Someone has a parochial outlook when they are mostly concerned with narrow or limited issues that affect a small, local area or a very specific cause; they are also unconcerned with wider or more general issues.</p>
<p class='rw-defn idx24' style='display:none'>To be politic in a decision is to be socially wise and diplomatic.</p>
<p class='rw-defn idx25' style='display:none'>A radical solution to a problem goes to the heart or root of it; a radical solution is most often an extreme solution to the problem.</p>
<p class='rw-defn idx26' style='display:none'>A recluse is someone who chooses to live alone and deliberately avoids other people.</p>
<p class='rw-defn idx27' style='display:none'>A schism causes a group or organization to divide into two groups as a result of differences in their aims and beliefs.</p>
<p class='rw-defn idx28' style='display:none'>Sedition is the act of encouraging people to disobey and oppose the government currently in power.</p>
<p class='rw-defn idx29' style='display:none'>If you are staid, you are set in your ways; consequently, you are settled, serious, law-abiding, conservative, and traditional—and perhaps even a tad dull.</p>
<p class='rw-defn idx30' style='display:none'>Something statutory, such as the power given to a governor or president, is created, established, and controlled by rules and laws; hence, it has the full force of the law behind it and must be followed.</p>
<p class='rw-defn idx31' style='display:none'>A troglodyte is reclusive, severely lacking in social skills, and is out of step with current times.</p>
<p class='rw-defn idx32' style='display:none'>An unorthodox opinion is unusual, not customary, and goes against established ways of thinking.</p>
<p class='rw-defn idx33' style='display:none'>Something that is volatile can change easily and vary widely.</p>
<p class='rw-defn idx34' style='display:none'>If you act in a wayward fashion, you are very headstrong, independent, disobedient, unpredictable, and practically unmanageable.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>bohemian</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='bohemian#' id='pronounce-sound' path='audio/words/amy-bohemian'></a>
boh-HEE-mee-uhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='bohemian#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='bohemian#' id='context-sound' path='audio/wordcontexts/brian-bohemian'></a>
My unconventional great-aunt, who always questioned the ways of society, was an artist and freethinker: a real <em>bohemian</em>.  She lived in a small, remote cooperative that gathered <em>bohemians</em> together who were unhappy with the rules of society in general.  Her friends were all poets and writers and musicians who lived a <em>bohemian</em> lifestyle free from the constraints of society.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Which person is a <em>bohemian</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Karl, who believes he should be able to lead the lifestyle he wishes to—not the one that society dictates.
</li>
<li class='choice '>
<span class='result'></span>
Abe, who thinks that communities should freely debate ideas and work together to solve problems.
</li>
<li class='choice '>
<span class='result'></span>
Yanko, who has created a business plan to sell his art that includes a unique brand for himself.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='bohemian#' id='definition-sound' path='audio/wordmeanings/amy-bohemian'></a>
A <em>bohemian</em> is someone who lives outside mainstream society, often embracing an unconventional lifestyle of self-expression, creativity, and rebellion against conventional rules and ways of society.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>freethinker</em>
</span>
</span>
</div>
<a class='quick-help' href='bohemian#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='bohemian#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/bohemian/memory_hooks/6178.json'></span>
<p>
<span class="emp0"><span>Freddy Mercury's <span class="emp1"><span>Bohemian</span></span> Rhapsody</span></span> Not only was Freddy Mercury a <span class="emp1"><span>bohemian</span></span> even amongst rock stars, but he even wrote a song named <span class="emp1"><span>Bohemian</span></span> Rhapsody.
</p>
</div>

<div id='memhook-button-bar'>
<a href="bohemian#" id="add-public-hook" style="" url="https://membean.com/mywords/bohemian/memory_hooks">Use other public hook</a>
<a href="bohemian#" id="memhook-use-own" url="https://membean.com/mywords/bohemian/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='bohemian#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Jason and Johnny are presented as polar opposites: the well-heeled businessman who lives in a Los Angeles hilltop aerie versus the <b>bohemian</b>, who turns out to be a single dad and lives in a funky Venice canal district.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
Almost everyone in his [opera] company works multiple jobs—waiting tables, teaching music, writing for little publications. Their lives don't seem too different from the artsy <b>bohemians</b> in Puccini's opera.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/bohemian/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='bohemian#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>Tribes of roving gypsies, who often lived outside of established societies, were thought to have come from the land of &#8220;Bohemia.&#8221;</p>
<a href="bohemian#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> French</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='bohemian#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Bohemian" src="https://cdn0.membean.com/public/images/wordimages/cons2/bohemian.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='bohemian#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>anomaly</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>apostate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>awry</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>dissident</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>eccentric</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>foible</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>heretic</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>heterodox</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>iconoclast</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>idiosyncratic</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>incongruous</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>maverick</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>mercurial</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>nonconformist</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>pariah</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>radical</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>recluse</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>schism</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>sedition</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>troglodyte</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>unorthodox</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>volatile</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>wayward</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='4' class = 'rw-wordform notlearned'><span>bourgeois</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>docile</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>dogmatic</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>idolatry</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>norm</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>parochial</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>politic</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>staid</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>statutory</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='bohemian#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
bohemian
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not conventional</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="bohemian" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>bohemian</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="bohemian#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

