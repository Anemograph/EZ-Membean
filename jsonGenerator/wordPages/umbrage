
<!DOCTYPE html>
<html>
<head>
<title>Word: umbrage | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Someone who has an abrasive manner is unkind and rude, wearing away at you in an irritating fashion.</p>
<p class='rw-defn idx1' style='display:none'>You affront someone by openly and intentionally offending or insulting him.</p>
<p class='rw-defn idx2' style='display:none'>Apathy is a lack of interest or unenthusiastic involvement in an activity; there is no effort to change or improve it at all.</p>
<p class='rw-defn idx3' style='display:none'>Someone who has a bilious personality is highly irritable and bad-tempered.</p>
<p class='rw-defn idx4' style='display:none'>The adjective blithe indicates that someone does something casually or in a carefree fashion without much concern for the end result; as a result, they are happy and lighthearted.</p>
<p class='rw-defn idx5' style='display:none'>Someone who has a bristling personality is easily offended, annoyed, or angered.</p>
<p class='rw-defn idx6' style='display:none'>Someone in a buoyant mood is in good spirits.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is contumacious is purposely stubborn, contrary, or disobedient.</p>
<p class='rw-defn idx8' style='display:none'>A convivial atmosphere or occasion is friendly, pleasant, cheerful, and relaxed.</p>
<p class='rw-defn idx9' style='display:none'>A derogatory statement about another person is highly offensive, critical, uncomplimentary, and often insulting.</p>
<p class='rw-defn idx10' style='display:none'>A disaffected member of a group or organization is not satisfied with it; consequently, they feel little loyalty towards it.</p>
<p class='rw-defn idx11' style='display:none'>If something discomfits you, it makes you feel embarrassed, confused, uncomfortable, or frustrated.</p>
<p class='rw-defn idx12' style='display:none'>When someone feels disquiet about a situation, they feel very worried or nervous.</p>
<p class='rw-defn idx13' style='display:none'>If something enthralls you, it makes you so interested and excited that you give it all your attention.</p>
<p class='rw-defn idx14' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx15' style='display:none'>If you exult, you show great pleasure and excitement, especially about something you have achieved.</p>
<p class='rw-defn idx16' style='display:none'>If someone is fractious, they are easily upset or annoyed over unimportant things.</p>
<p class='rw-defn idx17' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx18' style='display:none'>If someone is imperturbable, they are always calm and not easily upset or disturbed by any situation, even dangerous ones.</p>
<p class='rw-defn idx19' style='display:none'>When you are indignant about something, you are angry or really annoyed about it.</p>
<p class='rw-defn idx20' style='display:none'>Insouciance is a lack of concern or worry for something that should be shown more careful attention or consideration.</p>
<p class='rw-defn idx21' style='display:none'>Someone who is jocular is cheerful and often makes jokes or tries to make people laugh.</p>
<p class='rw-defn idx22' style='display:none'>Malice is a type of hatred that causes a strong desire to harm others both physically and emotionally.</p>
<p class='rw-defn idx23' style='display:none'>If you nettle someone, you irritate or annoy them.</p>
<p class='rw-defn idx24' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx25' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx26' style='display:none'>Someone who is obstreperous is noisy, unruly, and difficult to control.</p>
<p class='rw-defn idx27' style='display:none'>A task or responsibility is onerous if you dislike having to do it because it is difficult or tiring.</p>
<p class='rw-defn idx28' style='display:none'>If you are pensive, you are deeply thoughtful, often in a sad and/or serious way.</p>
<p class='rw-defn idx29' style='display:none'>When you are piqued by something, either your curiosity is aroused by it or you feel resentment or anger towards it.</p>
<p class='rw-defn idx30' style='display:none'>If you are sanguine about a situation, especially a difficult one, you are confident and cheerful that everything will work out the way you want it to.</p>
<p class='rw-defn idx31' style='display:none'>A troglodyte is reclusive, severely lacking in social skills, and is out of step with current times.</p>
<p class='rw-defn idx32' style='display:none'>An untoward situation is something that is unfavorable, unfortunate, inappropriate, or troublesome.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>umbrage</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='umbrage#' id='pronounce-sound' path='audio/words/amy-umbrage'></a>
UHM-brij
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='umbrage#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='umbrage#' id='context-sound' path='audio/wordcontexts/brian-umbrage'></a>
Maria took <em>umbrage</em> or offense at her relatives&#8217; rudeness while visiting&#8212;not only did they noisily gobble down huge quantities of food, but they even had the nerve to criticize her cooking! Maria took great personal displeasure in their poor conduct and asked them to leave; they, in turn, took <em>umbrage</em> at being kicked out, showing considerable annoyance. Maria&#8217;s relatives said she was an unkind hostess; furthermore, they let her know that her manners left much to be desired. Taking <em>umbrage</em> at her relatives&#8217; unsociable and unseemly behavior, the resentful Maria decided to cut off all contact with them.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What does it mean if you take <em>umbrage</em> at someone&#8217;s statement?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
You find it disrespectful and irritating.
</li>
<li class='choice '>
<span class='result'></span>
You express a different opinion than it conveys.
</li>
<li class='choice '>
<span class='result'></span>
You intentionally make the statement seem silly and unconvincing.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='umbrage#' id='definition-sound' path='audio/wordmeanings/amy-umbrage'></a>
When you take <em>umbrage</em>, you take offense at what another has done.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>offense</em>
</span>
</span>
</div>
<a class='quick-help' href='umbrage#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='umbrage#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/umbrage/memory_hooks/6186.json'></span>
<p>
<img alt="Umbrage" src="https://cdn1.membean.com/public/images/wordimages/hook/umbrage.jpg?qdep8" />
<span class="emp0"><span><span class="emp1"><span>Umbr</span></span>ella <span class="emp2"><span>Rage</span></span></span></span> "What do you mean only girls are supposed to use sun <span class="emp1"><span>umbr</span></span>ellas?! I take <span class="emp1"><span>umb</span></span><span class="emp2"><span>rage</span></span> at that! I love this <span class="emp1"><span>umbr</span></span>ella! Why I oughta ..." WHACK!
</p>
</div>

<div id='memhook-button-bar'>
<a href="umbrage#" id="add-public-hook" style="" url="https://membean.com/mywords/umbrage/memory_hooks">Use other public hook</a>
<a href="umbrage#" id="memhook-use-own" url="https://membean.com/mywords/umbrage/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='umbrage#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Lafayette took <b>umbrage—just</b> gobs and gobs of <b>umbrage—at</b> the patriots' vilification of his countrymen for leaving Newport.
<span class='attribution'>&mdash; Sarah Vowell, American writer</span>
<img alt="Sarah vowell, american writer" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Sarah Vowell, American writer.jpg?qdep8" width="80" />
</li>
<li>
To understand how significant the Apollo system was, and why its tiny amount of raw processing power is irrelevant, you only have to listen to the OG computer programmer and volunteer NASA historian Frank O’Brien. . . . [I]t’s not surprising that O’Brien takes <b>umbrage</b> at the idea that a microwave or calculator could be considered “as powerful” as the Apollo computer.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
"Irregardless means not regardless. And that's not what you're trying to say at all. So why, in what context, would irregardless make sense? I can't understand it." The brouhaha regarding the word seems to have started last week when a popular Twitter user took <b>umbrage</b> at Merriam-Webster's listing, decrying the death of the English language.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
So what is Mr. Rabideau’s problem with all of this? Why does he take such <b>umbrage</b> to my assertion that the tourism-based economy does not serve the whole community? It’s just this: He specializes in high-priced housing for the well-heeled outsiders.
<cite class='attribution'>
&mdash;
Adirondack Daily Enterprise
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/umbrage/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='umbrage#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep5" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='umbr_shadow' data-tree-url='//cdn1.membean.com/public/data/treexml' href='umbrage#'>
<span class=''></span>
umbr
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>shade, shadow</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='age_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='umbrage#'>
<span class=''></span>
-age
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state</td>
</tr>
</table>
<p>You take <em>umbrage</em> when a &#8220;shadow&#8221; or &#8220;shade&#8221; is cast over you, darkening your outlook towards a person or situation.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='umbrage#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep5" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Umbrage" src="https://cdn3.membean.com/public/images/wordimages/cons2/umbrage.png?qdep5" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='umbrage#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abrasive</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>affront</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bilious</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>bristling</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>contumacious</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>derogatory</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>disaffected</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>discomfit</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>disquiet</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>fractious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>indignant</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>malice</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>nettle</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>obstreperous</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>onerous</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>pensive</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>pique</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>troglodyte</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>untoward</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>apathy</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>blithe</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>buoyant</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>convivial</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>enthrall</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>exult</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>imperturbable</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>insouciance</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>jocular</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>sanguine</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="umbrage" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>umbrage</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="umbrage#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

