
<!DOCTYPE html>
<html>
<head>
<title>Word: pander | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abase yourself, people respect you less because you act in a way that is beneath you; due to this behavior, you are lowered or reduced in rank, esteem, or reputation. </p>
<p class='rw-defn idx1' style='display:none'>When you adjure someone to do something, you persuade, eagerly appeal, or solemnly order them to do it.</p>
<p class='rw-defn idx2' style='display:none'>If someone beguiles you, you are charmed by and attracted to them; you can also be tricked or deceived by someone who beguiles you.</p>
<p class='rw-defn idx3' style='display:none'>Blandishments are words or actions that are pleasant and complimentary, intended to persuade someone to do something via a use of flattery.</p>
<p class='rw-defn idx4' style='display:none'>You cajole people by gradually persuading them to do something, usually by being very nice to them.</p>
<p class='rw-defn idx5' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx6' style='display:none'>You describe someone as a charlatan if they pretend to have special knowledge or skill that they don&#8217;t actually possess.</p>
<p class='rw-defn idx7' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx8' style='display:none'>If you denounce people or actions, you criticize them severely in public because you feel strongly that they are wrong or evil.</p>
<p class='rw-defn idx9' style='display:none'>If you deprecate something, you disapprove of it strongly.</p>
<p class='rw-defn idx10' style='display:none'>Someone who is disingenuous is not straightforward or is dishonest in what they say or do.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is epicurean derives great pleasure in material and sensual things, especially good food and drink.</p>
<p class='rw-defn idx12' style='display:none'>If you eschew something, you deliberately avoid doing it, especially for moral reasons.</p>
<p class='rw-defn idx13' style='display:none'>If you exhort someone to do something, you try very hard to persuade them to do it.</p>
<p class='rw-defn idx14' style='display:none'>A fete is a celebration or festival in honor of a special occasion.</p>
<p class='rw-defn idx15' style='display:none'>Hedonism is the belief that pleasure is important, so much so that life should be spent doing only things that one enjoys.</p>
<p class='rw-defn idx16' style='display:none'>Someone who is indulgent tends to let other people have what they want; someone can be kind to excess when being indulgent.</p>
<p class='rw-defn idx17' style='display:none'>If people try to ingratiate themselves with you, they try to get your approval by doing or saying things that they think will please you.</p>
<p class='rw-defn idx18' style='display:none'>If you malign someone, you say unpleasant things about them to damage their reputation.</p>
<p class='rw-defn idx19' style='display:none'>When you behave with moderation, you live in a balanced and measured way; you do nothing to excess.</p>
<p class='rw-defn idx20' style='display:none'>A mountebank is a smooth-talking, dishonest person who tricks and cheats people.</p>
<p class='rw-defn idx21' style='display:none'>If someone is being obsequious, they are trying so hard to please someone that they lack sincerity in their actions towards that person.</p>
<p class='rw-defn idx22' style='display:none'>If someone is ostracized from a group, its members deliberately refuse to talk or listen to them and do not allow them to take part in any of their social activities.</p>
<p class='rw-defn idx23' style='display:none'>If someone is <em>pilloried</em>, they are publicly criticized or ridiculed by a lot of people, especially in the media.</p>
<p class='rw-defn idx24' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx25' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx26' style='display:none'>You show rectitude if you behave or conduct yourself in an honest and morally correct manner.</p>
<p class='rw-defn idx27' style='display:none'>If you refrain from doing something, you do not do it—even though you want to.</p>
<p class='rw-defn idx28' style='display:none'>If someone regales you, they tell you stories and jokes to entertain you— and they could also serve you a wonderful feast.</p>
<p class='rw-defn idx29' style='display:none'>Revelry is a festive celebration that includes wild, noisy, and happy dancing, eating, and drinking.</p>
<p class='rw-defn idx30' style='display:none'>If something, such as food or drink, satiates you, it satisfies your need or desire so completely that you often feel that you have had too much.</p>
<p class='rw-defn idx31' style='display:none'>If you say that someone is servile, you don&#8217;t respect them because they are too obedient, too agreeable, and too willing to do anything for another person.</p>
<p class='rw-defn idx32' style='display:none'>Something that is specious seems to be good, sound, or correct but is actually wrong or fake.</p>
<p class='rw-defn idx33' style='display:none'>Something that stymies you presents an obstacle that prevents you from doing what you need or want to do.</p>
<p class='rw-defn idx34' style='display:none'>If you are subservient, you are too eager and willing to do what other people want and often put your own wishes aside.</p>
<p class='rw-defn idx35' style='display:none'>If you have a surfeit of something, you have much more than what you need.</p>
<p class='rw-defn idx36' style='display:none'>A sybarite is very fond of expensive and luxurious things; therefore, they are devoted to a life of pleasure.</p>
<p class='rw-defn idx37' style='display:none'>Sycophants praise people in authority or those who have considerable influence in order to seek some favor from them in return.</p>
<p class='rw-defn idx38' style='display:none'>If you show temperance, you limit yourself so that you don’t do too much of something; you act in a controlled and well-balanced way.</p>
<p class='rw-defn idx39' style='display:none'>An unctuous person acts in an overly deceptive manner that is obviously insincere because they want to convince you of something.</p>
<p class='rw-defn idx40' style='display:none'>A veneer is a thin layer, such as a thin sheet of expensive wood over a cheaper type of wood that gives a false appearance of higher quality; a person can also put forth a false front or veneer.</p>
<p class='rw-defn idx41' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx42' style='display:none'>The verity of something is the truth or reality of it.</p>
<p class='rw-defn idx43' style='display:none'>If you vilify people, you write or say bad things about them in an attempt to make them unpopular.</p>
<p class='rw-defn idx44' style='display:none'>Vituperative remarks are full of hate, anger, and cruel criticism.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>pander</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='pander#' id='pronounce-sound' path='audio/words/amy-pander'></a>
PAN-der
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='pander#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='pander#' id='context-sound' path='audio/wordcontexts/brian-pander'></a>
People complain that commercials <em>pander</em> or play to the baser desires of children. They <em>pander</em> by making toys and candies appear to be the most amazing things on Earth and send the message that to be truly happy and feel loved kids need those toys. Even video games <em>pander</em> to children by making them feel powerful and able to do things they really cannot do, thereby encouraging kids to buy them in order to exploit their love of play.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How can a speaker <em>pander</em> to a crowd?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
By doing something unexpected to get their attention.
</li>
<li class='choice answer '>
<span class='result'></span>
By trying to please them in order to gain their favor.
</li>
<li class='choice '>
<span class='result'></span>
By sharing some ways they can be better listeners.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='pander#' id='definition-sound' path='audio/wordmeanings/amy-pander'></a>
To <em>pander</em> is to cater or suck up to a person&#8217;s baser instincts in order to influence or exploit them in some way.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>cater basely</em>
</span>
</span>
</div>
<a class='quick-help' href='pander#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='pander#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/pander/memory_hooks/4146.json'></span>
<p>
<span class="emp0"><span>Ex<span class="emp3"><span>pand</span></span> the Market</span></span> Advertisers often ex<span class="emp3"><span>pand</span></span> their market by <span class="emp3"><span>pand</span></span>ering to their clients by falling all over them, or by appealing to their baser instincts, such as using beautiful women to sell cars.
</p>
</div>

<div id='memhook-button-bar'>
<a href="pander#" id="add-public-hook" style="" url="https://membean.com/mywords/pander/memory_hooks">Use other public hook</a>
<a href="pander#" id="memhook-use-own" url="https://membean.com/mywords/pander/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='pander#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Now forty-nine and settled into a life that makes sense to him, Springsteen no longer needs to <b>pander</b> to his audience as shamelessly as he once did.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
<li>
Some dictators <b>pander</b> to the industrialists and other elites who supply power in the form of resources and intelligence, and it’s at the proles' expense.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
This is exactly what they want, because now he can <b>pander</b> to the environmentalists and say, I'm going to shut it down because it’s too dangerous.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
Those who find Sarkozy’s new initiative crass say that identity questions <b>pander</b> to the French right wing, a vote that Sarkozy and his party will need to win elections next spring.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/pander/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='pander#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>&#8220;Pandarus&#8221; acted as a &#8220;go-between&#8221; in the love affair between Troilus and Criseyde in the Chaucer poem of the same name, thus furthering and increasing the desire that both lovers felt for each other, thus <em>pandering</em> to their desires.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='pander#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Late Show with Stephen Colbert</strong><span> Stephen Colbert is really pandering to the Chinese.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/pander.jpg' video_url='examplevids/pander' video_width='350'></span>
<div id='wt-container'>
<img alt="Pander" height="288" src="https://cdn1.membean.com/video/examplevids/pander.jpg" width="350" />
<div class='center'>
<a href="pander#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='pander#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Pander" src="https://cdn2.membean.com/public/images/wordimages/cons2/pander.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='pander#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abase</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>adjure</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>beguile</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>blandishment</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>cajole</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>charlatan</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>disingenuous</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>epicurean</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>exhort</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>fete</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>hedonism</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>indulgent</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>ingratiate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>mountebank</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>obsequious</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>regale</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>revelry</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>satiate</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>servile</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>specious</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>subservient</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>surfeit</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>sybarite</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>sycophant</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>unctuous</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>veneer</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='5' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>denounce</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>deprecate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>eschew</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>malign</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>moderation</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>ostracize</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>pillory</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>rectitude</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>refrain</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>stymie</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>temperance</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>vilify</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>vituperative</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="pander" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>pander</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="pander#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

