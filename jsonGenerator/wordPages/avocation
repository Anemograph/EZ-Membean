
<!DOCTYPE html>
<html>
<head>
<title>Word: avocation | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>The word aesthetic is used to talk about art, beauty, the study of beauty, and the appreciation of beautiful things.</p>
<p class='rw-defn idx1' style='display:none'>Apathy is a lack of interest or unenthusiastic involvement in an activity; there is no effort to change or improve it at all.</p>
<p class='rw-defn idx2' style='display:none'>A connoisseur is an appreciative, informed, and knowledgeable judge of the fine arts; they can also just have excellent or perceptive taste in the fine arts without being an expert.</p>
<p class='rw-defn idx3' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx4' style='display:none'>A deft movement or action is made quickly and with skill.</p>
<p class='rw-defn idx5' style='display:none'>A dilettante is someone who frequently pursues an interest in a subject; nevertheless, they never spend the time and effort to study it thoroughly enough to master it.</p>
<p class='rw-defn idx6' style='display:none'>Someone does something in a disinterested way when they have no personal involvement or attachment to the action.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx8' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is epicurean derives great pleasure in material and sensual things, especially good food and drink.</p>
<p class='rw-defn idx10' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx11' style='display:none'>Insouciance is a lack of concern or worry for something that should be shown more careful attention or consideration.</p>
<p class='rw-defn idx12' style='display:none'>If you do something in a lackadaisical way, you do it carelessly and without putting much effort into it—thereby showing that you are not really interested in what you&#8217;re doing.</p>
<p class='rw-defn idx13' style='display:none'>A neophyte is a person who is just beginning to learn a subject or skill—or how to do an activity of some kind.</p>
<p class='rw-defn idx14' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx15' style='display:none'>Something nonpareil has no equal because it is much better than all others of its kind or type.</p>
<p class='rw-defn idx16' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx17' style='display:none'>A paragon is an example of a thing or person at its or their very best.</p>
<p class='rw-defn idx18' style='display:none'>A perfunctory action is done with little care or interest; consequently, it is only completed because it is expected of someone to do so.</p>
<p class='rw-defn idx19' style='display:none'>A tyro has just begun learning something.</p>
<p class='rw-defn idx20' style='display:none'>A virtuoso is someone who is very skillful at something, especially playing a musical instrument.</p>
<p class='rw-defn idx21' style='display:none'>Someone who is zealous spends a lot of time, energy, and effort to support something—  notably that of a political or religious nature—because they believe in it very strongly.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>avocation</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='avocation#' id='pronounce-sound' path='audio/words/amy-avocation'></a>
av-oh-KAY-shuhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='avocation#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='avocation#' id='context-sound' path='audio/wordcontexts/brian-avocation'></a>
When I was young, one of my favorite <em>avocations</em> or hobbies was to go walking in the beautiful woods near the home where I grew up.  In time, this <em>avocation</em> turned into a true profession for me&#8212;I led tour groups throughout national forests.  I was pleased that this childhood love of mine could stay with me my entire life as an <em>avocation</em> that not only earned money but also satisfied my soul.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an <em>avocation</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A selfless job that inspires others to do what you do.
</li>
<li class='choice answer '>
<span class='result'></span>
A fascinating occupation that you really love to do.
</li>
<li class='choice '>
<span class='result'></span>
A complicated task that combines many of your talents.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='avocation#' id='definition-sound' path='audio/wordmeanings/amy-avocation'></a>
An <em>avocation</em> is an activity, such as a hobby, that you do because you are interested in it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>hobby</em>
</span>
</span>
</div>
<a class='quick-help' href='avocation#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='avocation#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/avocation/memory_hooks/6167.json'></span>
<p>
<span class="emp0"><span>Bob's Av<span class="emp2"><span>ocation</span></span> Causes Rel<span class="emp2"><span>ocation</span></span></span></span>  Bob became so interested in his av<span class="emp2"><span>ocation</span></span> of bobbing for apples that he, instead of sobbing, decided upon an av<span class="emp2"><span>ocation</span></span> rel<span class="emp2"><span>ocation</span></span> to Bobbaloosa, the apple bobbing capital of the world.
</p>
</div>

<div id='memhook-button-bar'>
<a href="avocation#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/avocation/memory_hooks">Use other hook</a>
<a href="avocation#" id="memhook-use-own" url="https://membean.com/mywords/avocation/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='avocation#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Judging from the number of community theater companies in the area, there is no shortage of those with an <b>avocation</b> for acting, singing and dancing.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Details about his <b>avocation</b> as a musician are disappointingly sparse, but his affection for the blues and gratitude for music’s saving grace [are] deeply watermarked into every page.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
What remains of another stroke patient’s <b>avocation—the</b> painstaking restoration of antique sleighs and buggies—is two albums of photos . . . .
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/avocation/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='avocation#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='a_away' data-tree-url='//cdn1.membean.com/public/data/treexml' href='avocation#'>
<span class=''></span>
a-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>away, from</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='vocat_called' data-tree-url='//cdn1.membean.com/public/data/treexml' href='avocation#'>
<span class=''></span>
vocat
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>called, called upon</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ion_act' data-tree-url='//cdn1.membean.com/public/data/treexml' href='avocation#'>
<span class=''></span>
-ion
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>act, state, or result of doing something</td>
</tr>
</table>
<p>One&#8217;s <em>avocation</em> is the state of being &#8220;called away&#8221; from one&#8217;s regular job to do something that you really enjoy.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='avocation#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Avocation" src="https://cdn2.membean.com/public/images/wordimages/cons2/avocation.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='avocation#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>aesthetic</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>connoisseur</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>deft</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>dilettante</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>epicurean</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>nonpareil</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>paragon</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>perfunctory</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>virtuoso</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>zealous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>apathy</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>disinterested</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>insouciance</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>lackadaisical</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>neophyte</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>tyro</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="avocation" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>avocation</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="avocation#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

