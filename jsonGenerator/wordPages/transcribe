
<!DOCTYPE html>
<html>
<head>
<title>Word: transcribe | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If someone in authority abolishes a law or practice, they formally put an end to it.</p>
<p class='rw-defn idx1' style='display:none'>If you abridge a book or play, you make it shorter by making cuts to the original.</p>
<p class='rw-defn idx2' style='display:none'>To bowdlerize a book, play, or other literary work is to remove parts of it that are considered indecent or unsuitable for family reading.</p>
<p class='rw-defn idx3' style='display:none'>Censure is written or verbal disapproval and harsh criticism of something that someone has done.</p>
<p class='rw-defn idx4' style='display:none'>A compendium is a detailed collection of information on a particular or specific subject, usually in a book.</p>
<p class='rw-defn idx5' style='display:none'>If you curtail something, you reduce or limit it.</p>
<p class='rw-defn idx6' style='display:none'>If you delineate something, such as an idea or situation or border, you describe it in great detail.</p>
<p class='rw-defn idx7' style='display:none'>To efface something is to erase or remove it completely from recognition or memory.</p>
<p class='rw-defn idx8' style='display:none'>If you emulate someone, you try to behave the same way they do because you admire them a great deal.</p>
<p class='rw-defn idx9' style='display:none'>If you expropriate something, you take it away for your own use although it does not belong to you; governments frequently expropriate private land to use for public purposes.</p>
<p class='rw-defn idx10' style='display:none'>If you expunge something, you completely remove it or do away with it.</p>
<p class='rw-defn idx11' style='display:none'>To expurgate part of a book, play, or other text is to remove parts of it before publishing because they are considered objectionable, harmful, or offensive.</p>
<p class='rw-defn idx12' style='display:none'>Two items are fungible if one can be exchanged for the other with no loss in inherent value; for example, one $10 bill and two $5 bills are fungible.</p>
<p class='rw-defn idx13' style='display:none'>If you gloss a difficult word, phrase, or other text, you provide an explanation for it in the form of a note.</p>
<p class='rw-defn idx14' style='display:none'>To obviate a problem is to eliminate it or do something that makes solving the problem unnecessary.</p>
<p class='rw-defn idx15' style='display:none'>An omnibus is a book containing a collection of stories or articles that have previously been printed separately.</p>
<p class='rw-defn idx16' style='display:none'>An opus is an important piece of artistic work by a writer, painter, musician, etc.; an opus can also be one in a series of numbered musical works by the same composer.</p>
<p class='rw-defn idx17' style='display:none'>Plagiarism is the copying of another person&#8217;s work, ideas, or words and pretending that you created or thought of them.</p>
<p class='rw-defn idx18' style='display:none'>If you reciprocate, you give something to someone because they have given something similar to you.</p>
<p class='rw-defn idx19' style='display:none'>A simulacrum is an image or representation of something that can be a true copy or may just have a vague similarity to it.</p>
<p class='rw-defn idx20' style='display:none'>Something transmutes when it changes from one form or state into another.</p>
<p class='rw-defn idx21' style='display:none'>If you transpose two things, you make them change places or reverse their normal order.</p>
<p class='rw-defn idx22' style='display:none'>If someone is unlettered, they are illiterate, unable to read and write.</p>
<p class='rw-defn idx23' style='display:none'>If you repeat something verbatim, you use the same words that were spoken or written.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>transcribe</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='transcribe#' id='pronounce-sound' path='audio/words/amy-transcribe'></a>
tran-SKRAHYB
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='transcribe#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='transcribe#' id='context-sound' path='audio/wordcontexts/brian-transcribe'></a>
Every year Gladys <em>transcribed</em> or copied out her favorite passages from poems in her beautiful handwriting onto her handmade holiday cards.  Gladys took a great deal of time to generously <em>transcribe</em> or transfer each stanza to those cards, writing them out line by line.  Her friends loved these beautiful cards, and they lovingly called her <em>transcribed</em>, written-out quotations &#8220;Literary Gifts from Gladys.&#8221;
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What are you doing if you <em>transcribe</em> something?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
You summarize its main points in your own words.
</li>
<li class='choice '>
<span class='result'></span>
You add colorful designs to it.
</li>
<li class='choice answer '>
<span class='result'></span>
You copy it down from another text.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='transcribe#' id='definition-sound' path='audio/wordmeanings/amy-transcribe'></a>
If you <em>transcribe</em> something, such as a speech or other text, you write it or type it in full.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>copy</em>
</span>
</span>
</div>
<a class='quick-help' href='transcribe#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='transcribe#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/transcribe/memory_hooks/4510.json'></span>
<p>
<span class="emp0"><span>De<span class="emp2"><span>scribe</span></span> the <span class="emp1"><span>Tran</span></span>sfer</span></span> Could you de<span class="emp2"><span>scribe</span></span> for me how you <span class="emp1"><span>tran</span></span>sferred or <span class="emp1"><span>tran</span></span><span class="emp2"><span>scribe</span></span>d the writing you copied so accurately?
</p>
</div>

<div id='memhook-button-bar'>
<a href="transcribe#" id="add-public-hook" style="" url="https://membean.com/mywords/transcribe/memory_hooks">Use other public hook</a>
<a href="transcribe#" id="memhook-use-own" url="https://membean.com/mywords/transcribe/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='transcribe#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Volunteers will <b>transcribe</b> the Freedmen’s Bureau Records held by the National Museum of African American History and Culture, helping to make them searchable, digitized resources accessible to all.
<cite class='attribution'>
&mdash;
The Philadelphia Inquirer
</cite>
</li>
<li>
"It mattered not that I was in the shower, but thankfully the mirror was sufficiently fogged that I was able to write the press release on the mirror with my finger." As Steinbrenner recited the release's content, Idelson cranked the hot water even more to keep the steam building and later grabbed pencil and paper to <b>transcribe</b> it off the mirror.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
In its weekly email blast last Sunday, the New London Maritime Society put out a call for help <b>transcribing</b> the 19th century whaler's journal the society's Custom House Maritime Museum acquired last year.
<cite class='attribution'>
&mdash;
The Day
</cite>
</li>
<li>
Thanks to _Los Angeles Times_ music critic Randall Roberts—who did yeoman’s work recording and <b>transcribing</b> the speech in full—Dylan’s elaborate (for him, anyway) public comments became the surprise story of Grammy weekend.
<cite class='attribution'>
&mdash;
The Commercial Appeal
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/transcribe/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='transcribe#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='trans_across' data-tree-url='//cdn1.membean.com/public/data/treexml' href='transcribe#'>
<span class='common'></span>
trans-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>across, through</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='scrib_write' data-tree-url='//cdn1.membean.com/public/data/treexml' href='transcribe#'>
<span class=''></span>
scrib
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>write</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='transcribe#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>To <em>transcribe</em> a document is to &#8220;write it across&#8221; to another sheet of paper.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='transcribe#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Primal Video</strong><span> One way to transcribe voice to text.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/transcribe.jpg' video_url='examplevids/transcribe' video_width='350'></span>
<div id='wt-container'>
<img alt="Transcribe" height="288" src="https://cdn1.membean.com/video/examplevids/transcribe.jpg" width="350" />
<div class='center'>
<a href="transcribe#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='transcribe#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Transcribe" src="https://cdn3.membean.com/public/images/wordimages/cons2/transcribe.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='transcribe#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='4' class = 'rw-wordform notlearned'><span>compendium</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>delineate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>emulate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>expropriate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>fungible</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>omnibus</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>opus</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>plagiarism</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>reciprocate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>simulacrum</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>transmute</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>transpose</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>verbatim</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abolish</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abridge</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bowdlerize</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>censure</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>curtail</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>efface</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>expunge</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>expurgate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>gloss</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>obviate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>unlettered</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="transcribe" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>transcribe</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="transcribe#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

