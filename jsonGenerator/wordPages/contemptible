
<!DOCTYPE html>
<html>
<head>
<title>Word: contemptible | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Contemptible-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/contemptible-large.jpg?qdep8" />
</div>
<a href="contemptible#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>If you abase yourself, people respect you less because you act in a way that is beneath you; due to this behavior, you are lowered or reduced in rank, esteem, or reputation. </p>
<p class='rw-defn idx1' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx2' style='display:none'>If you abhor something, you dislike it very much, usually because you think it&#8217;s immoral.</p>
<p class='rw-defn idx3' style='display:none'>The word abject emphasizes a very bad situation or quality, thereby making it even worse.</p>
<p class='rw-defn idx4' style='display:none'>If you abominate something, you hate it because you think it is extremely wrong and unacceptable.</p>
<p class='rw-defn idx5' style='display:none'>A situation or condition that is abysmal is extremely bad or of wretched quality.</p>
<p class='rw-defn idx6' style='display:none'>When you admonish someone, you tell them gently but with seriousness that they have done something wrong; you usually caution and advise them not to do it again.</p>
<p class='rw-defn idx7' style='display:none'>A benefaction is a charitable contribution of money or assistance that someone gives to a person or organization.</p>
<p class='rw-defn idx8' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx9' style='display:none'>Censure is written or verbal disapproval and harsh criticism of something that someone has done.</p>
<p class='rw-defn idx10' style='display:none'>To decry something is to speak against it and find fault with it.</p>
<p class='rw-defn idx11' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx12' style='display:none'>If you denounce people or actions, you criticize them severely in public because you feel strongly that they are wrong or evil.</p>
<p class='rw-defn idx13' style='display:none'>Depravity is behavior that is immoral, corrupt, or evil.</p>
<p class='rw-defn idx14' style='display:none'>If you say a person&#8217;s actions are despicable, you think they are extremely unpleasant or nasty.</p>
<p class='rw-defn idx15' style='display:none'>An egregious mistake, failure, or problem is an extremely bad and very noticeable one.</p>
<p class='rw-defn idx16' style='display:none'>Errant things or people behave in a way that is unacceptable or wrong, such as a missile that travels in the wrong direction or a student who does not follow classroom rules.</p>
<p class='rw-defn idx17' style='display:none'>A eulogy is a speech or other piece of writing, often part of a funeral, in which someone or something is highly praised.</p>
<p class='rw-defn idx18' style='display:none'>When you exalt another person, you either praise them highly or promote them to a higher position in an organization.</p>
<p class='rw-defn idx19' style='display:none'>An exemplary person is one that sets an ideal or praiseworthy example for others to follow.</p>
<p class='rw-defn idx20' style='display:none'>If you extol something or someone, you praise it or them very enthusiastically.</p>
<p class='rw-defn idx21' style='display:none'>An action that is flagrant shows that someone does not care if they obviously break the rules or highly offend people.</p>
<p class='rw-defn idx22' style='display:none'>If you describe something as heinous, you mean that is extremely evil, shocking, and bad.</p>
<p class='rw-defn idx23' style='display:none'>Ignominy is a dishonorable or shameful situation in which someone feels publicly embarrassed and loses the respect of others.</p>
<p class='rw-defn idx24' style='display:none'>If you describe someone&#8217;s appearance or behavior as impeccable, you mean that it is perfect and therefore impossible to criticize.</p>
<p class='rw-defn idx25' style='display:none'>If something is infallible, it is never wrong and so is incapable of making mistakes.</p>
<p class='rw-defn idx26' style='display:none'>An infamous person has a very bad reputation because they have done disgraceful or dishonorable things.</p>
<p class='rw-defn idx27' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx28' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx29' style='display:none'>A laudatory article or speech expresses praise or admiration for someone or something.</p>
<p class='rw-defn idx30' style='display:none'>When someone is lionized, they are treated as being very important or famous—although they may not deserve to be.</p>
<p class='rw-defn idx31' style='display:none'>Malfeasance is an unlawful act, especially one committed by a trusted public official.</p>
<p class='rw-defn idx32' style='display:none'>A nefarious activity is considered evil and highly dishonest.</p>
<p class='rw-defn idx33' style='display:none'>If something is noisome, it is extremely unpleasant, especially because it is very dirty or has an awful smell.</p>
<p class='rw-defn idx34' style='display:none'>Something that is noxious is harmful, extremely unpleasant, and often poisonous.</p>
<p class='rw-defn idx35' style='display:none'>If you describe people or things as odious, you think that they are extremely unpleasant.</p>
<p class='rw-defn idx36' style='display:none'>A paean is a piece of writing, verbal expression, or song that expresses enthusiastic praise, admiration, or joy.</p>
<p class='rw-defn idx37' style='display:none'>A panegyric is a speech or article that praises someone or something a lot.</p>
<p class='rw-defn idx38' style='display:none'>A paragon is an example of a thing or person at its or their very best.</p>
<p class='rw-defn idx39' style='display:none'>If someone is <em>pilloried</em>, they are publicly criticized or ridiculed by a lot of people, especially in the media.</p>
<p class='rw-defn idx40' style='display:none'>If you receive a plaudit, you receive admiration, praise, and approval from someone.</p>
<p class='rw-defn idx41' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx42' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx43' style='display:none'>If you think a type of behavior or idea is reprehensible, you think that it is very bad, morally wrong, and deserves to be strongly criticized.</p>
<p class='rw-defn idx44' style='display:none'>A reprobate has a bad character and behaves in an immoral or improper way.</p>
<p class='rw-defn idx45' style='display:none'>If you repudiate something, you state that you do not accept or agree with it and no longer want to be connected with it in any way.</p>
<p class='rw-defn idx46' style='display:none'>Repugnance is a strong feeling of dislike for something or someone you find horrible and offensive.</p>
<p class='rw-defn idx47' style='display:none'>Turpitude is the state of engaging in immoral or evil activities.</p>
<p class='rw-defn idx48' style='display:none'>To act in an uncouth manner is to be awkward and unmannerly.</p>
<p class='rw-defn idx49' style='display:none'>If you say someone&#8217;s behavior is unseemly, you disapprove of it because it is not in good taste or not suitable for a particular situation.</p>
<p class='rw-defn idx50' style='display:none'>Something that is unsullied is unstained and clean.</p>
<p class='rw-defn idx51' style='display:none'>Venerable people command respect because they are old and wise.</p>
<p class='rw-defn idx52' style='display:none'>Something vile is evil, very unpleasant, horrible, or extremely bad.</p>
<p class='rw-defn idx53' style='display:none'>If you vilify people, you write or say bad things about them in an attempt to make them unpopular.</p>
<p class='rw-defn idx54' style='display:none'>A wanton action deliberately harms someone or damages something for no apparent or good reason.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>contemptible</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='contemptible#' id='pronounce-sound' path='audio/words/amy-contemptible'></a>
kuhn-TEMP-tuh-buhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='contemptible#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='contemptible#' id='context-sound' path='audio/wordcontexts/brian-contemptible'></a>
Littering is <em>contemptible</em> or shameful&#8212;why anyone would throw his trash into the road is beyond all comprehension.  To think the rest of the world is there to pick up your trash is a <em>contemptible</em> or disgraceful way to live.  Even more <em>contemptible</em> or distasteful behavior is discarding oil and gasoline on bare ground or dumping it into a body of water, allowing it to contaminate the water supply.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is <em>contemptible</em> behavior?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is slightly impolite.
</li>
<li class='choice answer '>
<span class='result'></span>
It is incredibly mean.
</li>
<li class='choice '>
<span class='result'></span>
It is very noticeable.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='contemptible#' id='definition-sound' path='audio/wordmeanings/amy-contemptible'></a>
A <em>contemptible</em> act is shameful, disgraceful, and worthy of scorn.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>shameful</em>
</span>
</span>
</div>
<a class='quick-help' href='contemptible#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='contemptible#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/contemptible/memory_hooks/6078.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Can't</span></span> <span class="emp2"><span>Empty</span></span> That <span class="emp3"><span>Ble</span></span>ach</span></span> You <span class="emp1"><span>can't</span></span> <span class="emp2"><span>empty</span></span> all that <span class="emp3"><span>ble</span></span>ach into the ocean--that would be a <span class="emp1"><span>cont</span></span><span class="emp2"><span>empti</span></span><span class="emp3"><span>ble</span></span> act!
</p>
</div>

<div id='memhook-button-bar'>
<a href="contemptible#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/contemptible/memory_hooks">Use other hook</a>
<a href="contemptible#" id="memhook-use-own" url="https://membean.com/mywords/contemptible/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='contemptible#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
The meanest, most <b>contemptible</b> kind of praise is that which first speaks well of a man, and then qualifies it with a "but".
<span class='attribution'>&mdash; Henry Ward Beecher, nineteenth century religious leader</span>
<img alt="Henry ward beecher, nineteenth century religious leader" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Henry Ward Beecher, nineteenth century religious leader.jpg?qdep8" width="80" />
</li>
<li>
Some of the camps were under the command of [General] George S. Patton, a great man on the screen, a <b>contemptible</b> bigot in real life.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
The ad shows not just a disregard for the truth, but a calculated intent to deceive the voters of Maryland; it is <b>contemptible</b>.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/contemptible/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='contemptible#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='con_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='contemptible#'>
<span class='common'></span>
con-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td class='partform'>tempt</td>
<td>
&rarr;
</td>
<td class='meaning'>despise</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ible_handy' data-tree-url='//cdn1.membean.com/public/data/treexml' href='contemptible#'>
<span class=''></span>
-ible
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>handy, capable of</td>
</tr>
</table>
<p>When a person is acting in a <em>contemptible</em> fashion, they are &#8220;capable of being thoroughly despised&#8221; by others for what they have done.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='contemptible#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Contemptible Conduct</strong><span> Former Mayor Kilpatrick is going back to prison for a contemptible misrepresentation of his finances.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/contemptible.jpg' video_url='examplevids/contemptible' video_width='350'></span>
<div id='wt-container'>
<img alt="Contemptible" height="198" src="https://cdn1.membean.com/video/examplevids/contemptible.jpg" width="350" />
<div class='center'>
<a href="contemptible#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='contemptible#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Contemptible" src="https://cdn0.membean.com/public/images/wordimages/cons2/contemptible.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='contemptible#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abase</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abhor</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>abject</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>abominate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>abysmal</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>admonish</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>censure</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>decry</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>denounce</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>depravity</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>despicable</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>egregious</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>errant</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>flagrant</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>heinous</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>ignominy</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>infamous</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>malfeasance</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>nefarious</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>noisome</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>noxious</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>odious</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>pillory</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>reprehensible</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>reprobate</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>repudiate</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>repugnance</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>turpitude</span> &middot;</li><li data-idx='48' class = 'rw-wordform notlearned'><span>uncouth</span> &middot;</li><li data-idx='49' class = 'rw-wordform notlearned'><span>unseemly</span> &middot;</li><li data-idx='52' class = 'rw-wordform notlearned'><span>vile</span> &middot;</li><li data-idx='53' class = 'rw-wordform notlearned'><span>vilify</span> &middot;</li><li data-idx='54' class = 'rw-wordform notlearned'><span>wanton</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='7' class = 'rw-wordform notlearned'><span>benefaction</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>eulogy</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>exalt</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>exemplary</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>extol</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>impeccable</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>infallible</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>laudatory</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>lionize</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>paean</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>panegyric</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>paragon</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>plaudit</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='50' class = 'rw-wordform notlearned'><span>unsullied</span> &middot;</li><li data-idx='51' class = 'rw-wordform notlearned'><span>venerable</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='contemptible#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
contempt
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>looking down on someone; scorn</td>
</tr>
<tr>
<td class='wordform'>
<span>
contemptuous
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>exhibiting scorn</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="contemptible" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>contemptible</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="contemptible#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

