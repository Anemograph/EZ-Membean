
<!DOCTYPE html>
<html>
<head>
<title>Word: indulgent | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Indulgent-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/indulgent-large.jpg?qdep8" />
</div>
<a href="indulgent#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When you act with abandon, you give in to the impulse of the moment and behave in a wild, uncontrolled way.</p>
<p class='rw-defn idx1' style='display:none'>Someone who is abstemious avoids doing too much of something enjoyable, such as eating or drinking; rather, they consume in a moderate fashion.</p>
<p class='rw-defn idx2' style='display:none'>Abstinence is the practice of keeping away from or avoiding something you enjoy—such as the physical pleasures of excessive food and drink—usually for health or religious reasons.</p>
<p class='rw-defn idx3' style='display:none'>Something austere is simple and plain in its style; an austere person is strict and severe with themselves.</p>
<p class='rw-defn idx4' style='display:none'>Avarice is the extremely strong desire to have a lot of money and possessions.</p>
<p class='rw-defn idx5' style='display:none'>Braggadocio is an excessively proud way of talking about your achievements or possessions that can annoy others.</p>
<p class='rw-defn idx6' style='display:none'>If you condone someone&#8217;s behavior, you go along with it and provide silent support for it—despite having doubts about it.</p>
<p class='rw-defn idx7' style='display:none'>A decadent person has low moral standards and is more interested in pleasure than serious matters.</p>
<p class='rw-defn idx8' style='display:none'>If you describe a person, group, or civilization as effete, you mean it is weak, exhausted,  powerless, unproductive, and/or corrupt.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is epicurean derives great pleasure in material and sensual things, especially good food and drink.</p>
<p class='rw-defn idx10' style='display:none'>A fete is a celebration or festival in honor of a special occasion.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is frugal spends very little money—and even then only on things that are absolutely necessary.</p>
<p class='rw-defn idx12' style='display:none'>Hedonism is the belief that pleasure is important, so much so that life should be spent doing only things that one enjoys.</p>
<p class='rw-defn idx13' style='display:none'>An inebriated person has had too much alcohol; by extension, if someone is inebriated with an emotion or feeling, they are filled with it—and possibly excited or confused by it too.</p>
<p class='rw-defn idx14' style='display:none'>If something is inordinate, it is much larger in amount or degree than is normally expected.</p>
<p class='rw-defn idx15' style='display:none'>Largess is the generous act of giving money or presents to a large number of people.</p>
<p class='rw-defn idx16' style='display:none'>Lavish praise, giving, or a meal is rich, plentiful, or very generous; it can sometimes border on being too much.</p>
<p class='rw-defn idx17' style='display:none'>When you behave with moderation, you live in a balanced and measured way; you do nothing to excess.</p>
<p class='rw-defn idx18' style='display:none'>A monastic lifestyle is very simple—it is not given to excess in any way; rather, it is a lifestyle of self-denial.</p>
<p class='rw-defn idx19' style='display:none'>A plethora of something is too much of it.</p>
<p class='rw-defn idx20' style='display:none'>Someone who behaves in a prodigal way spends a lot of money and/or time carelessly and wastefully with no concern for the future.</p>
<p class='rw-defn idx21' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx22' style='display:none'>If someone regales you, they tell you stories and jokes to entertain you— and they could also serve you a wonderful feast.</p>
<p class='rw-defn idx23' style='display:none'>To renounce something, such as a position or practice, is to let go of it or reject it.</p>
<p class='rw-defn idx24' style='display:none'>Revelry is a festive celebration that includes wild, noisy, and happy dancing, eating, and drinking.</p>
<p class='rw-defn idx25' style='display:none'>If something, such as food or drink, satiates you, it satisfies your need or desire so completely that you often feel that you have had too much.</p>
<p class='rw-defn idx26' style='display:none'>A stricture is a rule or condition that imposes restrictions or limits on what you can do.</p>
<p class='rw-defn idx27' style='display:none'>Something that stymies you presents an obstacle that prevents you from doing what you need or want to do.</p>
<p class='rw-defn idx28' style='display:none'>Something that is sumptuous is impressive, grand, and very expensive.</p>
<p class='rw-defn idx29' style='display:none'>If you have a surfeit of something, you have much more than what you need.</p>
<p class='rw-defn idx30' style='display:none'>A sybarite is very fond of expensive and luxurious things; therefore, they are devoted to a life of pleasure.</p>
<p class='rw-defn idx31' style='display:none'>If you show temperance, you limit yourself so that you don’t do too much of something; you act in a controlled and well-balanced way.</p>
<p class='rw-defn idx32' style='display:none'>A feeling that is unbridled is enthusiastic and unlimited in its expression.</p>
<p class='rw-defn idx33' style='display:none'>A voracious person has a strong desire to want a lot of something, especially food.</p>
<p class='rw-defn idx34' style='display:none'>A wanton action deliberately harms someone or damages something for no apparent or good reason.</p>
<p class='rw-defn idx35' style='display:none'>A wastrel is a lazy person who wastes time and money.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>indulgent</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='indulgent#' id='pronounce-sound' path='audio/words/amy-indulgent'></a>
in-DUHL-juhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='indulgent#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='indulgent#' id='context-sound' path='audio/wordcontexts/brian-indulgent'></a>
When I was young my mother was a bit too <em>indulgent</em> or permissive when it came to giving me candy.  When we went to the store I would beg my mother to be <em>indulgent</em> or lenient in buying me all the candy that I wanted.  Now I wish that she hadn&#8217;t been so <em>indulgent</em> or generous since I have diabetes and a real weight problem.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How do <em>indulgent</em> people behave?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
They give in easily to other people&#8217;s requests.
</li>
<li class='choice '>
<span class='result'></span>
They like to spend time with the people they love.
</li>
<li class='choice '>
<span class='result'></span>
They tend to save money and rarely spend any on themselves.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='indulgent#' id='definition-sound' path='audio/wordmeanings/amy-indulgent'></a>
Someone who is <em>indulgent</em> tends to let other people have what they want; someone can be kind to excess when being <em>indulgent</em>.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>permissive</em>
</span>
</span>
</div>
<a class='quick-help' href='indulgent#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='indulgent#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/indulgent/memory_hooks/5299.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Dull</span></span> <span class="emp2"><span>Gent</span></span></span></span> You would be quite the <span class="emp3"><span>dull</span></span> <span class="emp2"><span>gent</span></span> if you were never in<span class="emp3"><span>dul</span></span><span class="emp2"><span>gent</span></span> when it came to treating yourself!
</p>
</div>

<div id='memhook-button-bar'>
<a href="indulgent#" id="add-public-hook" style="" url="https://membean.com/mywords/indulgent/memory_hooks">Use other public hook</a>
<a href="indulgent#" id="memhook-use-own" url="https://membean.com/mywords/indulgent/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='indulgent#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Even Abdul-Jabbar spent much of his career hearing complaints that he was—apart from his lone breakthrough with the Bucks—too self-<b>indulgent</b> to be a dominant winner.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
The <b>indulgent</b> parent centers his whole life around his children and wants to offer them the world by trying to remove as many obstacles to happiness as he can.
<cite class='attribution'>
&mdash;
Psychology Today
</cite>
</li>
<li>
The self-<b>indulgent</b> Clinton was an <b>indulgent</b> dog-parent, giving the animal the run of the Oval Office without a care in the world about priceless furniture coverings.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
Experts of the 1950s extolled self-sacrificing and <b>indulgent</b> mothers like those seen on TV.
<cite class='attribution'>
&mdash;
Slate
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/indulgent/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='indulgent#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>The word <em>indulgent</em> comes from a root word which means to &#8220;be lenient;&#8221; one &#8220;is lenient&#8221; when one readily <em>indulges</em> the wishes of others.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='indulgent#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Amadeus</strong><span> Mozart's father was too indulgent with his now spoiled son.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/indulgent.jpg' video_url='examplevids/indulgent' video_width='350'></span>
<div id='wt-container'>
<img alt="Indulgent" height="288" src="https://cdn1.membean.com/video/examplevids/indulgent.jpg" width="350" />
<div class='center'>
<a href="indulgent#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='indulgent#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Indulgent" src="https://cdn2.membean.com/public/images/wordimages/cons2/indulgent.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='indulgent#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abandon</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>avarice</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>braggadocio</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>condone</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>decadent</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>effete</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>epicurean</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>fete</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>hedonism</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>inebriated</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>inordinate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>largess</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>lavish</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>plethora</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>prodigal</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>regale</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>revelry</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>satiate</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>sumptuous</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>surfeit</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>sybarite</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>unbridled</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>voracious</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>wanton</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>wastrel</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>abstemious</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abstinence</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>austere</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>frugal</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>moderation</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>monastic</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>renounce</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>stricture</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>stymie</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>temperance</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='indulgent#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
indulge
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>treat; permit to have</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="indulgent" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>indulgent</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="indulgent#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

