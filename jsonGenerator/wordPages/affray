
<!DOCTYPE html>
<html>
<head>
<title>Word: affray | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Affray-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/affray-large.jpg?qdep8" />
</div>
<a href="affray#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>Bedlam is a situation with a great deal of noise and confusion.</p>
<p class='rw-defn idx1' style='display:none'>If you are bellicose, you behave in an aggressive way and are likely to start an argument or fight.</p>
<p class='rw-defn idx2' style='display:none'>Someone who has a bilious personality is highly irritable and bad-tempered.</p>
<p class='rw-defn idx3' style='display:none'>Someone who has a bristling personality is easily offended, annoyed, or angered.</p>
<p class='rw-defn idx4' style='display:none'>When someone is contentious, they are argumentative and likely to provoke a fight.</p>
<p class='rw-defn idx5' style='display:none'>A cudgel is a short, thick stick used for beating.</p>
<p class='rw-defn idx6' style='display:none'>A situation or thing that is discordant does not fit in with other things; therefore, it is disagreeable, strange, or unpleasant.</p>
<p class='rw-defn idx7' style='display:none'>An imbroglio is a very difficult, confusing, and complicated situation.</p>
<p class='rw-defn idx8' style='display:none'>Pandemonium is a very noisy and uncontrolled situation, especially one that is caused by a lot of angry or excited people.</p>
<p class='rw-defn idx9' style='display:none'>A polemic is a strong written or spoken statement that usually attacks or less often defends a particular idea, opinion, or belief.</p>
<p class='rw-defn idx10' style='display:none'>To act in a pugnacious manner is to act in a combative and aggressive way.</p>
<p class='rw-defn idx11' style='display:none'>A tumultuous event or period of time is filled with great excitement, confusion, or violence; a tumultuous reaction to something is likewise very loud and noisy because people are happy and excited.</p>
<p class='rw-defn idx12' style='display:none'>When you have a vehement feeling about something, you feel very strongly or intensely about it.</p>
<p class='rw-defn idx13' style='display:none'>A vendetta is a prolonged situation in which one person or group tries to harm another person or group—and vice versa.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 6
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>affray</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='affray#' id='pronounce-sound' path='audio/words/amy-affray'></a>
uh-FRAY
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='affray#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='affray#' id='context-sound' path='audio/wordcontexts/brian-affray'></a>
The ancient Romans used to have many <em>affrays</em> or noisy fights during elections.  If certain senators didn&#8217;t like particular candidates who were making speeches, they would hire ex-gladiators to start an <em>affray</em> by attacking those candidates and their supporters in the Roman Forum.  Once everyone was involved in the <em>affray</em> or brawl, the election could not go on as planned and people would end up rethinking their votes on the next day.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an affray?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is a way in which the Romans held elections.
</li>
<li class='choice answer '>
<span class='result'></span>
It is an open and very often violent disturbance in public.
</li>
<li class='choice '>
<span class='result'></span>
It is a ceremonial combat held to celebrate an election.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='affray#' id='definition-sound' path='audio/wordmeanings/amy-affray'></a>
An <em>affray</em> is a noisy and aggressive disagreement or fight in public.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>public fight</em>
</span>
</span>
</div>
<a class='quick-help' href='affray#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='affray#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/affray/memory_hooks/116533.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Afr</span></span>aid to S<span class="emp3"><span>ay</span></span></span></span> I'm <span class="emp1"><span>afra</span></span>id to s<span class="emp3"><span>ay</span></span> my opinion, for I fear it will lead to an <span class="emp1"><span>affr</span></span><span class="emp3"><span>ay</span></span> between my teacher and myself.
</p>
</div>

<div id='memhook-button-bar'>
<a href="affray#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/affray/memory_hooks">Use other hook</a>
<a href="affray#" id="memhook-use-own" url="https://membean.com/mywords/affray/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='affray#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The pre-eminent leader of the opposition, Anwar Ibrahim, was shoved to the ground and injured in the <b>affray</b>.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Great excitement has existed in and around Port Ewen, Ulster County, during the last two or three days, in consequence of a shocking and bloody <b>affray</b> which occurred on the ice opposite that village on Saturday afternoon last, about 3 o'clock.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/affray/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='affray#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ex_out' data-tree-url='//cdn1.membean.com/public/data/treexml' href='affray#'>
<span class='common'></span>
ex-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>out of, from, off</td>
</tr>
</table>
<p>The word &#8220;affray&#8221; probably comes from the Latin root <em>ex</em>, &#8220;out of,&#8221; and a Frankish root word for &#8220;peace.&#8221;  Hence, one is &#8220;out of peace&#8221; when involved in an &#8220;affray.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='affray#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Outside the Revolution Bar</strong><span> This is quite an extended affray along this sidewalk!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/affray.jpg' video_url='examplevids/affray' video_width='350'></span>
<div id='wt-container'>
<img alt="Affray" height="288" src="https://cdn1.membean.com/video/examplevids/affray.jpg" width="350" />
<div class='center'>
<a href="affray#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='affray#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Affray" src="https://cdn0.membean.com/public/images/wordimages/cons2/affray.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='affray#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>bedlam</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>bellicose</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>bilious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bristling</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>contentious</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>cudgel</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>discordant</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>imbroglio</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>pandemonium</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>polemic</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>pugnacious</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>tumultuous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>vehement</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>vendetta</span> &middot;</li></ul>
<ul class='related-ants'></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="affray" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>affray</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="affray#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

