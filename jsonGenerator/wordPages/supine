
<!DOCTYPE html>
<html>
<head>
<title>Word: supine | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Supine-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/supine-large.jpg?qdep8" />
</div>
<a href="supine#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>The word abject emphasizes a very bad situation or quality, thereby making it even worse.</p>
<p class='rw-defn idx1' style='display:none'>If you acquiesce to something, you allow it to happen by doing what someone wants without putting up a struggle or voicing your own concerns.</p>
<p class='rw-defn idx2' style='display:none'>When you cavort, you jump and dance around in a playful, excited, or physically lively way.</p>
<p class='rw-defn idx3' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx4' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx5' style='display:none'>If something enervates you, it makes you very tired and weak—almost to the point of collapse.</p>
<p class='rw-defn idx6' style='display:none'>If someone gambols, they run, jump, and play like a young child or animal.</p>
<p class='rw-defn idx7' style='display:none'>An indolent person is lazy.</p>
<p class='rw-defn idx8' style='display:none'>When something is inert, it has no power of movement or is inactive.</p>
<p class='rw-defn idx9' style='display:none'>Something kinetic is moving, active, and using energy.</p>
<p class='rw-defn idx10' style='display:none'>If you do something in a lackadaisical way, you do it carelessly and without putting much effort into it—thereby showing that you are not really interested in what you&#8217;re doing.</p>
<p class='rw-defn idx11' style='display:none'>A languid person is slow, relaxed, and shows little energy or interest in doing anything.</p>
<p class='rw-defn idx12' style='display:none'>If you are lethargic, you are tired, lack energy, and are unwilling to exert effort.</p>
<p class='rw-defn idx13' style='display:none'>When you are listless, you lack energy and interest and are unwilling to exert any effort.</p>
<p class='rw-defn idx14' style='display:none'>When you meander, you either wander about with no particular goal or follow a path that twists and turns a great deal.</p>
<p class='rw-defn idx15' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx16' style='display:none'>Someone who is obstreperous is noisy, unruly, and difficult to control.</p>
<p class='rw-defn idx17' style='display:none'>Someone who is phlegmatic stays calm and unemotional even in dangerous or exciting situations.</p>
<p class='rw-defn idx18' style='display:none'>If you are prone to doing something, you are likely or inclined to do it.</p>
<p class='rw-defn idx19' style='display:none'>A recalcitrant animal or person is difficult to control and refuses to obey orders—even after stiff punishment.</p>
<p class='rw-defn idx20' style='display:none'>A recumbent figure or person is lying down.</p>
<p class='rw-defn idx21' style='display:none'>Refractory people deliberately don&#8217;t obey someone in authority and so are difficult to deal with or control.</p>
<p class='rw-defn idx22' style='display:none'>A restive person is not willing or able to keep still or be patient because they are bored or dissatisfied with something; consequently, they are becoming difficult to control.</p>
<p class='rw-defn idx23' style='display:none'>When you saunter along, you are taking a stroll or slow walk from one place to another.</p>
<p class='rw-defn idx24' style='display:none'>If you are somnolent, you are sleepy.</p>
<p class='rw-defn idx25' style='display:none'>If you are subservient, you are too eager and willing to do what other people want and often put your own wishes aside.</p>
<p class='rw-defn idx26' style='display:none'>If your body is affected by torpor, you are severely lacking in energy; therefore, you are idle—and can even be numb.</p>
<p class='rw-defn idx27' style='display:none'>Trenchant comments or criticisms are expressed forcefully, directly, and clearly, even though they may be hurtful to the receiver.</p>
<p class='rw-defn idx28' style='display:none'>A vibrant person is lively and full of energy in a way that is exciting and attractive.</p>
<p class='rw-defn idx29' style='display:none'>If someone is described as vivacious, they are lively and have a happy, lighthearted manner.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>supine</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='supine#' id='pronounce-sound' path='audio/words/amy-supine'></a>
SOO-pahyn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='supine#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='supine#' id='context-sound' path='audio/wordcontexts/brian-supine'></a>
In a humble response to the larger, older dog, the young, eager puppy stretched out on her back in a <em>supine</em> position, belly and snout to the sky.  She held her <em>supine</em> body still on its back with her paws in the air as the dominant, assertive dog wagged its tail in approval.  After clarifying who was in charge with her inactive, <em>supine</em> pose, the puppy then proceeded to race energetically in circles, much to the older dog&#8217;s annoyance.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Which of these people is in a <em>supine</em> position?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A yoga instructor who is bent in half at the waist during a stretch.
</li>
<li class='choice answer '>
<span class='result'></span>
A man lying on his back on an operating table during surgery.
</li>
<li class='choice '>
<span class='result'></span>
A child hanging upside down by their knees from a tree branch.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='supine#' id='definition-sound' path='audio/wordmeanings/amy-supine'></a>
If you are <em>supine</em>, you are lying on your back with your face upward.
</li>
<li class='def-text'>
If you behave in a <em>supine</em> way, you are passive or inactive, allowing other people to make decisions for you.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>lying on your back</em>
</span>
</span>
</div>
<a class='quick-help' href='supine#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='supine#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/supine/memory_hooks/3336.json'></span>
<p>
<span class="emp0"><span>T<span class="emp3"><span>su</span></span>nami <span class="emp2"><span>Pine</span></span>s</span></span> After the t<span class="emp3"><span>su</span></span>nami had whipped through the coastline, all the fallen <span class="emp2"><span>pine</span></span>s were lying <span class="emp3"><span>su</span></span><span class="emp2"><span>pine</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="supine#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/supine/memory_hooks">Use other hook</a>
<a href="supine#" id="memhook-use-own" url="https://membean.com/mywords/supine/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='supine#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Luge is essentially sledding at the Olympic level. . . . I eventually even learned to fairly smoothly transition from the seated start position into the <b>supine</b> riding position I’d be assuming for the length of the run.
<cite class='attribution'>
&mdash;
Slate
</cite>
</li>
<li>
For a child of the 1940s or ’50s, polio meant the same thing whether you contracted it or not: confinement. . . . The children lying <b>supine</b> in iron lungs could see only what was on either side of their head (a line of patients telescoping down the ward) or reflected in mirrors mounted overhead (the floor’s tessellation of bleached tiles).
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
You could visit the aft cabin by lying <b>supine</b> on a wheeled cart and pulling yourself along an overhead rope through a tunnel 85 feet long and two feet in diameter.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
Wearing black Wayfarers, he stared into a cloudless blue sky from a <b>supine</b> position in one of the island’s 50 new red-rope hammocks, the much-publicized centerpiece of a 30-acre expansion this summer.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/supine/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='supine#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>supin</td>
<td>
&rarr;
</td>
<td class='meaning'>lie on one's back</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='supine#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>When one is <em>supine</em>, one &#8220;lies on one&#8217;s back.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='supine#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Star Trek the Next Generation</strong><span> Geordi is supine.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/supine.jpg' video_url='examplevids/supine' video_width='350'></span>
<div id='wt-container'>
<img alt="Supine" height="288" src="https://cdn1.membean.com/video/examplevids/supine.jpg" width="350" />
<div class='center'>
<a href="supine#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='supine#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Supine" src="https://cdn1.membean.com/public/images/wordimages/cons2/supine.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='supine#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abject</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>acquiesce</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>enervate</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>indolent</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>inert</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>lackadaisical</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>languid</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>lethargic</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>listless</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>phlegmatic</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>prone</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>recumbent</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>somnolent</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>subservient</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>torpor</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>cavort</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>gambol</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>kinetic</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>meander</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>obstreperous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>recalcitrant</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>refractory</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>restive</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>saunter</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>trenchant</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>vibrant</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>vivacious</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="supine" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>supine</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="supine#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

