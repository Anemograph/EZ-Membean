
<!DOCTYPE html>
<html>
<head>
<title>Word: lithe | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Lithe-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/lithe-large.jpg?qdep8" />
</div>
<a href="lithe#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>If something moves or grows with celerity, it does so rapidly.</p>
<p class='rw-defn idx1' style='display:none'>The choreography of a work of dance is either the art of arranging the steps or the actual dance composition itself.</p>
<p class='rw-defn idx2' style='display:none'>The word corporeal refers to the physical or material world rather than the spiritual; it also means characteristic of the body rather than the mind or feelings.</p>
<p class='rw-defn idx3' style='display:none'>Someone who is corpulent is extremely fat.</p>
<p class='rw-defn idx4' style='display:none'>Efficacy is the ability or power to produce an expected effect or result.</p>
<p class='rw-defn idx5' style='display:none'>If someone behaves in a furtive way, they do things sneakily and secretly in order to avoid being noticed.</p>
<p class='rw-defn idx6' style='display:none'>A hapless person is unlucky, so much so that a stream of unfortunate things has happened and continues to happen to them.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is limber can stretch, move, or bend themselves in many different directions or ways.</p>
<p class='rw-defn idx8' style='display:none'>If a dancer is lissome, she moves gracefully and is very flexible.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is malleable is easily influenced or controlled by other people.</p>
<p class='rw-defn idx10' style='display:none'>A person who is plastic can be easily influenced and molded by others.</p>
<p class='rw-defn idx11' style='display:none'>Ponderous writing or speech is boring, highly serious, and seems very long and wordy; it definitely lacks both grace and style.</p>
<p class='rw-defn idx12' style='display:none'>Something or someone that shows resilience is able to recover quickly and easily from unpleasant, difficult, and damaging situations or events.</p>
<p class='rw-defn idx13' style='display:none'>If someone is rotund, they have a round and fat body.</p>
<p class='rw-defn idx14' style='display:none'>If you sidle, you walk slowly, cautiously and often sideways in a particular direction, usually because you do not want to be noticed.</p>
<p class='rw-defn idx15' style='display:none'>A svelte person, most often a female, describes one who is attractive, slender, and possesses a graceful figure.</p>
<p class='rw-defn idx16' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx17' style='display:none'>Something unfeasible cannot be made or achieved.</p>
<p class='rw-defn idx18' style='display:none'>If you say someone&#8217;s behavior is unseemly, you disapprove of it because it is not in good taste or not suitable for a particular situation.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>lithe</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='lithe#' id='pronounce-sound' path='audio/words/amy-lithe'></a>
lahyth
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='lithe#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='lithe#' id='context-sound' path='audio/wordcontexts/brian-lithe'></a>
The <em>lithe</em> panther crept gracefully and silently among the night shadows.  Under the full moon, the panther&#8217;s <em>lithe</em> and quick movements were lit, as if the creature were dancing on stage.  Such <em>lithe</em>, controlled, flexible, and fluid movements enabled the predator to sneak up on her prey with ease and swiftness.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of someone who is <em>lithe</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A thief who waits patiently until a house is empty before breaking into it.
</li>
<li class='choice answer '>
<span class='result'></span>
A dancer who moves across stage in a smooth and elegant way.
</li>
<li class='choice '>
<span class='result'></span>
A small child who can easily be picked up and carried by an adult.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='lithe#' id='definition-sound' path='audio/wordmeanings/amy-lithe'></a>
Someone with a <em>lithe</em> body can move easily and gracefully.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>flexible</em>
</span>
</span>
</div>
<a class='quick-help' href='lithe#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='lithe#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/lithe/memory_hooks/6161.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Lit</span></span> <span class="emp3"><span>He</span></span></span></span> After I <span class="emp1"><span>lit</span></span> <span class="emp3"><span>he</span></span>, you should have seen his <span class="emp1"><span>lit</span></span><span class="emp3"><span>he</span></span> movements!
</p>
</div>

<div id='memhook-button-bar'>
<a href="lithe#" id="add-public-hook" style="" url="https://membean.com/mywords/lithe/memory_hooks">Use other public hook</a>
<a href="lithe#" id="memhook-use-own" url="https://membean.com/mywords/lithe/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='lithe#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Li and Sang sank the women’s field with four 10s on their second effort [at the Olympic women's synchronized platform dive], creating hardly a splash as their <b>lithe</b> bodies knifed through the water.
<cite class='attribution'>
&mdash;
ESPN
</cite>
</li>
<li>
[The rapper Le1f] whips his glow-in-the-dark hair back and forth, and moves on stage like a dancer—all <b>lithe</b>, coiled energy and imminent shirtlessness, with aggression that never overwhelms his playful demeanor.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Yet lyrics aside, her <b>lithe</b> vocals are sometimes reward enough, set against a textured backdrop that emphasizes acoustic guitar, strings and percussion.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/lithe/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='lithe#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>From a root word meaning &#8220;flexible.&#8221;</p>
<a href="lithe#"><img alt="origin button" id="world-btn" src="https://membean.com/images/world-special.png?qdep3" /></a>
<div id='wordorigin-map-container' style='display:none'>
<p id='wordorigin-desc'>
origin: <strong> Old English</strong>
<span id='wordorigin-map'></span>
</p>
<p id='wordorigin-warning'>
Knowing language of origin or etymology is not important; but is sometimes interesting enough to make the word memorable. Only such words have associated maps.
</p>
</div>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='lithe#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Atelier ID)</strong><span> These breakdancers are all impressively lithe.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/lithe.jpg' video_url='examplevids/lithe' video_width='350'></span>
<div id='wt-container'>
<img alt="Lithe" height="288" src="https://cdn1.membean.com/video/examplevids/lithe.jpg" width="350" />
<div class='center'>
<a href="lithe#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='lithe#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Lithe" src="https://cdn3.membean.com/public/images/wordimages/cons2/lithe.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='lithe#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>celerity</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>choreography</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>efficacy</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>furtive</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>limber</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>lissome</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>malleable</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>plastic</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>resilience</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>sidle</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>svelte</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>corporeal</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>corpulent</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>hapless</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>ponderous</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>rotund</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>unfeasible</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>unseemly</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="lithe" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>lithe</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="lithe#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

