
<!DOCTYPE html>
<html>
<head>
<title>Word: fulsome | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If your behavior or manner is altruistic, you show you care more about other people and their interests than you care about yourself.</p>
<p class='rw-defn idx1' style='display:none'>When you employ artifice, you use clever tricks and cunning to deceive someone.</p>
<p class='rw-defn idx2' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx3' style='display:none'>If you say a person&#8217;s actions are despicable, you think they are extremely unpleasant or nasty.</p>
<p class='rw-defn idx4' style='display:none'>Someone who is disingenuous is not straightforward or is dishonest in what they say or do.</p>
<p class='rw-defn idx5' style='display:none'>Someone who is effusive expresses happiness, pleasure, admiration, praise, etc., in an extremely enthusiastic way.</p>
<p class='rw-defn idx6' style='display:none'>An egregious mistake, failure, or problem is an extremely bad and very noticeable one.</p>
<p class='rw-defn idx7' style='display:none'>If someone behaves in a furtive way, they do things sneakily and secretly in order to avoid being noticed.</p>
<p class='rw-defn idx8' style='display:none'>Grandiloquent speech is highly formal, exaggerated, and often seems both silly and hollow because it is expressly used to appear impressive and important.</p>
<p class='rw-defn idx9' style='display:none'>People who are ingenuous are excessively trusting and believe almost everything that people tell them, especially because they have not had much life experience.</p>
<p class='rw-defn idx10' style='display:none'>Something innocuous is not likely to offend or harm anyone.</p>
<p class='rw-defn idx11' style='display:none'>When you are acting in a melodramatic way, you are overreacting to something in an overly dramatic and exaggerated way.</p>
<p class='rw-defn idx12' style='display:none'>When you behave with moderation, you live in a balanced and measured way; you do nothing to excess.</p>
<p class='rw-defn idx13' style='display:none'>If something is noisome, it is extremely unpleasant, especially because it is very dirty or has an awful smell.</p>
<p class='rw-defn idx14' style='display:none'>If someone is being obsequious, they are trying so hard to please someone that they lack sincerity in their actions towards that person.</p>
<p class='rw-defn idx15' style='display:none'>If you describe an action as ostentatious, you think it is an extreme and exaggerated way of impressing people.</p>
<p class='rw-defn idx16' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx17' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx18' style='display:none'>If you think a type of behavior or idea is reprehensible, you think that it is very bad, morally wrong, and deserves to be strongly criticized.</p>
<p class='rw-defn idx19' style='display:none'>Rhetoric is the skill or art of using language to persuade or influence people, especially language that sounds impressive but may not be sincere or honest.</p>
<p class='rw-defn idx20' style='display:none'>If you say that someone is servile, you don&#8217;t respect them because they are too obedient, too agreeable, and too willing to do anything for another person.</p>
<p class='rw-defn idx21' style='display:none'>Something that is specious seems to be good, sound, or correct but is actually wrong or fake.</p>
<p class='rw-defn idx22' style='display:none'>If you are subservient, you are too eager and willing to do what other people want and often put your own wishes aside.</p>
<p class='rw-defn idx23' style='display:none'>If you employ subterfuge, you use a secret plan or action to get what you want by outwardly doing one thing that cleverly hides your true intentions.</p>
<p class='rw-defn idx24' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx25' style='display:none'>An unctuous person acts in an overly deceptive manner that is obviously insincere because they want to convince you of something.</p>
<p class='rw-defn idx26' style='display:none'>A veneer is a thin layer, such as a thin sheet of expensive wood over a cheaper type of wood that gives a false appearance of higher quality; a person can also put forth a false front or veneer.</p>
<p class='rw-defn idx27' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx28' style='display:none'>The verity of something is the truth or reality of it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>fulsome</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='fulsome#' id='pronounce-sound' path='audio/words/amy-fulsome'></a>
FUHL-suhm
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='fulsome#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='fulsome#' id='context-sound' path='audio/wordcontexts/brian-fulsome'></a>
The <em>fulsome</em> or fake flattery of the used car salesman was too much.  He gave Suzy <em>fulsome</em>, excessive compliments to put her in a good mood for making a purchase.  Then the salesman continued on by exaggerating about the car he was trying to sell her in a <em>fulsome</em>, overdone manner as well.  These <em>fulsome</em>, insincere comments made Suzy both uncomfortable and suspicious, which caused her to walk away from the deal.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>fulsome</em> compliment?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Telling someone they look incredibly amazing in an overly flattering tone of voice.
</li>
<li class='choice '>
<span class='result'></span>
Telling someone they look nice because they expect you to tell them that.
</li>
<li class='choice '>
<span class='result'></span>
Telling someone they look very nice in a quiet voice that no one else can hear.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='fulsome#' id='definition-sound' path='audio/wordmeanings/amy-fulsome'></a>
Praise, an apology, or gratitude is <em>fulsome</em> if it is so exaggerated and elaborate that it does not seem sincere.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>overdone</em>
</span>
</span>
</div>
<a class='quick-help' href='fulsome#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='fulsome#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/fulsome/memory_hooks/5070.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Ful</span></span>l and Then <span class="emp1"><span>Some</span></span></span></span> Sue's <span class="emp2"><span>ful</span></span><span class="emp1"><span>some</span></span> compliments of Steve were <span class="emp2"><span>ful</span></span>l and then <span class="emp1"><span>some</span></span>, but her <span class="emp2"><span>ful</span></span><span class="emp1"><span>some</span></span> flattery turned Steve on, since he was extraordinarily vain.
</p>
</div>

<div id='memhook-button-bar'>
<a href="fulsome#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/fulsome/memory_hooks">Use other hook</a>
<a href="fulsome#" id="memhook-use-own" url="https://membean.com/mywords/fulsome/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='fulsome#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
European leaders marked Reagan’s death with unusually <b>fulsome</b> praise, crediting him also with setting the stage for Europe’s reunification, celebrated in last month’s historic expansion of the European Union to include eight members of the former Soviet bloc.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
This is how the U.S. team members are, constantly building one another up, but with candor, not <b>fulsome</b> praise.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/fulsome/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='fulsome#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ful_full' data-tree-url='//cdn1.membean.com/public/data/treexml' href='fulsome#'>
<span class=''></span>
ful
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>full</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='some_having' data-tree-url='//cdn1.membean.com/public/data/treexml' href='fulsome#'>
<span class=''></span>
-some
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>having a certain quality</td>
</tr>
</table>
<p>From a root word meaning &#8220;abundant;&#8221; <em>fulsome</em> comments are too &#8220;abundant&#8221; in their use of excessive flattery.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='fulsome#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Fulsome" src="https://cdn1.membean.com/public/images/wordimages/cons2/fulsome.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='fulsome#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>artifice</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>despicable</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>disingenuous</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>effusive</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>egregious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>furtive</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>grandiloquent</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>melodramatic</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>noisome</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>obsequious</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>ostentatious</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>reprehensible</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>rhetoric</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>servile</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>specious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>subservient</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>subterfuge</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>unctuous</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>veneer</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>altruistic</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>ingenuous</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>innocuous</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>moderation</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="fulsome" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>fulsome</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="fulsome#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

