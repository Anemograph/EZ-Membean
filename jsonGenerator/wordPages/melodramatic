
<!DOCTYPE html>
<html>
<head>
<title>Word: melodramatic | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>To accentuate something is to emphasize it or make it more noticeable.</p>
<p class='rw-defn idx1' style='display:none'>Something austere is simple and plain in its style; an austere person is strict and severe with themselves.</p>
<p class='rw-defn idx2' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx3' style='display:none'>If you describe someone, usually a young woman, as demure, you mean that she is quiet, shy, and always behaves modestly.</p>
<p class='rw-defn idx4' style='display:none'>Someone who is dour is serious, stubborn, and unfriendly; they can also be gloomy.</p>
<p class='rw-defn idx5' style='display:none'>If you exhibit equanimity, you demonstrate a calm mental state—without showing upset or annoyance—when you deal with a difficult situation.</p>
<p class='rw-defn idx6' style='display:none'>If you think something, such as a statement or idea, is fatuous, you consider it stupid or extremely silly; a fatuous hope is unrealistic.</p>
<p class='rw-defn idx7' style='display:none'>If someone or something is flamboyant, the former is trying to show off in a way that deliberately attracts attention, and the latter is brightly colored and highly decorated.</p>
<p class='rw-defn idx8' style='display:none'>Something florid has too much decoration or is too elaborate.</p>
<p class='rw-defn idx9' style='display:none'>Praise, an apology, or gratitude is fulsome if it is so exaggerated and elaborate that it does not seem sincere.</p>
<p class='rw-defn idx10' style='display:none'>Grandiloquent speech is highly formal, exaggerated, and often seems both silly and hollow because it is expressly used to appear impressive and important.</p>
<p class='rw-defn idx11' style='display:none'>When you do something in a grandiose way, it is very showy, impressive, and magnificent.</p>
<p class='rw-defn idx12' style='display:none'>The gravity of a situation or event is its seriousness or importance.</p>
<p class='rw-defn idx13' style='display:none'>If you describe someone&#8217;s behavior as histrionic, you disapprove of it because it is overly loud, dramatic, emotional, and insincere.</p>
<p class='rw-defn idx14' style='display:none'>If you describe someone&#8217;s behavior as inane, you think it is completely stupid or without much meaning.</p>
<p class='rw-defn idx15' style='display:none'>Something that is inconspicuous does not attract attention and is not easily seen or noticed because it is small or ordinary.</p>
<p class='rw-defn idx16' style='display:none'>An inebriated person has had too much alcohol; by extension, if someone is inebriated with an emotion or feeling, they are filled with it—and possibly excited or confused by it too.</p>
<p class='rw-defn idx17' style='display:none'>If people try to ingratiate themselves with you, they try to get your approval by doing or saying things that they think will please you.</p>
<p class='rw-defn idx18' style='display:none'>An irreproachable person is very honest and so morally upright that their behavior cannot be criticized.</p>
<p class='rw-defn idx19' style='display:none'>If someone is lachrymose, they tend to cry often; if something, such as a song, is lachrymose, it tends to cause tears because it is so sad.</p>
<p class='rw-defn idx20' style='display:none'>A person who is being laconic uses very few words to say something.</p>
<p class='rw-defn idx21' style='display:none'>A maudlin person talks in a sad, silly, and overly emotional way, perhaps because they have had too much to drink.</p>
<p class='rw-defn idx22' style='display:none'>Something is mawkish when it is overly sentimental and silly in an embarrassing way.</p>
<p class='rw-defn idx23' style='display:none'>When you behave with moderation, you live in a balanced and measured way; you do nothing to excess.</p>
<p class='rw-defn idx24' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx25' style='display:none'>If you feel nostalgic about a happy time in the past, you are feeling sad when you think about it and wish that things had not changed or long for their return.</p>
<p class='rw-defn idx26' style='display:none'>If you describe an action as ostentatious, you think it is an extreme and exaggerated way of impressing people.</p>
<p class='rw-defn idx27' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx28' style='display:none'>If you are acting in a saccharine fashion, you are being way too sugary sweet or are being extremely sentimental, both of which can be irritating to others.</p>
<p class='rw-defn idx29' style='display:none'>Someone who is saturnine is looking miserable and sad, sometimes in a threatening or unfriendly way.</p>
<p class='rw-defn idx30' style='display:none'>If you say that someone is servile, you don&#8217;t respect them because they are too obedient, too agreeable, and too willing to do anything for another person.</p>
<p class='rw-defn idx31' style='display:none'>If you are staid, you are set in your ways; consequently, you are settled, serious, law-abiding, conservative, and traditional—and perhaps even a tad dull.</p>
<p class='rw-defn idx32' style='display:none'>A stoic person does not show their emotions and does not complain when bad things happen to them.</p>
<p class='rw-defn idx33' style='display:none'>Sycophants praise people in authority or those who have considerable influence in order to seek some favor from them in return.</p>
<p class='rw-defn idx34' style='display:none'>A taciturn person is quiet or silent by nature.</p>
<p class='rw-defn idx35' style='display:none'>If you show temperance, you limit yourself so that you don’t do too much of something; you act in a controlled and well-balanced way.</p>
<p class='rw-defn idx36' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx37' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>
<p class='rw-defn idx38' style='display:none'>Something that is vaunted, such as someone&#8217;s ability, is too highly praised and boasted about.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>melodramatic</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='melodramatic#' id='pronounce-sound' path='audio/words/amy-melodramatic'></a>
mel-uh-druh-MAT-ik
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='melodramatic#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='melodramatic#' id='context-sound' path='audio/wordcontexts/brian-melodramatic'></a>
The children were being so <em>melodramatic</em> and overly emotional about something that was not really a big deal.  We couldn&#8217;t see the movie we had planned to, so they got all <em>melodramatic</em> and cried excessively, acting as if their lives had been permanently ruined.  When we told them we could see it in just three hours, their <em>melodramatic</em> or theatrical behavior increased as they cried and cried over having to be completely bored for three whole hours.  By that point we told them to stop the <em>melodrama</em> or the afternoon movie plans would be canceled, really giving them grounds to complain!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of a <em>melodramatic</em> reaction?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
A child who begins to wail loudly after losing a card game.
</li>
<li class='choice '>
<span class='result'></span>
A parent who calls the doctor if their child is running a fever. 
</li>
<li class='choice '>
<span class='result'></span>
A coach who congratulates another coach after losing a game to them.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='melodramatic#' id='definition-sound' path='audio/wordmeanings/amy-melodramatic'></a>
When you are acting in a <em>melodramatic</em> way, you are overreacting to something in an overly dramatic and exaggerated way.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>exaggerated</em>
</span>
</span>
</div>
<a class='quick-help' href='melodramatic#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='melodramatic#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/melodramatic/memory_hooks/3260.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Mel</span></span>'s <span class="emp2"><span>O</span></span>ver<span class="emp2"><span>dramatic</span></span></span></span> My friend <span class="emp1"><span>Mel</span></span> is <span class="emp2"><span>o</span></span>ver<span class="emp2"><span>dramatic</span></span> about everything to the point that no one takes his <span class="emp1"><span>mel</span></span><span class="emp2"><span>odramatic</span></span> outbursts seriously at all.
</p>
</div>

<div id='memhook-button-bar'>
<a href="melodramatic#" id="add-public-hook" style="" url="https://membean.com/mywords/melodramatic/memory_hooks">Use other public hook</a>
<a href="melodramatic#" id="memhook-use-own" url="https://membean.com/mywords/melodramatic/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='melodramatic#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Living alone, with no one to consult or talk to, one might easily become <b>melodramatic</b>, and imagine things which had no foundation on fact.
<span class='attribution'>&mdash; Agatha Christie, English writer, known for writing detective novels</span>
<img alt="Agatha christie, english writer, known for writing detective novels" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Agatha Christie, English writer, known for writing detective novels.jpg?qdep8" width="80" />
</li>
<li>
Daytime soaps, televised five days a week and 52 weeks a year, were far less polished than prime-time fare. Actors often ad-libbed, and productions were rushed. Small, minimally lighted sets with lots of shadows created a stagy look that was surprisingly well suited to the small black-and-white screen and heightened the <b>melodramatic</b> mood.
<cite class='attribution'>
&mdash;
Smithsonian Magazine
</cite>
</li>
<li>
Sam Mendes’s _1917,_ a film of patriotic bombast, has an imagination-free script filled with <b>melodramatic</b> coincidences that trivialize the life-and-death action by reducing it to sentiment.
<cite class='attribution'>
&mdash;
The New Yorker
</cite>
</li>
<li>
“The single biggest threat to man’s continued dominance on the planet is the virus.” I used that searing quote from Nobel laureate Joshua Lederberg, who was president of Rockefeller University and Morse’s boss, in the introduction to my book. Back then I thought it was a little bit <b>melodramatic</b>. Now it strikes me as terrifyingly accurate.
<cite class='attribution'>
&mdash;
National Geographic
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/melodramatic/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='melodramatic#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='mel_song' data-tree-url='//cdn1.membean.com/public/data/treexml' href='melodramatic#'>
<span class=''></span>
mel
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>song</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='o_connective' data-tree-url='//cdn1.membean.com/public/data/treexml' href='melodramatic#'>
<span class=''></span>
-o-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>connective</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='dramat_play' data-tree-url='//cdn1.membean.com/public/data/treexml' href='melodramatic#'>
<span class=''></span>
dramat
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>play, drama</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ic_like' data-tree-url='//cdn1.membean.com/public/data/treexml' href='melodramatic#'>
<span class=''></span>
-ic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>characterized by, like</td>
</tr>
</table>
<p>Some who acts in a <em>melodramatic</em> fashion pretends she is &#8220;singing in a drama.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='melodramatic#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Star Trek</strong><span> A melodramatic moment for Captain Kirk.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/melodramatic.jpg' video_url='examplevids/melodramatic' video_width='350'></span>
<div id='wt-container'>
<img alt="Melodramatic" height="288" src="https://cdn1.membean.com/video/examplevids/melodramatic.jpg" width="350" />
<div class='center'>
<a href="melodramatic#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='melodramatic#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Melodramatic" src="https://cdn0.membean.com/public/images/wordimages/cons2/melodramatic.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='melodramatic#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>accentuate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>fatuous</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>flamboyant</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>florid</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>fulsome</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>grandiloquent</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>grandiose</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>histrionic</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>inane</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>inebriated</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>ingratiate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>lachrymose</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>maudlin</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>mawkish</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>nostalgic</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>ostentatious</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>saccharine</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>saturnine</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>servile</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>sycophant</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>vaunted</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>austere</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>demure</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>dour</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>equanimity</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>gravity</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>inconspicuous</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>irreproachable</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>laconic</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>moderation</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>staid</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>stoic</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>taciturn</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>temperance</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='melodramatic#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
melodrama
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>a play that has exaggerated emotions</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="melodramatic" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>melodramatic</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="melodramatic#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

