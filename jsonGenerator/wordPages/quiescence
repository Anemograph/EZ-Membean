
<!DOCTYPE html>
<html>
<head>
<title>Word: quiescence | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Quiescence-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/quiescence-large.jpg?qdep8" />
</div>
<a href="quiescence#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>If something is in abeyance, it has been put on hold and is not proceeding or being used at the present time.</p>
<p class='rw-defn idx1' style='display:none'>Something that is arduous is extremely difficult; hence, it involves a lot of effort.</p>
<p class='rw-defn idx2' style='display:none'>An audacious person acts with great daring and sometimes reckless bravery—despite risks and warnings from other people—in order to achieve something.</p>
<p class='rw-defn idx3' style='display:none'>If you describe a person&#8217;s behavior as brazen, you mean that they are not embarrassed by anything they do—they simply don&#8217;t care what other people think about them.</p>
<p class='rw-defn idx4' style='display:none'>Someone who has a bristling personality is easily offended, annoyed, or angered.</p>
<p class='rw-defn idx5' style='display:none'>When you cavort, you jump and dance around in a playful, excited, or physically lively way.</p>
<p class='rw-defn idx6' style='display:none'>Complacent persons are too confident and relaxed because they think that they can deal with a situation easily; however, in many circumstances, this is not the case.</p>
<p class='rw-defn idx7' style='display:none'>Consternation is the feeling of anxiety or fear, sometimes paralyzing in its effect, and often caused by something unexpected that has happened.</p>
<p class='rw-defn idx8' style='display:none'>When you are in a state of disarray, you are disorganized, disordered, and in a state of confusion.</p>
<p class='rw-defn idx9' style='display:none'>If something discomfits you, it makes you feel embarrassed, confused, uncomfortable, or frustrated.</p>
<p class='rw-defn idx10' style='display:none'>When someone feels disquiet about a situation, they feel very worried or nervous.</p>
<p class='rw-defn idx11' style='display:none'>When something is dormant, it is in a state of sleep or temporarily not active.</p>
<p class='rw-defn idx12' style='display:none'>Someone who is ebullient is filled with enthusiasm, very happy, and extremely excited about something.</p>
<p class='rw-defn idx13' style='display:none'>An effervescent individual is lively, very happy, and enthusiastic.</p>
<p class='rw-defn idx14' style='display:none'>To foment is to encourage people to protest, fight, or cause trouble and violent opposition to something that is viewed by some as undesirable.</p>
<p class='rw-defn idx15' style='display:none'>A fusillade is a rapid burst of gunfire from multiple guns fired at once or in succession; in turn, a fusillade can also be a rapid outburst or discharge of something.</p>
<p class='rw-defn idx16' style='display:none'>If someone gambols, they run, jump, and play like a young child or animal.</p>
<p class='rw-defn idx17' style='display:none'>A halcyon time is calm, peaceful, and undisturbed.</p>
<p class='rw-defn idx18' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx19' style='display:none'>An impending event is approaching fast or is about to occur; this word usually has a negative implication, referring to something threatening or harmful coming.</p>
<p class='rw-defn idx20' style='display:none'>When something is inert, it has no power of movement or is inactive.</p>
<p class='rw-defn idx21' style='display:none'>Something kinetic is moving, active, and using energy.</p>
<p class='rw-defn idx22' style='display:none'>A laborious job or process takes a long time, requires a lot of effort, and is often boring.</p>
<p class='rw-defn idx23' style='display:none'>Something that is latent, such as an undiscovered talent or undiagnosed disease, exists but is not active or has not developed at the moment—it may develop or become active in the future.</p>
<p class='rw-defn idx24' style='display:none'>A maelstrom is either a large whirlpool in the sea or a violent or agitated state of affairs.</p>
<p class='rw-defn idx25' style='display:none'>When you meander, you either wander about with no particular goal or follow a path that twists and turns a great deal.</p>
<p class='rw-defn idx26' style='display:none'>If you mollify someone, you say or do something to make that person feel less angry or upset.</p>
<p class='rw-defn idx27' style='display:none'>An ominous sign predicts or shows that something bad will happen in the near future.</p>
<p class='rw-defn idx28' style='display:none'>A paroxysm is a sudden uncontrolled expression of emotion or a short attack of pain, coughing, or shaking.</p>
<p class='rw-defn idx29' style='display:none'>A peregrination is a long journey or act of traveling from place to place, especially by foot.</p>
<p class='rw-defn idx30' style='display:none'>A placid scene or person is calm, quiet, and undisturbed.</p>
<p class='rw-defn idx31' style='display:none'>To quaver is to shake or tremble, especially when speaking.</p>
<p class='rw-defn idx32' style='display:none'>If you are in a state of repose, your mind is at peace or your body is at rest.</p>
<p class='rw-defn idx33' style='display:none'>If you are sedate, you are calm, unhurried, and unlikely to be disturbed by anything.</p>
<p class='rw-defn idx34' style='display:none'>Someone who has a sedentary habit, job, or lifestyle spends a lot of time sitting down without moving or exercising often.</p>
<p class='rw-defn idx35' style='display:none'>A serene place or situation is peaceful and calm.</p>
<p class='rw-defn idx36' style='display:none'>Skittish persons or animals are made easily nervous or alarmed; they are likely to change behavior quickly and unpredictably.</p>
<p class='rw-defn idx37' style='display:none'>If you are somnolent, you are sleepy.</p>
<p class='rw-defn idx38' style='display:none'>A spate of things is a sudden series, significant quantity, or outpouring of them.</p>
<p class='rw-defn idx39' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx40' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx41' style='display:none'>A stoic person does not show their emotions and does not complain when bad things happen to them.</p>
<p class='rw-defn idx42' style='display:none'>Stupor is a state in which someone&#8217;s mind and senses are dulled; consequently, they are unable to think clearly or act normally, usually due to the use of alcohol or drugs.</p>
<p class='rw-defn idx43' style='display:none'>If you are suffering from tedium, you are bored.</p>
<p class='rw-defn idx44' style='display:none'>If your body is affected by torpor, you are severely lacking in energy; therefore, you are idle—and can even be numb.</p>
<p class='rw-defn idx45' style='display:none'>A torrential downpour of rain is very heavy and intense.</p>
<p class='rw-defn idx46' style='display:none'>If something is tranquil, it is peaceful, calm, and quiet.</p>
<p class='rw-defn idx47' style='display:none'>Someone is tremulous when they are shaking slightly from nervousness or fear; they may also simply be afraid of something.</p>
<p class='rw-defn idx48' style='display:none'>Trepidation is fear or uneasiness about something that is going to happen.</p>
<p class='rw-defn idx49' style='display:none'>A tumultuous event or period of time is filled with great excitement, confusion, or violence; a tumultuous reaction to something is likewise very loud and noisy because people are happy and excited.</p>
<p class='rw-defn idx50' style='display:none'>When you experience turmoil, there is great confusion, disturbance, instability, and disorder in your life.</p>
<p class='rw-defn idx51' style='display:none'>Someone who is zealous spends a lot of time, energy, and effort to support something—  notably that of a political or religious nature—because they believe in it very strongly.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>quiescence</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='quiescence#' id='pronounce-sound' path='audio/words/amy-quiescence'></a>
kwee-ES-uhns
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='quiescence#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='quiescence#' id='context-sound' path='audio/wordcontexts/brian-quiescence'></a>
After the bustle of the exam period, we experienced a time of relative, restful <em>quiescence</em> during break.  We enjoyed this peaceful <em>quiescence</em>, knowing that life would soon return to its hectic, hurried pace.  This was also a period of political <em>quiescence</em> or inactivity on our campus; there were no protests or rallies for two months since many of the most vocal political activists were away.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What might someone do during a state of <em>quiescence</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
They might nap or rest.
</li>
<li class='choice '>
<span class='result'></span>
They might try to stay well hidden from others.
</li>
<li class='choice '>
<span class='result'></span>
They might nervously await a decision.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='quiescence#' id='definition-sound' path='audio/wordmeanings/amy-quiescence'></a>
A state of <em>quiescence</em> is one of quiet and restful inaction.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>restful inactivity</em>
</span>
</span>
</div>
<a class='quick-help' href='quiescence#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='quiescence#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/quiescence/memory_hooks/5301.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>Sense</span></span> To Be <span class="emp3"><span>Quie</span></span>t</span></span> We must all have the good <span class="emp2"><span>sense</span></span> to be <span class="emp3"><span>quie</span></span>t; we must practice this <span class="emp3"><span>quie</span></span><span class="emp2"><span>scence</span></span> often, lest we become too filled with worry and have a nervous breakdown.
</p>
</div>

<div id='memhook-button-bar'>
<a href="quiescence#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/quiescence/memory_hooks">Use other hook</a>
<a href="quiescence#" id="memhook-use-own" url="https://membean.com/mywords/quiescence/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='quiescence#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
After a long interval of comparative <b>quiescence</b>, the Irish gang in Parliament have broken out again, and obstruction is once more the order of the day.
<cite class='attribution'>
&mdash;
The New York Times, from 1881
</cite>
</li>
<li>
Since declaring a ceasefire for his Mahdi Army militia last August, Sadr has effectively disappeared from public life, designating five trusted aides to speak on his behalf. U.S. commanders say that the Mahdi Army’s <b>quiescence</b> is a significant factor behind the recent drop in attacks in Baghdad—by a third compared with six months ago, according to one estimate.
<cite class='attribution'>
&mdash;
Newsweek
</cite>
</li>
<li>
Mount St. Helens completed its second year without any ''significant volcanic activity'' yesterday, the United States Geological Survey reported. This is the longest period of <b>quiescence</b> since the volcano blew off its top on May 18, 1980, causing at least 60 deaths.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
The coronavirus pandemic and the associated economic shutdowns led to an unprecedented drop in the seismic “noise” caused by human activity, scientists reported Thursday. The shutdown effect registered by seismometers is akin to that typically seen in the middle of the night and during holiday periods, only this one lasted from March to May and encompassed almost every corner of the planet—a stunning global <b>quiescence</b> never before seen in the history of earthquake science.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/quiescence/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='quiescence#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='quiesc_rest' data-tree-url='//cdn1.membean.com/public/data/treexml' href='quiescence#'>
<span class=''></span>
quiesc
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to rest, be still, stay calm</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ence_state' data-tree-url='//cdn1.membean.com/public/data/treexml' href='quiescence#'>
<span class=''></span>
-ence
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>state or condition</td>
</tr>
</table>
<p><em>Quiescence</em> is the &#8220;state or condition&#8221; of &#8220;resting, being still, or staying calm.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='quiescence#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Numb3rs</strong><span> His state of quiescence has been disturbed.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/quiescence.jpg' video_url='examplevids/quiescence' video_width='350'></span>
<div id='wt-container'>
<img alt="Quiescence" height="288" src="https://cdn1.membean.com/video/examplevids/quiescence.jpg" width="350" />
<div class='center'>
<a href="quiescence#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='quiescence#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Quiescence" src="https://cdn1.membean.com/public/images/wordimages/cons2/quiescence.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='quiescence#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abeyance</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>complacent</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>dormant</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>halcyon</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>inert</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>latent</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>mollify</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>placid</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>repose</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>sedate</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>sedentary</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>serene</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>somnolent</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>stoic</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>stupor</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>torpor</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>tranquil</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>arduous</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>audacious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>brazen</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>bristling</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>cavort</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>consternation</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>disarray</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>discomfit</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>disquiet</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>ebullient</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>effervescent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>foment</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>fusillade</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>gambol</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>impending</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>kinetic</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>laborious</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>maelstrom</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>meander</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>ominous</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>paroxysm</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>peregrination</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>quaver</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>skittish</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>spate</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>tedium</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>torrential</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>tremulous</span> &middot;</li><li data-idx='48' class = 'rw-wordform notlearned'><span>trepidation</span> &middot;</li><li data-idx='49' class = 'rw-wordform notlearned'><span>tumultuous</span> &middot;</li><li data-idx='50' class = 'rw-wordform notlearned'><span>turmoil</span> &middot;</li><li data-idx='51' class = 'rw-wordform notlearned'><span>zealous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='quiescence#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
quiescent
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>being still or quiet</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="quiescence" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>quiescence</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="quiescence#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

