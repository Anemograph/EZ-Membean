
<!DOCTYPE html>
<html>
<head>
<title>Word: cogitate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you adjudicate a competition or dispute, you officially decide who is right or what should be done concerning any difficulties that may arise.</p>
<p class='rw-defn idx1' style='display:none'>A catatonic person is in a state of suspended action; therefore, they are rigid, immobile, and unresponsive.</p>
<p class='rw-defn idx2' style='display:none'>Cognitive describes those things related to judgment, memory, and other mental processes of knowing.</p>
<p class='rw-defn idx3' style='display:none'>When you contemplate something, you either think about it deeply or gaze at it intently.</p>
<p class='rw-defn idx4' style='display:none'>The word corporeal refers to the physical or material world rather than the spiritual; it also means characteristic of the body rather than the mind or feelings.</p>
<p class='rw-defn idx5' style='display:none'>If you deliberate, you think about or discuss something very carefully, especially before making an important decision.</p>
<p class='rw-defn idx6' style='display:none'>When you envisage something, you imagine or consider its future possibility.</p>
<p class='rw-defn idx7' style='display:none'>When you formulate a plan of action, you carefully work it out or design it in great detail ahead of time.</p>
<p class='rw-defn idx8' style='display:none'>An impromptu speech is unplanned or spontaneous—it has not been practiced in any way beforehand.</p>
<p class='rw-defn idx9' style='display:none'>When someone improvises, they make something up at once because an unexpected situation has arisen.</p>
<p class='rw-defn idx10' style='display:none'>When you act in an imprudent fashion, you do something that is unwise, is lacking in good judgment, or has no forethought.</p>
<p class='rw-defn idx11' style='display:none'>Someone who is impulsive tends to do things without thinking about them ahead of time; therefore, their actions can be unpredictable.</p>
<p class='rw-defn idx12' style='display:none'>An inadvertent action is not done intentionally; rather, it is an accident that happens because someone is not being attentive to their surroundings.</p>
<p class='rw-defn idx13' style='display:none'>When something is inert, it has no power of movement or is inactive.</p>
<p class='rw-defn idx14' style='display:none'>A judicious person shows good or sound judgment because they are wise and careful in making decisions.</p>
<p class='rw-defn idx15' style='display:none'>When you are listless, you lack energy and interest and are unwilling to exert any effort.</p>
<p class='rw-defn idx16' style='display:none'>Someone is obtuse when they are slow to understand things; someone can choose to be obtuse when they deliberately do not try to figure things out.</p>
<p class='rw-defn idx17' style='display:none'>To opine is to state your opinion on something.</p>
<p class='rw-defn idx18' style='display:none'>When someone exhibits profundity, they display great intellectual depth and understanding; profundity can also be the depth or complexity of something.</p>
<p class='rw-defn idx19' style='display:none'>A state of quiescence is one of quiet and restful inaction.</p>
<p class='rw-defn idx20' style='display:none'>A reverie is a state of pleasant dreamlike thoughts that makes you forget what you are doing and what is happening around you.</p>
<p class='rw-defn idx21' style='display:none'>If you ruminate on something, you think carefully and deeply about it over a long period of time.</p>
<p class='rw-defn idx22' style='display:none'>If you are somnolent, you are sleepy.</p>
<p class='rw-defn idx23' style='display:none'>Something soporific makes you feel sleepy or drowsy.</p>
<p class='rw-defn idx24' style='display:none'>Spontaneity is freedom to act when and how you want to, often in an unpredictable or unplanned way.</p>
<p class='rw-defn idx25' style='display:none'>Stasis is a state of little change over a long period of time, or a condition of inactivity caused by an equal balance of opposing forces.</p>
<p class='rw-defn idx26' style='display:none'>Stupor is a state in which someone&#8217;s mind and senses are dulled; consequently, they are unable to think clearly or act normally, usually due to the use of alcohol or drugs.</p>
<p class='rw-defn idx27' style='display:none'>If your body is affected by torpor, you are severely lacking in energy; therefore, you are idle—and can even be numb.</p>
<p class='rw-defn idx28' style='display:none'>If you do something in an unwitting fashion, you didn&#8217;t know that you were doing it; therefore, it was unintentional on your part.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>cogitate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='cogitate#' id='pronounce-sound' path='audio/words/amy-cogitate'></a>
KOJ-i-tayt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='cogitate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='cogitate#' id='context-sound' path='audio/wordcontexts/brian-cogitate'></a>
On many evenings, the scholar Dr. Bindings chose to think deeply in his study after dinner, <em><em>cogitating</em></em> over his most recent readings and discoveries.  Dr. Bindings <em><em>cogitated</em></em> and considered ideas in depth well into the long hours of the night.  In the morning, his butler often would find that the professor had stayed awake all night, <em><em>cogitating</em></em> and reflecting on new ideas until the sun rose.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Which person is <em>cogitating</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Clara is nervously waiting to present her project.
</li>
<li class='choice answer '>
<span class='result'></span>
Ciara is meditating deeply about her life choices.
</li>
<li class='choice '>
<span class='result'></span>
Coleman is staying up late to play a video game.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='cogitate#' id='definition-sound' path='audio/wordmeanings/amy-cogitate'></a>
<em>Cogitating</em> about something is thinking deeply about it for a long time.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>think</em>
</span>
</span>
</div>
<a class='quick-help' href='cogitate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='cogitate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/cogitate/memory_hooks/3263.json'></span>
<p>
<span class="emp0"><span>Re<span class="emp1"><span>cog</span></span>nized Too L<span class="emp3"><span>ate</span></span></span></span> Although I <span class="emp1"><span>cog</span></span><span class="emp2"><span>it</span></span><span class="emp3"><span>ate</span></span>d for a long time, I re<span class="emp1"><span>cog</span></span>nized too l<span class="emp3"><span>ate</span></span> that the stock was going to go up in value ... I should have acted on my intuition and gut feelings and bought <span class="emp2"><span>it</span></span> earlier!
</p>
</div>

<div id='memhook-button-bar'>
<a href="cogitate#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/cogitate/memory_hooks">Use other hook</a>
<a href="cogitate#" id="memhook-use-own" url="https://membean.com/mywords/cogitate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='cogitate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
One of the Democratic senators, Byron L. Dorgan of North Dakota, said: "I intend to inquire and <b>cogitate</b> on this nomination, and it will take me a while to <b>cogitate</b>."
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
The hottest verb in the headline world? That's easy: it's the short word for think about, contemplate, <b>cogitate</b> and consider. Though ponder is big—evidently pensiveness is coming on strong in the news—the word of the year is mull.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
It is difficult, even, to <b>cogitate</b> that what we are experiencing with the pandemic is within the realm of environmental crisis, rather than separate from it.
<cite class='attribution'>
&mdash;
The New Yorker
</cite>
</li>
<li>
This has been a particular strength of the Patriots under coach Bill Belichick, but the Eagles are 9-0 under Andy Reid with two weeks to <b>cogitate</b>.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/cogitate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='cogitate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>cogit</td>
<td>
&rarr;
</td>
<td class='meaning'>think, consider, ponder</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ate_quality' data-tree-url='//cdn1.membean.com/public/data/treexml' href='cogitate#'>
<span class=''></span>
-ate
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to make someone have a certain quality</td>
</tr>
</table>
<p>To <em>cogitate</em> is to &#8220;think, consider, or ponder.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='cogitate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Cogitate" src="https://cdn2.membean.com/public/images/wordimages/cons2/cogitate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='cogitate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adjudicate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>cognitive</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>contemplate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>deliberate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>envisage</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>formulate</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>judicious</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>opine</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>profundity</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>reverie</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>ruminate</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>catatonic</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>corporeal</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>impromptu</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>improvise</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>imprudent</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>impulsive</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>inadvertent</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>inert</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>listless</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>obtuse</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>quiescence</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>somnolent</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>soporific</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>spontaneity</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>stasis</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>stupor</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>torpor</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>unwitting</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="cogitate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>cogitate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="cogitate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

