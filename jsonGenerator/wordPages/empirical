
<!DOCTYPE html>
<html>
<head>
<title>Word: empirical | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you adduce, you give facts and examples in order to prove that something is true.</p>
<p class='rw-defn idx1' style='display:none'>A conjecture is a theory or guess that is based on information that is not certain or complete.</p>
<p class='rw-defn idx2' style='display:none'>A credulous person is very ready to believe what people tell them; therefore, they can be easily tricked or cheated.</p>
<p class='rw-defn idx3' style='display:none'>When something is subjected to distortion, it is twisted out of shape in some way.</p>
<p class='rw-defn idx4' style='display:none'>A heuristic method of teaching encourages learning based on students&#8217; discovering and experiencing things for themselves.</p>
<p class='rw-defn idx5' style='display:none'>Something that is hypothetical is based on possible situations or events rather than actual ones.</p>
<p class='rw-defn idx6' style='display:none'>Something that is imperceptible is either impossible to be perceived by the mind or is very difficult to perceive.</p>
<p class='rw-defn idx7' style='display:none'>Something that is implausible is unlikely to be true or hard to believe that it&#8217;s true.</p>
<p class='rw-defn idx8' style='display:none'>Something that is inconceivable cannot be imagined or thought of; that is, it is beyond reason or unbelievable.</p>
<p class='rw-defn idx9' style='display:none'>If the results of something are inconclusive, they are uncertain or provide no final answers.</p>
<p class='rw-defn idx10' style='display:none'>Inductive reasoning involves the observation of data to arrive at a general conclusion or principle.</p>
<p class='rw-defn idx11' style='display:none'>An inference is a conclusion that is reached using available data.</p>
<p class='rw-defn idx12' style='display:none'>A judicious person shows good or sound judgment because they are wise and careful in making decisions.</p>
<p class='rw-defn idx13' style='display:none'>If someone is lucid, they are able to understand things and think clearly; this adjective also describes writing or speech that is crystal clear and easy to understand.</p>
<p class='rw-defn idx14' style='display:none'>If you describe something as nebulous, you mean that it is unclear, vague, and not clearly defined; a shape that is nebulous has no clear boundaries.</p>
<p class='rw-defn idx15' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx16' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx17' style='display:none'>If you handle something in a pragmatic way, you deal with it in a realistic and practical manner rather than just following unproven theories or ideas.</p>
<p class='rw-defn idx18' style='display:none'>A subjective opinion is not based upon facts or hard evidence; rather, it rests upon someone&#8217;s personal feelings or beliefs about a matter or concern.</p>
<p class='rw-defn idx19' style='display:none'>If you surmise why something has occurred, you make a guess, offer a possibility, or have a theory about it.</p>
<p class='rw-defn idx20' style='display:none'>To be tentative is to be hesitant or uncertain about something; an agreement or decision of this kind is likely to have changes before it reaches its final form.</p>
<p class='rw-defn idx21' style='display:none'>An unfounded claim is not based upon evidence; instead, it is unproven or groundless.</p>
<p class='rw-defn idx22' style='display:none'>An unwarranted decision cannot be justified because there is no evidence to back it up; therefore, it is needless and uncalled-for.</p>
<p class='rw-defn idx23' style='display:none'>A utilitarian object is useful, serviceable, and practical—rather than fancy or unnecessary.</p>
<p class='rw-defn idx24' style='display:none'>Something that is vaporous is not completely formed but foggy and misty; in the same vein, a vaporous idea is insubstantial and vague.</p>
<p class='rw-defn idx25' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>empirical</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='empirical#' id='pronounce-sound' path='audio/words/amy-empirical'></a>
em-PIR-i-kuhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='empirical#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='empirical#' id='context-sound' path='audio/wordcontexts/brian-empirical'></a>
The scientific method says that theories are not enough, but must be supported by strong <em>empirical</em> and observational evidence.  One can construct grand detailed theories, but if they are not backed by <em>empirical</em> and experimental evidence they are worthless.  Would you rather believe a theory that says that the sun must be purple, or your own <em>empirical</em> and practical evidence which says that it is clearly yellow? Scientific progress is firmly grounded in <em>empiricism</em>, or trial by experience of that which is observed.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is <em>empirical</em> knowledge?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Knowledge that comes from evaluating many different theories.
</li>
<li class='choice '>
<span class='result'></span>
Knowledge that one assumes to be true without any actual proof.
</li>
<li class='choice answer '>
<span class='result'></span>
Knowledge gained after observing the results of an experiment.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='empirical#' id='definition-sound' path='audio/wordmeanings/amy-empirical'></a>
<em>Empirical</em> evidence or study is based on real experience or scientific experiments rather than on unproven theories.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>observable</em>
</span>
</span>
</div>
<a class='quick-help' href='empirical#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='empirical#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/empirical/memory_hooks/3126.json'></span>
<p>
<span class="emp0"><span>Log<span class="emp3"><span>ical</span></span> <span class="emp2"><span>Pir</span></span>ate</span></span> By using em<span class="emp2"><span>pir</span></span><span class="emp3"><span>ical</span></span> evidence the <span class="emp2"><span>pir</span></span>ate discovered that it was log<span class="emp3"><span>ical</span></span>ly better to attack the English than the French because the English were wealthier and they couldn't fight as well, something the log<span class="emp3"><span>ical</span></span> <span class="emp2"><span>pir</span></span>ate had learned through a great deal of personal "observation."
</p>
</div>

<div id='memhook-button-bar'>
<a href="empirical#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/empirical/memory_hooks">Use other hook</a>
<a href="empirical#" id="memhook-use-own" url="https://membean.com/mywords/empirical/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='empirical#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
<b>Empirical</b> economists test a theory or a belief by coming up with just the right data or observations. . . . [Alan B. Kreueger] came to his conclusions about class size after tapping into a Tennessee study of 11,600 children who had been placed randomly in small or large classes.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
With knowledge comes power, and change; <b>empirical</b> evidence of lead contamination in Flint, Mich., was key for bringing the injustices there to global attention.
<cite class='attribution'>
&mdash;
Scientific American
</cite>
</li>
<li>
The economic theory is simple: If something becomes more expensive, people will buy less of it. And the <b>empirical</b> evidence is overwhelming: When patients have to pay more, they use less medical care.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
Judges are trained to perform legal analysis, not scientific analysis, and law and science are two very different fields. Science is forward-looking, always changing and adapting to discoveries and new <b>empirical</b> evidence. The law, by contrast, puts a premium on consistency and predictability. It relies on precedent, so courts look to previous courts for guidance and are often bound by prior decisions.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/empirical/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='empirical#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='em_in' data-tree-url='//cdn1.membean.com/public/data/treexml' href='empirical#'>
<span class=''></span>
em-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>in, on</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='pir_attempt' data-tree-url='//cdn1.membean.com/public/data/treexml' href='empirical#'>
<span class=''></span>
pir
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>attempt</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ic_like' data-tree-url='//cdn1.membean.com/public/data/treexml' href='empirical#'>
<span class=''></span>
-ic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>like</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='al_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='empirical#'>
<span class=''></span>
-al
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p><em>Empirical</em> evidence is that evidence which a scientist has made &#8220;an attempt on,&#8221; that is, has observed, in order to discover truths about it.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='empirical#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Sherlock</strong><span> The marvels of empirical evidence.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/empirical.jpg' video_url='examplevids/empirical' video_width='350'></span>
<div id='wt-container'>
<img alt="Empirical" height="288" src="https://cdn1.membean.com/video/examplevids/empirical.jpg" width="350" />
<div class='center'>
<a href="empirical#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='empirical#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Empirical" src="https://cdn1.membean.com/public/images/wordimages/cons2/empirical.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='empirical#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>adduce</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>heuristic</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>inductive</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>inference</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>judicious</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>lucid</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>pragmatic</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>utilitarian</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>conjecture</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>credulous</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>distortion</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>hypothetical</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>imperceptible</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>implausible</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>inconceivable</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>inconclusive</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>nebulous</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>subjective</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>surmise</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>tentative</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>unfounded</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>unwarranted</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>vaporous</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='empirical#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
empiricism
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>the belief that experience is the only true source of knowledge</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="empirical" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>empirical</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="empirical#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

