
<!DOCTYPE html>
<html>
<head>
<title>Word: expedient | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx1' style='display:none'>If something is in abeyance, it has been put on hold and is not proceeding or being used at the present time.</p>
<p class='rw-defn idx2' style='display:none'>An abortive attempt or action is cut short before it is finished; hence, it is unsuccessful.</p>
<p class='rw-defn idx3' style='display:none'>When you are adept at something, you are very good, skillful, or talented at it.</p>
<p class='rw-defn idx4' style='display:none'>Someone who is adroit is skillful and clever, especially in thought, behavior, or action.</p>
<p class='rw-defn idx5' style='display:none'>Something that is apposite is relevant or suitable to what is happening or being discussed.</p>
<p class='rw-defn idx6' style='display:none'>If someone is competent in a job, they are able and skilled enough to do it well.</p>
<p class='rw-defn idx7' style='display:none'>A condign reward or punishment is deserved by and appropriate or fitting for the person who receives it.</p>
<p class='rw-defn idx8' style='display:none'>If someone shows consummate skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.</p>
<p class='rw-defn idx9' style='display:none'>A deft movement or action is made quickly and with skill.</p>
<p class='rw-defn idx10' style='display:none'>Something that is defunct is no longer in existence or does not function.</p>
<p class='rw-defn idx11' style='display:none'>Efficacy is the ability or power to produce an expected effect or result.</p>
<p class='rw-defn idx12' style='display:none'>Errant things or people behave in a way that is unacceptable or wrong, such as a missile that travels in the wrong direction or a student who does not follow classroom rules.</p>
<p class='rw-defn idx13' style='display:none'>When you expedite an action or process, you make it happen more quickly.</p>
<p class='rw-defn idx14' style='display:none'>When you facilitate something, such as an event or project, you make it easier for everyone to get it done by giving assistance.</p>
<p class='rw-defn idx15' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx16' style='display:none'>A futile attempt at doing something is hopeless and pointless because it simply cannot be accomplished.</p>
<p class='rw-defn idx17' style='display:none'>A hapless person is unlucky, so much so that a stream of unfortunate things has happened and continues to happen to them.</p>
<p class='rw-defn idx18' style='display:none'>A hindrance is an obstacle or obstruction that gets in your way and prevents you from doing something.</p>
<p class='rw-defn idx19' style='display:none'>An impediment is something that blocks or obstructs progress; it can also be a weakness or disorder, such as having difficulty with speaking.</p>
<p class='rw-defn idx20' style='display:none'>Someone who is ineffectual at a task is useless or ineffective when attempting to do it.</p>
<p class='rw-defn idx21' style='display:none'>Someone who is inept is unable or unsuitable to do a job; hence, they are unfit for it.</p>
<p class='rw-defn idx22' style='display:none'>Something that inhibits you from doing something restricts or keeps you from doing it.</p>
<p class='rw-defn idx23' style='display:none'>A judicious person shows good or sound judgment because they are wise and careful in making decisions.</p>
<p class='rw-defn idx24' style='display:none'>If you do something in a lackadaisical way, you do it carelessly and without putting much effort into it—thereby showing that you are not really interested in what you&#8217;re doing.</p>
<p class='rw-defn idx25' style='display:none'>When you are listless, you lack energy and interest and are unwilling to exert any effort.</p>
<p class='rw-defn idx26' style='display:none'>To be politic in a decision is to be socially wise and diplomatic.</p>
<p class='rw-defn idx27' style='display:none'>If you handle something in a pragmatic way, you deal with it in a realistic and practical manner rather than just following unproven theories or ideas.</p>
<p class='rw-defn idx28' style='display:none'>When you preclude something from happening, you prevent it from doing so.</p>
<p class='rw-defn idx29' style='display:none'>When you have been remiss, you have been careless because you did not do something that you should have done.</p>
<p class='rw-defn idx30' style='display:none'>To be tentative is to be hesitant or uncertain about something; an agreement or decision of this kind is likely to have changes before it reaches its final form.</p>
<p class='rw-defn idx31' style='display:none'>When you thwart someone&#8217;s ambitions, you obstruct or stop them from happening.</p>
<p class='rw-defn idx32' style='display:none'>Something unfeasible cannot be made or achieved.</p>
<p class='rw-defn idx33' style='display:none'>Something that is unwieldy is hard or awkward to handle because of the way that it is shaped.</p>
<p class='rw-defn idx34' style='display:none'>A utilitarian object is useful, serviceable, and practical—rather than fancy or unnecessary.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>expedient</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='expedient#' id='pronounce-sound' path='audio/words/amy-expedient'></a>
ik-SPEE-dee-uhnt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='expedient#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='expedient#' id='context-sound' path='audio/wordcontexts/brian-expedient'></a>
Please be <em>expedient</em> in delivering this package; we want it done as quickly and efficiently as possible.  Overnight mail will be the most <em>expedient</em> or advantageous way to get it there, so I suggest you do that.  That fast shipping method will be the most <em>expedient</em> or appropriate for us because the customer needs the product by tomorrow morning, so you&#8217;d better get it over to FedEx <span class="caps">ASAP</span>.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If something is <em>expedient</em>, what is it?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is an expensive way to complete a significant task.
</li>
<li class='choice '>
<span class='result'></span>
It is an easy but ineffective way to go about doing something.
</li>
<li class='choice answer '>
<span class='result'></span>
It is the most efficient way possible to get a job done.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='expedient#' id='definition-sound' path='audio/wordmeanings/amy-expedient'></a>
A thing or action that is <em>expedient</em> is useful, advantageous, or appropriate to get something accomplished.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>useful</em>
</span>
</span>
</div>
<a class='quick-help' href='expedient#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='expedient#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/expedient/memory_hooks/5040.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Expe</span></span>rt and Effic<span class="emp2"><span>ient</span></span></span></span> An <span class="emp1"><span>expe</span></span>d<span class="emp2"><span>ient</span></span> method is both <span class="emp1"><span>expe</span></span>rtly accomplished and effic<span class="emp2"><span>ient</span></span>ly done.
</p>
</div>

<div id='memhook-button-bar'>
<a href="expedient#" id="add-public-hook" style="" url="https://membean.com/mywords/expedient/memory_hooks">Use other public hook</a>
<a href="expedient#" id="memhook-use-own" url="https://membean.com/mywords/expedient/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='expedient#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Meanwhile, expanding populations in desert areas are putting intense pressure on existing fresh water supplies, forcing communities to turn to desalinization as the most <b>expedient</b> way to satisfy their collective thirst.
<cite class='attribution'>
&mdash;
Scientific American
</cite>
</li>
<li>
“When small business owners comply with requests for information and submit their applications, they deserve the same <b>expedient</b> response that departments afford well-resourced developers,” [Supervisor Aaron Peskin] added.
<cite class='attribution'>
&mdash;
San Francisco Chronicle
</cite>
</li>
<li>
We must build these places not in the most <b>expedient</b> and easy manner, but in a way that is designed to create local pride and beauty in solid construction that can last for generations to come.
<cite class='attribution'>
&mdash;
The Syracuse Post-Standard
</cite>
</li>
<li>
There is one type of friend almost everyone has: the buddy who can help you get ahead in life, the friend from whom you need or want something. . . . These are what some social scientists call “<b>expedient</b> friendships”—with people we might call “deal friends”—and they are probably the most common type most of us have.
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/expedient/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='expedient#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ex_out' data-tree-url='//cdn1.membean.com/public/data/treexml' href='expedient#'>
<span class='common'></span>
ex-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>out of, from, off</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ped_foot' data-tree-url='//cdn1.membean.com/public/data/treexml' href='expedient#'>
<span class='common'></span>
ped
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>foot</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ent_being' data-tree-url='//cdn1.membean.com/public/data/treexml' href='expedient#'>
<span class=''></span>
-ent
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>that which does something</td>
</tr>
</table>
<p>An <em>expedient</em> solution is suitable or appropriate to get results quickly, hence freeing the &#8220;foot out of&#8221; being stuck in a problem or rut.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='expedient#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Building Expedient Military Roads</strong><span> Expedient building of roads is a must when traveling through areas with poor roadway systems.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/expedient.jpg' video_url='examplevids/expedient' video_width='350'></span>
<div id='wt-container'>
<img alt="Expedient" height="288" src="https://cdn1.membean.com/video/examplevids/expedient.jpg" width="350" />
<div class='center'>
<a href="expedient#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='expedient#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Expedient" src="https://cdn0.membean.com/public/images/wordimages/cons2/expedient.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='expedient#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='3' class = 'rw-wordform notlearned'><span>adept</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>adroit</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>apposite</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>competent</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>condign</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>consummate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>deft</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>efficacy</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>expedite</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>facilitate</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>judicious</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>politic</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>pragmatic</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>utilitarian</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abeyance</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abortive</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>defunct</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>errant</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>futile</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>hapless</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>hindrance</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>impediment</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>ineffectual</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>inept</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>inhibit</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>lackadaisical</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>listless</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>preclude</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>remiss</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>tentative</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>thwart</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>unfeasible</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>unwieldy</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="expedient" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>expedient</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="expedient#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

