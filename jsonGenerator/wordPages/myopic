
<!DOCTYPE html>
<html>
<head>
<title>Word: myopic | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Myopic-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/myopic-large.jpg?qdep8" />
</div>
<a href="myopic#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When you act with abandon, you give in to the impulse of the moment and behave in a wild, uncontrolled way.</p>
<p class='rw-defn idx1' style='display:none'>Acuity is sharpness or clearness of vision, hearing, or thought.</p>
<p class='rw-defn idx2' style='display:none'>Acumen is the ability to make quick decisions and keen, precise judgments.</p>
<p class='rw-defn idx3' style='display:none'>When you describe someone as astute, you think they quickly understand situations or behavior and use it to their advantage.</p>
<p class='rw-defn idx4' style='display:none'>If you are bemused, you are puzzled and confused; hence, you are lost or absorbed in puzzling thought.</p>
<p class='rw-defn idx5' style='display:none'>A cursory examination or reading of something is very quick, incomplete, and does not pay close attention to detail.</p>
<p class='rw-defn idx6' style='display:none'>When you discern something, you notice, detect, or understand it, often after thinking about it carefully or studying it for some time.  </p>
<p class='rw-defn idx7' style='display:none'>Someone does something in a disinterested way when they have no personal involvement or attachment to the action.</p>
<p class='rw-defn idx8' style='display:none'>An enclave is a small area within a larger area of a city or country in which people of a different nationality or culture live.</p>
<p class='rw-defn idx9' style='display:none'>If you think something, such as a statement or idea, is fatuous, you consider it stupid or extremely silly; a fatuous hope is unrealistic.</p>
<p class='rw-defn idx10' style='display:none'>When a person is being flippant, they are not taking something as seriously as they should be; therefore, they tend to dismiss things they should respectfully pay attention to.</p>
<p class='rw-defn idx11' style='display:none'>If someone is being glib, they make something sound simple, easy, and problem-free— when it isn&#8217;t at all.</p>
<p class='rw-defn idx12' style='display:none'>If someone is impassive, they are not showing any emotion.</p>
<p class='rw-defn idx13' style='display:none'>When you act in an imprudent fashion, you do something that is unwise, is lacking in good judgment, or has no forethought.</p>
<p class='rw-defn idx14' style='display:none'>If you describe someone&#8217;s behavior as inane, you think it is completely stupid or without much meaning.</p>
<p class='rw-defn idx15' style='display:none'>Someone who is indiscreet shows lack of judgment, especially because they say or do things in public that should only be said or done privately—if at all.</p>
<p class='rw-defn idx16' style='display:none'>Insouciance is a lack of concern or worry for something that should be shown more careful attention or consideration.</p>
<p class='rw-defn idx17' style='display:none'>A judicious person shows good or sound judgment because they are wise and careful in making decisions.</p>
<p class='rw-defn idx18' style='display:none'>Someone who is nonchalant is very relaxed and appears not to be worried about anything.</p>
<p class='rw-defn idx19' style='display:none'>If you are oblivious to something that is happening, you do not notice it.</p>
<p class='rw-defn idx20' style='display:none'>Someone has a parochial outlook when they are mostly concerned with narrow or limited issues that affect a small, local area or a very specific cause; they are also unconcerned with wider or more general issues.</p>
<p class='rw-defn idx21' style='display:none'>A perfunctory action is done with little care or interest; consequently, it is only completed because it is expected of someone to do so.</p>
<p class='rw-defn idx22' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx23' style='display:none'>To be politic in a decision is to be socially wise and diplomatic.</p>
<p class='rw-defn idx24' style='display:none'>Something that is within the purview of an individual or organization is within the range of its operation, authority, control, or concerns that it deals with.</p>
<p class='rw-defn idx25' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx26' style='display:none'>A sagacious person is wise, intelligent, and has the ability to make good practical decisions.</p>
<p class='rw-defn idx27' style='display:none'>If you are sapient, you are wise or very learned.</p>
<p class='rw-defn idx28' style='display:none'>Stupor is a state in which someone&#8217;s mind and senses are dulled; consequently, they are unable to think clearly or act normally, usually due to the use of alcohol or drugs.</p>
<p class='rw-defn idx29' style='display:none'>Something that is vacuous is empty or blank, such as a mind or stare.</p>
<p class='rw-defn idx30' style='display:none'>A yokel is uneducated, naive, and unsophisticated; they do not know much about modern life or ideas because they sequester themselves in a rural setting.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>myopic</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='myopic#' id='pronounce-sound' path='audio/words/amy-myopic'></a>
mahy-OP-ik
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='myopic#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='myopic#' id='context-sound' path='audio/wordcontexts/brian-myopic'></a>
People who do not recycle are <em>myopic</em> or short-sighted in their vision of the future.  They think only of the trash can; this <em>myopic</em> behavior that can only see things near at hand is destructive and irresponsible.  Some people&#8217;s <em>myopic</em> or nearsighted views continue throughout their lives&#8212;they just cannot leap beyond the present moment, not even to the next day.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>myopic</em> view?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
One that is lazy and carefree.
</li>
<li class='choice answer '>
<span class='result'></span>
One that focuses only on the present thing at hand.
</li>
<li class='choice '>
<span class='result'></span>
One that has a great vision of the future.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='myopic#' id='definition-sound' path='audio/wordmeanings/amy-myopic'></a>
A <em>myopic</em> person is unable to visualize the long-term negative consequences of their current actions; rather, they are narrowly focused upon short-term results.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>short-sighted</em>
</span>
</span>
</div>
<a class='quick-help' href='myopic#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='myopic#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/myopic/memory_hooks/6270.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>My</span></span> T<span class="emp3"><span>opic</span></span></span></span> "<span class="emp1"><span>My</span></span>ron, I really think you should change your t<span class="emp3"><span>opic</span></span> to something more interesting."  "But it's <span class="emp1"><span>my</span></span> t<span class="emp3"><span>opic</span></span>."  "Yes, <span class="emp1"><span>My</span></span>ron, but it really doesn't address what the teacher asked for."  "But it's <span class="emp1"><span>my</span></span> t<span class="emp3"><span>opic</span></span>."  "Umm, yes, it is your t<span class="emp3"><span>opic</span></span>, but it won't get you what you want, that is, a good grade."  "But it's <span class="emp1"><span>my</span></span> t<span class="emp3"><span>opic</span></span>."  <span class="emp1"><span>My</span></span>ron is so <span class="emp1"><span>my</span></span><span class="emp3"><span>opic</span></span>
</p>
</div>

<div id='memhook-button-bar'>
<a href="myopic#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/myopic/memory_hooks">Use other hook</a>
<a href="myopic#" id="memhook-use-own" url="https://membean.com/mywords/myopic/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='myopic#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Madam C.J. Walker was the first person to devise and scale a business model that addressed the hair care and beauty needs of women of color, while also challenging the <b>myopic</b> ideals of the beauty industry at that time.
<span class='attribution'>&mdash; Richelieu Dennis, American businessman</span>
<img alt="Richelieu dennis, american businessman" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Richelieu Dennis, American businessman.jpg?qdep8" width="80" />
</li>
<li>
We often get <b>myopic</b> about the team we follow and can be fooled into thinking that it is the only one going through whatever it is experiencing at a certain point. But look no further than the defending World Series champions to see that sometimes bad things happen for good teams.
<cite class='attribution'>
&mdash;
The San Diego Union-Tribune
</cite>
</li>
<li>
I think the fear is that you might get more parochial council members who were <b>myopic</b> about their district. And for really good governance, you need everybody thinking about what’s best for the city as a whole.
<cite class='attribution'>
&mdash;
Fort Worth Magazine
</cite>
</li>
<li>
[In] all the research I’ve done on the controversy of single-use bag bans, I have yet to find the objection that trumps the prescient environmental concerns that even kids appreciate. Instead, it seems many counterpoints can be traced back to the nearsightedness of consumers who resist the notion of change. . . . These are the <b>myopic</b> viewpoints that lay the groundwork for retailers’ arguments that banning plastic bags would make them less competitive.
<cite class='attribution'>
&mdash;
The Plain Dealer
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/myopic/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='myopic#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>my</td>
<td>
&rarr;
</td>
<td class='meaning'>to shut the eyes</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='op_eye' data-tree-url='//cdn1.membean.com/public/data/treexml' href='myopic#'>
<span class=''></span>
op
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>eye</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ic_like' data-tree-url='//cdn1.membean.com/public/data/treexml' href='myopic#'>
<span class=''></span>
-ic
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>like</td>
</tr>
</table>
<p>When one is being <em>myopic</em> about the doing of something, one &#8220;shuts one&#8217;s eyes&#8221; and thus remains blind to its ultimate or far-ranging results.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='myopic#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Myopic" src="https://cdn2.membean.com/public/images/wordimages/cons2/myopic.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='myopic#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abandon</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>bemused</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>cursory</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>disinterested</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>enclave</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>fatuous</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>flippant</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>glib</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>impassive</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>imprudent</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>inane</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>indiscreet</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>insouciance</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>nonchalant</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>oblivious</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>parochial</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>perfunctory</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>stupor</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>vacuous</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>yokel</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>acuity</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>acumen</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>astute</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>discern</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>judicious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>politic</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>purview</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>sagacious</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>sapient</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='myopic#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
myopia
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>nearsightedness</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="myopic" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>myopic</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="myopic#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

