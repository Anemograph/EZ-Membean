
<!DOCTYPE html>
<html>
<head>
<title>Word: consummate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Consummate-large" id="bk-img" src="https://cdn3.membean.com/public/images/wordimages/bkgd2/consummate-large.jpg?qdep8" />
</div>
<a href="consummate#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>The word abject emphasizes a very bad situation or quality, thereby making it even worse.</p>
<p class='rw-defn idx1' style='display:none'>When you are adept at something, you are very good, skillful, or talented at it.</p>
<p class='rw-defn idx2' style='display:none'>Someone who is adroit is skillful and clever, especially in thought, behavior, or action.</p>
<p class='rw-defn idx3' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx4' style='display:none'>The caliber of a person is the level or quality of their ability, intelligence, and other talents.</p>
<p class='rw-defn idx5' style='display:none'>A connoisseur is an appreciative, informed, and knowledgeable judge of the fine arts; they can also just have excellent or perceptive taste in the fine arts without being an expert.</p>
<p class='rw-defn idx6' style='display:none'>A definitive opinion on an issue cannot be challenged; therefore, it is the final word or the most authoritative pronouncement on that issue.</p>
<p class='rw-defn idx7' style='display:none'>A deft movement or action is made quickly and with skill.</p>
<p class='rw-defn idx8' style='display:none'>A dilettante is someone who frequently pursues an interest in a subject; nevertheless, they never spend the time and effort to study it thoroughly enough to master it.</p>
<p class='rw-defn idx9' style='display:none'>If you describe a person, group, or civilization as effete, you mean it is weak, exhausted,  powerless, unproductive, and/or corrupt.</p>
<p class='rw-defn idx10' style='display:none'>Efficacy is the ability or power to produce an expected effect or result.</p>
<p class='rw-defn idx11' style='display:none'>Errant things or people behave in a way that is unacceptable or wrong, such as a missile that travels in the wrong direction or a student who does not follow classroom rules.</p>
<p class='rw-defn idx12' style='display:none'>An exemplary person is one that sets an ideal or praiseworthy example for others to follow.</p>
<p class='rw-defn idx13' style='display:none'>One thing that exemplifies another serves as an example of it, illustrates it, or demonstrates it.</p>
<p class='rw-defn idx14' style='display:none'>If you think something, such as a statement or idea, is fatuous, you consider it stupid or extremely silly; a fatuous hope is unrealistic.</p>
<p class='rw-defn idx15' style='display:none'>Someone who is feckless is incompetent and lacks the determination or skill to achieve much of anything at all in life.</p>
<p class='rw-defn idx16' style='display:none'>A fledgling business endeavor is just beginning or developing.</p>
<p class='rw-defn idx17' style='display:none'>A fruitless effort at doing something does not bring about a successful result.</p>
<p class='rw-defn idx18' style='display:none'>A futile attempt at doing something is hopeless and pointless because it simply cannot be accomplished.</p>
<p class='rw-defn idx19' style='display:none'>A hapless person is unlucky, so much so that a stream of unfortunate things has happened and continues to happen to them.</p>
<p class='rw-defn idx20' style='display:none'>Someone who has had an illustrious professional career is celebrated and outstanding in their given field of expertise.</p>
<p class='rw-defn idx21' style='display:none'>Something that is immaculate is very clean, pure, or completely free from error.</p>
<p class='rw-defn idx22' style='display:none'>If you describe someone&#8217;s appearance or behavior as impeccable, you mean that it is perfect and therefore impossible to criticize.</p>
<p class='rw-defn idx23' style='display:none'>If you describe someone&#8217;s behavior as inane, you think it is completely stupid or without much meaning.</p>
<p class='rw-defn idx24' style='display:none'>Someone who is ineffectual at a task is useless or ineffective when attempting to do it.</p>
<p class='rw-defn idx25' style='display:none'>Someone who is inept is unable or unsuitable to do a job; hence, they are unfit for it.</p>
<p class='rw-defn idx26' style='display:none'>Something that has inestimable value or benefit has so much of it that it cannot be calculated.</p>
<p class='rw-defn idx27' style='display:none'>If something is infallible, it is never wrong and so is incapable of making mistakes.</p>
<p class='rw-defn idx28' style='display:none'>Ingenuity in solving a problem uses creativity, intelligence, and cleverness.</p>
<p class='rw-defn idx29' style='display:none'>Someone, such as a performer or athlete, is inimitable when they are so good or unique in their talent that it is unlikely anyone else can be their equal.</p>
<p class='rw-defn idx30' style='display:none'>If you describe ideas as jejune, you are saying they are childish and uninteresting; the adjective jejune also describes those having little experience, maturity, or knowledge.</p>
<p class='rw-defn idx31' style='display:none'>If you do something in a lackadaisical way, you do it carelessly and without putting much effort into it—thereby showing that you are not really interested in what you&#8217;re doing.</p>
<p class='rw-defn idx32' style='display:none'>When you are listless, you lack energy and interest and are unwilling to exert any effort.</p>
<p class='rw-defn idx33' style='display:none'>Something mediocre is average or ordinary in quality; it’s just OK.</p>
<p class='rw-defn idx34' style='display:none'>Someone who is meritorious is worthy of receiving recognition or is praiseworthy because of what they have accomplished.</p>
<p class='rw-defn idx35' style='display:none'>If you are a novice at an activity, you have just begun or started doing it.</p>
<p class='rw-defn idx36' style='display:none'>Something that is of paramount importance or significance is chief or supreme in those things.</p>
<p class='rw-defn idx37' style='display:none'>When you personify something, such as a quality, you are the perfect example of it.</p>
<p class='rw-defn idx38' style='display:none'>When someone exhibits profundity, they display great intellectual depth and understanding; profundity can also be the depth or complexity of something.</p>
<p class='rw-defn idx39' style='display:none'>If you have prowess, you possess considerable skill or ability in something.</p>
<p class='rw-defn idx40' style='display:none'>Something is quintessential when it is a perfect example of its type.</p>
<p class='rw-defn idx41' style='display:none'>When you have been remiss, you have been careless because you did not do something that you should have done.</p>
<p class='rw-defn idx42' style='display:none'>A sagacious person is wise, intelligent, and has the ability to make good practical decisions.</p>
<p class='rw-defn idx43' style='display:none'>A savant is a person who knows a lot about a subject, either via a lifetime of learning or by considerable natural ability.</p>
<p class='rw-defn idx44' style='display:none'>A seasoned professional has a great deal of experience in what they do.</p>
<p class='rw-defn idx45' style='display:none'>A superlative deed or act is excellent, outstanding, or the best of its kind.</p>
<p class='rw-defn idx46' style='display:none'>If you surmount a problem or difficulty, you get the better of it by conquering or overcoming it.</p>
<p class='rw-defn idx47' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx48' style='display:none'>If someone is unlettered, they are illiterate, unable to read and write.</p>
<p class='rw-defn idx49' style='display:none'>If you are unsurpassed in what you do, you are the best—period.</p>
<p class='rw-defn idx50' style='display:none'>A virtuoso is someone who is very skillful at something, especially playing a musical instrument.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>consummate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='consummate#' id='pronounce-sound' path='audio/words/amy-consummate'></a>
KON-suh-muht
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='consummate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='consummate#' id='context-sound' path='audio/wordcontexts/brian-consummate'></a>
Joe is a <em>consummate</em> teacher: he is simply the best instructor I&#8217;ve ever known.  Not only can this <em>consummate</em>, highly skilled instructor reach every student in the classroom, but he also performs his professional duties to the highest of standards.  I would have no hesitation whatsoever in hiring this <em>consummate</em>, complete, and highly accomplished teacher.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
When is someone considered a <em>consummate</em> professional?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
When they are willing to train others to do their job. 
</li>
<li class='choice '>
<span class='result'></span>
When they have completed the training required for their job.
</li>
<li class='choice answer '>
<span class='result'></span>
When they have the highest level of skill at their job.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='consummate#' id='definition-sound' path='audio/wordmeanings/amy-consummate'></a>
If someone shows <em>consummate</em> skill at doing something, that person&#8217;s skill is very great or almost perfect in every way.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>highly skilled</em>
</span>
</span>
</div>
<a class='quick-help' href='consummate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='consummate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/consummate/memory_hooks/3389.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Summ</span></span>ary of All R<span class="emp1"><span>ate</span></span>d Gr<span class="emp1"><span>eat</span></span> Literature</span></span> Shakespeare was, is, and always will be r<span class="emp1"><span>ate</span></span>d <em>the</em> con<span class="emp3"><span>summ</span></span><span class="emp1"><span>ate</span></span> writer of all time; he is the absolute <span class="emp3"><span>summ</span></span>ary of all gr<span class="emp1"><span>eat</span></span> writing.
</p>
</div>

<div id='memhook-button-bar'>
<a href="consummate#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/consummate/memory_hooks">Use other hook</a>
<a href="consummate#" id="memhook-use-own" url="https://membean.com/mywords/consummate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='consummate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Playing the Beethoven symphonies, for example, is a <b>consummate</b> experience for a musician because Beethoven speaks so directly to who we are as people.
<span class='attribution'>&mdash; Joshua Bell, American violinist and conductor</span>
<img alt="Joshua bell, american violinist and conductor" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Joshua Bell, American violinist and conductor.jpg?qdep8" width="80" />
</li>
<li>
A <b>consummate</b> self-promoter, and a skillful creator of her own myth, Annie became a global celebrity, her adventures reported by newspapers from San Francisco to Saigon and Chicago to Shanghai.
<cite class='attribution'>
&mdash;
The Christian Science Monitor
</cite>
</li>
<li>
The off-season addition of Vincent Jackson, a <b>consummate</b> pro with a similar build, has provided a role model for Williams and relieved any burden he felt as the team's top receiver.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Everyone knew that Mr Koizumi was a <b>consummate</b> showman and a hard act to follow: a possibly unique Japanese politician with a flair for the common touch.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/consummate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='consummate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='con_thoroughly' data-tree-url='//cdn1.membean.com/public/data/treexml' href='consummate#'>
<span class='common'></span>
con-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thoroughly</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='summ_highest' data-tree-url='//cdn1.membean.com/public/data/treexml' href='consummate#'>
<span class=''></span>
summ
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>highest, topmost</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ate_possess' data-tree-url='//cdn1.membean.com/public/data/treexml' href='consummate#'>
<span class=''></span>
-ate
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>possessing a certain quality</td>
</tr>
</table>
<p>When you are <em>consummate</em> at what you do, you &#8220;possess the quality of&#8221; the &#8220;highest or topmost&#8221; talent in that profession.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='consummate#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Elizabeth</strong><span> The presiding queen accuses Elizabeth of being a consummate actress.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/consummate.jpg' video_url='examplevids/consummate' video_width='350'></span>
<div id='wt-container'>
<img alt="Consummate" height="198" src="https://cdn1.membean.com/video/examplevids/consummate.jpg" width="350" />
<div class='center'>
<a href="consummate#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='consummate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Consummate" src="https://cdn2.membean.com/public/images/wordimages/cons2/consummate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='consummate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>adept</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>adroit</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>caliber</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>connoisseur</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>definitive</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>deft</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>efficacy</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>exemplary</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>exemplify</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>illustrious</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>immaculate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>impeccable</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>inestimable</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>infallible</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>ingenuity</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>inimitable</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>meritorious</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>paramount</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>personify</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>profundity</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>prowess</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>quintessential</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>sagacious</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>savant</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>seasoned</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>superlative</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>surmount</span> &middot;</li><li data-idx='49' class = 'rw-wordform notlearned'><span>unsurpassed</span> &middot;</li><li data-idx='50' class = 'rw-wordform notlearned'><span>virtuoso</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abject</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>dilettante</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>effete</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>errant</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>fatuous</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>feckless</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>fledgling</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>fruitless</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>futile</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>hapless</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>inane</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>ineffectual</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>inept</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>jejune</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>lackadaisical</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>listless</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>mediocre</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>novice</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>remiss</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li><li data-idx='48' class = 'rw-wordform notlearned'><span>unlettered</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='consummate#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
consummate
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to achieve, finish, or conclude</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="consummate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>consummate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="consummate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

