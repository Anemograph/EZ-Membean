
<!DOCTYPE html>
<html>
<head>
<title>Word: acme | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>The apex of anything, such as an organization or system, is its highest part or most important position.</p>
<p class='rw-defn idx1' style='display:none'>The apogee of something is its highest or greatest point, especially in reference to a culture or career.</p>
<p class='rw-defn idx2' style='display:none'>The best or perfect example of something is its apotheosis; likewise, this word can be used to indicate the best or highest point in someone&#8217;s life or job.</p>
<p class='rw-defn idx3' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx4' style='display:none'>If something, such as a sound, is undergoing a crescendo, it is getting louder and louder or increasing in intensity and is about to reach its peak strength.</p>
<p class='rw-defn idx5' style='display:none'>A cynosure is an object that serves as the center of attention.</p>
<p class='rw-defn idx6' style='display:none'>Debility is a state of being physically or mentally weak, usually due to illness.</p>
<p class='rw-defn idx7' style='display:none'>A denouement is the end of a book, play, or series of events when everything is explained and comes to a conclusion.</p>
<p class='rw-defn idx8' style='display:none'>If you describe a person, group, or civilization as effete, you mean it is weak, exhausted,  powerless, unproductive, and/or corrupt.</p>
<p class='rw-defn idx9' style='display:none'>Hegemony manifests when a country, group, or organization has more political control or influence than others.</p>
<p class='rw-defn idx10' style='display:none'>Someone who has had an illustrious professional career is celebrated and outstanding in their given field of expertise.</p>
<p class='rw-defn idx11' style='display:none'>The nadir of a situation is its lowest point.</p>
<p class='rw-defn idx12' style='display:none'>A paragon is an example of a thing or person at its or their very best.</p>
<p class='rw-defn idx13' style='display:none'>If someone reaches a pinnacle of something—such as a career or mountain—they have arrived at the highest point of it.</p>
<p class='rw-defn idx14' style='display:none'>If you are unsurpassed in what you do, you are the best—period.</p>
<p class='rw-defn idx15' style='display:none'>The zenith of something is its highest, most powerful, or most successful point.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>acme</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='acme#' id='pronounce-sound' path='audio/words/amy-acme'></a>
Ak-me
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='acme#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='acme#' id='context-sound' path='audio/wordcontexts/brian-acme'></a>
Jamie reached the <em>acme</em> or topmost point of her career when she found a cure for a type of cancer. Receiving the Nobel prize in medicine sweetened the <em>acme</em> or height of her considerable accomplishment. Now she is teaching at a university hospital, inspiring her students to work hard so that they can reach meaningful and similar <em>acmes</em> or peaks of accomplishment as well.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is the <em>acme</em> of someone&#8217;s career?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is the training they need to do it.
</li>
<li class='choice answer '>
<span class='result'></span>
It is the highest level they can reach in it.
</li>
<li class='choice '>
<span class='result'></span>
It is the beginning or first year of it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='acme#' id='definition-sound' path='audio/wordmeanings/amy-acme'></a>
The <em>acme</em> of something is its highest point of achievement or excellence.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>highest point</em>
</span>
</span>
</div>
<a class='quick-help' href='acme#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='acme#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/acme/memory_hooks/3893.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>ACME</span></span> Corporation is Not the <span class="emp1"><span>Acme</span></span></span></span> Looney Tunes explosives were comically branded <span class="emp1"><span>ACME</span></span> to depict that instead of being the very <em>best</em> or <span class="emp1"><span>acme</span></span> of products, they were famously unreliable and would fail at the worst possible moment.
</p>
</div>

<div id='memhook-button-bar'>
<a href="acme#" id="add-public-hook" style="" url="https://membean.com/mywords/acme/memory_hooks">Use other public hook</a>
<a href="acme#" id="memhook-use-own" url="https://membean.com/mywords/acme/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='acme#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
To win one hundred victories in one hundred battles is not the <b>acme</b> of skill. To subdue the enemy without fighting is the <b>acme</b> of skill.
<cite class='attribution'>
&mdash;
Sun Tzu, from _The Art of War_
</cite>
</li>
<li>
The Nobel Prizes for Physics, Chemistry and Medicine are the <b>acme</b> of scientific achievement—honoring great minds and life-changing discoveries.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
The cost of the collapse[d merger] is startling: it eclipses the total value of many other recent headline-grabbing bids. . . . When it was announced, the deal seemed the <b>acme</b> of the mergers that have swept through America and Europe in the past three years.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Gordon believed that to take large fish when they were shy was the <b>acme</b> of sport.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/acme/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='acme#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ac_sharp' data-tree-url='//cdn1.membean.com/public/data/treexml' href='acme#'>
<span class=''></span>
ac
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>point</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='e_used' data-tree-url='//cdn1.membean.com/public/data/treexml' href='acme#'>
<span class=''></span>
e
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>used for spelling and pronunciation</td>
</tr>
</table>
<p>The <em>acme</em> of achievement often exists as a singular point towards which all effort has been pointing.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='acme#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube: Nastia Liukin Wins Gold</strong><span> The acme of any gymnast's career is to win Olympic gold.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/acme.jpg' video_url='examplevids/acme' video_width='350'></span>
<div id='wt-container'>
<img alt="Acme" height="288" src="https://cdn1.membean.com/video/examplevids/acme.jpg" width="350" />
<div class='center'>
<a href="acme#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='acme#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Acme" src="https://cdn1.membean.com/public/images/wordimages/cons2/acme.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='acme#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>apex</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>apogee</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>apotheosis</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>crescendo</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>cynosure</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>denouement</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>hegemony</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>illustrious</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>paragon</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>pinnacle</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>unsurpassed</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>zenith</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>debility</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>effete</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>nadir</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="acme" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>acme</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="acme#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

