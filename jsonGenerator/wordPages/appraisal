
<!DOCTYPE html>
<html>
<head>
<title>Word: appraisal | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An acrimonious meeting or discussion is bitter, resentful, and filled with angry contention.</p>
<p class='rw-defn idx1' style='display:none'>If you adjudicate a competition or dispute, you officially decide who is right or what should be done concerning any difficulties that may arise.</p>
<p class='rw-defn idx2' style='display:none'>Adulation is praise and admiration for someone that includes more than they deserve, usually for the purposes of flattery.</p>
<p class='rw-defn idx3' style='display:none'>An arbiter acts as a go-between who can settle an argument or provide advice in a dispute.</p>
<p class='rw-defn idx4' style='display:none'>To assay something, such as a substance, is to analyze the chemicals present in it; it can also refer to putting something to the test.</p>
<p class='rw-defn idx5' style='display:none'>To carp at someone is to complain about them to their face or to nag and criticize them in a whining voice.</p>
<p class='rw-defn idx6' style='display:none'>To cavil is to make unnecessary complaints about things that are unimportant.</p>
<p class='rw-defn idx7' style='display:none'>A commendation is a giving of praise to someone who has won approval for excellent work.</p>
<p class='rw-defn idx8' style='display:none'>If you deliberate, you think about or discuss something very carefully, especially before making an important decision.</p>
<p class='rw-defn idx9' style='display:none'>If you delineate something, such as an idea or situation or border, you describe it in great detail.</p>
<p class='rw-defn idx10' style='display:none'>If you demur, you delay in doing or mildly object to something because you don&#8217;t really want to do it.</p>
<p class='rw-defn idx11' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx12' style='display:none'>If you disparage someone or something, you say unpleasant words that show you have no respect for that person or thing.</p>
<p class='rw-defn idx13' style='display:none'>Emolument is money or another form of payment you get for work you have done.</p>
<p class='rw-defn idx14' style='display:none'>An encomium strongly praises someone or something via oral or written communication.</p>
<p class='rw-defn idx15' style='display:none'>A eulogy is a speech or other piece of writing, often part of a funeral, in which someone or something is highly praised.</p>
<p class='rw-defn idx16' style='display:none'>If you extol something or someone, you praise it or them very enthusiastically.</p>
<p class='rw-defn idx17' style='display:none'>If you fathom something complicated or mysterious, you are able to understand it.</p>
<p class='rw-defn idx18' style='display:none'>If an idea or thought is incisive, it is expressed in a penetrating and knowledgeable manner that is clear and brief; additionally, it can demonstrate impressive understanding of related ideas or thoughts.</p>
<p class='rw-defn idx19' style='display:none'>Someone who is irresolute is unable to decide what to do.</p>
<p class='rw-defn idx20' style='display:none'>A laudatory article or speech expresses praise or admiration for someone or something.</p>
<p class='rw-defn idx21' style='display:none'>If a business is lucrative, it makes a lot of money.</p>
<p class='rw-defn idx22' style='display:none'>Something mediocre is average or ordinary in quality; it’s just OK.</p>
<p class='rw-defn idx23' style='display:none'>Something that is mundane is very ordinary and not interesting or exciting, especially because it happens very often.</p>
<p class='rw-defn idx24' style='display:none'>A paean is a piece of writing, verbal expression, or song that expresses enthusiastic praise, admiration, or joy.</p>
<p class='rw-defn idx25' style='display:none'>A panegyric is a speech or article that praises someone or something a lot.</p>
<p class='rw-defn idx26' style='display:none'>Pecuniary means relating to or concerning money.</p>
<p class='rw-defn idx27' style='display:none'>Someone who demonstrates perspicacity notices or understands things very quickly.</p>
<p class='rw-defn idx28' style='display:none'>If you receive a plaudit, you receive admiration, praise, and approval from someone.</p>
<p class='rw-defn idx29' style='display:none'>To be punctilious is to pay precise attention to detail.</p>
<p class='rw-defn idx30' style='display:none'>If you refrain from doing something, you do not do it—even though you want to.</p>
<p class='rw-defn idx31' style='display:none'>Someone&#8217;s remuneration is the payment or other rewards they receive for work completed, goods provided, or services rendered.</p>
<p class='rw-defn idx32' style='display:none'>If you repudiate something, you state that you do not accept or agree with it and no longer want to be connected with it in any way.</p>
<p class='rw-defn idx33' style='display:none'>If you ruminate on something, you think carefully and deeply about it over a long period of time.</p>
<p class='rw-defn idx34' style='display:none'>To be tentative is to be hesitant or uncertain about something; an agreement or decision of this kind is likely to have changes before it reaches its final form.</p>
<p class='rw-defn idx35' style='display:none'>A touchstone is the standard or best example by which the value or quality of something else is judged.</p>
<p class='rw-defn idx36' style='display:none'>When someone vacillates, they go back and forth about a choice or opinion, unable to make a firm decision.</p>
<p class='rw-defn idx37' style='display:none'>If you waver, you cannot decide between two things because you have serious doubts about which choice is better.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>appraisal</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='appraisal#' id='pronounce-sound' path='audio/words/amy-appraisal'></a>
uh-PRAY-zuhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='appraisal#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='appraisal#' id='context-sound' path='audio/wordcontexts/brian-appraisal'></a>
When I needed extra money, I brought my diamond ring to a jeweler for an <em>appraisal</em> to find out what it was worth before heading over to a pawn shop. The town&#8217;s <em>appraisal</em> of the jeweler herself was high; she was considered both honest and accurate in her estimations of the value of jewelry. The jeweler&#8217;s <em>appraisal</em> showed the value of the diamond to be worth $2,000; however, the owner of the pawn shop said the ring was only worth $500! Now my <em>appraisal</em> of the owner of the pawn shop has significantly decreased—I don&#8217;t think that the level of his integrity is very high at all!
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an <em>appraisal</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is the amount of money you need to pay back on a loan.
</li>
<li class='choice '>
<span class='result'></span>
It is the price you have to pay at an auction for something valuable, such as a painting.
</li>
<li class='choice answer '>
<span class='result'></span>
It is an estimation of the value of something.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='appraisal#' id='definition-sound' path='audio/wordmeanings/amy-appraisal'></a>
When someone offers an <em>appraisal</em> of something, they indicate the value or worth of it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>evaluate the value of</em>
</span>
</span>
</div>
<a class='quick-help' href='appraisal#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='appraisal#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/appraisal/memory_hooks/3264.json'></span>
<p>
<span class="emp0"><span>Due <span class="emp3"><span>Praise</span></span></span></span> I offered heartfelt <span class="emp3"><span>praise</span></span> for his great humanitarian work; he appreciated my ap<span class="emp3"><span>prais</span></span>al, thanking me graciously.
</p>
</div>

<div id='memhook-button-bar'>
<a href="appraisal#" id="add-public-hook" style="" url="https://membean.com/mywords/appraisal/memory_hooks">Use other public hook</a>
<a href="appraisal#" id="memhook-use-own" url="https://membean.com/mywords/appraisal/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='appraisal#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Because of this open invitation to free-riding, a market-based investor will not want to spend much on <b>appraisal</b> and monitoring: unlike the bank lender, whose transactions are private, he can not keep the benefits to himself.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Wouldn’t America benefit from having a society whose citizens had a more realistic <b>appraisal</b> of their economic prospects, and hence their interests?
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Paul Van Wart, a mortgage broker in Westwood, Mass., says that a lot of <b>appraisals</b> these days are coming in at values that are way too low.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Putting <b>appraisals</b> completely in the hands of lenders may sound like a good idea in principle, because it is supposed to be lenders who are putting their money at risk in a home loan.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/appraisal/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='appraisal#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ap_to' data-tree-url='//cdn1.membean.com/public/data/treexml' href='appraisal#'>
<span class=''></span>
ap-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to, towards</td>
</tr>
<tr>
<td class='partform'>prais</td>
<td>
&rarr;
</td>
<td class='meaning'>value, price</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='al_action' data-tree-url='//cdn1.membean.com/public/data/treexml' href='appraisal#'>
<span class=''></span>
-al
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>action, process</td>
</tr>
</table>
<p>An <em>appraisal</em> of a property or object is the &#8220;action or process of (bringing) a value or price to&#8221; it.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='appraisal#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Antique Roadshow</strong><span> An appraisal of an old guitar.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/appraisal.jpg' video_url='examplevids/appraisal' video_width='350'></span>
<div id='wt-container'>
<img alt="Appraisal" height="288" src="https://cdn1.membean.com/video/examplevids/appraisal.jpg" width="350" />
<div class='center'>
<a href="appraisal#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='appraisal#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Appraisal" src="https://cdn1.membean.com/public/images/wordimages/cons2/appraisal.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='appraisal#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>adjudicate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>adulation</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>arbiter</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>assay</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>commendation</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>deliberate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>delineate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>emolument</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>encomium</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>eulogy</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>extol</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>fathom</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>incisive</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>laudatory</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>lucrative</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>paean</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>panegyric</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>pecuniary</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>perspicacity</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>plaudit</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>punctilious</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>remuneration</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>ruminate</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>touchstone</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>acrimonious</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>carp</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>cavil</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>demur</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>disparage</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>irresolute</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>mediocre</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>mundane</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>refrain</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>repudiate</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>tentative</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>vacillate</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>waver</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="appraisal" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>appraisal</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="appraisal#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

