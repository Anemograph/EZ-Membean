
<!DOCTYPE html>
<html>
<head>
<title>Word: jettison | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Jettison-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/jettison-large.jpg?qdep8" />
</div>
<a href="jettison#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>If someone abdicates, they give up their responsibility for something, such as a king&#8217;s transfer of power when he gives up his throne.</p>
<p class='rw-defn idx1' style='display:none'>If you abjure a belief or a way of behaving, you state publicly that you will give it up or reject it.</p>
<p class='rw-defn idx2' style='display:none'>The abnegation of something is someone&#8217;s giving up their rights or claim to it or denying themselves of it; this action may or may not be in their best interest.</p>
<p class='rw-defn idx3' style='display:none'>If you appropriate something that does not belong to you, you take it for yourself without the right to do so.</p>
<p class='rw-defn idx4' style='display:none'>When you arrogate something, such as a position or privilege, you take it even though you don&#8217;t have the legal right to it.</p>
<p class='rw-defn idx5' style='display:none'>Avarice is the extremely strong desire to have a lot of money and possessions.</p>
<p class='rw-defn idx6' style='display:none'>If you expropriate something, you take it away for your own use although it does not belong to you; governments frequently expropriate private land to use for public purposes.</p>
<p class='rw-defn idx7' style='display:none'>When you extricate yourself from a difficult or unpleasant situation, you manage to escape it.</p>
<p class='rw-defn idx8' style='display:none'>If you recant, you publicly announce that your once firmly held beliefs or statements were wrong and that you no longer agree with them.</p>
<p class='rw-defn idx9' style='display:none'>If governments, companies, or other institutions retrench, they reduce costs and/or decrease the amount that they spend in order to save money.</p>
<p class='rw-defn idx10' style='display:none'>When something is scuttled, it is done away with or discarded.</p>
<p class='rw-defn idx11' style='display:none'>When you usurp someone else&#8217;s power, position, or role, you take it from them although you do not have the right to do so.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Verb</li>
<li>
Level 4
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>jettison</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='jettison#' id='pronounce-sound' path='audio/words/amy-jettison'></a>
JET-uh-suhn
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='jettison#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='jettison#' id='context-sound' path='audio/wordcontexts/brian-jettison'></a>
Due to the formality of his sister&#8217;s wedding, Fred decided to get rid of or <em>jettison</em> his favorite purple tennis shoes and wear his shiny dress shoes instead.  Likewise, Fred <em>jettisoned</em> or discarded his plan of bringing along his spiky-haired, loud, and rather beastly girlfriend.  Fred decided to <em>jettison</em> or cast aside his views against marriage as well for his sister&#8217;s special day.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an example of <em>jettisoning</em> something?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Finn flies his kite at the beach for many hours one summer day.
</li>
<li class='choice '>
<span class='result'></span>
Tina tugs at her ear to signal her love for her parents.
</li>
<li class='choice answer '>
<span class='result'></span>
Devin deletes his entire essay when he thinks of a better idea.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='jettison#' id='definition-sound' path='audio/wordmeanings/amy-jettison'></a>
If you <em>jettison</em> something, you get rid of it because you think it is not useful or appropriate.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>discard</em>
</span>
</span>
</div>
<a class='quick-help' href='jettison#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='jettison#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/jettison/memory_hooks/4833.json'></span>
<p>
<span class="emp0"><span>Jett<span class="emp1"><span>ison</span></span> Mad<span class="emp1"><span>ison</span></span></span></span> "Oh my god!  I couldn't believe how much Mad<span class="emp1"><span>ison</span></span> would complain and complain and complain!  And we were all stuck with her sailing across the Atlantic Ocean in a small slow boat!  Finally we had to jett<span class="emp1"><span>ison</span></span> Mad<span class="emp1"><span>ison</span></span>--good riddance!"
</p>
</div>

<div id='memhook-button-bar'>
<a href="jettison#" id="add-public-hook" style="" url="https://membean.com/mywords/jettison/memory_hooks">Use other public hook</a>
<a href="jettison#" id="memhook-use-own" url="https://membean.com/mywords/jettison/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='jettison#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Near the end of his life, the Rev. Martin Luther King Jr. felt cornered and under siege. His opposition to the Vietnam War was widely criticized, even by friends. He was being pressured both to repudiate the black power movement and to embrace it. Some of his lieutenants were urging him to <b>jettison</b> his urgent new campaign to uplift the poor, believing that King had taken on too much and was compromising support for the civil rights struggle.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
<li>
You should write down the things you need to do, the places you have to go, and ideas that occur to you. Then, at the beginning of every month, you should look at your list, <b>jettison</b> the items that are no longer relevant, and start afresh.
<cite class='attribution'>
&mdash;
Slate
</cite>
</li>
<li>
A lawsuit was filed Friday against Delta Air Lines by four teachers at an elementary school drenched by jet fuel from an airliner preparing to make an emergency landing at Los Angeles International Airport. The four say jet fuel rained down on them after it was <b>jettisoned</b> by the Boeing 777 that was returning to the airport shortly after takeoff. . . . Pilots often <b>jettison</b> fuel in order to lighten the aircraft for landing.
<cite class='attribution'>
&mdash;
USA Today
</cite>
</li>
<li>
The immediate effect of the decision will be to cut by more than half the pensions of many members of United Airlines' four unions, who have now become wards of the federal government's pension guarantee program. . . . After a federal bankruptcy judge approved a United Airlines proposal to <b>jettison</b> its pension plans, unionized employees protested outside the courthouse in Chicago.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/jettison/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='jettison#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='jet_throw' data-tree-url='//cdn1.membean.com/public/data/treexml' href='jettison#'>
<span class=''></span>
jet
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>throw, cast</td>
</tr>
</table>
<p>To <em>jettison</em> something is to &#8220;throw&#8221; or &#8220;cast&#8221; it away.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='jettison#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Knight Rider</strong><span> Kitt suggests that they jettison the bomb into the stratosphere so that its radiation does minimal damage.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/jettison.jpg' video_url='examplevids/jettison' video_width='350'></span>
<div id='wt-container'>
<img alt="Jettison" height="288" src="https://cdn1.membean.com/video/examplevids/jettison.jpg" width="350" />
<div class='center'>
<a href="jettison#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='jettison#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Jettison" src="https://cdn0.membean.com/public/images/wordimages/cons2/jettison.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='jettison#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abdicate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abjure</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>abnegation</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>extricate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>recant</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>scuttle</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='3' class = 'rw-wordform notlearned'><span>appropriate</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>arrogate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>avarice</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>expropriate</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>retrench</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>usurp</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="jettison" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>jettison</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="jettison#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

