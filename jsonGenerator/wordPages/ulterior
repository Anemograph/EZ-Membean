
<!DOCTYPE html>
<html>
<head>
<title>Word: ulterior | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you describe something as ambiguous, you mean that it is unclear or confusing because it can be understood in multiple ways.</p>
<p class='rw-defn idx1' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx2' style='display:none'>Something that is clandestine is hidden or kept a secret, often because it is illegal.</p>
<p class='rw-defn idx3' style='display:none'>If you collude with people, you work secretly with them to do something dishonest.</p>
<p class='rw-defn idx4' style='display:none'>A conclave is a meeting between a group of people who discuss something secretly.</p>
<p class='rw-defn idx5' style='display:none'>If one person connives with another, they secretly plan to achieve something of mutual benefit, usually a thing that is illegal or immoral.</p>
<p class='rw-defn idx6' style='display:none'>Covert activities or situations are secret and hidden.</p>
<p class='rw-defn idx7' style='display:none'>Someone who is disingenuous is not straightforward or is dishonest in what they say or do.</p>
<p class='rw-defn idx8' style='display:none'>To divulge something is to reveal information that was once private or secret.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is effusive expresses happiness, pleasure, admiration, praise, etc., in an extremely enthusiastic way.</p>
<p class='rw-defn idx10' style='display:none'>Someone or something that is enigmatic is mysterious and difficult to understand.</p>
<p class='rw-defn idx11' style='display:none'>If you ensconce yourself somewhere, you put yourself into a comfortable place or safe position and have no intention of moving or leaving for quite some time.</p>
<p class='rw-defn idx12' style='display:none'>When people equivocate, they avoid making a clear statement; they are deliberately vague in order to avoid giving a direct answer to a question.</p>
<p class='rw-defn idx13' style='display:none'>If you exude a quality or feeling, people easily notice that you have a large quantity of it because it flows from you; if a smell or liquid exudes from something, it flows steadily and slowly from it.</p>
<p class='rw-defn idx14' style='display:none'>An action that is flagrant shows that someone does not care if they obviously break the rules or highly offend people.</p>
<p class='rw-defn idx15' style='display:none'>If someone behaves in a furtive way, they do things sneakily and secretly in order to avoid being noticed.</p>
<p class='rw-defn idx16' style='display:none'>Something that is inconspicuous does not attract attention and is not easily seen or noticed because it is small or ordinary.</p>
<p class='rw-defn idx17' style='display:none'>People who are ingenuous are excessively trusting and believe almost everything that people tell them, especially because they have not had much life experience.</p>
<p class='rw-defn idx18' style='display:none'>Something that is insidious is dangerous because it seems harmless or not important; nevertheless, over time it gradually develops the capacity to cause harm and damage.</p>
<p class='rw-defn idx19' style='display:none'>Something that is latent, such as an undiscovered talent or undiagnosed disease, exists but is not active or has not developed at the moment—it may develop or become active in the future.</p>
<p class='rw-defn idx20' style='display:none'>Something is opaque if it is either difficult to understand or is not transparent.</p>
<p class='rw-defn idx21' style='display:none'>An overt act is not hidden or secret but is done in an open and public way.</p>
<p class='rw-defn idx22' style='display:none'>If a mood or feeling is palpable, it is so strong and intense that it is easily noticed and is almost able to be physically felt.</p>
<p class='rw-defn idx23' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx24' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx25' style='display:none'>If you sidle, you walk slowly, cautiously and often sideways in a particular direction, usually because you do not want to be noticed.</p>
<p class='rw-defn idx26' style='display:none'>A surreptitious deed is done secretly to avoid bringing any attention to it.</p>
<p class='rw-defn idx27' style='display:none'>Something that is tangible is able to be touched and thus is considered real.</p>
<p class='rw-defn idx28' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>
<p class='rw-defn idx29' style='display:none'>Wiles are clever tricks or cunning schemes that are used to persuade someone to do what you want.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>ulterior</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='ulterior#' id='pronounce-sound' path='audio/words/amy-ulterior'></a>
uhl-TEER-ee-er
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='ulterior#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='ulterior#' id='context-sound' path='audio/wordcontexts/brian-ulterior'></a>
Sophie the fashionable social mover invited Sir Reginald and his wife to the party with a hidden plan, or <em>ulterior</em> motive, in mind.  Sophie really did not like the couple, but hoped they would fall for her <em>ulterior</em> motive or concealed goal: to get an introduction to their famous friend, the Marquis Malvolio.  The sophisticated Sir Reginald was not fooled, however, and Sophie&#8217;s secret, <em>ulterior</em> plan miserably failed.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If someone has an <em>ulterior</em> purpose for doing something, what do they have?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
Many different reasons for doing something that they have made clear to everyone.
</li>
<li class='choice '>
<span class='result'></span>
A truly unselfish reason for doing it for which they would prefer not to receive recognition.
</li>
<li class='choice answer '>
<span class='result'></span>
A reason for doing something that is different than the one they are telling people.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='ulterior#' id='definition-sound' path='audio/wordmeanings/amy-ulterior'></a>
When you have an <em>ulterior</em> motive, you have a hidden reason for doing something.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>hidden</em>
</span>
</span>
</div>
<a class='quick-help' href='ulterior#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='ulterior#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/ulterior/memory_hooks/3847.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Ult</span></span>erior Sup<span class="emp3"><span>erior</span></span></span></span>  My ult<span class="emp3"><span>erior</span></span> motive for going to the beach was to see the annual migration of the whales, but it was raining hard and really windy so my friend suggested a sup<span class="emp3"><span>erior</span></span> idea of drinking hot chocolate at a cafe.
</p>
</div>

<div id='memhook-button-bar'>
<a href="ulterior#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/ulterior/memory_hooks">Use other hook</a>
<a href="ulterior#" id="memhook-use-own" url="https://membean.com/mywords/ulterior/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='ulterior#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
I really have no <b>ulterior</b> motive in taking on certain roles. I have no larger issue that I really want to show people. I'm an actor, that's all. I just do what I do.
<span class='attribution'>&mdash; Sally Field, American actor and director</span>
<img alt="Sally field, american actor and director" height="80" src="https://cdn2.membean.com/public/images/wordimages/quotes/Sally Field, American actor and director.jpg?qdep8" width="80" />
</li>
<li>
Federer sees the Haiti disaster on the news. "Let's do something.". . . Through a quick publicity blast—thanks, technology!—a capacity crowd pays $10 to watch. . . . This was not some slickly packaged event run by a management agency. . . . Also note the cause here: There are no Haitian players on tour, no event in Haiti, no Haitian sponsor, no <b>ulterior</b> motive.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
The Interior Ministry has denied any <b>ulterior</b> motives in Magnitsky’s detention, saying he was being held solely because of the tax evasion charges. (Browder says those charges were without merit.)
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
<li>
Politics—specifically, the geopolitics of chaos and confusion—may be the <b>ulterior</b> motive behind criminal actions that led to the Gulf oil spill disaster.
<cite class='attribution'>
&mdash;
The Washington Post
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/ulterior/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='ulterior#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ult_beyond' data-tree-url='//cdn1.membean.com/public/data/treexml' href='ulterior#'>
<span class=''></span>
ult
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>beyond, farther off</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ior_more' data-tree-url='//cdn1.membean.com/public/data/treexml' href='ulterior#'>
<span class=''></span>
-ior
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>more</td>
</tr>
</table>
<p><em>Ulterior</em> motives are &#8220;more beyond&#8221; or &#8220;farther off&#8221; than what lies on the surface.</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='ulterior#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Ulterior" src="https://cdn0.membean.com/public/images/wordimages/cons2/ulterior.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='ulterior#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>ambiguous</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>clandestine</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>collude</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>conclave</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>connive</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>covert</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>disingenuous</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>enigmatic</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>ensconce</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>equivocate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>furtive</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>inconspicuous</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>insidious</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>latent</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>opaque</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>sidle</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>surreptitious</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>wile</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>divulge</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>effusive</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>exude</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>flagrant</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>ingenuous</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>overt</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>palpable</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>tangible</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="ulterior" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>ulterior</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="ulterior#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

