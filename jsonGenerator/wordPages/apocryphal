
<!DOCTYPE html>
<html>
<head>
<title>Word: apocryphal | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you describe something as ambiguous, you mean that it is unclear or confusing because it can be understood in multiple ways.</p>
<p class='rw-defn idx1' style='display:none'>When the Catholic Church canonizes someone, they are officially declared a saint; this word also refers to placing a work of art or literature in an accepted group of the best of its kind.</p>
<p class='rw-defn idx2' style='display:none'>A conjecture is a theory or guess that is based on information that is not certain or complete.</p>
<p class='rw-defn idx3' style='display:none'>Something that is delusive deceives you by giving a false belief about yourself or the situation you are in.</p>
<p class='rw-defn idx4' style='display:none'>If you disabuse someone of an idea or notion, you persuade them that the idea is in fact untrue.</p>
<p class='rw-defn idx5' style='display:none'>If you describe something as ersatz, you dislike it because it is artificial or fake—and is used in place of a higher quality item.</p>
<p class='rw-defn idx6' style='display:none'>Something factitious is not genuine or natural; it is made to happen in a forced, artificial way.</p>
<p class='rw-defn idx7' style='display:none'>A fallacy is an idea or belief that is false.</p>
<p class='rw-defn idx8' style='display:none'>A mendacious person does not tell the truth.</p>
<p class='rw-defn idx9' style='display:none'>A poseur pretends to have a certain quality or social position, usually because they want to influence others in some way.</p>
<p class='rw-defn idx10' style='display:none'>If you prevaricate, you avoid giving a direct or honest answer, usually because you want to hide the truth or want to delay or avoid making a hard decision.</p>
<p class='rw-defn idx11' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx12' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx13' style='display:none'>A spurious statement is false because it is not based on sound thinking; therefore, it is not what it appears to be.</p>
<p class='rw-defn idx14' style='display:none'>To be tentative is to be hesitant or uncertain about something; an agreement or decision of this kind is likely to have changes before it reaches its final form.</p>
<p class='rw-defn idx15' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx16' style='display:none'>Verisimilitude is something&#8217;s authenticity or appearance of being real or true.</p>
<p class='rw-defn idx17' style='display:none'>Something that is veritable is authentic, actual, genuine, or without doubt.</p>
<p class='rw-defn idx18' style='display:none'>The verity of something is the truth or reality of it.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>apocryphal</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='apocryphal#' id='pronounce-sound' path='audio/words/amy-apocryphal'></a>
uh-POK-ruh-fuhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='apocryphal#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='apocryphal#' id='context-sound' path='audio/wordcontexts/brian-apocryphal'></a>
When the Stuart family moved into their rich uncle&#8217;s crumbling mansion, local <em>apocryphal</em> rumors questionably claimed that it was haunted.  While exploring the attic, Sam Stuart, the youngest son, remembered the fictitious and <em>apocryphal</em> stories told about the ghost when he thought he saw a shadow in a dark corner.  Hair raised on end, Sam shared his story at dinner, yet another incident added to the collection of doubtful, <em>apocryphal</em> tales about the house spirit.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an appropriate response to an <em>apocryphal</em> story?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Investigate its source because it may not be factual.
</li>
<li class='choice '>
<span class='result'></span>
Add details to the narrative to make it more interesting.
</li>
<li class='choice '>
<span class='result'></span>
Apply its moral lesson in your interactions with others.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='apocryphal#' id='definition-sound' path='audio/wordmeanings/amy-apocryphal'></a>
An <em>apocryphal</em> story is widely known but probably not true.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>of questionable veracity</em>
</span>
</span>
</div>
<a class='quick-help' href='apocryphal#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='apocryphal#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/apocryphal/memory_hooks/6198.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>A Pocket Full</span></span>?</span></span> Pinocchio once said he'd collected <span class="emp1"><span>a</span></span> whole <span class="emp1"><span>pocket full</span></span> of gold coins ... was Pinocchio's story true, or <span class="emp1"><span>apocryphal</span></span>, <span class="emp1"><span>a pocket full</span></span> of lies?
</p>
</div>

<div id='memhook-button-bar'>
<a href="apocryphal#" id="add-public-hook" style="" url="https://membean.com/mywords/apocryphal/memory_hooks">Use other public hook</a>
<a href="apocryphal#" id="memhook-use-own" url="https://membean.com/mywords/apocryphal/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='apocryphal#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The story is <b>apocryphal</b> but widely believed because it epitomizes Steve Jobs and his unflagging obsession with originality, engineering authenticity, and design detail.
<cite class='attribution'>
&mdash;
Christian Science Monitor
</cite>
</li>
<li>
Springdale residents who remember Rachel as a young girl tell the story, perhaps true, perhaps <b>apocryphal</b>, that her romance with the ocean began one day when she found a large fossilized shell in the rocky outcroppings on the family’s hillside property.
<cite class='attribution'>
&mdash;
Linda Lear, historian, from _Rachel Carson: Witness for Nature_
</cite>
</li>
<li>
The grave of Abelard and Heloise: if someone proves to you that it is <b>apocryphal</b>, exclaim: "You are robbing me of my illusions!"
<cite class='attribution'>
&mdash;
John Forrester, historian and philosopher, from _Dispatches from the Freud Wars
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/apocryphal/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='apocryphal#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='apo_away' data-tree-url='//cdn1.membean.com/public/data/treexml' href='apocryphal#'>
<span class=''></span>
apo-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>away, from</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='crypt_hidden' data-tree-url='//cdn1.membean.com/public/data/treexml' href='apocryphal#'>
<span class=''></span>
crypt
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>hidden</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='al_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='apocryphal#'>
<span class=''></span>
-al
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>The truth of an <em>apocryphal</em> tale or belief is &#8220;hidden away.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='apocryphal#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Apocryphal" src="https://cdn0.membean.com/public/images/wordimages/cons2/apocryphal.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='apocryphal#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>ambiguous</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>conjecture</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>delusive</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>ersatz</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>factitious</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>fallacy</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>mendacious</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>poseur</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>prevaricate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>spurious</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>tentative</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='1' class = 'rw-wordform notlearned'><span>canonize</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>disabuse</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>verisimilitude</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>veritable</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="apocryphal" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>apocryphal</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="apocryphal#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

