
<!DOCTYPE html>
<html>
<head>
<title>Word: execrate | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>If you abhor something, you dislike it very much, usually because you think it&#8217;s immoral.</p>
<p class='rw-defn idx1' style='display:none'>If you abominate something, you hate it because you think it is extremely wrong and unacceptable.</p>
<p class='rw-defn idx2' style='display:none'>When you absolve someone, you publicly and formally say that they are not guilty or responsible for any wrongdoing.</p>
<p class='rw-defn idx3' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx4' style='display:none'>Amity is a peaceful friendship between two parties, particularly between two countries.</p>
<p class='rw-defn idx5' style='display:none'>If something is anathema to you, such as a cursed object or idea, you very strongly dislike it or even hate it.</p>
<p class='rw-defn idx6' style='display:none'>If you have animus against someone, you have a strong feeling of dislike or hatred towards them, often without a good reason or based upon your personal prejudices.</p>
<p class='rw-defn idx7' style='display:none'>Antipathy is a strong feeling of dislike or hostility towards someone or something.</p>
<p class='rw-defn idx8' style='display:none'>Approbation is official praise or approval of something.</p>
<p class='rw-defn idx9' style='display:none'>A bane is something that causes ruin, misery, or destruction.</p>
<p class='rw-defn idx10' style='display:none'>Someone who is benevolent wishes others well, often by being kind, filled with goodwill, and charitable towards them.</p>
<p class='rw-defn idx11' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx12' style='display:none'>Someone who has a bilious personality is highly irritable and bad-tempered.</p>
<p class='rw-defn idx13' style='display:none'>Censure is written or verbal disapproval and harsh criticism of something that someone has done.</p>
<p class='rw-defn idx14' style='display:none'>If someone is granted clemency, they are punished less severely than they could have been—they have been shown mercy.</p>
<p class='rw-defn idx15' style='display:none'>If someone will countenance something, they will approve, tolerate, or support it.</p>
<p class='rw-defn idx16' style='display:none'>To decry something is to speak against it and find fault with it.</p>
<p class='rw-defn idx17' style='display:none'>If you behave with deference towards someone, you show them respect and accept their opinions or decisions, especially because they have an important position.</p>
<p class='rw-defn idx18' style='display:none'>To defile something holy or important is to do something to it or say something about it that is highly offensive to others.</p>
<p class='rw-defn idx19' style='display:none'>If you denigrate something, you criticize or speak ill of it in a way that shows you think it has little to no value at all.</p>
<p class='rw-defn idx20' style='display:none'>If you denounce people or actions, you criticize them severely in public because you feel strongly that they are wrong or evil.</p>
<p class='rw-defn idx21' style='display:none'>If you say a person&#8217;s actions are despicable, you think they are extremely unpleasant or nasty.</p>
<p class='rw-defn idx22' style='display:none'>If you say something is diabolical, you are emphasizing that it is evil, cruel, or very bad.</p>
<p class='rw-defn idx23' style='display:none'>When you are disposed towards a particular action or thing, you are inclined or partial towards it.</p>
<p class='rw-defn idx24' style='display:none'>If you excoriate someone, you express very strong disapproval of something they did.</p>
<p class='rw-defn idx25' style='display:none'>If you exculpate someone, you prove that they are not guilty of a crime.</p>
<p class='rw-defn idx26' style='display:none'>When someone is exonerated from guilt, a task, or a responsibility, they are set free or excused from it.</p>
<p class='rw-defn idx27' style='display:none'>If you are experiencing felicity about something, you are very happy about it.</p>
<p class='rw-defn idx28' style='display:none'>If you describe something as heinous, you mean that is extremely evil, shocking, and bad.</p>
<p class='rw-defn idx29' style='display:none'>When you pay homage to another person, you show them great admiration or respect; you might even worship them.</p>
<p class='rw-defn idx30' style='display:none'>If you believe someone&#8217;s behavior exhibits idolatry, you think they blindly admire someone or something too much.</p>
<p class='rw-defn idx31' style='display:none'>If you hurl invective at another person, you are verbally abusing or highly criticizing them.</p>
<p class='rw-defn idx32' style='display:none'>When someone is lionized, they are treated as being very important or famous—although they may not deserve to be.</p>
<p class='rw-defn idx33' style='display:none'>Something loathsome is offensive, disgusting, and brings about intense dislike.</p>
<p class='rw-defn idx34' style='display:none'>A malevolent person or thing is evil due to deliberate attempts to cause harm.</p>
<p class='rw-defn idx35' style='display:none'>Malfeasance is an unlawful act, especially one committed by a trusted public official.</p>
<p class='rw-defn idx36' style='display:none'>If you malign someone, you say unpleasant things about them to damage their reputation.</p>
<p class='rw-defn idx37' style='display:none'>A malignant thing or person, such as a tumor or criminal, is deadly or does great harm.</p>
<p class='rw-defn idx38' style='display:none'>If someone is <em>pilloried</em>, they are publicly criticized or ridiculed by a lot of people, especially in the media.</p>
<p class='rw-defn idx39' style='display:none'>A proclivity is the tendency to behave in a particular way or to like a particular thing.</p>
<p class='rw-defn idx40' style='display:none'>A propensity is a natural tendency towards a particular behavior.</p>
<p class='rw-defn idx41' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx42' style='display:none'>A reprobate has a bad character and behaves in an immoral or improper way.</p>
<p class='rw-defn idx43' style='display:none'>If you repudiate something, you state that you do not accept or agree with it and no longer want to be connected with it in any way.</p>
<p class='rw-defn idx44' style='display:none'>Repugnance is a strong feeling of dislike for something or someone you find horrible and offensive.</p>
<p class='rw-defn idx45' style='display:none'>When you are reverent, you show a great deal of respect, admiration, or even awe for someone or something.</p>
<p class='rw-defn idx46' style='display:none'>Something that is sacrosanct is considered to be so important, special, or holy that no one is allowed to criticize, tamper with, or change it in any way.</p>
<p class='rw-defn idx47' style='display:none'>Venerable people command respect because they are old and wise.</p>
<p class='rw-defn idx48' style='display:none'>If you vilify people, you write or say bad things about them in an attempt to make them unpopular.</p>
<p class='rw-defn idx49' style='display:none'>If a person is vindicated, their ideas, decisions, or actions—once considered wrong—are proved correct or not to be blamed.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>execrate</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='execrate#' id='pronounce-sound' path='audio/words/amy-execrate'></a>
EK-si-krayt
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='execrate#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='execrate#' id='context-sound' path='audio/wordcontexts/brian-execrate'></a>
Throughout the centuries people from one religion have <em>execrated</em> or cursed people from other religions.  They tended to <em>execrate</em> or show their intense dislike because they were convinced that their way was the only right way, so anyone else must be an enemy.  Even today we find intense religious people who <em>execrate</em> or feel great dislike or even disgust towards others who don&#8217;t think the way that they do.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
Which of the following lines from Shakespeare is an example of <em>execrating</em> someone?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
You are not worth the dust which the rude wind / Blows in your face.—King Lear
</li>
<li class='choice '>
<span class='result'></span>
Tomorrow, and tomorrow, and tomorrow, / Creeps in this petty pace from day to day,—Macbeth
</li>
<li class='choice '>
<span class='result'></span>
There is a tide in the affairs of men, / Which, taken at the flood, leads on to fortune;—Julius Caesar
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='execrate#' id='definition-sound' path='audio/wordmeanings/amy-execrate'></a>
When you <em>execrate</em> someone, you curse them to show your intense dislike or hatred of them.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>curse</em>
</span>
</span>
</div>
<a class='quick-help' href='execrate#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='execrate#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/execrate/memory_hooks/3309.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Ex</span></span>ed out the <span class="emp2"><span>Crate</span></span></span></span> I hated that <span class="emp2"><span>crate</span></span> so much that I <span class="emp1"><span>exe</span></span><span class="emp2"><span>crate</span></span><span class="emp1"><span>d</span></span> that <span class="emp2"><span>crate</span></span> by <span class="emp1"><span>ex</span></span>ing it out with a big red Sharpie!
</p>
</div>

<div id='memhook-button-bar'>
<a href="execrate#" id="add-public-hook" style="" url="https://membean.com/mywords/execrate/memory_hooks">Use other public hook</a>
<a href="execrate#" id="memhook-use-own" url="https://membean.com/mywords/execrate/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='execrate#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
With every drop of my blood I hate and <b>execrate</b> every form of tyranny, every form of slavery. I hate dictation. I love liberty.
<span class='attribution'>&mdash; Robert G. Ingersoll, 19th century American writer </span>
<img alt="Robert g" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Robert G. Ingersoll, 19th century American writer .jpg?qdep8" width="80" />
</li>
<li>
We have certainly become too accustomed to hearing beneficiaries of the status quo deride compassion, despite its awful scarcity, as a “vice”—to use Jordan Peterson’s pejorative—and loudly <b>execrate</b> “social justice warriors” while presenting as immutable scientific fact the socially constructed hierarchy in which they are on top.
<cite class='attribution'>
&mdash;
The New York Review of Books
</cite>
</li>
<li>
"When I conclude that a particular usage is execrable, I can <b>execrate</b> at the top of my lungs," he wrote in the introduction to his 1984 _The Writer’s Art_, also the title of his long-running syndicated column.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/execrate/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='execrate#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ex_out' data-tree-url='//cdn1.membean.com/public/data/treexml' href='execrate#'>
<span class='common'></span>
ex-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>out of, from, off</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='sacr_sacred' data-tree-url='//cdn1.membean.com/public/data/treexml' href='execrate#'>
<span class=''></span>
sacr
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>sacred, holy</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ate_quality' data-tree-url='//cdn1.membean.com/public/data/treexml' href='execrate#'>
<span class=''></span>
-ate
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>make someone have a certain quality</td>
</tr>
</table>
<p>To <em>execrate</em> someone is to bring him &#8220;outside holiness.&#8221;  Note: an &#8220;x&#8221; is linguistically a &#8220;ks.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='execrate#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Execrate" src="https://cdn3.membean.com/public/images/wordimages/cons2/execrate.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='execrate#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>abhor</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abominate</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>anathema</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>animus</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>antipathy</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>bane</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>bilious</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>censure</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>decry</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>defile</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>denigrate</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>denounce</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>despicable</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>diabolical</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>excoriate</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>heinous</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>invective</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>loathsome</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>malevolent</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>malfeasance</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>malign</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>malignant</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>pillory</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>reprobate</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>repudiate</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>repugnance</span> &middot;</li><li data-idx='48' class = 'rw-wordform notlearned'><span>vilify</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='2' class = 'rw-wordform notlearned'><span>absolve</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>amity</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>approbation</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>benevolent</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>clemency</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>countenance</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>deference</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>disposed</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>exculpate</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>exonerate</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>felicity</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>homage</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>idolatry</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>lionize</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>proclivity</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>propensity</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>reverent</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>sacrosanct</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>venerable</span> &middot;</li><li data-idx='49' class = 'rw-wordform notlearned'><span>vindicate</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='execrate#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
execration
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>curse</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="execrate" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>execrate</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="execrate#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

