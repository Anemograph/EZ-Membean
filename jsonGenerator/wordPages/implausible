
<!DOCTYPE html>
<html>
<head>
<title>Word: implausible | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn2.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An adage is an inherited saying or phrase that has been historically used to express a common experience.</p>
<p class='rw-defn idx1' style='display:none'>If you describe something as ambiguous, you mean that it is unclear or confusing because it can be understood in multiple ways.</p>
<p class='rw-defn idx2' style='display:none'>An aphorism is a short, witty statement that contains a wise idea.</p>
<p class='rw-defn idx3' style='display:none'>An axiom is a wise saying or widely recognized general truth that is often self-evident; it can also be an established rule or law.</p>
<p class='rw-defn idx4' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx5' style='display:none'>Someone who is capricious changes their mind repeatedly or behaves in unexpected ways; a capricious series of events follows no predictable pattern.</p>
<p class='rw-defn idx6' style='display:none'>If you employ chicanery, you are devising and carrying out clever plans and trickery to cheat and deceive people.</p>
<p class='rw-defn idx7' style='display:none'>A chimerical idea is unrealistic, imaginary, or unlikely to be fulfilled.</p>
<p class='rw-defn idx8' style='display:none'>If you are ___, you are cautious; you think carefully about something before saying or doing it.</p>
<p class='rw-defn idx9' style='display:none'>A confidant is a close and trusted friend to whom you can tell secret matters of importance and remain assured that they will be kept safe.</p>
<p class='rw-defn idx10' style='display:none'>A conjecture is a theory or guess that is based on information that is not certain or complete.</p>
<p class='rw-defn idx11' style='display:none'>To corroborate something that has been said or reported means to give information or evidence that further supports it.</p>
<p class='rw-defn idx12' style='display:none'>Credence is the mental act of believing or accepting that something is true.</p>
<p class='rw-defn idx13' style='display:none'>If you act in a credible fashion, you are easy to believe or trust.</p>
<p class='rw-defn idx14' style='display:none'>A credulous person is very ready to believe what people tell them; therefore, they can be easily tricked or cheated.</p>
<p class='rw-defn idx15' style='display:none'>A cynical person thinks that people in general are most often motivated by selfish concerns; therefore, they doubt, mistrust, and question what people do.</p>
<p class='rw-defn idx16' style='display:none'>When you debunk someone&#8217;s statement, you show that it is false, thereby exposing the truth of the matter.</p>
<p class='rw-defn idx17' style='display:none'>Something that is delusive deceives you by giving a false belief about yourself or the situation you are in.</p>
<p class='rw-defn idx18' style='display:none'>If someone makes a dubious claim, there is a great deal of disbelief or doubt about it.</p>
<p class='rw-defn idx19' style='display:none'>Empirical evidence or study is based on real experience or scientific experiments rather than on unproven theories.</p>
<p class='rw-defn idx20' style='display:none'>When people equivocate, they avoid making a clear statement; they are deliberately vague in order to avoid giving a direct answer to a question.</p>
<p class='rw-defn idx21' style='display:none'>If you describe something as ersatz, you dislike it because it is artificial or fake—and is used in place of a higher quality item.</p>
<p class='rw-defn idx22' style='display:none'>A euphemism is a polite synonym or expression that people use when they want to avoid talking about—or indirectly replace—an unpleasant or embarrassing thing.</p>
<p class='rw-defn idx23' style='display:none'>A fallacy is an idea or belief that is false.</p>
<p class='rw-defn idx24' style='display:none'>If you think something, such as a statement or idea, is fatuous, you consider it stupid or extremely silly; a fatuous hope is unrealistic.</p>
<p class='rw-defn idx25' style='display:none'>Fidelity towards something, such as a marriage or promise, is faithfulness or loyalty towards it.</p>
<p class='rw-defn idx26' style='display:none'>A gullible person is easy to trick because they are too trusting of other people.</p>
<p class='rw-defn idx27' style='display:none'>Something that is hypothetical is based on possible situations or events rather than actual ones.</p>
<p class='rw-defn idx28' style='display:none'>Something illusory appears real or possible; in fact, it is neither.</p>
<p class='rw-defn idx29' style='display:none'>Something that is inconceivable cannot be imagined or thought of; that is, it is beyond reason or unbelievable.</p>
<p class='rw-defn idx30' style='display:none'>Incontrovertible facts are certain, unquestionably true, and impossible to doubt however hard you might try to convince yourself otherwise.</p>
<p class='rw-defn idx31' style='display:none'>An irrefutable argument or statement cannot be proven wrong; therefore, it must be accepted because it is certain.</p>
<p class='rw-defn idx32' style='display:none'>A maxim is a recognized rule of conduct or a general statement of a truth or principle.</p>
<p class='rw-defn idx33' style='display:none'>A mendacious person does not tell the truth.</p>
<p class='rw-defn idx34' style='display:none'>A mountebank is a smooth-talking, dishonest person who tricks and cheats people.</p>
<p class='rw-defn idx35' style='display:none'>If you describe something as nebulous, you mean that it is unclear, vague, and not clearly defined; a shape that is nebulous has no clear boundaries.</p>
<p class='rw-defn idx36' style='display:none'>Something that is notional exists only as an idea or in theory—not in reality.</p>
<p class='rw-defn idx37' style='display:none'>If someone is objective, they base their opinions on facts rather than personal feelings or beliefs.</p>
<p class='rw-defn idx38' style='display:none'>A poseur pretends to have a certain quality or social position, usually because they want to influence others in some way.</p>
<p class='rw-defn idx39' style='display:none'>If you handle something in a pragmatic way, you deal with it in a realistic and practical manner rather than just following unproven theories or ideas.</p>
<p class='rw-defn idx40' style='display:none'>A precept is a rule or principle that teaches correct behavior.</p>
<p class='rw-defn idx41' style='display:none'>If you prevaricate, you avoid giving a direct or honest answer, usually because you want to hide the truth or want to delay or avoid making a hard decision.</p>
<p class='rw-defn idx42' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx43' style='display:none'>Quixotic plans or ideas are not very practical or realistic; they are often based on unreasonable hopes for improving the world.</p>
<p class='rw-defn idx44' style='display:none'>Something that is specious seems to be good, sound, or correct but is actually wrong or fake.</p>
<p class='rw-defn idx45' style='display:none'>A spurious statement is false because it is not based on sound thinking; therefore, it is not what it appears to be.</p>
<p class='rw-defn idx46' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx47' style='display:none'>Substantive issues are the most important, serious, and real issues of a subject.</p>
<p class='rw-defn idx48' style='display:none'>A tenable argument is able to be maintained or defended because there is sufficient evidence to support it.</p>
<p class='rw-defn idx49' style='display:none'>To be tentative is to be hesitant or uncertain about something; an agreement or decision of this kind is likely to have changes before it reaches its final form.</p>
<p class='rw-defn idx50' style='display:none'>Something tenuous is thin, weak, and unconvincing.</p>
<p class='rw-defn idx51' style='display:none'>An unwarranted decision cannot be justified because there is no evidence to back it up; therefore, it is needless and uncalled-for.</p>
<p class='rw-defn idx52' style='display:none'>A utilitarian object is useful, serviceable, and practical—rather than fancy or unnecessary.</p>
<p class='rw-defn idx53' style='display:none'>A veneer is a thin layer, such as a thin sheet of expensive wood over a cheaper type of wood that gives a false appearance of higher quality; a person can also put forth a false front or veneer.</p>
<p class='rw-defn idx54' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx55' style='display:none'>Verisimilitude is something&#8217;s authenticity or appearance of being real or true.</p>
<p class='rw-defn idx56' style='display:none'>Something that is veritable is authentic, actual, genuine, or without doubt.</p>
<p class='rw-defn idx57' style='display:none'>The verity of something is the truth or reality of it.</p>
<p class='rw-defn idx58' style='display:none'>A viable project is practical or can be accomplished; therefore, it is worth doing.</p>
<p class='rw-defn idx59' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>implausible</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='implausible#' id='pronounce-sound' path='audio/words/amy-implausible'></a>
im-PLAW-zuh-buhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='implausible#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='implausible#' id='context-sound' path='audio/wordcontexts/brian-implausible'></a>
It is <em>implausible</em> or unlikely that astronomers will reclassify Pluto as a planet, at least any time soon.  Because so much study has gone into classifying it as other than a planet, it is <em>implausible</em> or hard to believe that the scientific community will reverse its decision.  It is also <em>implausible</em> or unreasonable to think that humans will be able to visit Pluto bodily any time soon, since space travel is still in its earliest phases.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If a friend&#8217;s story is <em>implausible</em>, what is it?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
It is true but some details have intentionally been left out.
</li>
<li class='choice answer '>
<span class='result'></span>
It is unlikely to be true and therefore hard to believe.
</li>
<li class='choice '>
<span class='result'></span>
It is very believable and almost certainly true.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='implausible#' id='definition-sound' path='audio/wordmeanings/amy-implausible'></a>
Something that is <em>implausible</em> is unlikely to be true or hard to believe that it&#8217;s true.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>improbable</em>
</span>
</span>
</div>
<a class='quick-help' href='implausible#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='implausible#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/implausible/memory_hooks/5618.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Impossible</span></span></span></span> Something <span class="emp1"><span>implausible</span></span> is next to <span class="emp1"><span>impossible</span></span> to do.
</p>
</div>

<div id='memhook-button-bar'>
<a href="implausible#" id="add-public-hook" style="" url="https://membean.com/mywords/implausible/memory_hooks">Use other public hook</a>
<a href="implausible#" id="memhook-use-own" url="https://membean.com/mywords/implausible/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='implausible#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The question now was, how fast were the glaciers moving? The answer, Hamilton knew, could have profound implications for the world’s coasts. . . . Stearns opened her laptop and started downloading data from the monitors. When she was done, the speed was so <b>implausible</b> that she checked her calculations five times to make sure she had the math right before she showed her boss.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
<li>
The opera has a raft of over-the-top characters populating a story so complex and unlikely, it could be fairly described either as incomprehensible or <b>implausible</b>, if not both.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
Toyota Motor Co. is making a $394 million investment in Joby Aviation, one of the handful of companies with the seemingly <b>implausible</b> goal of making electric air taxis that shuttle people over gridlocked highways and city streets.
<cite class='attribution'>
&mdash;
Dallas Morning News
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/implausible/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='implausible#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='im_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='implausible#'>
<span class='common'></span>
im-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='plaus_applaud' data-tree-url='//cdn1.membean.com/public/data/treexml' href='implausible#'>
<span class=''></span>
plaus
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>applaud, clap, strike</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ible_handy' data-tree-url='//cdn1.membean.com/public/data/treexml' href='implausible#'>
<span class=''></span>
-ible
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>handy, capable of</td>
</tr>
</table>
<p>An <em>implausible</em> argument is &#8220;not capable of (being) clapped&#8221; for or winning any &#8220;applause.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='implausible#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>History Channel (Ancient Astronauts)</strong><span> What this man is saying is very implausible, not to mention im"paws"ible.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/implausible.jpg' video_url='examplevids/implausible' video_width='350'></span>
<div id='wt-container'>
<img alt="Implausible" height="288" src="https://cdn1.membean.com/video/examplevids/implausible.jpg" width="350" />
<div class='center'>
<a href="implausible#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='implausible#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Implausible" src="https://cdn1.membean.com/public/images/wordimages/cons2/implausible.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='implausible#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>ambiguous</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>capricious</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>chicanery</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>chimerical</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>circumspect</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>conjecture</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>cynical</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>debunk</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>delusive</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>dubious</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>equivocate</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>ersatz</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>euphemism</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>fallacy</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>fatuous</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>hypothetical</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>illusory</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>inconceivable</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>mendacious</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>mountebank</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>nebulous</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>notional</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>poseur</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>prevaricate</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>quixotic</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>specious</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>spurious</span> &middot;</li><li data-idx='49' class = 'rw-wordform notlearned'><span>tentative</span> &middot;</li><li data-idx='50' class = 'rw-wordform notlearned'><span>tenuous</span> &middot;</li><li data-idx='51' class = 'rw-wordform notlearned'><span>unwarranted</span> &middot;</li><li data-idx='53' class = 'rw-wordform notlearned'><span>veneer</span> &middot;</li><li data-idx='59' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>adage</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>aphorism</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>axiom</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>confidant</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>corroborate</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>credence</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>credible</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>credulous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>empirical</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>fidelity</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>gullible</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>incontrovertible</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>irrefutable</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>maxim</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>objective</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>pragmatic</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>precept</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>substantive</span> &middot;</li><li data-idx='48' class = 'rw-wordform notlearned'><span>tenable</span> &middot;</li><li data-idx='52' class = 'rw-wordform notlearned'><span>utilitarian</span> &middot;</li><li data-idx='54' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='55' class = 'rw-wordform notlearned'><span>verisimilitude</span> &middot;</li><li data-idx='56' class = 'rw-wordform notlearned'><span>veritable</span> &middot;</li><li data-idx='57' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li><li data-idx='58' class = 'rw-wordform notlearned'><span>viable</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="implausible" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>implausible</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="implausible#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

