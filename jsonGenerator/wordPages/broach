
<!DOCTYPE html>
<html>
<head>
<title>Word: broach | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>An abortive attempt or action is cut short before it is finished; hence, it is unsuccessful.</p>
<p class='rw-defn idx1' style='display:none'>Abstinence is the practice of keeping away from or avoiding something you enjoy—such as the physical pleasures of excessive food and drink—usually for health or religious reasons.</p>
<p class='rw-defn idx2' style='display:none'>When you are apprised of something, you are given information about it.</p>
<p class='rw-defn idx3' style='display:none'>If you circumvent something, such as a rule or restriction, you try to get around it in a clever and perhaps dishonest way.</p>
<p class='rw-defn idx4' style='display:none'>Something that is clandestine is hidden or kept a secret, often because it is illegal.</p>
<p class='rw-defn idx5' style='display:none'>Covert activities or situations are secret and hidden.</p>
<p class='rw-defn idx6' style='display:none'>When someone disinters a dead body, they dig it up; likewise, something disinterred is exposed or revealed to the public after having been hidden for some time.</p>
<p class='rw-defn idx7' style='display:none'>When people dissemble, they hide their real thoughts, feelings, or intentions.</p>
<p class='rw-defn idx8' style='display:none'>To divulge something is to reveal information that was once private or secret.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is dour is serious, stubborn, and unfriendly; they can also be gloomy.</p>
<p class='rw-defn idx10' style='display:none'>If you elucidate something, you make it easier to understand by giving more relevant information.</p>
<p class='rw-defn idx11' style='display:none'>If you ensconce yourself somewhere, you put yourself into a comfortable place or safe position and have no intention of moving or leaving for quite some time.</p>
<p class='rw-defn idx12' style='display:none'>If you eschew something, you deliberately avoid doing it, especially for moral reasons.</p>
<p class='rw-defn idx13' style='display:none'>If you evince particular feelings, qualities, or attitudes, you show them, often clearly.</p>
<p class='rw-defn idx14' style='display:none'>To explicate an idea or plan is to make it clear by explaining it.</p>
<p class='rw-defn idx15' style='display:none'>If you expunge something, you completely remove it or do away with it.</p>
<p class='rw-defn idx16' style='display:none'>If you extirpate something, you completely destroy it because it is unpleasant or unwanted.</p>
<p class='rw-defn idx17' style='display:none'>If someone behaves in a furtive way, they do things sneakily and secretly in order to avoid being noticed.</p>
<p class='rw-defn idx18' style='display:none'>A garrulous person talks a lot, especially about unimportant things.</p>
<p class='rw-defn idx19' style='display:none'>An interlocutor is the person with whom you are having a (usually formal) conversation or discussion.</p>
<p class='rw-defn idx20' style='display:none'>Something that is interminable continues for a very long time in a boring or annoying way.</p>
<p class='rw-defn idx21' style='display:none'>Something that is latent, such as an undiscovered talent or undiagnosed disease, exists but is not active or has not developed at the moment—it may develop or become active in the future.</p>
<p class='rw-defn idx22' style='display:none'>Someone who is loquacious talks too much.</p>
<p class='rw-defn idx23' style='display:none'>To obfuscate something is to make it deliberately unclear, confusing, and difficult to understand.</p>
<p class='rw-defn idx24' style='display:none'>To obviate a problem is to eliminate it or do something that makes solving the problem unnecessary.</p>
<p class='rw-defn idx25' style='display:none'>If you recant, you publicly announce that your once firmly held beliefs or statements were wrong and that you no longer agree with them.</p>
<p class='rw-defn idx26' style='display:none'>If you refrain from doing something, you do not do it—even though you want to.</p>
<p class='rw-defn idx27' style='display:none'>If you renege on a deal, agreement, or promise, you do not do what you promised or agreed to do.</p>
<p class='rw-defn idx28' style='display:none'>When someone in power rescinds a law or agreement, they officially end it and state that it is no longer valid.</p>
<p class='rw-defn idx29' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx30' style='display:none'>A surreptitious deed is done secretly to avoid bringing any attention to it.</p>
<p class='rw-defn idx31' style='display:none'>If someone acts in an unobtrusive way, their actions are not easily noticed and do not stand out in any way.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Verb</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>broach</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='broach#' id='pronounce-sound' path='audio/words/amy-broach'></a>
brohch
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='broach#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='broach#' id='context-sound' path='audio/wordcontexts/brian-broach'></a>
Even though it was a tense topic, Eva finally <em><em>broached</em></em> or brought up the subject of her daughter&#8217;s wedding with her husband.  Eric did not approve of his future son-in-law, and when Eva <em><em>broached</em></em> or mentioned the idea of paying for the ceremony, Eric lost his temper.  Eva wisely decided that she would wait until his bad mood had passed before <em><em>broaching</em></em> the topic of the upcoming wedding again with her husband.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
How would you <em>broach</em> a subject with a friend?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
In a gentle fashion as it may be difficult to talk about.
</li>
<li class='choice '>
<span class='result'></span>
On a daily basis because it is an ongoing problem.
</li>
<li class='choice '>
<span class='result'></span>
Right away because it could be considered an emergency.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='broach#' id='definition-sound' path='audio/wordmeanings/amy-broach'></a>
When you <em>broach</em> a subject, especially one that may be embarrassing or unpleasant, you mention it in order to begin a discussion about it.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>bring up</em>
</span>
</span>
</div>
<a class='quick-help' href='broach#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='broach#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/broach/memory_hooks/6068.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Bro</span></span>ther <span class="emp1"><span>Ach</span></span>e</span></span> Please don't <span class="emp3"><span>bro</span></span><span class="emp1"><span>ach</span></span> the subject ever again about my lost <span class="emp3"><span>bro</span></span>ther--my heart<span class="emp1"><span>ach</span></span>e for him is too great.
</p>
</div>

<div id='memhook-button-bar'>
<a href="broach#" id="add-public-hook" style="" url="https://membean.com/mywords/broach/memory_hooks">Use other public hook</a>
<a href="broach#" id="memhook-use-own" url="https://membean.com/mywords/broach/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='broach#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Parents can see their children’s efforts to <b>broach</b> [difficult] topics as attempts to take control, while the adult children may shy away from intruding into their parents' lives.
<cite class='attribution'>
&mdash;
Los Angeles Times
</cite>
</li>
<li>
The Democrats do not even plan to <b>broach</b> the subject until later in the year, after their first energy bill has got through.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
Immortality. I notice that as soon as writers <b>broach</b> this question they begin to quote. I hate quotation. Tell me what you know.
<cite class='attribution'>
&mdash;
Ralph Waldo Emerson, 19th century American essayist and philosopher from his journal of 1849
</cite>
</li>
<li>
One rightist political party, Yisrael Beiteinu, has already left Ehud Olmert’s governing coalition and another, the religious Shas, threatens to do so if the talks <b>broach</b> the sharing of Jerusalem, as they eventually must.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/broach/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='broach#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td class='partform'>broach</td>
<td>
&rarr;
</td>
<td class='meaning'>pointed stick, spike</td>
</tr>
</table>
<p>To <em>broach</em> a subject is to pierce it with a &#8220;spike,&#8221; thereby symbolically &#8220;breaking the ice&#8221; to introduce it, usually because it&#8217;s a difficult topic to bring up.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='broach#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Mr. Bean</strong><span> He broaches the question of whether Mr. Bean should continue working for them ... or not.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/broach.jpg' video_url='examplevids/broach' video_width='350'></span>
<div id='wt-container'>
<img alt="Broach" height="288" src="https://cdn1.membean.com/video/examplevids/broach.jpg" width="350" />
<div class='center'>
<a href="broach#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='broach#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Broach" src="https://cdn2.membean.com/public/images/wordimages/cons2/broach.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='broach#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>apprise</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>disinter</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>divulge</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>elucidate</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>evince</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>explicate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>garrulous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>interlocutor</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>loquacious</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abortive</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>abstinence</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>circumvent</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>clandestine</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>covert</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>dissemble</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>dour</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>ensconce</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>eschew</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>expunge</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>extirpate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>furtive</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>interminable</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>latent</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>obfuscate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>obviate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>recant</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>refrain</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>renege</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>rescind</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>surreptitious</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>unobtrusive</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="broach" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>broach</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="broach#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

