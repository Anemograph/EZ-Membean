
<!DOCTYPE html>
<html>
<head>
<title>Word: martinet | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something bad or painful abates, it becomes less strong or severe.</p>
<p class='rw-defn idx1' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx2' style='display:none'>If you alleviate pain or suffering, you make it less intense or severe.</p>
<p class='rw-defn idx3' style='display:none'>When something attenuates, it lessens in size or intensity; it becomes thin or weakened.</p>
<p class='rw-defn idx4' style='display:none'>An autocratic person rules with complete power; consequently, they make decisions and give orders to people without asking them for their opinion or advice.</p>
<p class='rw-defn idx5' style='display:none'>If one&#8217;s powers or rights are circumscribed, they are limited or restricted.</p>
<p class='rw-defn idx6' style='display:none'>If someone is granted clemency, they are punished less severely than they could have been—they have been shown mercy.</p>
<p class='rw-defn idx7' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx8' style='display:none'>If you condone someone&#8217;s behavior, you go along with it and provide silent support for it—despite having doubts about it.</p>
<p class='rw-defn idx9' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx10' style='display:none'>If someone will countenance something, they will approve, tolerate, or support it.</p>
<p class='rw-defn idx11' style='display:none'>A despot is a leader who has a lot of power and uses it in a cruel and unreasonable way.</p>
<p class='rw-defn idx12' style='display:none'>Draconian rules and laws are extremely strict and harsh.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is exacting expects others to work very hard and carefully.</p>
<p class='rw-defn idx14' style='display:none'>When someone is exonerated from guilt, a task, or a responsibility, they are set free or excused from it.</p>
<p class='rw-defn idx15' style='display:none'>If circumstances extenuate someone’s actions in a questionable situation, you feel that it was reasonable for someone to break the usual rules; therefore, you partly excuse and sympathize with their wrongdoing.</p>
<p class='rw-defn idx16' style='display:none'>Something that is immutable is always the same and cannot be changed.</p>
<p class='rw-defn idx17' style='display:none'>Someone who is imperious behaves in a proud, overbearing, and highly confident manner that shows they expect to be obeyed without question.</p>
<p class='rw-defn idx18' style='display:none'>If you indemnify someone against something bad happening, you promise to protect them from financial loss or legal responsibility if it happens.</p>
<p class='rw-defn idx19' style='display:none'>Someone who is irresolute is unable to decide what to do.</p>
<p class='rw-defn idx20' style='display:none'>If you mitigate something that causes harm, you reduce the harmful or painful effects of it.</p>
<p class='rw-defn idx21' style='display:none'>An obstinate person refuses to change their mind, even when other people think they are being highly unreasonable.</p>
<p class='rw-defn idx22' style='display:none'>Someone who is pertinacious is determined to continue doing something rather than giving up—even when it gets very difficult.</p>
<p class='rw-defn idx23' style='display:none'>A potentate is a ruler who has great power over people.</p>
<p class='rw-defn idx24' style='display:none'>Something statutory, such as the power given to a governor or president, is created, established, and controlled by rules and laws; hence, it has the full force of the law behind it and must be followed.</p>
<p class='rw-defn idx25' style='display:none'>If you are steadfast, you have a firm belief in your actions or opinions and refuse to give up or change them because you are certain that you are doing the right thing.</p>
<p class='rw-defn idx26' style='display:none'>A stricture is a rule or condition that imposes restrictions or limits on what you can do.</p>
<p class='rw-defn idx27' style='display:none'>Stringent measures or rules are strict, severe, and controlled in a very tight fashion.</p>
<p class='rw-defn idx28' style='display:none'>A tenacious person does not quit until they finish what they&#8217;ve started.</p>
<p class='rw-defn idx29' style='display:none'>Someone who is unassuming is not boastful or arrogant; rather, they are modest or humble.</p>
<p class='rw-defn idx30' style='display:none'>If you are unflagging while doing a task, you are untiring when working upon it and do not stop until it is finished.</p>
<p class='rw-defn idx31' style='display:none'>When you have a vehement feeling about something, you feel very strongly or intensely about it.</p>
<p class='rw-defn idx32' style='display:none'>Someone who is zealous spends a lot of time, energy, and effort to support something—  notably that of a political or religious nature—because they believe in it very strongly.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 5
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>martinet</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='martinet#' id='pronounce-sound' path='audio/words/amy-martinet'></a>
MAHR-tn-et
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='martinet#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='martinet#' id='context-sound' path='audio/wordcontexts/brian-martinet'></a>
Once an effective commander in the army, the mayor was now a harsh master or <em>martinet</em> who demanded strict obedience to the law.  Our absolute ruler or <em>martinet</em> believed that order created a healthy society, so he supported extreme punishments against lawbreakers.  Citizens were concerned that their leader&#8217;s policies would soon resemble those of well-known political <em>martinets</em> or unbending dictators from history.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>martinet</em>?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
A person who rises to leadership during his or her life.
</li>
<li class='choice '>
<span class='result'></span>
A person who craves power and control.
</li>
<li class='choice answer '>
<span class='result'></span>
A person who demands that people absolutely follow the rules.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='martinet#' id='definition-sound' path='audio/wordmeanings/amy-martinet'></a>
A <em>martinet</em> is a very strict person who demands that people obey rules exactly.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>strict rule enforcer</em>
</span>
</span>
</div>
<a class='quick-help' href='martinet#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='martinet#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/martinet/memory_hooks/3561.json'></span>
<p>
<span class="emp0"><span>In<span class="emp3"><span>ternet</span></span> Mar<span class="emp3"><span>tinet</span></span></span></span> Some people would say that nations who place restrictions on In<span class="emp3"><span>ternet</span></span> use by their people are mar<span class="emp3"><span>tinet</span></span>s of the In<span class="emp3"><span>ternet</span></span>.
</p>
</div>

<div id='memhook-button-bar'>
<a href="martinet#" id="add-public-hook" style="" url="https://membean.com/mywords/martinet/memory_hooks">Use other public hook</a>
<a href="martinet#" id="memhook-use-own" url="https://membean.com/mywords/martinet/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='martinet#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Selinger was equal parts <b>martinet</b> and Svengali to the American women, a charismatic figure who could keep his teams in the gym eight hours a day, six days a week and make them like it.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
His prim teacher, Miss Rich, a <b>martinet</b> in tweed suit and broach (“And no laughing, either,” she sputters), requites him with a dreadful report card that produces a storm of parental prohibition: “No ice cream! No movies! No sleeping! No breathing! No nothing!”
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
<li>
What is it about football, anyway, that causes its observers to venerate <b>martinets</b>? . . . Hall of Fame general manager Bill Polian, 74, says every coach he had growing up carried himself like a drill sergeant.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/martinet/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='martinet#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
</table>
<p>The word <em>martinet</em> comes from Jean &#8220;Martinet,&#8221; a Dutch colonel who was known for his very strict and sometimes severe disciplinary techniques.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='martinet#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Matilda</strong><span> Miss Trunchbull is a martinet who is so strict she even disapproves of pigtails.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/martinet.jpg' video_url='examplevids/martinet' video_width='350'></span>
<div id='wt-container'>
<img alt="Martinet" height="198" src="https://cdn1.membean.com/video/examplevids/martinet.jpg" width="350" />
<div class='center'>
<a href="martinet#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='martinet#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Martinet" src="https://cdn3.membean.com/public/images/wordimages/cons2/martinet.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='martinet#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='4' class = 'rw-wordform notlearned'><span>autocratic</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>circumscribe</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>despot</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>draconian</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>exacting</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>immutable</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>imperious</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>obstinate</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>pertinacious</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>potentate</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>statutory</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>steadfast</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>stricture</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>stringent</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>tenacious</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>unflagging</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>vehement</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>zealous</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>abate</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>alleviate</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>attenuate</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>clemency</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>condone</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>countenance</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>exonerate</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>extenuate</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>indemnify</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>irresolute</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>mitigate</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>unassuming</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="martinet" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>martinet</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="martinet#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

