
<!DOCTYPE html>
<html>
<head>
<title>Word: incompatible | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Incompatible-large" id="bk-img" src="https://cdn0.membean.com/public/images/wordimages/bkgd2/incompatible-large.jpg?qdep8" />
</div>
<a href="incompatible#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>When you are in accord with someone, you are in agreement or harmony with them.</p>
<p class='rw-defn idx1' style='display:none'>An affable person is pleasant, friendly, and easy to talk to.</p>
<p class='rw-defn idx2' style='display:none'>If you are amenable to doing something, you willingly accept it without arguing.</p>
<p class='rw-defn idx3' style='display:none'>An amicable person is very friendly and agreeable towards others.</p>
<p class='rw-defn idx4' style='display:none'>Amity is a peaceful friendship between two parties, particularly between two countries.</p>
<p class='rw-defn idx5' style='display:none'>If you have animus against someone, you have a strong feeling of dislike or hatred towards them, often without a good reason or based upon your personal prejudices.</p>
<p class='rw-defn idx6' style='display:none'>Your antagonist is an opponent in a competition or battle.</p>
<p class='rw-defn idx7' style='display:none'>Antipathy is a strong feeling of dislike or hostility towards someone or something.</p>
<p class='rw-defn idx8' style='display:none'>Two points or places are antipodal if they are directly opposite each other, such as the north and south poles; likewise, ideas can be antipodal if they are direct opposites.</p>
<p class='rw-defn idx9' style='display:none'>The antithesis of something is its opposite.</p>
<p class='rw-defn idx10' style='display:none'>Something that is apposite is relevant or suitable to what is happening or being discussed.</p>
<p class='rw-defn idx11' style='display:none'>If you are bellicose, you behave in an aggressive way and are likely to start an argument or fight.</p>
<p class='rw-defn idx12' style='display:none'>A belligerent person or country is aggressive, very unfriendly, and likely to start a fight.</p>
<p class='rw-defn idx13' style='display:none'>Someone who is cantankerous is bad-tempered and always finds things to complain about.</p>
<p class='rw-defn idx14' style='display:none'>If someone is complaisant, they are willing to please others and do what they want without complaining.</p>
<p class='rw-defn idx15' style='display:none'>Conciliation is a process intended to end an argument between two groups of people.</p>
<p class='rw-defn idx16' style='display:none'>A condign reward or punishment is deserved by and appropriate or fitting for the person who receives it.</p>
<p class='rw-defn idx17' style='display:none'>A congenial person, place, or environment is pleasant, friendly, and enjoyable.</p>
<p class='rw-defn idx18' style='display:none'>When a group of people reaches a consensus, it has reached a general agreement about something.</p>
<p class='rw-defn idx19' style='display:none'>When someone is contentious, they are argumentative and likely to provoke a fight.</p>
<p class='rw-defn idx20' style='display:none'>A coterie is a small group of people who are close friends; therefore, when its members do things together, they do not want other people to join them.</p>
<p class='rw-defn idx21' style='display:none'>A man who is debonair wears fashionable clothes and is sophisticated, charming, friendly, and confident.</p>
<p class='rw-defn idx22' style='display:none'>A disaffected member of a group or organization is not satisfied with it; consequently, they feel little loyalty towards it.</p>
<p class='rw-defn idx23' style='display:none'>A situation or thing that is discordant does not fit in with other things; therefore, it is disagreeable, strange, or unpleasant.</p>
<p class='rw-defn idx24' style='display:none'>Things that are disparate are clearly different from each other and belong to different groups or classes.</p>
<p class='rw-defn idx25' style='display:none'>Dissension is a disagreement or difference of opinion among a group of people that can cause conflict.</p>
<p class='rw-defn idx26' style='display:none'>A dissident is someone who disagrees publicly with a government, especially in a country where this is not allowed.</p>
<p class='rw-defn idx27' style='display:none'>Dissonance is an unpleasant situation of opposition in which ideas or actions are not in agreement or harmony; dissonance also refers to a harsh combination of sounds.</p>
<p class='rw-defn idx28' style='display:none'>Empathy is the ability to understand how people feel because you can imagine what it is to be like them.</p>
<p class='rw-defn idx29' style='display:none'>A small group or issue that is factious is controversial and promotes strong disagreement or dissatisfaction within a larger group.</p>
<p class='rw-defn idx30' style='display:none'>A fracas is a rough and noisy fight or loud argument that can involve multiple people.</p>
<p class='rw-defn idx31' style='display:none'>A halcyon time is calm, peaceful, and undisturbed.</p>
<p class='rw-defn idx32' style='display:none'>When two people are in a harmonious state, they are in agreement with each other; when a sound is harmonious, it is pleasant or agreeable to the ear.</p>
<p class='rw-defn idx33' style='display:none'>Something that is incongruous is strange when considered along with its group because it seems very different from the other things in its group.</p>
<p class='rw-defn idx34' style='display:none'>If someone is inimical, they are unfriendly and will likely cause you harm.</p>
<p class='rw-defn idx35' style='display:none'>When something happens at an inopportune time it is inconvenient or not suitable.</p>
<p class='rw-defn idx36' style='display:none'>Two irreconcilable opinions or points of view are so opposed to each other that it is not possible to accept both of them or reach a settlement between them.</p>
<p class='rw-defn idx37' style='display:none'>Polarization between two groups is a division or separation caused by a difference in opinion or conflicting views.</p>
<p class='rw-defn idx38' style='display:none'>If two people have established a good rapport, their connection is such that they have a good understanding of and can communicate well with one other.</p>
<p class='rw-defn idx39' style='display:none'>Rapprochement is the development of greater understanding and friendliness between two countries or groups of people after a period of unfriendly relations.</p>
<p class='rw-defn idx40' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx41' style='display:none'>Repugnance is a strong feeling of dislike for something or someone you find horrible and offensive.</p>
<p class='rw-defn idx42' style='display:none'>A schism causes a group or organization to divide into two groups as a result of differences in their aims and beliefs.</p>
<p class='rw-defn idx43' style='display:none'>Strife is struggle or conflict that sometimes turns violent.</p>
<p class='rw-defn idx44' style='display:none'>Synergy is the extra energy or additional effectiveness gained when two groups or organizations combine their efforts instead of working separately.</p>
<p class='rw-defn idx45' style='display:none'>A troglodyte is reclusive, severely lacking in social skills, and is out of step with current times.</p>
<p class='rw-defn idx46' style='display:none'>All people involved in a unanimous decision agree or are united in their opinion about something.</p>
<p class='rw-defn idx47' style='display:none'>Doing something in unison is doing it all together at one time.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>incompatible</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='incompatible#' id='pronounce-sound' path='audio/words/amy-incompatible'></a>
in-kuhm-PAT-uh-buhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='incompatible#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='incompatible#' id='context-sound' path='audio/wordcontexts/brian-incompatible'></a>
Larry and Lucy are <em>incompatible</em>: they can&#8217;t get along at all, no matter how much they try to do so.  Perhaps one reason that they are <em>incompatible</em> or always in disagreement is because Larry likes lots of noise whereas Lucy likes the silence.  Larry loves to stay out late, whereas Lucy likes to go to bed early, another good reason why they are <em>incompatible</em> or unsuited for one another.  The main reason that they are <em>incompatible</em> or conflicting is that each are unwilling to compromise in any way whatsoever, which will eventually lead them towards separation.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What happens if you are <em>incompatible</em> with someone?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
You disagree with them so much that you don&#8217;t get along with them.
</li>
<li class='choice '>
<span class='result'></span>
You spend so much time together that you get tired of each other sometimes.
</li>
<li class='choice '>
<span class='result'></span>
You thought about dating them but decided to just be friends instead.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='incompatible#' id='definition-sound' path='audio/wordmeanings/amy-incompatible'></a>
If two people are <em>incompatible</em>, they do not get along, tend to disagree, and are unable to cooperate with one another.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>disagreeing</em>
</span>
</span>
</div>
<a class='quick-help' href='incompatible#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='incompatible#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/incompatible/memory_hooks/6111.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Un</span></span>suit<span class="emp1"><span>able</span></span> <span class="emp3"><span>Comp</span></span>anions</span></span> People who are <span class="emp1"><span>in</span></span><span class="emp3"><span>comp</span></span>at<span class="emp1"><span>ible</span></span> are <span class="emp1"><span>un</span></span>suit<span class="emp1"><span>able</span></span> <span class="emp3"><span>comp</span></span>anions for each other.
</p>
</div>

<div id='memhook-button-bar'>
<a href="incompatible#" id="add-public-hook" style="" url="https://membean.com/mywords/incompatible/memory_hooks">Use other public hook</a>
<a href="incompatible#" id="memhook-use-own" url="https://membean.com/mywords/incompatible/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='incompatible#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
"Are athletics and academics <b>incompatible</b>? Well, athletics and engineering—very definitely. Neither side is very giving about relaxing its demands." [Don] Canham [Michigan athletic director] says, "They're not <b>incompatible</b>, but they're very difficult to combine."
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Coalitions tend to consist of a big party and a few like-minded small ones. In post-communist countries, in contrast, political parties have small memberships, personalities matter more than ideologies and the division of spoils looms large. Coalitions often include seemingly <b>incompatible</b> members.
<cite class='attribution'>
&mdash;
The Economist
</cite>
</li>
<li>
All of a sudden, the [Stone] Roses were not merely a band, but a phenomenon that was reshaping youth culture in its image. . . . They would steadily orbit one another in the decades that followed—never quite fast friends, much less kindred spirits. Rather, they were often wildly <b>incompatible</b> individuals who couldn’t quite get out of the gravitation[al] pull of the other.
<cite class='attribution'>
&mdash;
The Independent
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/incompatible/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='incompatible#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='in_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='incompatible#'>
<span class='common'></span>
in-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='com_with' data-tree-url='//cdn1.membean.com/public/data/treexml' href='incompatible#'>
<span class=''></span>
com-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>with, together</td>
</tr>
<tr>
<td class='partform'>pat</td>
<td>
&rarr;
</td>
<td class='meaning'>suffer, endure, tolerate</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ible_handy' data-tree-url='//cdn1.membean.com/public/data/treexml' href='incompatible#'>
<span class=''></span>
-ible
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>handy, capable of</td>
</tr>
</table>
<p>When two people are <em>incompatible</em>, they are &#8220;not capable of suffering (or putting up) with&#8221; each other, that is, they find it impossible to be patient with one another.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='incompatible#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Veronica</strong><span> These two people are incompatible.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/incompatible.jpg' video_url='examplevids/incompatible' video_width='350'></span>
<div id='wt-container'>
<img alt="Incompatible" height="288" src="https://cdn1.membean.com/video/examplevids/incompatible.jpg" width="350" />
<div class='center'>
<a href="incompatible#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='incompatible#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Incompatible" src="https://cdn3.membean.com/public/images/wordimages/cons2/incompatible.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='incompatible#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='5' class = 'rw-wordform notlearned'><span>animus</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>antagonist</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>antipathy</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>antipodal</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>antithesis</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>bellicose</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>belligerent</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>cantankerous</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>contentious</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>disaffected</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>discordant</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>disparate</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>dissension</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>dissident</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>dissonance</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>factious</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>fracas</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>incongruous</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>inimical</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>inopportune</span> &middot;</li><li data-idx='36' class = 'rw-wordform notlearned'><span>irreconcilable</span> &middot;</li><li data-idx='37' class = 'rw-wordform notlearned'><span>polarization</span> &middot;</li><li data-idx='40' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='41' class = 'rw-wordform notlearned'><span>repugnance</span> &middot;</li><li data-idx='42' class = 'rw-wordform notlearned'><span>schism</span> &middot;</li><li data-idx='43' class = 'rw-wordform notlearned'><span>strife</span> &middot;</li><li data-idx='45' class = 'rw-wordform notlearned'><span>troglodyte</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>accord</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>affable</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>amenable</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>amicable</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>amity</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>apposite</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>complaisant</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>conciliation</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>condign</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>congenial</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>consensus</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>coterie</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>debonair</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>empathy</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>halcyon</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>harmonious</span> &middot;</li><li data-idx='38' class = 'rw-wordform notlearned'><span>rapport</span> &middot;</li><li data-idx='39' class = 'rw-wordform notlearned'><span>rapprochement</span> &middot;</li><li data-idx='44' class = 'rw-wordform notlearned'><span>synergy</span> &middot;</li><li data-idx='46' class = 'rw-wordform notlearned'><span>unanimous</span> &middot;</li><li data-idx='47' class = 'rw-wordform notlearned'><span>unison</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='incompatible#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
compatible
<sup class='pos'>adj</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>able to get along or cooperate with another</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="incompatible" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>incompatible</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="incompatible#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn1.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

