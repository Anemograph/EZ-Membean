
<!DOCTYPE html>
<html>
<head>
<title>Word: nonconformist | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn3.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When something is aberrant, it is unusual, not socially acceptable, or a departure from the norm.</p>
<p class='rw-defn idx1' style='display:none'>An apostate has abandoned their religious faith, political party, or devotional cause.</p>
<p class='rw-defn idx2' style='display:none'>An autonomous person makes their own decisions without being influenced by anyone else; an autonomous country or organization is independent and has the power to govern itself.</p>
<p class='rw-defn idx3' style='display:none'>A bohemian is someone who lives outside mainstream society, often embracing an unconventional lifestyle of self-expression, creativity, and rebellion against conventional rules and ways of society.</p>
<p class='rw-defn idx4' style='display:none'>Bourgeois members of society are from the upper middle class and are typically conservative, traditional, and materialistic.</p>
<p class='rw-defn idx5' style='display:none'>A conventional way of thinking or behaving is the one most commonly accepted by social groups.</p>
<p class='rw-defn idx6' style='display:none'>Decorous appearance or behavior is respectable, polite, and appropriate for a given occasion.</p>
<p class='rw-defn idx7' style='display:none'>When someone&#8217;s behavior is deviating, they do things differently by departing or straying from their usual way of acting.</p>
<p class='rw-defn idx8' style='display:none'>Dissension is a disagreement or difference of opinion among a group of people that can cause conflict.</p>
<p class='rw-defn idx9' style='display:none'>A dissident is someone who disagrees publicly with a government, especially in a country where this is not allowed.</p>
<p class='rw-defn idx10' style='display:none'>Divergent opinions differ from each other; divergent paths move apart from one another.</p>
<p class='rw-defn idx11' style='display:none'>An animal or person that is docile is not aggressive; rather, they are well-behaved, quiet, and easy to control.</p>
<p class='rw-defn idx12' style='display:none'>Someone is being dogmatic when they express opinions in an assertive and often overly self-important manner.</p>
<p class='rw-defn idx13' style='display:none'>If someone is eccentric, they behave in a strange and unusual way that is different from most people.</p>
<p class='rw-defn idx14' style='display:none'>The adjective ecclesiastical refers to a priest or member of the clergy in the Christian Church; this word also refers to something associated with a church as an organized institution.</p>
<p class='rw-defn idx15' style='display:none'>A small group or issue that is factious is controversial and promotes strong disagreement or dissatisfaction within a larger group.</p>
<p class='rw-defn idx16' style='display:none'>A heretic is someone who doubts or acts in opposition to commonly or generally accepted beliefs.</p>
<p class='rw-defn idx17' style='display:none'>Heterodox beliefs, ideas, or practices are different from accepted or official ones.</p>
<p class='rw-defn idx18' style='display:none'>An iconoclast is someone who often attacks beliefs, ideas, or customs that are generally accepted by society.</p>
<p class='rw-defn idx19' style='display:none'>A maverick individual does not follow general opinions of society as a whole; rather, they think for themselves and often strike out on their own.</p>
<p class='rw-defn idx20' style='display:none'>A social norm is the standard, model, or rule by which people conduct themselves.</p>
<p class='rw-defn idx21' style='display:none'>Someone has a parochial outlook when they are mostly concerned with narrow or limited issues that affect a small, local area or a very specific cause; they are also unconcerned with wider or more general issues.</p>
<p class='rw-defn idx22' style='display:none'>Propriety is behaving in a socially acceptable and appropriate way.</p>
<p class='rw-defn idx23' style='display:none'>Protocol is the rules of conduct or proper behavior that a social situation demands.</p>
<p class='rw-defn idx24' style='display:none'>Something that is proverbial is very well-known, famous, or known for its wisdom.</p>
<p class='rw-defn idx25' style='display:none'>A radical solution to a problem goes to the heart or root of it; a radical solution is most often an extreme solution to the problem.</p>
<p class='rw-defn idx26' style='display:none'>A schism causes a group or organization to divide into two groups as a result of differences in their aims and beliefs.</p>
<p class='rw-defn idx27' style='display:none'>If you say that someone is servile, you don&#8217;t respect them because they are too obedient, too agreeable, and too willing to do anything for another person.</p>
<p class='rw-defn idx28' style='display:none'>If you are staid, you are set in your ways; consequently, you are settled, serious, law-abiding, conservative, and traditional—and perhaps even a tad dull.</p>
<p class='rw-defn idx29' style='display:none'>Something statutory, such as the power given to a governor or president, is created, established, and controlled by rules and laws; hence, it has the full force of the law behind it and must be followed.</p>
<p class='rw-defn idx30' style='display:none'>A stereotype is a customary way of thinking about a particular group of people that is narrow-minded and often faulty.</p>
<p class='rw-defn idx31' style='display:none'>If you are subservient, you are too eager and willing to do what other people want and often put your own wishes aside.</p>
<p class='rw-defn idx32' style='display:none'>A tenet is a belief held by a group, organization, or person.</p>
<p class='rw-defn idx33' style='display:none'>An unorthodox opinion is unusual, not customary, and goes against established ways of thinking.</p>
<p class='rw-defn idx34' style='display:none'>If you act in a wayward fashion, you are very headstrong, independent, disobedient, unpredictable, and practically unmanageable.</p>
<p class='rw-defn idx35' style='display:none'>A whimsical idea or person is slightly strange, unusual, and amusing rather than serious and practical.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>nonconformist</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='nonconformist#' id='pronounce-sound' path='audio/words/amy-nonconformist'></a>
non-kuhn-FAWR-mist
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='nonconformist#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='nonconformist#' id='context-sound' path='audio/wordcontexts/brian-nonconformist'></a>
My brother Gilbert was a true <em>nonconformist</em>, for he was unwilling to follow the generally accepted opinions or actions of others.  He was even a <em>nonconformist</em> or person who did not follow the crowd in middle school, which was a pretty brave thing to do.  As he got older his tendency to be a <em>nonconformist</em> continued, so he would often speak against what most everyone else believed in.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What might a <em>nonconformist</em> do?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
They might have an opinion that is considered brilliant.
</li>
<li class='choice answer '>
<span class='result'></span>
They might go against an established pattern of behavior.
</li>
<li class='choice '>
<span class='result'></span>
They might agree with just about everyone else.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='nonconformist#' id='definition-sound' path='audio/wordmeanings/amy-nonconformist'></a>
A <em>nonconformist</em> is unwilling to believe in the same things other people do or act in a fashion that society sets as a standard.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>not doing what others do</em>
</span>
</span>
</div>
<a class='quick-help' href='nonconformist#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='nonconformist#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/nonconformist/memory_hooks/3464.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>No</span></span> <span class="emp2"><span>Can</span></span> <span class="emp3"><span>Form</span></span></span></span> I <span class="emp2"><span>can</span></span><span class="emp1"><span>no</span></span>t <span class="emp3"><span>form</span></span> the same opinion as you and everyone else, for I am a <span class="emp1"><span>no</span></span>n<span class="emp2"><span>con</span></span><span class="emp3"><span>form</span></span>ist.
</p>
</div>

<div id='memhook-button-bar'>
<a href="nonconformist#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/nonconformist/memory_hooks">Use other hook</a>
<a href="nonconformist#" id="memhook-use-own" url="https://membean.com/mywords/nonconformist/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='nonconformist#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
Originals are <b>nonconformists</b>, people who not only have new ideas but take action to champion them. They are people who stand out and speak up. Originals drive creativity and change in the world.
<span class='attribution'>&mdash; Adam Grant, American author and psychologist</span>
<img alt="Adam grant, american author and psychologist" height="80" src="https://cdn0.membean.com/public/images/wordimages/quotes/Adam Grant, American author and psychologist.jpg?qdep8" width="80" />
</li>
<li>
Good old country Butch [Goring] earned something of a reputation as a <b>nonconformist</b> early in his career with the Los Angeles Kings, with whom he spent 11 seasons.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
"We can't all be <b>nonconformists</b> at every moment, but conformity is dangerous," Grant says, explaining the value of originals and iconoclasts in a fascinating and lengthy recent _First Round Review_ article.
<cite class='attribution'>
&mdash;
Inc.
</cite>
</li>
<li>
Quakers and other <b>nonconformists</b> kept alive another tradition — that of the English Revolution that had culminated in the execution of the impious King Charles I in 1649.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/nonconformist/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='nonconformist#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='non_not' data-tree-url='//cdn1.membean.com/public/data/treexml' href='nonconformist#'>
<span class=''></span>
non-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>not</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='con_with' data-tree-url='//cdn1.membean.com/public/data/treexml' href='nonconformist#'>
<span class='common'></span>
con-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>with, together</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='form_shape' data-tree-url='//cdn1.membean.com/public/data/treexml' href='nonconformist#'>
<span class=''></span>
form
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>shape</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ist_performer' data-tree-url='//cdn1.membean.com/public/data/treexml' href='nonconformist#'>
<span class=''></span>
-ist
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>one who performs a certain action</td>
</tr>
</table>
<p>A <em>nonconformist</em> is &#8220;one who does not shape herself with&#8221; the ideas of others.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='nonconformist#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>A Wild And Crazy Guy: Steve Martin 1978</strong><span> The nonconformist oath.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/nonconformist.jpg' video_url='examplevids/nonconformist' video_width='350'></span>
<div id='wt-container'>
<img alt="Nonconformist" height="288" src="https://cdn1.membean.com/video/examplevids/nonconformist.jpg" width="350" />
<div class='center'>
<a href="nonconformist#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='nonconformist#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Nonconformist" src="https://cdn2.membean.com/public/images/wordimages/cons2/nonconformist.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='nonconformist#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>aberrant</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>apostate</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>autonomous</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>bohemian</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>deviate</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>dissension</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>dissident</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>divergent</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>eccentric</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>factious</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>heretic</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>heterodox</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>iconoclast</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>maverick</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>radical</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>schism</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>unorthodox</span> &middot;</li><li data-idx='34' class = 'rw-wordform notlearned'><span>wayward</span> &middot;</li><li data-idx='35' class = 'rw-wordform notlearned'><span>whimsical</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='4' class = 'rw-wordform notlearned'><span>bourgeois</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>conventional</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>decorous</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>docile</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>dogmatic</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>ecclesiastical</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>norm</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>parochial</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>propriety</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>protocol</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>proverbial</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>servile</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>staid</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>statutory</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>stereotype</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>subservient</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>tenet</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='nonconformist#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://www.membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
conformity
<sup class='pos'>n</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>agreement with accepted societal behavior</td>
</tr>
<tr>
<td class='wordform'>
<span>
conform
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to believe in the same things that other people do; act in a way that is acceptable to society</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="nonconformist" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>nonconformist</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="nonconformist#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

