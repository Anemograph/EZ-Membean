
<!DOCTYPE html>
<html>
<head>
<title>Word: spatial | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Something that is capacious has a lot of space and can contain a lot of things.</p>
<p class='rw-defn idx1' style='display:none'>A cavernous space is very large and empty; it is both hollow and huge.</p>
<p class='rw-defn idx2' style='display:none'>Something colossal is extremely big, gigantic, or huge.</p>
<p class='rw-defn idx3' style='display:none'>A commodious room or house is large and roomy, which makes it convenient and highly suitable for living.</p>
<p class='rw-defn idx4' style='display:none'>Something expansive has a wide scope or is large in area.</p>
<p class='rw-defn idx5' style='display:none'>A fissure is a narrow and long crack or opening, usually in a rock face.</p>
<p class='rw-defn idx6' style='display:none'>Something gargantuan is extremely large.</p>
<p class='rw-defn idx7' style='display:none'>Something infinitesimal is so extremely small or minute that it is very hard to measure it.</p>
<p class='rw-defn idx8' style='display:none'>The adjective interstitial pertains to a narrow opening or a crack between two things.</p>
<p class='rw-defn idx9' style='display:none'>A lacuna is an empty space or gap where something is missing; if there is a lacuna in a person&#8217;s argument, for example, part of that argument is lacking.</p>
<p class='rw-defn idx10' style='display:none'>A leviathan is something that is very large, powerful, difficult to control, and rather frightening.</p>
<p class='rw-defn idx11' style='display:none'>A macrocosm is a large, complex, and organized system or structure that is made of many small parts that form one unit, such as a society or the universe.</p>
<p class='rw-defn idx12' style='display:none'>A microcosm is a small group, place, or activity that has all the same qualities as a much larger one; therefore, it seems like a smaller version of it.</p>
<p class='rw-defn idx13' style='display:none'>Something minuscule is extremely small in size or amount.</p>
<p class='rw-defn idx14' style='display:none'>A palatial structure is grand and impressive, such as a palace or mansion.</p>
<p class='rw-defn idx15' style='display:none'>A panoramic view of a landscape is a sweeping or wide-ranging view of it.</p>
<p class='rw-defn idx16' style='display:none'>Something that is prodigious is very large, impressive, or great.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>spatial</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='spatial#' id='pronounce-sound' path='audio/words/amy-spatial'></a>
SPAY-shuhl
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='spatial#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='spatial#' id='context-sound' path='audio/wordcontexts/brian-spatial'></a>
I had a hard time with <em>spatial</em> relationships in math, or how distant something was from something else.  Even today I have a hard time with <em>spatial</em> matter, or situations relating to space.  I feel overwhelmed even considering <em>spatial</em> aspects of the Universe itself; how can something be that huge?
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If someone has a strong <em>spatial</em> awareness, what do they have?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
The ability to judge the size of a room.
</li>
<li class='choice '>
<span class='result'></span>
The ability to add numbers quickly in their head.
</li>
<li class='choice '>
<span class='result'></span>
The ability to sense when someone else is whispering.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='spatial#' id='definition-sound' path='audio/wordmeanings/amy-spatial'></a>
The <em>spatial</em> aspect of a room or other area is the amount of space it has.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>of open space</em>
</span>
</span>
</div>
<a class='quick-help' href='spatial#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='spatial#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/spatial/memory_hooks/4602.json'></span>
<p>
<span class="emp0"><span><span class="emp3"><span>Spat</span></span>ial</span></span>  When you know that the letter "<span class="emp3"><span>t</span></span>" can change to the letter "<span class="emp3"><span>c</span></span>," it's easy to see how "<span class="emp3"><span>spat</span></span>ial" concerns "<span class="emp3"><span>spac</span></span>e."
</p>
</div>

<div id='memhook-button-bar'>
<a href="spatial#" id="add-public-hook" style="" url="https://membean.com/mywords/spatial/memory_hooks">Use other public hook</a>
<a href="spatial#" id="memhook-use-own" url="https://membean.com/mywords/spatial/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='spatial#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
Adult mice that voluntarily used running wheels increased their number of brain cells and performed better at <b>spatial</b> learning tests than non-exercising mice, they discovered.
<cite class='attribution'>
&mdash;
WebMD
</cite>
</li>
<li>
This is hardly enough room for even one good golf hole, but Robert Trent Jones, the golf architect who was called in, dealt with the <b>spatial</b> problem with fine imagination.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
The only way to find out whether there is a biological basis for differences in <b>spatial</b> skills, LeVay said, is to scan the brains of very young children to determine if the differences are present at a very young age.
<cite class='attribution'>
&mdash;
Wired
</cite>
</li>
<li>
Here’s another insight about dyslexia: men with the condition may have enhanced visual-<b>spatial</b> abilities, according to a study in the journal Learning and Individual Differences.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/spatial/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='spatial#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='spat_distance' data-tree-url='//cdn1.membean.com/public/data/treexml' href='spatial#'>
<span class=''></span>
spati
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>space</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='al_relating' data-tree-url='//cdn1.membean.com/public/data/treexml' href='spatial#'>
<span class=''></span>
-al
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or relating to</td>
</tr>
</table>
<p>The word <em>spatial</em> means &#8220;of or relating to space.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='spatial#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Spatial" src="https://cdn3.membean.com/public/images/wordimages/cons2/spatial.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='spatial#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='0' class = 'rw-wordform notlearned'><span>capacious</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>cavernous</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>colossal</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>commodious</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>expansive</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>fissure</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>gargantuan</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>interstitial</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>lacuna</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>leviathan</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>macrocosm</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>microcosm</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>minuscule</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>palatial</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>panoramic</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>prodigious</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='7' class = 'rw-wordform notlearned'><span>infinitesimal</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="spatial" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>spatial</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="spatial#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn2.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn2.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

