
<!DOCTYPE html>
<html>
<head>
<title>Word: abortive | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>Something done under the aegis of an organization or person is done with their support, backing, and protection.</p>
<p class='rw-defn idx1' style='display:none'>Something that is apposite is relevant or suitable to what is happening or being discussed.</p>
<p class='rw-defn idx2' style='display:none'>The adjective auspicious describes a positive beginning of something, such as a new business, or a certain time that looks to have a good chance of success or prosperity.</p>
<p class='rw-defn idx3' style='display:none'>If you describe someone as benign, they are kind, gentle, and harmless.</p>
<p class='rw-defn idx4' style='display:none'>A commodious room or house is large and roomy, which makes it convenient and highly suitable for living.</p>
<p class='rw-defn idx5' style='display:none'>A condign reward or punishment is deserved by and appropriate or fitting for the person who receives it.</p>
<p class='rw-defn idx6' style='display:none'>If someone will countenance something, they will approve, tolerate, or support it.</p>
<p class='rw-defn idx7' style='display:none'>If you curtail something, you reduce or limit it.</p>
<p class='rw-defn idx8' style='display:none'>To decry something is to speak against it and find fault with it.</p>
<p class='rw-defn idx9' style='display:none'>Something that is deleterious has a harmful effect.</p>
<p class='rw-defn idx10' style='display:none'>A diminution of something is a reduction in the size, number, or importance of it.</p>
<p class='rw-defn idx11' style='display:none'>If you disparage someone or something, you say unpleasant words that show you have no respect for that person or thing.</p>
<p class='rw-defn idx12' style='display:none'>If you ensconce yourself somewhere, you put yourself into a comfortable place or safe position and have no intention of moving or leaving for quite some time.</p>
<p class='rw-defn idx13' style='display:none'>When you excise something, you remove it by cutting it out.</p>
<p class='rw-defn idx14' style='display:none'>If you exhort someone to do something, you try very hard to persuade them to do it.</p>
<p class='rw-defn idx15' style='display:none'>A thing or action that is expedient is useful, advantageous, or appropriate to get something accomplished.</p>
<p class='rw-defn idx16' style='display:none'>To expurgate part of a book, play, or other text is to remove parts of it before publishing because they are considered objectionable, harmful, or offensive.</p>
<p class='rw-defn idx17' style='display:none'>If you extirpate something, you completely destroy it because it is unpleasant or unwanted.</p>
<p class='rw-defn idx18' style='display:none'>If you are experiencing felicity about something, you are very happy about it.</p>
<p class='rw-defn idx19' style='display:none'>A fruitless effort at doing something does not bring about a successful result.</p>
<p class='rw-defn idx20' style='display:none'>A halcyon time is calm, peaceful, and undisturbed.</p>
<p class='rw-defn idx21' style='display:none'>A hapless person is unlucky, so much so that a stream of unfortunate things has happened and continues to happen to them.</p>
<p class='rw-defn idx22' style='display:none'>Someone who is irresolute is unable to decide what to do.</p>
<p class='rw-defn idx23' style='display:none'>A laudatory article or speech expresses praise or admiration for someone or something.</p>
<p class='rw-defn idx24' style='display:none'>If you malign someone, you say unpleasant things about them to damage their reputation.</p>
<p class='rw-defn idx25' style='display:none'>Something that is propitious shows favorable conditions.</p>
<p class='rw-defn idx26' style='display:none'>A providential event is a very lucky one because it happens at exactly the right time and often when it is needed most.</p>
<p class='rw-defn idx27' style='display:none'>If you refrain from doing something, you do not do it—even though you want to.</p>
<p class='rw-defn idx28' style='display:none'>If something is reviled, it is intensely hated and criticized.</p>
<p class='rw-defn idx29' style='display:none'>An untoward situation is something that is unfavorable, unfortunate, inappropriate, or troublesome.</p>
<p class='rw-defn idx30' style='display:none'>If you vilify people, you write or say bad things about them in an attempt to make them unpopular.</p>
<p class='rw-defn idx31' style='display:none'>Vituperative remarks are full of hate, anger, and cruel criticism.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Adj.</li>
<li>
Level 3
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>abortive</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='abortive#' id='pronounce-sound' path='audio/words/amy-abortive'></a>
uh-BAWR-tiv
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='abortive#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='abortive#' id='context-sound' path='audio/wordcontexts/brian-abortive'></a>
Jayma was highly frustrated by her failed, <em>abortive</em> attempt to write her first novel.  The <em>abortive</em> effort at literary fame did not succeed as she thought it should, so she stopped at the third chapter.  Jayma decided to <em>abort</em> the project without further ado and cast the incomplete pages into the fireplace.  As she watched the <em>abortive</em>, unsuccessful narrative burst into flames, her thoughts turned toward another possible plot.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is an <em>abortive</em> military mission?
</h3>
<ul id='choice-section'>
<li class='choice '>
<span class='result'></span>
One which does not go as planned.
</li>
<li class='choice answer '>
<span class='result'></span>
One which is not accomplished.
</li>
<li class='choice '>
<span class='result'></span>
One which is carried out in secrecy.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='abortive#' id='definition-sound' path='audio/wordmeanings/amy-abortive'></a>
An <em>abortive</em> attempt or action is cut short before it is finished; hence, it is unsuccessful.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>failed</em>
</span>
</span>
</div>
<a class='quick-help' href='abortive#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='abortive#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/abortive/memory_hooks/4942.json'></span>
<p>
<span class="emp0"><span><span class="emp2"><span>A</span></span> Short-l<span class="emp1"><span>ive</span></span>d <span class="emp3"><span>Bor</span></span>edom</span></span> Hopefully <span class="emp2"><span>a</span></span> <span class="emp3"><span>bor</span></span>ing speech will not l<span class="emp1"><span>ive</span></span> for too long, so we can all call it <span class="emp2"><span>a</span></span><span class="emp3"><span>bor</span></span>t<span class="emp1"><span>ive</span></span> before too long.
</p>
</div>

<div id='memhook-button-bar'>
<a href="abortive#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/abortive/memory_hooks">Use other hook</a>
<a href="abortive#" id="memhook-use-own" url="https://membean.com/mywords/abortive/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='abortive#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
The basis for the <b>abortive</b> negotiations was provided by a phone conversation that took place between Foreman and the player’s lawyers shortly after Barry’s most recent appeal against the Caps' injunction failed.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
Many members said that the Speaker had rebounded from the <b>abortive</b> coup by focusing more attention on running the House and meeting with rank-and-file members.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
The attack, which also left 25 militants dead, took place following an <b>abortive</b> ambush on a coalition convoy route by suspected Taliban in central Oruzgan province, a military statement said.
<cite class='attribution'>
&mdash;
CNN
</cite>
</li>
<li>
Soon after he shot at Walker, Oswald took an <b>abortive</b> fling at organizing a Fair Play for Cuba Committee in New Orleans, but his deepest drive then was to get to Cuba himself.
<cite class='attribution'>
&mdash;
TIME
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/abortive/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='abortive#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ab_away' data-tree-url='//cdn1.membean.com/public/data/treexml' href='abortive#'>
<span class='common'></span>
ab-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>away, from</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='or_rise' data-tree-url='//cdn1.membean.com/public/data/treexml' href='abortive#'>
<span class=''></span>
or
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>rise, begin</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ive_does' data-tree-url='//cdn1.membean.com/public/data/treexml' href='abortive#'>
<span class=''></span>
-ive
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>of or that which does something</td>
</tr>
</table>
<p>An <em>abortive</em> attempt at a project has risen away or begun to move from its original goal, and therefore cannot be completed.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='abortive#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Space Shuttle</strong><span> Abort the launch!</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='198' video_image_url='https://cdn1.membean.com/video/examplevids/abortive.jpg' video_url='examplevids/abortive' video_width='350'></span>
<div id='wt-container'>
<img alt="Abortive" height="198" src="https://cdn1.membean.com/video/examplevids/abortive.jpg" width="350" />
<div class='center'>
<a href="abortive#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='abortive#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Abortive" src="https://cdn0.membean.com/public/images/wordimages/cons2/abortive.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='abortive#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='7' class = 'rw-wordform notlearned'><span>curtail</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>decry</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>deleterious</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>diminution</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>disparage</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>excise</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>expurgate</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>extirpate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>fruitless</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>hapless</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>irresolute</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>malign</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>refrain</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>revile</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>untoward</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>vilify</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>vituperative</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>aegis</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>apposite</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>auspicious</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>benign</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>commodious</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>condign</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>countenance</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>ensconce</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>exhort</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>expedient</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>felicity</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>halcyon</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>laudatory</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>propitious</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>providential</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='abortive#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
abort
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>to cut something short</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="abortive" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>abortive</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="abortive#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

