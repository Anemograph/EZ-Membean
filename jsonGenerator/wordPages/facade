
<!DOCTYPE html>
<html>
<head>
<title>Word: facade | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn1.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<div id='bk-img-container'>
<img alt="Facade-large" id="bk-img" src="https://cdn1.membean.com/public/images/wordimages/bkgd2/facade-large.jpg?qdep8" />
</div>
<a href="facade#" id="bk-show">show<br/>image</a>
<p class='rw-defn idx0' style='display:none'>Candor is the quality of being honest and open in speech or action.</p>
<p class='rw-defn idx1' style='display:none'>Something that is clandestine is hidden or kept a secret, often because it is illegal.</p>
<p class='rw-defn idx2' style='display:none'>Covert activities or situations are secret and hidden.</p>
<p class='rw-defn idx3' style='display:none'>Something that is delusive deceives you by giving a false belief about yourself or the situation you are in.</p>
<p class='rw-defn idx4' style='display:none'>Someone who is disingenuous is not straightforward or is dishonest in what they say or do.</p>
<p class='rw-defn idx5' style='display:none'>If you feign something, such as a sickness or an attitude, you pretend and try to convince people that you have it—even though you don&#8217;t.</p>
<p class='rw-defn idx6' style='display:none'>A feint is the act of pretending to make a movement in one direction while actually moving in the other, especially to trick an opponent; a feint can also be a deceptive act meant to turn attention away from one&#8217;s true purpose.</p>
<p class='rw-defn idx7' style='display:none'>If someone behaves in a furtive way, they do things sneakily and secretly in order to avoid being noticed.</p>
<p class='rw-defn idx8' style='display:none'>If you gloss a difficult word, phrase, or other text, you provide an explanation for it in the form of a note.</p>
<p class='rw-defn idx9' style='display:none'>Someone who is hypocritical pretends to be a person they are not.</p>
<p class='rw-defn idx10' style='display:none'>People who are ingenuous are excessively trusting and believe almost everything that people tell them, especially because they have not had much life experience.</p>
<p class='rw-defn idx11' style='display:none'>An overt act is not hidden or secret but is done in an open and public way.</p>
<p class='rw-defn idx12' style='display:none'>A patina is a smooth, shiny film or surface that gradually develops on things—such as wood, leather, and metal utensils—that have seen a lot of use.</p>
<p class='rw-defn idx13' style='display:none'>Probity is very moral and honest behavior.</p>
<p class='rw-defn idx14' style='display:none'>A semblance is an outward appearance of what is wanted or expected but is not exactly as hoped for.</p>
<p class='rw-defn idx15' style='display:none'>A simulacrum is an image or representation of something that can be a true copy or may just have a vague similarity to it.</p>
<p class='rw-defn idx16' style='display:none'>If you employ subterfuge, you use a secret plan or action to get what you want by outwardly doing one thing that cleverly hides your true intentions.</p>
<p class='rw-defn idx17' style='display:none'>A veneer is a thin layer, such as a thin sheet of expensive wood over a cheaper type of wood that gives a false appearance of higher quality; a person can also put forth a false front or veneer.</p>
<p class='rw-defn idx18' style='display:none'>The veracity of something is its truthfulness.</p>
<p class='rw-defn idx19' style='display:none'>The verity of something is the truth or reality of it.</p>
<p class='rw-defn idx20' style='display:none'>Wiles are clever tricks or cunning schemes that are used to persuade someone to do what you want.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='alternate' id='misc-word-info'>
<li>Noun</li>
<li>
Level 2
<br>
<small>
High School
</small>
</li>
</ul>
<div class='alternate' id='wordform-container'>
<h1 class='wordform'>facade</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='facade#' id='pronounce-sound' path='audio/words/amy-facade'></a>
fuh-SAHD
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='facade#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='facade#' id='context-sound' path='audio/wordcontexts/brian-facade'></a>
When my friend Henry faked the election for student President, I put up a <em>facade</em> or false front of not caring about the fact that he won unjustly.  Despite the fact that I acted uncaring by putting up a <em>facade</em> or deceptive appearance of being OK about this, I was nevertheless very uncomfortable about the whole thing.  For the rest of my time in high school I had to put up a fake mask or <em>facade</em> whenever I saw him because I knew that what he had done was wrong, and that I was in the wrong as well since I hadn&#8217;t reported it.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
What is a <em>facade</em>?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
It is a false appearance or display of emotion.
</li>
<li class='choice '>
<span class='result'></span>
It is temporary wall or partition to divide a space.
</li>
<li class='choice '>
<span class='result'></span>
It is a plastic surgeon&#8217;s fixing of someone&#8217;s skin after a terrible accident.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='facade#' id='definition-sound' path='audio/wordmeanings/amy-facade'></a>
A <em>facade</em> is a false outward appearance or way of behaving that hides what someone or something is really like.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>false front</em>
</span>
</span>
</div>
<a class='quick-help' href='facade#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='facade#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/facade/memory_hooks/5890.json'></span>
<p>
<span class="emp0"><span>Sur<span class="emp1"><span>fac</span></span>e Ac<span class="emp3"><span>ade</span></span>mic</span></span> Although Jack says he's a great ac<span class="emp3"><span>ade</span></span>mic, it's only on the sur<span class="emp1"><span>fac</span></span>e; it's just a <span class="emp1"><span>fac</span></span><span class="emp3"><span>ade</span></span> to make people think he's smart.
</p>
</div>

<div id='memhook-button-bar'>
<a href="facade#" id="add-public-hook" style="" url="https://membean.com/mywords/facade/memory_hooks">Use other public hook</a>
<a href="facade#" id="memhook-use-own" url="https://membean.com/mywords/facade/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='facade#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li class='word-quote'>
It turns out that an eerie type of chaos can lurk just behind a <b>facade</b> of order—and yet, deep inside the chaos lurks an even eerier type of order.
<span class='attribution'>&mdash; Douglas Hofstadter, American scholar</span>
<img alt="Douglas hofstadter, american scholar" height="80" src="https://cdn1.membean.com/public/images/wordimages/quotes/Douglas Hofstadter, American scholar.jpg?qdep8" width="80" />
</li>
<li>
The judge’s declaration of a mistrial provided one of those breathtaking moments when the <b>facade</b> of a Big Lie is peeled back to reveal the men behind the curtain.
<cite class='attribution'>
&mdash;
Rolling Stone
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/facade/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='facade#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='fac_face' data-tree-url='//cdn1.membean.com/public/data/treexml' href='facade#'>
<span class=''></span>
fac
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>face, surface</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='ade_thing' data-tree-url='//cdn1.membean.com/public/data/treexml' href='facade#'>
<span class=''></span>
-ade
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>thing or person made by a certain process</td>
</tr>
</table>
<p>A <em>facade</em> is simply the outward &#8220;face or surface&#8221; of something, while the true essence lies beneath.</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='facade#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>YouTube:PunchRobert</strong><span> Revealing the facade.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/facade.jpg' video_url='examplevids/facade' video_width='350'></span>
<div id='wt-container'>
<img alt="Facade" height="288" src="https://cdn1.membean.com/video/examplevids/facade.jpg" width="350" />
<div class='center'>
<a href="facade#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='facade#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Facade" src="https://cdn2.membean.com/public/images/wordimages/cons2/facade.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='facade#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='1' class = 'rw-wordform notlearned'><span>clandestine</span> &middot;</li><li data-idx='2' class = 'rw-wordform notlearned'><span>covert</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>delusive</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>disingenuous</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>feign</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>feint</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>furtive</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>gloss</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>hypocritical</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>patina</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>semblance</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>simulacrum</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>subterfuge</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>veneer</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>wile</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>candor</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>ingenuous</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>overt</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>probity</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>veracity</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>verity</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="facade" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>facade</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="facade#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn1.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

