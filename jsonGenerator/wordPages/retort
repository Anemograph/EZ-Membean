
<!DOCTYPE html>
<html>
<head>
<title>Word: retort | Membean</title>
<script src="https://membean.com/cdn-cgi/apps/head/YD_9Bn3e0IPhQdz4DMUu0Tn3Y8s.js"></script><link href='https://membean.com/favicon.ico' rel='shortcut icon'>
<link href='https://membean.com/apple-touch-icon.png?v=3162a74' rel='apple-touch-icon' sizes='180x180'>
<link href='https://membean.com/favicon-32x32.png?v=3162a74' rel='icon' sizes='32x32' type='image/png'>
<link href='https://membean.com/favicon-16x16.png?v=3162a74' rel='icon' sizes='16x16' type='image/png'>
<link href='https://membean.com/manifest.json' rel='manifest'>
<link color='#60953c' href='https://membean.com/safari-pinned-tab.svg' rel='mask-icon'>
<meta content='Membean helps students remember vocabulary for the GRE/SAT/ACT. It tunes into students’ forgetting patterns and teaches words in multiple ways by applying learning techniques from linguistic research.' name='description'>
<meta name='keywords'>
<meta content='text/html; charset=UTF-8' http-equiv='content-type'>
<link href="https://cdn0.membean.com/public/vendor/stylesheets/bundle_all_vendor_v5.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mb.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<link href="https://membean.com/stylesheets/bundle_mbword.css?qdep8" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<link href="https://membean.com/stylesheets/idevices.css?qdep8" media="screen" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<link href="https://membean.com/stylesheets/ie.css?qdep8" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<style>body .error { padding:0 ; border: none;}</style>
<![endif]-->

<!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HSNJLJ');
</script>
<!-- End Google Tag Manager -->


</head>
<body class='full-word-page' id='customized_words'>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSNJLJ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- /for sticky footer -->
<div id='root'>
<div class='screen-width' id='header-wrapper'>
<div id='header'>
<div id='logo'><a href="https://membean.com/" class="small-logo"></a></div>
<div id='page-menu'>
<ul class='secondary'>
<li>
<a href="https://membean.com/logout" class="nav-logout">Sign out</a>
</li>
<li>
<a href="https://membean.com/classic_dashboard" class="nav-dashboard">Dashboard</a>
</li>
<li>
<a href="https://membean.com/help" class="nav-help">Help</a>
</li>
<li>
<a href="https://membean.com/settings" class="nav-dashboard">My Info</a>
</li>
</ul>

</div>

</div>
</div>
<div id='content-wrapper'>
<div id='spinner'></div>

<div class='screen-width' id='section1-wrapper'>
<p class='rw-defn idx0' style='display:none'>When you accede to a demand or proposal, you agree to it, especially after first disagreeing with it.</p>
<p class='rw-defn idx1' style='display:none'>If you acquiesce to something, you allow it to happen by doing what someone wants without putting up a struggle or voicing your own concerns.</p>
<p class='rw-defn idx2' style='display:none'>If you do something with alacrity, you do it eagerly and quickly.</p>
<p class='rw-defn idx3' style='display:none'>If you are amenable to doing something, you willingly accept it without arguing.</p>
<p class='rw-defn idx4' style='display:none'>Badinage is lighthearted conversation that involves teasing, jokes, and humor.</p>
<p class='rw-defn idx5' style='display:none'>Banter is friendly conversation during which people make friendly jokes and laugh at—and with—one another.</p>
<p class='rw-defn idx6' style='display:none'>Someone who has a bristling personality is easily offended, annoyed, or angered.</p>
<p class='rw-defn idx7' style='display:none'>If you describe a person&#8217;s behavior or speech as brusque, you mean that they say or do things abruptly or too quickly in a somewhat rude and impolite way.</p>
<p class='rw-defn idx8' style='display:none'>If something moves or grows with celerity, it does so rapidly.</p>
<p class='rw-defn idx9' style='display:none'>If you behave with deference towards someone, you show them respect and accept their opinions or decisions, especially because they have an important position.</p>
<p class='rw-defn idx10' style='display:none'>If you describe someone, usually a young woman, as demure, you mean that she is quiet, shy, and always behaves modestly.</p>
<p class='rw-defn idx11' style='display:none'>An animal or person that is docile is not aggressive; rather, they are well-behaved, quiet, and easy to control.</p>
<p class='rw-defn idx12' style='display:none'>An irrefutable argument or statement cannot be proven wrong; therefore, it must be accepted because it is certain.</p>
<p class='rw-defn idx13' style='display:none'>Obeisance is respect and obedience shown to someone or something, expressed by bowing or some other humble gesture.</p>
<p class='rw-defn idx14' style='display:none'>If someone is being obsequious, they are trying so hard to please someone that they lack sincerity in their actions towards that person.</p>
<p class='rw-defn idx15' style='display:none'>Someone is obtuse when they are slow to understand things; someone can choose to be obtuse when they deliberately do not try to figure things out.</p>
<p class='rw-defn idx16' style='display:none'>To parry is to ward something off or deflect it.</p>
<p class='rw-defn idx17' style='display:none'>If you rebuff someone, you give an unfriendly answer to a suggestion or offer of help; you hastily turn that person away.</p>
<p class='rw-defn idx18' style='display:none'>If you reciprocate, you give something to someone because they have given something similar to you.</p>
<p class='rw-defn idx19' style='display:none'>When you offer recompense to someone, you give them something, usually money, for the trouble or loss that you have caused them or as payment for their help.</p>
<p class='rw-defn idx20' style='display:none'>To recoup is to get back an amount of money you have lost or spent.</p>
<p class='rw-defn idx21' style='display:none'>If you redress a complaint or a bad situation, you correct or improve it for the person who has been wronged, usually by paying them money or offering an apology.</p>
<p class='rw-defn idx22' style='display:none'>A rejoinder is a quick answer to a reply or remark that can be rude, angry, clever, or defensive.</p>
<p class='rw-defn idx23' style='display:none'>Restitution is the formal act of giving something that was taken away back to the rightful owner—or paying them money for the loss of the object.</p>
<p class='rw-defn idx24' style='display:none'>When you retaliate, you get back at or get even with someone for something that they did to you.</p>
<p class='rw-defn idx25' style='display:none'>People who are reticent are unwilling to share information, especially about themselves, their thoughts, or their feelings.</p>
<p class='rw-defn idx26' style='display:none'>A riposte is a quick and clever reply that is often made in answer to criticism of some kind.</p>
<p class='rw-defn idx27' style='display:none'>Someone&#8217;s sardonic smile, expression, or comment shows a lack of respect for what someone else has said or done; this occurs because the former thinks they are too important to consider or discuss such a supposedly trivial matter of the latter.</p>
<p class='rw-defn idx28' style='display:none'>If you say that someone is servile, you don&#8217;t respect them because they are too obedient, too agreeable, and too willing to do anything for another person.</p>
<p class='rw-defn idx29' style='display:none'>If you are subservient, you are too eager and willing to do what other people want and often put your own wishes aside.</p>
<p class='rw-defn idx30' style='display:none'>If you feel unrequited love for another, you love that person, but they don&#8217;t love you in return.</p>
<p class='rw-defn idx31' style='display:none'>Something that is vacuous is empty or blank, such as a mind or stare.</p>
<p class='rw-defn idx32' style='display:none'>Vitriolic words are bitter, unkind, and mean-spirited.</p>
<p class='rw-defn idx33' style='display:none'>Vituperative remarks are full of hate, anger, and cruel criticism.</p>

<div id='section1'>

<noscript>
<span class='error'>You need to enable Javascript to get the best out of this site. Please :-).</span>
</noscript>
<div id='top-row-word'>
<ul class='' id='misc-word-info'>
<li>Noun</li>
<li>
Level 1
<br>
<small>
High School
</small>
</li>
</ul>
<div class='' id='wordform-container'>
<h1 class='wordform'>retort</h1>
<span id='add-note'></span>
<!-- /%span#pos -->
<!-- /  ="#{word.pos.to_s.downcase}." -->
<div id='spelled-pron'>
<span id='orthoepy'>
<a class='sound' href='retort#' id='pronounce-sound' path='audio/words/amy-retort'></a>
ri-TAWRT
<a class='hidden' id='pronounce-sound-dummy'></a>
</span>
</div>
</div>


<div id='word-flags'>
</div>
</div>
<div class='' id='main-column'>
<div class='module-large panel' id='context'>
<h3 class='title'>
Context
<div class='panel-button-bar'>
<a href='retort#' alt='help' class='button help-button video-link ' id='context-help' video-id='contexts'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<p id='context-paragraph'>
<a class='sound' href='retort#' id='context-sound' path='audio/wordcontexts/brian-retort'></a>
My wife and I were having an argument, and I thought that I had made a reasonable point when she suddenly came back with a <em>retort</em> or sharp, biting reply.  I was not particularly happy by that, and so offered up yet another <em>retort</em>, or snappy response in kind.  We went on giving <em>retorts</em> or trading angry insults until we both started laughing at and finally with one another.
</p>
<h3 class='question'>
<strong>Q<span>uiz:</span></strong>
<span class='status wrong hidden'>Try again!</span>
If you want to give a <em>retort</em> to your sister, what should you do?
</h3>
<ul id='choice-section'>
<li class='choice answer '>
<span class='result'></span>
Quickly think of something clever to say to back to her.
</li>
<li class='choice '>
<span class='result'></span>
Dress up like a pirate and scare her when she comes home.
</li>
<li class='choice '>
<span class='result'></span>
Write out a long apology and put it where she will find it.
</li>
</ul>

<div id='definition'>
<a class='show-definition'>Definition</a>
<div class='def-bubble' style='display:none'>
<ol>
<li class='def-text'>
<a class='sound' href='retort#' id='definition-sound' path='audio/wordmeanings/amy-retort'></a>
When you give a <em>retort</em> to what someone has said, you reply in a quick and witty fashion.
</li>
</ol>
<div class='def-bubble-foot'></div>
<div id='one-word-tab'>
<span class='quick-glance'>
Quick glance:
<span class='one-word-tab-right'>
<em>witty reply</em>
</span>
</span>
</div>
<a class='quick-help' href='retort#' id='defn-note'>
A brief word on definitions.
<p class='quick-help-text hidden'>Not all possible definitions of a word are given &mdash; only the ones most likely to be tested. The word constellation has additional meanings of the word or use the fast dictionary in the bottom toolbar.</p>
</a>
</div>
</div>


</div>
</div>
<div class='module-large panel' id='memhook'>
<h3 class='title'>
Memory Hook
<div class='panel-button-bar'>
<a href='retort#' alt='help' class='button help-button video-link ' id='memhook-help' video-id='memhooks'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<div class='hook'>
<span url='/mywords/retort/memory_hooks/3046.json'></span>
<p>
<span class="emp0"><span><span class="emp1"><span>Ret</span></span>urned a Sh<span class="emp3"><span>ort</span></span> Sn<span class="emp3"><span>ort</span></span></span></span> Her method of <span class="emp1"><span>ret</span></span><span class="emp3"><span>ort</span></span> was <span class="emp1"><span>ret</span></span>urning a sh<span class="emp3"><span>ort</span></span> sn<span class="emp3"><span>ort</span></span> in way of a <span class="emp1"><span>ret</span></span><span class="emp3"><span>ort</span></span> to my admittedly weak insult.
</p>
</div>

<div id='memhook-button-bar'>
<a href="retort#" id="add-public-hook" style="display:none" url="https://membean.com/mywords/retort/memory_hooks">Use other hook</a>
<a href="retort#" id="memhook-use-own" url="https://membean.com/mywords/retort/memory_hooks.json">Make My Own</a>
</div>

</div>
</div>
<div class='module-large panel' id='examples'>
<h3 class='title'>
Examples
<div class='panel-button-bar'>
<a href='retort#' alt='help' class='button help-button video-link ' id='examples-help' video-id='journals'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<ul>
<li>
“What grind are you talking about?” was Carroll’s <b>retort</b> during a conference call with Packers beat reporters on Wednesday.
<cite class='attribution'>
&mdash;
Sports Illustrated
</cite>
</li>
<li>
“Why are you yelling?” my husband would say. “I'm not,” I'd <b>retort</b>.
<cite class='attribution'>
&mdash;
NPR
</cite>
</li>
<li>
These questions will naturally sound absurd to an American, and he might <b>retort</b>, “Well, how does it feel to be a Frenchman?”
<cite class='attribution'>
&mdash;
The Atlantic
</cite>
</li>
<li>
“Ah, but he was born in Texas,” can be the <b>retort</b>, though he moved to Kansas as an infant and grew up there.
<cite class='attribution'>
&mdash;
The New York Times
</cite>
</li>
</ul>


</div>
</div>

</div>
<div class='' id='secondary-column'>
<div id='note-container'>
<div class='note-template hidden'>
<div class='invisible' data-create-url='/mywords/retort/notes' format='json'>
<div class='note-header'>
<span class='note-status'></span>
<a class='note-delete'></a>
</div>
<div class='note-content' contenteditable='true'>Add a note or make a sentence using this word. Get creative!</div>
</div>
</div>
</div>
<div class='module-medium panel ' id='word-structure'>
<h3 class='title'>
Word Ingredients
<div class='panel-button-bar'>
<a href='retort#' alt='help' class='button help-button video-link ' id='word-structure-help' video-id='ingredients'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<!-- Also use in treelist.html.haml. -->
<table>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='re_back' data-tree-url='//cdn1.membean.com/public/data/treexml' href='retort#'>
<span class='common'></span>
re-
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>back, again</td>
</tr>
<tr>
<td>
<a class='wi-btn partform' data-tree-id='tort_twisted' data-tree-url='//cdn1.membean.com/public/data/treexml' href='retort#'>
<span class=''></span>
tort
</a>
</td>
<td>
&rarr;
</td>
<td class='meaning'>twisted, wound, wrapped</td>
</tr>
</table>
<p>A <em>retort</em> is a reply that is &#8220;twisted back&#8221; or &#8220;wrapped again.&#8221;</p>


</div>
</div>
<div class='module-medium panel ' id='word-theater'>
<h3 class='title'>
Word Theater
<div class='panel-button-bar'>
<a href='retort#' alt='help' class='button help-button video-link ' id='word-theater-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div class='caption'>
<strong>Presidential Debate, Carter v. Reagan, 1980</strong><span> A pretty mild retort.</span>
</div>
<span class='wt-config' id='wt' replace_el='wt-container' video_height='288' video_image_url='https://cdn1.membean.com/video/examplevids/retort.jpg' video_url='examplevids/retort' video_width='350'></span>
<div id='wt-container'>
<img alt="Retort" height="288" src="https://cdn1.membean.com/video/examplevids/retort.jpg" width="350" />
<div class='center'>
<a href="retort#" class="play-btn"></a>
</div>
</div>
<p class='hidden help'>The panel shows a small video clip of either the word in actual use or a scene that represents  the meaning of a word. This not only breaks up the monotony of studying words but also provides another avenue to strengthen word meaning. Enjoy!</p>


</div>
</div>
<div class='module-medium panel ' id='wordmap-container'>
<h3 class='title'>
Word Constellation
<div class='panel-button-bar'>
<a href='retort#' alt='help' class='button help-button video-link ' id='wordmap-container-help' video-id='constellation'><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<div id='wordmap-wrapper'>
<img alt="Retort" src="https://cdn2.membean.com/public/images/wordimages/cons2/retort.png?qdep3" />
<span class='prompt invisible'>Click for an interactive map of this word</span>
</div>


</div>
</div>
<div class='module-medium panel ' id='related-words'>
<h3 class='title'>
Related Words
<div class='panel-button-bar'>
<a href='retort#' alt='help' class='button help-button video-link ' id='related-words-help' video-id='relateds'><img alt="Help" src="https://membean.com/images/help.png?qdep8" /></a>
</div>
</h3>
<div class='content'>
<!-- /always cache definitions -->
<ul class='related-syns'><li data-idx='2' class = 'rw-wordform notlearned'><span>alacrity</span> &middot;</li><li data-idx='4' class = 'rw-wordform notlearned'><span>badinage</span> &middot;</li><li data-idx='5' class = 'rw-wordform notlearned'><span>banter</span> &middot;</li><li data-idx='6' class = 'rw-wordform notlearned'><span>bristling</span> &middot;</li><li data-idx='7' class = 'rw-wordform notlearned'><span>brusque</span> &middot;</li><li data-idx='8' class = 'rw-wordform notlearned'><span>celerity</span> &middot;</li><li data-idx='16' class = 'rw-wordform notlearned'><span>parry</span> &middot;</li><li data-idx='17' class = 'rw-wordform notlearned'><span>rebuff</span> &middot;</li><li data-idx='18' class = 'rw-wordform notlearned'><span>reciprocate</span> &middot;</li><li data-idx='19' class = 'rw-wordform notlearned'><span>recompense</span> &middot;</li><li data-idx='20' class = 'rw-wordform notlearned'><span>recoup</span> &middot;</li><li data-idx='21' class = 'rw-wordform notlearned'><span>redress</span> &middot;</li><li data-idx='22' class = 'rw-wordform notlearned'><span>rejoinder</span> &middot;</li><li data-idx='23' class = 'rw-wordform notlearned'><span>restitution</span> &middot;</li><li data-idx='24' class = 'rw-wordform notlearned'><span>retaliate</span> &middot;</li><li data-idx='26' class = 'rw-wordform notlearned'><span>riposte</span> &middot;</li><li data-idx='27' class = 'rw-wordform notlearned'><span>sardonic</span> &middot;</li><li data-idx='32' class = 'rw-wordform notlearned'><span>vitriolic</span> &middot;</li><li data-idx='33' class = 'rw-wordform notlearned'><span>vituperative</span> &middot;</li></ul>
<ul class='related-ants'><li data-idx='0' class = 'rw-wordform notlearned'><span>accede</span> &middot;</li><li data-idx='1' class = 'rw-wordform notlearned'><span>acquiesce</span> &middot;</li><li data-idx='3' class = 'rw-wordform notlearned'><span>amenable</span> &middot;</li><li data-idx='9' class = 'rw-wordform notlearned'><span>deference</span> &middot;</li><li data-idx='10' class = 'rw-wordform notlearned'><span>demure</span> &middot;</li><li data-idx='11' class = 'rw-wordform notlearned'><span>docile</span> &middot;</li><li data-idx='12' class = 'rw-wordform notlearned'><span>irrefutable</span> &middot;</li><li data-idx='13' class = 'rw-wordform notlearned'><span>obeisance</span> &middot;</li><li data-idx='14' class = 'rw-wordform notlearned'><span>obsequious</span> &middot;</li><li data-idx='15' class = 'rw-wordform notlearned'><span>obtuse</span> &middot;</li><li data-idx='25' class = 'rw-wordform notlearned'><span>reticent</span> &middot;</li><li data-idx='28' class = 'rw-wordform notlearned'><span>servile</span> &middot;</li><li data-idx='29' class = 'rw-wordform notlearned'><span>subservient</span> &middot;</li><li data-idx='30' class = 'rw-wordform notlearned'><span>unrequited</span> &middot;</li><li data-idx='31' class = 'rw-wordform notlearned'><span>vacuous</span> &middot;</li></ul>
<ul class='related-words-legend'>
<li class='legend-syns'>Similar sense</li>
<li class='legend-ants'>Opposite sense</li>
</ul>


</div>
</div>
<div class='module-medium panel ' id='word-variants'>
<h3 class='title'>
Word Variants
<div class='panel-button-bar'>
<a href='retort#' alt='help' class='button help-button video-link ' id='word-variants-help' ><img alt="Help" src="https://membean.com/images/help.png?qdep3" /></a>
</div>
</h3>
<div class='content'>
<table>
<tr>
<td class='wordform'>
<span>
retort
<sup class='pos'>v</sup>
</span>
</td>
<td>
&rarr;
</td>
<td class='meaning'>reply in a quick and witty fashion</td>
</tr>
</table>
<p class='hidden help'>The section lists important variants and alternate definitions of the headword. Knowing variants will often help you both remember and understand the word. Not all variants are listed - only the ones we think that are important for you to know.</p>


</div>
</div>

<div class='timer' id='pbar'></div>
</div>
<div class='hidden' id='config'>
<span id='skip-confirm'></span>
</div>
<div class='hidden' id='tooltips-container'>
<p class='tooltip-marker' data-element='context-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='word-structure-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='memhook-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='examples-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='wordmap-container-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='related-words-help' data-msg='Watch a short video that tells you why this section is important to learn a word.'></p>
<p class='tooltip-marker' data-element='bk-show' data-msg='See background image. Background images are carefully selected to be stimulating and interesting and help encode a word with a mental image.'></p>

</div>


</div>
</div>
</div>
<!-- /for sticky footer -->
<div id='root-footer'></div>

</div>
<div id='primary-toolbar-wrapper'>
<div id='primary-toolbar'>
<div class='hidden' id='fastdict-embedded-wrapper'>
<div class='fastdict-embedded'>
<div id='fastdict-banner'>
<h1>SlowMo</h1>
<h2>A <em><a href="https://membean.com/slowmo/very">very</a></em> fast dictionary.</h2>
</div>
<blockquote class='hidden'>Faster than a speeding bullet, more powerful than a locomotive, and able to leap tall buildings in a single bound. We are getting a tad carried away aren't we? .. :-)</blockquote>
<form action="retort" class="fastdict-entry" method="post" onsubmit="return false">
<input id="word" maxlength="20" name="word" type="text" value="" />
<div class='fastdict-defnbox'>
<h2 id='auto-load'>retort</h2>
</div>
</form>
<div class='fastdict-suggestion-box'></div>

</div>
</div>
<div class='' id='toolbar-bbar'>
<a href="retort#" data-url="/word_learning_strategy" id="help-for-page-icon"><img alt="Help" src="https://membean.com/images/help.png?qdep8" />How To Learn</a>
<a href="https://membean.com/fastdict" id="dictionary-icon"><img alt="Turtle-small-icon" src="https://membean.com/images/turtle-small-icon.png?qdep8" />Dictionary</a>
</div>

</div>
</div>
<div class='screen-width' id='footer-wrapper'>
<div id='footer'>
<!-- /by default we put nothing in the footer other than copyright -->

<p id='copyright'>
All text and design are copyrighted &copy;2010-2022 Membean, Inc. All rights reserved.
<a href="https://membean.com/privacy">Privacy Policy</a>.
<a href="https://membean.com/agreement">Terms of Service</a>.
</p>


</div>
</div>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_essential_v8.js?qdep8" type="text/javascript"></script>
<script src="https://cdn0.membean.com/public/vendor/javascripts/jquery-1.8.3.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn1.membean.com/public/vendor/javascripts/createjs-2015.11.26.min.js?qdep8" type="text/javascript"></script>



<script src="https://membean.com/javascripts/bundle_membean-app-qdep8.js?qdep8" type="text/javascript"></script>

<div id='userconfig'>

</div>
<!-- /used sparingly for things like admin javascript that should be the very last javascript -->
<!-- /included -->
<script src="https://cdn3.membean.com/public/vendor/javascripts/jquery-1.9.0.min.js?qdep8" type="text/javascript"></script>
<script src="https://cdn3.membean.com/public/vendor/javascripts/bundle_ui_v4.js?qdep8" type="text/javascript"></script>

</body>
</html>

