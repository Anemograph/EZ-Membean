//document.getElementById("single-question").getElementsByTagName("em")[0].innerText
//document.getElementById("single-question").className
{
    // content-script.js
    "use strict";
    //let word = question.getElementsByTagName("em")[0].innerText;
    let question = document.getElementById("single-question");
    let questionType = document.getElementById("review-wrapper").classList[0];

    let words = [];
    let provided;
    let hint;

    switch(questionType) {
        case "mc_blank_context":
        case "mc_blank_definition":
            let textQuestion = question.getElementsByClassName("question")[0].innerText;
            textQuestion = textQuestion.split("_").reverse()[0];
            textQuestion = textQuestion.substr(textQuestion.length - 30);
            provided = textQuestion;

            for(let i=0; i<=3; i++){
                words[i] = question.getElementsByClassName("choice")[i].innerText.substring(2).toLowerCase(); 
            }
            break;

        case "cloze_definition": //gives a hint, the definition, and part of word
        case "cloze_context":    //gives a hint, the context, and part of word
        case "cloze_background": //gives a hint, a picture, and part of word
            words[0] = document.getElementById("answer-box").innerText.toLowerCase();
            hint = document.getElementById("word-hint").getElementsByTagName("span")[0].innerText;
            break;

        case "mc_alternate":
        case "mc_context":          //choose the definition that fits
            let wordForm = document.getElementById("wordform-container");
            words[0] = wordForm.getElementsByClassName("wordform")[0].innerText.toLowerCase();
            break;

        case "mc_root":
            words[0] = question.getElementsByTagName("em")[1].innerText.toLowerCase();
            provided = question.getElementsByTagName("em")[0].innerText;
            break;
        default:
    }
    chrome.runtime.sendMessage({type: questionType, words: words, provided: provided, hint: hint});
}
