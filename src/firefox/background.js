"use strict";

function onError(error) {
  console.error(`Error: ${error}`);
}

fetch("./wordlist.json")
  .then(response => response.json())
  .then(json => wordlist = json);

function findWord(provided, caseSensitive = false, search = ["def", "cont", "quest", "hint"]) {
    let result = [];
    for (let i in wordlist){
        for (let j of search){
            if(caseSensitive) {
                if(wordlist[i][j] == provided){
                    result.push(i);
                }
            }else{
                if(wordlist[i][j].indexOf(provided)!=-1){
                    result.push(i);
                }
            }
        }
    }
    return result;
}

function clozeCanidate(match, list) {
    for(let i in list){
        if( (list[i][0]==match[0]) && (list[i][0].length<=match.length) && (list[i].length>=match.length-2) ){
            return list[i];
        }
    }
}

browser.pageAction.onClicked.addListener(function(tab){browser.tabs.executeScript(tab.id, {file: "/content_scripts/load.js"})});

browser.runtime.onMessage.addListener(function(response){
   
    let answer;
    let word;

    switch(response.type){
        case "mc_blank_definition":
        case "mc_blank_context":
            let partOne = response.provided.split("_")[0];
            let partTwo = response.provided.split("_")[1];

            let listOne = findWord(partOne);
            let listTwo = findWord(partTwo);

            for (let x in listOne){
                for (let y in listTwo){
                    if (listOne[x]==listTwo[y]){
                        console.log(listOne[x]);
                        word = listOne[x];
                    };
                }
            }

            if(typeof word != "undefined"){
                answer = word;
            }else{
                for(let i = 0; i<=3; i++){
                    browser.notifications.create({
                        "type": "basic",
                        "title": response.words[i],
                        "message": wordlist[response.words[i]]["def"]
                    });

                }
                return;
            }

            break;

        case "cloze_definition":
        case "cloze_context":
        case "cloze_background":
            word = findWord(response.hint, true, ["hint"]);
            answer = clozeCanidate(response.words[0], word);
            break;

        case "mc_alternate":
            answer = wordlist[response.words[0]]["def"];
            break;

        case "mc_context":
            answer = wordlist[response.words[0]]["quest"];
            break;

        case "mc_root":
            answer = wordlist[response.words[0]]["root"][response.provided]
            break;

        default:
            return;
    };

    browser.notifications.create({
        "type": "basic",
        "title": "answer",
        "message": answer
    });
    return;
});
