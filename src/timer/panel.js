var timerButton = document.getElementById("timerButton");
var rightCounterButton = document.getElementById("rightCounterButton");
var rightResetButton = document.getElementById("rightResetButton");
var wrongCounterButton = document.getElementById("wrongCounterButton");
var wrongResetButton = document.getElementById("wrongResetButton");

var rightCounter = 0;
var wrongCounter = 0;

var timerStartTime = 0;

function updateTimer(){
    var countdownSeconds = 300 - Math.floor((Date.now() - timerStartTime) /1000);
    var seconds = countdownSeconds % 60;
    var minutes = Math.floor(countdownSeconds/60);
    if(countdownSeconds < 0){
        timerButton.value = "Time: Done";
    }else{
        timerButton.value = "Time: " + minutes + ":" + seconds;
    }
}

timerButton.onclick = function() {
    timerStartTime = Date.now();
}

setInterval(updateTimer, 500);

rightCounterButton.onclick = function () {
    rightCounter = rightCounter + 1;
    rightCounterButton.value = "# Right: " + rightCounter;
}
rightResetButton.onclick = function () {
    rightCounter = 0;
    rightCounterButton.value = "# Right: " + rightCounter;
}
wrongCounterButton.onclick = function () {
    wrongCounter = wrongCounter + 1;
    wrongCounterButton.value = "# Wrong: " + wrongCounter;
}
wrongResetButton.onclick = function () {
    wrongCounter = 0;
    wrongCounterButton.value = "# Wrong: " + wrongCounter;
}
