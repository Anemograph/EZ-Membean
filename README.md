# EZ-Membean

A Firefox extension that automatically finds the answers to membean prompts. 

Since the extension only pulls information from the page, it should be very difficult for membean to detect as cheating.

**This is the part where I state that I don't condone cheating. Some of the study questions are terrible, which is why I made this. (Just look at the primeval word picture)**

## Usage

1. Install the extension from the releases page.
2. Press the button in the url bar to scan the page for words while training.

## Developing

Most of the answers are pulled from the learning pages (using wget), and put into an json format (via python script) for the extension to read.
The folder jsonGenerator is where I put the code I used to do that.


I am aware that it currently doesn't support word constellations.